/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.test.domainbuilder.ActionWordExpandos.ActionWordExpando
import org.squashtest.tm.test.domainbuilder.ActionWordExpandos.ActionWordParameterExpando
import org.squashtest.tm.test.domainbuilder.ActionWordExpandos.ActionWordParameterValueExpando
import org.squashtest.tm.test.domainbuilder.ActionWordExpandos.ActionWordTextExpando
import org.squashtest.tm.test.domainbuilder.ApiTokenExpandos.ApiTokenExpando
import org.squashtest.tm.test.domainbuilder.AttachmentExpandos.AttachmentExpando
import org.squashtest.tm.test.domainbuilder.AutomatedExecutionExtenderExpandos.AutomatedExecutionExtenderExpando
import org.squashtest.tm.test.domainbuilder.AutomatedSuiteExpandos.AutomatedSuiteExpando
import org.squashtest.tm.test.domainbuilder.AutomatedTestExpandos.AutomatedTestExpando
import org.squashtest.tm.test.domainbuilder.AutomatedTestTechnologyExpandos.AutomatedTestTechnologyExpando
import org.squashtest.tm.test.domainbuilder.AutomationRequestExpandos.AutomationRequestExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.CampaignExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.CampaignFolderExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.CampaignTestPlanItemExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.TestPlanStatisticValueExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.ExecutionExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.ExecutionStepExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.ExploratoryExecutionExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.ExploratoryExecutionEventExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.ExploratorySessionExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.IterationExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.IterationTestPlanItemExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.KeywordExecutionExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.ScriptedExecutionExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.SessionNoteExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.SprintExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.SprintGroupExpando
import org.squashtest.tm.test.domainbuilder.CampaignExpandos.TestSuiteExpando
import org.squashtest.tm.test.domainbuilder.CustomFieldExpandos.CustomFieldBindingExpando
import org.squashtest.tm.test.domainbuilder.CustomFieldExpandos.CustomFieldExpando
import org.squashtest.tm.test.domainbuilder.CustomFieldExpandos.CustomFieldValueExpando
import org.squashtest.tm.test.domainbuilder.CustomFieldExpandos.DenormalizedFieldValueExpando
import org.squashtest.tm.test.domainbuilder.DenoCustomFieldExpandos.DenoCustomFieldValueExpando
import org.squashtest.tm.test.domainbuilder.IssueExpandos.IssueExpando
import org.squashtest.tm.test.domainbuilder.PermissionGroupExpandos.PermissionGroupExpando
import org.squashtest.tm.test.domainbuilder.ProjectExpandos.CampaignLibraryExpando
import org.squashtest.tm.test.domainbuilder.ProjectExpandos.ProjectExpando
import org.squashtest.tm.test.domainbuilder.ProjectExpandos.TestCaseLibraryExpando
import org.squashtest.tm.test.domainbuilder.ProjectTemplateExpandos.ProjectTemplateExpando
import org.squashtest.tm.test.domainbuilder.CustomReportLibraryExpandos.CustomReportLibraryExpando
import org.squashtest.tm.test.domainbuilder.RequirementExpandos.RequirementExpando
import org.squashtest.tm.test.domainbuilder.RequirementExpandos.RequirementFolderExpando
import org.squashtest.tm.test.domainbuilder.RequirementExpandos.RequirementVersionExpando
import org.squashtest.tm.test.domainbuilder.RequirementExpandos.ResourceExpando
import org.squashtest.tm.test.domainbuilder.ScmRepositoryExpandos.ScmRepositoryExpando
import org.squashtest.tm.test.domainbuilder.ScmServerExpandos.ScmServerExpando
import org.squashtest.tm.test.domainbuilder.TeamExpandos.TeamExpando
import org.squashtest.tm.test.domainbuilder.TestAutomationProjectExpandos.TestAutomationProjectExpando
import org.squashtest.tm.test.domainbuilder.TestAutomationServerExpandos.TestAutomationServerExpando
import org.squashtest.tm.test.domainbuilder.TestCaseExpandos.ActionStepExpando
import org.squashtest.tm.test.domainbuilder.TestCaseExpandos.CallStepExpando
import org.squashtest.tm.test.domainbuilder.TestCaseExpandos.DatasetExpando
import org.squashtest.tm.test.domainbuilder.TestCaseExpandos.DatasetParamValueExpando
import org.squashtest.tm.test.domainbuilder.TestCaseExpandos.ExploratoryTestCaseExpando
import org.squashtest.tm.test.domainbuilder.TestCaseExpandos.KeywordStepExpando
import org.squashtest.tm.test.domainbuilder.TestCaseExpandos.KeywordTestCaseExpando
import org.squashtest.tm.test.domainbuilder.TestCaseExpandos.ParameterExpando
import org.squashtest.tm.test.domainbuilder.TestCaseExpandos.ScriptedTestCaseExpando
import org.squashtest.tm.test.domainbuilder.TestCaseExpandos.TestCaseExpando
import org.squashtest.tm.test.domainbuilder.TestCaseExpandos.TestCaseFolderExpando
import org.squashtest.tm.test.domainbuilder.UserExpandos.UserExpando
import org.squashtest.tm.test.domainbuilder.UsersGroupExpandos.UsersGroupExpando

public class SquashEntityBuilder {

    public static def project(Closure params) {
        ProjectExpando expando = new ProjectExpando()
        expando.with params
        expando.create()
    }

    public static def projectTemplate(Closure params) {
        ProjectTemplateExpando expando = new ProjectTemplateExpando()
        expando.with params
        expando.create()
    }

    public static def user(Closure params) {
        UserExpando expando = new UserExpando()
        expando.with params
        expando.create()
    }

    public static def usersGroup(Closure params) {
        UsersGroupExpando expando = new UsersGroupExpando()
        expando.with params
        expando.create()
    }

    public static def permissionGroup(Closure params) {
        PermissionGroupExpando expando = new PermissionGroupExpando()
        expando.with params
        expando.create()
    }

    public static def team(Closure params) {
        TeamExpando expando = new TeamExpando()
        expando.with params
        expando.create()
    }

    public static def tcLibrary(Closure params) {
        TestCaseLibraryExpando expando = new TestCaseLibraryExpando()
        expando.with params
        expando.create()
    }

    public static def campLibrary(Closure params) {
        CampaignLibraryExpando expando = new CampaignLibraryExpando()
        expando.with params
        expando.create()
    }

    public static def customReportLibrary(Closure params) {
        CustomReportLibraryExpando expando = new CustomReportLibraryExpando()
        expando.with params
        expando.create()
    }

    public static def testCaseFolder(Closure params) {
        TestCaseFolderExpando expando = new TestCaseFolderExpando()
        expando.with params
        expando.create()
    }

    public static def testCase(Closure params) {
        TestCaseExpando expando = new TestCaseExpando()
        expando.with params
        expando.create()
    }

    public static def scriptedTestCase(Closure params) {
        ScriptedTestCaseExpando expando = new ScriptedTestCaseExpando()
        expando.with params
        expando.create()
    }

    public static def keywordTestCase(Closure params) {
        KeywordTestCaseExpando expando = new KeywordTestCaseExpando()
        expando.with params
        expando.create()
    }

    public static def exploratoryTestCase(Closure params) {
        ExploratoryTestCaseExpando expando = new ExploratoryTestCaseExpando()
        expando.with params
        expando.create()
    }

    public static def actionStep(Closure params) {
        ActionStepExpando expando = new ActionStepExpando()
        expando.with params
        expando.create()
    }

    public static def callStep(Closure params) {
        CallStepExpando expando = new CallStepExpando()
        expando.with params
        expando.create()
    }

    public static def keywordStep(Closure params) {
        KeywordStepExpando expando = new KeywordStepExpando()
        expando.with params
        expando.create()
    }

    public static def parameter(Closure params) {
        ParameterExpando expando = new ParameterExpando()
        expando.with params
        expando.create()
    }

    public static def paramValue(Closure params) {
        DatasetParamValueExpando expando = new DatasetParamValueExpando()
        expando.with params
        expando.create()
    }

    public static def dataset(Closure params) {
        DatasetExpando expando = new DatasetExpando()
        expando.with params
        expando.create()
    }

    public static def requirementFolder(Closure params) {
        RequirementFolderExpando expando = new RequirementFolderExpando()
        expando.with params
        expando.create()
    }

    public static def requirementSyncExtender(Closure params) {
        RequirementExpandos.RequirementSyncExtenderExpando expando = new RequirementExpandos.RequirementSyncExtenderExpando()
        expando.with params
        expando.create()
    }

    public static def resource(Closure params) {
        ResourceExpando expando = new ResourceExpando()
        expando.with params
        expando.create()
    }

    public static def requirementVersion(Closure params) {
        RequirementVersionExpando expando = new RequirementVersionExpando()
        expando.with params
        expando.create()
    }

    public static def requirement(Closure params) {
        RequirementExpando expando = new RequirementExpando()
        expando.with params
        expando.create()
    }

    public static def campaign(Closure params) {
        CampaignExpando expando = new CampaignExpando()
        expando.with params
        expando.create()
    }

    public static def campaignFolder(Closure params) {
        CampaignFolderExpando expando = new CampaignFolderExpando()
        expando.with params
        expando.create()
    }

    public static def campaignTestPlanItem(Closure params) {
        CampaignTestPlanItemExpando expando = new CampaignTestPlanItemExpando()
        expando.with params
        expando.create()
    }

    public static def sprint(Closure params) {
        SprintExpando expando = new SprintExpando()
        expando.with params
        expando.create()
    }

    public static def sprintGroup(Closure params) {
        SprintGroupExpando expando = new SprintGroupExpando()
        expando.with params
        expando.create()
    }

    public static def testPlanStatisticValue(Closure params) {
        TestPlanStatisticValueExpando expando = new TestPlanStatisticValueExpando()
        expando.with params
        expando.create()
    }
    // alias for iterationTestPlanItem
    public static def itpi(Closure params) {
        IterationTestPlanItemExpando expando = new IterationTestPlanItemExpando()
        expando.with params
        expando.create()
    }

    public static def testSuite(Closure params) {
        TestSuiteExpando expando = new TestSuiteExpando()
        expando.with params
        expando.create()
    }

    public static def iteration(Closure params) {
        IterationExpando expando = new IterationExpando()
        expando.with params
        expando.create()
    }

    public static def iterationTestPlanItem(Closure params) {
        IterationTestPlanItemExpando expando = new IterationTestPlanItemExpando()
        expando.with params
        expando.create()
    }

    public static def exploratorySession(Closure params) {
        ExploratorySessionExpando expando = new ExploratorySessionExpando()
        expando.with params
        expando.create()
    }

    public static def execution(Closure params) {
        ExecutionExpando expando = new ExecutionExpando()
        expando.with params
        expando.create()
    }

    public static def scriptedExecution(Closure params) {
        ScriptedExecutionExpando expando = new ScriptedExecutionExpando()
        expando.with params
        expando.create()
    }

    public static def keywordExecution(Closure params) {
        KeywordExecutionExpando expando = new KeywordExecutionExpando()
        expando.with params
        expando.create()
    }
    public static def exploratoryExecution(Closure params) {
        ExploratoryExecutionExpando expando = new ExploratoryExecutionExpando()
        expando.with params
        expando.create()
    }

    public static def executionStep(Closure params) {
        ExecutionStepExpando expando = new ExecutionStepExpando()
        expando.with params
        expando.create()
    }
    public static def sessionNote(Closure params) {
        SessionNoteExpando expando = new SessionNoteExpando()
        expando.with params
        expando.create()
    }

    public static def exploratoryExecutionEvent(Closure params) {
        ExploratoryExecutionEventExpando expando = new ExploratoryExecutionEventExpando()
        expando.with params
        expando.create()
    }

    public static def cufValue(Closure params) {
        CustomFieldValueExpando expando = new CustomFieldValueExpando()
        expando.with params
        expando.create()
    }

    public static def denormCufValue(Closure params) {
        DenormalizedFieldValueExpando expando = new CustomFieldExpandos.DenormalizedFieldValueExpando()
        expando.with params
        expando.create()
    }

    public static def cuf(Closure params) {
        CustomFieldExpando expando = new CustomFieldExpando()
        expando.with params
        expando.create()
    }

    public static def cufBinding(Closure params) {
        CustomFieldBindingExpando expando = new CustomFieldBindingExpando()
        expando.with params
        expando.create()
    }

    public static def environmentVariable(Closure params) {
        EnvironmentVariableExpandos.EnvironmentVariableExpando expando = new EnvironmentVariableExpandos.EnvironmentVariableExpando()
        expando.with params
        expando.create()
    }

    public static def environmentVariableBinding(Closure params) {
        EnvironmentVariableExpandos.EnvironmentVariableBindingExpando expando = new EnvironmentVariableExpandos.EnvironmentVariableBindingExpando()
        expando.with params
        expando.create()
    }

    public static def denormalizedEnvironmentVariable(Closure params) {
        EnvironmentVariableExpandos.DenormalizedEnvironmentVariableExpando expando = new EnvironmentVariableExpandos.DenormalizedEnvironmentVariableExpando()
        expando.with params
        expando.create()
    }

    public static def denoValue(Closure params) {
        DenoCustomFieldValueExpando expando = new DenoCustomFieldValueExpando()
        expando.with params
        expando.create()
    }

    public static def issue(Closure params) {
        IssueExpando expando = new IssueExpando()
        expando.with params
        expando.create()
    }

    public static def testAutomationServer(Closure params) {
        TestAutomationServerExpando expando = new TestAutomationServerExpando()
        expando.with params
        expando.create()
    }

    public static def testAutomatioProject(Closure params) {
        TestAutomationProjectExpando expando = new TestAutomationProjectExpando()
        expando.with params
        expando.create()
    }

    public static def automatedSuite(Closure params) {
        AutomatedSuiteExpando expando = new AutomatedSuiteExpando()
        expando.with params
        expando.create()
    }

    public static def automatedTest(Closure params) {
        AutomatedTestExpando expando = new AutomatedTestExpando()
        expando.with params
        expando.create()
    }

    public static def automatedTestTechnology(Closure params) {
        AutomatedTestTechnologyExpando expando = new AutomatedTestTechnologyExpando()
        expando.with params
        expando.create()
    }

    public static def automatedExecutionExtender(Closure params) {
        AutomatedExecutionExtenderExpando expando = new AutomatedExecutionExtenderExpando()
        expando.with params
        expando.create()
    }

    public static def automationRequest(Closure params) {
        AutomationRequestExpando expando = new AutomationRequestExpando()
        expando.with params
        expando.create()
    }

    public static def attachment(Closure params) {
        AttachmentExpando expando = new AttachmentExpando()
        expando.with params
        expando.create()
    }

    public static def scmRepository(Closure params) {
        ScmRepositoryExpando expando = new ScmRepositoryExpando()
        expando.with params
        expando.create()
    }

    public static def scmServer(Closure params) {
        ScmServerExpando expando = new ScmServerExpando()
        expando.with params
        expando.create()
    }

    public static def actionWord(Closure params) {
        ActionWordExpando expando = new ActionWordExpando()
        expando.with params
        expando.create()
    }

    static def actionWordText(Closure parameters) {
        ActionWordTextExpando expando = new ActionWordTextExpando()
        expando.with parameters
        expando.create()
    }

    static def actionWordParameter(Closure parameters) {
        ActionWordParameterExpando expando = new ActionWordParameterExpando()
        expando.with parameters
        expando.create()
    }

    static def actionWordParameterValue(Closure parameters) {
        ActionWordParameterValueExpando expando = new ActionWordParameterValueExpando()
        expando.with parameters
        expando.create()
    }

    static def apiToken(Closure parameters) {
        ApiTokenExpando expando = new ApiTokenExpando()
        expando.with parameters
        expando.create()
    }
}
