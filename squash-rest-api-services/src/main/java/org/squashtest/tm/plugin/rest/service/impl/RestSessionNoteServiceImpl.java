/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.plugin.rest.repository.RestSessionNoteRepository;
import org.squashtest.tm.plugin.rest.service.RestSessionNoteService;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import javax.inject.Inject;
import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.*;

@Service
@Transactional
public class RestSessionNoteServiceImpl implements RestSessionNoteService {

    @Inject
    private RestSessionNoteRepository sessionNoteRepository;

    @Inject
    private DSLContext dslContext;

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.execution.SessionNote' , 'READ')")
    @Transactional(readOnly=true)
    public SessionNote getOne(long id) {
        return sessionNoteRepository.getReferenceById(id);
    }

    @Override
    public List<SessionNote> getSessionNotesFromIssue(String remoteIssueId, Long executionId) {

        return dslContext.selectDistinct(SESSION_NOTE.NOTE_ID.as(RequestAliasesConstants.ID))
            .from(SESSION_NOTE)
            .join(Tables.ISSUE_LIST).on(Tables.ISSUE_LIST.ISSUE_LIST_ID.eq(SESSION_NOTE.ISSUE_LIST_ID))
            .join(ISSUE).on(Tables.ISSUE.ISSUE_LIST_ID.eq(Tables.ISSUE_LIST.ISSUE_LIST_ID))
            .join(EXPLORATORY_EXECUTION).on(EXPLORATORY_EXECUTION.EXECUTION_ID.eq(SESSION_NOTE.EXECUTION_ID))
            .join(EXECUTION_ISSUES_CLOSURE)
            .on(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXPLORATORY_EXECUTION.EXECUTION_ID))
            .where(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(executionId))
                    .and(ISSUE.REMOTE_ISSUE_ID.eq(remoteIssueId))
            .fetchInto(SessionNote.class);
    }

    @Override
    public ExploratoryExecution getExecutionFromSessionNote(Long sessionNoteId) {
        return dslContext.select(EXPLORATORY_EXECUTION.EXECUTION_ID.as(RequestAliasesConstants.ID))
            .from(EXPLORATORY_EXECUTION)
            .join(SESSION_NOTE).on(SESSION_NOTE.EXECUTION_ID.eq(EXPLORATORY_EXECUTION.EXECUTION_ID))
            .where(SESSION_NOTE.NOTE_ID.eq(sessionNoteId)).fetchOneInto(ExploratoryExecution.class);
    }

}
