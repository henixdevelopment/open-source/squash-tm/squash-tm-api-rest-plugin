/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestAutomatedTestService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestAutomatedTestController)
class RestAutomatedTestControllerIT extends BaseControllerSpec {

    @Inject
    RestAutomatedTestService automatedTestService;

    def "get-automated-test"() {

        given:
        def taServer = SquashEntityBuilder.automatedTest {
            id = 123L
            name = "auto test/class A"
            project = SquashEntityBuilder.testAutomatioProject {
                id = 569L
                jobName = "job 1"
                label = "wan wan"
                server = SquashEntityBuilder.testAutomationServer {
                    id = 789L
                    name = "TA server"
                    url = "http://1234:4567/jenkins/"
                    login = "ajenkins"
                    password = "password"
                    kind = TestAutomationServerKind.jenkins
                    description = "<p>nice try</p>"
                }
                tmProject = SquashEntityBuilder.project {
                    id = 367L
                    description = "<p>This project is the main sample project</p>"
                    label = "Main Sample Project"
                }
                slaves = "no slavery"
            }
        }
        and:
        automatedTestService.findById(123) >> taServer
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/automated-tests/{id}", 123).header("Accept", "application/json"))
        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "automated-test"
            "id".is 123
            "name".is "auto test/class A"
            "test_automation_project._type".is "test-automation-project"
            "test_automation_project.id".is "569"
            "test_automation_project.remote_name".is "job 1"
            "test_automation_project.local_name".is "wan wan"

            selfRelIs "http://localhost:8080/api/rest/latest/automated-tests/123"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {

                    pathParams {
                        add "id : the id of the automated test"
                    }

                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (number) : the id of the automated test"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the automated test"
                        addAndStop "test_automation_project (object) : the TA project associated to this automated test"

                        add DocumentationSnippets.DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this automated test"
                    }
                }
        ))


    }
}
