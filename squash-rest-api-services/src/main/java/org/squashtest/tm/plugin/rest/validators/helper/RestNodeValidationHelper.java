/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper;

import org.springframework.validation.Errors;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.library.LibraryNode;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.ParentEntity;
import org.squashtest.tm.plugin.rest.jackson.model.RestNode;
import org.squashtest.tm.plugin.rest.jackson.model.RestType;
import org.squashtest.tm.service.customfield.CustomFieldBindingFinderService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by jthebault on 13/06/2017.
 */
public class RestNodeValidationHelper {

    @PersistenceContext
    protected EntityManager entityManager;

    @Inject
    private CustomFieldBindingFinderService bindingFinderService;

    public void assignProject(RestNode restNode) {
        RestType restType = restNode.getParent().getRestType();
        Long id = restNode.getParent().getId();

        switch (restType) {
            case PROJECT:
                Project project = entityManager.find(Project.class, id);
                restNode.notifyAssociatedWithProject(project);
                break;
            case REQUIREMENT_FOLDER:
                RequirementFolder requirementFolder = entityManager.find(RequirementFolder.class, id);
                restNode.notifyAssociatedWithProject(requirementFolder.getProject());
                break;
            case REQUIREMENT:
                Requirement requirement = entityManager.find(Requirement.class, id);
                restNode.notifyAssociatedWithProject(requirement.getProject());
                break;
            case TEST_CASE_FOLDER:
                TestCaseFolder testCaseFolder = entityManager.find(TestCaseFolder.class, id);
                restNode.notifyAssociatedWithProject(testCaseFolder.getProject());
                break;
            case CAMPAIGN:
                Campaign campaign = entityManager.find(Campaign.class, id);
                restNode.notifyAssociatedWithProject(campaign.getProject());
                break;
            case CAMPAIGN_FOLDER:
                CampaignFolder campaignFolder = entityManager.find(CampaignFolder.class, id);
                restNode.notifyAssociatedWithProject(campaignFolder.getProject());
                break;
            case ITERATION:
                Iteration iteration = entityManager.find(Iteration.class, id);
                restNode.notifyAssociatedWithProject(iteration.getProject());
                break;
            default:
                throw new IllegalArgumentException("Programmatic error you should check rest type and parent id before looking for project");
        }
    }

    public void checkParent(Errors errors, RestNode restNode, RestType newNodeRestType) {

        if (restNode.getParent() == null) {
            return;
        }

        ParentEntity parent = restNode.getParent();
        RestType type = RestType.findByTypeName(parent.get_type());
        parent.setRestType(type);

        if (!newNodeRestType.isValidParentType(type)) {
            errors.rejectValue("parent._type", "invalid parent type", "Invalid parent type. Authorized parent types : " + newNodeRestType.getValidParent());
        } else {
            Long id = parent.getId();
            checkEntityExist(errors, type, id);
        }
    }

    public void checkEntityExist(Errors errors, RestType type, Long id) {
        Class typeClass = type.getTypeClass();
        Object o = entityManager.find(typeClass, id);
        if (o == null) {
            String message = String.format("No entity known for type %s and id %d", type.getTypeId(), id);
            errors.rejectValue("id", "invalid id", message);
        }
    }

    public void checkCufs(final Errors errors, RestNode restNode, BindableEntity bindableEntity) {
        Long projectId = restNode.getProject().getId();
        List<CustomFieldValueDto> valueDtos = restNode.getCustomFields();
        if (valueDtos == null) {
            return;
        }
        for (CustomFieldValueDto valueDto : valueDtos){
            RawValue value = valueDto.getValue();
            if (value == null){
                throw new IllegalArgumentException("The value of [" + valueDto.getCode() + "] can not be null");
            }
            if (value.isEmpty()){
                errors.rejectValue("customFields", "blank value", "the value of [" + valueDto.getCode() +"] can not be blank");
            }
        }
        List<CustomField> customFields = bindingFinderService.findBoundCustomFields(projectId, bindableEntity);

        CustomFieldValueValidationHelper helper = new CustomFieldValueValidationHelper(bindableEntity, projectId, customFields, valueDtos, errors);

        helper.validate();
    }

    public void checkCufs(final Errors errors, List<CustomFieldValueDto> valueDtos, Long projectId, BindableEntity bindableEntity) {

        List<CustomField> customFields = bindingFinderService.findBoundCustomFields(projectId, bindableEntity);

        CustomFieldValueValidationHelper helper = new CustomFieldValueValidationHelper(bindableEntity, projectId, customFields, valueDtos, errors);

        helper.validate();

    }

    public void loadProject(RestNode restNode) {
        Object entity = entityManager.find(restNode.getRestType().getTypeClass(), restNode.getId());
        LibraryNode libraryNode = (LibraryNode) entity;
        Project project = libraryNode.getProject();
        restNode.notifyAssociatedWithProject(project);
    }
}
