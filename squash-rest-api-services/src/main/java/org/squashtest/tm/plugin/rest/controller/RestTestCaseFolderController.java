/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.ContentInclusion;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.FolderTreeDto;
import org.squashtest.tm.plugin.rest.jackson.model.ParentFolderTreeDto;
import org.squashtest.tm.plugin.rest.jackson.model.ProjectTreeDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseFolderDto;
import org.squashtest.tm.plugin.rest.service.RestProjectService;
import org.squashtest.tm.plugin.rest.service.RestTestCaseFolderService;
import org.squashtest.tm.plugin.rest.validators.TestCaseFolderPatchValidator;
import org.squashtest.tm.plugin.rest.validators.TestCaseFolderPostValidator;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestApiController(TestCaseFolder.class)
@UseDefaultRestApiConfiguration
@SuppressWarnings("rawtypes")
public class RestTestCaseFolderController extends BaseRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestTestCaseFolderController.class);


    @Inject
    private RestTestCaseFolderService service;

    @Inject
    private TestCaseFolderPostValidator testCaseFolderPostValidator;

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private TestCaseFolderPatchValidator testCaseFolderPatchValidator;

    @Inject
    private RestProjectService projectService;

    @GetMapping("/test-case-folders")
    @ResponseBody
    @DynamicFilterExpression("name")
    public ResponseEntity<PagedModel<EntityModel<TestCaseFolder>>> findAllReadableTestCaseFolders(Pageable pageable) {
        Page<TestCaseFolder> folders = service.findAllReadable(pageable);
        PagedModel<EntityModel<TestCaseFolder>> res = toPagedModel(folders);

        return ResponseEntity.ok(res);

    }

    @GetMapping("/test-case-folders/tree/{projectIds}")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<List<ProjectTreeDto>> findTestCaseFoldersTreeByProjects(@PathVariable("projectIds") List<Long> projectIds) {
		List<Long> alreadyAddedIds = new ArrayList<>();

        List<Long> readableProjectIds = projectService.getReadableProjectIdsOnTestCaseLibrary(projectIds);
    	List<TestCaseFolder> folders = service.findAllByProjectIds(readableProjectIds);

        List<ParentFolderTreeDto> tree = getTestCaseFolderTree(folders, alreadyAddedIds);
        List<ProjectTreeDto> projTree = buildProjectTreeFromParentFolders(tree, readableProjectIds);

        return ResponseEntity.ok(projTree);
    }


    @GetMapping("/test-case-folders/{id}")
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
    public ResponseEntity<EntityModel<TestCaseFolder>> findTestCaseFolder(@PathVariable("id") long id) {

        TestCaseFolder folder = service.getOne(id);

        EntityModel<TestCaseFolder> res = toEntityModel(folder);

        res.add(createLinkTo(folder.getProject()));
        res.add(createRelationTo("content"));
        res.add(createRelationTo("attachments"));

        return ResponseEntity.ok(res);
    }

    @GetMapping("/test-case-folders/{id}/content")
    @ResponseBody
    @DynamicFilterExpression("name,reference")
    public ResponseEntity<PagedModel<EntityModel<TestCaseLibraryNode>>> findTestCaseFolderContent(@PathVariable("id") long folderId,
                                                                                                  Pageable pageable,
                                                                                                  ContentInclusion include) {

        Page<TestCaseLibraryNode> content = null;
        switch (include) {
            case NESTED:
                content = service.findFolderAllContent(folderId, pageable);
                break;
            default:
                content = service.findFolderContent(folderId, pageable);
                break;
        }

        PagedModel<EntityModel<TestCaseLibraryNode>> res = toPagedResourcesWithRel(content, "content");

        return ResponseEntity.ok(res);
    }


    @PostMapping(value = "/test-case-folders")
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
    public ResponseEntity<EntityModel<TestCaseFolder>> createCampaignFolder(@RequestBody TestCaseFolderDto folderDto) throws BindException {

        testCaseFolderPostValidator.validatePostTestCaseFolder(folderDto);

        TestCaseFolder folder = service.addTestCaseFolder(folderDto);

        EntityModel<TestCaseFolder> res = toEntityModel(folder);

        linksHelper.addAllLinksForTestCaseFolder(res);


        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    @PatchMapping(value = "/test-case-folders/{id}")
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
    public ResponseEntity<EntityModel<TestCaseFolder>> patchTestCaseFolder(@RequestBody TestCaseFolderDto folderPatch, @PathVariable("id") long id) throws BindException {
        folderPatch.setId(id);

        testCaseFolderPatchValidator.validatePatchTestCaseFolder(folderPatch);

        TestCaseFolder folder = service.patchTestCaseFolder(folderPatch, id);

        EntityModel<TestCaseFolder> res = toEntityModel(folder);

        linksHelper.addAllLinksForTestCaseFolder(res);

        return ResponseEntity.ok(res);

    }


    @DeleteMapping(value = "/test-case-folders/{ids}")
    @ResponseBody
    public ResponseEntity<Void> deleteTestCaseFolder(@PathVariable("ids") List<Long> folderIds) {

        service.deleteFolder(folderIds);

        return ResponseEntity.noContent().build();
    }

    private List<ParentFolderTreeDto> getTestCaseFolderTree(List<TestCaseFolder> folders, List<Long> alreadyAddedIds) {

        List<ParentFolderTreeDto> listDto = new ArrayList<>();
        for (TestCaseFolder folder : folders) {

            if (!alreadyAddedIds.contains(folder.getId())) {

                ParentFolderTreeDto dto = new ParentFolderTreeDto();

                dto.setType("test-case-folder");
                dto.setProject(folder.getProject());
                dto.setId(folder.getId());
                dto.setName(folder.getName());
                dto.setUrl(createSelfLink(folder).getHref());
                dto.setChildren(Collections.emptyList());

                if (folder.hasContent()) {
                    List<TestCaseFolder> folderChildren = getAllTestCaseFolderFromContent(folder);
                    List<ParentFolderTreeDto> tcftd = getTestCaseFolderTree(folderChildren,alreadyAddedIds);
                    dto.setChildren(tcftd);
                }

                alreadyAddedIds.add(folder.getId());
                listDto.add(dto);
            }
        }

        return listDto;
    }

    private List<TestCaseFolder> getAllTestCaseFolderFromContent(TestCaseFolder folder) {
        List<TestCaseFolder> testCaseFolderList = new ArrayList<>();

        for (TestCaseLibraryNode tcln : folder.getContent()) {
            if (tcln instanceof TestCaseFolder) {
                testCaseFolderList.add((TestCaseFolder) tcln);
            }
        }

        return testCaseFolderList;
    }

    private List<ProjectTreeDto> buildProjectTreeFromParentFolders(List<ParentFolderTreeDto> parentFolders, List<Long> projectIds) {

        if (!parentFolders.isEmpty()) {
            return appendProjectTreesWithFolderList(parentFolders);
        } else if (!projectIds.isEmpty()){
            return appendProjectTreesWithoutFolderList(projectIds);
        }

        return Collections.emptyList();
    }

    private List<ProjectTreeDto> appendProjectTreesWithFolderList(List<ParentFolderTreeDto> tree) {
        List<ProjectTreeDto> projTree = new ArrayList<>();

        Map<Project, List<FolderTreeDto>> map = tree.stream().collect(Collectors.groupingBy(FolderTreeDto::getProject));

        for (Map.Entry<Project, List<FolderTreeDto>> folderTreesByProjectMap : map.entrySet()) {

            Project project =  folderTreesByProjectMap.getKey();
            List<FolderTreeDto> list =  folderTreesByProjectMap.getValue();
            Collections.sort(list);

            ProjectTreeDto dto = new ProjectTreeDto(project.getId(),project.getName(),list);
            projTree.add(dto);

        }

        Collections.sort(projTree);

        return projTree;
    }

    private List<ProjectTreeDto> appendProjectTreesWithoutFolderList(List<Long> projectIds) {
        List<ProjectTreeDto> projTree = new ArrayList<>();

        Map<Long, String> projectNameByIdMap = projectService.findNamesByProjectIds(projectIds);

        projectNameByIdMap.forEach((projectId, projectName) -> {
            ProjectTreeDto dto = new ProjectTreeDto(projectId,projectName, new ArrayList<>());
            projTree.add(dto);
        });

        Collections.sort(projTree);
        return projTree;
    }
}
