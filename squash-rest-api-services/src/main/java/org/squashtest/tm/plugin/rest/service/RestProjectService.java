/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.resource.Resource;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyClearance;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyPermission;

import java.util.List;
import java.util.Map;

public interface RestProjectService {

    Boolean isGenericProjectExist(long id);

    GenericProject getOne(long id);

    GenericProject getOneByName(String projectName);

    Page<GenericProject> findAllReadable(Pageable pageable);

    Page<GenericProject> findAllReadableProjectTemplate(Pageable pageable);

    Page<GenericProject> findAllReadableStandardProject(Pageable pageable);

    /**
     * Returns a paged list of the requirements and folders that belong to the project.
     * @param id
     * @param paging
     * @return
     */
    Page<RequirementLibraryNode<Resource>> findRequirementLibraryAllContent(long id, Pageable paging);

    /**
     * Returns a paged list of the requirements and folders that are shelved at the root of the project. Other
     * nodes deeper in the requirements hierarchy will not be returned.
     * @param id
     * @param paging
     * @return
     */
    Page<RequirementLibraryNode<Resource>> findRequirementLibraryRootContent(long id, Pageable paging);

    /**
     * Returns a paged list of the test cases and folders that belong to the project.
     *
     * @param id
     * @return
     */
    Page<TestCaseLibraryNode> findTestCaseLibraryAllContent(long id, Pageable paging);

    /**
     * Returns a paged list of the test cases and folders that are shelved at the root of the project. Other
     * nodes deeper in the test cases hierarchy will not be returned.
     *
     * @param id
     * @return
     */
    Page<TestCaseLibraryNode> findTestCaseLibraryRootContent(long id, Pageable paging);

    /**
     * Returns a paged list of campaigns and folders that belong to the project.
     *
     * @param id
     * @return
     */
    Page<CampaignLibraryNode> findCampaignLibraryAllContent(long id, Pageable paging);

    /**
     * Returns a paged list of the campaigns and folders that are shelved at the root of the project. Other
     * nodes deeper in the campaign hierarchy will not be returned.
     *
     * @param id
     * @return
     */
    Page<CampaignLibraryNode> findCampaignLibraryRootContent(long id, Pageable paging);


    GenericProject createGenericProject(GenericProjectDto genericProjectDto);

    RestPartyClearance findAllClearancesByProjectId(long projectId);
    RestPartyPermission findAllPermissionsByProjectId(long projectId);

    RestPartyPermission buildPartyPermissionDataModel(String permissionGroup, List<Long> partyIds);

    RestPartyClearance buildPartyClearanceDataModel(long profileId, List<Long> partyIds);

    void addNewPermissionToProject(long userId, long projectId, String permissionGroup);

    void addNewPermissionToProject(long userId, long projectId, long profileId);

    List<AclGroup> findAllPossiblePermission();

    Page<Requirement> findRequirementsByProject(long projectId, Pageable paging);

    Page<TestCase> findTestCasesByProject(long projectId, Pageable paging, List<String> fields);

    Page<Campaign> findCampaignsByProject(long projectId, Pageable paging);

    void deletePartyFromProject(Long partyIds, long projectId);

    Map<Long, String> findNamesByProjectIds(List<Long> projectIds);

    List<Long> getReadableProjectIdsOnRequirementLibrary(List<Long> projectIds);

    List<Long> getReadableProjectIdsOnTestCaseLibrary(List<Long> projectIds);

    List<Long> getReadableProjectIdsOnCampaignLibrary(List<Long> projectIds);

    Page<CustomReportLibraryNode> findCustomReportLibraryAllContent(long projectId, Pageable paging);

    Page<CustomReportLibraryNode> findCustomReportLibraryRootContent(long projectId, Pageable paging);

    <T> void replaceLinksByCustomSelfLinks(EntityModel<T> entityModel, String entityType);

    String toKebabCase(String input);
}

