/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.plugin.rest.jackson.model.ExecutionStepAndCustomFields;
import org.squashtest.tm.plugin.rest.repository.RestExecutionStepRepository;
import org.squashtest.tm.plugin.rest.service.RestExecutionStepService;
import org.squashtest.tm.plugin.rest.service.RestInternalCustomFieldValueUpdaterService;
import org.squashtest.tm.service.execution.ExecutionProcessingService;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by jthebault on 21/06/2017.
 */
@Service
@Transactional
public class RestExecutionStepServiceImpl implements RestExecutionStepService{

    @Inject
    private RestExecutionStepRepository dao;

    @Inject
    private ExecutionProcessingService executionProcessingService;

    @Inject
    private RestInternalCustomFieldValueUpdaterService internalCufService;

    @Inject
    private DSLContext dslContext;



    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.execution.ExecutionStep' , 'READ')")
    //TODO tester les droits sur le retour en manuel pas d'inherited...
    //TODO sisi ça marche, il y a un @AclConstrainedObject, du coup AnnotatedPropertyObjectIdentityRetrievalStrategy peut retrouver quelle entité porte les ACLs
    public ExecutionStep getOne(long id) {
        return dao.getReferenceById(id);
    }

    @Override
    public void modifyExecutionStatus(Long executionStepId, ExecutionStatus status){
        executionProcessingService.changeExecutionStepStatus(executionStepId, status);
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#step.wrapped , 'EXECUTE')")
    public void updateStep(ExecutionStepAndCustomFields step) {
        // NOOP is to be done on the execution itself,
        // as we just entered a writable transaction.
        // The custom fields still need to be merged though.
        internalCufService.mergeCustomFields(step);
    }

    @Override
    public List<ExecutionStep> getExecutionStepFromIssue(String remoteIssueId, Long executionId) {
        return dslContext.select(Tables.EXECUTION_STEP.EXECUTION_STEP_ID.as(RequestAliasesConstants.ID), Tables.EXECUTION_STEP.ACTION).from(Tables.EXECUTION_STEP)
            .join(Tables.ISSUE_LIST).on(Tables.ISSUE_LIST.ISSUE_LIST_ID.eq(Tables.EXECUTION_STEP.ISSUE_LIST_ID))
            .join(Tables.ISSUE).on(Tables.ISSUE.ISSUE_LIST_ID.eq(Tables.ISSUE_LIST.ISSUE_LIST_ID))
            .join(Tables.EXECUTION_EXECUTION_STEPS).on(Tables.EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID.eq(Tables.EXECUTION_STEP.EXECUTION_STEP_ID))
            .where(Tables.ISSUE.REMOTE_ISSUE_ID.eq(remoteIssueId))
            .and(Tables.EXECUTION_EXECUTION_STEPS.EXECUTION_ID.eq(executionId))
            .fetchInto(ExecutionStep.class);
    }

    @Override
    public List<Execution> getExecutionFromExecutionStep(Long executionStepId) {
        return dslContext.select(Tables.EXECUTION.EXECUTION_ID.as(RequestAliasesConstants.ID)).from(Tables.EXECUTION)
            .join(Tables.EXECUTION_EXECUTION_STEPS).on(Tables.EXECUTION.EXECUTION_ID.eq(Tables.EXECUTION_EXECUTION_STEPS.EXECUTION_ID))
            .join(Tables.EXECUTION_STEP).on(Tables.EXECUTION_EXECUTION_STEPS.EXECUTION_STEP_ID.eq(Tables.EXECUTION_STEP.EXECUTION_STEP_ID))
            .where(Tables.EXECUTION_STEP.EXECUTION_STEP_ID.eq(executionStepId))
            .fetchInto(Execution.class);
    }

}
