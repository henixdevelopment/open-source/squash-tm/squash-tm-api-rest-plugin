/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model


import org.squashtest.tm.plugin.rest.jackson.model.ScriptedTestCaseDto
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto
import spock.lang.Specification

class TestCaseDtoTest extends Specification{
    def "when creating a test case, should create script by default when type is Scripted (no script in dto)"(){
        given: "preconditions"
        def testCaseDto = new ScriptedTestCaseDto()

        when: "action"
        def testCase = TestCaseDto.convertDto(testCaseDto)

        then: "assertions"
        testCase.getScript() == "# language: fr\n" + "Fonctionnalité: null";
    }

    def "when creating a test case, should add correct script when type is Scripted (with script in dto)"(){
        given: "preconditions"
        def testCaseDto = new ScriptedTestCaseDto()
        testCaseDto.setScript("# language: fr\n" + "Fonctionnalité: test manuel GHERKIN")

        when: "action"
        def testCase = TestCaseDto.convertDto(testCaseDto)

        then: "assertions"
        testCase.getScript() == "# language: fr\n" + "Fonctionnalité: test manuel GHERKIN";
    }

}
