/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.users.ApiToken
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestApiTokenService
import org.squashtest.tm.service.internal.dto.ApiTokenDto

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.apiToken
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.user

import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

import javax.inject.Inject;

@WebMvcTest(RestApiTokenController)
class RestApiTokenControllerIT extends BaseControllerSpec {

    @Inject
    private RestApiTokenService restApiTokenService

    def "get-tokens-by-user"() {
        given:
        def userMock = user {
            id = 4L
        }

        def token1 = apiToken {
            id = 1L
            uuid = "f8e2f2d8-0fbf-4d29-8003-b729bfe212c8"
            user = userMock
            name = "token 1"
            permissions = "READ"
            createdOn "2024/06/15 09:48:02"
            createdBy = "user1"
            expiryDate = "2025-02-03"
            lastUsage "2024/06/28 11:54:27"
        }

        def token2 = apiToken {
            id = 2L
            uuid = "9e02ea67-f7ae-4c22-80c5-cb6bd64d1786"
            user = userMock
            name = "token 2"
            permissions = "READ-WRITE"
            createdOn "2024/05/02 15:18:14"
            createdBy = "user2"
            expiryDate = "2024-12-28"
            lastUsage "2024/06/10 18:00:07"
        }

        and:
        restApiTokenService.getAllTokensByUser(_) >> { args ->
            new PageImpl<ApiToken>([token1, token2], args[0], 2)
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/tokens").header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.api-tokens".hasSize 2
            "_embedded.api-tokens".test {
                "[0]".test {
                    "name".is "token 1"
                    "id".is 1
                }
                "[1]".test {
                    "name".is "token 2"
                    "id".is 2
                }
            }
            "page".test {
                "size".is 20
                "totalElements".is 2
                "totalPages".is 1
                "number".is 0
            }
            selfRelIs "http://localhost:8080/api/rest/latest/tokens?page=0&size=20"
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
            documentationBuilder {
                requestParams {
                    add DocumentationSnippets.DescriptorLists.paginationAndSortParams
                    add DocumentationSnippets.DescriptorLists.fieldsParams
                }
                fields {
                    embeddedAndStop "api-tokens (array) : the personal API tokens of the user"
                    add DocumentationSnippets.DescriptorLists.paginationFields
                    add DocumentationSnippets.DescriptorLists.linksFields
                }
                _links {
                    add DocumentationSnippets.DescriptorLists.paginationLinks
                }
            }
        ))
    }

    def "delete-token"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/tokens/{id}", "3")
            .accept("application/json")
            .contentType("application/json"))

        then:
        res.andExpect(status().isNoContent())
        restApiTokenService.deletePersonalApiToken(3)

        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the token"
                }
            }
        ))
    }

    def "post-api-token"() {
        given:
        def json =
            """{
    "name" : "token read write",
    "permissions" : "READ_WRITE",
    "expiry_date": "2025-05-06"
}"""
        def userMock = user {
            id = 4L
        }

        def token = apiToken {
            id = 6L
            uuid = "9e02ea67-f7ae-4c22-80c5-cb6bd64d1786"
            user = userMock
            name = "token read write"
            permissions = "READ-WRITE"
            createdOn "2024/07/02 15:18:14"
            createdBy = "user1"
            expiryDate = "2025-05-06"
            lastUsage = null
        }

        and:
        restApiTokenService.addToken(_) >> new ApiTokenDto(
            token as ApiToken,
            "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwidXVpZCI6IjBmZmUxODdmLTAxYzctNGViZC05NjY5LWI0ODFlNzEyMGE1ZiIsInBlcm1pc3Npb25zIjoiUkVBRCIsImlhdCI6MTcyMTMwODc4OSwiZXhwIjoxNzUyNTM3NjAwfQ.qWlfn0D4R5-5PXwTpkArmjY2NOjpcE50dzwlaXkNbRu0K8CRS7YO-xsjPegup73nKxYQBQvbYJ_EPnTURRFQng");

        when:
        def res = mockMvc.perform(
            post("/api/rest/latest/tokens")
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res
            .andExpect(status().isCreated())
            .andExpect(content().contentType("application/json"))
        withResult(res) {
            "id".is 6
            "name".is "token read write"
            "permissions".is "READ_WRITE"
            "user".test {
                "id".is 4
            }
            "created_on".is "2024-07-02T13:18:14.000+00:00"
            "created_by".is "user1"
            "expiry_date".is "2025-05-06T00:00:00.000+00:00"
            "last_usage".is null
            "uuid".is "9e02ea67-f7ae-4c22-80c5-cb6bd64d1786"
            "generated_token".is "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwidXVpZCI6IjBmZmUxODdmLTAxYzctNGViZC05NjY5LWI0ODFlNzEyMGE1ZiIsInBlcm1pc3Npb25zIjoiUkVBRCIsImlhdCI6MTcyMTMwODc4OSwiZXhwIjoxNzUyNTM3NjAwfQ.qWlfn0D4R5-5PXwTpkArmjY2NOjpcE50dzwlaXkNbRu0K8CRS7YO-xsjPegup73nKxYQBQvbYJ_EPnTURRFQng"
        }

        res.andDo(
            doc.document(
                documentationBuilder {
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    requestFields {
                        add "name (string): the name of the personal API token (maximum 255 characters)"
                        add "permissions (string): the permissions for the token ('READ' or 'READ_WRITE')"
                        add "expiry_date (string): the expiry date for the token in the format 'YYYY-MM-DD' (it cannot exceed one year)"
                    }

                    fields {
                        add "id (number): the ID of the personal API token"
                        addAndStop "user (object): the user who owns the API token"
                        add "uuid (string): the UUID of the API token"
                        add "name (string): the name of the API token"
                        add "created_on (string): the date when the API token was created"
                        add "created_by (string): the user who has created the token"
                        add "permissions (string): the permissions associated with the API token"
                        add "expiry_date (string): the expiry date of the API token"
                        add "last_usage (string?): the date when the token was last used"
                        add "generated_token (string): the generated token that the user should keep secret"
                    }
                }))
    }
}
