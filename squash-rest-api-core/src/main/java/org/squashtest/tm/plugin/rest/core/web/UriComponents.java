/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

/* 
 * TODO : I suspect that interface has no usage at all, if that is the case indeed feel free to ditch it
 * 
 * @author bsiri
 *
 */
public interface UriComponents {

	/**
	 * Name of the query parameter for page number, for pagined services
	 * 
	 */
	public static final String PAGE = "page";
	
	/**
	 * Name of the query parameter for the page size, for paginated services
	 */
	public static final String SIZE = "size";
	
	/**
	 * Name of the sort parameter, for paginated services
	 */
	public static final String SORT = "sort";
	
	/**
	 * Name of the content level parameter, for queries on the content of a container 
	 * 
	 */
	public static final String CONTENT_INCLUDE = "include";
	
	/**
	 * Value for CONTENT_INCLUDE : only the direct children of a container 
	 * 
	 */
	public static final String CONTENT_INCLUDE_ROOT = "root";
	
	/**
	 * Value for CONTENT_INCLUDE : include everything in the subtree
	 */
	public static final String CONTENT_INCLUDE_NESTED = "nested";

	/**
	 * Name of the test case type filter parameter, for queries on the test cases
	 *
	 */
	public static final String TEST_CASE_TYPE_FILTER = "type";

	/**
	 * Value for TEST_CASE_TYPE_FILTER : all types of test case
	 *
	 */
	public static final String TEST_CASE_TYPE_FILTER_ALL = "all";

	/**
	 * Value for TEST_CASE_TYPE_FILTER : standard type of test case
	 *
	 */
	public static final String TEST_CASE_TYPE_FILTER_STANDARD = "standard";

	/**
	 * Value for TEST_CASE_TYPE_FILTER : scripted type of test case
	 *
	 */
	public static final String TEST_CASE_TYPE_FILTER_SCRIPTED = "scripted";

	/**
	 * Value for TEST_CASE_TYPE_FILTER : keyword type of test case
	 *
	 */
	public static final String TEST_CASE_TYPE_FILTER_KEYWORD = "keyword";


	public static final String PROJECT_TYPE_FILTER = "type";

	public static final String PROJECT_TYPE_FILTER_ALL = "all";

	public static final String PROJECT_TYPE_FILTER_STANDARD = "standard";

	public static final String PROJECT_TYPE_FILTER_TEMPLATE = "template";
}
