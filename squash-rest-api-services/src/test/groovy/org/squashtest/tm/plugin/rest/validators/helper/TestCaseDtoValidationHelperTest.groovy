/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper

import org.springframework.validation.Errors
import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.Parameter
import org.squashtest.tm.plugin.rest.jackson.model.KeywordTestCaseDto
import org.squashtest.tm.plugin.rest.jackson.model.ScriptedTestCaseDto
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto
import spock.lang.Specification

class TestCaseDtoValidationHelperTest extends Specification{
    TestCaseDtoValidationHelper helper = new TestCaseDtoValidationHelper()

    Errors errors = Mock(Errors)

    def "should check invalid attribute for standard test case"(){
        given:
        TestCaseDto testCaseDto = new TestCaseDto()

        when:
        helper.checkInvalidAttributeForEachTestCaseType(errors, testCaseDto)

        then:
        0 * errors.rejectValue(*_)
    }

    def "should check invalid attribute for keyword test case"(){
        given:
        TestCaseDto testCaseDto = new KeywordTestCaseDto()

        when:
        helper.checkInvalidAttributeForEachTestCaseType(errors, testCaseDto)

        then:
        0 * errors.rejectValue(*_)
    }

    def "should check invalid attribute for scripted test case"(){
        given:
        TestCaseDto testCaseDto = new ScriptedTestCaseDto()
        testCaseDto.setSteps(_)
        Set<Parameter> parameters = new HashSet<>()
        testCaseDto.setParameters(parameters)
        Set<Dataset> datasets = new HashSet<>()
        testCaseDto.setDatasets(datasets)

        when:
        helper.checkInvalidAttributeForEachTestCaseType(errors, testCaseDto)

        then:
        3 * errors.rejectValue(*_)
    }

}
