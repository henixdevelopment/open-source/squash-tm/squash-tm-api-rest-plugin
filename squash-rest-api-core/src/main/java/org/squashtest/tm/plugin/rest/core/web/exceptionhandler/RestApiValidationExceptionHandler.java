/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web.exceptionhandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.squashtest.tm.plugin.rest.core.web.IRestController;
import org.squashtest.tm.plugin.rest.core.web.exceptionhandler.response.FieldValidationErrors;

import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.List;

@ControllerAdvice(assignableTypes = IRestController.class)
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestApiValidationExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestApiValidationExceptionHandler.class);

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<FieldValidationErrors> handleValidationException(MethodArgumentNotValidException ex) {
        LOGGER.error(ex.getMessage(), ex);

        final List<FieldValidationErrors.ValidationError> validationErrors = ex
            .getBindingResult()
            .getFieldErrors()
            .stream()
            .map(fieldError -> FieldValidationErrors.error(fieldError.getField(), fieldError.getDefaultMessage()))
            .toList();

        return new ResponseEntity<>(FieldValidationErrors.withErrors(validationErrors), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<FieldValidationErrors> handleConstraintViolation(ConstraintViolationException ex) {
        LOGGER.error(ex.getMessage(), ex);

        final List<FieldValidationErrors.ValidationError> validationErrors = ex
            .getConstraintViolations()
            .stream()
            .map(violation -> FieldValidationErrors.error(extractFieldName(violation.getPropertyPath()), violation.getMessage()))
            .toList();

        return new ResponseEntity<>(FieldValidationErrors.withErrors(validationErrors), HttpStatus.BAD_REQUEST);
    }

    // Extracts the field name from the property path. Constraint validations are reported with the full path to the
    // field, starting from the controller method, e.g. "editBannerMessage.bannerMessage.message", but we only want the
    // field name, e.g. "message".
    private String extractFieldName(Path propertyPath) {
        StringBuilder fieldName = new StringBuilder();
        int depth = 0;

        for (Path.Node node : propertyPath) {
            if (depth < 2) {
                depth++;
                continue;
            }

            if (depth == 2) {
                fieldName.append(node.getName());
            } else {
                fieldName.append('.').append(node.getName());
            }

            depth++;
        }

        return fieldName.toString();
    }
}
