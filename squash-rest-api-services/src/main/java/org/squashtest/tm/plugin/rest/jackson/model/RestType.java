/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestStep;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by jthebault on 02/06/2017.
 */
public enum RestType {

    PROJECT("project", Project.class, new HashSet<>()),
    REQUIREMENT_FOLDER("requirement-folder", RequirementFolder.class, new HashSet<>(Arrays.asList("project", "requirement-folder"))),
    REQUIREMENT("requirement", Requirement.class, new HashSet<>(Arrays.asList("project", "requirement-folder", "requirement"))),
    REQUIREMENT_VERSION("requirement-version", RequirementVersion.class, new HashSet<>(Arrays.asList("project", "requirement-folder", "requirement", "requirement-version"))),
    TEST_CASE_FOLDER("test-case-folder", TestCaseFolder.class, new HashSet<>(Arrays.asList("project", "test-case-folder"))),
    TEST_CASE("test-case", TestCase.class, new HashSet<>(Arrays.asList("project", "test-case-folder", "dataset"))),
    TEST_STEP("test-step", TestStep.class, new HashSet<>()),
    TEST_SUITE("test-suite", TestSuite.class, new HashSet<>(Arrays.asList("project", "campaign-folder","campaign", "iteration"))),
    DATASET("dataset", Dataset.class, new HashSet<>()),
    PARAMETER("parameter", Parameter.class, new HashSet<>()),
    CAMPAIGN("campaign",Campaign.class, new HashSet<>(Arrays.asList("project", "campaign-folder"))),
    CAMPAIGN_FOLDER("campaign-folder", CampaignFolder.class, new HashSet<>(Arrays.asList("project", "campaign-folder"))),
    ITERATION_TEST_PLAN_ITEM("iteration-test-plan-item", IterationTestPlanItem.class, new HashSet<>(Arrays.asList("iteration-test-plan-item","test-case", "dataset"))),
    CAMPAIGN_TEST_PLAN_ITEM("campaign-test-plan-item", CampaignTestPlanItem.class, new HashSet<>(Arrays.asList("campaign-test-plan-item","test-case", "dataset"))),
    ITERATION("iteration",Iteration.class, new HashSet<>(Collections.singletonList("campaign"))),
    SCM_REPOSITORY("scm-repository", ScmRepository.class, new HashSet<>());


    private String typeId;
    private Class typeClass;
    private Set<String> validParent = new HashSet<>();

    RestType(String typeId, Class typeClass, Set<String> validParent) {
        this.typeId = typeId;
        this.typeClass = typeClass;
        this.validParent = validParent;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public Class getTypeClass() {
        return typeClass;
    }

    public void setTypeClass(Class typeClass) {
        this.typeClass = typeClass;
    }

    public static RestType findByTypeName(String enumName) {
        EnumSet<RestType> restTypes = EnumSet.allOf(RestType.class);
        for (RestType restType : restTypes) {
            if (restType.getTypeId().equals(enumName)) {
                return restType;
            }
        }
        return null;
    }

    public Set<String> getValidParent() {
        return validParent;
    }

    public void setValidParent(Set<String> validParent) {
        this.validParent = validParent;
    }

    public boolean isValidParentType(RestType type) {
        return this.validParent.contains(type.getTypeId());
    }
}
