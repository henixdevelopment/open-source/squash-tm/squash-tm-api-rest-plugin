/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;
import org.squashtest.tm.plugin.rest.core.exception.ProgrammingError;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 *  Re-packages the {@link DeserializationHints} shipped with the parameter when appropriate. Usually, it kicks in
 *  when {@link PersistentEntityArgumentResolver} is involved, and the {@link PersistentEntityJacksonHttpMessageConverter}
 *  will then respond in time
 *
 * Created by bsiri on 11/07/2017.
 */
public class DeserializationConfigurationRequestBodyAdvice extends RequestBodyAdviceAdapter {

    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        // don't apply if we aren't not really working with
        return methodParameter.hasParameterAnnotation(PersistentEntity.class);
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) throws IOException {

        if (parameter instanceof ContextualizedMethodParameter){
            DeserializationHints hints = ((ContextualizedMethodParameter)parameter).getHints();
            return new DeserializationConfigurationInputMessage(inputMessage, hints);

        }
        else{
            throw new ProgrammingError("somehow invoked DeserializationConfigurationRequestBodyAdvice by another mean of PersistentEntityArgumentResolver");
        }

    }


}
