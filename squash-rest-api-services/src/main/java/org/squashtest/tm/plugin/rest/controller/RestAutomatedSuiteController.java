/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.LinkBuilder;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.BasicResourceAssembler;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.AutomatedSuiteDto;
import org.squashtest.tm.plugin.rest.service.RestAutomatedExecutionExtenderService;
import org.squashtest.tm.plugin.rest.service.RestAutomatedSuiteService;

import javax.inject.Inject;
import java.util.List;


/**
 * Because AutomatedSuite is not an identified class, so everything became complicated,
 * we can only use a rest dto {@link AutomatedSuiteDto} for jackson serialization,
 * and all toResource methods were customized.
 */
@RestApiController
@UseDefaultRestApiConfiguration
public class RestAutomatedSuiteController extends BaseRestController {

    @Inject
    private RestAutomatedSuiteService automatedSuiteService;

    @Inject
    private RestAutomatedExecutionExtenderService automatedExecutionExtenderService;

    private final AutomatedSuiteResourceAssembler suiteResourceAssembler = new AutomatedSuiteResourceAssembler();


    @GetMapping(value = "/automated-suites")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<PagedModel<EntityModel<AutomatedSuiteDto>>> findAllReadableAutomatedSuites(Pageable pageable) {

        Page<AutomatedSuite> suites = automatedSuiteService.findAllReadable(pageable);
        Page<AutomatedSuiteDto> suiteDtos = suites.map(suite -> new AutomatedSuiteDto(suite.getId(), suite.getExecutionExtenders()));
        PagedModel<EntityModel<AutomatedSuiteDto>> res = pageAssembler.toModel(suiteDtos, suiteResourceAssembler);

        return ResponseEntity.ok(res);
    }

    @GetMapping(value = "/automated-suites/{id}")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<EntityModel<AutomatedSuiteDto>> findAutomatedSuite(@PathVariable("id") String id) {
        AutomatedSuite suite = automatedSuiteService.findById(id);
        EntityModel<AutomatedSuiteDto> res = toEntityModel(suite);

        UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
        res.add(Link.of(builder.toUriString(), LinkRelation.of("self")));
        builder.path("/" + "executions");
        res.add(Link.of(builder.toUriString(), LinkRelation.of("executions")));

        return ResponseEntity.ok(res);
    }

    @GetMapping(value = "/automated-suites/{id}/executions")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<PagedModel<EntityModel<AutomatedExecutionExtender>>> findAutomatedSuiteExecutions(@PathVariable("id") String id, Pageable pageable) {

        Page<AutomatedExecutionExtender> executions = automatedExecutionExtenderService.findAutomatedExecutionExtenderByAutomatedSuiteId(id, pageable);
        PagedModel<EntityModel<AutomatedExecutionExtender>> res = toPagedModel(executions);
        return ResponseEntity.ok(res);
    }

    @PostMapping(value = "/automated-suite-utils/from-iteration-test-plan-items")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<EntityModel<AutomatedSuiteDto>> createAutomatedSuiteFromIterationTestPlanItems(@RequestParam(value = "itemIds") List<Long> itemIds) {

        AutomatedSuite suite = automatedSuiteService.createAutomatedSuiteFromIterationTestPlanItems(itemIds);
        EntityModel<AutomatedSuiteDto> res = toEntityModel(suite);

        addAllLinksForAutomatedSuite(suite.getId(), res);
        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    @PostMapping(value = "/automated-suite-utils/from-test-suite")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<EntityModel<AutomatedSuiteDto>> createAutomatedSuiteFromTestSuite(@RequestParam(value = "testSuiteId") long testSuiteId) {

        AutomatedSuite suite = automatedSuiteService.createAutomatedSuiteFromTestSuite(testSuiteId);
        EntityModel<AutomatedSuiteDto> res = toEntityModel(suite);

        addAllLinksForAutomatedSuite(suite.getId(), res);
        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    @PostMapping(value = "/automated-suite-utils/from-iteration")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<EntityModel<AutomatedSuiteDto>> createAutomatedSuiteFromIteration(@RequestParam(value = "iterationId") long iterationId) {

        AutomatedSuite suite = automatedSuiteService.createAutomatedSuiteFromIteration(iterationId);
        EntityModel<AutomatedSuiteDto> res = toEntityModel(suite);

        addAllLinksForAutomatedSuite(suite.getId(), res);
        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    @PostMapping(value = "/automated-suite-utils/{suiteId}/executor")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<EntityModel<AutomatedSuiteDto>> executeAutomatedSuite(@PathVariable("suiteId") String suiteId) {

        AutomatedSuite suite = automatedSuiteService.findById(suiteId);
        automatedSuiteService.start(suite);

        EntityModel<AutomatedSuiteDto> res = toEntityModel(suite);

        addAllLinksForAutomatedSuite(suite.getId(), res);
        return ResponseEntity.ok(res);
    }

    /**
     * customized toResource method
     */
    private EntityModel<AutomatedSuiteDto> toEntityModel(AutomatedSuite suite) {
        AutomatedSuiteDto suiteDto = new AutomatedSuiteDto(suite.getId(), suite.getExecutionExtenders());
        return EntityModel.of(suiteDto);
    }

    /**
     * customized add links method
     */
    private void addAllLinksForAutomatedSuite(String suiteId, EntityModel<AutomatedSuiteDto> res) {
        LinkBuilder builder = linkService.fromBasePath(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(RestAutomatedSuiteController.class).findAutomatedSuite(suiteId)));
        res.add(Link.of(builder.toString(), LinkRelation.of("self")));
        res.add(Link.of(builder.slash("/executions").toString(), LinkRelation.of("executions")));
    }

    /**
     * customized assembler to replace {@link BasicResourceAssembler}, for now, only used for AutomatedSuite
     */
    private class AutomatedSuiteResourceAssembler implements RepresentationModelAssembler<AutomatedSuiteDto, EntityModel<AutomatedSuiteDto>> {

        @Override
        public EntityModel<AutomatedSuiteDto> toModel(AutomatedSuiteDto entity) {
            EntityModel<AutomatedSuiteDto> res = EntityModel.of(entity);
            addAllLinksForAutomatedSuite(entity.getId(), res);
            return res;
        }
    }
}
