/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.bdd.Keyword
import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.service.RestTestCaseService
import org.squashtest.tm.plugin.rest.service.RestTestStepService
import org.squashtest.tm.plugin.rest.service.RestVerifyingRequirementManagerService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.testcase.bdd.KeywordTestStepActionWordParser
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionStep
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWord
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWordParameter
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWordParameterValue
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWordText
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.callStep
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufValue
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.dataset
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordStep
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCase

@WebMvcTest(RestTestStepController)
public class RestTestStepControllerIT extends BaseControllerSpec {

    @Inject
    private RestTestStepService restTestStepService

    @Inject
    private RestTestCaseService restTestCaseService;

    @Inject
    private CustomFieldValueFinderService cufService

    @Inject
    private PermissionEvaluationService permService

    @Inject
    private EntityManagerFactory emf;

    @Inject
    private RestVerifyingRequirementManagerService verifRequirementManagerService;

    @Inject
    private NodeHierarchyHelpService nodeHierarchyHelpService;

    def "get-action-step"() {

        given:
        def step = SquashEntityBuilder.actionStep {
            id = 235l
            action = "<p>Wave your hand</p>"
            expectedResult = "<p>The door opens</p>"
            index = 0

            testCase = SquashEntityBuilder.testCase {
                id = 120l
                name = "Door opening system"
            }
        }

        and:
        restTestStepService.getOne(_) >> step
        cufService.findAllCustomFieldValues(step) >> [
                SquashEntityBuilder.cufValue {
                    label = "note"
                    inputType "PLAIN_TEXT"
                    code = "cuf_txt_note"
                    value = "Star Trek style welcomed but not mandatory"
                },
                SquashEntityBuilder.cufValue {
                    label = "see also"
                    inputType "TAG"
                    code = "cuf_tags_see_also"
                    value = ["smart home", "sensors", "hand gesture"]
                }
        ]


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-steps/{id}", 235).header("Accept", "application/json"))

        then:

        // test

        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "action-step"
            "id".is 235
            "action".is "<p>Wave your hand</p>"
            "expected_result".is "<p>The door opens</p>"
            "index".is 0
            "custom_fields".hasSize 2
            "custom_fields[0]".test {
                "code".is "cuf_txt_note"
                "label".is "note"
                "value".is "Star Trek style welcomed but not mandatory"
            }
            "custom_fields[1]".test {
                "code".is "cuf_tags_see_also"
                "label".is "see also"
                "value".contains "smart home", "sensors", "hand gesture"
            }
            "test_case".test {
                "_type".is "test-case"
                "id".is 120
                "name".is "Door opening system"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/120"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/test-steps/235"

        }

        // document
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the step (optional)"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (number) : the id of the step"
                        add "_type (string) : the type of step"
                        add "action (string) : the action to be accomplished, format is html"
                        add "expected_result (string) : the state or behavior that should be observable when the action has been performed, format is html)"
                        addAndStop "test_case (object) : the test case this step is part of"
                        add "index(number) : the index of current step in the test case"
                        add "custom_fields (array) : the custom fields of that test step"
                        add "custom_fields[].label (string) : the label of the custom field"
                        add "custom_fields[].code (string) : the code of the custom field"
                        add "custom_fields[].value (varies) : the value of the custom field. The value is either a string (for most custom fields), or an array of strings (for multivalued custom fields eg a tag list)"
                        add "attachments (array) : the attachments of that test step"
                        addAndStop"verified_requirements (array): the list of verified requirements. Please refer to the requirements documentation."
                        addAndStop "_links (object) : related links"
                    }

                    _links {
                        add "self : link to this step"
                    }
                }
        ))


    }

    def "get-call-step"() {

        given:
        def step = SquashEntityBuilder.callStep {
            id = 441L
            delegateParameterValues = false
            calledDataset = dataset {
                id = 33L
                name = "topping"
            }
            testCase = testCase {
                id = 297L
                name = "Order a meal"
            }
            calledTestCase = testCase {
                id = 276L
                name = "Order a coffee"
            }
        }
        restTestStepService.getOne(_) >> step
        and:
        permService.canRead(_) >> true

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-steps/{id}", 441).header("Accept", "application/json"))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "call-step"
            "id".is 441
            "delegate_parameter_values".is false
            "index".is 0
            "test_case".test {
                "_type".is "test-case"
                "id".is 297
                "name".is "Order a meal"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/297"
            }
            "called_test_case".test {
                "_type".is "test-case"
                "id".is 276
                "name".is "Order a coffee"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/276"

            }
            "called_dataset".test {
                "_type".is "dataset"
                "id".is 33
                "name".is "topping"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/33"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/test-steps/441"
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test step"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add "id (number) : the id of the step"
                        add "_type (string) : the type of step"
                        addAndStop "test_case (object) : the test case this step belongs to"
                        add "index(number) : the index of the step in the test case"
                        addAndStop "called_test_case (object) : the test case called by this step"
                        addAndStop "called_dataset (object) : the dataset to use for the called test case, if any (may be null if not)"
                        add "delegate_parameter_values (boolean) : whether the parameters of the called test case should be " +
                                "set by the caller rather than by a dataset of the called. A value of 'true' usually mean that " +
                                "the 'called_dataset' is null."
                        addAndStop "_links (object) : related links"
                    }
                    _links {
                        add "self : link to this step"
                    }
                }
        ))

    }

    def "post-action-test-step"() {

        given:
        def json = """{               
                "_type": "action-step",
                "action": "<p>simple test step</p>",
                "expected_result": "<p>action step</p>",
                "custom_fields": [
                    {
                        "code": "cuf_txt_note",
                        "value": "Star Trek style welcomed but not mandatory"
                    },
                    {
                        "code": "cuf_tags_see_also",
                        "value": ["smart home", "sensors", "hand gesture"]
                    }
                ]
            }
            """

        def proj = project {
            id = 15L
            name = "Project alpha"
        }
        def tc = testCase {
            id = 240L
            name = "target test case"
            project = proj
        }

        def step = actionStep {
            id = 210L
            action = "<p>simple test step</p>"
            expectedResult = "<p>action step</p>"
            testCase = tc
        }

        and:
        cufService.findAllCustomFieldValues(step) >> [
                cufValue {
                    label = "note"
                    inputType "PLAIN_TEXT"
                    code = "cuf_txt_note"
                    value = "Star Trek style welcomed but not mandatory"
                },
                cufValue {
                    label = "see also"
                    inputType "TAG"
                    code = "cuf_tags_see_also"
                    value = ["smart home", "sensors", "hand gesture"]
                }
        ]
        restTestCaseService.getOne(240L) >> tc
        restTestStepService.createTestStep(_, _) >> step

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases/{testCaseId}/steps", 240)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "action-step"
            "id".is 210
            "action".is "<p>simple test step</p>"
            "expected_result".is "<p>action step</p>"
            "custom_fields".hasSize 2
            "custom_fields[0]".test {
                "code".is "cuf_txt_note"
                "label".is "note"
                "value".is "Star Trek style welcomed but not mandatory"
            }
            "custom_fields[1]".test {
                "code".is "cuf_tags_see_also"
                "label".is "see also"
                "value".contains "smart home", "sensors", "hand gesture"
            }
            "test_case".test {
                "_type".is "test-case"
                "id".is 240
                "name".is "target test case"
            }

        }

        // document
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "testCaseId : the id of the test case of which to add new test step"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    requestFields {
                        add "_type (string) : the type of step"
                        add "action (string) : the action to be accomplished, format is html"
                        add "expected_result (string) : the state or behavior that should be observable when the action has been performed, format is html)"
                        add "custom_fields (array) : the custom fields of that test step"
                        add "custom_fields[].code (string) : the code of the custom field"
                        add "custom_fields[].value (varies) : the value of the custom field. The value is either a string (for most custom fields), or an array of strings (for multivalued custom fields eg a tag list)"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the step"
                        add "test_case (object) : the test case this step is part of"
                        add "index(number) : the index of current step in the test case"
                        add "custom_fields[].label (string) : the label of the custom field"
                        add "_links (object) : related links"
                    }

                    _links {
                        add "self : link to this step"
                        add "test-case : link to the test case where this step belongs"
                    }
                }
        ))

    }

    def "post-call-test-step"() {
        given:
        def json = """{        
       
                "_type": "call-step",
                "delegate_parameter_values": false,
                "called_test_case": {
                        "_type": "test-case",
                        "id": 276,
                        "name": "test case been called"
                },
                 "called_dataset": {
                        "_type": "dataset",
                        "id": 33,
                        "name": "topping"
                }
        }"""

        def proj = project {
            id = 15L
            name = "Project alpha"
        }
        def tc = testCase {
            id = 297L
            name = "target test case"
            project = proj
        }

        def calledTc = testCase {
            id = 276L
            name = "test case been called"
            project = proj
        }

        def calledDs = dataset {
            id = 33L
            name = "topping"
        }

        def ts = callStep {
            id = 441L
            delegateParameterValues = false
            calledDataset = calledDs
            testCase = tc
            calledTestCase = calledTc
        }

        and:
        EntityManager em = Mock()
        emf.createEntityManager() >> em
        em.find(TestCase.class, 276L) >> calledTc
        em.find(Dataset.class, 33L) >> calledDs

        restTestCaseService.getOne(297L) >> tc
        restTestStepService.createTestStep(_, _) >> ts

        permService.canRead(_) >> true

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases/{testCaseId}/steps", 297L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "call-step"
            "id".is 441
            "called_test_case".test {
                "_type".is "test-case"
                "id".is 276
                "name".is "test case been called"
            }
            "test_case".test {
                "_type".is "test-case"
                "id".is 297
                "name".is "target test case"
            }
            "called_dataset".test {
                "_type".is "dataset"
                "id".is 33
                "name".is "topping"
            }
        }

        // document
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "testCaseId : the id of the test case of which to add new test step"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    requestFields {
                        add "_type (string) : the type of step"
                        add "delegate_parameter_values (boolean) : whether the parameters of the called test case should be " +
                                "set by the caller rather than by a dataset of the called. A value of 'true' usually mean that " +
                                "the 'called_dataset' is null."
                        addAndStop "called_test_case (object) : the test case called by this step"
                        addAndStop "called_dataset (object) : the dataset to use for the called test case, if any (may be null if not)"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the step"
                        add "test_case (object) : the test case this step belongs to"
                        add "index(number) : the index of the step in the test case"
                        add "_links (object) : related links"
                    }

                    _links {
                        add "self : link to this step"
                        add "test-case : link to the test case where this step belongs"
                    }
                }
        ))

    }

    def "patch-action-test-step"() {

        given:
        def json = """{
                "_type": "action-step",
                "action": "<p>hadouken</p>",
                "expected_result": "<p>ko</p>",
                "custom_fields": [
                    {
                        "code": "cuf_txt_note",
                        "value": "Star Trek style welcomed but not mandatory"
                    },
                    {
                        "code": "cuf_tags_see_also",
                        "value": ["smart home", "sensors", "hand gesture"]
                    }
                ]
        }
        """

        def tc = testCase {
            id = 240L
            name = "target test case"
            project = project {
                id = 15L
                name = "Project alpha"
            }
        }

        def stepPatched = actionStep {
            id = 210L
            action = "<p>hadouken</p>"
            expectedResult = "<p>ko</p>"
            testCase = tc
        }

        def step = actionStep {
            id = 210L
            action = "<p>simple test step</p>"
            expectedResult = "<p>action step</p>"
            testCase = tc
        }

        and:
        restTestStepService.getOne(210L) >> step
        restTestStepService.patchTestStep(_, _) >> stepPatched

        cufService.findAllCustomFieldValues(stepPatched) >> [
                cufValue {
                    label = "note"
                    inputType "PLAIN_TEXT"
                    code = "cuf_txt_note"
                    value = "Star Trek style welcomed but not mandatory"
                },
                cufValue {
                    label = "see also"
                    inputType "TAG"
                    code = "cuf_tags_see_also"
                    value = ["smart home", "sensors", "hand gesture"]
                }
        ]

        cufService.findAllCustomFieldValues(step) >> [
                cufValue {
                    label = "note"
                    inputType "PLAIN_TEXT"
                    code = "cuf_txt_note"
                    value = "old value"
                },
                cufValue {
                    label = "see also"
                    inputType "TAG"
                    code = "cuf_tags_see_also"
                    value = ["home", "sensors", "gesture"]
                }
        ]

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-steps/{id}", 210)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "action-step"
            "id".is 210
            "action".is "<p>hadouken</p>"
            "expected_result".is "<p>ko</p>"
            "custom_fields".hasSize 2
            "custom_fields[0]".test {
                "code".is "cuf_txt_note"
                "label".is "note"
                "value".is "Star Trek style welcomed but not mandatory"
            }
            "custom_fields[1]".test {
                "code".is "cuf_tags_see_also"
                "label".is "see also"
                "value".contains "smart home", "sensors", "hand gesture"
            }
        }

        // document
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the step"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    requestFields {
                        add "_type (string) : the type of step"
                        add "action (string) : the action to be accomplished, format is html"
                        add "expected_result (string) : the state or behavior that should be observable when the action has been performed, format is html)"
                        add "custom_fields (array) : the custom fields of that test step"
                        add "custom_fields[].code (string) : the code of the custom field"
                        add "custom_fields[].value (varies) : the value of the custom field. The value is either a string (for most custom fields), or an array of strings (for multivalued custom fields eg a tag list)"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the step"
                        add "test_case (object) : the test case this step is part of"
                        add "index(number) : the index of current step in the test case"
                        add "custom_fields[].label (string) : the label of the custom field"
                        add "_links (object) : related links"
                    }

                    _links {
                        add "self : link to this step"
                        add "test-case : link to the test case where this step belongs"
                    }
                }
        ))
    }

    def "patch-call-test-step"() {
        given:
        def json = """{
                "_type": "call-step",
                "delegate_parameter_values": "false",
                "index" : 0,
                 "called_dataset": {
                        "_type": "dataset",
                        "id": 33
                }
        }
        """

        def proj = project {
            id = 15L
            name = "Project alpha"
        }

        def tc = testCase {
            id = 297L
            name = "target test case"
            project = proj
        }

        def calledTc = testCase {
            id = 365L
            name = "call me later"
            project = proj
        }

        def calledDs = dataset {
            id = 33L
            name = "topping"
        }

        def ts = callStep {
            id = 441L
            delegateParameterValues = true
            testCase = tc
            calledTestCase = calledTc
        }

        def updatedTs = callStep {
            id = 441L
            delegateParameterValues = false
            calledDataset = calledDs
            testCase = tc
            calledTestCase = calledTc
        }

        and:
        EntityManager em = Mock()
        emf.createEntityManager() >> em
        em.find(TestCase.class, 365L) >> calledTc
        em.find(Dataset.class, 33L) >> calledDs

        restTestStepService.getOne(441L) >> ts

        permService.canRead(_) >> true
        restTestStepService.patchTestStep(_, _) >> updatedTs

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-steps/{id}", 441)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "call-step"
            "id".is 441
            "delegate_parameter_values".is false
            "called_test_case".test {
                "_type".is "test-case"
                "id".is 365
                "name".is "call me later"
            }
            "test_case".test {
                "_type".is "test-case"
                "id".is 297
                "name".is "target test case"
            }
            "called_dataset".test {
                "_type".is "dataset"
                "id".is 33
                "name".is "topping"
            }
        }

        // document
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the step"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    requestFields {
                        add "_type (string) : the type of step"
                        add "delegate_parameter_values (string) : whether the parameters of the called test case should be " +
                                "set by the caller rather than by a dataset of the called. A value of 'true' usually mean that " +
                                "the 'called_dataset' is null."
                        add "index(number) : the index of the step in the test case"
                        addAndStop "called_dataset (object) : the dataset to use for the called test case, if any (may be null if not)"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the step"
                        add "_links (object) : related links"
                    }

                    _links {
                        add "self : link to this step"
                        add "test-case : link to the test case where this step belongs"
                    }
                }
        ))
    }

    def "delete-test-step"() {

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/test-steps/{ids}", "169,180")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * restTestStepService.deleteTestStepsByIds(_)

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the steps"
                    }
                }
        ))
    }

    def "get-keyword-step"() {

        given:
            def fragment1 = actionWordText {
                text = "a customer named "
            }
            def fragment2 = actionWordParameter {
                id = 1L
                name = "param1"
                defaultValue = "Roger"
            }
            def value2 = actionWordParameterValue {
                value = "Gustave"
                actionWordParam = fragment2
            }

            def action = actionWord {
                id = 14L
                fragments = [
                        fragment1,
                        fragment2
                ]
            }
        and:
            def step = keywordStep {
                id = 442L
                testCase = keywordTestCase {
                    id = 298L
                    name = "Order a meal"
                }
                keyword = Keyword.GIVEN
                actionWord = action
                paramValues = [value2]
                datatable = """| product | price |
| Expresso | 0.40 |"""
                docstring = ""
                comment = """Products are from France.
Prices are all with tax."""
            }
        and:
            restTestStepService.getOne(_) >> step
        when:
            def res = mockMvc.perform(
                    RestDocumentationRequestBuilders
                            .get("/api/rest/latest/test-steps/{id}", 442)
                            .header("Accept", "application/json"))
        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "keyword-step"
            "id".is 442
            "test_case".test {
                "_type".is "keyword-test-case"
                "id".is 298
                "name".is "Order a meal"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/298"
            }
            "keyword".is "GIVEN"
            "action".is "a customer named \"Gustave\""
            "datatable".is """| product | price |
| Expresso | 0.40 |"""
            "docstring".is ""
            "comment".is """Products are from France.
Prices are all with tax."""
            "index".is 0
            selfRelIs "http://localhost:8080/api/rest/latest/test-steps/442"
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test step"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add "id (number) : the id of the step"
                        add "_type (string) : the type of step"
                        addAndStop "test_case (object) : the test case this step belongs to"
                        add "keyword (string) : the part which gives structure and meaning to an action word. Possible values are : GIVEN, WHEN, THEN, BUT, AND"
                        add "action (string) : the action of the step"
                        add "datatable (string) : the datatable of the step"
                        add "docstring (string) : the docstring of the step"
                        add "comment (string) : the comment of the step"
                        add "index(number) : the index of the step in the test case"
                        addAndStop "_links (object) : related links"
                    }
                    _links {
                        add "self : link to this step"
                    }
                }
        ))

    }

    def "post-keyword-test-step"() {

        given:
        def json = """{
                "_type": "keyword-step",
                "keyword": "AND",
                "action": "I have 5 apples",
                "datatable": "| Product | Price |\\n| Espresso | 0.30 |\\n| Cappuccino | 0.40 |\\n| Macchiato | 0.40 |",
                "docstring": "",
                "comment": "Products are from France.\\nPrices are all with tax."
            }
            """

        def proj = project {
            id = 16L
            name = "Project beta"
        }

        def tc = keywordTestCase {
            id = 241L
            name = "target keyword test case"
            project = proj
        }

        def fragment1 = actionWordText {
            text = "I have "
        }
        def fragment2 = actionWordParameter {
            id = 1L
            name = "param1"
            defaultValue = "4"
        }
        def value2 = actionWordParameterValue {
            value = "5"
            actionWordParam = fragment2
        }
        def fragment3 = actionWordText {
            text = " apples"
        }

        def step = keywordStep {
            id = 210L
            testCase = tc
            keyword = Keyword.AND
            actionWord = actionWord {
                id = 12L
                fragments = [fragment1, fragment2, fragment3]
            }
            paramValues = [value2]
            datatable = "| Product | Price |\n| Espresso | 0.30 |\n| Cappuccino | 0.40 |\n| Macchiato | 0.40 |"
            docstring = ""
            comment = "Products are from France.\nPrices are all with tax."
        }

        and:
        restTestCaseService.getOne(241L) >> tc
        restTestStepService.createTestStep(_, _) >> step

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases/{testCaseId}/steps", 241)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        // Test

        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "keyword-step"
            "id".is 210
            "test_case".test {
                "_type".is "keyword-test-case"
                "id".is 241
                "name".is "target keyword test case"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/241"
            }
            "keyword".is "AND"
            "action".is "I have \"5\" apples"
            "datatable".is "| Product | Price |\n| Espresso | 0.30 |\n| Cappuccino | 0.40 |\n| Macchiato | 0.40 |"
            "docstring".is ""
            "comment".is "Products are from France.\nPrices are all with tax."
            "index".is 0
            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/test-steps/210"
                "keyword-test-case".linksTo "http://localhost:8080/api/rest/latest/test-cases/241"
            }


        }

        // document
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "testCaseId : the id of the keyword test case of which to add new keyword test step"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    requestFields {
                        add "_type (string) : the type of step"
                        add "keyword (string) : the part which gives structure and meaning to an action word. Possible values are : GIVEN, WHEN, THEN, BUT, AND"
                        add "action (string) : the action of the step"
                        add "datatable (string) : the datatable of the step"
                        add "docstring (string) : the docstring of the step"
                        add "comment (string) : the comment of the step"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the step"
                        add "test_case (object) : the keyword test case this keyword step is part of"
                        add "index(number) : the index of current keyword step in the keyword test case"
                        add "_links (object) : related links"
                    }

                    _links {
                        add "self : link to this keyword step"
                        add "keyword-test-case : link to the keyword test case where this keyword step belongs"
                    }
                }
        ))

    }

    def "patch-keyword-test-step"() {
        given:
        def json = """{
                "_type": "keyword-step",
                "index" : 0,
                "keyword" : "GIVEN",
                "action": "I am \\"happy\\"",
                "datatable": "| Product | Price |\\n| Espresso | 0.30 |\\n| Cappuccino | 0.30 |\\n| Macchiato | 0.30 |",
                "docstring" : "",
                "comment": "Products are from France.\\nPrices are all with tax."
        }
        """

        def proj = project {
            id = 15L
            name = "Project alpha"
        }

        def tc = keywordTestCase {
            id = 297L
            name = "target test case"
            project = proj
        }

        def aw = new KeywordTestStepActionWordParser()
                .createActionWordFromKeywordTestStep("a default action")

        def ts = keywordStep {
            id = 441L
            testCase = tc
            keyword = Keyword.GIVEN
            actionWord = aw
            datatable = "| Product | Price |\n| Espresso | 0.40 |\n| Cappuccino | 0.50 |\n| Macchiato | 0.50 |"
            docstring = ""
            comment = "Products are from France.\nPrices are all with tax."
        }

        def fragment1 = actionWordText {
            text = "I am "
        }
        def fragment2 = actionWordParameter {
            id = 1L
            name = "param1"
            defaultValue = "sad"
        }
        def value2 = actionWordParameterValue {
            value = "happy"
            actionWordParam = fragment2
        }

        def updatedTs = keywordStep {
            id = 441L
            testCase = tc
            keyword = Keyword.AND
            actionWord = actionWord {
                id = 366L
                fragments = [fragment1, fragment2]
            }
            paramValues = [value2]
            datatable = "| Product | Price |\n| Espresso | 0.30 |\n| Cappuccino | 0.30 |\n| Macchiato | 0.30 |"
            docstring = ""
            comment = "Products are from France.\nPrices are all with tax."
        }

        and:
        restTestStepService.getOne(441L) >> ts

        restTestStepService.patchTestStep(_, _) >> updatedTs

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-steps/{id}", 441)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:


         // Test

        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "keyword-step"
            "id".is 441
            "test_case".test {
                "_type".is "keyword-test-case"
                "id".is 297
                "name".is "target test case"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/297"
            }
            "keyword".is "AND"
            "action".is "I am \"happy\""
            "datatable".is "| Product | Price |\n| Espresso | 0.30 |\n| Cappuccino | 0.30 |\n| Macchiato | 0.30 |"
            "docstring".is ""
            "comment".is "Products are from France.\nPrices are all with tax."
            "index".is 0
            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/test-steps/441"
                "keyword-test-case".linksTo "http://localhost:8080/api/rest/latest/test-cases/297"
            }
        }

        // document
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the step"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    requestFields {
                        add "_type (string) : the type of step"
                        add "index(number) : the index of the step in the test case"
                        add "keyword (string) : the part which gives structure and meaning to an action word. Possible values are : GIVEN, WHEN, THEN, BUT, AND"
                        add "action (string) : the action of the step"
                        add "datatable (string) : the datatable of the step"
                        add "docstring (string) : the docstring of the step"
                        add "comment (string) : the comment of the step"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the step"
                        add "test_case (object) : the test case this step is part of"
                        add "_links (object) : related links"
                    }

                    _links {
                        add "self : link to this step"
                        add "keyword-test-case : link to the test case where this step belongs"
                    }
                }
        ))
    }

    def "link-requirements-to-test-step"() {
        given:
        def verifiedRequirementVersions = [
                SquashEntityBuilder.requirementVersion {
                    id = 12L
                    name = "My first requirement"
                    withDefaultRequirement()
                },
                SquashEntityBuilder.requirementVersion {
                    id = 13L
                    name = "My second requirement"
                    withDefaultRequirement()
                },
                SquashEntityBuilder.requirementVersion {
                    id = 14L
                    name = "My third requirement"
                    withDefaultRequirement()
                }
        ]
        def step = SquashEntityBuilder.actionStep {
            id = 235l
            action = "<p>Wave your hand</p>"
            expectedResult = "<p>The door opens</p>"
            index = 0

            testCase = SquashEntityBuilder.testCase {
                id = 120l
                name = "Door opening system"
            }

            requirementVersions = verifiedRequirementVersions
        }



        def customFields = [SquashEntityBuilder.cufValue {
            label = "note"
            inputType "PLAIN_TEXT"
            code = "cuf_txt_note"
            value = "Star Trek style welcomed but not mandatory"
        },
        SquashEntityBuilder.cufValue {
            label = "see also"
            inputType "TAG"
            code = "cuf_tags_see_also"
            value = ["smart home", "sensors", "hand gesture"]
        }];

        and:
        restTestStepService.getOne(_) >> step
        1 * verifRequirementManagerService.linkRequirementsToTestStep([12, 13, 14], 235)
        permService.canRead(_) >> true
        cufService.findAllCustomFieldValues(step) >> customFields

        when:
        def res = mockMvc.perform(
                post("/api/rest/latest/test-steps/{id}/coverages/{requirementIds}", 235L, "12,13,14")
                        .accept("application/json"))

        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "action-step"
            "id".is 235
            "action".is "<p>Wave your hand</p>"
            "expected_result".is "<p>The door opens</p>"
            "index".is 0
            "custom_fields".hasSize 2
            "custom_fields[0]".test {
                "code".is "cuf_txt_note"
                "label".is "note"
                "value".is "Star Trek style welcomed but not mandatory"
            }
            "custom_fields[1]".test {
                "code".is "cuf_tags_see_also"
                "label".is "see also"
                "value".contains "smart home", "sensors", "hand gesture"
            }
            "test_case".test {
                "_type".is "test-case"
                "id".is 120
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/120"
            }
            "verified_requirements".hasSize 3
            "verified_requirements".test {
                "[0]".test {
                    "_type".is "requirement-version"
                    "id".is 12
                    "name".is "My first requirement"
                }
                "[1]".test {
                    "_type".is "requirement-version"
                    "id".is 13
                    "name".is "My second requirement"
                }
                "[2]".test {
                    "_type".is "requirement-version"
                    "id".is 14
                    "name".is "My third requirement"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/test-steps/235"

        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test step"
                        add "requirementIds : the ids of the requirements to link"
                    }
                }
        ))
    }

    def "unlink-requirements-to-test-step"() {

        when:
        def res = mockMvc.perform(
                delete("/api/rest/latest/test-steps/{id}/coverages/{requirementIds}", 235L, "12,14")
                        .accept("application/json"))

        then:
        1 * verifRequirementManagerService.unlinkRequirementsFromTestStep([12,14],235)
        res.andExpect(status().isNoContent())
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test step"
                        add "requirementIds : the ids of the requirements to unlink"
                    }
                }
        ))
    }
}
