/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.utils

import org.springframework.web.bind.annotation.RequestMapping
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.plugin.rest.core.jackson.EntityManagerWrapper
import org.squashtest.tm.plugin.rest.core.jackson.WrappedDTO
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import spock.lang.Specification
import spock.lang.Unroll

import java.lang.reflect.Method
import java.util.regex.Pattern

/**
 * Created by bsiri on 13/07/2017.
 */
class DeserializationConfigHelperTest extends Specification{

    DeserializationConfigHelper utils

    EntityManagerWrapper em = Mock()
    NodeHierarchyHelpService hier = Mock()

    def setup(){
        utils = new DeserializationConfigHelper(em, hier)
    }


    @Unroll("should return #{parameterType} as the parameter type of WrappedDTO subclass #{type}")
    def "should return the parameter type of a subclass of WrappedDTO"(){

        expect :
            utils.extractTargetType(type) == parameterType

        where :
        type                        |   parameterType
        DirectSubtype               |   String
        GrandChildrenType           |   Long

    }

    def "should return the type itself if not a wrapped dto"(){

        expect :
            utils.extractTargetType(Execution) == Execution

    }


    @RequestMapping("/test-cases/{id}/whatever")
    def requestMapping1(){

    }

    def "should create a pattern out of a mapping"(){

        given :
            // convoluted way to retrieve the annotation
            Method thismethod = DeserializationConfigHelperTest.class.methods.find { it.name == "requestMapping1"}
            RequestMapping annot = thismethod.getAnnotation(RequestMapping)

        when :
            Pattern p = utils.toPattern(annot)

        then :
            p.toString() == '/test-cases/(\\d+)/whatever$'
    }


    private static final class DirectSubtype extends WrappedDTO<String>{

    }

    private static abstract class IntermediateType<ENTITY> extends WrappedDTO<ENTITY>{

    }

    private static final class GrandChildrenType extends IntermediateType<Long>{

    }

}
