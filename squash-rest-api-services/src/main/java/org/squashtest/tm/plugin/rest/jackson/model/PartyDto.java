/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.PartyPreference;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.domain.users.UsersGroup;

import java.util.List;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "_type")
@JsonSubTypes({ //here are the _type -> java class for DESERIALIZATION only
        @JsonSubTypes.Type(value = UserDto.class, name = "user"),
        @JsonSubTypes.Type(value = TeamDto.class, name = "team"),
})
public abstract class PartyDto {

    private Long id;

    private UsersGroup group;

    private List<PartyPreference> preferences;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UsersGroup getGroup() {
        return group;
    }

    public void setGroup(UsersGroup group) {
        this.group = group;
    }

    public List<PartyPreference> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<PartyPreference> preferences) {
        this.preferences = preferences;
    }

    public abstract void accept(PartyDtoVisitor partyDtoVisitor);


    public static Party convertDto(PartyDto dto) {
        final Party[] parties = new Party[1];

        PartyDtoVisitor visitor = new PartyDtoVisitor() {

            @Override
            public void visit(UserDto userDto) {
                User user = new User();
                user.setGroup(userDto.getGroup());
                parties[0] = user;
            }

            @Override
            public void visit(TeamDto teamDto) {
                Team team = new Team();
                team.setGroup(teamDto.getGroup());
                parties[0] = team;
            }
        };
        dto.accept(visitor);
        return parties[0];
    }
}
