/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.requirement.RequirementCriticality;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.domain.requirement.RequirementVersionLink;
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage;
import org.squashtest.tm.plugin.rest.jackson.deserializer.RestNatureTypeCategoryDeserializer;

import java.util.List;
import java.util.Set;

public class RequirementVersionDto{

    private Long id;

    //convenient attribute used for validation as we need the test case dto to have the project and check cufs
    @JsonIgnore
    private Long projectId;

    private String name;

    private String description;

    private Set<RequirementVersionLink> requirementVersionLinks;

    private Set<RequirementVersionCoverage> requirementVersionCoverages;

    private String reference;

    private RequirementCriticality criticality;

    @JsonDeserialize(using=RestNatureTypeCategoryDeserializer.class)
    private InfoListItem category;

    private RequirementStatus status;

    private RequirementDto requirement;

    private int versionNumber;

    private Set<Milestone> milestones;

    private boolean hasName=false;
    private boolean hasDescription=false;
    private boolean hasReference=false;
    private boolean hasCriticality=false;
    private boolean hasCategory=false;
    private boolean hasStatus=false;
    private boolean hasCufs=false;

    public boolean isHasName() {return hasName;}
    public boolean isHasDescription() {return hasDescription;}
    public boolean isHasReference() {return hasReference; }
    public boolean isHasCriticality() {return hasCriticality;}
    public boolean isHasCategory() {return hasCategory;}
    public boolean isHasStatus() {return hasStatus;}
    public boolean isHasCufs() {return hasCufs;}

    @JsonProperty("custom_fields")
    private List<CustomFieldValueDto> customFields;

    public Set<RequirementVersionLink> getRequirementVersionLinks() {
        return requirementVersionLinks;
    }

    public void setRequirementVersionLinks(Set<RequirementVersionLink> requirementVersionLinks) {
        this.requirementVersionLinks = requirementVersionLinks;
    }

    public Set<RequirementVersionCoverage> getRequirementVersionCoverages() {
        return requirementVersionCoverages;
    }

    public void setRequirementVersionCoverages(Set<RequirementVersionCoverage> requirementVersionCoverages) {
        this.requirementVersionCoverages = requirementVersionCoverages;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {

        this.reference = reference;
        this.hasReference=true;
    }

    public RequirementCriticality getCriticality() { return criticality;}

    public void setCriticality(RequirementCriticality criticality) {

        this.criticality = criticality;
        this.hasCriticality = true;
    }

    public InfoListItem getCategory() {
        return category;
    }

    public void setCategory(InfoListItem category) {
        this.category = category;
        this.hasCategory = true;
    }

    public RequirementStatus getStatus() {
        return status;
    }

    public void setStatus(RequirementStatus status) {
        this.status = status;
        this.hasStatus = true;
    }

    public RequirementDto getRequirement() {
        return requirement;
    }

    public void setRequirement(RequirementDto requirement) {
        this.requirement = requirement;
    }

    public int getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(int versionNumber) {
        this.versionNumber = versionNumber;
    }

    public Set<Milestone> getMilestones() {
        return milestones;
    }

    public void setMilestones(Set<Milestone> milestones) {
        this.milestones = milestones;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        this.hasName = true;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        this.hasDescription = true;
    }

    public List<CustomFieldValueDto> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(List<CustomFieldValueDto> customFields) {

        this.customFields = customFields;
        this.hasCufs =true;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

}
