/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.utils;

import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.HibernateProxyHelper;
import org.springframework.aop.support.AopUtils;

public class ProxyUtils {

	/**
	 * Returns the actual class of a given object, regardless of which proxies wrap it
	 * 
	 * @param obj
	 */
	public static final Class<?> getClass(Object obj){
		Class<?> clazz = obj.getClass();
		if (AopUtils.isAopProxy(obj) || AopUtils.isCglibProxy(obj)){
			clazz = AopUtils.getTargetClass(obj);
		}
		else if (HibernateProxy.class.isAssignableFrom(clazz)){
			clazz = HibernateProxyHelper.getClassWithoutInitializingProxy(obj);
		}
		return clazz;
	}
	
}
