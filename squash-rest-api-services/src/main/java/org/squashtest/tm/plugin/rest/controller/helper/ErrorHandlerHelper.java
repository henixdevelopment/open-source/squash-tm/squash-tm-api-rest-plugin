/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller.helper;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;

import java.util.List;

public class ErrorHandlerHelper {

    public static void throwIfError(Object target, List<Errors> errors, String objectName) throws BindException {
        if (errors.size() > 0) {
            BindException bindException = new BindException(target, objectName);
            for (Errors error : errors) {
                bindException.addAllErrors(error);
            }
            throw bindException;
        }
    }

}
