<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>org.squashtest.tm.plugin</groupId>
	<artifactId>api.rest</artifactId>
	<version>${revision}${sha1}${changelist}</version>
	<packaging>pom</packaging>

	<name>api-rest</name>
	<url>http://www.henix.com/</url>
	<inceptionYear>2017</inceptionYear>
	<description>A plugin that exposes a REST API for Squash-TM, version 1.16+</description>

	<organization>
		<name>Henix, henix.fr</name>
		<url>http://www.henix.fr</url>
	</organization>


	<licenses>
		<license>
			<name>GNU Lesser General Public License V3</name>
			<url>https://www.gnu.org/licenses/lgpl-3.0.txt</url>
		</license>
	</licenses>

	<developers>
		<developer>
			<id>bsiri</id>
			<email>bsiri{at}henix.fr</email>
		</developer>
		<developer>
			<id>jthebault</id>
			<email>jthebault{at}qualixo.com</email>
		</developer>
		<developer>
			<id>jlor</id>
			<email>jlor{at}qualixo.com</email>
		</developer>
		<developer>
			<id>zyang</id>
			<email>zyang{at}henix.fr</email>
		</developer>
	</developers>

	<scm>
		<connection>scm:git:${project.basedir}</connection>
		<developerConnection>scm:git:${project.basedir}</developerConnection>
		<tag>HEAD</tag>
	</scm>

	<distributionManagement>
		<repository>
			<id>squash-release-deploy-repo</id>
			<name>Squash releases deployment repo</name>
			<url>${deploy-repo.release.url}</url>
		</repository>
		<snapshotRepository>
			<id>squash-snapshot-deploy-repo</id>
			<name>Squash snapshot deployment repo</name>
			<url>${deploy-repo.snapshot.url}</url>
		</snapshotRepository>
	</distributionManagement>

	<properties>
		<!-- PROJECT -->
		<revision>10.0.0</revision>
		<sha1></sha1>
		<changelist>-SNAPSHOT</changelist>
		<java.version>17</java.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

		<!-- DEPENDENCIES -->
        <squash.core.version>10.0.0-SNAPSHOT</squash.core.version>

		<jackson-datatype-joda.version>2.17.2</jackson-datatype-joda.version>
		<spock-junit.version>2.4-M4-groovy-4.0</spock-junit.version>

		<!-- MAVEN PLUGINS -->
		<asciidoc.version>2.2.2</asciidoc.version>
		<spring-asciidoc.version>3.0.0</spring-asciidoc.version>
		<asciidoc.filtered-directory>${project.build.directory}/filtered-asciidoc-sources</asciidoc.filtered-directory>
		<asciidoc.packaging.jar-path>${project.build.outputDirectory}/static/api/rest/latest/docs</asciidoc.packaging.jar-path>
		<build-helper-maven-plugin.version>3.6.0</build-helper-maven-plugin.version>
		<ci-friendly.version>1.0.20</ci-friendly.version>
		<groovy-eclipse-batch.version>3.0.10-02</groovy-eclipse-batch.version>
		<groovy-eclipse-compiler.version>3.7.0</groovy-eclipse-compiler.version>
		<license-maven-plugin.version>4.5</license-maven-plugin.version>
		<maven-assembly-plugin.version>3.7.1</maven-assembly-plugin.version>
		<maven-compiler-plugin.version>3.13.0</maven-compiler-plugin.version>
		<maven-enforcer-plugin.version>3.5.0</maven-enforcer-plugin.version>
		<maven-failsafe-plugin.version>3.3.1</maven-failsafe-plugin.version>
		<maven-jar-plugin.version>3.4.2</maven-jar-plugin.version>
		<maven-release-plugin.version>3.1.1</maven-release-plugin.version>
		<maven-resources-plugin.version>3.3.1</maven-resources-plugin.version>
		<maven-surefire-plugin.version>3.3.1</maven-surefire-plugin.version>

		<!-- SONAR -->
		<sonar.projectKey>${project.groupId}:${project.artifactId}${env.APPEND_SONAR_PROJECT_KEY}</sonar.projectKey>
		<sonar.projectName>${project.name}${env.APPEND_SONAR_PROJECT_NAME}</sonar.projectName>
	</properties>


	<modules>
		<module>squash-rest-api-services</module>
		<module>squash-rest-api-distribution</module>
		<module>squash-tm-domain-builder</module>
		<module>squash-rest-api-core</module>
	</modules>


	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.squashtest.tm</groupId>
				<artifactId>squash-tm-bom</artifactId>
				<version>${squash.core.version}</version>
				<scope>import</scope>
				<type>pom</type>
			</dependency>

			<!-- squash dependencies -->
			<dependency>
				<groupId>org.squashtest.tm</groupId>
				<artifactId>tm.domain</artifactId>
				<version>${squash.core.version}</version>
				<scope>provided</scope>
			</dependency>

			<dependency>
				<groupId>org.squashtest.tm</groupId>
				<artifactId>tm.service</artifactId>
				<version>${squash.core.version}</version>
				<scope>provided</scope>
			</dependency>
			<!-- /squash dependencies -->

			<!-- support dependencies. Note : spring-boot-starter-hateoas jackson-datatype-hibernate5
				spring-restdocs-mockmvc should be managed here but were finally not included.
				It is so because their management is already specified by the squash-tm-bom,
				and redeclaring in this dependencyManagement would not make them inherit
				properly in the children modules. -->
			<dependency>
				<groupId>com.fasterxml.jackson.datatype</groupId>
				<artifactId>jackson-datatype-joda</artifactId>
				<version>${jackson-datatype-joda.version}</version>
				<scope>provided</scope>
			</dependency>

			<!-- /support dependencies -->


			<!-- test dependencies -->
			<dependency>
				<groupId>org.squashtest.tm</groupId>
				<artifactId>spock-test-dependencies</artifactId>
				<version>${squash.core.version}</version>
				<type>pom</type>
				<scope>test</scope>
			</dependency>

			<dependency>
				<groupId>org.squashtest.tm</groupId>
				<artifactId>tm.web</artifactId>
				<version>${squash.core.version}</version>
				<classifier>classes</classifier>
				<scope>test</scope>
			</dependency>

			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-api</artifactId>
				<scope>provided</scope>
			</dependency>

			<!-- /test dependencies -->

		</dependencies>
	</dependencyManagement>



	<build>

		<sourceDirectory>src/main/java</sourceDirectory>
		<testSourceDirectory>src/test/groovy</testSourceDirectory>


		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>${maven-compiler-plugin.version}</version>
					<configuration>
						<!-- Std java compiler plugin is configured to compile groovy using
							groovy-eclipse-compiler -->
						<source>${java.version}</source>
						<target>${java.version}</target>
						<compilerId>groovy-eclipse-compiler</compilerId>

						<excludes>
							<exclude>src/it/groovy/**</exclude>
						</excludes>

					</configuration>
					<dependencies>
						<dependency>
							<groupId>org.codehaus.groovy</groupId>
							<artifactId>groovy-eclipse-batch</artifactId>
							<version>${groovy-eclipse-batch.version}</version>
						</dependency>
						<dependency>
							<groupId>org.codehaus.groovy</groupId>
							<artifactId>groovy-eclipse-compiler</artifactId>
							<version>${groovy-eclipse-compiler.version}</version>
						</dependency>
					</dependencies>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>${maven-surefire-plugin.version}</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-failsafe-plugin</artifactId>
					<version>${maven-failsafe-plugin.version}</version>
				</plugin>

				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-release-plugin</artifactId>
					<version>${maven-release-plugin.version}</version>
				</plugin>
			</plugins>
		</pluginManagement>


		<plugins>
			<plugin>
				<!-- Checks license headers throughout the project -->
				<groupId>com.mycila</groupId>
				<artifactId>license-maven-plugin</artifactId>
				<version>${license-maven-plugin.version}</version>
				<inherited>false</inherited>
				<configuration>
					<strictCheck>true</strictCheck>
					<aggregate>true</aggregate>
					<properties>
						<project.label>REST API plugin for Squash TM (${project.name})</project.label>
						<license.yearSpan>${project.inceptionYear}</license.yearSpan>
						<license.copyrightOwner>${project.organization.name}</license.copyrightOwner>
					</properties>
					<licenseSets>
						<licenseSet>
							<header>license/header.txt</header>
							<excludes>
								<!-- license files -->
								<exclude>**/NOTICE.*</exclude>
								<exclude>**/README.*</exclude>
								<exclude>**/NOTICE</exclude>
								<exclude>**/README</exclude>
								<exclude>**/README*.*</exclude>
								<exclude>**/readme*.*</exclude>
								<!-- manifest templates -->
								<exclude>**/*.mf</exclude>
								<exclude>**/bnd.bnd</exclude>
								<!-- mercurial files -->
								<exclude>**/*.orig</exclude>
								<exclude>**/.hgtags</exclude>
								<exclude>**/.hgignore</exclude>
								<!-- misc system / ide files -->
								<exclude>**/build.properties</exclude>
								<!-- m2e generated files -->
								<exclude>**/pom.properties</exclude>
								<!-- others -->
								<exclude>**/pom.xml</exclude>
								<exclude>.settings</exclude>
								<exclude>build.properties</exclude>
								<exclude>.classpath</exclude>
								<exclude>.project</exclude>
								<exclude>**/license/**</exclude>
								<exclude>**/*.iml</exclude>
								<exclude>**/.idea/**</exclude>
								<exclude>**/pom.xml.*</exclude>
								<exclude>**/META-INF/**</exclude>
								<exclude>**/assembly/layout/**</exclude>
								<exclude>**/*.adoc</exclude>
								<exclude>.m2/**</exclude>
								<exclude>**/.ci-friendly-pom.xml</exclude>
								<exclude>.editorconfig</exclude>
							</excludes>
						</licenseSet>
					</licenseSets>
					<mapping>
						<tag>DYNASCRIPT_STYLE</tag>
					</mapping>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>check</goal>
						</goals>
						<phase>validate</phase>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>com.outbrain.swinfra</groupId>
				<artifactId>ci-friendly-flatten-maven-plugin</artifactId>
				<version>${ci-friendly.version}</version>
				<executions>
					<execution>
						<goals>
							<!-- Ensure proper cleanup. Will run on clean phase-->
							<goal>clean</goal>
							<!-- Enable ci-friendly version resolution. Will run on process-resources phase-->
							<goal>flatten</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>



	<profiles>

		<!--
			This profile forbids the javadoc plugin to fail for linter reasons.
			The proper fix (that is, really documenting parameters and such) must
			wait a bit.
		-->
		<profile>
			<id>disable-java8-doclint</id>
			<activation>
				<jdk>[1.8,)</jdk>
			</activation>
			<properties>
				<additionalparam>-Xdoclint:none</additionalparam>
			</properties>
		</profile>

		<profile>
			<!-- We need to define the profile integration-tests here even if it has
				no impact because otherwise the profile wont be activated at this project
				root level and will make the enforcer plugin fail -->
			<id>integration-tests</id>
			<activation>
				<activeByDefault>false</activeByDefault>
			</activation>
			<properties>
				<it.enabled>true</it.enabled>
			</properties>
		</profile>

		<profile>
			<id>distro</id>
			<activation>
				<activeByDefault>false</activeByDefault>
			</activation>

			<build>
				<plugins>

					<plugin>
						<!-- Enforce that the profile integration-tests is enabled to make
							sure that all tests are green and the doc is generated (see the service module) -->
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-enforcer-plugin</artifactId>
						<version>${maven-enforcer-plugin.version}</version>
						<executions>
							<execution>
								<id>enforce-profile-integration-tests</id>
								<goals>
									<goal>enforce</goal>
								</goals>
								<configuration>
									<rules>
										<requireProperty>
											<property>it.enabled</property>
											<message>You forgot to enable the profile integration-tests. That profile runs all the tests and generates the documentation.</message>
											<regex>true</regex>
										</requireProperty>
									</rules>
									<fail>true</fail>
								</configuration>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>



	<repositories>
		<repository>
			<id>squash-release-repo</id>
			<url>http://repo.squashtest.org/maven2/releases</url>
		</repository>
		<repository>
			<id>squash-snapshot-repo</id>
			<url>http://repo.squashtest.org/maven2/snapshots</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
			<releases>
				<enabled>false</enabled>
			</releases>
		</repository>
	</repositories>
</project>
