/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.resource.Resource;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.ParentEntity;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementFolderDto;
import org.squashtest.tm.plugin.rest.repository.RestRequirementFolderRepository;
import org.squashtest.tm.plugin.rest.service.RestRequirementFolderService;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.library.FolderModificationService;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.List;

import static org.squashtest.tm.plugin.rest.utils.PaginationUtils.emptyPage;

@Service
@Transactional
public class RestRequirementFolderServiceImpl implements RestRequirementFolderService {

	@Inject 
	@Qualifier("squashtest.tm.service.RequirementFolderModificationService")
	private FolderModificationService<RequirementFolder> nativeService;
	
	@Inject
	private ProjectFinder projectFinder;
	
	@Inject
	private RestRequirementFolderRepository dao;

	@Inject
	private RequirementLibraryNavigationService  requirementLibraryNavigationService;

	@Inject
	private CustomFieldValueHelper customFieldValueHelper;

	@PersistenceContext
	private EntityManager entityManager;
	
	
	@Override
    @Transactional(readOnly=true)
	public RequirementFolder getOne(Long id) {
		return nativeService.findFolder(id);
	}

	@Override
    @Transactional(readOnly=true)
	public Page<RequirementFolder> findAllReadable(Pageable pageable) {
		Collection<Long> projectIds = projectFinder.findReadableProjectIdsOnRequirementLibrary();
		return projectIds.isEmpty() ? emptyPage(pageable) : dao.findAllInProjects(projectIds, pageable);
	}

	@Override
	@Transactional(readOnly=true)
	public List<RequirementFolder> findAllRequirementFoldersByPojectIds(List<Long> projectIds) {
		return dao.findRequirementFolderByProjects(projectIds);
	}
	
	@Override
    @Transactional(readOnly=true)
	@PreAuthorize("@apiSecurity.hasPermission(#folderId,'org.squashtest.tm.domain.requirement.RequirementFolder' , 'READ')")
	public Page<RequirementLibraryNode<Resource>> findFolderContent(long folderId, Pageable pageable) {
		return dao.findFolderContent(folderId, pageable);
	}
	
	
	@Override
    @Transactional(readOnly=true)
	@PreAuthorize("@apiSecurity.hasPermission(#folderId,'org.squashtest.tm.domain.requirement.RequirementFolder' , 'READ')")
	public Page<RequirementLibraryNode<Resource>> findFolderAllContent(long folderId, Pageable pageable) {
		return dao.findFolderAllContent(folderId, pageable);
	}

	@Override
	public RequirementFolder addRequirementFolder(RequirementFolderDto folderDto) throws InvocationTargetException, IllegalAccessException {
		final RequirementFolder folder = new RequirementFolder();

		folder.setName(folderDto.getName());
		folder.setDescription(folderDto.getDescription());

		addToParent(folderDto, folder);
		if(folderDto.getCustomFields()!=null) {
			List<CustomFieldValueDto> listCufsDto = folderDto.getCustomFields();
			customFieldValueHelper.patchCustomFieldValue(folder,listCufsDto);
		}
		entityManager.persist(folder);
		entityManager.flush();
		return folder;
	}

	@Override
	@PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.requirement.RequirementFolder' , 'WRITE')")
	public RequirementFolder patchRequirementFolder(RequirementFolderDto folderPatch, long id) {

		RequirementFolder folder = this.getOne(id);

		folder.setName(folderPatch.getName());
		folder.setDescription(folderPatch.getDescription());

		if(folderPatch.getCustomFields()!=null) {
			List<CustomFieldValueDto> listCufsDto = folderPatch.getCustomFields();
			customFieldValueHelper.patchCustomFieldValue(folder,listCufsDto);
		}

		return folder;
	}

	@Override
	public void deleteFolder(List<Long> folderIds) {
		requirementLibraryNavigationService.deleteNodes(folderIds);
	}


	private void addToParent(RequirementFolderDto folderDto, RequirementFolder folder) {

		ParentEntity parent = folderDto.getParent();
		switch (parent.getRestType()) {
			case PROJECT:
				addRequirementFolderToLibrary(folder, folderDto, parent);
				break;
			case REQUIREMENT_FOLDER:
				addRequirementFolderToFolder(folder, folderDto, parent);
				break;
			default:
				throw new IllegalArgumentException("Programmatic error : Rest type " + parent.getRestType() + "is not a valid parent. You should validate this before.");
		}

	}

	private void addRequirementFolderToFolder(RequirementFolder folder, RequirementFolderDto folderDto, ParentEntity parent) {
		RequirementFolder requirementFolder = entityManager.find(RequirementFolder.class, parent.getId());
		if (requirementFolder != null) {
			requirementLibraryNavigationService.addFolderToFolder(requirementFolder.getId(), folder);
		} else {
			throw new IllegalArgumentException("Programmatic error : test case folder with id " + parent.getId() + "is unknown. You should validate this before.");

		}
	}

	private void addRequirementFolderToLibrary(RequirementFolder folder, RequirementFolderDto folderDto, ParentEntity parent) {
		Project project = entityManager.find(Project.class, parent.getId());
		if (project != null) {
			requirementLibraryNavigationService.addFolderToLibrary(project.getRequirementLibrary().getId(), folder);
		} else {
			throw new IllegalArgumentException("Programmatic error : project with id " + parent.getId() + "is unknown. You should validate this before.");
		}

	}



}
