/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;

import org.squashtest.tm.service.internal.dto.AutomatedTestPlanDTO;
import org.squashtest.tm.service.internal.dto.TriggerRequestDTO;
import org.squashtest.tm.service.testautomation.testplanretriever.CustomFieldValuesForExec;
import org.squashtest.tm.service.testautomation.testplanretriever.RestTestPlanFinder;
import org.squashtest.tm.service.testautomation.testplanretriever.TestPlanRetrievalService;
import org.squashtest.tm.service.testautomation.testplanretriever.exception.InvalidParameterException;
import org.squashtest.tm.service.testautomation.testplanretriever.exception.NoITPIFoundException;
import org.squashtest.tm.service.testautomation.testplanretriever.exception.NotFoundTargetUUIDException;
import org.squashtest.tm.service.testautomation.testplanretriever.exception.TestPlanException;
import org.squashtest.tm.service.testautomation.testplanretriever.validator.TriggerRequestValidator;

/** Controller that returns execution order for Squash Orchestrator automation job. */
public abstract class RestAbstractExecutionOrderController<
        T extends TriggerRequestDTO, C extends CustomFieldValuesForExec> {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(RestAbstractExecutionOrderController.class);

    private static final String TRIGGER_ITERATION = "trigger-iteration";
    private static final String TRIGGER_TEST_SUITE = "trigger-test-suite";
    private static final Map<String, String> TRIGGER_TYPE_FOR_ERROR_MESSAGES =
            Map.of(TRIGGER_ITERATION, "an iteration", TRIGGER_TEST_SUITE, "a test suite");

    @Inject protected TestPlanRetrievalService<T, C> tpRetrievalService;

    @Inject protected TriggerRequestValidator triggerRequestValidator;

    @Inject
    protected RestTestPlanFinder restTestPlanFinder;

    public abstract ResponseEntity<AutomatedTestPlanDTO> triggerIterationExecutionOrder(
            T triggerRequest) throws BindException;

    public abstract ResponseEntity<AutomatedTestPlanDTO> triggerTestSuiteExecutionOrder(
            T triggerRequest) throws BindException;

    protected ResponseEntity<AutomatedTestPlanDTO> getIterationResponse(T triggerRequest)
            throws BindException {
        String uuid = triggerRequest.getTargetUUID();
        LOGGER.debug("Got request for iteration {}", uuid);
        validateTriggerRequest(triggerRequest, TRIGGER_ITERATION);
        ResponseEntity<AutomatedTestPlanDTO> response;
        try {
            Iteration iteration = restTestPlanFinder.findIterationByUuid(uuid);
            List<Long> itemTestPlanIds = restTestPlanFinder.getItemTestPlanIdsByIterationUuid(uuid);
            AutomatedTestPlanDTO testPlan = tpRetrievalService.getIterationTestPlan(triggerRequest, iteration, itemTestPlanIds);
            response = ResponseEntity.ok(testPlan);
        } catch (TestPlanException ex) {
            response = handleTestPlanException(ex, triggerRequest, TRIGGER_ITERATION);
        }
        return response;
    }

    protected ResponseEntity<AutomatedTestPlanDTO> getTestSuiteResponse(T triggerRequest)
            throws BindException {
        String uuid = triggerRequest.getTargetUUID();
        LOGGER.debug("Got request for test suite {}", uuid);
        validateTriggerRequest(triggerRequest, TRIGGER_TEST_SUITE);
        ResponseEntity<AutomatedTestPlanDTO> response;
        try {
            TestSuite suite = restTestPlanFinder.findTestSuiteByUuid(uuid);
            List<Long> itemTestPlanIds = restTestPlanFinder.getItemTestPlanIdsByTestSuiteUuid(uuid);
            AutomatedTestPlanDTO testPlan = tpRetrievalService.getTestSuiteTestPlan(triggerRequest, suite, itemTestPlanIds);
            response = ResponseEntity.ok(testPlan);
        } catch (TestPlanException ex) {
            response = handleTestPlanException(ex, triggerRequest, TRIGGER_TEST_SUITE);
        }
        return response;
    }

    private ResponseEntity<AutomatedTestPlanDTO> handleTestPlanException(
            TestPlanException ex, T triggerRequest, String triggerType) throws BindException {
        if (ex instanceof InvalidParameterException) {
            return logInvalidParameterException(
                    (InvalidParameterException) ex, triggerRequest, triggerType);
        } else {
            logNoITPIFoundException((NoITPIFoundException) ex, triggerRequest, triggerType);
            return null;
        }
    }

    protected void validateTriggerRequest(T triggerRequest, String objectName) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(triggerRequest, objectName);
        triggerRequestValidator.validate(triggerRequest, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }
        ErrorHandlerHelper.throwIfError(triggerRequest, errors, objectName);
    }

    /* ******************************************* Exceptions management ******************************************* */

    protected ResponseEntity<AutomatedTestPlanDTO> logInvalidParameterException(
            InvalidParameterException ex, T triggerRequest, String targetType) throws BindException {
        ResponseEntity<AutomatedTestPlanDTO> response = null;
        if (ex instanceof NotFoundTargetUUIDException) {
            LOGGER.error("Trigger request rejected : this UUID was not found in the database.", ex);
            rejectTriggerRequestForNotFoundUUID(triggerRequest, targetType);
        } else {
            LOGGER.error("Return 500 response because of error while reading Execution Order JSON", ex);
            response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }

    protected void logNoITPIFoundException(
            NoITPIFoundException ex, T triggerRequest, String targetType) throws BindException {
        LOGGER.error(
                "Trigger request rejected : this UUID refers to "
                        + TRIGGER_TYPE_FOR_ERROR_MESSAGES.get(targetType)
                        + " with no automated test.",
                ex);
        rejectTriggerRequestForNoAutomatedTest(triggerRequest, targetType);
    }

    private void rejectTriggerRequestForNotFoundUUID(T triggerRequest, String objectName)
            throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(triggerRequest, objectName);
        validation.rejectValue(
                "targetUUID", "not found", "This target UUID was not found in the database.");

        errors.add(validation);

        ErrorHandlerHelper.throwIfError(triggerRequest, errors, objectName);
    }

    private void rejectTriggerRequestForNoAutomatedTest(T triggerRequest, String objectName)
            throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(triggerRequest, objectName);
        validation.rejectValue(
                "targetUUID",
                "empty test list",
                "This target UUID refers to "
                        + TRIGGER_TYPE_FOR_ERROR_MESSAGES.get(objectName)
                        + " with no automated test.");

        errors.add(validation);

        ErrorHandlerHelper.throwIfError(triggerRequest, errors, objectName);
    }
}
