/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import org.squashtest.tm.domain.requirement.ManagementMode

// ************** requirement 1 ***************************

def req1Ver1 = requirementVersion {
    id = 91L
    name = "sample requirement 98-1"
    versionNumber = 1
    reference = "REQ01"

    createdBy "User-1"
    createdOn "2017/07/17"
    lastModifiedBy "User-1"
    lastModifiedOn "2017/07/17"

    criticality "MAJOR"
    category "CAT_FUNCTIONAL"
    description = "<p>Description of the sample requirement.</p>"

    testCases = [
            testCase {
                id =101L
                name = "sample test case 1"
            },
            testCase {
                id =101L
                name = "sample test case 2"
            }
    ]
}

def req1 = requirement {

    id = 624L
    name = "sample requirement"

    project = project {
        id = 41L
        name = "sample project"
    }

    mode = ManagementMode.SYNCHRONIZED

    resource = req1Ver1

    versions = [
            req1Ver1
    ]
}


def req1Cufs = [
        cufValue {
            label = "cuf text"
            inputType "PLAIN_TEXT"
            code = "CF_TXT"
            value = "text value"
        },
        cufValue {
            label = "cuf tag"
            inputType  "TAG"
            code = "CF_TAG"
            value = ["tag_1", "tag_2"]
        }
]



// ************** requirement 2 ***************************

def req2Ver1 = requirementVersion {
    id = 101L
    name = "sample requirement 108-1"
    versionNumber = 1
    reference = "REQ02"

    createdBy "User-1"
    createdOn "2018/07/17"
    lastModifiedBy "User-1"
    lastModifiedOn "2018/07/17"

    criticality "MAJOR"
    category "CAT_FUNCTIONAL"
    description = "<p>Description of the other sample requirement.</p>"

    testCases = [
            testCase {
                id =101L
                name = "sample test case 1"
            },
            testCase {
                id =101L
                name = "sample test case 2"
            }
    ]
}

def req2 = requirement {

    id = 101L
    name = "sample requirement"

    project = project {
        id = 41L
        name = "sample project"
    }

    mode = ManagementMode.SYNCHRONIZED

    resource = req2Ver1

    versions = [
            req2Ver1
    ]
}


def req2Cufs = [
        cufValue {
            label = "cuf text"
            inputType "PLAIN_TEXT"
            code = "CF_TXT"
            value = "text value for req 1"
        },
        cufValue {
            label = "cuf tag"
            inputType  "TAG"
            code = "CF_TAG"
            value = ["tag_21", "tag_22"]
        }
]


// ****** Now pack the mocks and return ********************

[
        req1 : req1,
        req1Cufs : req1Cufs,

        req2 : req2,
        req2Cufs : req2Cufs
]
