/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestSuiteDto;
import org.squashtest.tm.plugin.rest.service.RestIssueService;
import org.squashtest.tm.plugin.rest.service.RestTestSuiteService;
import org.squashtest.tm.plugin.rest.validators.TestSuiteValidator;

import javax.inject.Inject;
import java.util.List;

import static org.squashtest.tm.plugin.rest.controller.RestIterationController.ITPI_EMBEDDED_FILTER;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(TestSuite.class)
@UseDefaultRestApiConfiguration
public class RestTestSuiteController extends BaseRestController{

    public static final String TEST_SUITE_DYNAMIC_FILTER = "*, parent[name], -path,test_plan[referenced_test_case[name],referenced_dataset[name], execution_status]";

    @Inject
    private RestTestSuiteService restTestSuiteService;
    @Inject
    private TestSuiteValidator testSuiteValidator;

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private RestIssueService restIssueService;

    @GetMapping(value = "/test-suites/{id}")
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression(TEST_SUITE_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<TestSuite>> findTestSuite(@PathVariable("id") long id){

        TestSuite testSuite = restTestSuiteService.getOne(id);

        EntityModel<TestSuite> res = toEntityModel(testSuite);

        linksHelper.addAllLinksForTestSuite(res);
        res.add(createRelationTo("issues"));

        return ResponseEntity.ok(res);
    }

    @GetMapping("/test-suites/{id}/test-plan")
    @ResponseBody
    @DynamicFilterExpression(ITPI_EMBEDDED_FILTER)
    public ResponseEntity<PagedModel<EntityModel<IterationTestPlanItem>>> findIterationTestPlan(@PathVariable("id") long testSuiteId, Pageable pageable){

        Page<IterationTestPlanItem> steps = restTestSuiteService.findTestPlan(testSuiteId, pageable);

        PagedModel<EntityModel<IterationTestPlanItem>> res = toPagedResourcesWithRel(steps, "test-plan");

        return ResponseEntity.ok(res);

    }

    @PostMapping(value="/test-suites")
    @ResponseBody
    @DynamicFilterExpression(TEST_SUITE_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<TestSuite>> addTestSuite(@RequestBody TestSuiteDto testSuiteDto) throws BindException {

        testSuiteValidator.validatePostTestSuite(testSuiteDto);

        TestSuite testSuite =  restTestSuiteService.addTestSuite(testSuiteDto.getParent().getId(), testSuiteDto);

        EntityModel<TestSuite> res = toEntityModel(testSuite);

        linksHelper.addAllLinksForTestSuite(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    /*AMK modify Test Suite*/

    @PatchMapping(value = "/test-suites/{id}")
    @ResponseBody
    @DynamicFilterExpression(TEST_SUITE_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<TestSuite>> modifyTestSuite(@RequestBody TestSuiteDto testSuiteDto ,
                                                                   @PathVariable("id") Long testSuiteId)throws BindException{

        testSuiteValidator.validatePatchTestSuite(testSuiteDto,testSuiteId);

        TestSuite testSuite = restTestSuiteService.modifyTestSuite(testSuiteDto,testSuiteId);

        EntityModel<TestSuite> res = toEntityModel(testSuite);

        linksHelper.addAllLinksForTestSuite(res);

        return ResponseEntity.ok(res);
    }

    @DeleteMapping(value = "/test-suites/{ids}")
    public ResponseEntity<Void> deleteTestSuite(@PathVariable("ids") List<Long> testSuiteIds) {
        restTestSuiteService.deleteTestSuite(testSuiteIds);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(value = "/test-suites/{testSuiteId}/test-plan/{ids}")
    @ResponseBody
    @DynamicFilterExpression(TEST_SUITE_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<TestSuite>> detachItemFromTestSuite(@PathVariable("testSuiteId") Long testSuiteId,
                                                            @PathVariable("ids") List<Long> ids){


        TestSuite testSuite = restTestSuiteService.detachTestPlanFromTestSuite(testSuiteId,ids);

        EntityModel<TestSuite> res = toEntityModel(testSuite);

        linksHelper.addAllLinksForTestSuite(res);

        return ResponseEntity.ok(res);
    }

    @PostMapping(value = "/test-suites/{testSuiteId}/test-plan/{ids}")
    @ResponseBody
    @DynamicFilterExpression(TEST_SUITE_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<TestSuite>> attachTestPlanToTestSuite(@PathVariable("testSuiteId") Long testSuiteId,
                                                                            @PathVariable("ids") List<Long> ids){

        TestSuite testSuite = restTestSuiteService.attachTestPlanToTestSuite(testSuiteId,ids);

        EntityModel<TestSuite> res = toEntityModel(testSuite);

        linksHelper.addAllLinksForTestSuite(res);

        return ResponseEntity.ok(res);
    }

    @GetMapping("/test-suites/{id}/issues")
    @ResponseBody
    @DynamicFilterExpression("executions[id]")
    public ResponseEntity<PagedModel<EntityModel<IssueDto>>> findTestSuiteIssues(@PathVariable("id") long id, Pageable pageable) {

        List<Long> executionIds =  restTestSuiteService.getExecutionIdsByTestSuite(id);

        Page<IssueDto> pagedIssue = restIssueService.getIssuesFromExecutionIds(executionIds, pageable);

        PagedModel<EntityModel<IssueDto>> res = pageAssembler.toModel(pagedIssue);

        return ResponseEntity.ok(res);
    }

}
