/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.infolist.ListItemReference
import org.squashtest.tm.domain.requirement.ManagementMode
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementCriticality
import org.squashtest.tm.domain.requirement.RequirementFolder
import org.squashtest.tm.domain.requirement.RequirementStatus
import org.squashtest.tm.domain.requirement.RequirementSyncExtender
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.domain.resource.SimpleResource
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage

import static org.squashtest.tm.test.domainbuilder.PropertyHelper.append
import static org.squashtest.tm.test.domainbuilder.PropertyHelper.assign
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirement
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirementVersion

class RequirementExpandos {

	private RequirementExpandos(){
		super();
	}

    public static ManagementMode mode(ManagementMode mode) {
        return mode;
    }

    static class RequirementSyncExtenderExpando extends BaseExpando<RequirementSyncExtender> implements AuditableFeatures{

        RequirementSyncExtenderExpando(){
            super()
        }
    }


	static class RequirementFolderExpando extends BaseExpando<RequirementFolder> implements AuditableFeatures{

		RequirementFolderExpando(){
			super()

			id = 1l
			createdBy "admin"
			createdOn "2017/06/15"
			lastModifiedBy "admin"
			lastModifiedOn "2017/06/15"


		}

		@Override
		def create(){
			def folder = super.create()

			if (content){
				content.each{ folder.addContent (it) }
			}

			folder

		}


	}

	static class ResourceExpando extends BaseExpando<SimpleResource> implements AuditableFeatures {

        ResourceExpando() {
            super()

            id = 1l
            name = "resource"

			createdBy "admin"
			createdOn "2017/07/19"
			lastModifiedBy "admin"
			lastModifiedOn "2017/07/19"
        }
    }

	static class RequirementExpando extends BaseExpando<Requirement> {

		RequirementExpando(){
			super()

			id = 1l

		}

		// ********* creation override **************

		def withDefaultVersion(){
			this.withDefaultVersion = true
		}

		@Override
		def create(){
			Requirement req = super.create()

			// if requested, add a default version
			if (withDefaultVersion){
				def defaultVersion = requirementVersion({})
				assign defaultVersion, "versionNumber", 1
				append req, "versions", defaultVersion
			}

			// assign attributes 'resource' and 'mainResource'
			if (! req.versions.isEmpty()){
				def lastVersion = req.versions.last()
				assign req, "resource", lastVersion
				assign req, "mainResource", lastVersion
			}

			// wire the requirement versions to the requirement
			req.requirementVersions.each {
				assign it, "requirement", req
			}

			req

		}

		// ********** helpers ***********



	}


	/**
	 * The TestCaseExpando allows a special attributes :
	 *
	 *  <ul>
	 *  	<li>'testCases' : is an array of TestCase
	 * and will automatically wrap them in RequirementVersionCoverage when the requirement version is created </li>
	 *  </ul>
	 *
	 * @author bsiri
	 *
	 */
	static class RequirementVersionExpando extends BaseExpando<RequirementVersion> implements AuditableFeatures{

		RequirementVersionExpando(){
			super();

			id = 1l
			createdBy "admin"
			createdOn "2017/06/15"
			lastModifiedBy "admin"
			lastModifiedOn "2017/06/15"

			name = "sample requirement"
			reference = "REQ-1"
			description = "<p>a requirement</p>"

			category "CAT_FUNCTIONAL"
			criticality "MINOR"

		}

		// ********* creation override **************

		def withDefaultRequirement(){
			this.withDefaultRequirement = true
		}

		@Override
		def create(){
			def version = super.create()

			if (withDefaultRequirement){
				def req = requirement({})
				append req, "versions", version
			}

			// assign requirement.resource and mainResource
			version.requirement?.resource = version
			version.requirement?.mainResource = version

			// if declares some verifying test cases, wrap them as coverages
			this?.testCases.each {
				def cov = new RequirementVersionCoverage()
				append version, "requirementVersionCoverages", cov
				append it, "requirementVersionCoverages", cov
				cov.verifiedRequirementVersion = version
                cov.verifyingTestCase = it
			}


			version
		}

		// ********************* helpers *************************

		def category(String strcat){
			this.category = new ListItemReference(strcat)
		}

		def criticality(String strcrit) {
			this.criticality = RequirementCriticality.valueOf strcrit
		}

        def status(String strstat) {
            this.status = RequirementStatus.valueOf strstat
        }

	}
}
