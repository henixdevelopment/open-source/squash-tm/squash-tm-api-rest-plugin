/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.plugin.rest.jackson.serializer.AutomatedExecutionExtenderPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;

import java.net.URL;

@JsonAutoDetect(fieldVisibility=Visibility.NONE, getterVisibility=Visibility.NONE, isGetterVisibility=Visibility.NONE)
@JsonTypeName("automated-execution-extender")
@JsonAppend(props = @JsonAppend.Prop(name = "result_status", value = AutomatedExecutionExtenderPropertyWriter.class))
@JsonPropertyOrder({"_type", "id", "result_url", "result_summary", "result_status", "duration", "execution_node", "execution","automated_test"})
public abstract class RestAutomatedExecutionExtenderMixin extends RestIdentifiedMixin{

    @JsonProperty("automated_test")
    @JsonSerialize(converter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    AutomatedTest automatedTest;

    @JsonProperty
    @JsonSerialize(converter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
    Execution execution;

    @JsonProperty("result_url")
    URL resultURL;

    @JsonProperty("result_summary")
    String resultSummary;

    @JsonProperty("duration")
    Integer duration;

    @JsonProperty("execution_node")
    String nodeName;

}
