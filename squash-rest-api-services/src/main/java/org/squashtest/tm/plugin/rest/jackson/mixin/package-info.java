/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 * Note : this package contains the mixins that target the actual Squash entities. If you need to map a DTO, consider  
 * package org.squashtest.tm.plugin.rest.jackson.mixin.dto instead.
 * 
 *  
 * <p>Mixin Guidelines :</p>
 * 
 * <p> A mixin :
 * 
 * 	<ul>
 * 		<li>SHOULD extend {@link org.squashtest.tm.plugin.rest.jackson.mixin.RestIdentifiedMixin} for most entities. In that case, don't forget to append an entry to the list of JsonSubType there.</li>
 * 		<li>MUST be mapped in {@link org.squashtest.tm.plugin.rest.configuration.SquashRestApiJacksonModule}</li>
 * 		<li>
 * 			for serialization, MUST convert related entities as HATEOAS resource using {@link org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter}, 
 * 			either using {@link com.fasterxml.jackson.databind.annotation.JsonSerialize}#converter or #contentConverter, either with {@link org.squashtest.tm.plugin.rest.jackson.serializer.ChainConverter}.
 * 		</li>
 * 		<li>
 * 			for serialization and due to a bug, MUST redeclare on the property {@link com.fasterxml.jackson.annotation.JsonTypeInfo}#use = NONE if a converter (of any kind) is used. This will prevent double-inclusion 
 * 			of type identifier attribute _type, which happens because of a conjunction of : 
 * 				<ul>
 * 					<li>The use of a converter will make Jackson process the annotation {@link com.fasterxml.jackson.annotation.JsonTypeInfo} for the related entity in undesired ways, </li>
 * 					<li>the virtual property _type, declared in {@link org.squashtest.tm.plugin.rest.jackson.mixin.RestIdentifiedMixin}, kicks in too </li>
 * 					<li>{@link org.springframework.hateoas.Resource#getContent()} uses {@link com.fasterxml.jackson.annotation.JsonUnwrapped}, which causes both declaration of '_type' to be included.</li>
 * 				</ul>
 * 		</li>
 * 			for serialization, MUST convert using {@link org.squashtest.tm.plugin.rest.jackson.serializer.UnauthorizedResourcesConverter} if the related entity can belong to a different project (and thus be 
 * not accessible to the requestor).
 * 		<li>
 * 	</li>
 * </ul>
 * 
 * </p>
 * 
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

