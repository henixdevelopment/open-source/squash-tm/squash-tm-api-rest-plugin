/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.ExploratorySessionOverview;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.service.RestExploratoryExecutionService;
import org.squashtest.tm.plugin.rest.service.RestExploratorySessionService;
import org.squashtest.tm.plugin.rest.service.RestIssueService;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;

@RestApiController(ExploratorySessionOverview.class)
@UseDefaultRestApiConfiguration
public class RestExploratorySessionController extends BaseRestController {

    public static final String EXPLO_SESSION_DYNAMIC_FILTER = "*, iteration_test_plan_item[execution_status,created_on,created_by,last_modified_on,last_modified_by,executions[last_executed_on,assignee_user,task_division,reviewed]]";

    @Inject
    private RestExploratorySessionService service;

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private RestIssueService restIssueService;

    @Inject
    private RestExploratoryExecutionService exploratoryExecutionService;

    @GetMapping(value = "/exploratory-sessions/{id}")
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression(EXPLO_SESSION_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<ExploratorySessionOverview>> findExploratorySession(@PathVariable("id") long id) {

        ExploratorySessionOverview exploSessionOverview = service.getOne(id);

        EntityModel<ExploratorySessionOverview> res = toEntityModel(exploSessionOverview);

        linksHelper.addAllLinksForExploratorySessionOverview(res);
        res.add(createRelationTo( "issues"));

        return ResponseEntity.ok(res);
    }

    @GetMapping("/exploratory-sessions/{id}/issues")
    @ResponseBody
    @DynamicFilterExpression("exploratory-executions[id]")
    public ResponseEntity<PagedModel<EntityModel<IssueDto>>> findExploratorySessionIssues(@PathVariable("id") long id, Pageable pageable) {

        ExploratorySessionOverview sessionOverview = service.getOne(id);
        Page<ExploratoryExecution> exploratoryExecution = service.findExploratoryExecutionsByItpiId(sessionOverview.getIterationTestPlanItem().getId(), pageable);
        List<Long> exploratoryExecutionList = exploratoryExecution.getContent().stream().map(ExploratoryExecution::getId).toList();
        Page<IssueDto> pagedIssue = restIssueService.getIssuesFromExecutionIds(exploratoryExecutionList, pageable);

        for (IssueDto issue : pagedIssue.getContent()) {
            List<ExploratoryExecution> exploratoryExecutions = exploratoryExecutionService.getExploratoryExecutionFromIssues(issue.getRemoteIssueId(), exploratoryExecutionList);
            issue.setExploratoryExecutions(new HashSet<>(exploratoryExecutions));
            issue.getExecutions().clear();
        }

        PagedModel<EntityModel<IssueDto>> res = pageAssembler.toModel(pagedIssue);

        return ResponseEntity.ok(res);
    }
}
