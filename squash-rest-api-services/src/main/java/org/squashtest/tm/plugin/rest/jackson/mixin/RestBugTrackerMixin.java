/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.squashtest.tm.domain.bugtracker.BugTracker;

/**
 * This mixin is a stub, and it exists only because since Jackson 2.9, when it initializes
 * the BeanSerializers and BeanDeserializers, Jackson navigates all the relations of involved
 * entities even tough some are marked as ignored and/or not part of mixins.
 *
 * This is the case eg for executions, that navigates to the issues and then to the
 * {@link org.squashtest.csp.core.bugtracker.domain.BugTracker}. Problem is, we have a name
 * conflict in this class with {@link BugTracker#getUrl()} and {@link BugTracker#getURL()}
 * (duh). This leads to a crash due to the eager property navigation explained above.
 *
 * So, in order to prevent Jackson to crash we declare a mixin for BugTrackers anyway,
 * despite they aren't part of the rest API at the moment.
 *
 *
 */
@JsonAutoDetect(fieldVisibility= JsonAutoDetect.Visibility.NONE, getterVisibility= JsonAutoDetect.Visibility.NONE, isGetterVisibility= JsonAutoDetect.Visibility.NONE)
public interface RestBugTrackerMixin {

	// ignore especially that one
	@JsonIgnore
	BugTracker getURL();
}
