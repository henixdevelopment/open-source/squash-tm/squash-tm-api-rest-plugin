/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.service.RestIssueService;
import org.squashtest.tm.plugin.rest.service.RestSessionNoteService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@RestApiController(SessionNote.class)
@UseDefaultRestApiConfiguration
public class RestSessionNoteController extends BaseRestController {

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private RestSessionNoteService service;

    @Inject
    private RestIssueService restIssueService;

    @GetMapping(value = "/session-notes/{id}")
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression("*,-execution")
    public ResponseEntity<EntityModel<SessionNote>> findSessionNote(@PathVariable("id") long id){
        SessionNote note = service.getOne(id);
        EntityModel<SessionNote> res = toEntityModel(note);
        linksHelper.addAllLinksForSessionNote(res);
        res.add(createRelationTo( "issues"));

        return ResponseEntity.ok(res);
    }

    @GetMapping("/session-notes/{id}/issues")
    @ResponseBody
    @DynamicFilterExpression("id")
    public ResponseEntity<PagedModel<EntityModel<IssueDto>>> finSessionNotesIssues(@PathVariable("id") long id, Pageable pageable) {

        SessionNote note = service.getOne(id);
        if (note == null) {
            throw new IllegalArgumentException("Session note with id " + id + " does not exist");
        }

        ExploratoryExecution exploratoryExecution = service.getExecutionFromSessionNote(id);
        Long exploratoryExecutionId = exploratoryExecution.getId();

        Page<IssueDto> pagedIssue = restIssueService.getIssuesFromExecutionIds(Collections.singletonList(exploratoryExecutionId), pageable);

        List<IssueDto> filteredIssues = new ArrayList<>();
        for (IssueDto issue : pagedIssue.getContent()) {
            List<SessionNote> sessionNotes = service.getSessionNotesFromIssue(issue.getRemoteIssueId(), exploratoryExecutionId);
            issue.setSessionNotes(new HashSet<>(sessionNotes));

            if(sessionNotes.stream().anyMatch(session -> session.getId() == id)) {
                filteredIssues.add(issue);
                issue.setSessionNotes(new HashSet<>());
            }

            issue.getExecutions().clear();
        }

        Page<IssueDto> pagedIssueToReturn = new PageImpl<>(filteredIssues, pageable, filteredIssues.size());

        PagedModel<EntityModel<IssueDto>> res = pageAssembler.toModel(pagedIssueToReturn);

        return ResponseEntity.ok(res);
    }

}
























