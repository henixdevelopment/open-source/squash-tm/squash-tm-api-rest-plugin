/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;


import org.springframework.hateoas.server.mvc.RepresentationModelProcessorHandlerMethodReturnValueHandler;
import org.springframework.hateoas.server.mvc.RepresentationModelProcessorInvoker;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.HandlerMethodReturnValueHandlerComposite;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
/* *
* This class doesnt exit anymore with hateoas update, the alternative wasn't documented,we were forced to duplicate the source code of remove class.
* see: https://jar-download.com/artifacts/org.springframework.hateoas/spring-hateoas/0.21.0.RELEASE/source-code/org/springframework/hateoas/mvc/ResourceProcessorInvokingHandlerAdapter.java
* see: https://javadoc.io/doc/org.springframework.hateoas/spring-hateoas/1.4.1/org/springframework/hateoas/server/RepresentationModelProcessor.html
* */
public class RepresentationModelProcessorInvokingHandlerAdapter extends RequestMappingHandlerAdapter {

  private static final Method RETURN_VALUE_HANDLER_METHOD = ReflectionUtils
      .findMethod(RepresentationModelProcessorInvokingHandlerAdapter.class, "getReturnValueHandlers");

  private final RepresentationModelProcessorInvoker invoker;

  public RepresentationModelProcessorInvokingHandlerAdapter(RepresentationModelProcessorInvoker invoker) {
    this.invoker = invoker;
  }

  @Override
  public void afterPropertiesSet() {

    super.afterPropertiesSet();

    // Retrieve actual handlers to use as delegate
    HandlerMethodReturnValueHandlerComposite oldHandlers = getReturnValueHandlersComposite();

    // Set up ResourceProcessingHandlerMethodResolver to delegate to originally configured ones
    List<HandlerMethodReturnValueHandler> newHandlers = new ArrayList<>();
    newHandlers.add(new RepresentationModelProcessorHandlerMethodReturnValueHandler(oldHandlers, () -> invoker));

    // Configure the new handler to be used
    this.setReturnValueHandlers(newHandlers);
  }

  /**
   * Gets a {@link HandlerMethodReturnValueHandlerComposite} for return handlers, dealing with API changes introduced in
   * Spring 4.0.
   *
   * @return a HandlerMethodReturnValueHandlerComposite
   */
  @SuppressWarnings("unchecked")
  private HandlerMethodReturnValueHandlerComposite getReturnValueHandlersComposite() {

    Object handlers = ReflectionUtils.invokeMethod(RETURN_VALUE_HANDLER_METHOD, this);

    if (handlers instanceof HandlerMethodReturnValueHandlerComposite) {
      return (HandlerMethodReturnValueHandlerComposite) handlers;
    }

    return new HandlerMethodReturnValueHandlerComposite()
        .addHandlers((List<HandlerMethodReturnValueHandler>) handlers);
  }
}
