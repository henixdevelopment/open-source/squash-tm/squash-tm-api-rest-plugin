/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.squashtest.tm.domain.bdd.ActionWord
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestActionWordService

import javax.inject.Inject

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.patch
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWord
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWordParameter
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionWordText
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project

@WebMvcTest(RestActionWordController)
class RestActionWordControllerIT extends BaseControllerSpec {

    @Inject
    private RestActionWordService restActionWordService

    def "get-action-word"() {
        given:
        def projectMock = project {
            id = 6L
            name = "my project"
        }
        def actionWord = actionWord {
            id = 721L
            description = "Action word describing the color of an icon."
            lastImplementationTechnology "ROBOT"
            lastImplementationDate "2020/09/06 11:35:47"
            createdBy "Arthur"
            createdOn "2020/08/15 09:48:02"
            lastModifiedBy "Arthur"
            lastModifiedOn "2020/08/15 09:53:11"
            lastModifiedBy "Arthur"
            project = projectMock
            fragments = [
                    actionWordText {
                        text = "The color of the icon "
                    },
                    actionWordParameter {
                        id = 3L
                        name = "iconName"
                        defaultValue = "home"
                    },
                    actionWordText {
                        text = " is "
                    },
                    actionWordParameter {
                        id = 4L
                        name = "color"
                        defaultValue = "green"
                    },
                    actionWordText {
                        text = "."
                    }
            ]

        }
        and:
        def testCases = [
                keywordTestCase {
                    id = 476L
                    reference = "UX-17"
                    name = "User interface icons colors"
                    importance "MEDIUM"
                    status "APPROVED"
                    // automationStatus "AUTOMATED"
                    project = projectMock
                }
        ]
        and:
        restActionWordService.getOne(721) >> actionWord
        restActionWordService.findAllByActionWord(721) >> testCases
        when:
        def res = mockMvc.perform(
                get("/api/rest/latest/action-words/{id}", 721)
                        .header("Accept", "application/json"))
        then:
        res
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
        withResult(res) {
            "_type".is "action-word"
            "id".is 721
            "word".is "The color of the icon \"iconName\" is \"color\"."
            "description".is "Action word describing the color of an icon."
            "last_implementation_technology".is "ROBOT"
            "last_implementation_date".is "2020-09-06T09:35:47.000+00:00"
            "created_by".is "Arthur"
            "created_on".is "2020-08-15T07:48:02.000+00:00"
            "last_modified_by".is "Arthur"
            "last_modified_on".is "2020-08-15T07:53:11.000+00:00"
            "project".test {
                "_type".is "project"
                "id".is 6
                "name".is "my project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/6"
            }
            "parameters".hasSize 2
            "parameters".test {
                "[0]".test {
                    "_type".is "action-word-parameter"
                    "id".is 3
                    "name".is "iconName"
                    "default_value".is "home"
                }
                "[1]".test {
                    "_type".is "action-word-parameter"
                    "id".is 4
                    "name".is "color"
                    "default_value".is "green"
                }
            }
            "test_cases".hasSize 1
            "test_cases".test {
                "[0]".test {
                    "_type".is "keyword-test-case"
                    "id".is 476
                    "reference".is "UX-17"
                    "name".is "User interface icons colors"
                    "importance".is "MEDIUM"
                    "status".is "APPROVED"
                    // "automation_status".is "AUTOMATED"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/action-words/721"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/6"
            }
        }
        res.andDo(
                doc.document(
                        documentationBuilder {
                            requestParams {
                                add DescriptorLists.fieldsParams
                            }
                            fields {
                                add "_type (string) : the type of the entity"
                                add "id (number) : the id of the action word"
                                add "word (string) : the action itself with its parameters"
                                add "description (string) : the description of the action word"
                                add "last_implementation_technology (string) : the automation framework used for the last implementation of the action word"
                                add "last_implementation_date (string) : the date when the action word was last implemented"
                                add "created_by (string) : the user who created this action word"
                                add "created_on (string) : the date the action word was created"
                                add "last_modified_by (string) : the user who last modified the action word"
                                add "last_modified_on (string) : the date the action word was last modified"
                                addAndStop "project (object) : the project the action word belongs to"
                                addAndStop "parameters (array) : the parameters of the action word"
                                addAndStop "test_cases (array) : the test cases which use this action word"
                                add DescriptorLists.linksFields
                            }
                            _links {
                                add "self : link to the action word"
                                add "project : link to the project of the action word"
                            }
                        }))
    }

    def "get-project-action-words"() {
        given:
        restActionWordService.findAllActionWordsByProject(_, _) >> { args ->
            def actionWord1 = actionWord {
                id = 121L
                fragments = [
                        actionWordText {
                            text = "I have "
                        },
                        actionWordParameter {
                            name = "number"
                        },
                        actionWordText {
                            text = " oranges in my basket"
                        }
                ]
            }
            def actionWord2 = actionWord {
                id = 122L
                fragments = [
                        actionWordText {
                            text = "I eat "
                        },
                        actionWordParameter {
                            name = "number"
                        },
                        actionWordText {
                            text = " oranges"
                        }
                ]
            }
            def actionWord3 = actionWord {
                id = 124L
                fragments = [
                        actionWordText {
                            text = "There are "
                        },
                        actionWordParameter {
                            name = "number"
                        },
                        actionWordText {
                            text = " oranges left in my basket"
                        }
                ]
            }
            new PageImpl<ActionWord>([actionWord1, actionWord2, actionWord3], args[1], 10)
        }
        when:
        def res = mockMvc.perform(
                get("/api/rest/latest/projects/{id}/action-words?page=2&size=3", 14)
                        .header("Accept", "application/json"))
        then:
        res
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
        withResult(res) {
            "_embedded.action-words".hasSize 3
            "_embedded.action-words".test {
                "[0]".test {
                    "_type".is "action-word"
                    "id".is 121
                    "word".is "I have \"number\" oranges in my basket"
                    selfRelIs "http://localhost:8080/api/rest/latest/action-words/121"
                }
                "[1]".test {
                    "_type".is "action-word"
                    "id".is 122
                    "word".is "I eat \"number\" oranges"
                    selfRelIs "http://localhost:8080/api/rest/latest/action-words/122"
                }
                "[2]".test {
                    "_type".is "action-word"
                    "id".is 124
                    "word".is "There are \"number\" oranges left in my basket"
                    selfRelIs "http://localhost:8080/api/rest/latest/action-words/124"
                }
            }
            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 2
            }
            "_links".test {
                "first".linksTo "http://localhost:8080/api/rest/latest/projects/14/action-words?page=0&size=3"
                "prev".linksTo "http://localhost:8080/api/rest/latest/projects/14/action-words?page=1&size=3"
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/14/action-words?page=2&size=3"
                "next".linksTo "http://localhost:8080/api/rest/latest/projects/14/action-words?page=3&size=3"
                "last".linksTo "http://localhost:8080/api/rest/latest/projects/14/action-words?page=3&size=3"
            }
        }
        res.andDo(
                doc.document(
                        DocumentationSnippets.AllInOne.createBrowseAllEntities("action-words")))
    }

    def "get-all-action-words"() {
        given:
        restActionWordService.findAllReadableActionWords(_) >> { args ->
            def actionWord1 = actionWord {
                id = 87L
                fragments = [
                        actionWordText {
                            text = "I write "
                        },
                        actionWordParameter {
                            name = "login"
                        },
                        actionWordText {
                            text = " in login"
                        }
                ]
            }
            def actionWord2 = actionWord {
                id = 88L
                fragments = [
                        actionWordText {
                            text = "I write "
                        },
                        actionWordParameter {
                            name = "password"
                        },
                        actionWordText {
                            text = " in password"
                        }
                ]
            }
            def actionWord3 = actionWord {
                id = 89L
                fragments = [
                        actionWordText {
                            text = "I log in"
                        }
                ]
            }
            new PageImpl<ActionWord>([actionWord1, actionWord2, actionWord3], args[0], 10)
        }
        when:
        def res = mockMvc.perform(
                get("/api/rest/latest/action-words?page=1&size=3")
                        .header("Accept", "application/json"))
        then:
        res
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
        withResult(res) {
            "_embedded.action-words".hasSize 3
            "_embedded.action-words".test {
                "[0]".test {
                    "_type".is "action-word"
                    "id".is 87
                    "word".is "I write \"login\" in login"
                    selfRelIs "http://localhost:8080/api/rest/latest/action-words/87"
                }
                "[1]".test {
                    "_type".is "action-word"
                    "id".is 88
                    "word".is "I write \"password\" in password"
                    selfRelIs "http://localhost:8080/api/rest/latest/action-words/88"
                }
                "[2]".test {
                    "_type".is "action-word"
                    "id".is 89
                    "word".is "I log in"
                    selfRelIs "http://localhost:8080/api/rest/latest/action-words/89"
                }
            }
            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 1
            }
            "_links".test {
                "first".linksTo "http://localhost:8080/api/rest/latest/action-words?page=0&size=3"
                "prev".linksTo "http://localhost:8080/api/rest/latest/action-words?page=0&size=3"
                "self".linksTo "http://localhost:8080/api/rest/latest/action-words?page=1&size=3"
                "next".linksTo "http://localhost:8080/api/rest/latest/action-words?page=2&size=3"
                "last".linksTo "http://localhost:8080/api/rest/latest/action-words?page=3&size=3"
            }
        }
        res.andDo(
                doc.document(
                        DocumentationSnippets.AllInOne.createBrowseAllEntities("action-words")))
    }

    def "patch-action-word"() {
        given:
        def json =
                """{
                    "_type" : "action-word",
                    "description" : "Action word describing the number of icons."
                }"""
        and:
        def projectMock = project {
            id = 6L
            name = "my project"
        }
        def actionWord = actionWord {
            id = 721L
            fragments = [
                    actionWordText {
                        text = "There are "
                    },
                    actionWordParameter {
                        id = 3L
                        name = "iconCount"
                        defaultValue = "5"
                    },
                    actionWordText {
                        text = " displayed."
                    }
            ]
            description = "Action word describing the number of icons."
            project = projectMock
            lastImplementationTechnology "ROBOT"
            lastImplementationDate "2020/09/06 11:35:47"
            createdBy "Arthur"
            createdOn "2020/08/15 09:48:02"
            lastModifiedBy "Arthur"
            lastModifiedOn "2020/09/02 14:36:17"
        }
        def testCases = [
                keywordTestCase {
                    id = 476L
                    reference = "UX-17"
                    name = "User interface icons colors"
                    importance "MEDIUM"
                    status "APPROVED"
                    // automationStatus "AUTOMATED"
                    project = projectMock
                }
        ]
        and:
        restActionWordService.patchActionWord(721L, _) >> actionWord
        restActionWordService.findAllByActionWord(721L) >> testCases
        when:
        def res = mockMvc.perform(
                patch("/api/rest/latest/action-words/{id}", 721L)
                        .accept("application/json")
                        .contentType("application/json")
                        .content(json))
        then:
        res
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
        withResult(res) {
            "_type".is "action-word"
            "id".is 721
            "word".is "There are \"iconCount\" displayed."
            "description".is "Action word describing the number of icons."
            "last_implementation_technology".is "ROBOT"
            "last_implementation_date".is "2020-09-06T09:35:47.000+00:00"
            "created_by".is "Arthur"
            "created_on".is "2020-08-15T07:48:02.000+00:00"
            "last_modified_by".is "Arthur"
            "last_modified_on".is "2020-09-02T12:36:17.000+00:00"
            "project".test {
                "_type".is "project"
                "id".is 6
                "name".is "my project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/6"
            }
            "parameters".hasSize 1
            "parameters".test {
                "[0]".test {
                    "_type".is "action-word-parameter"
                    "id".is 3
                    "name".is "iconCount"
                    "default_value".is "5"
                }
            }
            "test_cases".hasSize 1
            "test_cases".test {
                "[0]".test {
                    "_type".is "keyword-test-case"
                    "id".is 476
                    "reference".is "UX-17"
                    "name".is "User interface icons colors"
                    "importance".is "MEDIUM"
                    "status".is "APPROVED"
                    // "automation_status".is "AUTOMATED"
                }
            }
            selfRelIs"http://localhost:8080/api/rest/latest/action-words/721"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/6"
            // "parameters.href".is "http://localhost:8080/api/rest/latest/action-words/721/parameters"
            // "test-cases.href".is "http://localhost:8080/api/rest/latest/action-words/721/test-cases"
            }
        }
        res.andDo(
                doc.document(
                        documentationBuilder {
                            requestParams {
                                add DescriptorLists.fieldsParams
                            }
                            requestFields {
                                add "_type (string) : the type of the entity"
                                add "description (string) : the description of the action word"
                            }
                            fields {
                                add "_type (string) : the type of the entity"
                                add "id (number) : the id of the action word"
                                add "word (string) : the action itself with its parameters"
                                add "description (string) : the description of the action word"
                                add "last_implementation_technology (string) : the automation framework used for the last implementation of the action word"
                                add "last_implementation_date (string) : the date when the action word was last implemented"
                                add "created_by (string) : the user who created this action word"
                                add "created_on (string) : the date the action word was created"
                                add "last_modified_by (string) : the user who last modified the action word"
                                add "last_modified_on (string) : the date the action word was last modified"
                                addAndStop "project (object) : the project the action word belongs to"
                                addAndStop "parameters (array) : the parameters of the action word"
                                addAndStop "test_cases (array) : the test cases which use this action word"
                                add DescriptorLists.linksFields
                            }
                            _links {
                                add "self : link to the action word"
                                add "project : link to the project of the action word"
                            }
                        }))
    }

    def "post-action-word"() {
        given:
        def json =
"""{
    "_type" : "action-word",
    "word" : "The color of the icon \\\"iconName\\\" is \\\"color\\\".",
    "description" : "Action word describing the number of icons.",
    "project" : {
        "_type" : "project",
        "id" : 6
    }
}"""
        and:
        def projectMock = project {
            id = 6L
            name = "my project"
        }
        def actionWord = actionWord {
            id = 721L
            description = "Action word describing the color of an icon."
            createdBy "Arthur"
            createdOn "2020/08/15 09:48:02"
            lastModifiedBy "Arthur"
            lastModifiedOn "2020/08/15 09:48:03"
            lastModifiedBy "Arthur"
            project = projectMock
            fragments = [
                    actionWordText {
                        text = "The color of the icon "
                    },
                    actionWordParameter {
                        id = 3L
                        name = "iconName"
                        defaultValue = "home"
                    },
                    actionWordText {
                        text = " is "
                    },
                    actionWordParameter {
                        id = 4L
                        name = "color"
                        defaultValue = "green"
                    },
                    actionWordText {
                        text = "."
                    }
            ]

        }
        and:
        restActionWordService.createActionWord(_) >> actionWord
        restActionWordService.findAllByActionWord(721) >> []
        when:
        def res = mockMvc.perform(
                post("/api/rest/latest/action-words")
                        .accept("application/json")
                        .contentType("application/json")
                        .content(json))
        then:
        res
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
        withResult(res) {
            "_type".is "action-word"
            "id".is 721
            "word".is "The color of the icon \"iconName\" is \"color\"."
            "description".is "Action word describing the color of an icon."
            "created_by".is "Arthur"
            "created_on".is "2020-08-15T07:48:02.000+00:00"
            "project".test {
                "_type".is "project"
                "id".is 6
                "name".is "my project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/6"
            }
            "parameters".hasSize 2
            "parameters".test {
                "[0]".test {
                    "_type".is "action-word-parameter"
                    "id".is 3
                    "name".is "iconName"
                    "default_value".is "home"
                }
                "[1]".test {
                    "_type".is "action-word-parameter"
                    "id".is 4
                    "name".is "color"
                    "default_value".is "green"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/action-words/721"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/6"
                // "parameters.href".is "http://localhost:8080/api/rest/latest/action-words/721/parameters"
                // "test-cases.href".is "http://localhost:8080/api/rest/latest/action-words/721/test-cases"
            }
        }
        res.andDo(
                doc.document(
                        documentationBuilder {
                            requestParams {
                                add DescriptorLists.fieldsParams
                            }
                            requestFields {
                                add "_type (string) : the type of the entity"
                                add "description (string) : the description of the action word"
                                add "word (string) : the action itself"
                                addAndStop "project (object) : the parent project of the action word"
                            }
                            fields {
                                add "_type (string) : the type of the entity"
                                add "id (number) : the id of the action word"
                                add "word (string) : the action itself with its parameters"
                                add "description (string) : the description of the action word"
                                add "created_by (string) : the user who created this action word"
                                add "created_on (string) : the date the action word was created"
                                addAndStop "project (object) : the project the action word belongs to"
                                addAndStop "parameters (array) : the parameters of the action word"
                                add DescriptorLists.linksFields
                            }
                            _links {
                                add "self : link to the action word"
                                add "project : link to the project of the action word"
                            }
                        }))
    }

    def "delete-action-words"() {
        given:
            1 * restActionWordService.deleteActionWordsByIds(_) >> [14L, 278L]

        when:
            def res = mockMvc.perform(
                    delete("/api/rest/latest/action-words/{ids}", "14,38,278,1247")
                    .accept("application/json")
                    .contentType("application/json"))
        then:
            res.andExpect(status().isOk())
            withResult(res) {
            }
            /* Documentation */
            res.andDo(doc.document(
                    documentationBuilder {
                        pathParams {
                            add "ids : the list of ids of the action words"
                        }
                    }
            ))
    }

}
