/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.core.foundation.lang.Couple;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.DatasetParamValue;
import org.squashtest.tm.domain.testcase.Parameter;

import java.util.List;

/**
 * Created by jthebault on 30/05/2017.
 */
public interface RestDatasetRepository extends JpaRepository<Dataset,Long>{

    @Query("from Dataset d inner join fetch d.testCase where d.id = :id")
    Dataset retrieveById(@Param("id") Long id);

    Page<Dataset> findByTestCase_Id(long testCaseId, Pageable pageable);
    @Query("Select new org.squashtest.tm.core.foundation.lang.Couple(param.id, tc.id) From Parameter param"
            + " inner join param.testCase tc"
            + " Where tc.id in (:listIds)")
    List<Couple<Long,Long>> findIdTcIdParam(@Param("listIds") List<Long> listIds);

    @Query("Select dpv From DatasetParamValue dpv "
            + " Where dpv.dataset.id = :idDataset ")
    List<DatasetParamValue> findParamValueByIdDataset(@Param("idDataset") Long idDataset);

    @Query("Select p From Parameter p "
            + " Where p.testCase.id in(:tcIds) ")
    List<Parameter> findParameterByIdTc (@Param("tcIds") List<Long> tcIds);

    @Query("select distinct called.calledTestCase.id from CallTestStep called "
            +   " where called.delegateParameterValues=true and called.calledTestCase.id in (:testCaseIds)")
    List<Long> findTestCaseDeleguetePram(@Param("testCaseIds") List<Long> testCaseIds);


}
