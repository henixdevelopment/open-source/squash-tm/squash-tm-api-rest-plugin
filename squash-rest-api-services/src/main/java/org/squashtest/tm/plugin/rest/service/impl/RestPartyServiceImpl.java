/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.ProjectPermission;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.PartyVisitor;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.exception.IllegalPremiumFeatureAccessException;
import org.squashtest.tm.plugin.rest.jackson.model.PartyDto;
import org.squashtest.tm.plugin.rest.jackson.model.PartyDtoVisitor;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyClearance;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyProfileDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestUserPermission;
import org.squashtest.tm.plugin.rest.jackson.model.TeamDto;
import org.squashtest.tm.plugin.rest.jackson.model.UserDto;
import org.squashtest.tm.plugin.rest.repository.RestPartyRepository;
import org.squashtest.tm.plugin.rest.repository.RestTeamRepository;
import org.squashtest.tm.plugin.rest.repository.RestUserRepository;
import org.squashtest.tm.plugin.rest.service.RestPartyService;
import org.squashtest.tm.service.plugin.PluginFinderService;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.project.ProjectsPermissionFinder;
import org.squashtest.tm.service.project.ProjectsPermissionManagementService;
import org.squashtest.tm.service.security.acls.model.ObjectAclService;
import org.squashtest.tm.service.user.TeamModificationService;
import org.squashtest.tm.service.user.UserAdministrationService;
import org.squashtest.tm.service.user.UserManagerService;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.squashtest.tm.jooq.domain.Tables.CORE_USER;
import static org.squashtest.tm.service.security.Authorizations.HAS_ROLE_ADMIN;

@Service
@Transactional
public class RestPartyServiceImpl implements RestPartyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestPartyServiceImpl.class);

    @Inject
    private UserManagerService userManagerService;

    @Inject
    private TeamModificationService teamModificationService;

    @Inject
    private GenericProjectManagerService projectManagerService;

    @Inject
    private ObjectAclService aclService;

    @Inject
    private RestTeamRepository teamDao;

    @Inject
    private RestUserRepository userDao;
    @Inject private DSLContext dslContext;
    @Inject private UserAdministrationService userAdministrationService;

    @Inject
    private RestPartyRepository partyRepository;

    @Inject
    private UserPatcher userPatcher;

    @Inject
    private ProjectsPermissionManagementService permissionService;

    @Inject
    private ProjectsPermissionFinder permissionFinder;

    @Inject
    private PluginFinderService pluginFinderService;

    private static final String NAMESPACE = "squashtest.acl.group.tm.";

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @Transactional(readOnly = true)
    public Team findTeamByPartyId(long id) {
        return (Team) findById(id);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    @Transactional(readOnly = true)
    public User findUserByPartyId(long id) {
        return (User) findById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Party findById(long id) {
        Optional<Party> party = partyRepository.findById(id);
        if (!party.isPresent()) {
            throw new EntityNotFoundException("The user/team with id: " + id + " does not exist.");
        }

        final Party[] result = new Party[1];
        PartyVisitor visitor = new PartyVisitor() {
            @Override
            public void visit(User user) {
                result[0] = user;
            }

            @Override
            public void visit(Team team) {
                result[0] = team;
            }
        };
        party.get().accept(visitor);

        return result[0];
    }

    @Override
    @Transactional(readOnly = true)
    public List<Party> findByIds(List<Long> partyIds) {
        return
            partyRepository.findAllById(partyIds)
            .stream()
            .map(party -> {
                final Party[] result = new Party[1];
                PartyVisitor visitor = new PartyVisitor() {
                    @Override
                    public void visit(User user) {
                        result[0] = user;
                    }

                    @Override
                    public void visit(Team team) {
                        result[0] = team;
                    }
                };
                party.accept(visitor);
                return result[0];
            })
            .toList();
    }

    @Override
    @Transactional(readOnly=true)
    public Party getOne(long id) {
        Party party = projectManagerService.findPartyById(id);
        if(party == null){
            throw new EntityNotFoundException("The user/team with id: " + id + " does not exist.");
        }
        return party;
    }

    @Override
    public User findUserByLogin(String login) {
        return userManagerService.findByLogin(login);
    }

    @Override
    @PostAuthorize(HAS_ROLE_ADMIN)
    public Page<User> findAllUsers(Pageable pageable) {
        return userDao.findAll(pageable);
    }

    @Override
    @PostAuthorize(HAS_ROLE_ADMIN)
    public Page<Team> findAllTeams(Pageable pageable) {
        return teamDao.findAll(pageable);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public Page<Team> findTeamsByUser(long userId, Pageable pageable) {
        return teamDao.findTeamsByUserId(userId, pageable);
    }

    @Override
    public Party createParty(PartyDto partyDto) {
        final Party[] parties = new Party[1];
        PartyDtoVisitor visitor = new PartyDtoVisitor() {
            @Override
            public void visit(UserDto userDto) {
                User user = new User();

                if(userDto.getFirstName() != null) user.setFirstName(userDto.getFirstName());
                user.setLastName(userDto.getLastName());
                user.setLogin(userDto.getLogin());
                if(userDto.getEmail() != null) user.setEmail(userDto.getEmail());
                user.setActive(userDto.getActive());
                userManagerService.addUser(user, userDto.getGroupId(), userDto.getPassword());
                if(userDto.getCanDeleteFromFront() != null) {
                    assertPremiumPluginIsInstalled();
                    user.setCanDeleteFromFront(userDto.getCanDeleteFromFront());
                }
                parties[0] = user;
            }

            @Override
            public void visit(TeamDto teamDto) {
                Team team = new Team();
                team.setName(teamDto.getName());
                team.setDescription(teamDto.getDescription());
                teamModificationService.persist(team);
                parties[0] = team;
            }
        };

        partyDto.accept(visitor);
        return parties[0];
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public Party patchParty(PartyDto patch, long id) {
        Party party = findById(id);

        PartyDtoVisitor visitor = new PartyDtoVisitor() {
            @Override
            public void visit(UserDto userDto) {
                if(userDto.getPassword() != null){
                    userManagerService.resetUserPassword(id, userDto.getPassword());
                }
                if(userDto.getLogin() != null){
                    userManagerService.modifyUserLogin(id, userDto.getLogin());
                }
                if(userDto.getGroupId() != null){
                    userManagerService.setUserGroupAuthority(id, userDto.getGroupId());
                }
                if (userDto.getActive() != null) {
                    activateOrDeactivateUser(id, userDto.getActive());
                }
                if (userDto.getCanDeleteFromFront() != null) {
                    assertPremiumPluginIsInstalled();
                    userManagerService.changeCanDeleteFromFront(id, userDto.getCanDeleteFromFront());
                }
                userPatcher.patch(party, userDto);
            }

            @Override
            public void visit(TeamDto teamDto) {
                if (teamDto.getName() != null) {
                    teamModificationService.changeName(id, teamDto.getName());
                }
                if(teamDto.getDescription()!= null) {
                    teamModificationService.changeDescription(id, teamDto.getDescription());
                }
            }
        };

        patch.accept(visitor);

        return party;
    }

    @Override
    public void deleteUsers(List<Long> ids) {
        userManagerService.deleteUsers(ids);
    }

    @Override
    public void deleteTeams(List<Long> ids) {
        teamModificationService.deleteTeam(ids);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public Page<User> findAllTeamMembers(long teamId, Pageable pageable) {
        return userDao.findMembersByTeamId(teamId, pageable);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void addMembersToTeam(long teamId, List<Long> userIds) {
        List<String> userLogins = dslContext.select(CORE_USER.LOGIN)
                .from(CORE_USER)
                .where(CORE_USER.PARTY_ID.in(userIds))
                .fetchInto(String.class);
        teamModificationService.addMembers(teamId, userLogins);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void removeMembersFromTeam(long teamId, List<Long> userIds) {
        try {
            Team team = (Team) findById(teamId);
            teamModificationService.removeMembers(teamId, userIds);
        } catch (ClassCastException e){
            throw new EntityNotFoundException("The team with id: " + teamId + " does not exist.");
        }

    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public Set<String> addTeamsToUser(long userId, List<Long> teamIds) {
        return userAdministrationService.associateToTeams(userId, teamIds);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void disassociateUserFromTeams(long userId, List<Long> teamIds) {
        try {
           userManagerService.deassociateTeams(userId, teamIds);
        } catch (ClassCastException e){
            throw new EntityNotFoundException("The user with id: " + userId + " does not exist.");
        }

    }

    @Override
    @PostAuthorize(HAS_ROLE_ADMIN)
    public RestPartyClearance getAllClearancesForGivenUser(long userId) {

        List<ProjectPermission> projectPermissions = this.permissionFinder.findProjectPermissionByParty(userId);

        if (projectPermissions.isEmpty()) {
            throw new EntityNotFoundException("The user with id: " + userId + " does not have any permissions.");
        }
        return getUserClearances(projectPermissions);
    }

    @Override
    @PostAuthorize(HAS_ROLE_ADMIN)
    public RestUserPermission getAllPermissionsForGivenUser(long userId) {

        List<ProjectPermission> projectPermissions = this.permissionFinder.findProjectPermissionByParty(userId);

        if(projectPermissions.isEmpty()){
            throw new EntityNotFoundException("The user with id: " + userId + " does not have any permissions.");
        }
        return getUserPermissions(projectPermissions);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void addClearanceToUser(long userId, List<Long> projectIds, long profileId) {
        if (!ifUserExist(userId)) {
            throw new EntityNotFoundException("The user with id: " + userId + " does not exist.");
        }

        permissionService.addNewPermissionToProject(userId, projectIds, profileId);
    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void addPermissionToUser(long userId, List<Long> projectIds, String permission) {

        if(!ifUserExist(userId)){
            throw new EntityNotFoundException("The user with id: " + userId + " does not exist.");
        }

        try {
            permissionService.addNewPermissionToProject(userId,projectIds,NAMESPACE + permission);
        } catch (RuntimeException e){
            LOGGER.debug("An error occurred trying to add permission {} to user {} on following project ids : {}", permission, userId, projectIds, e);
            throw new EntityNotFoundException(e.getMessage());
        }

    }

    @Override
    @PreAuthorize(HAS_ROLE_ADMIN)
    public void removePermissionToUser(long userId, List<Long> projectIds) {

        if(!ifUserExist(userId)){
            throw new EntityNotFoundException("The user with id: " + userId + " does not exist.");
        }

        try {
            permissionService.removeProjectPermission(userId, projectIds);

        } catch (RuntimeException e){
            LOGGER.debug("An error occurred trying to remove permission to user {} on following project ids : {}", userId, projectIds, e);
            throw new EntityNotFoundException(e.getMessage());
        }

    }

    private RestPartyClearance getUserClearances(List<ProjectPermission> projectPermissions) {
        RestPartyClearance clearances = new RestPartyClearance();

        for (ProjectPermission projPermission : projectPermissions) {
            AclGroup profile = projPermission.getPermissionGroup();
            String profileName = profile.getSimpleName();

            // why do we name advanceTester instead of advancedTester?? Then this is what you got for translation
            if ("advanceTester".equalsIgnoreCase(profileName)) {
                profileName = "advancedTester";
            }

            GenericProject currentProject = projPermission.getProject();
            RestPartyProfileDto targetList = clearances.getPartyClearances().get(profileName);

            if (targetList == null) {
                long profileId = profile.getId();
                boolean isSystem = profile.isSystem();
                List<GenericProject> projects = new ArrayList<>();
                projects.add(currentProject);
                targetList = new RestPartyProfileDto(projects, profileId, profileName, isSystem);

                clearances.getPartyClearances().put(profileName, targetList);
            } else {
                targetList.getProjects().add(currentProject);
            }

        }
        return clearances;
    }

    private RestUserPermission getUserPermissions(List<ProjectPermission> projectPermissions) {
        RestUserPermission userPermissions = new RestUserPermission();

        for (ProjectPermission projPermission : projectPermissions) {
            String groupName =  projPermission.getPermissionGroup().getSimpleName();

            // why do we name advanceTester instead of advancedTester?? Then this is what you got for translation
            if("advanceTester".equalsIgnoreCase(groupName)){
                groupName = "advancedTester";
            }

            GenericProject currentProject = projPermission.getProject();
            List<GenericProject> targetList = userPermissions.getUserPermissions().get(groupName);

            if (targetList == null) {
                targetList = new ArrayList<>();
                targetList.add(currentProject);
                userPermissions.getUserPermissions().put(groupName, targetList);
            } else {
                targetList.add(currentProject);
            }

        }
        return userPermissions;
    }

    private boolean ifUserExist(long userId){
        return userDao.existsById(userId);
    }

    private void activateOrDeactivateUser(long userId, boolean active) {
        if (active) {
            userManagerService.activateUser(userId);
        } else {
            userManagerService.deactivateUser(userId);
        }
    }

    private void assertPremiumPluginIsInstalled() throws IllegalPremiumFeatureAccessException {
        if (! pluginFinderService.isPremiumPluginInstalled()) {
            throw new IllegalPremiumFeatureAccessException();
        }
    }

}
