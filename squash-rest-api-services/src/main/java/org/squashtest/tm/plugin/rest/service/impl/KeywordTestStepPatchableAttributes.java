/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.apache.commons.lang3.StringUtils;

public enum KeywordTestStepPatchableAttributes implements PatchableAttributes {
    DATATABLE("datatable", String.class),
    DOCSTRING("docstring", String.class),
    COMMENT("comment", String.class);

    private String propertyName;
    private Class propertyClass;

    KeywordTestStepPatchableAttributes(String propertyName, Class propertyClass) {
        this.propertyName = propertyName;
        this.propertyClass = propertyClass;
    }

    @Override
    public String getPropertyName() {
        return propertyName;
    }

    @Override
    public Class getPropertyClass() {
        return propertyClass;
    }

    @Override
    public String getGetterName() {
        return "get" + StringUtils.capitalize(getPropertyName());
    }

    @Override
    public String getSetterName() {
        return "set" + StringUtils.capitalize(getPropertyName());
    }
}
