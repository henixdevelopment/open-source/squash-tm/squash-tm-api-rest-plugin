/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.plugin.rest.repository.RestAutomatedTestRepository;
import org.squashtest.tm.plugin.rest.service.RestAutomatedTestService;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
@Transactional
public class RestAutomatedTestServiceImpl implements RestAutomatedTestService {

    @Inject
    private RestAutomatedTestRepository dao;

    @Override
    public AutomatedTest getOne(long id) {
        return dao.getReferenceById(id);
    }

    @Override
    public AutomatedTest findById(long id) {
        Optional<AutomatedTest> automatedTest = dao.findById(id);
        if (!automatedTest.isPresent()) {
            throw new EntityNotFoundException("The automated test with id: " + id + " does not exist.");
        }
        return automatedTest.get();
    }
}
