/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.IterationTestPlanItemDto;
import org.squashtest.tm.plugin.rest.service.RestIterationTestPlanItemService;
import org.squashtest.tm.plugin.rest.validators.IterationTestPlanItemPostValidator;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(IterationTestPlanItem.class)
@UseDefaultRestApiConfiguration
public class RestIterationTestPlanItemController extends BaseRestController {

    public static final String ITPI_DYNAMIC_FILTER = "*, referenced_test_case[name, reference], referenced_dataset[name], iteration[name,reference], executions[execution_status,last_executed_by,last_executed_on], exploratory_session_overview[id,charter,session_duration], -created_on, -created_by, -last_modified_on, -last_modified_by";

    @Inject
    private RestIterationTestPlanItemService service;
    @Inject
    private IterationTestPlanItemPostValidator iterationTestPlanItemValidator;
    @Inject
    private IterationTestPlanItemPostValidator iterationTestPlanItemPostValidator;



    @Inject
    private ResourceLinksHelper linksHelper;

    @GetMapping(value = "/iteration-test-plan-items/{id}")
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression(ITPI_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<IterationTestPlanItem>> findIterationTestPlanItem(@PathVariable("id") long id) {

        IterationTestPlanItem itpi = service.getOne(id);

        EntityModel<IterationTestPlanItem> res = toEntityModel(itpi);

        linksHelper.addAllLinksForIterationTestPlanItem(res);

        return ResponseEntity.ok(res);
    }

    @GetMapping(value="/iteration-test-plan-items/{id}/executions")
    @ResponseBody
    @DynamicFilterExpression("name, execution_order, execution_status, last_executed_by, last_executed_on")
    public ResponseEntity<PagedModel<EntityModel<Execution>>> findItpiExecutions(@PathVariable("id") long itpiId, Pageable pageable){

        Page<Execution> execs = service.findExecutions(itpiId, pageable);

        PagedModel<EntityModel<Execution>> res = toPagedResourcesWithRel(execs, "executions");

        return ResponseEntity.ok(res);

    }

    @PostMapping(value="/iteration-test-plan-items/{id}/executions")
    @ResponseBody
    @DynamicFilterExpression(RestExecutionController.EXECUTION_DYNAMIC_FILTER + ", -test_plan_item")
    public ResponseEntity<EntityModel<Execution>> createNewExecution(@PathVariable("id") long itpiId){

        Execution exec = service.createExecution(itpiId);

        EntityModel<Execution> res = toEntityModel(exec);
        linksHelper.addAllLinksForExecution(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);

    }


    /*
     * Hidden feature intended for the jira plugin. Will not be publicly documented. It exists
     * because there are no search service that would fulfill such use case.
     * See also RestRequirementController#findSynchronizedRequirement()
     */
    @GetMapping(value="/iteration-test-plan-items", params={"remote_key", "server_name"})
    @ResponseBody
    @DynamicFilterExpression("id") // we want minimal informations on this
    public ResponseEntity<PagedModel<EntityModel<IterationTestPlanItem>>> findItemsByCoveredRemoteRequirement
    	(@RequestParam("remote_key") String remoteKey, @RequestParam("server_name") String serverName, Pageable pageable){

    	Page<IterationTestPlanItem> items = service.findItemsByCoveredRemoteRequirement(pageable, remoteKey, serverName);

    	PagedModel<EntityModel<IterationTestPlanItem>> res = toPagedResourcesWithRel(items, "iteration-test-plan-items");

    	return ResponseEntity.ok(res);

    }

    @PatchMapping(value = "/iteration-test-plan-items/{id}")
    @ResponseBody
    @DynamicFilterExpression(ITPI_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<IterationTestPlanItem>> modifyTestPlanItemToCampaign(@RequestBody IterationTestPlanItemDto itpiDto,
                                                                                       @PathVariable("id") long testPlanId) throws BindException {

        //validation DTO DataSet assigned to
        iterationTestPlanItemValidator.validatePatchTestPlanItem(itpiDto,testPlanId);
        //change test Plan item
        IterationTestPlanItem itpi = service.modifyIterationTestPlan(itpiDto,testPlanId);

        EntityModel<IterationTestPlanItem> res = toEntityModel(itpi);

        linksHelper.addAllLinksForIterationTestPlanItem(res);

        return ResponseEntity.ok(res);
    }

    @ResponseBody
    @DeleteMapping(value = "/iteration-test-plan-items/{testPlanItemsIds}")
    public ResponseEntity<Void> removeTestPlanItemsFromIteration(@PathVariable("testPlanItemsIds") List<Long> testPlanItemsIds) {

        service.deleteIterationTestPlan(testPlanItemsIds);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/iterations/{iterationId}/test-plan")
    @ResponseBody
    @DynamicFilterExpression("*,iteration-test-plan-item, referenced_test_case, referenced_dataset ")
    public  ResponseEntity<EntityModel<IterationTestPlanItem>>   addTestCasesToIteration(@RequestBody IterationTestPlanItemDto testPlanItemDto ,
                                                                                      @PathVariable("iterationId") long iterationId) throws BindException {
        iterationTestPlanItemPostValidator.validatePostTestPlanItem(testPlanItemDto,iterationId);

        IterationTestPlanItem itp = service.addIterationTestPlanItem(testPlanItemDto,iterationId);

        EntityModel<IterationTestPlanItem> res = toEntityModel(itp);

        linksHelper.addAllLinksForIterationTestPlanItem(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

}
