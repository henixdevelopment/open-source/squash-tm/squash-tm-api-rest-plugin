/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.users.ApiToken;
import org.squashtest.tm.plugin.rest.core.utils.ExceptionUtils;
import org.squashtest.tm.plugin.rest.jackson.model.ApiTokenRestDto;
import org.squashtest.tm.plugin.rest.service.RestApiTokenService;
import org.squashtest.tm.security.UserContextHolder;
import org.squashtest.tm.service.internal.dto.ApiTokenDto;
import org.squashtest.tm.service.user.ApiTokenService;
import org.squashtest.tm.service.user.UserManagerService;

import java.util.NoSuchElementException;

@Service
@Transactional
public class RestApiTokenServiceImpl implements RestApiTokenService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestApiTokenServiceImpl.class);

    private final ApiTokenService apiTokenService;
    private final UserManagerService userManagerService;

    public RestApiTokenServiceImpl(ApiTokenService apiTokenService, UserManagerService userManagerService) {
        this.apiTokenService = apiTokenService;
        this.userManagerService = userManagerService;
    }

    @Override
    public void selfDestroyToken(String token) {
        apiTokenService.selfDestroyApiToken(token);
    }

    @Override
    public Page<ApiToken> getAllTokensByUser(Pageable pageable) {
        Long userId = userManagerService.findByLogin(UserContextHolder.getUsername()).getId();
        return apiTokenService.findAllByUserId(userId, pageable);
    }

    @Override
    public void deletePersonalApiToken(long tokenId) {
        try {
            apiTokenService.deletePersonalApiToken(tokenId);
        } catch(NoSuchElementException e) {
            LOGGER.error(e.getMessage(), e);
            throw ExceptionUtils.entityNotFoundException(ApiToken.class, tokenId);
        }
    }

    @Override
    public ApiTokenDto addToken(ApiTokenRestDto apiTokenRestDto) {
        return apiTokenService.generateApiToken(apiTokenRestDto.getName(), apiTokenRestDto.getExpiryDate(), apiTokenRestDto.getPermissions());
    }
}
