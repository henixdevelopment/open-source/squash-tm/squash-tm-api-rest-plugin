/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators

import org.squashtest.tm.domain.testcase.ExploratoryTestCase
import org.squashtest.tm.domain.testcase.KeywordTestCase
import org.squashtest.tm.domain.testcase.ScriptedTestCase
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.plugin.rest.jackson.model.ExploratoryTestCaseDto
import org.squashtest.tm.plugin.rest.jackson.model.KeywordTestCaseDto
import org.squashtest.tm.plugin.rest.jackson.model.ScriptedTestCaseDto
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto
import spock.lang.Specification

class TestCasePatchValidatorTest extends Specification{
    TestCasePatchValidator validator = new TestCasePatchValidator()

    def "should not throw for correct associations"(){
        when:
        validator.checkTestCaseTypeWithGivenDtoType(dto, entity)

        then:
        noExceptionThrown()

        where:
        dto                          | entity
        new TestCaseDto()            | new TestCase()
        new KeywordTestCaseDto()     | new KeywordTestCase()
        new ScriptedTestCaseDto()    | new ScriptedTestCase()
        new ExploratoryTestCaseDto() | new ExploratoryTestCase()
    }

    def "should throw exception for scriptedTC DTO and standard TC"(){
        when:
        validator.checkTestCaseTypeWithGivenDtoType(new ScriptedTestCaseDto(), new TestCase())

        then:
        final IllegalArgumentException exception = thrown()
        exception.message == "Invalid patch type (GHERKIN) for STANDARD entity."
    }

    def "should throw exception for keywordTC DTO and standard TC"(){
        when:
        validator.checkTestCaseTypeWithGivenDtoType(new KeywordTestCaseDto(), new TestCase())

        then:
        final IllegalArgumentException exception = thrown()
        exception.message == "Invalid patch type (KEYWORD) for STANDARD entity."
    }

    def "should throw exception for standardTC DTO and scripted TC"(){
        when:
        validator.checkTestCaseTypeWithGivenDtoType(new TestCaseDto(), new ScriptedTestCase())

        then:
        final IllegalArgumentException exception = thrown()
        exception.message == "Invalid patch type (STANDARD) for GHERKIN entity."
    }

    def "should throw exception for keywordTC DTO and scripted TC"(){
        when:
        validator.checkTestCaseTypeWithGivenDtoType(new KeywordTestCaseDto(), new ScriptedTestCase())

        then:
        final IllegalArgumentException exception = thrown()
        exception.message == "Invalid patch type (KEYWORD) for GHERKIN entity."
    }

    def "should throw exception for standardTC DTO and keyword TC"(){
        when:
        validator.checkTestCaseTypeWithGivenDtoType(new TestCaseDto(), new KeywordTestCase())

        then:
        final IllegalArgumentException exception = thrown()
        exception.message == "Invalid patch type (STANDARD) for KEYWORD entity."
    }

    def "should throw exception for scriptedTC DTO and keyword TC"(){
        when:
        validator.checkTestCaseTypeWithGivenDtoType(new ScriptedTestCaseDto(), new KeywordTestCase())

        then:
        final IllegalArgumentException exception = thrown()
        exception.message == "Invalid patch type (GHERKIN) for KEYWORD entity."
    }

}
