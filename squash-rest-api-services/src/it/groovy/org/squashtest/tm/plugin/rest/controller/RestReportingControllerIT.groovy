/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.squashtest.tm.domain.customreport.CustomReportTreeDefinition
import org.squashtest.tm.plugin.rest.service.RestReportingService
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestReportingController)
public class RestReportingControllerIT extends BaseControllerSpec {

    @Inject
    private RestReportingService restReportingService;

    def "get-chart"() {

        given:
        def customReportLibraryNode = SquashEntityBuilder.customReportLibrary {
            id = 44l
            name = "First chart"
            entityType = CustomReportTreeDefinition.CHART
        }

        and:
        restReportingService.getReportingEntity(44, "CHART") >> customReportLibraryNode

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/charts/{id}", 44)
            .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {
            "entity_type".is "CHART"
            "id".is 44
            "name".is "First chart"
        }

        res.andDo(doc.document(
            documentationBuilder {
                requestParams {
                    add DescriptorLists.fieldsParams
                }
                pathParams {
                    add "id : the id of the chart"
                }
                fields {
                    add "entity_type (string) : the type of the entity"
                    add "id (number) : the id of the chart"
                    add "name (string) : the name of the chart"
                    add DescriptorLists.linksFields
                }
                _links {
                    add "self : link to this chart"
                }
            }
        ))
    }

    def "get-custom-export"() {

        given:
        def customReportLibraryNode = SquashEntityBuilder.customReportLibrary {
            id = 44l
            name = "First custom export"
            entityType = CustomReportTreeDefinition.CUSTOM_EXPORT
        }

        and:
        restReportingService.getReportingEntity(44, "CUSTOM_EXPORT") >> customReportLibraryNode

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/custom-exports/{id}", 44)
            .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {
            "entity_type".is "CUSTOM_EXPORT"
            "id".is 44
            "name".is "First custom export"
        }

        res.andDo(doc.document(
            documentationBuilder {
                requestParams {
                    add DescriptorLists.fieldsParams
                }
                pathParams {
                    add "id : the id of the custom export"
                }
                fields {
                    add "entity_type (string) : the type of the entity"
                    add "id (number) : the id of the custom export"
                    add "name (string) : the name of the custom export"
                    add DescriptorLists.linksFields
                }
                _links {
                    add "self : link to this custom export"
                }
            }
        ))
    }

    def "get-dashboard"() {

        given:
        def customReportLibraryNode = SquashEntityBuilder.customReportLibrary {
            id = 44l
            name = "First dashboard"
            entityType = CustomReportTreeDefinition.DASHBOARD
        }

        and:
        restReportingService.getReportingEntity(44, "DASHBOARD") >> customReportLibraryNode

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/dashboards/{id}", 44)
            .header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {
            "entity_type".is "DASHBOARD"
            "id".is 44
            "name".is "First dashboard"
        }

        res.andDo(doc.document(
            documentationBuilder {
                requestParams {
                    add DescriptorLists.fieldsParams
                }
                pathParams {
                    add "id : the id of the dashboard"
                }
                fields {
                    add "entity_type (string) : the type of the entity"
                    add "id (number) : the id of the dashboard"
                    add "name (string) : the name of the dashboard"
                    add DescriptorLists.linksFields
                }
                _links {
                    add "self : link to this dashboard"
                }
            }
        ))
    }

    def "get-reporting-folder"() {

        given:
        def customReportLibraryNode = SquashEntityBuilder.customReportLibrary {
            id = 44l
            name = "First folder"
            entityType = CustomReportTreeDefinition.FOLDER
        }

        and:
        restReportingService.getReportingEntity(44, "FOLDER") >> customReportLibraryNode

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/reporting-folders/{id}", 44)
            .header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {
            "entity_type".is "FOLDER"
            "id".is 44
            "name".is "First folder"
        }

        res.andDo(doc.document(
            documentationBuilder {
                requestParams {
                    add DescriptorLists.fieldsParams
                }
                pathParams {
                    add "id : the id of the reporting folder"
                }
                fields {
                    add "entity_type (string) : the type of the entity"
                    add "id (number) : the id of the reporting folder"
                    add "name (string) : the name of the reporting folder"
                    add DescriptorLists.linksFields
                }
                _links {
                    add "self : link to this reporting folder"
                }
            }
        ))
    }

    def "get-report"() {

        given:
        def customReportLibraryNode = SquashEntityBuilder.customReportLibrary {
            id = 44l
            name = "First report"
            entityType = CustomReportTreeDefinition.REPORT
        }

        and:
        restReportingService.getReportingEntity(44, "REPORT") >> customReportLibraryNode

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/reports/{id}", 44)
            .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {
            "entity_type".is "REPORT"
            "id".is 44
            "name".is "First report"
        }

        res.andDo(doc.document(
            documentationBuilder {
                requestParams {
                    add DescriptorLists.fieldsParams
                }
                pathParams {
                    add "id : the id of the report"
                }
                fields {
                    add "entity_type (string) : the type of the entity"
                    add "id (number) : the id of the report"
                    add "name (string) : the name of the report"
                    add DescriptorLists.linksFields
                }
                _links {
                    add "self : link to this report"
                }
            }
        ))
    }

    def "delete-charts"() {
        given:
        def ids = [169L, 189L]
        restReportingService.fetchAndDeleteReportingDeletableIds(ids, "CHART") >> ids

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/charts/{ids}", "169,189")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the charts"
                    }
                }
        ))
    }

    def "delete-custom-exports"() {
        given:
        def ids = [169L, 189L]
        restReportingService.fetchAndDeleteReportingDeletableIds(ids, "CUSTOM_EXPORT") >> ids

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/custom-exports/{ids}", "169,189")
            .accept("application/json")
            .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())

        /*
         * Documentation
         */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "ids : the list of ids of the custom exports"
                }
            }
        ))
    }

    def "delete-dashboards"() {
        given:
        def ids = [169L, 189L]
        restReportingService.fetchAndDeleteReportingDeletableIds(ids, "DASHBOARD") >> ids

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/dashboards/{ids}", "169,189")
            .accept("application/json")
            .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())

        /*
         * Documentation
         */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "ids : the list of ids of the dashboards"
                }
            }
        ))
    }

    def "delete-reporting-folders"() {
        given:
        def ids = [169L, 189L]
        restReportingService.fetchAndDeleteReportingDeletableIds(ids, "FOLDER") >> ids

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/reporting-folders/{ids}", "169,189")
            .accept("application/json")
            .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())

        /*
         * Documentation
         */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "ids : the list of ids of the reporting folders"
                }
            }
        ))
    }

    def "delete-reports"() {
        given:
        def ids = [169L, 189L]
        restReportingService.fetchAndDeleteReportingDeletableIds(ids, "REPORT") >> ids

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/reports/{ids}", "169,189")
            .accept("application/json")
            .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())

        /*
         * Documentation
         */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "ids : the list of ids of the reports"
                }
            }
        ))
    }

}
