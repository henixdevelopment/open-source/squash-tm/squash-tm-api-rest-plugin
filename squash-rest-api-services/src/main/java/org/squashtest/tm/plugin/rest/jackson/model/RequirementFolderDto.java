/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.domain.library.Library;
import org.squashtest.tm.domain.project.Project;

import java.util.List;

public class RequirementFolderDto implements  RestNode {


    private Long folderId;
    private String name;
    private String description;
    private ParentEntity parent;
    private Project project;

    @JsonProperty("custom_fields")
    private List<CustomFieldValueDto> customFields;


    @Override
    public ParentEntity getParent() {
        return parent;
    }

    @Override
    public List<CustomFieldValueDto> getCustomFields() {
        return customFields;
    }

    @Override
    public RestType getRestType() {
        return RestType.REQUIREMENT_FOLDER;
    }

    public void setCustomFields(List<CustomFieldValueDto> customFields) {
        this.customFields = customFields;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name=name;
    }

    @Override
    public void setDescription(String description) {
        this.description=description;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void notifyAssociatedWithProject(Project project) {
        this.project = project;
    }

    @Override
    public Long getId() {
        return folderId;
    }

    public void setId(Long id) {
        this.folderId = id;
    }

    @Override
    public Project getProject() {
        return project;
    }

    @Override
    public Library<?> getLibrary() {
        return getProject().getTestCaseLibrary();
    }


}
