/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.docutils

import org.springframework.restdocs.hypermedia.LinkDescriptor
import org.springframework.restdocs.hypermedia.LinksSnippet
import org.springframework.restdocs.payload.FieldDescriptor
import org.springframework.restdocs.payload.JsonFieldType
import org.springframework.restdocs.payload.PayloadDocumentation
import org.springframework.restdocs.payload.ResponseFieldsSnippet
import org.springframework.restdocs.request.ParameterDescriptor
import org.springframework.restdocs.request.RequestParametersSnippet
import org.springframework.restdocs.snippet.AbstractDescriptor
import org.springframework.restdocs.snippet.Snippet
import org.squashtest.tm.plugin.rest.core.exception.ProgrammingError

import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.halLinks
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.relaxedLinks
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.relaxedRequestFields
import static org.springframework.restdocs.payload.PayloadDocumentation.relaxedResponseFields
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters
import static org.springframework.restdocs.request.RequestDocumentation.relaxedPathParameters
import static org.springframework.restdocs.request.RequestDocumentation.relaxedRequestParameters
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters

/**
 * <p>That class provides several snippets for spring-restdoc.</p>
 *
 * <p>
 *     Because {@link org.springframework.restdocs.snippet.Snippet} cannot be composed, we propose them
 *     in two flavors :
 *     <ul>
 *         <li>As actual snippets, if you dont need to compose them,</li>
 *         <li>As a list of {@link org.springframework.restdocs.snippet.AbstractDescriptor}, that can be concatenated as will</li>
 *     </ul>
 *
 *      Both flavors are hosted in their nested namespaces.
 * </p>
 *
 * <p>
 *    In addition, you can also look at the off-the-shelf bundles in the AllInOne, or use the Builder to ease the creation of the
 *    documentation.
 * </p>
 *
 * Created by bsiri on 23/06/2017.
 */
public class DocumentationSnippets {

    /*************************************************************
     *
     *                  ARRAY OF DESCRIPTORS
     *
     *************************************************************/


    public static final class DescriptorLists {

        // ************************** parameters ****************************


        public static final List<ParameterDescriptor> paginationAndSortParams = [
                parameterWithName("page").optional().description("number of the page to retrieve (optional)"),
                parameterWithName("size").optional().description("size of the page to retrieve (optional)"),
                parameterWithName("sort").optional().description("which attributes of the returned entities should be sorted on (optional)")
        ]

        public static final List<ParameterDescriptor> paginationParams = [
                parameterWithName("page").optional().description("number of the page to retrieve (optional)"),
                parameterWithName("size").optional().description("size of the page to retrieve (optional)")
        ]


        public static final List<ParameterDescriptor> contentInclusionParams = [
                parameterWithName("include").optional().description("level of depth of the content that should be returned (optional)," +
                        " available values : root or nested (more info in Parameter 'include' section)"),
        ]

        public static final List<ParameterDescriptor> fieldsParams = [
                parameterWithName("fields").optional().description("which fields of the elements should be returned (optional)")

        ]

        public static final List<ParameterDescriptor> typeFilterParams = [
                parameterWithName("type").optional().description("which type of the element should be returned (optional)")

        ]

        // ************************* fields *************************************

        public static final List<FieldDescriptor> paginationFields = [
                fieldWithPath("page.size").type(JsonFieldType.NUMBER).description("the page size for that query"),
                fieldWithPath("page.totalElements").type(JsonFieldType.NUMBER).description("total number of elements the user is allowed to read"),
                fieldWithPath("page.totalPages").type(JsonFieldType.NUMBER).description("how many pages can be browsed"),
                fieldWithPath("page.number").type(JsonFieldType.NUMBER).description("the page number"),
        ]

        public static final List<FieldDescriptor> linksFields = [
                subsectionWithPath("_links").description("related links")
        ]

        public static final List<FieldDescriptor> idTypeFields = [
                fieldWithPath("id").type(JsonFieldType.NUMBER).description("the id of the entity"),
                fieldWithPath("_type").type(JsonFieldType.STRING).description("the type of the entity")
        ]

        public static final List<FieldDescriptor> embeddedContentFields = [
                fieldWithPath("_embedded.content").type(JsonFieldType.ARRAY).description("the list of elements returned for that page")
        ]

        public static final List<FieldDescriptor> auditableFields = [
                fieldWithPath("created_by").type(JsonFieldType.STRING).description("user that created the entity"),
                fieldWithPath("created_on").type(JsonFieldType.STRING).description("timestamp of the creation (ISO 8601)"),
                fieldWithPath("last_modified_by").type(JsonFieldType.STRING).description("user that modified the entity the most recently"),
                fieldWithPath("last_modified_on").type(JsonFieldType.STRING).description("timestamp of last modification (ISO 8601)"),
        ]

        public static final List<FieldDescriptor> scheduleFields = [
                fieldWithPath("actual_start_date").type(JsonFieldType.STRING).description("actual start date"),
                fieldWithPath("actual_end_date").type(JsonFieldType.STRING).description("actual end date"),
                fieldWithPath("actual_start_auto").type(JsonFieldType.BOOLEAN).description("whether the actual start date is automatically computed"),
                fieldWithPath("actual_end_auto").type(JsonFieldType.BOOLEAN).description("whether the actual end date is automatically computed"),
        ]

        public static final List<FieldDescriptor> regularLibraryNodeFields = [
                fieldWithPath("id").type(JsonFieldType.NUMBER).description("the id of the entity"),
                fieldWithPath("_type").type(JsonFieldType.STRING).description("the type of the entity"),
                fieldWithPath("name").type(JsonFieldType.STRING).description("name of the entity"),
                subsectionWithPath("project").type(JsonFieldType.OBJECT).description("project of the entity"),
                subsectionWithPath("parent").type(JsonFieldType.OBJECT).description("the location of the entity (either a folder or the project if located at the root of the library)"),
                fieldWithPath("path").type(JsonFieldType.STRING).description("the path of the entity"),
                fieldWithPath("created_by").type(JsonFieldType.STRING).description("user that created the entity"),
                fieldWithPath("created_on").type(JsonFieldType.STRING).description("timestamp of the creation (ISO 8601)"),
                fieldWithPath("last_modified_by").type(JsonFieldType.STRING).description("user that modified the entity the most recently"),
                fieldWithPath("last_modified_on").type(JsonFieldType.STRING).description("timestamp of last modification (ISO 8601)"),
                fieldWithPath("description").type(JsonFieldType.STRING).description("description of that entity (html)"),
                subsectionWithPath("attachments").type(JsonFieldType.ARRAY).description("the attachments of that entity"),
                subsectionWithPath("_links").type(JsonFieldType.VARIES).description("related links")


        ]


        public static final List<FieldDescriptor> updateCufFields = [
                fieldWithPath("custom_fields").type(JsonFieldType.ARRAY).description("an array of custom fields"),
                fieldWithPath("custom_fields[].code").type(JsonFieldType.STRING).description("the code of the custom field to modify"),
                fieldWithPath("custom_fields[].value").type(JsonFieldType.VARIES).description("the value of the custom field. It should match the type of the field (text, date etc). If the field accepts only a single value the content is a string, if it accepts multiple values (eg, tags) the content is an array of strings.")
        ]

        public static final List<FieldDescriptor>  denormalizedEnvironmentVariableFields = [
            fieldWithPath("environment_variables").type(JsonFieldType.ARRAY).description("an array of environment variables"),
            fieldWithPath("environment_variables[].name").type(JsonFieldType.STRING).description("the name of the environment variable."),
            fieldWithPath("environment_variables[].value").type(JsonFieldType.STRING).description("the new value assigned to the environment variable linked to the project."),
            fieldWithPath("environment_variables[].type").type(JsonFieldType.STRING).description("the type of the environment variable.")
        ]

        public static final List<FieldDescriptor>  issuesFields = [
            fieldWithPath("_embedded.issues[].remoteIssueId").type(JsonFieldType.STRING).description("the remote issue id of the issue linked to the entity."),
            fieldWithPath("_embedded.issues[].url").type(JsonFieldType.STRING).description("the URL of the issue linked to the entity."),
            fieldWithPath("_embedded.issues[].executions").type(JsonFieldType.ARRAY).description("the executions linked to the entity.")
        ]


        // ************************ links ****************************************


        public static final List<LinkDescriptor> paginationLinks = [
                linkWithRel("first").optional().description("link to the first page (optional)"),
                linkWithRel("prev").optional().description("link to the previous page (optional)"),
                linkWithRel("self").description("link to this page"),
                linkWithRel("next").optional().description("link to the next page (optional)"),
                linkWithRel("last").optional().description("link to the last page (optional)")
        ]


    }

    /*************************************************************
     *
     *                  SNIPPETS
     *
     *************************************************************/

    public static final class Snippets {

        // ************************** parameters ****************************


        public static
        final RequestParametersSnippet paginationParams = requestParameters(DescriptorLists.paginationAndSortParams)

        public static
        final RequestParametersSnippet contentInclusionParams = requestParameters(DescriptorLists.contentInclusionParams)

        public static final RequestParametersSnippet fieldsParams = requestParameters(DescriptorLists.fieldsParams)

        // ************************* fields *************************************

        public static final ResponseFieldsSnippet paginationFields = responseFields(DescriptorLists.paginationFields)

        public static final ResponseFieldsSnippet linksFields = responseFields(DescriptorLists.linksFields)

        public static final ResponseFieldsSnippet idTypeFields = responseFields(DescriptorLists.idTypeFields)

        public static
        final ResponseFieldsSnippet embeddedContentFields = responseFields(DescriptorLists.embeddedContentFields)

        // ************************ links ****************************************

        public static final LinksSnippet paginationLinks = links(halLinks(), DescriptorLists.paginationLinks)

    }

    /*************************************************************
     *
     *                  ALL IN ONE
     *
     *************************************************************/


    public static final class AllInOne {

        // for requests like /api/rest/latest/<entity>, which browse everything for that namespace
        public static final Snippet[] createBrowseAllEntities(String relName) {
            [
                    requestParameters((
                            DescriptorLists.paginationAndSortParams +
                                    DescriptorLists.fieldsParams +
                                    DescriptorLists.typeFilterParams
                    )),

                    responseFields((
                            [subsectionWithPath("_embedded.$relName").type(JsonFieldType.ARRAY).description("the list of elements for that page")] +
                                    DescriptorLists.paginationFields +
                                    DescriptorLists.linksFields

                    )),

                    Snippets.paginationLinks

            ] as Snippet[]
        }

        // for requests like /api/rest/latest/entity/<id>/content, that list the content of an element
        // very similar to the above, but notably supports the 'include' parameter as well
        public static final Snippet[] createListEntityContent(String relName, String containerType = "container", boolean supportsContentInclusion = false) {

            def reqParams = ( DescriptorLists.paginationAndSortParams + DescriptorLists.fieldsParams)
            if (supportsContentInclusion){
                reqParams = (reqParams + DescriptorLists.contentInclusionParams)
            }

            [

                    pathParameters(parameterWithName("id").description("the id of the $containerType")),

                    requestParameters( reqParams ),

                    responseFields((
                            [subsectionWithPath("_embedded.$relName").type(JsonFieldType.ARRAY).description("the list of elements for that page")] +
                                    DescriptorLists.paginationFields +
                                    DescriptorLists.linksFields

                    )),

                    Snippets.paginationLinks

            ] as Snippet[]
        }

    }

    /*************************************************************
     *
     *                  BUILDER
     *
     *************************************************************/

    public static Snippet[] documentationBuilder(@DelegatesTo(DocBuilder) Closure clos) {
        def builder = new DocBuilder()
        builder.with clos
        builder.build()
    }


    private static class DocBuilder {
        private PathParamsBuilder pathParamsBuilder = new PathParamsBuilder()
        private RequestParamsBuilder reqParamsBuilder = new RequestParamsBuilder()
        private RequestFieldParamsBuilder reqFieldsBuilder = new RequestFieldParamsBuilder()
        private FieldParamsBuilder fieldsBuilder = new FieldParamsBuilder()
        private LinksBuilder linksBuilder = new LinksBuilder()

        public void pathParams(@DelegatesTo(SnippetBuilder) Closure clos) {
            pathParamsBuilder.with clos
        }

        public void requestParams(@DelegatesTo(SnippetBuilder) Closure clos) {
            reqParamsBuilder.with clos
        }

        public void requestFields(@DelegatesTo(RequestFieldParamsBuilder) Closure clos) {
            reqFieldsBuilder.with clos
        }

        public void fields(@DelegatesTo(FieldParamsBuilder) Closure clos) {
            fieldsBuilder.with clos
        }

        public void _links(@DelegatesTo(LinksBuilder) Closure clos) {
            linksBuilder.with clos
        }

        private Snippet[] build() {
            def descriptors = [pathParamsBuilder, reqParamsBuilder, reqFieldsBuilder, fieldsBuilder, linksBuilder].collect {
                return (it.des.size() > 0) ? it.build() : null
            }
            descriptors.removeAll([null])
            return descriptors as Snippet[]
        }

    }

    private static abstract class SnippetBuilder<DESCRIPTOR_TYPE extends AbstractDescriptor> {
        protected List<DESCRIPTOR_TYPE> des = []
        protected boolean relaxed = false

        void add(DESCRIPTOR_TYPE descriptor) {
            des << descriptor
        }

        void add(List<DESCRIPTOR_TYPE> descriptors) {
            des.addAll(descriptors)
        }

        void throwError(String failedDescription) {
            throw new ProgrammingError("'$failedDescription' : invalid descriptor, must complies with template '${humanTemplate()}'")
        }

        /**
         * <p>
         * Adds a field documentation for this snippet, that contains all the relevant information for that field.
         * The documentation should match a template, which varies for each subclass of SnippetBuilder,
         * so that the SnippetBuilder can extract information like the field name, a summary, whether it is optional etc.
         * </p>
         *
         * @param descriptor
         */
        abstract void add(String descriptor)

        abstract Snippet build()

        abstract String humanTemplate()
    }

    private static class RequestParamsBuilder extends SnippetBuilder<ParameterDescriptor> {

        private static final REGEX = /\s*([^\s]+)\s*:\s*(\w.*)/

        String humanTemplate() { '<name> : <description> [(optional)]' }

        void add(String descriptor) {
            def groups = (descriptor =~ REGEX)
            if (groups.hasGroup()) {
                def (name, description) = [groups[0][1], groups[0][2]]
                boolean optional = (description =~ /\(optional\)/)
                ParameterDescriptor obj = parameterWithName(name).description(description)
                if (optional) {
                    obj.optional()
                }
                des << obj
            } else {
                throwError(descriptor)
            }
        }


        @Override
        Snippet build() {
            if (relaxed) {
                relaxedRequestParameters(des)
            } else {
                requestParameters(des)
            }
        }
    }

    private static class PathParamsBuilder extends RequestParamsBuilder {
        @Override
        Snippet build() {
            if (relaxed) {
                relaxedPathParameters(des)
            } else {
                pathParameters(des)
            }
        }
    }


    private static class FieldParamsBuilder extends SnippetBuilder<FieldDescriptor> {

        private static final REGEX = /\s*([^\s]+)\s*\((\w+\??)\)\s*:\s*(\w.*)/

        String humanTemplate() { '<id> (<type>) : <description>' }

        @Override
        void add(String descriptor) {
            add("", descriptor)
        }

        void embedded(String descriptor) {
            add("_embedded.", descriptor)
        }


        /**
         * <p>
         *  Like #add, but will inhibit the natural behavior of Spring RestDoc. Normally when a field value is an object
         *  its properties are expected to be documented as well. Sometimes it is undesirable, for example when the said
         *  object is documented in a proper section. By using addAndStop Spring will be instructed not to follow through.
         * </p>
         *
         * @param descriptor
         */
        void addAndStop(String descriptor){
            add("", descriptor, false)
        }

        void embeddedAndStop(String descriptor){
            add("_embedded.", descriptor, false)
        }

        void add(String prefix, String descriptor, Boolean documentNestedProperties = true) {
            def builderMethod = (documentNestedProperties) ?
                                        PayloadDocumentation.&fieldWithPath :
                                        PayloadDocumentation.&subsectionWithPath

            def groups = (descriptor =~ REGEX)
            if (groups.hasGroup()) {
                def (name, strType, description) = [groups[0][1], groups[0][2].toString(), groups[0][3]]
                def nullable = false
                if (strType.endsWith("?")) {
                    nullable = true
                    strType = strType.substring(0, strType.length() - 1)
                }
                JsonFieldType type = JsonFieldType.valueOf(strType.toUpperCase())
                def field = builderMethod(prefix + name).type(type).description(description)
                if (nullable) field = field.optional()
                des << field
            } else {
                throwError(descriptor)
            }
        }

        @Override
        Snippet build() {
            if (relaxed) {
                relaxedResponseFields(des)
            } else {
                responseFields(des)
            }
        }
    }

    private static final class RequestFieldParamsBuilder extends FieldParamsBuilder {
        @Override
        Snippet build() {
            if (relaxed) {
                relaxedRequestFields(des)
            } else {
                requestFields(des)
            }
        }
    }

    private static final class LinksBuilder extends SnippetBuilder<LinkDescriptor> {

        private static final String REGEX = /\s*([^\s]+)\s*:\s*(\w.*)/

        String humanTemplate() { '<rel> : <description> [(optional)]' }

        @Override
        void add(String descriptor) {
            def groups = (descriptor =~ REGEX)
            if (groups.hasGroup()) {
                def (name, description) = [groups[0][1], groups[0][2]]
                boolean optional = (description =~ /\(optional\)/)
                LinkDescriptor obj = linkWithRel(name).description(description)
                if (optional) {
                    obj.optional()
                }
                des << obj
            } else {
                throwError(descriptor)
            }
        }

        @Override
        Snippet build() {
            if (relaxed) {
                relaxedLinks(halLinks(), des)
            } else {
                links(halLinks(), des)
            }
        }
    }

}
