/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestPlanStatistics;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.plugin.rest.jackson.model.TestSuiteDto;

import java.util.List;

/**
 * Created by jthebault on 21/06/2017.
 */
public interface RestTestSuiteService {
    TestSuite getOne(long id);

    Page<IterationTestPlanItem> findTestPlan(long testSuiteId, Pageable pageable);

    TestSuite addTestSuite(Long iterationId, TestSuiteDto testSuiteDto);
    TestSuite modifyTestSuite( TestSuiteDto testSuiteDto, Long testSuiteId);
    void deleteTestSuite(List<Long> testSuiteIds);
    TestSuite detachTestPlanFromTestSuite(Long testSuiteId, List<Long> ids );
    TestSuite attachTestPlanToTestSuite(Long testSuiteId, List<Long> ids );

    List<Long> getExecutionIdsByTestSuite(Long testSuiteId);
    TestPlanStatistics getTestSuiteStatisticsByTestSuiteIds(Long testSuiteId);
}
