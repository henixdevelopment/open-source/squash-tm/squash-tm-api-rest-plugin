/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestDatasetService
import org.squashtest.tm.plugin.rest.validators.DatasetValidator
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

/**
 * Created by jlor on 10/07/2017.
 */
@WebMvcTest(RestDatasetController)
public class RestDatasetControllerIT extends BaseControllerSpec {

    @Inject
    private RestDatasetService restDatasetService;
    @Inject
    private DatasetValidator datasetValidator;

    // ***** GETting ***** //

    def "get-dataset"() {

        given:

        def theTestCase = SquashEntityBuilder.testCase {
            id = 9l
            name = "login test"
        }

        def dataset = SquashEntityBuilder.dataset {

            id = 7l
            name = "sample dataset"
            parameterValues = [
                    SquashEntityBuilder.paramValue {
                        id = 5l
                        parameter = SquashEntityBuilder.parameter {
                            id = 1l
                            name = "param_1"
                            description = "login parameter"
                            testCase = theTestCase
                        }
                        paramValue = "login_1"
                    },
                    SquashEntityBuilder.paramValue {
                        id = 6l
                        parameter = SquashEntityBuilder.parameter {
                            id = 2l
                            name = "param_2"
                            description = "password parameter"
                            testCase = theTestCase
                        }
                        paramValue = "password_1"
                    }
            ] as Set
            testCase = theTestCase
        }

        and:

        restDatasetService.getOne(7) >> dataset

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/datasets/{id}", 7).header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "dataset"
            "id".is 7
            "name".is "sample dataset"

            "parameters".hasSize 2
            "parameters".test {
                "[0]".test {
                    "_type".is "parameter"
                    "id".is 1
                    "name".is "param_1"
                }
                "[1]".test {
                    "_type".is "parameter"
                    "id".is 2
                    "name".is "param_2"
                }
            }
            "parameter_values".hasSize 2
            "parameter_values".test {
                "[0]".test {
                    "parameter_id".is 1
                    "parameter_name".is "param_1"
                    "parameter_value".is "login_1"
                    "parameter_test_case_id".is 9
                }
                "[1]".test {
                    "parameter_id".is 2
                    "parameter_name".is "param_2"
                    "parameter_value".is "password_1"
                    "parameter_test_case_id".is 9
                }
            }
            "test_case".test {
                "_type".is "test-case"
                "id".is 9
                "name".is "login test"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/9"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/datasets/7"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the dataset"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add "id (number) : the id of the dataset"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the dataset"
                        addAndStop "parameters (array) : the parameters of the dataset"
                        addAndStop "parameter_values (array) : the parameter values of the dataset"
                        addAndStop "test_case (object) : the test case this dataset belongs to"
                        addAndStop "_links (object) : related links"
                    }
                    _links {
                        add "self : link to this dataset"
                    }
                }
        ))
    }

    def "get-dataset for scripted test case"() {

        given:

        def theTestCase = SquashEntityBuilder.scriptedTestCase {
            id = 9l
            name = "login test"
        }

        def dataset = SquashEntityBuilder.dataset {

            id = 7l
            name = "sample dataset"
            parameterValues = [
                    SquashEntityBuilder.paramValue {
                        id = 5l
                        parameter = SquashEntityBuilder.parameter {
                            id = 1l
                            name = "param_1"
                            description = "login parameter"
                            testCase = theTestCase
                        }
                        paramValue = "login_1"
                    },
                    SquashEntityBuilder.paramValue {
                        id = 6l
                        parameter = SquashEntityBuilder.parameter {
                            id = 2l
                            name = "param_2"
                            description = "password parameter"
                            testCase = theTestCase
                        }
                        paramValue = "password_1"
                    }
            ] as Set
            testCase = theTestCase
        }

        and:

        restDatasetService.getOne(7) >> dataset

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/datasets/{id}", 7).header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "dataset"
            "id".is 7
            "name".is "sample dataset"

            "parameters".hasSize 2
            "parameters".test {
                "[0]".test {
                    "_type".is "parameter"
                    "id".is 1
                    "name".is "param_1"
                }
                "[1]".test {
                    "_type".is "parameter"
                    "id".is 2
                    "name".is "param_2"
                }
            }
            "parameter_values".hasSize 2
            "parameter_values".test {
                "[0]".test {
                    "parameter_id".is 1
                    "parameter_name".is "param_1"
                    "parameter_value".is "login_1"
                    "parameter_test_case_id".is 9
                }
                "[1]".test {
                    "parameter_id".is 2
                    "parameter_name".is "param_2"
                    "parameter_value".is "password_1"
                    "parameter_test_case_id".is 9
                }
            }
            "test_case".test {
                "_type".is "scripted-test-case"
                "id".is 9
                "name".is "login test"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/9"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/datasets/7"
        }

    }

    def "get-dataset for keyword test case"() {

        given:

        def theTestCase = SquashEntityBuilder.keywordTestCase {
            id = 9l
            name = "login test"
        }

        def dataset = SquashEntityBuilder.dataset {

            id = 7l
            name = "sample dataset"
            parameterValues = [
                    SquashEntityBuilder.paramValue {
                        id = 5l
                        parameter = SquashEntityBuilder.parameter {
                            id = 1l
                            name = "param_1"
                            description = "login parameter"
                            testCase = theTestCase
                        }
                        paramValue = "login_1"
                    },
                    SquashEntityBuilder.paramValue {
                        id = 6l
                        parameter = SquashEntityBuilder.parameter {
                            id = 2l
                            name = "param_2"
                            description = "password parameter"
                            testCase = theTestCase
                        }
                        paramValue = "password_1"
                    }
            ] as Set
            testCase = theTestCase
        }

        and:

        restDatasetService.getOne(7) >> dataset

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/datasets/{id}", 7).header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "dataset"
            "id".is 7
            "name".is "sample dataset"

            "parameters".hasSize 2
            "parameters".test {
                "[0]".test {
                    "_type".is "parameter"
                    "id".is 1
                    "name".is "param_1"
                }
                "[1]".test {
                    "_type".is "parameter"
                    "id".is 2
                    "name".is "param_2"
                }
            }
            "parameter_values".hasSize 2
            "parameter_values".test {
                "[0]".test {
                    "parameter_id".is 1
                    "parameter_name".is "param_1"
                    "parameter_value".is "login_1"
                    "parameter_test_case_id".is 9
                }
                "[1]".test {
                    "parameter_id".is 2
                    "parameter_name".is "param_2"
                    "parameter_value".is "password_1"
                    "parameter_test_case_id".is 9
                }
            }
            "test_case".test {
                "_type".is "keyword-test-case"
                "id".is 9
                "name".is "login test"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/9"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/datasets/7"
        }

    }

    def "post-dataset"() {
        given:
        def json = """{
                    "_type": "dataset",
                    "name": "sample dataset",

                     "parameter_values": [
                        {
                            "parameter_test_case_id": 238,
                            "parameter_value": "login_1",
                            "parameter_name": "param_1",
                            "parameter_id": 1
                        },
                        {
                            "parameter_test_case_id": 238,
                            "parameter_value": "password_1",
                            "parameter_name": "param_2",
                            "parameter_id": 2
                        }

                    ],
                    "test_case": {
                        "_type": "test-case",
                        "id": 238
                    }

                }"""
        def tc = SquashEntityBuilder.testCase {
            id = 238L
        }
        def parameter1 = SquashEntityBuilder.parameter {
            id = 1l
            name = "param_1"
            description = "login parameter"
            testCase = tc
        }
        def parameter2 = SquashEntityBuilder.parameter {
            id = 2l
            name = "param_2"
            description = "password parameter"
            testCase = tc
        }
        def dataset = SquashEntityBuilder.dataset {
            id = 23L
            name = "sample dataset"
            parameterValues = [
                    SquashEntityBuilder.paramValue {
                        id = 5l
                        parameter = parameter1
                        paramValue = "login_1"
                    },
                    SquashEntityBuilder.paramValue {
                        id = 6l
                        parameter = parameter2
                        paramValue = "password_1"
                    }
            ] as Set
            testCase = tc
        }

        and:
        restDatasetService.addDataset(_, _) >> dataset
        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/datasets")
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:

        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "dataset"
            "id".is 23
            "name".is "sample dataset"
            "parameter_values".hasSize 2
            "parameter_values".test {
                "[0]".test {
                    "parameter_id".is 1
                    "parameter_name".is "param_1"
                    "parameter_value".is "login_1"
                    "parameter_test_case_id".is 238
                }
                "[1]".test {
                    "parameter_id".is 2
                    "parameter_name".is "param_2"
                    "parameter_value".is "password_1"
                    "parameter_test_case_id".is 238
                }
            }
            "test_case".test {
                "_type".is "test-case"
                "id".is 238
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/datasets/23"
            }
        }
    }

    def "post-dataset for scripted test case"() {
        given:
        def json = """{
                    "_type": "dataset",
                    "name": "sample dataset",

                     "parameter_values": [
                        {
                            "parameter_test_case_id": 238,
                            "parameter_value": "login_1",
                            "parameter_name": "param_1",
                            "parameter_id": 1
                        },
                        {
                            "parameter_test_case_id": 238,
                            "parameter_value": "password_1",
                            "parameter_name": "param_2",
                            "parameter_id": 2
                        }

                    ],
                    "test_case": {
                        "_type": "scripted-test-case",
                        "id": 238
                    }

                }"""
        def tc = SquashEntityBuilder.scriptedTestCase {
            id = 238L
        }
        def parameter1 = SquashEntityBuilder.parameter {
            id = 1l
            name = "param_1"
            description = "login parameter"
            testCase = tc
        }
        def parameter2 = SquashEntityBuilder.parameter {
            id = 2l
            name = "param_2"
            description = "password parameter"
            testCase = tc
        }
        def dataset = SquashEntityBuilder.dataset {
            id = 23L
            name = "sample dataset"
            parameterValues = [
                    SquashEntityBuilder.paramValue {
                        id = 5l
                        parameter = parameter1
                        paramValue = "login_1"
                    },
                    SquashEntityBuilder.paramValue {
                        id = 6l
                        parameter = parameter2
                        paramValue = "password_1"
                    }
            ] as Set
            testCase = tc
        }

        and:
        restDatasetService.addDataset(_, _) >> dataset
        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/datasets")
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:

        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "dataset"
            "id".is 23
            "name".is "sample dataset"
            "parameter_values".hasSize 2
            "parameter_values".test {
                "[0]".test {
                    "parameter_id".is 1
                    "parameter_name".is "param_1"
                    "parameter_value".is "login_1"
                    "parameter_test_case_id".is 238
                }
                "[1]".test {
                    "parameter_id".is 2
                    "parameter_name".is "param_2"
                    "parameter_value".is "password_1"
                    "parameter_test_case_id".is 238
                }
            }
            "test_case".test {
                "_type".is "scripted-test-case"
                "id".is 238
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/datasets/23"
            }
        }
    }

    def "post-dataset for keyword test case"() {
        given:
        def json = """{
                    "_type": "dataset",
                    "name": "sample dataset",

                     "parameter_values": [
                        {
                            "parameter_test_case_id": 238,
                            "parameter_value": "login_1",
                            "parameter_name": "param_1",
                            "parameter_id": 1
                        },
                        {
                            "parameter_test_case_id": 238,
                            "parameter_value": "password_1",
                            "parameter_name": "param_2",
                            "parameter_id": 2
                        }

                    ],
                    "test_case": {
                        "_type": "keyword-test-case",
                        "id": 238
                    }

                }"""
        def tc = SquashEntityBuilder.keywordTestCase {
            id = 238L
        }
        def parameter1 = SquashEntityBuilder.parameter {
            id = 1l
            name = "param_1"
            description = "login parameter"
            testCase = tc
        }
        def parameter2 = SquashEntityBuilder.parameter {
            id = 2l
            name = "param_2"
            description = "password parameter"
            testCase = tc
        }
        def dataset = SquashEntityBuilder.dataset {
            id = 23L
            name = "sample dataset"
            parameterValues = [
                    SquashEntityBuilder.paramValue {
                        id = 5l
                        parameter = parameter1
                        paramValue = "login_1"
                    },
                    SquashEntityBuilder.paramValue {
                        id = 6l
                        parameter = parameter2
                        paramValue = "password_1"
                    }
            ] as Set
            testCase = tc
        }

        and:
        restDatasetService.addDataset(_, _) >> dataset
        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/datasets")
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:

        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "dataset"
            "id".is 23
            "name".is "sample dataset"
            "parameter_values".hasSize 2
            "parameter_values".test {
                "[0]".test {
                    "parameter_id".is 1
                    "parameter_name".is "param_1"
                    "parameter_value".is "login_1"
                    "parameter_test_case_id".is 238
                }
                "[1]".test {
                    "parameter_id".is 2
                    "parameter_name".is "param_2"
                    "parameter_value".is "password_1"
                    "parameter_test_case_id".is 238
                }
            }
            "test_case".test {
                "_type".is "keyword-test-case"
                "id".is 238
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/datasets/23"
            }
        }
    }

    def "patch-dataset"() {
        given:
        def json = """{
                    "_type": "dataset",
                    "name": "modified data sample",

                     "parameter_values": [
                        {
                            "parameter_value": "new_login_1",
                            "parameter_name": "param_1",
                            "parameter_id": 1
                        }

                    ]
                }"""
        def tc = SquashEntityBuilder.testCase {
            id = 238L
        }
        def dataset = SquashEntityBuilder.dataset {
            id = 23L
            name = "modified data sample"
            parameterValues = [
                    SquashEntityBuilder.paramValue {
                        id = 5l
                        parameter = SquashEntityBuilder.parameter {
                            id = 1l
                            name = "param_1"
                            description = "login parameter"
                            testCase = tc
                        }
                        paramValue = "new_login_1"
                    }
            ] as Set
            testCase = tc
        }
        and:
        restDatasetService.modifyDataset(_, _) >> dataset
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/datasets/{id}", 2L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "dataset"
            "id".is 23
            "name".is "modified data sample"
            "parameter_values".hasSize 1
            "parameter_values".test {
                "[0]".test {
                    "parameter_id".is 1
                    "parameter_name".is "param_1"
                    "parameter_value".is "new_login_1"
                    "parameter_test_case_id".is 238
                }
            }
            "test_case".test {
                "_type".is "test-case"
                "id".is 238
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/datasets/23"
            }
        }
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the dataset"
                    }
                }))

    }

    def "patch-dataset for scripted test case"() {
        given:
        def json = """{
                    "_type": "dataset",
                    "name": "modified data sample",

                     "parameter_values": [
                        {
                            "parameter_value": "new_login_1",
                            "parameter_name": "param_1",
                            "parameter_id": 1
                        }

                    ]
                }"""
        def tc = SquashEntityBuilder.scriptedTestCase {
            id = 238L
        }
        def dataset = SquashEntityBuilder.dataset {
            id = 23L
            name = "modified data sample"
            parameterValues = [
                    SquashEntityBuilder.paramValue {
                        id = 5l
                        parameter = SquashEntityBuilder.parameter {
                            id = 1l
                            name = "param_1"
                            description = "login parameter"
                            testCase = tc
                        }
                        paramValue = "new_login_1"
                    }
            ] as Set
            testCase = tc
        }
        and:
        restDatasetService.modifyDataset(_, _) >> dataset
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/datasets/{id}", 2L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "dataset"
            "id".is 23
            "name".is "modified data sample"
            "parameter_values".hasSize 1
            "parameter_values".test {
                "[0]".test {
                    "parameter_id".is 1
                    "parameter_name".is "param_1"
                    "parameter_value".is "new_login_1"
                    "parameter_test_case_id".is 238
                }
            }
            "test_case".test {
                "_type".is "scripted-test-case"
                "id".is 238
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/datasets/23"
            }
        }

    }

    def "patch-dataset for keyword test case"() {
        given:
        def json = """{
                    "_type": "dataset",
                    "name": "modified data sample",

                     "parameter_values": [
                        {
                            "parameter_value": "new_login_1",
                            "parameter_name": "param_1",
                            "parameter_id": 1
                        }

                    ]
                }"""
        def tc = SquashEntityBuilder.keywordTestCase {
            id = 238L
        }
        def dataset = SquashEntityBuilder.dataset {
            id = 23L
            name = "modified data sample"
            parameterValues = [
                    SquashEntityBuilder.paramValue {
                        id = 5l
                        parameter = SquashEntityBuilder.parameter {
                            id = 1l
                            name = "param_1"
                            description = "login parameter"
                            testCase = tc
                        }
                        paramValue = "new_login_1"
                    }
            ] as Set
            testCase = tc
        }
        and:
        restDatasetService.modifyDataset(_, _) >> dataset
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/datasets/{id}", 2L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "dataset"
            "id".is 23
            "name".is "modified data sample"
            "parameter_values".hasSize 1
            "parameter_values".test {
                "[0]".test {
                    "parameter_id".is 1
                    "parameter_name".is "param_1"
                    "parameter_value".is "new_login_1"
                    "parameter_test_case_id".is 238
                }
            }
            "test_case".test {
                "_type".is "keyword-test-case"
                "id".is 238
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/datasets/23"
            }
        }
    }

    def "delete-dataset"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/datasets/{id}", 44L)
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * restDatasetService.deleteDataset(_)


        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the list of ids of the dataset"
                    }
                }
        ))
    }
}
