/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.stereotype.Component;

import java.util.EnumSet;

//[JTH 2017-06-07] cannot do it as generic type, as in squashtm the plugin class loader is not the same as the class loader created at startup.
//See PluginsPathClasspathExtender in SquashTM core/tm.web
@Component
public class TestCasePatcher extends AbstractPatcher{

    private final EnumSet<TestCasePatchableAttributes> patchableProperties = EnumSet.allOf(TestCasePatchableAttributes.class);

    public TestCasePatcher() {
    }

    @Override
    EnumSet<? extends PatchableAttributes> getPatchableAttributes() {
        return patchableProperties;
    }
}