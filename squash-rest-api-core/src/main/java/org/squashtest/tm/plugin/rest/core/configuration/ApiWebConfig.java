/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.Module.SetupContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.cfg.HandlerInstantiator;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import org.hibernate.validator.HibernateValidator;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.Ordered;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.HateoasSortHandlerMethodArgumentResolver;
import org.springframework.data.web.config.HateoasAwareSpringDataWebConfiguration;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.hateoas.mediatype.MessageResolver;
import org.springframework.hateoas.mediatype.hal.CurieProvider;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule;
import org.springframework.hateoas.server.LinkRelationProvider;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import org.springframework.hateoas.server.mvc.RepresentationModelProcessorInvoker;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewInterceptor;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.context.request.WebRequestInterceptor;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.JsonViewRequestBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.JsonViewResponseBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import org.squashtest.tm.plugin.rest.core.jackson.EntityManagerWrapper;
import org.squashtest.tm.plugin.rest.core.jackson.ModuleExclusionObjectMapper;
import org.squashtest.tm.plugin.rest.core.jackson.PersistentValueInstantiators;
import org.squashtest.tm.plugin.rest.core.jackson.SquashRestHalHandlerInstantiator;
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService;
import org.squashtest.tm.plugin.rest.core.service.impl.NodeHierarchyHelpServiceImpl;
import org.squashtest.tm.plugin.rest.core.utils.DeserializationConfigHelper;
import org.squashtest.tm.plugin.rest.core.web.BasePathAwareLinkBuildingService;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.BasicResourceAssembler;
import org.squashtest.tm.plugin.rest.core.web.ContentInclusionArgumentResolver;
import org.squashtest.tm.plugin.rest.core.web.DeserializationConfigurationRequestBodyAdvice;
import org.squashtest.tm.plugin.rest.core.web.DisableableSortResolver;
import org.squashtest.tm.plugin.rest.core.web.DynamicFilterResponseBodyAdvice;
import org.squashtest.tm.plugin.rest.core.web.PersistentEntityArgumentResolver;
import org.squashtest.tm.plugin.rest.core.web.PersistentEntityJacksonHttpMessageConverter;
import org.squashtest.tm.plugin.rest.core.web.ProjectTypeFilterArgumentResolver;
import org.squashtest.tm.plugin.rest.core.web.RestApiHandlerAdapter;
import org.squashtest.tm.plugin.rest.core.web.RestApiHandlerMapping;
import org.squashtest.tm.plugin.rest.core.web.RestApiRelProvider;
import org.squashtest.tm.plugin.rest.core.web.TestCaseTypeFilterArgumentResolver;

import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletContext;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;



/**
 * Configuration class that sets the servlet up. Most of the code is taken from Spring Data Rest project configuration class, RepositoryRestMvcConfiguration.
 *
 * @author bsiri
 *
 */
@Configuration
@EnableConfigurationProperties(RestApiProperties.class)
@SuppressWarnings("unchecked")
public class ApiWebConfig extends HateoasAwareSpringDataWebConfiguration{

	// inherited constructor
	public ApiWebConfig(ApplicationContext context, @Qualifier("mvcConversionService") ObjectFactory<ConversionService> conversionService) {
		super(context, conversionService);
	}

	@Autowired(required=false)
	private Collection<RepresentationModelProcessor<?>> resourceProcessors = new ArrayList<>();

	@Inject
	private ApplicationContext appContext;


	// this is the main source of jackson module configuration, brought gracefully to you
	// by JacksonAutoConfiguration. Note sure who imports @JacksonAutoConfiguration though,
	// probably the main app since we did not explicitly disable it.
	@Inject
	private Jackson2ObjectMapperBuilder objectMapperBuilder;


	@Autowired
	@Lazy
	private Collection<SquashRestApiJacksonModuleConfigurer> jacksonConfigurers;

	@Bean
	public LinkRelationProvider relProvider(){
		return new RestApiRelProvider();
	}


	/*
	 * Some beans here are private (we don't want to publish them in the bean factory). So we cannot use @Bean on their factory method.
	 * Thus, in order to prevent multiple creation of the same beans over and over we need to cache them. Like memoization, done manually here.
	 *
	 * I'm sorry if many kittens died in the process.
	 */

	private final Map<String, Object> memoizeMap = new HashMap<>();



	// ******************* a couple of non-web services *******************
	// if there are more to come, consider moving them in a separate config class

	@Bean
	public EntityManagerWrapper entityManagerWrapper(){
		return new EntityManagerWrapper();
	}

	@Bean
	public NodeHierarchyHelpService nodeHierarchyHelpService(){
		return new NodeHierarchyHelpServiceImpl();
	}

	// ******************* main configuration *********************

	/**
	 * The main MappingHandler instance. Its main purpose is to expose the controller handlers on URIs that are prefixed by the base path of the
	 * API, among other things.
	 *
	 * @param properties
	 * @param emf
	 * @return
	 */
	@Bean
	public HandlerMapping squashRestHandlerMapping(RestApiProperties properties, EntityManagerFactory emf){

		RestApiHandlerMapping mapping = new RestApiHandlerMapping(properties);

		mapping.addInterceptor(squashRestEntityManagerInViewInterceptor(emf));
		mapping.setOrder(Ordered.LOWEST_PRECEDENCE - 1000);


		return mapping;
	}



	/**
	 * The HandlerAdapter, that will pre/post process the argument/response of our RestApiHandlerMapping. An important mission
	 * is indeed to keep all the plumbing in this configuration class hidden from the rest of the platform, and it does so
	 * by kicking-in only when the controller is one of ours.
	 *
	 * @return
	 */
	@Bean
	public RequestMappingHandlerAdapter squashRestHandlerAdapter(){

		ConfigurableWebBindingInitializer initializer = new ConfigurableWebBindingInitializer();
		initializer.setConversionService(squashRestConversionService());

		RestApiHandlerAdapter handlerAdapter = new RestApiHandlerAdapter(squashRestResourceProcessorInvoker());

		handlerAdapter.setCustomArgumentResolvers(squashRestArgumentResolvers());
		handlerAdapter.setWebBindingInitializer(initializer);
		handlerAdapter.setMessageConverters(squashRestMessageConverters());

		// support for @JacksonView (see WebMvcConfigurationSupport#RequestMappingHandlerAdapter)
		handlerAdapter.setRequestBodyAdvice(requestBodyAdvices());
		handlerAdapter.setResponseBodyAdvice(responseBodyAdvices());

		return handlerAdapter;
	}

	@Bean
	public BasePathAwareLinkBuildingService linkService(ServletContext servletContext, RestApiProperties properties) {
		return new BasePathAwareLinkBuildingService(servletContext, properties, relProvider(), appContext);
	}


	@Bean
	public ExceptionHandlerExceptionResolver squashRestExceptionResolver(){

		ExceptionHandlerExceptionResolver resolver = new ExceptionHandlerExceptionResolver();
		resolver.setMappedHandlerClasses(BaseRestController.class);
		resolver.setCustomArgumentResolvers(squashRestArgumentResolvers());
		resolver.setMessageConverters(squashRestMessageConverters());

		return resolver;
	}

	// **************** private beans ********************************

	protected List<HandlerMethodArgumentResolver> squashRestArgumentResolvers(){

		return memoized("squashRestArgumentResolvers", new PrivateBeanCreator<List<HandlerMethodArgumentResolver>>() {
			@Override
			public List<HandlerMethodArgumentResolver> create() {

				List<HandlerMethodArgumentResolver> argumentResolvers = new ArrayList<>();

				HateoasPageableHandlerMethodArgumentResolver pageableResolver = pageableResolver();
				HateoasSortHandlerMethodArgumentResolver sortResolver = sortResolver();
				ContentInclusionArgumentResolver includeResolver = new ContentInclusionArgumentResolver();
				TestCaseTypeFilterArgumentResolver testCaseTypeFilterResolver = new TestCaseTypeFilterArgumentResolver();
				ProjectTypeFilterArgumentResolver projectTypeFilterArgumentResolver = new ProjectTypeFilterArgumentResolver();

				// That argument resolver cares for annotation @PersistentEntity
				PersistentEntityArgumentResolver persistenEntityResolver = new PersistentEntityArgumentResolver(squashRestMessageConverters(), requestBodyAdvices());
				persistenEntityResolver.setConfigHelper(deserializationConfigHelper());

				argumentResolvers.add(pageableResolver);
				argumentResolvers.add(sortResolver);
				argumentResolvers.add(includeResolver);
				argumentResolvers.add(persistenEntityResolver);
				argumentResolvers.add(testCaseTypeFilterResolver);
				argumentResolvers.add(projectTypeFilterArgumentResolver);

				return argumentResolvers;
			}
		});
	}


	@Bean
	@Override
	public HateoasSortHandlerMethodArgumentResolver sortResolver() {
		return new DisableableSortResolver();
	}


	protected WebRequestInterceptor squashRestEntityManagerInViewInterceptor(EntityManagerFactory emf){
		// not memoizable - but this is called only once anyway
		OpenEntityManagerInViewInterceptor interceptor = new OpenEntityManagerInViewInterceptor();
		interceptor.setEntityManagerFactory(emf);
		return interceptor;
	}

	protected List<RequestBodyAdvice> requestBodyAdvices(){
		return memoized("requestBodyAdvices", new PrivateBeanCreator<List<RequestBodyAdvice>>() {
			@Override
			public List<RequestBodyAdvice> create() {
				List<RequestBodyAdvice> requestBodyAdvices = new ArrayList<RequestBodyAdvice>();
				requestBodyAdvices.add(new JsonViewRequestBodyAdvice());
				requestBodyAdvices.add(new DeserializationConfigurationRequestBodyAdvice());
				return requestBodyAdvices;
			}
		});
	}

	protected List<ResponseBodyAdvice<?>> responseBodyAdvices(){
		return memoized("responseBodyAdvices", new PrivateBeanCreator<List<ResponseBodyAdvice<?>>>() {
			@Override
			public List<ResponseBodyAdvice<?>> create() {

				List<ResponseBodyAdvice<?>> responseBodyAdvices = new ArrayList<ResponseBodyAdvice<?>>();
				responseBodyAdvices.add(new JsonViewResponseBodyAdvice());
				responseBodyAdvices.add(new DynamicFilterResponseBodyAdvice());
				return responseBodyAdvices;

			}
		});

	}

	protected DeserializationConfigHelper deserializationConfigHelper(){
		return memoized("deserializationConfigHelper", new PrivateBeanCreator<DeserializationConfigHelper>() {
			@Override
			public DeserializationConfigHelper create() {
				return new DeserializationConfigHelper(entityManagerWrapper(), nodeHierarchyHelpService());
			}
		});
	}


	// ----------------- jackson-related configuration -----------------

	protected List<HttpMessageConverter<?>> squashRestMessageConverters(){
		return memoized("squashRestMessageConverters", new PrivateBeanCreator<List<HttpMessageConverter<?>>>() {
			@Override
			public List<HttpMessageConverter<?>> create() {

				List<HttpMessageConverter<?>> converters = new ArrayList<>();
				converters.add(restHttpMessageConverter());
				converters.add(new ByteArrayHttpMessageConverter());
				return converters;

			}
		});
	}

	/**
	 *  default object mapper, that applies to non-HATEOAS resources (like regular pojos)
	 * @return
	 */
	protected ObjectMapper basicRestObjectMapper(){

		return memoized("basicRestObjectMapper", new PrivateBeanCreator<ObjectMapper>() {
			@Override
			public ObjectMapper create() {
				// no memoize on it - we need separate, distinct instances because two distinct customization
				// will depend on it

				ObjectMapper mapper = new ModuleExclusionObjectMapper();
				mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(SerializationFeature.FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS, false);
				mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
				mapper.setDateFormat(new StdDateFormat()); // setDateFormat is redundant but at least it's explicit

				/*
				 * Add all standard modules that linger in the classpath
				 * except of course those that the ModuleExclusionObjectMapper will reject.
				 * As far as I can tell, for now it registers the following :
				 * - JodaModule
				 * - Hibernate5Module (declared in code Squash : SquashServletConfig
				 * - a few others (GeoModule, JsonComponentModule etc)
				 */
				objectMapperBuilder.configure(mapper);

				/*
				 * More modules. Note : it matters that the SquashRestApiJacksonModule is registered last,
				 * because of the TryUnwrappingWhenPossibleHibernateProxySerializer (read the javadoc to know why).
				 * By registering it last we make sure our serializer is on top of the list
				 */
				mapper.registerModule(new Jackson2HalModule());

				List<SquashRestApiJacksonModuleConfigurer> configurers = new ArrayList<>();
				configurers.addAll(jacksonConfigurers);
				configurers.add(new SquashRestApiJacksonModuleConfigurer() {
					@Override
					public void setupModule(SetupContext context) {
						context.addValueInstantiators(persistentValueInstantiators());
					}
				});
				mapper.registerModule(new SquashRestApiJacksonModule(configurers));

				// override the handler instantiator
				mapper.setHandlerInstantiator(restHandlerInstantiator());


				return mapper;
			}
		});
	}

	public PersistentValueInstantiators persistentValueInstantiators(){
		return memoized("persistentValueInstantiators", new PrivateBeanCreator<PersistentValueInstantiators>() {
			@Override
			public PersistentValueInstantiators create() {
				return new PersistentValueInstantiators(entityManagerWrapper());
			}
		});
	}

	protected MappingJackson2HttpMessageConverter restHttpMessageConverter(){

		return memoized("restHttpMessageConverter", new PrivateBeanCreator<MappingJackson2HttpMessageConverter>() {
			@Override
			public MappingJackson2HttpMessageConverter create() {

				ArrayList<MediaType> mediaTypes = new ArrayList<>();
				mediaTypes.add(mediaType("application/json"));

				MappingJackson2HttpMessageConverter converter = new PersistentEntityJacksonHttpMessageConverter();
				converter.setObjectMapper(basicRestObjectMapper());
				converter.setSupportedMediaTypes(mediaTypes);

				return converter;

			}
		});
	}

	protected HandlerInstantiator restHandlerInstantiator(){
		return memoized("restHandlerInstantiator",() -> {
				return new SquashRestHalHandlerInstantiator(relProvider(),
					CurieProvider.NONE,
					resourceDescriptionMessageSourceAccessor(),
					appContext.getAutowireCapableBeanFactory()
				);
		});
	}


	/*
	 * copy/pasta from SDR again
	 */
	public MessageResolver resourceDescriptionMessageSourceAccessor() {

		// for now, no big deal
		return MessageResolver.of(null);
	}

	@Bean
	public BasicResourceAssembler squashRestResourceAssembler(){
		return memoized("squashRestResourceAssembler", new PrivateBeanCreator<BasicResourceAssembler>() {
			@Override
			public BasicResourceAssembler create() {
				return new BasicResourceAssembler();
			}
		});
	}


    @Bean
    public Validator validator() {
        ValidatorFactory factory = Validation.byProvider(HibernateValidator.class)
            .configure()
            .propertyNodeNameProvider(new JacksonPropertyNodeNameProvider())
            .defaultLocale(Locale.ENGLISH)
            .buildValidatorFactory();

        return factory.getValidator();
    }

	// ----------------- more Spring-HATEOAS related configuration -----------------


	protected DefaultFormattingConversionService squashRestConversionService(){


		return memoized("squashRestConversionService", new PrivateBeanCreator<DefaultFormattingConversionService>() {
			@Override
			public DefaultFormattingConversionService create() {

				DefaultFormattingConversionService conversionService =  new DefaultFormattingConversionService();

				// no other converters for now, we'll see if we need more later on

				return conversionService;
			}
		});
	}

	/**
	 * The bean that will post-process the controller handlers with {@link ResourceProcessor}s, if any. See Spring HATEOAS for more.
	 *
	 * @return
	 */
	/*
	 *  NOTE : for now, none are defined yet :-) Still, by writing this down now I prevent myself from reworking that part of the
	 *  Spring plumbing I might not remember 6 months later
	 */
	protected RepresentationModelProcessorInvoker squashRestResourceProcessorInvoker(){

		return memoized("squashRestResourceProcessorInvoker", new PrivateBeanCreator<RepresentationModelProcessorInvoker>() {
			@Override
			public RepresentationModelProcessorInvoker create() {
				return new RepresentationModelProcessorInvoker(resourceProcessors);
			}
		});
	}



	private static final MediaType mediaType(String mtype){
		return MediaType.valueOf(mtype);
	}


	// ********************** Poor's man memoization ***************************


	private <T>  T memoized(String key, PrivateBeanCreator<T> creator){
		if (! memoizeMap.containsKey(key)){
			T bean = creator.create();
			memoizeMap.put(key, bean);
		}
		return (T)memoizeMap.get(key);
	}

	// Aaaah, I long java 8 so much.
	@FunctionalInterface
	private static interface PrivateBeanCreator<T>{
		T create();
	}
}
