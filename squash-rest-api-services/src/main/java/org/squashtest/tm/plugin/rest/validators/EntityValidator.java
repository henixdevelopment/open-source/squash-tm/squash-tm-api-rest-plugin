/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class EntityValidator implements Validator {

    private static final String CODE_INVALID_ID = "invalid id";
    private static final String ID = "id";
    private static final String MESSAGE_NO_ENTITY_KNOWN = "No entity found for type %s and id %d.";

    @PersistenceContext
    protected EntityManager entityManager;

    public void checkEntityExist(Errors errors, Class entityClass, long entityId) {
        Object entity = entityManager.find(entityClass, entityId);
        if (entity == null) {
            errors.rejectValue(
                    ID,
                    CODE_INVALID_ID,
                    String.format(MESSAGE_NO_ENTITY_KNOWN, entityClass.getSimpleName(), entityId));
        }
    }
}
