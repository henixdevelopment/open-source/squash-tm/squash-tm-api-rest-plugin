/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.springframework.restdocs.payload.JsonFieldType
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.squashtest.tm.domain.campaign.CampaignFolder
import org.squashtest.tm.domain.campaign.CampaignLibraryNode
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto
import org.squashtest.tm.plugin.rest.service.RestCampaignFolderService
import org.squashtest.tm.plugin.rest.service.RestIssueService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.library.PathService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject
import static org.hamcrest.Matchers.hasSize
import static org.hamcrest.Matchers.is
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project

/**
 * Created by bsiri on 06/07/2017.
 */
@WebMvcTest(RestCampaignFolderController)
class RestCampaignFolderControllerIT extends BaseControllerSpec {

    @Inject
    private RestCampaignFolderService service
    @Inject
    private PathService pathService
    @Inject
    private NodeHierarchyHelpService hierService
    @Inject
    private CustomFieldValueFinderService cufService
    @Inject
    private RestCampaignFolderService restCampaignFolderService

    @Inject
    private RestIssueService restIssueService


    def "browse-campaign-folders"() {

        given:
        service.findAllReadable(_) >> { args ->
            def folders = [
                    SquashEntityBuilder.campaignFolder {
                        id = 100L
                        name = "qualification"
                    },
                    SquashEntityBuilder.campaignFolder {
                        id = 101L
                        name = "CP-18.01"
                    },
                    SquashEntityBuilder.campaignFolder {
                        id = 102L
                        name = "DX-U17"
                    }
            ]

            new PageImpl<CampaignFolder>(folders, args[0], 10)
        }

        when:
        def res = mockMvc.perform(get("/api/rest/latest/campaign-folders?page=1&size=3")
                .header("Accept", "application/json"))

        then:

        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.campaign-folders".hasSize 3
            "_embedded.campaign-folders".test {
                "[0]".test {
                    "_type".is "campaign-folder"
                    "id".is 100
                    "name".is "qualification"
                    selfRelIs "http://localhost:8080/api/rest/latest/campaign-folders/100"
                }
                "[1]".test {
                    "_type".is "campaign-folder"
                    "id".is 101
                    "name".is "CP-18.01"
                    selfRelIs "http://localhost:8080/api/rest/latest/campaign-folders/101"
                }
                "[2]".test {
                    "_type".is "campaign-folder"
                    "id".is 102
                    "name".is "DX-U17"
                    selfRelIs "http://localhost:8080/api/rest/latest/campaign-folders/102"
                }
            }

            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 1
            }

            "_links".test {
                "first".linksTo "http://localhost:8080/api/rest/latest/campaign-folders?page=0&size=3"
                "prev".linksTo "http://localhost:8080/api/rest/latest/campaign-folders?page=0&size=3"
                "self".linksTo "http://localhost:8080/api/rest/latest/campaign-folders?page=1&size=3"
                "next".linksTo "http://localhost:8080/api/rest/latest/campaign-folders?page=2&size=3"
                "last".linksTo "http://localhost:8080/api/rest/latest/campaign-folders?page=3&size=3"
            }
        }

        // document
        res.andDo(doc.document(
                DocumentationSnippets.AllInOne.createBrowseAllEntities("campaign-folders")
        ))

    }

    def "browse-campaign-folders-tree-by-project"() {

        given:
        def folderProject1 = SquashEntityBuilder.project {
            id = 10L
            name = "project-1"

        }
        def folderProject2 = SquashEntityBuilder.project {
            id = 11L
            name = "project-2"

        }
        def folders = [
                SquashEntityBuilder.campaignFolder {
                    project = folderProject1
                    id = 100L
                    name = "folder1"
                    description = "<p>An embedded folder...</p>"
                    createdBy "User-1"
                    createdOn "2017/07/19"
                    lastModifiedBy "User-2"
                    lastModifiedOn "2017/07/20"

                },
                SquashEntityBuilder.campaignFolder {
                    project = folderProject1
                    id = 101L
                        name = "folder2"
                        description = "<p>An embedded folder...</p>"
                        createdBy "User-1"
                        createdOn "2017/07/19"
                        lastModifiedBy "User-2"
                        lastModifiedOn "2017/07/20"

                },
                SquashEntityBuilder.campaignFolder {
                    project = folderProject2
                    id = 102L
                        name = "folder3"
                        description = "<p>An embedded folder...</p>"
                        createdBy "User-1"
                        createdOn "2017/07/19"
                        lastModifiedBy "User-2"
                        lastModifiedOn "2017/07/20"

                }
        ]

        and:
        service.findCampaignFolderByProject(_) >> folders;

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/campaign-folders/tree/{ids}", "10,11")
                .header("Accept", "application/json"))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("[0]._type", is("project")))
                .andExpect(jsonPath("[0].id", is(10)))
                .andExpect(jsonPath("[0].name", is("project-1")))
                .andExpect(jsonPath("[0].folders[0]._type", is("campaign-folder")))
                .andExpect(jsonPath("[0].folders[0].id", is(100)))
                .andExpect(jsonPath("[0].folders[0].name", is("folder1")))
                .andExpect(jsonPath("[0].folders[0].url", is("http://localhost:8080/api/rest/latest/campaign-folders/100")))
                .andExpect(jsonPath("[0].folders[0].children", hasSize(0)))

        /*
         * Documentation
         */
        res.andDo(doc.document(

                pathParameters(
                        parameterWithName("ids").description("the ids of the projects")
                ),
                responseFields(
                        fieldWithPath("[]._type").type(JsonFieldType.STRING).description("the type of the entity"),
                        fieldWithPath("[].id").type(JsonFieldType.NUMBER).description("id of project"),
                        fieldWithPath("[].name").type(JsonFieldType.STRING).description("name of project"),
                        fieldWithPath("[].folders").type(JsonFieldType.ARRAY).description("all folders for the given project"),
                        fieldWithPath("[].folders[]._type").type(JsonFieldType.STRING).description("the type of the entity"),
                        fieldWithPath("[].folders[].id").type(JsonFieldType.NUMBER).description("id of the campaign folder"),
                        fieldWithPath("[].folders[].name").type(JsonFieldType.STRING).description("name of the campaign folder"),
                        fieldWithPath("[].folders[].url").type(JsonFieldType.STRING).description("url of the campaign folder"),
                        subsectionWithPath("[].folders[].children").description("children folders")
                ),

        ))

    }

    def "get-campaign-folder"() {

        given:
        def folderProject = SquashEntityBuilder.project {
            id = 10L
            name = "Mangrove"
            campaignLibrary = SquashEntityBuilder.campLibrary {}
        }

        def folder = SquashEntityBuilder.campaignFolder {
            project = folderProject
            id = 24L
            name = "old"
            description = "<p>where all the old campaigns go</p>"
            createdBy "User-1"
            createdOn "2011/09/30"
            lastModifiedBy "admin"
            lastModifiedOn "2017/06/16"
        }

        and:
        service.getOne(24L) >> folder

        hierService.findParentFor(folder) >> folderProject.campaignLibrary

        pathService.buildCampaignPath(24L) >> "/${folderProject.name}/${folder.name}"

        cufService.findAllCustomFieldValues(folder) >> [
                SquashEntityBuilder.cufValue {
                    code = "CF_TXT"
                    inputType "PLAIN_TEXT"
                    label = "test level"
                    value = "mandatory"
                },
                SquashEntityBuilder.cufValue {
                    code = "CF_TAGS"
                    inputType "TAG"
                    label = "see also"
                    value = ["walking", "bipedal"]
                }
        ]

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/campaign-folders/{id}", 24L)
                .header("Accept", "application/json"))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "campaign-folder"
            "id".is 24
            "name".is "old"
            "project".test {
                "_type".is "project"
                "id".is 10
                "name".is "Mangrove"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/10"
            }
            "path".is "/Mangrove/old"
            "parent".test {
                "_type".is "project"
                "id".is 10
                "name".is "Mangrove"
            }
            // Custom Fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CF_TXT"
                    "label".is "test level"
                    "value".is "mandatory"
                }
                "[1]".test {
                    "code".is "CF_TAGS"
                    "label".is "see also"
                    "value".contains "walking", "bipedal"
                }
            }
            "created_by".is "User-1"
            "created_on".is "2011-09-30T10:00:00.000+00:00"
            "last_modified_by".is "admin"
            "last_modified_on".is "2017-06-16T10:00:00.000+00:00"
            "description".is "<p>where all the old campaigns go</p>"
            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/campaign-folders/24"
                "project".linksTo "http://localhost:8080/api/rest/latest/projects/10"
                "content".linksTo "http://localhost:8080/api/rest/latest/campaign-folders/24/content"
                "issues".linksTo "http://localhost:8080/api/rest/latest/campaign-folders/24/issues"
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the campaign case folder"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        add DocumentationSnippets.DescriptorLists.regularLibraryNodeFields
                        add "custom_fields (array) : the custom fields of that campaign folder"
                        add "custom_fields[].label (string) : the label of the custom field"
                        add "custom_fields[].code (string) : the code of the custom field"
                        add "custom_fields[].value (varies) : the value of the custom field. The value is either a string (for most custom fields), or an array of strings (for multivalued custom fields eg a tag list)"

                    }
                    _links {
                        add "self : the link to this folder"
                        add "project : the link to its project"
                        add "content : the link to its content"
                        add "attachments : the link to its attachments"
                        add "issues : the link to its issues"
                    }
                }
        ))


    }

    def "get-campaign-folder-content"() {

        given:
        def content = [
                SquashEntityBuilder.campaign {
                    id = 13L
                    name = "non regression"
                },
                SquashEntityBuilder.campaign {
                    id = 150L
                    name = "new features"
                },
                SquashEntityBuilder.campaignFolder {
                    id = 1467L
                    name = "non-standard environment acceptance tests"
                }]

        and:
        service.findFolderContent(180L, _) >> { args -> new PageImpl<CampaignLibraryNode>(content, args[1], 3) }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/campaign-folders/{id}/content", 180)
                .header("Accept", "application/json"))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))

        withResult(res) {
            "_embedded.content".hasSize 3
            "_embedded.content".test {
                "[0]".test {
                    "_type".is "campaign"
                    "id".is 13
                    "name".is "non regression"
                    selfRelIs "http://localhost:8080/api/rest/latest/campaigns/13"
                }
                "[1]".test {
                    "_type".is "campaign"
                    "id".is 150
                    "name".is "new features"
                    selfRelIs "http://localhost:8080/api/rest/latest/campaigns/150"
                }
                "[2]".test {
                    "_type".is "campaign-folder"
                    "id".is 1467
                    "name".is "non-standard environment acceptance tests"
                    selfRelIs "http://localhost:8080/api/rest/latest/campaign-folders/1467"
                }
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                DocumentationSnippets.AllInOne.createListEntityContent("content", "campaign-folder", true)
        ))

    }


    def "get-campaign-folder-issues"() {

        given:

        def executions =  [
            SquashEntityBuilder.execution {
                id = 2L
                executionStatus "BLOCKED"
                lastExecutedBy = "User-1"
                lastExecutedOn "2017/06/24"
            }] as Set


        def issue1 = new IssueDto(
            "165",
            new URL ("http://192.175.1.51/bugzilla/show_bug.cgi?id=165"),
            executions
        )

        def issues = new PageImpl<IssueDto>([issue1], PageRequest.of(0, 20), 1)


        def executionIds = [2L]

        and:
        service.getExecutionIdsByCampaignFolder(_) >> executionIds
        restIssueService.getIssuesFromExecutionIds(_,_) >> issues


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders
            .get("/api/rest/latest/campaign-folders/{id}/issues", 1)
            .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.issues".hasSize 1
            "_embedded.issues".test {
                "[0]".test {
                    "remoteIssueId".is "165"
                    "url".is "http://192.175.1.51/bugzilla/show_bug.cgi?id=165"
                    "executions".hasSize 1
                    "executions".test {
                        "[0]".test {
                            "_type".is "execution"
                            "id".is 2
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/2"
                        }
                    }
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/campaign-folders/1/issues?page=0&size=20"
            "page".test {
                "size".is 20
                "totalElements".is 1
                "totalPages".is 1
                "number".is 0
            }

        }


        /*
     * Documentation
     */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the campaign folder"
                }
                requestParams {
                    add DocumentationSnippets.DescriptorLists.paginationParams
                    add DocumentationSnippets.DescriptorLists.fieldsParams
                }
                fields {
                    embeddedAndStop "issues (array) : the issues of this campaign folder"
                    add DocumentationSnippets.DescriptorLists.issuesFields
                    add DocumentationSnippets.DescriptorLists.paginationFields
                    add DocumentationSnippets.DescriptorLists.linksFields
                }
                _links {
                    add DocumentationSnippets.DescriptorLists.paginationLinks
                }
            }
        ))
    }



    def "post-campaign-folder"() {

        given:
        def json = """{
                "_type" : "campaign-folder",
                "name" : "Campaign folder 1",
                "custom_fields": [
                            {
                                "code": "cuf1",
                                "value": "Cuf1 Value"
                            }],
                "parent" : {
                    "_type" : "project",
                    "id" : 14
                }
            }
            """
        def proj = project {
            id = 14L
            name = "Test Project 1"
        }
        def cf = SquashEntityBuilder.campaignFolder {
            id = 33L
            name = "Campaign folder 1"
            project = proj
        }
        def lCuf = [
                SquashEntityBuilder.cufValue {
                    code = "cuf1"
                    label = "Lib Cuf1"
                    value = "Cuf1 Value"
                },
                SquashEntityBuilder.cufValue {
                    code = "cuf2"
                    label = "Lib Cuf2"
                    value = "true"
                }]

        and:
        restCampaignFolderService.addCampaignFolder(_) >> cf
        cufService.findAllCustomFieldValues(_) >> lCuf
        pathService.buildCampaignPath(_) >> '/Test Project 1/Campaign folder 1'
        hierService.findParentFor(_) >> SquashEntityBuilder.campLibrary {
            project = proj
        }
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/campaign-folders")
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "campaign-folder"
            "id".is 33
            "name".is "Campaign folder 1"
            // Custom Fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "cuf1"
                    "label".is "Lib Cuf1"
                    "value".is "Cuf1 Value"
                }
                "[1]".test {
                    "code".is "cuf2"
                    "label".is "Lib Cuf2"
                    "value".is "true"
                }
            }
            "parent".test {
                "_type".is "project"
                "id".is 14
            }
        }

    }

    def "patch-campaign-folder"() {
        given:
        def json = """{
                            "_type" : "campaign-folder",
                            "name" : "Update - Campaign folder 1",
                            "description": "Update - Description Campaign folder 1",
                            "custom_fields": [
                                        {
                                             "code": "cuf2",
                                            "label": "Cuf-CaC",
                                            "value": "true"
                                        }]
                            }"""
        def proj = project {
            id = 14L
            name = "Test Project 1"
        }
        def cf = SquashEntityBuilder.campaignFolder {
            id = 33L
            name = "Update - Campaign folder 1"
            description = "Update - Description Campaign folder 1"
            project = proj
        }
        def lCuf = [
                SquashEntityBuilder.cufValue {
                    code = "cuf1"
                    label = "Lib Cuf1"
                    value = "Cuf1 Value"
                },
                SquashEntityBuilder.cufValue {
                    code = "cuf2"
                    label = "Lib Cuf2"
                    value = "true"
                }]

        and:
        restCampaignFolderService.patchCampaignFolder(_, _) >> cf
        cufService.findAllCustomFieldValues(_) >> lCuf
        pathService.buildCampaignPath(_) >> '/Test Project 1/Update - Campaign folder 1'
        hierService.findParentFor(_) >> SquashEntityBuilder.campLibrary {
            project = proj
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/campaign-folders/{id}", 33L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "campaign-folder"
            "id".is 33
            "name".is "Update - Campaign folder 1"
            "description".is "Update - Description Campaign folder 1"
            // Custom Fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "cuf1"
                    "label".is "Lib Cuf1"
                    "value".is "Cuf1 Value"
                }
                "[1]".test {
                    "code".is "cuf2"
                    "label".is "Lib Cuf2"
                    "value".is "true"
                }
            }
            "parent".test {
                "_type".is "project"
                "id".is 14
            }
        }

    }

    def "delete-campaign-folder"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/campaign-folders/{ids}", "51,52")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        restCampaignFolderService.deleteFolder([51])

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the campaign folders"
                    }
                }
        ))
    }

}
