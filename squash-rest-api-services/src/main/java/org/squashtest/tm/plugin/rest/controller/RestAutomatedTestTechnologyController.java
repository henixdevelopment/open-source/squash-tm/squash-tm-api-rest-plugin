/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.AutomatedTestTechnologyDto;
import org.squashtest.tm.plugin.rest.service.RestAutomatedTestTechnologyService;
import org.squashtest.tm.plugin.rest.validators.AutomatedTestTechnologyValidator;

import javax.inject.Inject;

@UseDefaultRestApiConfiguration
@RestApiController(AutomatedTestTechnology.class)
public class RestAutomatedTestTechnologyController extends BaseRestController {

    @Inject
    private RestAutomatedTestTechnologyService restAutomatedTestTechnologyService;

    @Inject
    private AutomatedTestTechnologyValidator automatedTestTechnologyValidator;

    @ResponseBody
    @EntityGetter
    @GetMapping("/automated-test-technologies/{id}")
    public ResponseEntity<EntityModel<AutomatedTestTechnology>> findAutomatedTestTechnology(@PathVariable("id") long technologyId) {
        AutomatedTestTechnology technology = restAutomatedTestTechnologyService.getOne(technologyId);
        EntityModel<AutomatedTestTechnology> resource = toEntityModel(technology);
        return ResponseEntity.ok(resource);
    }

    @ResponseBody
    @GetMapping("/automated-test-technologies")
    public ResponseEntity<PagedModel<EntityModel<AutomatedTestTechnology>>> findAllAutomatedTestTechnologies(Pageable pageable) {
        Page<AutomatedTestTechnology> technologies = restAutomatedTestTechnologyService.findAll(pageable);
        PagedModel<EntityModel<AutomatedTestTechnology>> result = toPagedModel(technologies);
        return ResponseEntity.ok(result);
    }

    @ResponseBody
    @PostMapping(value = "/automated-test-technologies")
    public ResponseEntity<EntityModel<AutomatedTestTechnology>> insertAutomatedTestTechnology(
            @RequestBody AutomatedTestTechnologyDto automatedTestTechnologyDto) throws BindException {
        automatedTestTechnologyValidator.validatePostAutomatedTestTechnology(automatedTestTechnologyDto);
        AutomatedTestTechnology newTechnology = restAutomatedTestTechnologyService.addAutomatedTestTechnology(automatedTestTechnologyDto);
        EntityModel<AutomatedTestTechnology> resource = toEntityModel(newTechnology);
        return ResponseEntity.status(HttpStatus.CREATED).body(resource);
    }

}
