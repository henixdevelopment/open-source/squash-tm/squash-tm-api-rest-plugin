/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.service.internal.dto.AutomatedTestPlanDTO;
import org.squashtest.tm.service.internal.dto.TriggerRequestDTO;
import org.squashtest.tm.service.license.UltimateLicenseAvailabilityService;
import org.squashtest.tm.service.testautomation.testplanretriever.CustomFieldValuesForExec;

/** Controller that returns execution order for Squash Orchestrator automation job. */
@UseDefaultRestApiConfiguration
@RestApiController()
public class RestExecutionOrderControllerRest
        extends RestAbstractExecutionOrderController<TriggerRequestDTO, CustomFieldValuesForExec> {

    @Inject private UltimateLicenseAvailabilityService ultimateLicenseService;

    @Override
    @PostMapping(
            path = "/community-trigger-iteration-automated-test",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<AutomatedTestPlanDTO> triggerIterationExecutionOrder(
            @RequestBody TriggerRequestDTO triggerRequest) throws BindException {
        if (ultimateLicenseService.isAvailable()) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }
        return getIterationResponse(triggerRequest);
    }

    @Override
    @PostMapping(
            path = "/community-trigger-test-suite-automated-test",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<AutomatedTestPlanDTO> triggerTestSuiteExecutionOrder(
            @RequestBody TriggerRequestDTO triggerRequest) throws BindException {
        if (ultimateLicenseService.isAvailable()) {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
        }
        return getTestSuiteResponse(triggerRequest);
    }
}
