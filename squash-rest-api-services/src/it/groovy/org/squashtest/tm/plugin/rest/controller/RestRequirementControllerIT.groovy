/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto
import org.squashtest.tm.plugin.rest.jackson.model.RemoteReqInfoRecord;
import org.squashtest.tm.plugin.rest.service.RestIssueService
import org.squashtest.tm.plugin.rest.service.RestRequirementVersionService
import org.squashtest.tm.plugin.rest.service.RestVerifyingTestCaseManagerService
import org.squashtest.tm.plugin.rest.service.impl.RequirementVersionPatcher
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.library.PathService
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.AllInOne
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufValue
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirement
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirementVersion

@WebMvcTest(RestRequirementController)
public class RestRequirementControllerIT extends BaseControllerSpec {

    @Inject
    private RestRequirementVersionService reqVersionService

    @Inject
    private RestVerifyingTestCaseManagerService verifyingTestCaseService

    @Inject
    private NodeHierarchyHelpService hierarchyService

    @Inject
    private CustomFieldValueFinderService cufService;

    @Inject
    private PathService pathService

    @Inject
    private PermissionEvaluationService permService

    @Inject
    private RequirementVersionPatcher requirementVersionPatcher

    @Inject
    private NodeHierarchyHelpService nodeHierarchyHelpService

    @Inject
    private RestIssueService restIssueService

    @Inject
    private RestRequirementController restRequirementController


    // ***** Browsing ***** //

    def "browse-requirements"() {

        given:

        reqVersionService.findAllReadable(_) >> { args ->

            def req = SquashEntityBuilder.requirement {
                id = 60l
                name = "sample requirement"
                resource = SquashEntityBuilder.requirementVersion {
                    id = 12l
                    reference = "REQ_SAMP"
                }

            }

            new PageImpl<Requirement>([req], args[0], 6)
        }

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/requirements?page=2&size=1").header("Accept", "application/json"))

        then:

        /*
		 * Test (with TestHelper)
		 */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded".test {
                "requirements".hasSize 1
                "requirements[0]".test {
                    "_type".is "requirement"
                    "id".is 60
                    "name".is "sample requirement"
                    "current_version".test {
                        "_type".is "requirement-version"
                        "id".is 12
                        "reference".is "REQ_SAMP"
                        selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/12"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/requirements/60"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/requirements?page=2&size=1"
            "page".test {
                "size".is 1
                "totalElements".is 6
                "totalPages".is 6
                "number".is 2
            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                AllInOne.createBrowseAllEntities("requirements")
        ))
    }

    def "get-requirement-children"() {

        given:

        def req = SquashEntityBuilder.requirement {
            id = 47l
            resource = SquashEntityBuilder.requirementVersion {
                id = 33l
                name = "sample requirement 1"
                reference = "REQ_SAMP_1"
            }
        }
        def req2 = SquashEntityBuilder.requirement {
            id = 88l
            name = "sample requirement 2"
            resource = SquashEntityBuilder.requirementVersion {
                id = 11l
                name = "sample requirement 2"
                reference = "REQ_SAMP_2"
            }
        }

        reqVersionService.findRequirementAllChildren(_, _) >> {
            args -> new PageImpl<Requirement>([req, req2], args[1], 6)
        }
        reqVersionService.findRequirementChildren(_, _) >> {
            args -> new PageImpl<Requirement>([req, req2], args[1], 6)
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/requirements/{id}/children?page=2&size=2", 99).header("Accept", "application/json"))
        then:

        /*
		 * Test (with TestHelper)
        */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded".test {
                "children".hasSize 2
                "children[0]".test {
                    "_type".is "requirement"
                    "id".is 47
                    "name".is "sample requirement 1"
                    "current_version".test {
                        "_type".is "requirement-version"
                        "id".is 33
                        "reference".is "REQ_SAMP_1"
                        selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/33"
                    }
                }
                "children[1]".test {
                    "_type".is "requirement"
                    "id".is 88
                    "name".is "sample requirement 2"
                    "current_version".test {
                        "_type".is "requirement-version"
                        "id".is 11
                        "reference".is "REQ_SAMP_2"
                        selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/11"
                    }
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/requirements/99/children?page=2&size=2"
            "page".test {
                "size".is 2
                "totalElements".is 6
                "totalPages".is 3
                "number".is 2
            }
        }

        /*
        * Documentation
        */
        res.andDo(doc.document(AllInOne.createListEntityContent("children", "requirement", true)
        ))


    }

    // ***** GETting ***** //

    def "get-sync-requirements"() {

        given:

        def mocks = loadMock("mocks/get-synchronized-requirements.groovy")

        and:

        reqVersionService.findSynchronizedRequirementsBy(_, _) >> { args -> [mocks.req1, mocks.req2] }

        hierarchyService.findParentFor(_) >> SquashEntityBuilder.requirementFolder {
            id = 6l
            resource = SquashEntityBuilder.resource {
                id = 2l
                name = "domain 1"
            }
        }

        cufService.findAllCustomFieldValues(_) >>> [mocks.req1Cufs, mocks.req2Cufs]

        pathService.buildRequirementPath(624L) >> "/sample project/domain 1/sample requirement 98-1"
        pathService.buildRequirementPath(104L) >> "/sample project/domain 1/sample requirement 108-1"

        permService.canRead(_) >> true

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/requirements?remote_key=PROJ-208&server_name=jira").header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded".test {
                "requirements".hasSize 2
                "requirements[0]".test {
                    "_type".is "requirement"
                    "id".is 624
                    "mode".is "SYNCHRONIZED"
                    selfRelIs "http://localhost:8080/api/rest/latest/requirements/624"
                }
                "requirements[1]".test {
                    "_type".is "requirement"
                    "id".is 101
                    "mode".is "SYNCHRONIZED"
                    selfRelIs "http://localhost:8080/api/rest/latest/requirements/101"
                }
            }
        }
    }

    def "get-requirement"() {

        given:

        def mocks = loadMock("mocks/get-requirement.groovy")

        and:

        reqVersionService.findRequirement(624) >> mocks.req

        hierarchyService.findParentFor(_) >> SquashEntityBuilder.requirementFolder {
            id = 6l
            resource = SquashEntityBuilder.resource {
                id = 2l
                name = "domain 1"
            }
        }

        cufService.findAllCustomFieldValues(_) >> mocks.cufs

        pathService.buildRequirementPath(624l) >> "/sample project/domain 1/sample requirement 98-3"

        permService.canRead(_) >> true

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/requirements/{id}", 624).header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            // Simple attributes
            "id".is 624
            "_type".is "requirement"

            "name".is "sample requirement 98-3"

            "project".test {
                "_type".is "project"
                "id".is 44
                "name".is "sample project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/44"
            }

            "path".is "/sample project/domain 1/sample requirement 98-3"

            "parent".test {
                "_type".is "requirement-folder"
                "id".is 6
                "name".is "domain 1"
                selfRelIs "http://localhost:8080/api/rest/latest/requirement-folders/6"
            }
            "mode".is "NATIVE"

            "current_version".test {
                "_type".is "requirement-version"
                "id".is 98
                "name".is "sample requirement 98-3"
                "reference".is "REQ01"
                "version_number".is 3

                "created_by".is "User-1"
                "created_on".is "2017-07-17T10:00:00.000+00:00"
                "last_modified_by".is "User-1"
                "last_modified_on".is "2017-07-17T10:00:00.000+00:00"

                "criticality".is "MAJOR"
                "category".test {
                    "code".is "CAT_FUNCTIONAL"
                }
                "status".is "WORK_IN_PROGRESS"
                "description".is "<p>Description of the sample requirement.</p>"

                "verifying_test_cases".hasSize 3
                "verifying_test_cases".test {
                    "[0]".test {
                        "_type".is "test-case"
                        "id".is 100
                        "name".is "sample test case 1"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/100"
                    }
                    "[1]".test {
                        "_type".is "scripted-test-case"
                        "id".is 102
                        "name".is "sample scripted test case 2"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/102"
                    }
                    "[2]".test {
                        "_type".is "keyword-test-case"
                        "id".is 103
                        "name".is "sample keyword test case 3"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/103"
                    }
                }

                "custom_fields".hasSize 2
                "custom_fields".test {
                    "[0]".test {
                        "code".is "CF_TXT"
                        "label".is "cuf text"
                        "value".is "text value"
                    }
                    "[1]".test {
                        "code".is "CF_TAG"
                        "label".is "cuf tag"
                        "value".contains "tag_1", "tag_2"
                    }
                }

                selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/98"
            }

            "versions".hasSize 3
            "versions".test {
                "[0]".test {
                    "_type".is "requirement-version"
                    "id".is 78
                    "name".is "sample requirement 98-1"
                    "version_number".is 1
                    selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/78"
                }
                "[1]".test {
                    "_type".is "requirement-version"
                    "id".is 88
                    "name".is "sample requirement 98-2"
                    "version_number".is 2
                    selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/88"
                }
                "[2]".test {
                    "_type".is "requirement-version"
                    "id".is 98
                    "name".is "sample requirement 98-3"
                    "version_number".is 3
                    selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/98"
                }
            }

            selfRelIs "http://localhost:8080/api/rest/latest/requirements/624"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/44"
                "current_version.href".is "http://localhost:8080/api/rest/latest/requirement-versions/98"
                "issues.href".is "http://localhost:8080/api/rest/latest/requirements/624/issues"

            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the requirement"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (number) : the id of the requirement"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the current (latest) requirement version of this requirement"
                        addAndStop "project (object) : the project which this requirement belongs to"
                        add "path (string) : the path of this requirement"
                        addAndStop "parent (object) : the parent node of this requirement"
                        add "mode (string) : the management mode of the requirement"
                        add "remote_req_id (string) : distant requirement id, null if mode is native"
                        add "remote_req_url (string) : distant requirement url, null if mode is native"
                        add "remote_req_perimeter_status (string) : distant requirement perimeter status, null if mode is native"
                        addAndStop "current_version (object) : the current requirement version of this requirement"
                        addAndStop "versions (array) : the requirement versions of this requirement"

                        add DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this requirement"
                        add "project : link to the project this requirement belongs to"
                        add "current_version : link to the current version of this requirement"
                        add "issues : link to the issues of this requirement"
                    }
                }
        ))
    }

    def "get-sync-requirement"() {

        given:

        def mocks = loadMock("mocks/get-sync-requirement.groovy")

        def remoteReqInfo = new RemoteReqInfoRecord(
            "any string",
            "https://gitlab.com/custom-endpoint",
            "IN_CURRENT_PERIMETER"
        )

        and:

        reqVersionService.findRequirement(624) >> mocks.req

        hierarchyService.findParentFor(_) >> SquashEntityBuilder.requirementFolder {
            id = 6l
            resource = SquashEntityBuilder.resource {
                id = 2l
                name = "domain 1"
            }
        }

        cufService.findAllCustomFieldValues(_) >> mocks.cufs

        pathService.buildRequirementPath(624l) >> "/sample project/domain 1/sample requirement 98-3"

        permService.canRead(_) >> true

        reqVersionService.getRemoteReqInfoByRequirementId(_) >> remoteReqInfo

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/requirements/{id}", 624).header("Accept", "application/json"))

        then:

        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {

            "id".is 624
            "_type".is "requirement"

            "name".is "sample requirement 98-3"

            "project".test {
                "_type".is "project"
                "id".is 44
                "name".is "sample project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/44"
            }

            "path".is "/sample project/domain 1/sample requirement 98-3"

            "parent".test {
                "_type".is "requirement-folder"
                "id".is 6
                "name".is "domain 1"
                selfRelIs "http://localhost:8080/api/rest/latest/requirement-folders/6"
            }
            "mode".is "SYNCHRONIZED"
            "remote_req_id".is "any string"
            "remote_req_url".is "https://gitlab.com/custom-endpoint"
            "remote_req_perimeter_status".is "IN_CURRENT_PERIMETER"

            "current_version".test {
                "_type".is "requirement-version"
                "id".is 98
                "name".is "sample requirement 98-3"
                "reference".is "REQ01"
                "version_number".is 3

                "created_by".is "User-1"
                "created_on".is "2017-07-17T10:00:00.000+00:00"
                "last_modified_by".is "User-1"
                "last_modified_on".is "2017-07-17T10:00:00.000+00:00"

                "criticality".is "MAJOR"
                "category".test {
                    "code".is "CAT_FUNCTIONAL"
                }
                "status".is "WORK_IN_PROGRESS"
                "description".is "<p>Description of the sample requirement.</p>"

                "verifying_test_cases".hasSize 0
                "custom_fields".hasSize 0

                selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/98"
            }

            "versions".hasSize 1
            "versions".test {
                "[0]".test {
                    "_type".is "requirement-version"
                    "id".is 98
                    "name".is "sample requirement 98-3"
                    "version_number".is 3
                    selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/98"
                }
            }

            selfRelIs "http://localhost:8080/api/rest/latest/requirements/624"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/44"
                "current_version.href".is "http://localhost:8080/api/rest/latest/requirement-versions/98"
                "issues.href".is "http://localhost:8080/api/rest/latest/requirements/624/issues"

            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the requirement"
                }

                requestParams {
                    add DescriptorLists.fieldsParams
                }

                fields {
                    add "id (number) : the id of the requirement"
                    add "_type (string) : the type of the entity"
                    add "name (string) : the name of the current (latest) requirement version of this requirement"
                    addAndStop "project (object) : the project which this requirement belongs to"
                    add "path (string) : the path of this requirement"
                    addAndStop "parent (object) : the parent node of this requirement"
                    add "mode (string) : the management mode of the requirement"
                    add "remote_req_id (string) : distant requirement id, null if mode is native"
                    add "remote_req_url (string) : distant requirement url, null if mode is native"
                    add "remote_req_perimeter_status (string) : distant requirement perimeter status, null if mode is native"
                    addAndStop "current_version (object) : the current requirement version of this requirement"
                    addAndStop "versions (array) : the requirement versions of this requirement"

                    add DescriptorLists.linksFields
                }

                _links {
                    add "self : link to this requirement"
                    add "project : link to the project this requirement belongs to"
                    add "current_version : link to the current version of this requirement"
                    add "issues : link to the issues of this requirement"
                }
            }
        ))
    }

    def "get-requirement-issues"() {
        given:

        def reqVer1 = requirementVersion {
            id = 78l
            name = "sample requirement 98-1"
            versionNumber = 1
        }

        def reqVer2 = requirementVersion {
            id = 88l
            name = "sample requirement 98-2"
            versionNumber = 2
        }

        def req = requirement {

            id = 624l
            name = "sample requirement"

            resource = reqVer2

            versions = [
                reqVer1,
                reqVer2
            ]

        }

        def executions =  [
            SquashEntityBuilder.execution {
                id = 2L
                executionStatus "BLOCKED"
                lastExecutedBy = "User-1"
                lastExecutedOn "2017/06/24"
            }] as Set

        def requirementsVersion = [reqVer2] as List<RequirementVersion>
        def requirementVersion = [reqVer2] as Set<RequirementVersion>

        def issue = new IssueDto(
            "165",
            new URL("http://192.175.1.51/bugzilla/show_bug.cgi?id=165"),
            executions,
            requirementVersion
        )

        def issues = new PageImpl<IssueDto>([issue], PageRequest.of(0, 20), 1)


        and:
        reqVersionService.findCurrentVersionIdByRequirementId(_) >> reqVer2.id
        reqVersionService.getExecutionIdsByRequirementVersion(_) >> executions.id
        restIssueService.getIssuesFromExecutionIds(_, _) >> issues
        reqVersionService.getRequirementVersionFromIssue(_,_) >> requirementsVersion
        restRequirementController.findRequirementsIssues(_,_) >> issues

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders
            .get("/api/rest/latest/requirements/{id}/issues", 624)
            .header("Accept", "application/json"))

        then:
        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.issues".hasSize 1
            "_embedded.issues".test {
                "[0]".test {
                    "remoteIssueId".is "165"
                    "url".is "http://192.175.1.51/bugzilla/show_bug.cgi?id=165"
                    "executions".hasSize 1
                    "executions".test {
                        "[0]".test {
                            "_type".is "execution"
                            "id".is 2
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/2"
                        }
                    }
                    "requirement-versions".hasSize 1
                    "requirement-versions".test {
                        "[0]".test {
                            "_type".is "requirement-version"
                            "id".is 88
                            selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/88"
                        }
                    }
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/requirements/624/issues?page=0&size=20"
            "page".test {
                "size".is 20
                "totalElements".is 1
                "totalPages".is 1
                "number".is 0
            }

        }

        /*
   * Documentation
   */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the requirement"
                }
                requestParams {
                    add DescriptorLists.paginationParams
                    add DescriptorLists.fieldsParams
                }
                fields {
                    embeddedAndStop "issues (array) : the issues of this requirement"
                    add DocumentationSnippets.DescriptorLists.issuesFields
                    add "_embedded.issues[].requirement-versions (array) : the requirement versions linked to the issue."
                    add DescriptorLists.paginationFields
                    add DescriptorLists.linksFields
                }
                _links {
                    add DescriptorLists.paginationLinks
                }
            }
        ))
    }


    // ***** POSTing ***** //
    def "post-requirement"() {
        given:
        def json = """{
            "_type" : "requirement",
            "current_version": {
                    "_type" : "requirement-version",
                    "name" : "new age",
                    "criticality": "MINOR",
                    "category":{
                        "code": "CAT_USER_STORY"
                    },
                    "status": "UNDER_REVIEW",
                    "description": "<p>leave a comment please</p>",
                     "custom_fields": [
                    {
                        "code": "cuf_txt_note",
                        "value": "Star Trek style welcomed but not mandatory"
                    },
                    {
                        "code": "cuf_tags_see_also",
                        "value": ["smart home", "sensors", "hand gesture"]
                    }
                ]
            },
            "parent" : {
                    "_type": "requirement-folder",
                    "id": 300
                    }
        }"""

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Winter will be gone"
        }

        def folder = SquashEntityBuilder.requirementFolder {
            id = 300L
            resource = SquashEntityBuilder.resource {
                name = "root-level folder"
            }
            project = proj
        }

        def reqVer = SquashEntityBuilder.requirementVersion {
            id = 333L
            name = "new age"
            reference = "SAMP_REQ_VER"
            criticality "MINOR"
            category "CAT_USER_STORY"
            status "UNDER_REVIEW"
            description = "<p>leave a comment please</p>"

        }

        def req = SquashEntityBuilder.requirement {
            id = 456L
            resource = reqVer
            parent = folder
            project = proj
            versions = [reqVer]
        }

        and:
        reqVersionService.createRequirement(_) >> req
        hierarchyService.findParentFor(_) >> folder
        cufService.findAllCustomFieldValues(reqVer) >> [
                cufValue {
                    label = "note"
                    inputType "PLAIN_TEXT"
                    code = "cuf_txt_note"
                    value = "Star Trek style welcomed but not mandatory"
                },
                cufValue {
                    label = "see also"
                    inputType "TAG"
                    code = "cuf_tags_see_also"
                    value = ["smart home", "sensors", "hand gesture"]
                }
        ]

        when:
        def res = mockMvc.perform(post("/api/rest/latest/requirements")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        /*
             * Test
             */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "requirement"
            "id".is 456
            "name".is "new age"
            "parent".test {
                "_type".is "requirement-folder"
                "id".is 300
            }

            "current_version".test {
                "_type".is "requirement-version"
                "id".is 333
                "name".is "new age"
                "reference".is "SAMP_REQ_VER"
                "version_number".is 1

                "criticality".is "MINOR"
                "category".test {
                    "code".is "CAT_USER_STORY"
                }
                "status".is "UNDER_REVIEW"
                "description".is "<p>leave a comment please</p>"
                "custom_fields".hasSize 2
                "custom_fields[0]".test {
                    "code".is "cuf_txt_note"
                    "label".is "note"
                    "value".is "Star Trek style welcomed but not mandatory"
                }
                "custom_fields[1]".test {
                    "code".is "cuf_tags_see_also"
                    "label".is "see also"
                    "value".contains "smart home", "sensors", "hand gesture"
                }
            }
            "versions".hasSize 1

        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    requestFields {
                        add "_type (string) : the type of the entity (mandatory)"
                        addAndStop "current_version (object) : the current requirement version of this requirement"
                        addAndStop "parent (object) : the parent node of this requirement"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the requirement"
                        add "name (string) : the name of the current (latest) requirement version of this requirement"
                        add "project (object) : the project which this requirement belongs to"
                        add "mode (string) : the management mode of the requirement"
                        add "versions (array) : the requirement versions of this requirement"
                        add DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this requirement"
                        add "project : link to the project this requirement belongs to"
                        add "current_version : link to the current version of this requirement"
                    }
                }
        ))
    }

    def "patch-requirement"() {
        given:
        def json = """{
            "_type" : "requirement",
            "current_version": {
                    "_type" : "requirement-version",
                    "name" : "new age after modify",
                    "reference" : "SAMP_REQ_VER",
                    "criticality": "MAJOR",
                    "category":{
                        "code": "CAT_USER_STORY"
                    },
                    "status": "APPROVED",
                    "description": "<p>Comment after modify</p>",
                     "custom_fields": [
                    {
                        "code": "cuf_txt_note",
                        "value": "Star Trek style welcomed but not mandatory"
                    },
                    {
                        "code": "cuf_tags_see_also",
                        "value": ["smart home", "sensors", "hand gesture"]
                    }
                ]
            }
        }"""

        def proj = SquashEntityBuilder.project {
            id = 14l
            name = "sample project"
        }
        def folder = SquashEntityBuilder.requirementFolder {
            id = 300l
            resource = SquashEntityBuilder.resource {
                name = "sample folder"
            }
            //project = proj
        }
        def reqVer = SquashEntityBuilder.requirementVersion {
            id = 36l
            name = "new age after modify"
            reference = "SAMP_REQ_VER"
            criticality "MAJOR"
            category "CAT_USER_STORY"
            status "APPROVED"
            description = "<p>Comment after modify</p>"

        }

        def req = SquashEntityBuilder.requirement {
            id = 60l
            resource = reqVer
            parent = folder
            project = proj
            versions = [reqVer]
        }
        and:
        nodeHierarchyHelpService.findParentFor(_) >> folder
        requirementVersionPatcher.patch(_, _) >> req
        reqVersionService.modifyRequirement(_, _) >> req
        cufService.findAllCustomFieldValues(reqVer) >> [
                cufValue {
                    label = "note"
                    inputType "PLAIN_TEXT"
                    code = "cuf_txt_note"
                    value = "Star Trek style welcomed but not mandatory"
                },
                cufValue {
                    label = "see also"
                    inputType "TAG"
                    code = "cuf_tags_see_also"
                    value = ["smart home", "sensors", "hand gesture"]
                }
        ]
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/requirements/{id}", 60l)
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "requirement"
            "id".is 60
            "name".is "new age after modify"
            "project".test {
                "_type".is "project"
                "id".is 14
                "name".is "sample project"
            }
            "parent".test {
                "_type".is "requirement-folder"
                "id".is 300
            }
            "current_version".test {
                "_type".is "requirement-version"
                "id".is 36
                "name".is "new age after modify"
                "reference".is "SAMP_REQ_VER"
                "version_number".is 1

                "criticality".is "MAJOR"
                "category".test {
                    "code".is "CAT_USER_STORY"
                }
                "status".is "APPROVED"
                "description".is "<p>Comment after modify</p>"
                "custom_fields".hasSize 2
                "custom_fields[0]".test {
                    "code".is "cuf_txt_note"
                    "label".is "note"
                    "value".is "Star Trek style welcomed but not mandatory"
                }
                "custom_fields[1]".test {
                    "code".is "cuf_tags_see_also"
                    "label".is "see also"
                    "value".contains "smart home", "sensors", "hand gesture"
                }
            }
            "versions".hasSize 1
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/requirements/60"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/14"
            }

        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the requirement."
                    }
                }
        ))
    }

    def "delete-requirement"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/requirements/{ids}", "169,189")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * reqVersionService.deleteRequirements(_)

        /*
        * Documentation
        */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the requirements"
                    }
                }
        ))
    }

    def "associate-test-cases"() {
        given:
            def verifyingTestCases = [
                    SquashEntityBuilder.testCase {
                        id = 350L
                        name = "Verify click number"
                    },
                    SquashEntityBuilder.testCase {
                        id = 351L
                        name = "Verify element number"
                    },
                    SquashEntityBuilder.testCase {
                        id = 352L
                        name = "Verify page space"
                    }
            ]
            def requirementVersion = SquashEntityBuilder.requirementVersion {
                id = 658L
                name = "User friendly interface"
                reference = "UX-5"
                criticality "MAJOR"
                category "CAT_ERGONOMIC"
                status "WORK_IN_PROGRESS"
                description = "<p>User interface is minimalist and easy to use.</p>"
                testCases = verifyingTestCases
            }
            def projectMock = SquashEntityBuilder.project {
                id = 5L
                name = "Application-5"
            }
            def folder = SquashEntityBuilder.requirementFolder {
                id = 305L
                resource = SquashEntityBuilder.resource {
                    name = "User Interface Folder"
                }
                project = projectMock
            }
            def requirement = SquashEntityBuilder.requirement {
                id = 543L
                resource = requirementVersion
                parent = folder
                project = projectMock
                versions = [requirementVersion]
            }
            def customFields = [cufValue {
                label = "test_is_automated"
                inputType "CHECKBOX"
                code = "AUTOMATED"
                value = false
            }]
        and:
            reqVersionService.findRequirement(543) >> requirement
            1 * verifyingTestCaseService.addVerifyingTestCasesToRequirementVersion([350, 351, 352], 658)
            nodeHierarchyHelpService.findParentFor(requirement) >> folder
            permService.canRead(_) >> true
            cufService.findAllCustomFieldValues(requirementVersion) >> customFields
        when:
            def res = mockMvc.perform(
                    post("/api/rest/latest/requirements/{id}/coverages/{testCaseIds}", 543L, "350,351,352")
                            .accept("application/json"))
        then:
            res
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
            withResult(res) {
                "_type".is "requirement"
                "id".is 543
                "name".is "User friendly interface"
                "parent".test {
                    "_type".is "requirement-folder"
                    "id".is 305
                }
                "current_version".test {
                    "_type".is "requirement-version"
                    "id".is 658
                    "name".is "User friendly interface"
                    "reference".is "UX-5"
                    "version_number".is 1
                    "criticality".is "MAJOR"
                    "category.code".is "CAT_ERGONOMIC"
                    "status".is "WORK_IN_PROGRESS"
                    "description".is "<p>User interface is minimalist and easy to use.</p>"
                    "custom_fields[0]".test {
                        "code".is "AUTOMATED"
                        "label".is "test_is_automated"
                        "value".is "false"
                    }
                    "verifying_test_cases".hasSize 3
                    "verifying_test_cases".test {
                        "[0]".test {
                            "_type".is "test-case"
                            "id".is 350
                            "name".is "Verify click number"
                        }
                        "[1]".test {
                            "_type".is "test-case"
                            "id".is 351
                            "name".is "Verify element number"
                        }
                        "[2]".test {
                            "_type".is "test-case"
                            "id".is 352
                            "name".is "Verify page space"
                        }
                    }
                }
                "versions".hasSize 1
                "_links".test {
                    "self.href".is "http://localhost:8080/api/rest/latest/requirements/543"
                    "project.href".is "http://localhost:8080/api/rest/latest/projects/5"
                    "current_version.href".is "http://localhost:8080/api/rest/latest/requirement-versions/658"
                }
            }
            res.andDo(
                    doc.document(documentationBuilder {
                        pathParams {
                            add "id : the id of the requirement"
                            add "testCaseIds : the ids of the test cases to associate"
                        }
                        requestParams {
                            add DocumentationSnippets.DescriptorLists.fieldsParams
                        }
                        fields {
                            add "_type (string) : type of the entity"
                            add "id (number) : the id of the requirement"
                            add "name (string) : the name of the current (latest) requirement version of this requirement"
                            add "mode (string) : the management mode of the requirement"
                            add "remote_req_id (string) : distant requirement id, null if mode is native"
                            add "remote_req_url (string) : distant requirement url, null if mode is native"
                            add "remote_req_perimeter_status (string) : distant requirement perimeter status, null if mode is native"
                            addAndStop "project (object) : the project the requirement belongs to"
                            addAndStop "parent (object) : the parent of this requirement"
                            addAndStop "current_version (object) : the current requirement version of this requirement"
                            addAndStop "versions (array) : the requirement versions of this requirement"
                            add DescriptorLists.linksFields
                        }
                        _links {
                            add "self : link to this requirement"
                            add "project : link to the project this requirement belongs to"
                            add "current_version : link to the current version of this requirement"
                        }

                    }))
    }

    def "disassociate-test-cases"() {
        when:
        def res = mockMvc.perform(
                delete("/api/rest/latest/requirements/{id}/coverages/{testCaseIds}", 543L, "350,351")
                        .accept("application/json")
                        .contentType("application/json"))
        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * verifyingTestCaseService.removeVerifyingTestCasesToRequirementVersion(_,_)

        /*
        * Documentation
        */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the requirement"
                        add "testCaseIds : the ids of the test cases to disassociate"
                    }
                }
        ))
    }
}
