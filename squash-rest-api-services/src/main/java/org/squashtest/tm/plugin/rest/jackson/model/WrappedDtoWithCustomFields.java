/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.plugin.rest.core.jackson.SerializationDynamicFilter;
import org.squashtest.tm.plugin.rest.core.jackson.WrappedDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Specialized WrappedDTO that declares custom fields
 *
 * @param <ENTITY>
 */
@JsonFilter(SerializationDynamicFilter.FILTER_ID)
public abstract class WrappedDtoWithCustomFields<ENTITY extends BoundEntity> extends WrappedDTO<ENTITY> {

    @JsonProperty("custom_fields")
    private List<CustomFieldValueDto> customFields = new ArrayList<>();

    @Override
    public ENTITY getWrapped() {
        return super.getWrapped();
    }

    public List<CustomFieldValueDto> getCustomFields() {
        return customFields;
    }
}
