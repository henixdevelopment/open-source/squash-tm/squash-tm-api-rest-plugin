/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper;

import org.springframework.validation.Errors;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.project.Project;

import java.util.Set;

/**
 * Created by jlor on 28/07/2017.
 */
public class ExecutionValidationHelper {

    public Errors errors;

    public Project project;

    public Execution execution;

    public ExecutionValidationHelper(Execution execution, Project project, Errors errors) {
        this.execution = execution;
        this.project = project;
        this.errors = errors;
    }

    public void validate() {
        checkStatus();
    }

    private void checkStatus() {
        ExecutionStatus status = execution.getExecutionStatus();
        Set<ExecutionStatus> disabledStatuses = project.getCampaignLibrary().getDisabledStatuses();
        Set<ExecutionStatus> allowedStatuses = execution.getLegalStatusSet();
        if (disabledStatuses.contains(status)) {
            errors.rejectValue("wrapped.executionStatus", "Unauthorized execution status", "This execution status is not authorized for this project.");
        }
    }
}
