/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import com.google.common.base.CaseFormat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.customreport.CustomReportTreeDefinition;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.hateoas.SingleRelPagedResources;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.BasicResourceAssembler;
import org.squashtest.tm.plugin.rest.core.web.ContentInclusion;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.ProjectTypeFilter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UriComponents;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyClearance;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyPermission;
import org.squashtest.tm.plugin.rest.service.RestPartyService;
import org.squashtest.tm.plugin.rest.service.RestProjectService;
import org.squashtest.tm.plugin.rest.validators.GenericProjectPostValidator;
import org.squashtest.tm.domain.resource.Resource;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestApiController(GenericProject.class)
@UseDefaultRestApiConfiguration
public class RestProjectController extends BaseRestController {

    @Inject
    private RestProjectService service;

    @Inject
    private RestPartyService partyService;

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private GenericProjectPostValidator genericProjectPostValidator;

    @Inject
    private BasicResourceAssembler resAssembler;

    @GetMapping("/projects/{id}")
    @EntityGetter({Project.class, ProjectTemplate.class})
    @DynamicFilterExpression("*")
    public ResponseEntity<EntityModel<GenericProject>> findProject(@PathVariable("id") long projectId) {

        GenericProject project = service.getOne(projectId);

        EntityModel<GenericProject> res = toEntityModel(project);

        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findRequirementLibraryContent(projectId, null, null))).withRel("requirements"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findTestCaseLibraryContent(projectId, null, null))).withRel("test-cases"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findCampaignLibraryContent(projectId, null, null))).withRel("campaigns"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findPartyClearancesByProject(projectId))).withRel("clearances"));
        res.add(createRelationTo("attachments"));

        return ResponseEntity.ok(res);
    }

    @GetMapping(value = "/projects", params = "projectName")
    @DynamicFilterExpression("*")
    public ResponseEntity<EntityModel<GenericProject>> findProjectByName(@RequestParam("projectName") String projectName) {
        GenericProject project = service.getOneByName(projectName);

        EntityModel<GenericProject> res = toEntityModel(project);

        long projectId = project.getId();
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findRequirementLibraryContent(projectId, null, null))).withRel("requirements"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findTestCaseLibraryContent(projectId, null, null))).withRel("test-cases"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findCampaignLibraryContent(projectId, null, null))).withRel("campaigns"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findPartyClearancesByProject(projectId))).withRel("clearances"));
        res.add(createRelationTo("attachments"));

        return ResponseEntity.ok(res);
    }

    @GetMapping(value = "/projects")
    @ResponseBody
    @DynamicFilterExpression("name")
    public ResponseEntity<PagedModel<EntityModel<GenericProject>>> findAllReadableProjects(Pageable pageable, ProjectTypeFilter type){

        Page<GenericProject> projects = switch (type) {
            case STANDARD -> service.findAllReadableStandardProject(pageable);
            case TEMPLATE -> service.findAllReadableProjectTemplate(pageable);
            default -> service.findAllReadable(pageable);
        };

        PagedModel<EntityModel<GenericProject>> res = toPagedModel(projects);
        return ResponseEntity.ok(res);

    }

    @PostMapping("/projects")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<EntityModel<GenericProject>> createGenericProject(@RequestBody GenericProjectDto genericProjectDto) throws BindException {

        validatePostGenericProject(genericProjectDto);
        GenericProject genericProject = service.createGenericProject(genericProjectDto);
        EntityModel<GenericProject> res = toEntityModel(genericProject);

        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findRequirementLibraryContent(genericProject.getId(), null, null))).withRel("requirements"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findTestCaseLibraryContent(genericProject.getId(), null, null))).withRel("test-cases"));
        res.add(linkService.fromBasePath(linkTo(methodOn(RestProjectController.class).findCampaignLibraryContent(genericProject.getId(), null, null))).withRel("campaigns"));

        return ResponseEntity.status(HttpStatus.CREATED).body(res);

    }


    // *********************** requirements **************************

    @GetMapping(value = "/projects/{id}/requirements-library/content")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedModel<EntityModel<RequirementLibraryNode<Resource>>>> findRequirementLibraryContent(@PathVariable("id") long projectId,
                                                                                                                   Pageable paging,
                                                                                                                   ContentInclusion include) {
        Page<RequirementLibraryNode<Resource>> nodes = null;
        switch (include) {
            case NESTED:
                nodes = service.findRequirementLibraryAllContent(projectId, paging);
                break;
            default:
                nodes = service.findRequirementLibraryRootContent(projectId, paging);
                break;
        }

        PagedModel<EntityModel<RequirementLibraryNode<Resource>>> res = toPagedResourcesWithRel(nodes, "requirement-library-content");

        return ResponseEntity.ok(res);
    }

    @GetMapping(value = "/projects/{id}/reporting-library/content")
    @DynamicFilterExpression("*")
    public ResponseEntity<PagedModel<EntityModel<CustomReportLibraryNode>>> findCustomReportLibraryContent(@PathVariable("id") long projectId,
                                                                                                                   Pageable paging,
                                                                                                                   ContentInclusion include) {
        Page<CustomReportLibraryNode> nodes = null;
        switch (include) {
            case NESTED:
                nodes = service.findCustomReportLibraryAllContent(projectId, paging);
                break;
            default:
                nodes = service.findCustomReportLibraryRootContent(projectId, paging);
                break;
        }

        PagedModel<EntityModel<CustomReportLibraryNode>> res = toPagedResourcesForCustomReportLibrary(nodes, "reporting-library-content");

        return ResponseEntity.ok(res);
    }



    @GetMapping(value = "/projects/{id}/requirements")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedModel<EntityModel<Requirement>>> findRequirementsByProject(@PathVariable("id") long projectId,
                                                                          Pageable paging) {

        Page<Requirement> reqs = service.findRequirementsByProject(projectId, paging);

        PagedModel<EntityModel<Requirement>> res = toPagedModel(reqs);

        return ResponseEntity.ok(res);
    }

    // *********************** test cases **************************


    /**
     * Returns the content of the test case library.
     *
     * @param projectId
     * @param paging
     * @param include   value is either {@link UriComponents#CONTENT_INCLUDE_ROOT} or {@link UriComponents#CONTENT_INCLUDE_NESTED}, if null defaults to CONTENT_INCLUDE_ROOT.
     * @return
     */
    @GetMapping(value = "/projects/{id}/test-cases-library/content")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedModel<EntityModel<TestCaseLibraryNode>>> findTestCaseLibraryContent(@PathVariable("id") long projectId,
                                                                               Pageable paging,
                                                                               ContentInclusion include) {

        Page<TestCaseLibraryNode> nodes = null;
        switch (include) {
            case NESTED:
                nodes = service.findTestCaseLibraryAllContent(projectId, paging);
                break;
            default:
                nodes = service.findTestCaseLibraryRootContent(projectId, paging);
                break;
        }

        PagedModel<EntityModel<TestCaseLibraryNode>> res = toPagedResourcesWithRel(nodes, "test-case-library-content");

        return ResponseEntity.ok(res);
    }

    @GetMapping(value = "/projects/{id}/test-cases")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedModel<EntityModel<TestCase>>> findTestCasesByProject(@PathVariable("id") long projectId,
                                                                                    Pageable paging,
                                                                                    @RequestParam(value = "fields", required = false) String fields){
        Page<TestCase> testCases = service.findTestCasesByProject(projectId, paging, getFields(fields));

        PagedModel<EntityModel<TestCase>> res = toPagedResourcesWithRel(testCases, "test-cases");

        return ResponseEntity.ok(res);
    }


    //************************* campaigns ************************

    /**
     * Returns the content of the execution library.
     *
     * @param projectId
     * @param paging
     * @param include   value is either {@link UriComponents#CONTENT_INCLUDE_ROOT} or {@link UriComponents#CONTENT_INCLUDE_NESTED}, if null defaults to CONTENT_INCLUDE_ROOT.
     * @return
     */
    @GetMapping(value = "/projects/{id}/campaigns-library/content")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedModel<EntityModel<CampaignLibraryNode>>> findCampaignLibraryContent(@PathVariable("id") long projectId,
                                                                               Pageable paging,
                                                                               ContentInclusion include) {

        Page<CampaignLibraryNode> nodes = null;
        switch (include) {
            case NESTED:
                nodes = service.findCampaignLibraryAllContent(projectId, paging);
                break;
            default:
                nodes = service.findCampaignLibraryRootContent(projectId, paging);
                break;
        }

        PagedModel<EntityModel<CampaignLibraryNode>> res = toPagedResourcesWithRel(nodes, "campaign-library-content");

        return ResponseEntity.ok(res);
    }

    @GetMapping(value = "/projects/{id}/campaigns")
    @DynamicFilterExpression("name, reference")
    public ResponseEntity<PagedModel<EntityModel<Campaign>>> findCampaignsByProject(@PathVariable("id") long projectId,
                                                                           Pageable paging) {

        Page<Campaign> testCases = service.findCampaignsByProject(projectId, paging);

        PagedModel<EntityModel<Campaign>> res = toPagedModel(testCases);

        return ResponseEntity.ok(res);
    }


    //************************* permissions ************************

    @GetMapping(value = "/projects/{id}/clearances")
    @DynamicFilterExpression("login, name")
    public ResponseEntity<EntityModel<RestPartyClearance>> findPartyClearancesByProject(@PathVariable("id") long projectId) {

        RestPartyClearance partyClearances = service.findAllClearancesByProjectId(projectId);

        EntityModel<RestPartyClearance> res = EntityModel.of(partyClearances);
        linksHelper.addAllLinksForPartyClearance(projectId, res);

        return ResponseEntity.ok(res);
    }

    @GetMapping(value = "/projects/{id}/permissions")
    @DynamicFilterExpression("login, name")
    public ResponseEntity<EntityModel<RestPartyPermission>> findPartyPermissionsByProject(@PathVariable("id") long projectId) {

        RestPartyPermission partyPermissions = service.findAllPermissionsByProjectId(projectId);

        EntityModel<RestPartyPermission> res = EntityModel.of(partyPermissions);
        linksHelper.addAllLinksForPartyPermission(projectId, res);

        return ResponseEntity.ok(res);
    }

    @PostMapping(value = "/projects/{projectId}/clearances/{profileId}/users/{partyIds}")
    @DynamicFilterExpression("login, name")
    public ResponseEntity<EntityModel<RestPartyClearance>> postNewClearanceToProjectWithPartyIds(@PathVariable("projectId") long projectId,
                                                                                                  @PathVariable("profileId") long profileId,
                                                                                                  @PathVariable("partyIds") List<Long> partyIds) {
        for (Long partyId : partyIds) {
            service.addNewPermissionToProject(partyId, projectId, profileId);
        }

        RestPartyClearance partyClearances = service.buildPartyClearanceDataModel(profileId, partyIds);

        EntityModel<RestPartyClearance> res = EntityModel.of(partyClearances);
        linksHelper.addAllLinksForPartyClearance(projectId, res);

        return ResponseEntity.ok(res);
    }

    @PostMapping(value = "/projects/{projectId}/permissions/{permissionGroup}")
    @DynamicFilterExpression("login, name")
    public ResponseEntity<EntityModel<RestPartyPermission>> postNewPermissionToProjectWithPartyIds(@PathVariable("projectId") long projectId,
                                                                                     @PathVariable("permissionGroup") String permissionGroup,
                                                                                     @RequestParam(value = "ids") List<Long> partyIds) throws BindException {

        PostProjectPermissionModel bean = new PostProjectPermissionModel(partyIds, permissionGroup);
        validatePostNewPermissionsToProject(bean);
        for (Long partyId : bean.partyIds) {
            service.addNewPermissionToProject(partyId, projectId, bean.permissionGroup);
        }

        RestPartyPermission partyPermissions = service.buildPartyPermissionDataModel(permissionGroup, partyIds);

        EntityModel<RestPartyPermission> res = EntityModel.of(partyPermissions);
        linksHelper.addAllLinksForPartyPermission(projectId, res);

        return ResponseEntity.ok(res);
    }

    @DeleteMapping("/projects/{projectId}/users/{partyIds}")
    public ResponseEntity<Void> removePartyFromProject(@PathVariable("projectId") long projectId, @PathVariable("partyIds") List<Long> partyIds) throws BindException {

        // 1 - validation
        RemoveProjectPartyModel bean = new RemoveProjectPartyModel(partyIds);
        validateRemovePartyFromProject(bean);

        // 2 - action
        for (Long partyId : bean.partyIds) {
            service.deletePartyFromProject(partyId, projectId);
        }

        return ResponseEntity.noContent().build();
    }

    private void validateRemovePartyFromProject(RemoveProjectPartyModel bean) throws BindException {
        List<Errors> errors = new ArrayList<>();
        List<Long> partyIds = bean.partyIds;

        for (int i = 0; i < partyIds.size(); ++i) {
            Long partyId = partyIds.get(i);
            Party party = partyService.findById(partyId);
            if (party == null) {
                String fieldName = "partyIds["+i+"]";
                String message = "No id of user or team known for "+ partyId;
                BindingResult validation = new BeanPropertyBindingResult(bean, "remove-project-party");
                validation.rejectValue(fieldName, "invalid ids for user or team", message);
                errors.add(validation);
            }
        }

        ErrorHandlerHelper.throwIfError(bean, errors, "remove-project-party");
    }

    private void validatePostGenericProject(GenericProjectDto genericProjectDto) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(genericProjectDto, "post-generic-project");
        genericProjectPostValidator.validate(genericProjectDto, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(genericProjectDto, errors, "post-generic-project");
    }

    private void validatePostNewPermissionsToProject(PostProjectPermissionModel bean) throws BindException {

        List<Errors> errors = new ArrayList<>();
        BindingResult validation = new BeanPropertyBindingResult(bean, "post-project-permission");
        int index = 0;
        for (Long partyId : bean.partyIds) {
            Party party = partyService.getOne(partyId);
            if (party == null) {
                String message = String.format("No id of user or team known for %d", partyId);
                String fieldName = String.format("partyIds[%d]", index);
                validation.rejectValue(fieldName, "invalid ids for user or team", message);
                errors.add(validation);
                validation = new BeanPropertyBindingResult(bean, "post-project-permission");
            }
            index += 1;
        }

        String rawData = bean.permissionGroup;

        // manual translation for advanced tester |*_*|
        if ("advanced_tester".equals(bean.permissionGroup)){
            bean.permissionGroup = "advance_tester";
        }

        bean.permissionGroup = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, bean.permissionGroup);

        if (!getAllPermissionGroups().contains(bean.permissionGroup)) {
            String message = String.format("No permission group known for %s", rawData);
            validation.rejectValue("permissionGroup", "invalid permission group", message);
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(bean, errors, "post-project-permission");

    }

    private List<String> getAllPermissionGroups(){
        List<AclGroup> groups = service.findAllPossiblePermission();
        List<String> groupNames = new ArrayList<>();
        for (AclGroup group : groups) {
            groupNames.add(group.getSimpleName());
        }

        return groupNames;
    }

    private <T> SingleRelPagedResources<EntityModel<T>> toPagedResourcesForCustomReportLibrary(Page<T> page, String rel) {
        PagedModel<EntityModel<T>> res = pageAssembler.toModel(page, resAssembler);
        for (EntityModel<T> entityModel : res.getContent()) {

            String entityType = service.toKebabCase(((CustomReportLibraryNode) entityModel.getContent()).getEntityType().getTypeName()) + "s";

            service.replaceLinksByCustomSelfLinks(entityModel, entityType);
        }
        return new SingleRelPagedResources<>(res, rel);
    }

    /*a private bean for validation propose*/
    private class PostProjectPermissionModel {
        private List<Long> partyIds;
        private String permissionGroup;

        private PostProjectPermissionModel(List<Long> partyIds, String permissionGroup) {
            this.partyIds = partyIds;
            this.permissionGroup = permissionGroup;
        }

        public List<Long> getPartyIds() {
            return partyIds;
        }

        public String getPermissionGroup() {
            return permissionGroup;
        }
    }

    private class RemoveProjectPartyModel {
        private List<Long> partyIds;

        public RemoveProjectPartyModel(List<Long> partyIds) {
            this.partyIds = partyIds;
        }

        public List<Long> getPartyIds() {return partyIds;}

    }
}
