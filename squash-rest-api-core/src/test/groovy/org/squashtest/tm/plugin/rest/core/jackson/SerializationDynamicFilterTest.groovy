/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson

import com.fasterxml.jackson.annotation.JsonFilter
import com.fasterxml.jackson.annotation.JsonUnwrapped
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider
import spock.lang.Specification

public class SerializationDynamicFilterTest extends Specification{


	
	def "with an empty filter, should serialize every property of a player but wont go further (mandatory properties still included)"(){
		
		given :
			def writer = createWriterWithFilter("")
			def player = createPlayer()
			
		when :
			def res = writer.writeValueAsString(player)

		then :
			res.normalize() == 
"""{
  "_type" : "player",
  "id" : 1,
  "name" : "BigKevin",
  "age" : 13,
  "games" : [ {
    "_type" : "game",
    "id" : 10
  }, {
    "_type" : "game",
    "id" : 20
  } ]
}"""
	
	}
	
	def "mandatory properties cannot be disabled"(){
		given :
			def writer = createWriterWithFilter("*,-_type,-id")
			def player = createPlayer()
			
		when :
			def res = writer.writeValueAsString(player)
	
		then :
			res.normalize() ==
"""{
  "_type" : "player",
  "id" : 1,
  "name" : "BigKevin",
  "age" : 13,
  "games" : [ {
    "_type" : "game",
    "id" : 10
  }, {
    "_type" : "game",
    "id" : 20
  } ]
}"""
	}
	
	def "should serialize the player but ommit the name"(){
		given :
			def writer = createWriterWithFilter("*,-name")
			def player = createPlayer()
			
		when :
			def res = writer.writeValueAsString(player)
	
		then :
			res.normalize() == 
"""{
  "_type" : "player",
  "id" : 1,
  "age" : 13,
  "games" : [ {
    "_type" : "game",
    "id" : 10
  }, {
    "_type" : "game",
    "id" : 20
  } ]
}"""
		
		
	}
	
	
	def "should serialize only the player and game names (mandatory properties still included)"(){
		
		given :
			def writer = createWriterWithFilter("name,games[name]")
			def player = createPlayer()
			
		when :
			def res = writer.writeValueAsString(player)

		then :
			res.normalize() == 
"""{
  "_type" : "player",
  "id" : 1,
  "name" : "BigKevin",
  "games" : [ {
    "_type" : "game",
    "id" : 10,
    "name" : "Call of Dully"
  }, {
    "_type" : "game",
    "id" : 20,
    "name" : "Lulz"
  } ]
}"""
		
	}
	
	
	def "should serialize everything"(){
		
		given :
			def writer = createWriterWithFilter("*,games[*,clans[*]]")
			def player = createPlayer()
			
		when :
			def res = writer.writeValueAsString(player)

		then :
			res.normalize() == 
"""{
  "_type" : "player",
  "id" : 1,
  "name" : "BigKevin",
  "age" : 13,
  "games" : [ {
    "_type" : "game",
    "id" : 10,
    "name" : "Call of Dully",
    "clans" : [ {
      "_type" : "clan",
      "id" : 100,
      "tagname" : "//Roxxor\\\\",
      "website" : "http://roxxor.com"
    }, {
      "_type" : "clan",
      "id" : 101,
      "tagname" : "__PGM__",
      "website" : "http://so-pro.com"
    } ]
  }, {
    "_type" : "game",
    "id" : 20,
    "name" : "Lulz",
    "clans" : [ {
      "_type" : "clan",
      "id" : 200,
      "tagname" : "---=== badass ===---",
      "website" : "http://utimate-badassery.org"
    } ]
  } ]
}"""
		
			
	}
	
	// *** test on the unwrapping property writer features ****
	
	def "should serialize a whole order, considering embedded beans as part of the root bean"(){
		given :
			def writer = createWriterWithFilter("")
			def player = createOrder()
			
		when :
			def res = writer.writeValueAsString(player)
			
		then :
			res.normalize() == 
"""{
  "_type" : "order",
  "id" : 1,
  "operation" : "move west",
  "date" : 1495208931980,
  "issuer" : "Helios"
}"""
	}
	
	
	def "should filter a guenuine root bean property but let pass the embedded properties"(){
		given :
			def writer = createWriterWithFilter("operation")
			def player = createOrder()
			
		when :
			def res = writer.writeValueAsString(player)
			
		then :
			res.normalize() == 
"""{
  "_type" : "order",
  "id" : 1,
  "operation" : "move west"
}"""			

	}
	
	
	
	def "should filter one of the property of an embedded bean"(){
		given :
			def writer = createWriterWithFilter("*,-issuer")
			def player = createOrder()
			
		when :
			def res = writer.writeValueAsString(player)
			
		then :
			res.normalize() == 
"""{
  "_type" : "order",
  "id" : 1,
  "operation" : "move west",
  "date" : 1495208931980
}"""
	}
	
	
	
	def "should filter the otherproperty of an embedded bean"(){
		given :
			def writer = createWriterWithFilter("operation,issuer")
			def player = createOrder()
			
		when :
			def res = writer.writeValueAsString(player)
			
		then :
			res.normalize() == 
"""{
  "_type" : "order",
  "id" : 1,
  "operation" : "move west",
  "issuer" : "Helios"
}"""			

	}
	
	
	// ********************* utils *************************
	
	
	ObjectWriter createWriterWithFilter(expression){
		def filter = new SerializationDynamicFilter(expression)
		SimpleFilterProvider provider = new SimpleFilterProvider()
		provider.addFilter("testfilter", (SimpleBeanPropertyFilter) filter)
		
		new ObjectMapper()
				.enable(SerializationFeature.INDENT_OUTPUT)
				.writer(provider)
	}
	
	// ********** "player" dataset utilities *********
	
	Player createPlayer(){
		new Player(name:"BigKevin", age:13, games : [
			new Game(id:10, name:"Call of Dully", clans : [
                    new Clan( id:100, tagname:"//Roxxor\\", website:"http://roxxor.com"),
                    new Clan( id:101, tagname:"__PGM__", website:"http://so-pro.com")
			]),
			new Game(id: 20, name:"Lulz", clans : [
				new Clan(id: 200, tagname:"---=== badass ===---", website:"http://utimate-badassery.org")	
			])
		])
	}
	
	
	
	@JsonFilter("testfilter")
	public static final class Player{
		String _type = "player"
		int id = 1
		String name
		int age
		List<Game> games
	}
	
	@JsonFilter("testfilter")
	public static final class Game{
		String _type = "game"
		int id
		String name
		List<Clan> clans
	}
	
	@JsonFilter("testfilter")
	public static final class Clan{
		String _type = "clan"
		int id
		String tagname
		String website
	}
	
	
	// ***** "Orders" dataset utilities ******
	
	
	Order createOrder(){
		return new Order(
			operation:"move west" ,
			confirmation : new Confirmation(
				issuer : "Helios",
				date : new Date(1495208931980)	// something like 2017/05/17 05:53pm
			)
		)
	}
	
	@JsonFilter("testfilter")
	public static final class Order{
		String _type = "order"
		int id = 1
		
		String operation
		
		@JsonUnwrapped
		Confirmation confirmation
	}
	
	@JsonFilter("testfilter")
	public static final class Confirmation{
		Date date
		String issuer
	}
	
}
