/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.deserializer

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import org.squashtest.tm.domain.testcase.TestCase
import spock.lang.Specification

import javax.persistence.EntityManager

/**
 * Created by jthebault on 08/06/2017.
 */
class JsonCrawlerTest extends Specification {

    JsonCrawler jsonCrawler = new JsonCrawler();

    EntityManager entityManager = Mock();

    def setup() {
        jsonCrawler.entityManager = entityManager
    }

    def "should compute path"() {
        given:
        String[] pathTokkens = pathGivenTokkens

        when:
        def pointer = jsonCrawler.computeIdPointer(pathTokkens)

        then:
        pointer.toString().equals(expectedJsonPath)

        where:
        pathGivenTokkens             || expectedJsonPath
        []                           || "/id"
        ["id"]                       || "/id"
        ["att1", "s-att1"]           || "/att1/s-att1/id"
        ["att1", "s-att1", "id"]     || "/att1/s-att1/id"
        ["att1", "s-att1", "s-att2"] || "/att1/s-att1/s-att2/id"
    }


    def "should traverse json object and return entity"() {
        given:
        JsonNode callStepNode = getJsonTree();

        and:
        TestCase testCase = new TestCase();
        entityManager.find(TestCase.class, 1) >> testCase

        when:
        def entity = jsonCrawler.findMandatoryEntity(callStepNode, TestCase.class, "called_test_case")

        then:
        entity == testCase

    }

    def "should reject invalid path"(){
        given:
        def jsonNode = getJsonTree()

        when:
        jsonCrawler.findMandatoryEntity(jsonNode, TestCase.class, "stupid","path","lol")

        then:
        thrown IllegalArgumentException

    }

    JsonNode getJsonTree(){
        JsonNodeFactory jsonNodeFactory = new JsonNodeFactory(false)
        def testCaseNode = jsonNodeFactory.objectNode()
        testCaseNode.put("id", 1)

        def callStepNode = jsonNodeFactory.objectNode()
        callStepNode.set("called_test_case", testCaseNode)
        callStepNode.put("id",1)
        callStepNode
    }
}
