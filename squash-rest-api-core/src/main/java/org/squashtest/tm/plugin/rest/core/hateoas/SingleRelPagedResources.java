/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.hateoas;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.hateoas.PagedModel;

/**
 * That class will be processed by {@link SingleRelHalResourcesSerializer}
 * 
 * 
 * @author bsiri
 *
 * @param <T>
 */
public class SingleRelPagedResources<T> extends PagedModel<T> implements SingleRelHateoasResources<T>{

	public SingleRelPagedResources() {
		super();
	}
	
	public SingleRelPagedResources(PagedModel<T> paged, String rel){
		super(paged.getContent(), paged.getMetadata(), paged.getLinks());
		this.rel = rel;
	}


	private String rel;

	@JsonIgnore
	@Override
	public String getRel() {
		return rel;
	}

	@JsonIgnore
	public void setRel(String rel) {
		this.rel = rel;
	}
	
}
