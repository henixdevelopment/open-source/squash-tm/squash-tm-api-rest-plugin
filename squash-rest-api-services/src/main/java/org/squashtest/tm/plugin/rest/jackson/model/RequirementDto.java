/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import org.squashtest.tm.domain.library.Library;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.ManagementMode;
import org.squashtest.tm.domain.requirement.RequirementSyncExtender;

import java.util.List;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "_type")
@JsonTypeName("requirement")
@JsonSubTypes({
        @JsonSubTypes.Type(value = HighLevelRequirementDto.class, name = "high-level-requirement")
})
public class RequirementDto implements RestNode {

    private Long id;

    private Project project;

    @JsonProperty("current_version")
    private RequirementVersionDto currentVersion;

    private List<RequirementVersionDto> versions;

    private ParentEntity parent;

    private String path;

    private ManagementMode mode;

    private RequirementSyncExtender syncExtender;

    public RequirementVersionDto getCurrentVersion() {
        return currentVersion;
    }

    public List<RequirementVersionDto> getVersions() {
        return versions;
    }

    public String getPath() {
        return path;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public void setCurrentVersion(RequirementVersionDto currentVersion) {
        this.currentVersion = currentVersion;
    }

    public void setVersions(List<RequirementVersionDto> versions) {
        this.versions = versions;
    }

    public void setParent(ParentEntity parent) {
        this.parent = parent;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ManagementMode getMode() {
        return mode;
    }

    public void setMode(ManagementMode mode) {
        this.mode = mode;
    }

    public RequirementSyncExtender getSyncExtender() {
        return syncExtender;
    }

    public void setSyncExtender(RequirementSyncExtender syncExtender) {
        this.syncExtender = syncExtender;
    }

    public List<CustomFieldValueDto> getCustomFields() {
        return currentVersion.getCustomFields();
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Project getProject() {
        return project;
    }

    @Override
    public String getName() {
        return currentVersion.getName();
    }

    @Override
    public void setName(String name) {
        currentVersion.setName(name);
    }

    @Override
    public String getDescription() {
        return currentVersion.getDescription();
    }

    @Override
    public void setDescription(String description) {
        currentVersion.setDescription(description);
    }

    @Override
    public ParentEntity getParent() {
        return parent;
    }

    @Override
    public RestType getRestType() {
        return RestType.REQUIREMENT;
    }

    @Override
    public void notifyAssociatedWithProject(Project project) {
        this.project = project;
    }


    @Override
    public Library<?> getLibrary() {
        return getProject().getRequirementLibrary();
    }

}
