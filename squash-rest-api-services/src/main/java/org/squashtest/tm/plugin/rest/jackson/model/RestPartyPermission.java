/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.plugin.rest.jackson.serializer.CollectionHateoasWrapperSerializer;
import org.squashtest.tm.plugin.rest.jackson.serializer.SnakeCaseSerializer;

import java.util.HashMap;
import java.util.List;
/*
* 05/05/2022
* @JsonSerialize doesn't work on class anymore
* We have to create a inside attribute of the object to make the annotation work again.
*/
public class RestPartyPermission {

  @JsonSerialize(keyUsing = SnakeCaseSerializer.class, contentUsing = CollectionHateoasWrapperSerializer.class)
  private final HashMap<String, List<Party>> partyPermissions = new HashMap<>();

  @JsonGetter("content")
  public HashMap<String, List<Party>> getPartyPermissions() {
    return partyPermissions;
  }

}
