/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentHolder;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.service.RestAttachmentService;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestApiController(Attachment.class)
@UseDefaultRestApiConfiguration
public class RestAttachmentController extends BaseRestController {

	@Inject
	private RestAttachmentService restAttachmentService;

	@GetMapping("/attachments/{id}")
	@EntityGetter
	@ResponseBody
	@DynamicFilterExpression("*")
	public ResponseEntity<EntityModel<Attachment>> findAttachment(@PathVariable("id") long id) throws IOException{

		Attachment attachment = restAttachmentService.findById(id);
		EntityModel<Attachment> res = EntityModel.of(attachment);
		res.add(createSelfLink(attachment));
		res.add(linkService.fromBasePath(linkTo(methodOn(RestAttachmentController.class).downloadAttachment(id))).withRel("content"));

		return ResponseEntity.ok(res);
	}

	@PatchMapping(value = "/attachments/{id}")
	@ResponseBody
	@DynamicFilterExpression("*")
	public ResponseEntity<EntityModel<Attachment>> renameAttachment(@PathVariable("id") long id, @RequestParam("name") String name) throws IOException{

		Attachment attachment = restAttachmentService.renameAttachment(id, name);
		EntityModel<Attachment> res = EntityModel.of(attachment);
		res.add(createSelfLink(attachment));
		res.add(linkService.fromBasePath(linkTo(methodOn(RestAttachmentController.class).downloadAttachment(id))).withRel("content"));

		return ResponseEntity.ok(res);
	}

	@GetMapping(value = "/attachments/{id}/content", produces = "application/octet-stream")
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public ResponseEntity downloadAttachment(@PathVariable("id") long id) throws IOException{

		Attachment attachment = restAttachmentService.findById(id);
		InputStream in = attachment.getContent().getStream();

		byte[] out = IOUtils.toByteArray(in);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.add("Content-Disposition", "attachment; filename=" + attachment.getName().replace(" ", "_"));
		responseHeaders.add("Content-Type", "application/octet-stream");

		return new ResponseEntity(out, responseHeaders, HttpStatus.OK);

}

	@GetMapping(value = "/{owner}/{ownerId}/attachments")
	@ResponseBody
	@DynamicFilterExpression("*, -type,-size, -added_on")
	public ResponseEntity<PagedModel<EntityModel<Attachment>>> findAttachmentsByOwner(@PathVariable("owner") String owner, @PathVariable("ownerId") long ownerId, Pageable pageable) {

		AttachmentHolder holder = restAttachmentService.findHolder(owner, ownerId);
		Page<Attachment> attachments = restAttachmentService.findPagedAttachments(holder, pageable);
		PagedModel<EntityModel<Attachment>> res = toPagedModel(attachments);

		return ResponseEntity.ok(res);

	}

	@PostMapping(value = "/{owner}/{ownerId}/attachments")
	@ResponseBody
	@DynamicFilterExpression("*, -type,-size, -added_on")
	@SuppressWarnings("unchecked")
	public ResponseEntity<CollectionModel<EntityModel<Attachment>>> uploadAttachments(@PathVariable("owner") String owner,
															 @PathVariable("ownerId") long ownerId,
															 @RequestParam("files") List<MultipartFile> files) {
        String permissionName = getPermissionNameFromOwner(owner);
		Map<EntityType, AttachmentHolder> holderMap = restAttachmentService.findHolderWithPermission(owner, ownerId, permissionName);
        EntityType holderType = holderMap.entrySet().iterator().next().getKey();
        AttachmentHolder holder = holderMap.get(holderType);

		List<Attachment> attachments = restAttachmentService.addAttachments(holder, files, holderType);

		List<EntityModel<Attachment>> resources = attachments.stream().map(attachment -> {
			EntityModel<Attachment> res = EntityModel.of(attachment);
			res.add(createSelfLink(attachment));
			return res;
		}).collect(Collectors.toList());

		CollectionModel<EntityModel<Attachment>> res = CollectionModel.of(resources);

		return ResponseEntity.status(HttpStatus.CREATED).body(res);
	}

	@DeleteMapping(value = "/{owner}/{ownerId}/attachments")
	@ResponseBody
	@DynamicFilterExpression("*, -type,-size, -added_on")
	@SuppressWarnings("unchecked")
	public ResponseEntity<Void> deleteAttachments(@PathVariable("owner") String owner,
												  @PathVariable("ownerId") long ownerId,
												  @RequestParam(value = "attachmentIds") List<Long> attachmentIds) throws IOException{
		String permissionName = getPermissionNameFromOwner(owner);
		Map<EntityType, AttachmentHolder> holderMap = restAttachmentService.findHolderWithPermission(owner, ownerId, permissionName);
        EntityType holderType = holderMap.entrySet().iterator().next().getKey();
        AttachmentHolder holder = holderMap.get(holderType);
		restAttachmentService.deleteAttachments(holder, attachmentIds, holderType);
		return ResponseEntity.noContent().build();
	}

	private String getPermissionNameFromOwner(String owner) {
		return "projects".equals(owner) ? Permissions.MANAGE_PROJECT.name() : Permissions.ATTACH.name();
	}
}
