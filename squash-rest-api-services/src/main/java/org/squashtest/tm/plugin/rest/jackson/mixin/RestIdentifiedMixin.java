/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonAppend.Prop;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.domain.testautomation.TestAutomationServer;
import org.squashtest.tm.plugin.rest.core.jackson.SerializationDynamicFilter;
import org.squashtest.tm.plugin.rest.jackson.model.ActionTestStepDto;
import org.squashtest.tm.plugin.rest.jackson.model.CalledTestStepDto;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignDto;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignFolderDto;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignTestPlanItemDto;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetDto;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetParamValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.jackson.model.IterationDto;
import org.squashtest.tm.plugin.rest.jackson.model.IterationTestPlanItemDto;
import org.squashtest.tm.plugin.rest.jackson.model.ParameterDto;
import org.squashtest.tm.plugin.rest.jackson.model.ProjectDto;
import org.squashtest.tm.plugin.rest.jackson.model.ProjectTemplateDto;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementDto;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementFolderDto;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementVersionDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyProfileDto;
import org.squashtest.tm.plugin.rest.jackson.model.TeamDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseFolderDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestSuiteDto;
import org.squashtest.tm.plugin.rest.jackson.model.UserDto;
import org.squashtest.tm.plugin.rest.jackson.serializer.TypePropertyWriter;


/**
 * <p>
 * 		Base class for all Jackson mixins, as long as the mixed entity implements {@link Identified}.
 * </p>
 *
 *
 * @author bsiri
 *
 */

/*
 * Important tech note : due to https://jira.spring.io/browse/DATAREST-304, we cannot rely on @JsonTypeInfo and @JsonSubTypes
 * to convey adequately the type information in the serialized content.
 *
 * This forces us to a double mechanism for the entity type processing : one for serialization and another one for deserialization.
 * If jackson code is corrected this hack could break and we should need to implement a proper TypeIdResolver
 *
 *  1/ serialization
 *  ----------------
 *
 *  The type is included as a VirtualProperty, using the annotation @JsonAppend. The VirtualPropertyWriter doing the job
 *  will look for the annotation @JsonTypeName, declared on each subclasses, to retrieve the value that should be serialized.
 *  Note that @JsonTypeName is normally used in conjunction with @JsonSubTypes. See deserialization, below.
 *
 *  2/ deserialization
 *  ------------------
 *
 *  The type is handled normally, using @JsonTypeInfo and JsonSubTypes in this class and @JsonTypeName in the subclasses.
 *  The normal processing performed by Jackson is applied normally.
 *
 *
 */
@JsonFilter(SerializationDynamicFilter.FILTER_ID)
@JsonSubTypes({ //here are the _type -> java class for DESERIALIZATION only
        @JsonSubTypes.Type(value = TestCaseDto.class, name = "test-case"),
        @JsonSubTypes.Type(value = ActionTestStepDto.class, name = "action-step"),
        @JsonSubTypes.Type(value = CalledTestStepDto.class, name = "call-step"),
        @JsonSubTypes.Type(value = ProjectDto.class, name = "project"),
        @JsonSubTypes.Type(value = ProjectTemplateDto.class, name = "project-template"),
        @JsonSubTypes.Type(value = ParameterDto.class, name = "parameter"),
        @JsonSubTypes.Type(value = InfoListItem.class, name = "infolist-item"),
        @JsonSubTypes.Type(value = RequirementFolder.class, name = "requirement-folder"),
        @JsonSubTypes.Type(value = DatasetDto.class, name = "dataset"),
        @JsonSubTypes.Type(value = DatasetParamValueDto.class, name = "dataset-param-value"),
        @JsonSubTypes.Type(value = RequirementDto.class, name = "requirement"),
        @JsonSubTypes.Type(value = RequirementVersionDto.class, name = "requirement-version"),
        @JsonSubTypes.Type(value = CampaignDto.class, name = "campaign"),
        @JsonSubTypes.Type(value = CampaignFolderDto.class, name = "campaign-folder"),
        @JsonSubTypes.Type(value = CampaignTestPlanItemDto.class, name = "campaign-test-plan-item"),
        @JsonSubTypes.Type(value = IterationDto.class, name = "iteration"),
        @JsonSubTypes.Type(value = TestSuiteDto.class, name = "test-suite"),
        @JsonSubTypes.Type(value = Execution.class, name = "execution"),
        @JsonSubTypes.Type(value = UserDto.class, name = "user"),
        @JsonSubTypes.Type(value = TeamDto.class, name = "team"),
        @JsonSubTypes.Type(value = TestAutomationServer.class, name = "test-automation-server"),
        @JsonSubTypes.Type(value = AutomatedTest.class, name = "automated-test"),
        @JsonSubTypes.Type(value = AutomatedExecutionExtender.class, name = "automated-execution-extender"),
        @JsonSubTypes.Type(value = Attachment.class, name = "attachment"),
        @JsonSubTypes.Type(value = CampaignFolderDto.class, name = "campaign-folder"),
        @JsonSubTypes.Type(value = TestCaseFolderDto.class, name = "test-case-folder"),
        @JsonSubTypes.Type(value = IssueDto.class, name = "issue"),
        @JsonSubTypes.Type(value = IterationTestPlanItemDto.class, name = "iteration-test-plan-item"),
        @JsonSubTypes.Type(value = RequirementFolderDto.class, name = "requirement-folder"),
        @JsonSubTypes.Type(value = RestPartyProfileDto.class, name = "profile")
})
/**
 * it' a hack
 * we manually append _type to our json object
 */

@JsonAppend(prepend = true, props = @Prop(name = "_type", value = TypePropertyWriter.class))
public abstract class RestIdentifiedMixin {
    @JsonProperty
    Long id;
}
