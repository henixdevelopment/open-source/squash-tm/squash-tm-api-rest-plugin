/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestIssueService
import org.squashtest.tm.service.internal.repository.IssueDao
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

/**
 * Created by jlor on 04/08/2017.
 */
@WebMvcTest(RestIssueController)
public class RestIssueControllerIT extends BaseControllerSpec {

    @Inject
    private RestIssueService restIssueService

    @Inject
    private IssueDao issueDao


    def "attache-issue-to-execution"() {
        given:
        def json = """{
                        "_type" : "issue",
                        "remoteIssueId" : "9999"
                    }
                    """

        def exec = SquashEntityBuilder.execution {
            id = 15L
        }

        def issueMck = SquashEntityBuilder.issue {
            id = 30L
            remoteissueId = "9999"
        }

        and:
        restIssueService.attachIssue(_, _) >> issueMck
        issueDao.findExecutionRelatedToIssue(_) >> exec

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/executions/{executionId}/issues", 15)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        res.andExpect(status().isCreated()).andExpect(content().contentType("application/json"))
        withResult(res) {
            "_type".is "issue"
            "id".is 30
            "remoteIssueId".is "9999"
            "execution".test {
                "_type".is "execution"
                "id".is 15
                selfRelIs "http://localhost:8080/api/rest/latest/executions/15"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/issues/30"
        }
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "executionId : the id of the execution"
                    }
                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "remoteIssueId (string) : the identifier of the issue"
                    }
                }
        ))
    }

    def "attache-issue-to-scripted-execution"() {
        given:
        def json = """{
                        "_type" : "issue",
                        "remoteIssueId" : "9999"
                    }
                    """

        def exec = SquashEntityBuilder.scriptedExecution {
            id = 15L
        }

        def issueMck = SquashEntityBuilder.issue {
            id = 30L
            remoteissueId = "9999"

        }

        and:
        restIssueService.attachIssue(_, _) >> issueMck
        issueDao.findExecutionRelatedToIssue(_) >> exec

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/executions/{executionId}/issues", 15)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
        withResult(res) {
            "_type".is "issue"
            "id".is 30
            "remoteIssueId".is "9999"
            "execution".test {
                "_type".is "scripted-execution"
                "id".is 15
                selfRelIs "http://localhost:8080/api/rest/latest/executions/15"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/issues/30"
        }
    }

    def "attache-issue-to-keyword-execution"() {
        given:
        def json = """{
                        "_type" : "issue",
                        "remoteIssueId" : "9999"
                    }
                    """

        def exec = SquashEntityBuilder.keywordExecution {
            id = 15L
        }

        def issueMck = SquashEntityBuilder.issue {
            id = 30L
            remoteissueId = "9999"

        }

        and:
        restIssueService.attachIssue(_, _) >> issueMck
        issueDao.findExecutionRelatedToIssue(_) >> exec

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/executions/{executionId}/issues", 15)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
        withResult(res) {
            "_type".is "issue"
            "id".is 30
            "remoteIssueId".is "9999"
            "execution".test {
                "_type".is "keyword-execution"
                "id".is 15
                selfRelIs "http://localhost:8080/api/rest/latest/executions/15"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/issues/30"
        }
    }

    def "search-attach-issue-to-execution"() {
        given:
        def json = """{
                        "values": {"key": "123", "projectPath": "group/project"}
                    }
                    """

        def exec = SquashEntityBuilder.execution {
            id = 15L
        }

        def issueMock = SquashEntityBuilder.issue {
            id = 30L
            remoteissueId = "16835"
        }

        and:
        restIssueService.searchAndAttachIssueToExecution(_, _) >> Optional.of(issueMock)
        issueDao.findExecutionRelatedToIssue(_) >> exec

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/executions/{executionId}/attach-issue", 15)
            .accept("application/json")
            .contentType("application/json")
            .content(json))

        then:
        res.andExpect(status().isCreated()).andExpect(content().contentType("application/json"))
        withResult(res) {
            "_type".is "issue"
            "id".is 30
            "remoteIssueId".is "16835"
            "execution".test {
                "_type".is "execution"
                "id".is 15
                selfRelIs "http://localhost:8080/api/rest/latest/executions/15"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/issues/30"
        }
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "executionId : the id of the execution"
                }
                requestFields {
                    add "values.key (string) : the identifier of the issue"
                    add "values.projectPath (string) : the path to the issue's project (required for GitLab and Azure DevOps)"
                }
            }
        ))
    }

}
/* == This service is not available in version 1.0.0. No test is possible yet. ==

def "get-issue"() {

    given :

    def issue = SquashEntityBuilder.issue {
        id = 42L
    }
    def execution = SquashEntityBuilder.execution {
        id = 21L
    }

    and :

    restIssueService.getOne(42) >> issue

    issueDao.findExecutionRelatedToIssue(42) >> execution

    when :

    def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/issues/{id}", 42)
            .header("Accept", "application/json"))

    then :

    /*
    * Test
    * *
    res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

    withResult(res) {
        "_type".is "issue"
        "id".is 42
        "execution".test {
            "id".is 21
            selfRelIs "http://localhost:8080/api/rest/latest/executions/21"
        }
        selfRelIs "http://localhost:8080/api/rest/latest/issues/42"
    }

    /*
    * Documentation
    * *
    res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the issue"
                }
                requestParams {
                    add DescriptorLists.fieldsParams
                }
                fields {
                    add "_type (string) : the of the entity"
                    add "id (number) : the id of the issue"
                    add "execution (object) : the execution to which the issue is bound"
                    add DescriptorLists.linksFields
                }
                _links {
                    add "self : link to this issue"
                }
            }
    ))

}
*/


