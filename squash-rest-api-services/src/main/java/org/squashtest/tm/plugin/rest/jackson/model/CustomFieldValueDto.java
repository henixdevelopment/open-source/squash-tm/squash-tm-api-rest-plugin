/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedFieldValue;
import org.squashtest.tm.domain.denormalizedfield.DenormalizedMultiSelectField;
import org.squashtest.tm.plugin.rest.jackson.deserializer.RestRawValueDeserializer;
import org.squashtest.tm.plugin.rest.jackson.serializer.RestRawValueSerializer;

/**
 * Created by jthebault on 12/06/2017.
 */
public class CustomFieldValueDto {

    private String code;

    private String label;

    @JsonSerialize(using = RestRawValueSerializer.class)
    @JsonDeserialize(using = RestRawValueDeserializer.class)
    private RawValue value;

    public CustomFieldValueDto() {
    }

    public CustomFieldValueDto(CustomField customField, CustomFieldValue customFieldValue) {
        this.code = customField.getCode();
        this.label = customField.getLabel();
        this.value = customFieldValue.asRawValue();
    }

    public CustomFieldValueDto(DenormalizedFieldValue dfv) {
        this.code = dfv.getCode();
        this.label = dfv.getLabel();

        RawValue rawValue;
        if (DenormalizedMultiSelectField.class.isAssignableFrom(dfv.getClass())){
            rawValue = new RawValue(((DenormalizedMultiSelectField)dfv).getValues());
        }
        else{
            rawValue = new RawValue(dfv.getValue());
        }

        this.value = rawValue;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public RawValue getValue() {
        return value;
    }

    public void setValue(RawValue value) {
        this.value = value;
    }
}
