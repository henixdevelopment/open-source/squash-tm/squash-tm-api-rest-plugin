/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.hateoas.mediatype.hal.CollectionModelMixin;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule.HalResourcesDeserializer;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule.HalResourcesSerializer;

import javax.xml.bind.annotation.XmlElement;
import java.util.Collection;

/**
 * That mixin overrides {@link CollectionModelMixin}. The default serializer used for '_embedded' resources ({@link HalResourcesSerializer} ) is not satisfying because it
 * maps the content by classes, which seems nice at first but breaks the ordering in the content. This is especially frustrating when all the objects in the resources content 
 * share a common supertype. 
 * 
 * @author bsiri
 *
 */
public abstract class RestResourcesMixin<T> extends CollectionModelMixin<T> {

	
	@Override
	@XmlElement(name = "embedded")
	@JsonProperty("_embedded")
	@JsonInclude(Include.NON_EMPTY)
	@JsonSerialize(using = HalResourcesSerializer.class)
	@JsonDeserialize(using = HalResourcesDeserializer.class)
	public abstract Collection<T> getContent();
	
	
}
