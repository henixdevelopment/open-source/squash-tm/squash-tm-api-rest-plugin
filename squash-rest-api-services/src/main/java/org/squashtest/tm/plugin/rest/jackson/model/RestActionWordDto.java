/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.bdd.ActionWordParameter;
import org.squashtest.tm.service.internal.dto.TestCaseDto;
import org.squashtest.tm.service.internal.testcase.bdd.LibraryActionWordParser;

import java.util.List;

public class RestActionWordDto {

    private Long id;

    private String word;

    private String description;

    private ProjectDto project;

    private List<ActionWordParameter> parameters;

    @JsonProperty("test_cases")
    private List<TestCaseDto> testCases;


    public ActionWord convertToDto() {
        LibraryActionWordParser parser = new LibraryActionWordParser();
        ActionWord actionWord = parser.createActionWordInLibrary(word.trim());
        actionWord.setDescription(description);
        return actionWord;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }
    public void setWord(String word) {
        this.word = word;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public List<ActionWordParameter> getParameters() {
        return parameters;
    }
    public void setParameters(List<ActionWordParameter> parameters) {
        this.parameters = parameters;
    }

    public List<TestCaseDto> getTestCases() {
        return testCases;
    }
    public void setTestCases(List<TestCaseDto> testCases) {
        this.testCases = testCases;
    }

    public ProjectDto getProject() {
        return project;
    }
    public void setProject(ProjectDto project) {
        this.project = project;
    }


}
