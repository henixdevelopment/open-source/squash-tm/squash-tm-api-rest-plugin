/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestParameterService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

/**
 * Created by jlor on 10/07/2017.
 */
@WebMvcTest(RestParameterController)
class RestParameterControllerIT extends BaseControllerSpec {

    @Inject
    private RestParameterService restParameterService;

    // ***** GETting ***** //
    def "get-parameter"() {

        given:

        def parameter = SquashEntityBuilder.parameter {
            id = 47l
            name = "sampleParameter"
            description = "<p>My parameter</p>"

            testCase = SquashEntityBuilder.testCase {
                id = 102l
                name = "sample test case"
            }
        }

        and:

        restParameterService.getOne(47) >> parameter

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/parameters/{id}", 47).header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "parameter"
            "id".is 47
            "name".is "sampleParameter"
            "description".is "<p>My parameter</p>"

            "test_case".test {
                "_type".is "test-case"
                "id".is 102
                "name".is "sample test case"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/102"
            }

            selfRelIs "http://localhost:8080/api/rest/latest/parameters/47"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the parameter"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add "id (number) : the id of the parameter"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the parameter"
                        add "description (string) : the description of the parameter"
                        addAndStop "test_case (object) : the test case this parameter belongs to"
                        addAndStop "_links (object) : related links"
                    }
                    _links {
                        add "self : link to this parameter"
                    }
                }
        ))

    }

    def "get-parameter in scripted test case"() {

        given:

        def parameter = SquashEntityBuilder.parameter {
            id = 47l
            name = "sampleParameter"
            description = "<p>My parameter</p>"

            testCase = SquashEntityBuilder.scriptedTestCase {
                id = 102l
                name = "sample test case"
            }
        }

        and:

        restParameterService.getOne(47) >> parameter

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/parameters/{id}", 47).header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "parameter"
            "id".is 47
            "name".is "sampleParameter"
            "description".is "<p>My parameter</p>"

            "test_case".test {
                "_type".is "scripted-test-case"
                "id".is 102
                "name".is "sample test case"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/102"
            }

            selfRelIs "http://localhost:8080/api/rest/latest/parameters/47"
        }

    }

    def "get-parameter in keyword test case"() {

        given:

        def parameter = SquashEntityBuilder.parameter {
            id = 47l
            name = "sampleParameter"
            description = "<p>My parameter</p>"

            testCase = SquashEntityBuilder.keywordTestCase {
                id = 102l
                name = "sample test case"
            }
        }

        and:

        restParameterService.getOne(47) >> parameter

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/parameters/{id}", 47).header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "parameter"
            "id".is 47
            "name".is "sampleParameter"
            "description".is "<p>My parameter</p>"

            "test_case".test {
                "_type".is "keyword-test-case"
                "id".is 102
                "name".is "sample test case"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/102"
            }

            selfRelIs "http://localhost:8080/api/rest/latest/parameters/47"
        }

    }

    // ***** POST ***** //
    def "post-parameter"() {
        given:
        def json = """{
                "_type": "parameter",
                "name": "sampleParameter",
                "description": "<p>My parameter</p> ",  
                "test_case": {
                    "_type": "test-case",
                    "id": 102
                }
            }"""

        def parameter = SquashEntityBuilder.parameter {
            id = 47l
            name = "sampleParameter"
            description = "<p>My parameter</p>"

            testCase = SquashEntityBuilder.testCase {
                id = 102l
                name = "sample test case"
            }
        }

        and:
        restParameterService.addParameter(_) >> parameter
        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/parameters")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "parameter"
            "id".is 47
            "name".is "sampleParameter"
            "description".is "<p>My parameter</p>"

            "test_case".test {
                "_type".is "test-case"
                "id".is 102
                "name".is "sample test case"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/102"
            }

        }
    }

    def "post-parameter in scripted test case"() {
        given:
        def json = """{
                "_type": "parameter",
                "name": "sampleParameter",
                "description": "<p>My parameter</p> ",  
                "test_case": {
                    "_type": "scripted-test-case",
                    "id": 102
                }
            }"""

        def parameter = SquashEntityBuilder.parameter {
            id = 47l
            name = "sampleParameter"
            description = "<p>My parameter</p>"

            testCase = SquashEntityBuilder.scriptedTestCase {
                id = 102l
                name = "sample test case"
            }
        }

        and:
        restParameterService.addParameter(_) >> parameter
        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/parameters")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "parameter"
            "id".is 47
            "name".is "sampleParameter"
            "description".is "<p>My parameter</p>"

            "test_case".test {
                "_type".is "scripted-test-case"
                "id".is 102
                "name".is "sample test case"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/102"
            }

        }
    }

    def "post-parameter in keyword test case"() {
        given:
        def json = """{
                "_type": "parameter",
                "name": "sampleParameter",
                "description": "<p>My parameter</p> ",  
                "test_case": {
                    "_type": "keyword-test-case",
                    "id": 102
                }
            }"""

        def parameter = SquashEntityBuilder.parameter {
            id = 47l
            name = "sampleParameter"
            description = "<p>My parameter</p>"

            testCase = SquashEntityBuilder.keywordTestCase {
                id = 102l
                name = "sample test case"
            }
        }

        and:
        restParameterService.addParameter(_) >> parameter
        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/parameters")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "parameter"
            "id".is 47
            "name".is "sampleParameter"
            "description".is "<p>My parameter</p>"

            "test_case".test {
                "_type".is "keyword-test-case"
                "id".is 102
                "name".is "sample test case"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/102"
            }

        }
    }

    // ***** PATCH ***** //
    def "patch-parameter"() {
        given:
        def json = """{
                "_type": "parameter",
                "name": "Update-sampleParameter",
                "description": "<p>Update My parameter</p> "  
                
            }"""

        def parameter = SquashEntityBuilder.parameter {
            id = 47l
            name = "Update-sampleParameter"
            description = "<p>Update My parameter</p>"

            testCase = SquashEntityBuilder.testCase {
                id = 102l
                name = "sample test case"
            }
        }

        and:
        restParameterService.modifyParameter(_) >> parameter
        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/parameters/{id}", 47l)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "parameter"
            "id".is 47
            "name".is "Update-sampleParameter"
            "description".is "<p>Update My parameter</p>"

            "test_case".test {
                "_type".is "test-case"
                "id".is 102
                "name".is "sample test case"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/102"
            }

        }
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the parameter"
                    }
                }))
    }

    def "patch-parameter in scripted test case"() {
        given:
        def json = """{
                "_type": "parameter",
                "name": "Update-sampleParameter",
                "description": "<p>Update My parameter</p> "  
                
            }"""

        def parameter = SquashEntityBuilder.parameter {
            id = 47l
            name = "Update-sampleParameter"
            description = "<p>Update My parameter</p>"

            testCase = SquashEntityBuilder.scriptedTestCase {
                id = 102l
                name = "sample test case"
            }
        }

        and:
        restParameterService.modifyParameter(_) >> parameter
        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/parameters/{id}", 47l)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "parameter"
            "id".is 47
            "name".is "Update-sampleParameter"
            "description".is "<p>Update My parameter</p>"

            "test_case".test {
                "_type".is "scripted-test-case"
                "id".is 102
                "name".is "sample test case"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/102"
            }

        }
    }

    def "patch-parameter in keyword test case"() {
        given:
        def json = """{
                "_type": "parameter",
                "name": "Update-sampleParameter",
                "description": "<p>Update My parameter</p> "  
                
            }"""

        def parameter = SquashEntityBuilder.parameter {
            id = 47l
            name = "Update-sampleParameter"
            description = "<p>Update My parameter</p>"

            testCase = SquashEntityBuilder.keywordTestCase {
                id = 102l
                name = "sample test case"
            }
        }

        and:
        restParameterService.modifyParameter(_) >> parameter
        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/parameters/{id}", 47l)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "parameter"
            "id".is 47
            "name".is "Update-sampleParameter"
            "description".is "<p>Update My parameter</p>"

            "test_case".test {
                "_type".is "keyword-test-case"
                "id".is 102
                "name".is "sample test case"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/102"
            }

        }
    }

    // ***** DELETE ***** //
    def "delete-parameter"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/parameters/{id}", 169L, 189L)
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * restParameterService.deleteParameter(_)

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the list of id of the parameter"
                    }
                }
        ))
    }


}
