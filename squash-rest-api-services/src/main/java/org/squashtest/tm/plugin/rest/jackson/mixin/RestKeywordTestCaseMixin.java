/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.squashtest.tm.plugin.rest.jackson.model.RestCustomFieldMembers;
import org.squashtest.tm.plugin.rest.jackson.serializer.CustomFieldValuesPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestCaseAutomatablePropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestCaseAutomationPriorityPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestCaseAutomationStatusPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestCaseScriptAutoPropertyWriter;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonTypeName("keyword-test-case")
@JsonAppend(props = {
        @JsonAppend.Prop(name = "custom_fields", type = RestCustomFieldMembers.class, value = CustomFieldValuesPropertyWriter.class),
        @JsonAppend.Prop(name = "script_auto", value = TestCaseScriptAutoPropertyWriter.class),
        @JsonAppend.Prop(name = "automatable", value = TestCaseAutomatablePropertyWriter.class),
        @JsonAppend.Prop(name = "automation_status", value = TestCaseAutomationStatusPropertyWriter.class),
        @JsonAppend.Prop(name = "automation_priority", value = TestCaseAutomationPriorityPropertyWriter.class)
})
@JsonPropertyOrder({"_type", "id", "name", "reference", "project", "path", "parent", "audit", "importance", "status", "nature", "type",
        "prerequisite", "description", "automated_test", "automated_test_technology", "scm_repository_url", "scm_repository_id", "automated_test_reference",
        "uuid", "automatable", "automation_status", "automation_priority", "custom_fields", "steps", "parameters", "datasets", "verified_requirements"})
public abstract class RestKeywordTestCaseMixin extends RestTestCaseMixin {
}
