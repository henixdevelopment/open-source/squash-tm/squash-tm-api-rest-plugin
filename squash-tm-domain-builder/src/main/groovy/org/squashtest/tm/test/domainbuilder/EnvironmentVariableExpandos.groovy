/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.environmentvariable.DenormalizedEnvironmentVariable
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariable
import org.squashtest.tm.domain.environmentvariable.EnvironmentVariableBinding

class EnvironmentVariableExpandos {

    private EnvironmentVariableExpandos() {
        super();
    }

    static class EnvironmentVariableExpando extends BaseExpando<EnvironmentVariable> {

        EnvironmentVariableExpando() {
            super()

            id = 1L

        }
    }

    static class EnvironmentVariableBindingExpando extends BaseExpando<EnvironmentVariableBinding> {


            EnvironmentVariableBindingExpando() {
                super()

                id = 1L

            }
        }

    static class DenormalizedEnvironmentVariableExpando extends BaseExpando<DenormalizedEnvironmentVariable> {

        DenormalizedEnvironmentVariableExpando() {
            super()

            id = 1L
        }
    }
}
