/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.squashtest.tm.domain.infolist.InfoListItem;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("serial")
public class RestNatureTypeCategorySerializer extends StdSerializer<InfoListItem>{

	public RestNatureTypeCategorySerializer(){
		super(InfoListItem.class);
	}
	
	@Override
	public void serialize(InfoListItem value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        doSerialize(value, gen);
	}

    @Override
	public void serializeWithType(InfoListItem value, JsonGenerator gen, SerializerProvider serializers,
			TypeSerializer typeSer) throws IOException {
		doSerialize(value,gen);
	}

    private void doSerialize(InfoListItem value, JsonGenerator gen) throws IOException {
        Map<String, String> attributes = new HashMap<>();
        attributes.put("code", value.getCode());
        gen.writeObject(attributes);
    }
}
