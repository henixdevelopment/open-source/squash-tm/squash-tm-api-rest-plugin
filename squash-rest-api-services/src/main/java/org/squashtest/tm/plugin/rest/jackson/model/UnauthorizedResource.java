/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonAppend.Prop;
import org.squashtest.tm.plugin.rest.jackson.serializer.TypePropertyWriter;

/**
 * That pseudo entity will be serialized in place of a real Entity when the user is not authorized to read it but still needs to know it exists. The main use-case
 * is to prevent inter-projects relations (like test case &lt;-&gt; requirements, test case calls etc) to leak informations.  
 * 
 * @author bsiri
 *
 */
@JsonTypeName("unauthorized-resource")
@JsonAppend(prepend=true, props=@Prop(name="_type", value=TypePropertyWriter.class))
@JsonPropertyOrder({"_type", "resource_type", "resource_id"})
public class UnauthorizedResource {
	
	@JsonProperty("resource_type")
	private String resourceType;
	
	@JsonProperty("resource_id")
	private long resourceId;

	public UnauthorizedResource(String type, long id){
		super();
		resourceId = id;
		resourceType = type;
	}
	
	public String getResourceType() {
		return resourceType;
	}

	public long getResourceId() {
		return resourceId;
	}


	
	
}
