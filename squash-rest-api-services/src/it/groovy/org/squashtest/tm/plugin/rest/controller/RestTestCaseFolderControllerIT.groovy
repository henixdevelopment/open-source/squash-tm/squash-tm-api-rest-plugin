/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.springframework.restdocs.payload.JsonFieldType
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.squashtest.tm.domain.testcase.TestCaseFolder
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.service.RestTestCaseFolderService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.library.PathService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.hamcrest.Matchers.hasSize
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.AllInOne
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project

@WebMvcTest(RestTestCaseFolderController)
class RestTestCaseFolderControllerIT extends BaseControllerSpec {

    @Inject
    private RestTestCaseFolderService service;

    @Inject
    private PathService pathService

    @Inject
    private NodeHierarchyHelpService hierService

    @Inject
    private CustomFieldValueFinderService cufService

    @Inject
    private RestTestCaseFolderService restTestCaseFolderService;


    def "browse-test-case-folders"() {

        given:
        service.findAllReadable(_) >> { args ->
            def folders = [
                    SquashEntityBuilder.testCaseFolder {
                        id = 100L
                        name = "top-secret"
                    },
                    SquashEntityBuilder.testCaseFolder {
                        id = 101L
                        name = "confidential"
                    },
                    SquashEntityBuilder.testCaseFolder {
                        id = 102L
                        name = "restricted access"
                    }
            ]

            new PageImpl<TestCaseFolder>(folders, args[0], 10)
        }

        when:
        def res = mockMvc.perform(get("/api/rest/latest/test-case-folders?page=1&size=3")
                .header("Accept", "application/json"))

        then:

        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.test-case-folders".hasSize 3
            "_embedded.test-case-folders".test {
                "[0]".test {
                    "_type".is "test-case-folder"
                    "id".is 100
                    "name".is "top-secret"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-case-folders/100"
                }
                "[1]".test {
                    "_type".is "test-case-folder"
                    "id".is 101
                    "name".is "confidential"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-case-folders/101"
                }
                "[2]".test {
                    "_type".is "test-case-folder"
                    "id".is 102
                    "name".is "restricted access"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-case-folders/102"
                }
            }

            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 1
            }

            "_links".test {
                "first".linksTo "http://localhost:8080/api/rest/latest/test-case-folders?page=0&size=3"
                "prev".linksTo "http://localhost:8080/api/rest/latest/test-case-folders?page=0&size=3"
                "self".linksTo "http://localhost:8080/api/rest/latest/test-case-folders?page=1&size=3"
                "next".linksTo "http://localhost:8080/api/rest/latest/test-case-folders?page=2&size=3"
                "last".linksTo "http://localhost:8080/api/rest/latest/test-case-folders?page=3&size=3"
            }
        }

        // document
        res.andDo(doc.document(
                AllInOne.createBrowseAllEntities("test-case-folders")
        ))

    }

    def "browse-test-case-folders-tree-by-project"() {

        given:
        def folderProject1 = SquashEntityBuilder.project {
            id = 10L
            name = "project-1"
            testCaseLibrary = SquashEntityBuilder.tcLibrary {}
        }
        def folderProject2 = SquashEntityBuilder.project {
            id = 11L
            name = "project-2"
            testCaseLibrary = SquashEntityBuilder.tcLibrary {}
        }
        def folders = [
                SquashEntityBuilder.testCaseFolder {
                    project = folderProject1
                    id = 100L
                    name = "folder1"
                    description = "<p>All recipes in alphabetical order from A to F inclusive</p>"
                    createdBy "User-1"
                    createdOn "2011/09/30"
                    lastModifiedBy "admin"
                    lastModifiedOn "2017/06/16"
                },
                SquashEntityBuilder.testCaseFolder {
                    project = folderProject1
                    id = 101L
                    name = "folder2"
                    description = "<p>All recipes in alphabetical order from A to F inclusive</p>"
                    createdBy "User-1"
                    createdOn "2011/09/30"
                    lastModifiedBy "admin"
                    lastModifiedOn "2017/06/16"
                },
                SquashEntityBuilder.testCaseFolder {
                    project = folderProject2
                    id = 102L
                    name = "folder3"
                    description = "<p>All recipes in alphabetical order from A to F inclusive</p>"
                    createdBy "User-1"
                    createdOn "2011/09/30"
                    lastModifiedBy "admin"
                    lastModifiedOn "2017/06/16"
                }
        ]

        and:
        service.findAllByProjectIds(_) >> folders;

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-case-folders/tree/{ids}", "10,11")
                .header("Accept", "application/json"))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("[0]._type", is("project")))
                .andExpect(jsonPath("[0].id", is(10)))
                .andExpect(jsonPath("[0].name", is("project-1")))
                .andExpect(jsonPath("[0].folders[0]._type", is("test-case-folder")))
                .andExpect(jsonPath("[0].folders[0].id", is(100)))
                .andExpect(jsonPath("[0].folders[0].name", is("folder1")))
                .andExpect(jsonPath("[0].folders[0].url", is("http://localhost:8080/api/rest/latest/test-case-folders/100")))
                .andExpect(jsonPath("[0].folders[0].children", hasSize(0)))

        /*
         * Documentation
         */
        res.andDo(doc.document(
                responseFields(
                        fieldWithPath("[]._type").type(JsonFieldType.STRING).description("the type of the entity"),
                        fieldWithPath("[].id").type(JsonFieldType.NUMBER).description("id of project"),
                        fieldWithPath("[].name").type(JsonFieldType.STRING).description("name of project"),
                        fieldWithPath("[].folders").type(JsonFieldType.ARRAY).description("all folders for the given project"),
                        fieldWithPath("[].folders[]._type").type(JsonFieldType.STRING).description("the type of the entity"),
                        fieldWithPath("[].folders[].id").type(JsonFieldType.NUMBER).description("id of the test case folder"),
                        fieldWithPath("[].folders[].name").type(JsonFieldType.STRING).description("name of the test case folder"),
                        fieldWithPath("[].folders[].url").type(JsonFieldType.STRING).description("url of the test case folder"),
                        subsectionWithPath("[].folders[].children").description("children folders")
                ),

        ))

    }

    def "get-test-case-folder"() {

        given:
        def folderProject = SquashEntityBuilder.project {
            id = 10L
            name = "Recipes inventory"
            testCaseLibrary = SquashEntityBuilder.tcLibrary {}
        }

        def folder = SquashEntityBuilder.testCaseFolder {
            project = folderProject
            id = 24L
            name = "Recipes : A-F"
            description = "<p>All recipes in alphabetical order from A to F inclusive</p>"
            createdBy "User-1"
            createdOn "2011/09/30"
            lastModifiedBy "admin"
            lastModifiedOn "2017/06/16"
        }

        and:
        service.getOne(24L) >> folder
        cufService.findAllCustomFieldValues(_) >> []
        hierService.findParentFor(folder) >> folderProject.testCaseLibrary
        pathService.buildTestCasePath(24L) >> "/${folderProject.name}/${folder.name}"

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-case-folders/{id}", 24L)
                .header("Accept", "application/json"))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "test-case-folder"
            "id".is 24
            "name".is "Recipes : A-F"
            "project".test {
                "_type".is "project"
                "id".is 10
                "name".is "Recipes inventory"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/10"
            }
            "path".is "/Recipes inventory/Recipes : A-F"
            "parent".test {
                "_type".is "project"
                "id".is 10
                "name".is "Recipes inventory"
            }
            "created_by".is "User-1"
            "created_on".is "2011-09-30T10:00:00.000+00:00"
            "last_modified_by".is "admin"
            "last_modified_on".is "2017-06-16T10:00:00.000+00:00"
            "description".is "<p>All recipes in alphabetical order from A to F inclusive</p>"
            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/test-case-folders/24"
                "project".linksTo "http://localhost:8080/api/rest/latest/projects/10"
                "content".linksTo "http://localhost:8080/api/rest/latest/test-case-folders/24/content"
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test case folder"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add DescriptorLists.regularLibraryNodeFields
                        addAndStop "custom_fields (array) : the custom fields of this execution step"
                    }
                    _links {
                        add "self : the link to this folder"
                        add "project : the link to its project"
                        add "content : the link to its content"
                        add "attachments : the link to its attachments"

                    }
                }
        ))


    }

    def "get-test-case-folder-content"() {

        given:
        def content = [
                SquashEntityBuilder.testCase {
                    id = 13L
                    name = "walk test"
                },
                SquashEntityBuilder.scriptedTestCase {
                    id = 150L
                    name = "fly test"
                },
                SquashEntityBuilder.keywordTestCase {
                    id = 180L
                    name = "swim test"
                },
                SquashEntityBuilder.testCaseFolder {
                    id = 1467L
                    name = "other, non-natural motions"
                }]

        and:
        service.findFolderContent(180L, _) >> { args -> new PageImpl<TestCaseLibraryNode>(content, args[1], 3) }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-case-folders/{id}/content", 180)
                .header("Accept", "application/json"))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))

        withResult(res) {
            "_embedded.content".hasSize 4
            "_embedded.content".test {
                "[0]".test {
                    "_type".is "test-case"
                    "id".is 13
                    "name".is "walk test"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/13"
                }
                "[1]".test {
                    "_type".is "scripted-test-case"
                    "id".is 150
                    "name".is "fly test"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/150"
                }
                "[2]".test {
                    "_type".is "keyword-test-case"
                    "id".is 180
                    "name".is "swim test"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/180"
                }
                "[3]".test {
                    "_type".is "test-case-folder"
                    "id".is 1467
                    "name".is "other, non-natural motions"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-case-folders/1467"
                }
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                AllInOne.createListEntityContent("content", "test-case-folder", true)
        ))

    }

    def "post-test-case-folder"() {

        given:
        def json = """{
                    "_type" : "test-case-folder",
                    "name" : "Test case folder 1",
                    "description" : "Description Test case folder 1",
                    "custom_fields": [
                                {
                                    "code": "cuf1",
                                    "value": "Cuf1 Value"
                                }],
                    "parent" : {
                        "_type" : "project",
                        "id" : 14
                    }
                }
                """
        def proj = project {
            id = 14L
            name = "Test Project 1"
        }
        def tcf = SquashEntityBuilder.testCaseFolder {
            id = 33L
            name = "Test case folder 1"
            description = "Description Test case folder 1"
            project = proj
        }
        def lCuf = [
                SquashEntityBuilder.cufValue {
                    code = "cuf1"
                    label = "Lib Cuf1"
                    value = "Cuf1 Value"
                },
                SquashEntityBuilder.cufValue {
                    code = "cuf2"
                    label = "Lib Cuf2"
                    value = "true"
                }]

        and:
        restTestCaseFolderService.addTestCaseFolder(_) >> tcf
        cufService.findAllCustomFieldValues(_) >> lCuf
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-case-folders")
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "test-case-folder"
            "id".is 33
            "name".is "Test case folder 1"
            "description".is "Description Test case folder 1"
            // Custom Fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "cuf1"
                    "label".is "Lib Cuf1"
                    "value".is "Cuf1 Value"
                }
                "[1]".test {
                    "code".is "cuf2"
                    "label".is "Lib Cuf2"
                    "value".is "true"
                }
            }
            "parent".test {
                "_type".is "project"
                "id".is 14
            }
        }

    }

    def "patch-test-case-folder"() {
        given:
        def json = """{
                                "_type" : "test-case-folder",
                                "name" : "Update - Test case folder 1",
                                "description": "Update - Description  Test case folder 1",
                                "custom_fields": [
                                            {
                                                 "code": "cuf1",
                                                "label": "Lib Cuf1",
                                                "value": "Cuf1 new value"
                                            }]
                            }"""
        def proj = project {
            id = 14L
            name = "Test Project 1"
        }
        def lCuf = [
                SquashEntityBuilder.cufValue {
                    code = "cuf1"
                    label = "Lib Cuf1"
                    value = "Cuf1 Value"
                },
                SquashEntityBuilder.cufValue {
                    code = "cuf2"
                    label = "Lib Cuf2"
                    value = "true"
                }]
        def tcf = SquashEntityBuilder.testCaseFolder {
            id = 33L
            name = "Update - Test case folder 1"
            description = "Update - Description Test case folder 1"
            project = proj
        }

        and:
        restTestCaseFolderService.patchTestCaseFolder(_, _) >> tcf
        cufService.findAllCustomFieldValues(_) >> lCuf
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-case-folders/{id}", 33L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "test-case-folder"
            "id".is 33
            "name".is "Update - Test case folder 1"
            "description".is "Update - Description Test case folder 1"
            // Custom Fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "cuf1"
                    "label".is "Lib Cuf1"
                    "value".is "Cuf1 Value"
                }
                "[1]".test {
                    "code".is "cuf2"
                    "label".is "Lib Cuf2"
                    "value".is "true"
                }
            }
            "parent".test {
                "_type".is "project"
                "id".is 14
            }
        }

    }

    def "delete-test-case-folder"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/test-case-folders/{ids}", "21,22")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        restTestCaseFolderService.deleteFolder([21])

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the test case folders"
                    }
                }
        ))
    }

}
