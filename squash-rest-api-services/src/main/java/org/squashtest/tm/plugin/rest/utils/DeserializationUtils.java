/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.plugin.rest.core.exception.ProgrammingError;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;
import org.squashtest.tm.plugin.rest.core.jackson.WrappedDTO;

public final class DeserializationUtils {
    private DeserializationUtils(){
        super();
    }

    public static DeserializationHints getHints(DeserializationContext ctxt){
        DeserializationHints hints = (DeserializationHints)ctxt.getAttribute(DeserializationHints.HINTS_KEY);

        if (hints == null){
            throw new ProgrammingError("No hints found in the deserialization context ! The hints are available only for url endpoint using annotation @PersistentEntity.");
        }

        return hints;
    }


    /**
     * return the object in the parent context. If that object is a subclass of {@link WrappedDTO}, the wrapped object will be returned instead.
     *
     * @param parser
     * @return
     */
    public static Object findEnclosingUnwrappedInstance(JsonParser parser){
        Object constructed = parser.getParsingContext().getParent().getCurrentValue();

        if (WrappedDTO.class.isAssignableFrom(constructed.getClass())){
            constructed = ((WrappedDTO)constructed).getWrapped();
        }

        return constructed;
    }

    public static BoundEntity findBoundEntity(JsonParser parser){
        Object constructed = findEnclosingUnwrappedInstance(parser);

        if (BoundEntity.class.isAssignableFrom(constructed.getClass())){
            return (BoundEntity)constructed;
        }
        else{
            return null;
        }
    }

    public static BoundEntity findBoundEntityOrDie(JsonParser p){
        BoundEntity entity = findBoundEntity(p);
        if (entity == null){
            Class<?> clz = findEnclosingUnwrappedInstance(p).getClass();
            throw new ProgrammingError("expected to find an instance of BoundEntity (an entity that can have custom field value) but found instead : "+clz);
        }
        return entity;
    }



}
