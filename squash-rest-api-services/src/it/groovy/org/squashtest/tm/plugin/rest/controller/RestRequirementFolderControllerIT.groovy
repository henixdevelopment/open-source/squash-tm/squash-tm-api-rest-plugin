/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.springframework.restdocs.payload.JsonFieldType
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.squashtest.tm.domain.requirement.RequirementFolder
import org.squashtest.tm.domain.requirement.RequirementLibraryNode
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.service.RestRequirementFolderService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.library.PathService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.hamcrest.Matchers.hasSize
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.hamcrest.Matchers.is
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.AllInOne
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestRequirementFolderController)
public class RestRequirementFolderControllerIT extends BaseControllerSpec {

    @Inject
    private RestRequirementFolderService reqFolderservice;
    @Inject
    private NodeHierarchyHelpService hierService
    @Inject
    private CustomFieldValueFinderService cufService
    @Inject
    private PathService pathService
    @Inject
    private RestRequirementFolderService restRequirementFolderService;


    def "browse-requirement-folders"() {

        given:

        reqFolderservice.findAllReadable(_) >> { args ->
            def folders = [
                    SquashEntityBuilder.requirementFolder {
                        id = 23l
                        resource = SquashEntityBuilder.resource {
                            name = "sample folder 1"
                        }
                    },
                    SquashEntityBuilder.requirementFolder {
                        id = 26l
                        resource = SquashEntityBuilder.resource {
                            name = "sample folder 2"
                        }
                    },
                    SquashEntityBuilder.requirementFolder {
                        id = 31l
                        resource = SquashEntityBuilder.resource {
                            name = "sample folder 3"
                        }
                    }
            ]

            new PageImpl<RequirementFolder>(folders, args[0], 10)
        }

        when:

        def res = mockMvc.perform(get("/api/rest/latest/requirement-folders?page=1&size=3")
                .header("Accept", "application/json"))
        then:

        /*
        * Test (With TestHelper)
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.requirement-folders".hasSize 3
            "_embedded.requirement-folders".test {
                "[0]".test {
                    "_type".is "requirement-folder"
                    "id".is 23
                    "name".is "sample folder 1"
                    selfRelIs "http://localhost:8080/api/rest/latest/requirement-folders/23"
                }
                "[1]".test {
                    "_type".is "requirement-folder"
                    "id".is 26
                    "name".is "sample folder 2"
                    selfRelIs "http://localhost:8080/api/rest/latest/requirement-folders/26"
                }
                "[2]".test {
                    "_type".is "requirement-folder"
                    "id".is 31
                    "name".is "sample folder 3"
                    selfRelIs "http://localhost:8080/api/rest/latest/requirement-folders/31"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/requirement-folders?page=1&size=3"

            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 1
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                AllInOne.createBrowseAllEntities("requirement-folders")
        ))

    }

    def "browse-requirement-folders-tree-by-project"() {

        given:
        def folderProject1 = SquashEntityBuilder.project {
            id = 10L
            name = "project-1"

        }
        def folderProject2 = SquashEntityBuilder.project {
            id = 11L
            name = "project-2"

        }


        def folders = [
                SquashEntityBuilder.requirementFolder {
                    project = folderProject1
                    id = 100L
                    resource = SquashEntityBuilder.resource {
                        name = "folder1"
                        description = "<p>An embedded folder...</p>"
                        createdBy "User-1"
                        createdOn "2017/07/19"
                        lastModifiedBy "User-2"
                        lastModifiedOn "2017/07/20"
                    }
                },
                SquashEntityBuilder.requirementFolder {
                    project = folderProject1
                    id = 110L
                    resource = SquashEntityBuilder.resource {
                        name = "folder2"
                        description = "<p>An embedded folder...</p>"
                        createdBy "User-1"
                        createdOn "2017/07/19"
                        lastModifiedBy "User-2"
                        lastModifiedOn "2017/07/20"
                    }
                },
                SquashEntityBuilder.requirementFolder {
                    project = folderProject2
                    id = 120L
                    resource = SquashEntityBuilder.resource {
                        name = "folder3"
                        description = "<p>An embedded folder...</p>"
                        createdBy "User-1"
                        createdOn "2017/07/19"
                        lastModifiedBy "User-2"
                        lastModifiedOn "2017/07/20"
                    }
                }
        ]

        and:
        reqFolderservice.findAllRequirementFoldersByPojectIds(_) >> folders;

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/requirement-folders/tree/{ids}", "10,11")
                .header("Accept", "application/json"))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("[0]._type", is("project")))
                .andExpect(jsonPath("[0].id", is(10)))
                .andExpect(jsonPath("[0].name", is("project-1")))
                .andExpect(jsonPath("[0].folders[0]._type", is("requirement-folder")))
                .andExpect(jsonPath("[0].folders[0].id", is(100)))
                .andExpect(jsonPath("[0].folders[0].name", is("folder1")))
                .andExpect(jsonPath("[0].folders[0].url", is("http://localhost:8080/api/rest/latest/requirement-folders/100")))
                .andExpect(jsonPath("[0].folders[0].children", hasSize(0)))

        /*
         * Documentation
         */
        res.andDo(doc.document(
                responseFields(
                        fieldWithPath("[]._type").type(JsonFieldType.STRING).description("the type of the entity"),
                        fieldWithPath("[].id").type(JsonFieldType.NUMBER).description("id of project"),
                        fieldWithPath("[].name").type(JsonFieldType.STRING).description("name of project"),
                        fieldWithPath("[].folders").type(JsonFieldType.ARRAY).description("all folders for the given project"),
                        fieldWithPath("[].folders[]._type").type(JsonFieldType.STRING).description("the type of the entity"),
                        fieldWithPath("[].folders[].id").type(JsonFieldType.NUMBER).description("id of the requirement folder"),
                        fieldWithPath("[].folders[].name").type(JsonFieldType.STRING).description("name of the requirement folder"),
                        fieldWithPath("[].folders[].url").type(JsonFieldType.STRING).description("url of the requirement folder"),
                        subsectionWithPath("[].folders[].children").description("children folders")
                ),

        ))

    }

    def "get-requirement-folder"() {

        given:

        def folderProject = SquashEntityBuilder.project {
            id = 12l
            name = "sample project"
        }

        def parentFolder = SquashEntityBuilder.requirementFolder {
            id = 34l
            resource = SquashEntityBuilder.resource {
                name = "sample parent folder"
            }
        }

        def folder = SquashEntityBuilder.requirementFolder {
            project = folderProject
            id = 356l
            resource = SquashEntityBuilder.resource {
                name = "embedded folder"
                description = "<p>An embedded folder...</p>"
                createdBy "User-1"
                createdOn "2017/07/19"
                lastModifiedBy "User-2"
                lastModifiedOn "2017/07/20"
            }
        }

        and:

        reqFolderservice.getOne(356) >> folder
        cufService.findAllCustomFieldValues(_) >> []
        hierService.findParentFor(folder) >> parentFolder
        pathService.buildRequirementPath(356) >> "/${folderProject.name}/${parentFolder.name}/${folder.name}"

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/requirement-folders/{id}", 356)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (with TestHelper)
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "requirement-folder"
            "id".is 356
            "project".test {
                "_type".is "project"
                "id".is 12
                "name".is "sample project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/12"
            }
            "path".is "/sample project/sample parent folder/embedded folder"
            "parent".test {
                "_type".is "requirement-folder"
                "id".is 34
                "name".is "sample parent folder"
                selfRelIs "http://localhost:8080/api/rest/latest/requirement-folders/34"
            }
            "name".is "embedded folder"
            "created_by".is "User-1"
            "created_on".is "2017-07-19T10:00:00.000+00:00"
            "last_modified_by".is "User-2"
            "last_modified_on".is "2017-07-20T10:00:00.000+00:00"
            "description".is "<p>An embedded folder...</p>"

            selfRelIs "http://localhost:8080/api/rest/latest/requirement-folders/356"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/12"
                "content.href".is "http://localhost:8080/api/rest/latest/requirement-folders/356/content"
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the requirement folder"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add DescriptorLists.regularLibraryNodeFields
                        addAndStop "custom_fields (array) : the custom fields of this execution step"
                    }
                    _links {
                        add "self : the link of the folder"
                        add "project : the link of its project"
                        add "content : the link of its content"
                        add "attachments : the link of its attachments"
                    }
                }
        ))
    }

    def "get-requirement-folder-content"() {

        given:

        def content = [
                SquashEntityBuilder.requirement {
                    id = 78l
                    resource = SquashEntityBuilder.requirementVersion {
                        id = 111l
                        name = "embedded requirement 1"
                    }
                },
                SquashEntityBuilder.requirement {
                    id = 44l
                    resource = SquashEntityBuilder.requirementVersion {
                        id = 77l
                        name = "embedded requirement 2"
                    }
                },
                SquashEntityBuilder.requirementFolder {
                    id = 12l
                    resource = SquashEntityBuilder.resource {
                        id = 7l
                        name = "embedded folder"
                    }
                }
        ]

        and:

        reqFolderservice.findFolderContent(71, _) >> { args ->
            new PageImpl<RequirementLibraryNode>(content, args[1], 6)
        }

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/requirement-folders/{id}/content?size=3&page=1", 71)
                .header("Accept", "application/json"))

        then:

        /*
	    * Test (with TestHelper)
		*/
        res.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))

        withResult(res) {
            "_embedded.content".hasSize 3
            "_embedded.content".test {
                "[0]".test {
                    "_type".is "requirement"
                    "id".is 78
                    "name".is "embedded requirement 1"
                    selfRelIs "http://localhost:8080/api/rest/latest/requirements/78"
                }
                "[1]".test {
                    "_type".is "requirement"
                    "id".is 44
                    "name".is "embedded requirement 2"
                    selfRelIs "http://localhost:8080/api/rest/latest/requirements/44"
                }
                "[2]".test {
                    "_type".is "requirement-folder"
                    "id".is 12
                    "name".is "embedded folder"
                    selfRelIs "http://localhost:8080/api/rest/latest/requirement-folders/12"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/requirement-folders/71/content?page=1&size=3"
            "page".test {
                "size".is 3
                "totalElements".is 6
                "totalPages".is 2
                "number".is 1
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                AllInOne.createListEntityContent("content", "requirement-folder", true)
        ))
    }

    def "post-requirement-folder"() {

        given:
        def json = """{
                "_type" : "requirement-folder",
                "name" : "Requirement subfolder 1",
                "custom_fields": [
                            {
                                "code": "cuf1",
                                "value": "Cuf1 Value"
                            }],
                "parent" : {
                    "_type" : "requirement-folder",
                    "id" : 11
                }
            }
            """


        def prj = SquashEntityBuilder.project {
            id = 14L
            name = "Test Project 1"
        }

        def parentFolder = SquashEntityBuilder.requirementFolder {
            id = 11L
            name = "Requirement folder 1"
        }

        def subF = SquashEntityBuilder.requirementFolder {
            project = prj
            id = 33l
            resource = SquashEntityBuilder.resource {
                name = "Requirement subfolder 1"

            }
        }
        def lCuf = [
                SquashEntityBuilder.cufValue {
                    code = "cuf1"
                    label = "Lib Cuf1"
                    value = "Cuf1 Value"
                },
                SquashEntityBuilder.cufValue {
                    code = "cuf2"
                    label = "Lib Cuf2"
                    value = "true"
                }]

        and:
        restRequirementFolderService.addRequirementFolder(_) >> subF
        cufService.findAllCustomFieldValues(_) >> lCuf
        pathService.buildRequirementPath(_) >> '/Folder 1/Requirement subfolder 1'
        hierService.findParentFor(_) >> parentFolder

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/requirement-folders")
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "requirement-folder"
            "id".is 33
            "name".is "Requirement subfolder 1"
            // Custom Fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "cuf1"
                    "label".is "Lib Cuf1"
                    "value".is "Cuf1 Value"
                }
                "[1]".test {
                    "code".is "cuf2"
                    "label".is "Lib Cuf2"
                    "value".is "true"
                }
            }
            "parent".test {
                "_type".is "requirement-folder"
                "id".is 11
            }
        }

    }

    def "patch-requirement-folder"() {
        given:
        def json = """{
                            "_type" : "requirement-folder",
                            "name" : "Update - Requirement subfolder 1",
                            "description": "Update - Description Requirement subfolder 1",
                            "custom_fields": [
                                        {
                                             "code": "cuf1",
                                            "label": "Lib Cuf1",
                                            "value": "Cuf1 new value"
                                        }]
                        }"""
        def prj = SquashEntityBuilder.project {
            id = 14L
            name = "Test Project 1"
        }
        def parentFolder = SquashEntityBuilder.requirementFolder {
            id = 11L
            name = "Requirement folder 1"
        }
        def subF = SquashEntityBuilder.requirementFolder {
            project = prj
            id = 33L
            resource = SquashEntityBuilder.resource {
                name = "Update - Requirement subfolder 1"
                description = "Update - Description Requirement subfolder 1"

            }
        }
        def lCuf = [
                SquashEntityBuilder.cufValue {
                    code = "cuf1"
                    label = "Lib Cuf1"
                    value = "Cuf1 Value"
                },
                SquashEntityBuilder.cufValue {
                    code = "cuf2"
                    label = "Lib Cuf2"
                    value = "true"
                }]

        and:
        restRequirementFolderService.patchRequirementFolder(_, _) >> subF
        cufService.findAllCustomFieldValues(_) >> lCuf
        pathService.buildRequirementPath(_) >> '/Folder 1/Update - Requirement subfolder 1'
        hierService.findParentFor(_) >> parentFolder

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/requirement-folders/{id}", 33L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "requirement-folder"
            "id".is 33
            "name".is "Update - Requirement subfolder 1"
            "description".is "Update - Description Requirement subfolder 1"
            // Custom Fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "cuf1"
                    "label".is "Lib Cuf1"
                    "value".is "Cuf1 Value"
                }
                "[1]".test {
                    "code".is "cuf2"
                    "label".is "Lib Cuf2"
                    "value".is "true"
                }
            }
            "parent".test {
                "_type".is "requirement-folder"
                "id".is 11
            }
        }

    }

    def "delete-requirement-folder"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/requirement-folders/{ids}", "21,22")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        restRequirementFolderService.deleteFolder([21])

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the requirement folders"
                    }
                }
        ))
    }

}
