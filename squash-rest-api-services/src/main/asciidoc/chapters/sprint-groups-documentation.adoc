== Sprint Groups

This chapter focuses on services for the sprint groups.

Endpoints only contain minimal information for this version. More information and endpoints will be available in future versions.

=== image:get.png[] Get sprint group

A `GET` to `/sprint-groups/{id}` returns the sprint group with the given id.

operation::RestSprintGroupControllerIT/get-sprint-group[snippets='path-parameters,http-request,http-response,response-fields,links']
