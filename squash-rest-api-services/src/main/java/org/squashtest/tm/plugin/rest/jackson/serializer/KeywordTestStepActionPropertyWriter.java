/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.Annotations;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.actionword.ConsumerForActionWordFragmentVisitor;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.bdd.ActionWordFragment;
import org.squashtest.tm.domain.bdd.ActionWordParameter;
import org.squashtest.tm.domain.bdd.ActionWordParameterValue;
import org.squashtest.tm.domain.testcase.KeywordTestStep;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@Component
public class KeywordTestStepActionPropertyWriter extends VirtualBeanPropertyWriter {

    private static final String ESCAPED_BACKSLASH = "\"";

    public KeywordTestStepActionPropertyWriter() {
        super();
    }

    private KeywordTestStepActionPropertyWriter(
            BeanPropertyDefinition propDef,
            Annotations contextAnnotations,
            JavaType declaredType) {
        super(propDef, contextAnnotations, declaredType);
    }

    @Override
    protected Object value(Object bean, JsonGenerator gen, SerializerProvider prov) throws Exception {
        KeywordTestStep holder = (KeywordTestStep) bean;
        ActionWord actionWord = holder.getActionWord();
        List<ActionWordFragment> fragments = actionWord.getFragments();
        List<ActionWordParameterValue> paramValues = holder.getParamValues();
        return createWord(fragments, paramValues);
    }

    private String createWord(List<ActionWordFragment> fragments, List<ActionWordParameterValue> paramValues) {
        StringBuilder builder = new StringBuilder();
        Consumer<ActionWordParameter> consumer = parameter ->
                appendParamValueToCreateWord(parameter, paramValues, builder);
        ConsumerForActionWordFragmentVisitor visitor = new ConsumerForActionWordFragmentVisitor(consumer, builder);
        for (ActionWordFragment fragment : fragments) {
            fragment.accept(visitor);
        }
        return builder.toString();
    }

    private void appendParamValueToCreateWord(ActionWordParameter param, List<ActionWordParameterValue> paramValues, StringBuilder builder) {
        Optional<ActionWordParameterValue> paramValue =
                paramValues.stream().filter(pv -> pv.getActionWordParam() != null && pv.getActionWordParam().getId().equals(param.getId())).findAny();
        paramValue.ifPresent(
                actionWordParameterValue -> {
                    if(actionWordParameterValue.isLinkedToTestCaseParam()) {
                        builder.append(actionWordParameterValue.getValue());
                    } else {
                        builder
                                .append(ESCAPED_BACKSLASH)
                                .append(actionWordParameterValue.getValue())
                                .append(ESCAPED_BACKSLASH);
                    }
                }
        );
    }

    @Override
    public VirtualBeanPropertyWriter withConfig(MapperConfig<?> config, AnnotatedClass declaringClass, BeanPropertyDefinition propDef, JavaType type) {
        return new KeywordTestStepActionPropertyWriter(propDef, declaringClass.getAnnotations(), type);
    }
}
