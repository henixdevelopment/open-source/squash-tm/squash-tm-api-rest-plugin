/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.configuration;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.Serializers;
import org.squashtest.tm.plugin.rest.core.jackson.SquashRestDeserializerModifier;
import org.squashtest.tm.plugin.rest.core.jackson.SquashRestSerializerModifier;
import org.squashtest.tm.plugin.rest.core.jackson.SquashRestSerializers;
import org.squashtest.tm.plugin.rest.core.jackson.StackableAnnotationIntrospector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;


/**
 * This is where all our custom Jackson configuration goes. It should NEVER be published as a Spring bean, otherwise it
 * would be picked up by the main application and could break its behavior. It is consumed internally by {@link ApiWebConfig}.
 * 
 * @author bsiri
 *
 */
public class SquashRestApiJacksonModule extends SimpleModule {
	
	private static final long serialVersionUID = 1L;
	
	private Collection<SquashRestApiJacksonModuleConfigurer> extraConfigurers = new ArrayList<>();
	
	public SquashRestApiJacksonModule(){
		this(Collections.<SquashRestApiJacksonModuleConfigurer>emptyList());
	}
	
	public SquashRestApiJacksonModule(Collection<SquashRestApiJacksonModuleConfigurer> configurers){
		super("RestApiModule", new Version(1, 0, 0, "REL"));		
		this.extraConfigurers = configurers;
	}

	
	@Override
	public void setupModule(SetupContext context) {
		
		/*
		 * Annotation introspectors
		 */
		
		context.appendAnnotationIntrospector(new StackableAnnotationIntrospector());
	
		
		/*
		 * Serialize modifiers
		 * 
		 */
		context.addBeanSerializerModifier(new SquashRestSerializerModifier());

		/*
		 * Deserializer modifiers
		 */
		context.addBeanDeserializerModifier(new SquashRestDeserializerModifier());

		/*
		 * Custom serializers
		 * 
		 */
		Serializers serializers = new SquashRestSerializers();		
		context.addSerializers(serializers);

		/*
		 * Extra configuration (most notably the mixins defined in the service layer
		 * 
		 */
		if (extraConfigurers != null && ! extraConfigurers.isEmpty()){
			for (SquashRestApiJacksonModuleConfigurer configurer : extraConfigurers){
				configurer.setupModule(context);
			}
		}
		
		
	}
	

}


















