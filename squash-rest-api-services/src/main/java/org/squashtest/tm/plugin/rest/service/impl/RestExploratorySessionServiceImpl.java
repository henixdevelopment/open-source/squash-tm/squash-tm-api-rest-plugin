/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.campaign.ExploratorySessionOverview;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.plugin.rest.repository.RestExploratorySessionRepository;
import org.squashtest.tm.plugin.rest.service.RestExploratorySessionService;

import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
@Service
@Transactional
public class RestExploratorySessionServiceImpl implements RestExploratorySessionService {

    @Inject
    private RestExploratorySessionRepository exploratorySessionRepository;

    @Override
    @PreAuthorize("hasPermission(#sessionId, 'org.squashtest.tm.domain.campaign.ExploratorySessionOverview', 'READ') or hasRole('ROLE_ADMIN')")
    @Transactional(readOnly=true)
    public ExploratorySessionOverview getOne(long sessionId) {
         return exploratorySessionRepository.getReferenceById(sessionId);
    }

    @Override
    public Page<ExploratoryExecution> findExploratoryExecutionsByItpiId(long itpiId, Pageable pageable) {
        return exploratorySessionRepository.findAllExploratoryExecutionsByItpiId(itpiId, pageable);
    }
}
