/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestSprintGroupService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult


@WebMvcTest(RestSprintGroupController)
public class RestSprintGroupControllerIT extends BaseControllerSpec {
    @Inject
    private RestSprintGroupService restSprintGroupService

    def "get-sprint-group"() {
        given:
        def campaignProject = SquashEntityBuilder.project {
            id = 16L
            name = "sample project"
        }
        def sprintGroup = SquashEntityBuilder.sprintGroup {
            id = 123L
            name = "sample sprint group"
            project = campaignProject
        }

        and:
        restSprintGroupService.getOne(123) >> sprintGroup

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/sprint-groups/{id}", 123)
            .header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "sprint-group"
            "id".is 123
            "name".is "sample sprint group"

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/sprint-groups/123"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/16"
            }
        }

        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the sprint group"
                }

                fields {
                    add "_type (string) : the type of this entity"
                    add "id (number) : the id of this sprint group"
                    add "name (string) : the name of this sprint group"
                    add DescriptorLists.linksFields
                }

                _links {
                    add "self : link to this sprint group"
                    add "project : link to the project of this sprint group"
                }
            }
        ))
    }
}
