== Parameters

This chapter focuses on services for the parameters.

=== image:get.png[] Get parameter

A `GET` to `/parameters/{id}` returns the parameter with the given id.

operation::RestParameterControllerIT/get-parameter[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== image:post.png[] Create parameter

A `POST` to `/parameters` creates a new parameter.

operation::RestParameterControllerIT/post-parameter[snippets='http-request,http-response']

=== image:patch.png[] Modify parameter

A `Patch` to `/parameters/{id}` modifies the parameter with the given id. You can modify name and/or description.

operation::RestParameterControllerIT/patch-parameter[snippets='path-parameters,http-request,http-response']

=== image:delete.png[] Delete parameter

A `DELETE` to `/parameters/{id}` deletes one parameter  with the given id.

operation::RestParameterControllerIT/delete-parameter[snippets='path-parameters,http-request']

