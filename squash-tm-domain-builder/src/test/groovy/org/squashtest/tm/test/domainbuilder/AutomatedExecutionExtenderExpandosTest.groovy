/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.testautomation.TestAutomationServerKind
import spock.lang.Specification

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class AutomatedExecutionExtenderExpandosTest extends Specification {


    def "should create a automated execution extender"(){

        when :
        def autoTest = SquashEntityBuilder.automatedTest {
            id = 569L
            name = "auto test"
            project = SquashEntityBuilder.testAutomatioProject {
                id = 569L
                jobName = "job 1"
                label = "wan wan"
                server = SquashEntityBuilder.testAutomationServer {
                    id = 569L
                    name = "TA server"
                    url = "http://1234:4567/jenkins/"
                    login = "ajenkins"
                    password = "password"
                    kind = TestAutomationServerKind.jenkins
                    description = "<p>nice try</p>"
                }
                tmProject = SquashEntityBuilder.project {
                    id = 367L
                    description = "<p>This project is the main sample project</p>"
                    label = "Main Sample Project"
                }
                slaves = "no slavery"
            }
        }

        def exec = SquashEntityBuilder.execution {

            referencedTestCase = SquashEntityBuilder.testCase {
            }

            id = 56l
            name = "sample test case 56"
            executionOrder = 4
            executionStatus "BLOCKED"

            lastExecutedBy = "User-5"
            lastExecutedOn "2017/07/24"

            executionMode "MANUAL"
            reference = "SAMP_EXEC_56"
            datasetLabel = "sample dataset"

            steps = [
                    SquashEntityBuilder.executionStep {
                        id = 22l
                        executionStatus "SUCCESS"
                        action = "<p>First action</p>"
                        expectedResult = "<p>First result</p>"
                    },
                    SquashEntityBuilder.executionStep {
                        id = 23l
                        executionStatus "BLOCKED"
                        action = "<p>Second action</p>"
                        expectedResult = "<p>Second result</p>"
                    },
                    SquashEntityBuilder.executionStep {
                        id = 27l
                        executionStatus "SUCCESS"
                        action = "<p>The Action</p>"
                        expectedResult = "<p>The Result</p>"
                    }
            ]

            description = "<p>I have no comment</p>"
            prerequisite = "<p>Being alive.</p>"
            tcdescription = "<p>This is nice.</p>"
            importance "LOW"
            nature "NAT_SECURITY_TESTING"
            type "TYP_EVOLUTION_TESTING"
            status "APPROVED"
            testPlan = SquashEntityBuilder.iterationTestPlanItem {
                id = 15L
                iteration = SquashEntityBuilder.iteration {
                    campaign = SquashEntityBuilder.campaign {
                        project = SquashEntityBuilder.project {
                            id = 87L
                        }
                    }
                }
            }

        }

        def res = SquashEntityBuilder.automatedExecutionExtender {
            id = 778L
            automatedTest = autoTest
            execution = exec
            resultURL = new URL("http://1234:4567/jenkins/report")
            resultSummary = "all right"
            nodeName = "no root"
        }

        then :
        res.id == 778

    }
}
