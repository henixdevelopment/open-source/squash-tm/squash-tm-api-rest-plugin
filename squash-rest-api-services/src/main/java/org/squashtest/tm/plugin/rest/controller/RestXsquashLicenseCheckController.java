/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.service.RestXsquashLicenseCheckService;

import javax.inject.Inject;

/**
 * This controller is for Xsquash datacenter version needs
 * It will not be documented
 */
@RestApiController
@UseDefaultRestApiConfiguration
public class RestXsquashLicenseCheckController extends BaseRestController {

    @Inject
    private RestXsquashLicenseCheckService restXsquashLicenseCheckService;


    @GetMapping(value = "/xsquash/is-valid-license")
    @DynamicFilterExpression("*")
    public ResponseEntity<Boolean> isValidLicenseForXsquash() {
        Boolean isValid = restXsquashLicenseCheckService.isValidLicenseForXsquash();
        return ResponseEntity.ok(isValid);
    }

}
