/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignFolderDto;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Created by jthebault on 16/06/2017.
 */
public interface RestCampaignFolderService {


    CampaignFolder getOne(Long campaignFolderId) ;


	/**
	 * Returns a paged list of all the CampaignFolders, as long as the requestor can read them
	 *
	 * @param pageable
	 * @return
	 */
	Page<CampaignFolder> findAllReadable(Pageable pageable);

	/**
	 * Returns a paged list of all the CampaignFolders, as long as the requestor can read them
	 *
	 * @param projectIds
	 * @return
	 */
	List<CampaignFolder> findCampaignFolderByProject(List<Long> projectIds);


	/**
	 * Returns the paged content of a CampaignFolder, given its id
	 *
	 * @param folderId
	 * @param pageable
	 * @return
	 */
    Page<CampaignLibraryNode> findFolderContent(long folderId, Pageable pageable);

	/**
	 * Returns the paged content of a folder, including every descendant nodes
	 *
	 *
	 * @param folderId
	 * @param pageable
	 * @return
	 */
	Page<CampaignLibraryNode> findFolderAllContent(long folderId, Pageable pageable);

	CampaignFolder addCampaignFolder(CampaignFolderDto folderDto )throws InvocationTargetException, IllegalAccessException;

	CampaignFolder patchCampaignFolder(CampaignFolderDto patch, long id);

	void deleteFolder( List<Long> folderIds);

    List<Long> getExecutionIdsByCampaignFolder(Long folderId);
}
