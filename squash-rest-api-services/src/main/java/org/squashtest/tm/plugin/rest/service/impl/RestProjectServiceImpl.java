/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import com.google.common.collect.ListMultimap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.LinkRelation;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.NodeReference;
import org.squashtest.tm.domain.NodeWorkspace;
import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.resource.Resource;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.domain.users.PartyProjectPermissionsBean;
import org.squashtest.tm.plugin.rest.core.utils.ExceptionUtils;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectCopyParameterDto;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectDto;
import org.squashtest.tm.plugin.rest.jackson.model.GenericProjectDtoVisitor;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyProfileDto;
import org.squashtest.tm.plugin.rest.jackson.model.ProjectDto;
import org.squashtest.tm.plugin.rest.jackson.model.ProjectTemplateDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyClearance;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyPermission;
import org.squashtest.tm.plugin.rest.jackson.serializer.SnakeCaseSerializer;
import org.squashtest.tm.plugin.rest.repository.RestGenericProjectRepository;
import org.squashtest.tm.plugin.rest.service.RestPartyService;
import org.squashtest.tm.plugin.rest.service.RestProjectService;
import org.squashtest.tm.plugin.rest.service.TestCaseLibraryFinderService;
import org.squashtest.tm.service.internal.repository.ProfileDao;
import org.squashtest.tm.service.internal.repository.ProjectDao;
import org.squashtest.tm.service.internal.repository.display.SingleHierarchyTreeBrowserDao;
import org.squashtest.tm.service.project.GenericProjectManagerService;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.project.ProjectManagerService;
import org.squashtest.tm.service.project.ProjectTemplateManagerService;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.user.UserAccountService;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static org.squashtest.tm.plugin.rest.utils.PaginationUtils.convertListToPage;
import static org.squashtest.tm.plugin.rest.utils.PaginationUtils.emptyPage;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

@Service
@Transactional
public class RestProjectServiceImpl implements RestProjectService {

    private static final String NAMESPACE = "squashtest.acl.group.tm.";
    private static final String ACCESS_DENIED_MESSAGE = "access is denied";
    private static final List<String> invalidEntityTypes = List.of("projects", "reports", "dashboards", "charts", "custom-exports", "reporting-folders", "custom-report");


    @Inject
    private RestGenericProjectRepository dao;

    @Inject
    private ProjectDao projectDao;

    @Inject
    private PermissionEvaluationService permService;

    @Inject
    private GenericProjectManagerService genericProjectManager;

    @Inject
    private ProjectManagerService projectManager;

    @Inject
    private ProjectTemplateManagerService projectTemplateManagerService;

    @Inject
    private RestPartyService restPartyService;

    @Inject
    private ProfileDao profileDao;

    @Inject
    private ProjectFinder projectFinder;

    @Inject
    private UserAccountService userAccountService;

    @Inject
    private TestCaseLibraryFinderService testCaseLibraryFinderService;

    @Inject
    private PermissionEvaluationService permissionEvaluationService;

    @Inject
    SingleHierarchyTreeBrowserDao treeBrowserDao;

    @Override
    @PreAuthorize("hasPermission(#id, 'org.squashtest.tm.domain.project.Project' , 'MANAGE_PROJECT')"
        + " or hasPermission(#id, 'org.squashtest.tm.domain.project.ProjectTemplate' , 'MANAGE_PROJECT')"
        + OR_HAS_ROLE_ADMIN)
    public Boolean isGenericProjectExist(long id) {
        return dao.existsById(id);
    }

    @Override
    @PreAuthorize("hasPermission(#id, 'org.squashtest.tm.domain.project.Project' , 'MANAGE_PROJECT')"
        + " or hasPermission(#id, 'org.squashtest.tm.domain.project.ProjectTemplate' , 'MANAGE_PROJECT')"
        + OR_HAS_ROLE_ADMIN)
    public GenericProject getOne(long id) {
        return dao.getOne(id);
    }

    public GenericProject getOneByName(String projectName) {
        GenericProject project = dao.getOneByName(projectName);
        if (isNull(project)) {
            throw new EntityNotFoundException("Unable to find org.squashtest.tm.domain.project.GenericProject with name " + projectName);
        }
        if (!permService.hasPermissionOnObject(Permissions.MANAGE_PROJECT.name(), project) && !permService.hasRole(Roles.ROLE_ADMIN)) {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
        return project;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<GenericProject> findAllReadable(Pageable pageable) {
        return userAccountService.findCurrentUserDto().isAdmin() ?
           dao.findAll(pageable) : findAllReadableStandardProject(pageable);
    }

    @Override
    public Page<GenericProject> findAllReadableProjectTemplate(Pageable pageable) {
        return userAccountService.findCurrentUserDto().isAdmin() ?
            dao.findAllTemplates(pageable) : emptyPage(pageable);
    }

    @Override
    public Page<GenericProject> findAllReadableStandardProject(Pageable pageable) {
        List<Long> readableProjectIds = projectFinder.findAllReadableIds();

        if (readableProjectIds.isEmpty()) {
            return emptyPage(pageable);
        }

        List<GenericProject> listGenericProject = Collections.unmodifiableList(projectDao.findByIdInOrderByName(readableProjectIds));

        return convertListToPage(listGenericProject, pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RequirementLibraryNode<Resource>> findRequirementLibraryAllContent(long id, Pageable paging) {
        checkReadOnRequirements(id);
        return dao.findRequirementLibraryAllContent(id, paging);
    }


    @Override
    @Transactional(readOnly = true)
    public Page<RequirementLibraryNode<Resource>> findRequirementLibraryRootContent(long id, Pageable paging) {
        checkReadOnRequirements(id);
        return dao.findRequirementLibraryRootContent(id, paging);
    }


    @Override
    @Transactional(readOnly = true)
    // secured by direct call to permService
    public Page<TestCaseLibraryNode> findTestCaseLibraryAllContent(long id, Pageable paging) {
        checkReadOnTestCases(id);
        return dao.findTestCaseLibraryAllContent(id, paging);
    }

    @Override
    @Transactional(readOnly = true)
    // secured by direct call to permService
    public Page<TestCaseLibraryNode> findTestCaseLibraryRootContent(long id, Pageable paging) {
        checkReadOnTestCases(id);
        return dao.findTestCaseLibraryRootContent(id, paging);
    }

    @Override
    @Transactional(readOnly = true)
    // secured by direct call to permService
    public Page<CampaignLibraryNode> findCampaignLibraryAllContent(long id, Pageable paging) {
        checkReadOnCampaigns(id);
        return dao.findCampaignLibraryAllContent(id, paging);
    }

    @Override
    @Transactional(readOnly = true)
    // secured by direct call to permService
    public Page<CampaignLibraryNode> findCampaignLibraryRootContent(long id, Pageable paging) {
        checkReadOnCampaigns(id);
        return dao.findCampaignLibraryRootContent(id, paging);
    }

    @Override
    public GenericProject createGenericProject(GenericProjectDto genericProjectDto) {
        final GenericProject genericProject = GenericProjectDto.convertDto(genericProjectDto);
        GenericProjectDtoVisitor visitor = new GenericProjectDtoVisitor() {
            @Override
            public void visit(ProjectDto projectDto) {
                if (projectDto.getTemplateId() != null) {
                    projectManager.addProjectFromTemplate((Project) genericProject,
                        projectDto.getTemplateId(), GenericProjectCopyParameterDto.convertDto(projectDto.getParams()));
                } else {
                    genericProjectManager.persist(genericProject);
                }
            }

            @Override
            public void visit(ProjectTemplateDto projectTemplateDto) {
                if (projectTemplateDto.getProjectId() != null) {
                    projectTemplateManagerService.addTemplateFromProject((ProjectTemplate) genericProject,
                        projectTemplateDto.getProjectId(), GenericProjectCopyParameterDto.convertDto(projectTemplateDto.getParams()));
                } else {
                    genericProjectManager.persist(genericProject);
                }
            }
        };
        genericProjectDto.accept(visitor);
        return genericProject;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#projectId, 'org.squashtest.tm.domain.project.GenericProject', 'MANAGE_PROJECT_CLEARANCE')")
    public RestPartyClearance findAllClearancesByProjectId(long projectId) {
        List<PartyProjectPermissionsBean> beanList = genericProjectManager.findPartyPermissionsBeansByProject(projectId);
        RestPartyClearance clearances = new RestPartyClearance();
        List<Long> partyIds = beanList.stream().map(bean -> bean.getParty().getId()).toList();
        Map<Long, Party> partyMap = restPartyService.findByIds(partyIds).stream().collect(Collectors.toMap(Party::getId, p -> p));
        for (PartyProjectPermissionsBean item : beanList) {
            AclGroup profile = item.getPermissionGroup();
            String profileName = profile.getSimpleName();

            // why do we name advanceTester instead of advancedTester?? Then this is what you got for translation
            if ("advanceTester".equalsIgnoreCase(profileName)) {
                profileName = "advancedTester";
            }

            Party target = partyMap.get(item.getParty().getId());

            RestPartyProfileDto targetList = clearances.getPartyClearances().get(profileName);
            if (targetList == null) {
                long profileId = profile.getId();
                boolean isSystem = profile.isSystem();
                List<Party> parties = new ArrayList<>();
                parties.add(target);
                targetList = new RestPartyProfileDto(profileId, profileName, isSystem, parties);

                clearances.getPartyClearances().put(profileName, targetList);
            } else {
                targetList.getUsers().add(target);
            }
        }
        return clearances;

    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#projectId, 'org.squashtest.tm.domain.project.GenericProject', 'MANAGE_PROJECT_CLEARANCE')")
    public RestPartyPermission findAllPermissionsByProjectId(long projectId) {
        List<PartyProjectPermissionsBean> beanList = genericProjectManager.findPartyPermissionsBeansByProject(projectId);
        RestPartyPermission permissions = new RestPartyPermission();
        for (PartyProjectPermissionsBean item : beanList) {
            String groupName = item.getPermissionGroup().getSimpleName();

            // why do we name advanceTester instead of advancedTester?? Then this is what you got for translation
            if ("advanceTester".equalsIgnoreCase(groupName)) {
                groupName = "advancedTester";
            }

            Party target = restPartyService.findById(item.getParty().getId());

            List<Party> targetList = permissions.getPartyPermissions().get(groupName);
            if (targetList == null) {
                targetList = new ArrayList<>();
                targetList.add(target);
                permissions.getPartyPermissions().put(groupName, targetList);
            } else {
                targetList.add(target);
            }
        }
        return permissions;

    }


    @Override
    public RestPartyClearance buildPartyClearanceDataModel(long profileId, List<Long> partyIds) {
        List<Party> parties =  restPartyService.findByIds(partyIds);

        Optional<AclGroup> optionalProfile = profileDao.findById(profileId);

        if (optionalProfile.isPresent()) {
            AclGroup profile = optionalProfile.get();
            RestPartyClearance clearances = new RestPartyClearance();
            RestPartyProfileDto partyProfile = new RestPartyProfileDto(profileId, profile.getSimpleName(), profile.isSystem(), parties);
            String profileNameKey = SnakeCaseSerializer.toSnakeCase(profile.getSimpleName());
            clearances.getPartyClearances().put(profileNameKey, partyProfile);

            return clearances;
        } else {
            throw new EntityNotFoundException("The profile with id: " + profileId + " does not exist.");
        }
    }


    @Override
    public RestPartyPermission buildPartyPermissionDataModel(String permissionGroup, List<Long> partyIds) {
        List<Party> parties = new ArrayList<>();
        for (Long partyId : partyIds) {
            parties.add(restPartyService.findById(partyId));
        }

        RestPartyPermission permissions = new RestPartyPermission();
        permissions.getPartyPermissions().put(permissionGroup, parties);

        return permissions;
    }



    @Override
    // secured by the native service
    public void addNewPermissionToProject(long userId, long projectId, String permissionGroup) {
        genericProjectManager.addNewPermissionToProject(Collections.singletonList(userId), projectId, NAMESPACE + permissionGroup);
    }

    @Override
    public void addNewPermissionToProject(long userId, long projectId, long profileId) {
        genericProjectManager.addNewPermissionToProject(Collections.singletonList(userId), projectId, profileId);
    }

    @Override
    public List<AclGroup> findAllPossiblePermission() {
        return genericProjectManager.findAllPossiblePermission();
    }

    @Override
    public Page<Requirement> findRequirementsByProject(long projectId, Pageable paging) {
        PermissionsUtils.checkPermission(permissionEvaluationService, Collections.singletonList(projectId),
                Permissions.READ.name(), Project.class.getName());
        return dao.findAllRequirementByProjectId(projectId, paging);
    }

    @Override
    public Page<TestCase> findTestCasesByProject(long projectId, Pageable paging, List<String> fields) {
        PermissionsUtils.checkPermission(permissionEvaluationService, Collections.singletonList(projectId),
            Permissions.READ.name(), Project.class.getName());
        return testCaseLibraryFinderService.findAllTestCaseByProjectId(projectId, paging, fields);
    }

    @Override
    public Page<Campaign> findCampaignsByProject(long projectId, Pageable paging) {
        PermissionsUtils.checkPermission(permissionEvaluationService, Collections.singletonList(projectId),
                Permissions.READ.name(), Project.class.getName());
        return dao.findAllCampaignByProjectId(projectId, paging);
    }

    @Override
    public void deletePartyFromProject(Long partyId, long projectId) {
        genericProjectManager.removeProjectPermission(Collections.singletonList(partyId), projectId);
    }

    @Override
    public Map<Long, String> findNamesByProjectIds(List<Long> projectIds) {
        Map<Long, String> projectNameByIdMap = new HashMap<>();
        List<Object[]> projectNameByIdObjectList = dao.findNamesByProjectIds(projectIds);
        projectNameByIdObjectList.forEach(projectIdentityTab -> {
            if (projectIdentityTab.length > 0) {
                projectNameByIdMap.put((Long) projectIdentityTab[0], (String) projectIdentityTab[1]);
            } else {
                throw new IllegalArgumentException("Project identity table cannot be empty. Check the validation and your rights on the project ids");
            }
        });
        return projectNameByIdMap;
    }

    @Override
    public List<Long> getReadableProjectIdsOnRequirementLibrary(List<Long> projectIds) {
        List<Long> allReadableIds = projectFinder.findReadableProjectIdsOnRequirementLibrary();
        projectIds.retainAll(allReadableIds);
        return projectIds;
    }

    @Override
    public List<Long> getReadableProjectIdsOnTestCaseLibrary(List<Long> projectIds) {
        List<Long> allReadableIds = projectFinder.findReadableProjectIdsOnTestCaseLibrary();
        projectIds.retainAll(allReadableIds);
        return projectIds;
    }

    @Override
    public List<Long> getReadableProjectIdsOnCampaignLibrary(List<Long> projectIds) {
        List<Long> allReadableIds = projectFinder.findReadableProjectIdsOnCampaignLibrary();
        projectIds.retainAll(allReadableIds);
        return projectIds;
    }

    @Override
    public Page<CustomReportLibraryNode> findCustomReportLibraryAllContent(long projectId, Pageable paging) {

        dao.findById(projectId).orElseThrow(() -> ExceptionUtils.entityNotFoundException(Project.class, projectId));

        PermissionsUtils.checkPermission(permissionEvaluationService, Collections.singletonList(projectId),
            Permissions.READ.name(), Project.class.getName());
        return dao.findAllCustomReportLibraryByProjectId(projectId, paging);
    }

    @Override
    public Page<CustomReportLibraryNode> findCustomReportLibraryRootContent(long projectId, Pageable paging) {


        dao.findById(projectId).orElseThrow(() -> ExceptionUtils.entityNotFoundException(Project.class, projectId));

        PermissionsUtils.checkPermission(permissionEvaluationService, Collections.singletonList(projectId),
            Permissions.READ.name(), Project.class.getName());

        Set<NodeReference> nodesToCollect =
            treeBrowserDao.findLibraryReferences(NodeWorkspace.CUSTOM_REPORT, Collections.singleton(projectId));
        ListMultimap<NodeReference, NodeReference> childrenReferences =
            treeBrowserDao.findChildrenReference(nodesToCollect);

        nodesToCollect.addAll(childrenReferences.values());
        List<Long> nodeIds = nodesToCollect.stream()
            .map(NodeReference::getId)
            .toList();

        return dao.findAllCustomReportLibraryByCustomReportLibraryNodeIds(nodeIds, paging);
    }

    @Override
    public <T> void replaceLinksByCustomSelfLinks(EntityModel<T> entityModel, String entityType) {
        entityModel.removeLinks();
        UriComponentsBuilder builder = ServletUriComponentsBuilder.fromCurrentRequestUri();
        String incorrectPath = builder.build().getPath();
        String newPath = invalidEntityTypes.stream()
            .filter(incorrectPath::contains)
            .findFirst()
            .map(option -> incorrectPath.replaceFirst(option + "/[^/]+", "").replaceFirst("/reporting-library/content", ""))
            .orElse(incorrectPath);


        builder.replacePath(newPath);
        builder.path("/" + entityType + "/" + ((CustomReportLibraryNode) entityModel.getContent()).getId());
        entityModel.add(Link.of(builder.toUriString(), LinkRelation.of("self")));
    }

    @Override
    public String toKebabCase(String input) {
        return input.replace('_', '-').toLowerCase();
    }


    // ***************** private **************************

    // see those methods canReadX here ? That how the world looked like before java 8

    private void checkReadOnRequirements(long projectId) {

        GenericProject p = dao.getOne(projectId);
        if (p == null) {
            throw new EntityNotFoundException();
        }

        if (!permService.canRead(p.getRequirementLibrary())) {
            throw new EntityNotFoundException(ACCESS_DENIED_MESSAGE);
        }
    }

    private void checkReadOnTestCases(long projectId) {
        GenericProject p = dao.getOne(projectId);
        if (p == null) {
            throw new EntityNotFoundException();
        }

        if (!permService.canRead(p.getTestCaseLibrary())) {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
    }

    private void checkReadOnCampaigns(long projectId) {
        GenericProject p = dao.getOne(projectId);
        if (p == null) {
            throw new EntityNotFoundException();
        }

        if (!permService.canRead(p.getCampaignLibrary())) {
            throw new AccessDeniedException(ACCESS_DENIED_MESSAGE);
        }
    }

}
