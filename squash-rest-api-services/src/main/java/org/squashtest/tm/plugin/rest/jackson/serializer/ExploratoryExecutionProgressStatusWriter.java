/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.Annotations;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.ExploratoryExecutionEvent;
import org.squashtest.tm.domain.execution.ExploratoryExecutionRunningState;

import java.util.Comparator;
import java.util.List;

public class ExploratoryExecutionProgressStatusWriter extends VirtualBeanPropertyWriter {

    public ExploratoryExecutionProgressStatusWriter() {
    }

    public ExploratoryExecutionProgressStatusWriter(BeanPropertyDefinition propDef, Annotations contextAnnotations, JavaType declaredType) {
        super(propDef, contextAnnotations, declaredType);
    }

    @Override
    protected Object value(Object bean, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws Exception {
        ExploratoryExecution exploratoryExecution = (ExploratoryExecution) bean;
        if (exploratoryExecution.getEvents().isEmpty()) {
            return ExploratoryExecutionRunningState.NEVER_STARTED;
        }

        List<ExploratoryExecutionEvent> events = exploratoryExecution.getEvents().stream()
            .sorted(Comparator.comparing(ExploratoryExecutionEvent::getDate).reversed())
            .toList();

        return switch(events.get(0).getEventType()) {
            case STOP -> ExploratoryExecutionRunningState.STOPPED;
            case PAUSE -> ExploratoryExecutionRunningState.PAUSED;
            case START, RESUME -> ExploratoryExecutionRunningState.RUNNING;
        };
    }

    @Override
    public VirtualBeanPropertyWriter withConfig(MapperConfig<?> mapperConfig, AnnotatedClass annotatedClass,
                                                BeanPropertyDefinition beanPropertyDefinition, JavaType javaType) {
        return new ExploratoryExecutionProgressStatusWriter(beanPropertyDefinition, annotatedClass.getAnnotations(), javaType);
    }
}
