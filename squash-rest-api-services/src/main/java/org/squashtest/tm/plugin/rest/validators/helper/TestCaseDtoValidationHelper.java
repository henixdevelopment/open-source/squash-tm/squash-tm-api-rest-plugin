/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.project.AutomationWorkflowType;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.plugin.rest.jackson.model.ExploratoryTestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.KeywordTestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestType;
import org.squashtest.tm.plugin.rest.jackson.model.ScriptedTestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDtoVisitor;
import org.squashtest.tm.service.infolist.InfoListItemFinderService;
import org.squashtest.tm.service.testautomation.AutomatedTestTechnologyFinderService;
import org.squashtest.tm.service.testcase.TestCaseFinder;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by jthebault on 13/06/2017.
 */
@Component
public class TestCaseDtoValidationHelper extends RestNodeValidationHelper {

    @Inject
    private InfoListItemFinderService infoListItemFinderService;

    @Inject
    private AutomatedTestTechnologyFinderService automatedTestTechnologyFinderService;

    @Inject
    private TestCaseFinder testCaseFinder;

    private final static String NONE_VALUE = "None";

    public void checkInvalidAttributeForEachTestCaseType(Errors errors, TestCaseDto iTestCase) {
        final TestCaseDtoVisitor testCaseDtoVisitor = new InvalidTestCaseAttributesVisitor(errors);
        iTestCase.accept(testCaseDtoVisitor);
    }

    public void assignInfoList(Errors errors, TestCaseDto iTestCase) {
        checkAndAssignType(errors, iTestCase);
        checkAndAssignNature(errors, iTestCase);
    }

    private void checkAndAssignNature(Errors errors, TestCaseDto iTestCase) {
        InfoListItem nature = iTestCase.getNature();
        if (nature != null) {
            String code = nature.getCode();
            Project project = iTestCase.getProject();
            if (project == null) {
                throw new IllegalArgumentException("Programmatic error : You must give a project to a Rest Node to be able to check infolist.");
            }
            if (infoListItemFinderService.isNatureConsistent(project.getId(), code)) {
                iTestCase.setNature(infoListItemFinderService.findByCode(code));
            } else {
                errors.rejectValue("nature", "invalid nature", "Invalid test case nature for this project");
            }
        }
    }

    private void checkAndAssignType(Errors errors, TestCaseDto iTestCase) {
        InfoListItem type = iTestCase.getType();
        if (type != null) {
            String code = type.getCode();
            if (infoListItemFinderService.isTypeConsistent(iTestCase.getProject().getId(), code)) {
                iTestCase.setType(infoListItemFinderService.findByCode(code));
            } else {
                errors.rejectValue("type", "invalid type", "Invalid test case type for this project");
            }
        }
    }

    public void checkAutomationAttributes(Errors errors, TestCaseDto patch) {
        if (patch.getAutomatedTestTechnology() != null) {
            checkAutomatedTestTechnologyAttribute(errors, patch);
        }

        if (patch.getScmRepositoryId() != null) {
            checkScmRepositoryAttribute(errors, patch);
        } else if (patch.getScmRepositoryUrl() != null) {
            errors.rejectValue("scmRepositoryUrl", "invalid attribute", "Scm Repository Url can not be modified directly. Use instead scm_repository_id to modify the associated scm repository.");
        }

        if (patch.getAutomatable() != null) {
            checkHasAutomationWorkflow(errors, patch, "automatable");
        }

        if (patch.getAutomationPriority() != null) {
            checkHasAutomationWorkflow(errors, patch, "automationPriority");
        }

        if (patch.getAutomationStatus() != null) {
            checkAutomationEligibility(errors, patch);
            checkValidAutomationWorkflowForAutomationStatusUpdate(errors, patch);
            checkValidAutomationStatus(errors, patch);
        }
    }

    private void checkAutomationEligibility(Errors errors, TestCaseDto patch) {
        TestCase tc = testCaseFinder.findById(patch.getId());

        boolean isAutomatable = TestCaseAutomatable.Y.equals(tc.getAutomatable());

        if (!isAutomatable) {
            String message = "The selected eligibility does not allow automation of test case";
            errors.rejectValue("automationStatus", "invalid eligibility for automation", message);
        }

    }

    private void checkValidAutomationWorkflowForAutomationStatusUpdate(Errors errors, TestCaseDto patch) {
        AutomationWorkflowType automationWorkflowType = patch.getProject().getAutomationWorkflowType();

        if (AutomationWorkflowType.NONE.equals(automationWorkflowType) ||
            AutomationWorkflowType.REMOTE_WORKFLOW.equals(automationWorkflowType)){
            String message = String.format("The current automation workflow '%s' doesn't allow modification of automation status.",
                automationWorkflowType.name());
            errors.rejectValue("automationStatus", "invalid automation workflow for automation status update", message);
        }
    }

    private void checkValidAutomationStatus(Errors errors, TestCaseDto patch) {
        List<AutomationRequestStatus> validStatuses = List.of(AutomationRequestStatus.AUTOMATED);

        if(!validStatuses.contains(patch.getAutomationStatus())) {
            List<String> validStatusesList = validStatuses.stream().map(Enum::name).collect(Collectors.toList());
            String message = String.format("The automation status '%s' is not handled by the API. Only the following statuses are currently supported: '%s'",
                patch.getAutomationStatus(), String.join(", ", validStatusesList));
            errors.rejectValue("automationStatus", "invalid automation status", message);
        }
    }

    private void checkAutomatedTestTechnologyAttribute(Errors errors, TestCaseDto patch) {
        AutomatedTestTechnology techno = automatedTestTechnologyFinderService.findByNameIgnoreCase(patch.getAutomatedTestTechnology());
        if (techno == null && !NONE_VALUE.equalsIgnoreCase(patch.getAutomatedTestTechnology())) {
            String allAutomatedTestTechnologies =
                    automatedTestTechnologyFinderService.getAllAvailableAutomatedTestTechnology()
                            .stream()
                            .map(AutomatedTestTechnology::getName)
                            .collect(Collectors.joining(", "));
            String message = String.format("No entity known for %s and name %s. Available values are : %s, %s",
                    AutomatedTestTechnology.class,
                    patch.getAutomatedTestTechnology(),
                    NONE_VALUE,
                    allAutomatedTestTechnologies);
            errors.rejectValue("automatedTestTechnology", "invalid automated test technology name", message);
        }
    }

    private void checkScmRepositoryAttribute(Errors errors, TestCaseDto patch) {
        checkEntityExist(errors, RestType.SCM_REPOSITORY, patch.getScmRepositoryId());
    }

    private void checkHasAutomationWorkflow(Errors errors, TestCaseDto patch, String field) {
        if (! patch.getProject().isAllowAutomationWorkflow()) {
            String message = String.format("The project '%s' doesn't have the automation workflow enabled.",
                    patch.getProject().getName());
            errors.rejectValue(field, "invalid automation workflow", message);
            return;
        }

        AutomationWorkflowType automationWorkflowType = patch.getProject().getAutomationWorkflowType();

        if (AutomationWorkflowType.NONE.equals(automationWorkflowType)) {
            String message = String.format(
                    "The current automation workflow '%s' doesn't allow modification.",
                    automationWorkflowType.name(), field);
            errors.rejectValue(field, "invalid automation workflow", message);
        }
    }

    private static final class InvalidTestCaseAttributesVisitor implements TestCaseDtoVisitor {
        private final Errors errors;

        InvalidTestCaseAttributesVisitor(Errors errors) {
            this.errors = errors;
        }

        @Override
        public void visit(TestCaseDto testCaseDto) {
            // NOOP
        }

        @Override
        public void visit(ScriptedTestCaseDto scriptedTestCaseDto) {
            rejectStepsAndParametersAndDatasets(scriptedTestCaseDto, "Scripted");
        }

        @Override
        public void visit(KeywordTestCaseDto keywordTestCaseDto) {
            // NOOP
        }

        @Override
        public void visit(ExploratoryTestCaseDto exploratoryTestCaseDto) {
            rejectStepsAndParametersAndDatasets(exploratoryTestCaseDto, "Exploratory");
        }

        private void rejectStepsAndParametersAndDatasets(TestCaseDto testCaseDto, String kind) {
            if (testCaseDto.getSteps() != null) {
                errors.rejectValue("steps", "invalid steps",
                    String.format("%s test case should not have steps", kind));
            }

            if (testCaseDto.getParameters() != null) {
                errors.rejectValue("parameters", "invalid parameters",
                    String.format("%s test case should not have parameters", kind));
            }

            if (testCaseDto.getDatasets() != null) {
                errors.rejectValue("datasets", "invalid datasets",
                    String.format("%s test case should not have datasets", kind));
            }
        }
    }
}
