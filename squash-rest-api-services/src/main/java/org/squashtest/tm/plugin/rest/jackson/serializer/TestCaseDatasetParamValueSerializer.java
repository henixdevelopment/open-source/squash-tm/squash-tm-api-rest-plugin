/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.squashtest.tm.domain.testcase.DatasetParamValue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestCaseDatasetParamValueSerializer extends StdSerializer<Collection<DatasetParamValue>> {

    public TestCaseDatasetParamValueSerializer() {
        super(TypeFactory.defaultInstance().constructType(Collection.class));
    }

    @Override
    public void serialize(Collection<DatasetParamValue> datasetParamValues, JsonGenerator gen, SerializerProvider provider) throws IOException {
        doSerialize(datasetParamValues, gen);
    }
    private void doSerialize(Collection<DatasetParamValue>  datasetParamValues, JsonGenerator gen) throws IOException {

        Map<String, Object> attributes = new HashMap<>();
        List<DatasetParamValue> listDatasetParamValue = new ArrayList<>(datasetParamValues);

        //trie la liste
        Collections.sort(listDatasetParamValue, ID_COMPARATOR);
        gen.writeStartArray();
        for (DatasetParamValue datasetParamValue : listDatasetParamValue) {
            attributes.put("parameter_id", datasetParamValue.getParameter().getId());
            attributes.put("parameter_name", datasetParamValue.getParameter().getName());
            attributes.put("parameter_value", datasetParamValue.getParamValue());
            attributes.put("parameter_test_case_id", datasetParamValue.getParameter().getTestCase().getId());
            gen.writeObject(attributes);
        }
        gen.writeEndArray();

    }

    public static final Comparator<DatasetParamValue> ID_COMPARATOR = new Comparator<DatasetParamValue>() {


        @Override
        public int compare(DatasetParamValue p, DatasetParamValue q) {
            if(p.getParameter().getId() == q.getParameter().getId())
            {
                return p.getParameter().getName().compareTo(q.getParameter().getName());
            }  else if (p.getParameter().getId()> q.getParameter().getId())  return 1;
            else return -1;
        }

    };
}
