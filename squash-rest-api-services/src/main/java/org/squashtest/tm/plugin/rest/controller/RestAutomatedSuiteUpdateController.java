/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.plugin.rest.jackson.model.AttachmentRestDto;
import org.squashtest.tm.service.internal.display.dto.AttachmentDto;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.service.testautomation.model.Attachment;
import org.squashtest.tm.service.testautomation.model.AutomatedExecutionState;
import org.squashtest.tm.service.testautomation.resultpublisher.ResultPublicationService;
import org.squashtest.tm.service.testautomation.resultpublisher.AutomatedSuitePublisherService;

@UseDefaultRestApiConfiguration
@RestApiController()
public class RestAutomatedSuiteUpdateController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestAutomatedSuiteUpdateController.class);

    @Inject private ResultPublicationService publicationService;

    @Inject
    private AutomatedSuitePublisherService automatedSuitePublisherService;

    /**
     * Changes the status of the automated execution
     *
     * @param suiteId Automated Suite id
     * @param testId ITPI id
     * @param stateChange The wrapper for the ExecutionStatus containing a list of attachments
     */
    @ResponseBody
    @PostMapping(
        value =
            "resultpublisher/{suiteId}/automated-executions/{testId}/test-status")
    public synchronized void updateITPIState(
        @PathVariable String suiteId,
        @PathVariable long testId,
        @RequestBody AutomatedExecutionState stateChange) {
        automatedSuitePublisherService.addItpiLabelAndIdToAttachmentName(testId, stateChange);
        publicationService.publishResult(suiteId, testId, stateChange);
    }

    //Will be removed around 2025-06, when we decide for good to remove allure reports.
    @ResponseBody
    @PostMapping(
        value = "resultpublisher/{suiteId}/automated-executions/allureReport")
    public synchronized ResponseEntity<AttachmentRestDto> deprecatedAttachToAutomatedSuite(
        @PathVariable String suiteId,
        @RequestBody Attachment allureAttachment) {
        AttachmentDto dto = automatedSuitePublisherService.attachReportToAutomatedSuite(suiteId, allureAttachment);
        AttachmentRestDto attachmentRestDto = new AttachmentRestDto(dto.getId(), dto.getName(), dto.getSize(), dto.getAddedOn());
        return ResponseEntity.status(HttpStatus.CREATED).body(attachmentRestDto);
    }

    @ResponseBody
    @PostMapping(
        value = "resultpublisher/{suiteId}/automated-executions/attachments")
    public synchronized ResponseEntity<AttachmentRestDto> attachToAutomatedSuite(
        @PathVariable String suiteId,
        @RequestBody Attachment allureAttachment) {
        AttachmentDto dto = automatedSuitePublisherService.attachReportToAutomatedSuite(suiteId, allureAttachment);
        AttachmentRestDto attachmentRestDto = new AttachmentRestDto(dto.getId(), dto.getName(), dto.getSize(), dto.getAddedOn());
        return ResponseEntity.status(HttpStatus.CREATED).body(attachmentRestDto);
    }

    @ResponseBody
    @PutMapping(
        value = "attachments/{id}")
    public synchronized ResponseEntity<AttachmentRestDto> updateAutomatedSuiteAttachmentContent(
        @PathVariable Long id,
        @RequestBody Attachment allureAttachment) {
        try {
            AttachmentDto dto = automatedSuitePublisherService.updateAttachmentContent(allureAttachment, id);
            AttachmentRestDto attachmentRestDto = new AttachmentRestDto(dto.getId(), dto.getName(), dto.getSize(), dto.getAddedOn());
            return ResponseEntity.status(HttpStatus.OK).body(attachmentRestDto);
        } catch (EntityNotFoundException ex) {
            LOGGER.error(ex.getMessage(), ex);
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(null);
        }
    }

    /**
     * Update automated suite status to running.
     *
     * @param suiteId Automated Suite id
     * @param request HTTP request containing the raw status string
     */
    @ResponseBody
    @PutMapping(value = "resultpublisher/{suiteId}/automated-executions/suite-status", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateAutomatedSuiteStatus(
        @PathVariable String suiteId,
        HttpServletRequest request) throws IOException {
        String status = extractBody(request);
        automatedSuitePublisherService.updateAutomatedSuiteExecStatus(suiteId, status);
    }

    private String extractBody(HttpServletRequest request) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()))) {
            return reader.lines().collect(Collectors.joining("\n"));

        }
    }
}
