/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;

import javax.annotation.concurrent.NotThreadSafe;

/**
 * Filter for serialization, that leverages {@link BaseDynamicFilter}.  It should be registered
 * 	in the {@link FilterProvider} with the identifier {@link SerializationDynamicFilter#FILTER_ID} : it will then apply to the mixins that uses the annotation {@link JsonFilter}, but leave the
 * 	wrapping structures untouched (notably {@link Resource} ).
 */
@NotThreadSafe
public class SerializationDynamicFilter extends SimpleBeanPropertyFilter {


	public static final String FILTER_ID = "squashrest";

	private BaseDynamicFilter filter;
	

	// ************************ constructors **********************
	
	public SerializationDynamicFilter(){
		this("");
	}
	
	
	public SerializationDynamicFilter(String filterExpression){
		super();
		this.filter = new BaseDynamicFilter(FilterExpressionBuilder.from(filterExpression));
	}

	
	/**
	 * Non-public constructor
	 * 
	 * @param objExpression
	 */
	public SerializationDynamicFilter(FilterExpression objExpression){
		super();
		this.filter = new BaseDynamicFilter(objExpression);
	}

	public SerializationDynamicFilter(BaseDynamicFilter filter){
		super();
		this.filter = filter;
	}


	// ******************* API **********************


	public boolean include(String pptName){
		return filter.include(pptName);
	}
	
	/**
	 * Returns a new filter, with the current pointer as the root pointer. The new filter 
	 * will then treat the current object as the new root, which changes then the 
	 * behavior of the "root object and empty filter" rule (see {@link FilterExpressionBuilder} )
	 * 
	 * @return
	 */
	public SerializationDynamicFilter forCurrentBean(){
		return new SerializationDynamicFilter(filter.forCurrentBean());
	}
	
	
	
	// *** internal usage by Jackson ***

	@Override
	public void serializeAsField(Object pojo, JsonGenerator jgen, SerializerProvider provider, PropertyWriter writer)
			throws Exception {
		
		filter.setCurrentProperty( writer.getName() );
		
		/*
		 * Special case : if the property happens to be an "unwrapping" property,  
		 * serialize it with the same filter pointer (is, no advance then reset pointer). 
		 * It is so because in that case the filter for that property is also unwrapped in the 
		 * filter definition.
		 */
		if (isUnwrapping(writer)){
			super.serializeAsField(pojo, jgen, provider, writer);
		}
		
		// if matches the rules 
		else if (filter.include()){
			internalSerialize(pojo, jgen, provider, writer);
		}
		
		// else, nothing to serialize
		
	}

	
	private boolean isUnwrapping(PropertyWriter writer){
		return (
			BeanPropertyWriter.class.isAssignableFrom(writer.getClass()) &&
			((BeanPropertyWriter)writer).isUnwrapping()
		);	
	}
	

	
	
	// *** operations ***

	
	private void internalSerialize(Object pojo, JsonGenerator jgen, SerializerProvider provider, PropertyWriter writer) throws Exception{
		filter.advancePointer();
		super.serializeAsField(pojo, jgen, provider, writer);
		filter.resetPointer();
	}

}
