/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */

import org.squashtest.tm.domain.requirement.ManagementMode

import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirement
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirementVersion
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirementSyncExtender

def reqVer1 = requirementVersion {
    id = 98l
    name = "sample requirement 98-3"
    versionNumber = 3
    reference = "REQ01"

    createdBy "User-1"
    createdOn "2017/07/17"
    lastModifiedBy "User-1"
    lastModifiedOn "2017/07/17"

    criticality "MAJOR"
    category "CAT_FUNCTIONAL"
    description = "<p>Description of the sample requirement.</p>"

    testCases = []
}

def req = requirement {

    id = 624l
    name = "sample requirement"
    mode = ManagementMode.SYNCHRONIZED

    project = project {
        id = 44l
        name = "sample project"
    }
    syncExtender = requirementSyncExtender { }

    resource = reqVer1

    versions = [ reqVer1 ]
}


def cufs = []
/*
 * Now pack the mocks and return
 *
 */
[
        req : req,
        cufs : cufs
]
