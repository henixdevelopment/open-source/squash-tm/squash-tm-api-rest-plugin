/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.resource.Resource;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.ContentInclusion;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.FolderTreeDto;
import org.squashtest.tm.plugin.rest.jackson.model.ParentFolderTreeDto;
import org.squashtest.tm.plugin.rest.jackson.model.ProjectTreeDto;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementFolderDto;
import org.squashtest.tm.plugin.rest.service.RestProjectService;
import org.squashtest.tm.plugin.rest.service.RestRequirementFolderService;
import org.squashtest.tm.plugin.rest.validators.RequirementFolderPatchValidator;
import org.squashtest.tm.plugin.rest.validators.RequirementFolderPostValidator;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestApiController(RequirementFolder.class)
@UseDefaultRestApiConfiguration
public class RestRequirementFolderController extends BaseRestController {

    private static final String REQUIREMENT_FOLDER_TYPE = "requirement-folder";

    @Inject
    private RestRequirementFolderService service;

    @Inject
    private RequirementFolderPostValidator requirementFolderPostValidator;

    @Inject
    private RequirementFolderPatchValidator requirementFolderPatchValidator;

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private RestProjectService projectService;


    @GetMapping("/requirement-folders")
    @ResponseBody
    @DynamicFilterExpression("name")
    public ResponseEntity<PagedModel<EntityModel<RequirementFolder>>> findAllReadableRequirementFolders(Pageable pageable) {

        Page<RequirementFolder> folders = service.findAllReadable(pageable);

        PagedModel<EntityModel<RequirementFolder>> res = toPagedModel(folders);

        return ResponseEntity.ok(res);

    }

    @GetMapping("/requirement-folders/tree/{projectIds}")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<List<ProjectTreeDto>> findRequirementFoldersTreeByProject(@PathVariable("projectIds") List<Long> projectIds) {
        List<Long> alreadyAddedIds = new ArrayList<>();

        List<Long> readableProjectIds = projectService.getReadableProjectIdsOnRequirementLibrary(projectIds);
        List<RequirementFolder> folders = service.findAllRequirementFoldersByPojectIds(readableProjectIds);

        List<ParentFolderTreeDto> tree = getRequirementFolderTree(folders, alreadyAddedIds);
        List<ProjectTreeDto> projTree = buildProjectTreeFromParentFolders(tree, readableProjectIds);

        return ResponseEntity.ok(projTree);
    }


    @GetMapping("/requirement-folders/{id}")
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
    @EntityGetter
    public ResponseEntity<EntityModel<RequirementFolder>> findRequirementFolder(@PathVariable("id") long id) {

        RequirementFolder folder = service.getOne(id);

        EntityModel<RequirementFolder> res = toEntityModel(folder);

        res.add(linkService.createLinkTo(folder.getProject()));
        res.add(createRelationTo("content"));
        res.add(createRelationTo("attachments"));


        return ResponseEntity.ok(res);
    }


    @GetMapping("/requirement-folders/{id}/content")
    @ResponseBody
    @DynamicFilterExpression("name")
    public ResponseEntity<PagedModel<EntityModel<RequirementLibraryNode<Resource>>>> findTestCaseFolderContent(@PathVariable("id") long folderId,
                                                                                                               Pageable pageable,
                                                                                                               ContentInclusion include) {

        Page<RequirementLibraryNode<Resource>> content = null;
        switch (include) {
            case NESTED:
                content = service.findFolderAllContent(folderId, pageable);
                break;
            default:
                content = service.findFolderContent(folderId, pageable);
                break;

        }

        PagedModel<EntityModel<RequirementLibraryNode<Resource>>> res = toPagedResourcesWithRel(content, "content");

        return ResponseEntity.ok(res);
    }


    @PostMapping("/requirement-folders")
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
    public ResponseEntity<EntityModel<RequirementFolder>> createRequirementFolder(@RequestBody RequirementFolderDto folderDto) throws BindException, InvocationTargetException, IllegalAccessException {

        requirementFolderPostValidator.validatePostRequirementFolder(folderDto);

        RequirementFolder folder = service.addRequirementFolder(folderDto);

        EntityModel<RequirementFolder> res = toEntityModel(folder);

        linksHelper.addAllLinksForRequirementFolder(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    @PatchMapping("/requirement-folders/{id}")
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
    public ResponseEntity<EntityModel<RequirementFolder>> patchRequirementFolder(@RequestBody RequirementFolderDto folderPatch, @PathVariable("id") long id) throws BindException {

        folderPatch.setId(id);

        requirementFolderPatchValidator.validatePatchRequirementFolder(folderPatch);
        RequirementFolder folder = service.patchRequirementFolder(folderPatch, id);
        EntityModel<RequirementFolder> res = toEntityModel(folder);
        linksHelper.addAllLinksForRequirementFolder(res);

        return ResponseEntity.ok(res);
    }


    @DeleteMapping("/requirement-folders/{ids}")
    @ResponseBody
    public ResponseEntity<Void> deleteTestCaseFolder(@PathVariable("ids") List<Long> folderIds) {

        service.deleteFolder(folderIds);
        return ResponseEntity.noContent().build();
    }

    private List<ParentFolderTreeDto> getRequirementFolderTree(List<RequirementFolder> folders, List<Long> alreadyAddedIds) {

        List<ParentFolderTreeDto> listDto = new ArrayList<>();

        for (RequirementFolder folder : folders) {

            if (!alreadyAddedIds.contains(folder.getId())) {

                ParentFolderTreeDto dto = new ParentFolderTreeDto();

                dto.setType(REQUIREMENT_FOLDER_TYPE);
                dto.setId(folder.getId());
                dto.setName(folder.getName());
                dto.setUrl(createSelfLink(folder).getHref());
                dto.setChildren(Collections.emptyList());
                dto.setProject(folder.getProject());

                if (folder.hasContent()) {
                    List<RequirementFolder> folderChildren = getAllRequirementFolderFromContent(folder);
                    List<ParentFolderTreeDto> tcftd = getRequirementFolderTree(folderChildren, alreadyAddedIds);
                    dto.setChildren(tcftd);
                }

                alreadyAddedIds.add(folder.getId());
                listDto.add(dto);
            }
        }

        return listDto;
    }

    private List<RequirementFolder> getAllRequirementFolderFromContent(RequirementFolder folder) {
        List<RequirementFolder> reqFolderList = new ArrayList<>();

        for (RequirementLibraryNode rln : folder.getContent()) {
            if (rln instanceof RequirementFolder) {
                reqFolderList.add((RequirementFolder) rln);
            }
        }

        return reqFolderList;
    }

    private List<ProjectTreeDto> buildProjectTreeFromParentFolders(List<ParentFolderTreeDto> parentFolders, List<Long> projectIds) {

        if (!parentFolders.isEmpty()) {
            return appendProjectTreesWithFolderList(parentFolders);
        } else if (!projectIds.isEmpty()){
            return appendProjectTreesWithoutFolderList(projectIds);
        }

        return Collections.emptyList();
    }

    private List<ProjectTreeDto> appendProjectTreesWithFolderList(List<ParentFolderTreeDto> tree) {
        List<ProjectTreeDto> projTree = new ArrayList<>();

        Map<Project, List<FolderTreeDto>> map = tree.stream().collect(Collectors.groupingBy(FolderTreeDto::getProject));

        for (Map.Entry<Project, List<FolderTreeDto>> folderTreesByProjectMap : map.entrySet()) {

            Project project =  folderTreesByProjectMap.getKey();
            List<FolderTreeDto> list =  folderTreesByProjectMap.getValue();
            Collections.sort(list);

            ProjectTreeDto dto = new ProjectTreeDto(project.getId(),project.getName(),list);
            projTree.add(dto);

        }

        Collections.sort(projTree);

        return projTree;
    }

    private List<ProjectTreeDto> appendProjectTreesWithoutFolderList(List<Long> projectIds) {
        List<ProjectTreeDto> projTree = new ArrayList<>();

        Map<Long, String> projectNameByIdMap = projectService.findNamesByProjectIds(projectIds);

        projectNameByIdMap.forEach((projectId, projectName) -> {
            ProjectTreeDto dto = new ProjectTreeDto(projectId,projectName, new ArrayList<>());
            projTree.add(dto);
        });

        Collections.sort(projTree);
        return projTree;
    }

}
