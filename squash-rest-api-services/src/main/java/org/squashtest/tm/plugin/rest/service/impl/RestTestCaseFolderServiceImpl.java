/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.ParentEntity;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseFolderDto;
import org.squashtest.tm.plugin.rest.repository.RestTestCaseFolderRepository;
import org.squashtest.tm.plugin.rest.service.RestTestCaseFolderService;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.library.FolderModificationService;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.testcase.TestCaseLibraryNavigationService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

import java.util.List;

import static org.squashtest.tm.plugin.rest.utils.PaginationUtils.emptyPage;

@Service
@Transactional
public class RestTestCaseFolderServiceImpl implements RestTestCaseFolderService {

	@Inject
	@Qualifier("squashtest.tm.service.TestCaseFolderModificationService")
	private FolderModificationService<TestCaseFolder> nativeService;

	@Inject
	private ProjectFinder projectFinder;

	@Inject
	private RestTestCaseFolderRepository dao;

	@Inject
	private TestCaseLibraryNavigationService testCaseLibraryNavigationService;

	@Inject
	private CustomFieldValueHelper customFieldValueHelper;

	@PersistenceContext
	private EntityManager entityManager;


	@Override
    @Transactional(readOnly=true)
	public TestCaseFolder getOne(Long id) {
		return nativeService.findFolder(id);
	}

	@Override
    @Transactional(readOnly=true)
	public Page<TestCaseFolder> findAllReadable(Pageable pageable) {
		Collection<Long> projectIds = projectFinder.findReadableProjectIdsOnTestCaseLibrary();
        return projectIds.isEmpty() ? emptyPage(pageable) : dao.findAllInProjects(projectIds, pageable);
	}

	@Override
	@Transactional(readOnly=true)
	public List<TestCaseFolder> findAllByProjectIds(List<Long> projectIds) {
		return dao.findAllByProjects(projectIds);
	}

	@Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#folderId,'org.squashtest.tm.domain.testcase.TestCaseFolder' , 'READ')")
	public Page<TestCaseLibraryNode> findFolderContent(long folderId, Pageable pageable) {
		return dao.findFolderContent(folderId, pageable);
	}

	@Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#folderId,'org.squashtest.tm.domain.testcase.TestCaseFolder' , 'READ')")
	public Page<TestCaseLibraryNode> findFolderAllContent(long folderId, Pageable pageable) {
		return dao.findFolderAllContent(folderId, pageable);
	}

	@Override
	public TestCaseFolder addTestCaseFolder(TestCaseFolderDto folderDto) {
		final TestCaseFolder folder = new TestCaseFolder();

		folder.setName(folderDto.getName());
		folder.setDescription(folderDto.getDescription());

		addToParent(folderDto, folder);
		if(folderDto.getCustomFields()!=null) {
			List<CustomFieldValueDto> listCufsDto = folderDto.getCustomFields();
			customFieldValueHelper.patchCustomFieldValue(folder,listCufsDto);
		}
		entityManager.persist(folder);
		entityManager.flush();
		return folder;
	}

	@Override
	@PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.testcase.TestCaseFolder' , 'WRITE')")
	public TestCaseFolder patchTestCaseFolder(TestCaseFolderDto folderPatch, long id) {

		TestCaseFolder folder = this.getOne(id);

		folder.setName(folderPatch.getName());
		folder.setDescription(folderPatch.getDescription());

		if(folderPatch.getCustomFields()!=null) {
			List<CustomFieldValueDto> listCufsDto = folderPatch.getCustomFields();
			customFieldValueHelper.patchCustomFieldValue(folder,listCufsDto);
		}

		return folder;
	}

	@Override
	public void deleteFolder(List<Long>  folderIds) {
		testCaseLibraryNavigationService.deleteNodes(folderIds);
	}

	private void addToParent(TestCaseFolderDto folderDto, TestCaseFolder folder) {

		ParentEntity parent = folderDto.getParent();
		switch (parent.getRestType()) {
			case PROJECT:
				addTestCaseFolderToLibrary(folder, folderDto, parent);
				break;
			case TEST_CASE_FOLDER:
				addTestCaseFolderToFolder(folder, folderDto, parent);
				break;
			default:
				throw new IllegalArgumentException("Programmatic error : Rest type " + parent.getRestType() + "is not a valid parent. You should validate this before.");
		}

	}

	private void addTestCaseFolderToFolder(TestCaseFolder folder, TestCaseFolderDto folderDto, ParentEntity parent) {
		TestCaseFolder testCaseFolder = entityManager.find(TestCaseFolder.class, parent.getId());
		if (testCaseFolder != null) {
			testCaseLibraryNavigationService.addFolderToFolder(testCaseFolder.getId(), folder);

		} else {
			throw new IllegalArgumentException("Programmatic error : test case folder with id " + parent.getId() + "is unknown. You should validate this before.");

		}
	}

	private void addTestCaseFolderToLibrary(TestCaseFolder folder, TestCaseFolderDto folderDto, ParentEntity parent) {
		Project project = entityManager.find(Project.class, parent.getId());
		if (project != null) {
			testCaseLibraryNavigationService.addFolderToLibrary(project.getTestCaseLibrary().getId(), folder);
		} else {
			throw new IllegalArgumentException("Programmatic error : project with id " + parent.getId() + "is unknown. You should validate this before.");
		}
	}

}
