/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.http.HttpHeaders
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.springframework.restdocs.payload.PayloadDocumentation
import org.squashtest.tm.domain.infolist.ListItemReference
import org.squashtest.tm.domain.testcase.Dataset
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.service.RestDatasetService
import org.squashtest.tm.plugin.rest.service.RestTestCaseService
import org.squashtest.tm.plugin.rest.service.RestVerifyingRequirementManagerService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.library.PathService
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufValue
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.dataset
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCase

@WebMvcTest(RestTestCaseController)
public class RestTestCasePostControllerIT extends BaseControllerSpec {

    @Inject
    private RestTestCaseService restTestCaseService;

    @Inject
    private CustomFieldValueFinderService cufService

    @Inject
    private PathService pathService

    @Inject
    private NodeHierarchyHelpService hierService

    @Inject
    private RestVerifyingRequirementManagerService verifyingRequirementManagerService;

    @Inject
    private PermissionEvaluationService permService

    @Inject
    private NodeHierarchyHelpService nodeHierarchyHelpService

    @Inject
    private EntityManagerFactory emf;

    @Inject
    private RestDatasetService restDatasetService;


    def "post-test-case"() {

        given:
        def json = """{
                "_type" : "test-case",
                "name" : "Christmas turkey test flight",
                "parent" : {
                    "_type" : "project",
                    "id" : 15
                },
                "importance": "MEDIUM",
                "status": "UNDER_REVIEW",
                "nature": {
                    "code": "NAT_FUNCTIONAL_TESTING"
                },
                "type": {
                    "code": "TYP_COMPLIANCE_TESTING"
                },
                "prerequisite": "Weather should be cold",
                "description": "Check the ability of the turkey to reach a distant house",
                "custom_fields" : [ {
                      "code" : "wingspan",
                      "value" : "About 100cm"
                    }, {
                      "code" : "limiting factor",
                      "value" : [ "weight", "neck length", "aerodynamics" ]
                    } ],
                "steps": [
                  {
                    "_type": "call-step",
                    "delegate_parameter_values": false,
                    "called_test_case": {
                      "id": 276
                    }
                  }, {
                    "_type": "call-step",
                    "delegate_parameter_values": false,
                    "called_test_case": {
                      "id": 276
                    },
                    "called_dataset": {
                        "id": 33
                    }
                  }, {
                    "_type" : "action-step",
                    "action" : "<p>flap its wings</p>",
                    "expected_result" : "<p>not much, it's lazy</p>",
                    "custom_fields" : [ {
                       "code" : "wingspan",
                      "value" : "About 100cm"
                    }, {
                      "code" : "limiting factor",
                      "value" : [ "weight", "neck length", "aerodynamics" ]
                      } ]
                    }
                   ],
                "datasets" : [ {
                        "name": "Add some weight lifting"
                    }, {
                        "name": "Add some cardio"
                    } ],
                "verified_requirements" : [ {
                        "id": 1664
                    }, {
                        "id": 4635
                    } ]

            }
            """

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }

        def stepDataset = SquashEntityBuilder.dataset {
            id = 33l
            name = "Add some weight lifting"
        }

        def dataset1 = [SquashEntityBuilder.dataset {
            id = 47l
            name = "Add some weight lifting"
        }, SquashEntityBuilder.dataset {
            id = 102l
            name = "Add some cardio"
        }] as Set

        def verifiedRequirements = [
            SquashEntityBuilder.requirementVersion {
                id = 1664L
                name = "First requirement: fly"
                withDefaultRequirement()
            },
            SquashEntityBuilder.requirementVersion {
                id = 4635L
                name = "Second requirement: fly further"
                withDefaultRequirement()
            }]

        def calledTc = testCase {
            id = 276L
            name = "Test the turkey"
            project = proj
        }

        def testSteps = [
            SquashEntityBuilder.callStep {
                id = 15L
                delegateParameterValues = false
                calledTestCase = calledTc
            },
            SquashEntityBuilder.callStep {
                id = 16L
                delegateParameterValues = false
                calledDataset = dataset {
                    id = 33L
                }
                calledTestCase = calledTc
            },
            SquashEntityBuilder.actionStep {
                id = 210L
                action = "<p>ask a turkey to fly</p>"
                expectedResult = "<p>watch the turkey eat seeds</p>"
            }] as List

        def tc = SquashEntityBuilder.testCase {
            id = 240L
            name = "Christmas turkey test flight"
            project = proj
            datasets = dataset1
            requirementVersions = verifiedRequirements
            steps = testSteps
            nature = new ListItemReference("NAT_FUNCTIONAL_TESTING")
            type = new ListItemReference("TYP_COMPLIANCE_TESTING")
            importance "MEDIUM"
            status "UNDER_REVIEW"
            prerequisite = "Weather should be cold"
            description = "Check the ability of the turkey to reach a distant house"
        }

        and:
        EntityManager em = Mock()
        emf.createEntityManager() >> em
        em.find(TestCase.class, 276L) >> calledTc
        em.find(Dataset.class, 33L) >> stepDataset

        permService.canRead(_) >> true
        restTestCaseService.createTestCase(_) >> tc
        cufService.findAllCustomFieldValues(_) >> [
            cufValue {
                label = "Turkey Feature Cuf1"
                inputType "PLAIN_TEXT"
                code = "wingspan"
                value = "About 100cm"
            },
            cufValue {
                label = "Turkey training cuf"
                inputType "TAG"
                code = "limiting factor"
                value = ["weight", "neck length", "aerodynamics"]
            }
        ]
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test flight'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases")
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "test-case"
            "id".is 240
            "name".is "Christmas turkey test flight"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "custom_fields".hasSize 2
            "custom_fields[0]".test {
                "code".is "wingspan"
                "label".is "Turkey Feature Cuf1"
                "value".is "About 100cm"
            }
            "custom_fields[1]".test {
                "code".is "limiting factor"
                "label".is "Turkey training cuf"
                "value".contains "weight", "neck length", "aerodynamics"
            }
            "datasets".hasSize 2
            "datasets".test {
                "[0]".test {
                    "id".is 47
                }
                "[1]".test {
                    "id".is 102
                }
            }
            "verified_requirements[0]".test {
                "_type".is "requirement-version"
                "id".is 1664
                "name".is "First requirement: fly"
            }
            "verified_requirements[1]".test {
                "_type".is "requirement-version"
                "id".is 4635
                "name".is "Second requirement: fly further"
            }
            "steps".hasSize 3
            "steps[1]".test {

                    "_type".is "call-step"
                    "delegate_parameter_values".is false
                    "called_test_case".test {
                        "id".is 276
                    }
                }
            "steps[2]".test {
                "_type".is "action-step"
                "action".is "<p>ask a turkey to fly</p>"
                "expected_result".is "<p>watch the turkey eat seeds</p>"
                }

            "nature".test {
                "code".is "NAT_FUNCTIONAL_TESTING"
            }
            "type".test {
                "code".is "TYP_COMPLIANCE_TESTING"
            }
            "importance".is "MEDIUM"
            "status".is "UNDER_REVIEW"
            "prerequisite".is "Weather should be cold"
            "description".is "Check the ability of the turkey to reach a distant house"
        }

        /*
       * Documentation
       */
        res.andDo(doc.document(
            documentationBuilder {
                requestFields {
                    add "_type (string) : type of the entity (mandatory)"
                    add "name (string) : name of the test case"
                    add "parent (object) : parent of the test case"
                    add "parent._type (string) : type of the parent (test-case-folder or project)"
                    add "parent.id (number) : id of the parent"
                    add "importance (string) : code of the importance"
                    add "status (string) : code of the status"
                    add "nature.code (string) : code of the nature"
                    add "type.code (string) : code of the type of test case"
                    add "prerequisite (string) : prerequisites that should be met before the execution of the test script (HTML)"
                    add "description (string) : description of the test case (HTML)"
                    add "custom_fields (array) : custom fields of that test step (optionnal)"
                    add "custom_fields[].code (string) : code of the custom field, must be associated with the parent"
                    add "custom_fields[].value (varies) : value of the custom field. The value is either a string (for most custom fields), or an array of strings (for multivalued custom fields e.g. a tag list)"
                    add "steps (array) : steps of the test case"
                    add "steps[]._type (string) : type of the step test you want to create (can be call-step or action-step for a standard test case)  "
                    add PayloadDocumentation.fieldWithPath("steps[].delegate_parameter_values").optional().description("whether the parameters of the callee test case should be set by the caller rather than by a dataset of the called (only for call-step, false by default) ")
                    add PayloadDocumentation.fieldWithPath("steps[].called_test_case.id").optional().description("steps of a test case you want to associate with the newly created step test (mandatory for a call-step)")
                    add PayloadDocumentation.fieldWithPath("steps[].called_dataset.id").optional().description("id of the dataset to use for the callee test case (only for a call step, optional)")
                    add PayloadDocumentation.fieldWithPath("steps[].action").optional().description("action to be accomplished, format is HTML (mandatory for action step) ")
                    add PayloadDocumentation.fieldWithPath("steps[].expected_result").optional().description("state or behavior that should be observable when the action has been performed, format is HTML (mandatory for action-step) ")
                    add PayloadDocumentation.fieldWithPath("steps[].custom_fields[].code").optional().description("code of the custom field, must be associated with the parent (optional for action-step) ")
                    add PayloadDocumentation.fieldWithPath("steps[].custom_fields[].value").optional().description("value of the custom field. The value is either a string (for most custom fields) or an array of strings (for multivalued custom fields e.g. a tag list) (optional for action-step) ")
                    add "datasets (array) : dataset to use when the test case is executed (optional)"
                    add "datasets[].name (string) : name of the dataset you want to create "
                    add "verified_requirements (array) : requirements verified by the test case "
                    add "verified_requirements[].id (number) : ids of the verified requirements "
                 }
            }
        ))

    }

    def "post-scripted-test-case"() {

        given:
        def json = """{
                "_type" : "scripted-test-case",
                "name" : "Christmas turkey test flight",
                "parent" : {
                    "_type" : "project",
                    "id" : 15
                },
                "script" : "this is Gherkin script"
            }
            """

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.scriptedTestCase {
            id = 240L
            name = "Christmas turkey test flight"
            project = proj
            script = "this is Gherkin script"
        }

        and:
        restTestCaseService.createTestCase(_) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test flight'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases")
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "scripted-test-case"
            "id".is 240
            "name".is "Christmas turkey test flight"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "script".is "this is Gherkin script"
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */

    }

    def "post-keyword-test-case"() {

        given:
        def json = """{
                "_type" : "keyword-test-case",
                "name" : "Christmas turkey test flight",
                "parent" : {
                    "_type" : "project",
                    "id" : 15
                },
                "steps" : [
                ]
            }
            """

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.keywordTestCase {
            id = 240L
            name = "Christmas turkey test flight"
            project = proj
        }

        and:
        restTestCaseService.createTestCase(_) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test flight'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases")
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "keyword-test-case"
            "id".is 240
            "name".is "Christmas turkey test flight"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "steps".hasSize 0
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */

    }

    def "post-exploratory-test-case"() {

        given:
        def json = """{
                "_type" : "exploratory-test-case",
                "name" : "Christmas Turkey test flight",
                "parent" : {
                    "_type" : "project",
                    "id" : 15
                },
                "charter" : "this is an Exploratory Test Case Charter",
                "session_duration" : 30
            }
            """

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Halloween"
        }
        def tc = SquashEntityBuilder.exploratoryTestCase {
            id = 240L
            name = "Christmas Turkey test flight"
            project = proj
            charter = "this is an Exploratory Test Case Charter"
            sessionDuration = 30
        }

        and:
        restTestCaseService.createTestCase(_) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test flight'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases")
            .accept("application/json")
            .contentType("application/json")
            .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isCreated())
            .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "exploratory-test-case"
            "id".is 240
            "name".is "Christmas Turkey test flight"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "charter".is "this is an Exploratory Test Case Charter"
            "session_duration".is 30
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */

    }

    def "post-test-case-with-automation-attributes"() {

        given:
        def json = """{
                "_type" : "keyword-test-case",
                "name" : "Christmas turkey test flight",
                "parent" : {
                    "_type" : "project",
                    "id" : 15
                },
                "automated_test_technology" : "Robot Framework",
                "scm_repository_id" : 6,
                "automated_test_reference" : ""
            }
            """

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def robotTechnology = SquashEntityBuilder.automatedTestTechnology {
            id = 3L
            name = "Robot Framework"
            actionProviderKey = "robotframework/execute@v1"
        }
        def scmRepo = SquashEntityBuilder.scmRepository {
            id = 6l
            name = "repo01"
            workingBranch = "master"
            scmServer = SquashEntityBuilder.scmServer {
                id = 2l
                url = "https://github.com/test"
            }
        }
        def tc = SquashEntityBuilder.keywordTestCase {
            id = 240L
            name = "Christmas turkey test flight"
            project = proj
            automatedTestTechnology = robotTechnology
            scmRepository = scmRepo
            automatedTestReference = ""
        }

        and:
        restTestCaseService.createTestCase(_) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test flight'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-cases")
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "keyword-test-case"
            "id".is 240
            "name".is "Christmas turkey test flight"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "automated_test_technology".is "Robot Framework"
            "scm_repository_id".is 6
            "scm_repository_url".is "https://github.com/test/repo01 (master)"
            "automated_test_reference".is ""
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */

    }


    //**************************************** PATCH ****************************************
    def "patch-test-case"() {

        given:
        def json = """{
    "_type" : "test-case",
    "name" : "Christmas turkey test launch"
}"""

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.testCase {
            id = 240L
            name = "Christmas turkey test launch"
            project = proj
        }

        and:
        restTestCaseService.patchTestCase(_, _) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test launch'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-cases/{id}", 240L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "test-case"
            "id".is 240
            "name".is "Christmas turkey test launch"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */


    }


    def "patch-scripted-test-case"() {

        given:
        def json = """{
    "_type" : "scripted-test-case",
    "name" : "Christmas turkey test launch",
    "script" : "this is Christmas Eve"
}"""

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.scriptedTestCase {
            id = 240L
            name = "Christmas turkey test launch"
            project = proj
            script = "this is Christmas Eve"
        }

        and:
        restTestCaseService.patchTestCase(_, _) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test launch'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-cases/{id}", 240L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "scripted-test-case"
            "id".is 240
            "name".is "Christmas turkey test launch"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "script".is "this is Christmas Eve"
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */


    }


    def "patch-keyword-test-case"() {

        given:
        def json = """{
    "_type" : "keyword-test-case",
    "name" : "Christmas turkey test launch"
}"""

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.keywordTestCase {
            id = 240L
            name = "Christmas turkey test launch"
            project = proj
        }

        and:
        restTestCaseService.patchTestCase(_, _) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test launch'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-cases/{id}", 240L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "keyword-test-case"
            "id".is 240
            "name".is "Christmas turkey test launch"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */


    }

    def "patch-exploratory-test-case"() {

        given:
        def json = """{
    "_type" : "exploratory-test-case",
    "name" : "Christmas turkey test launch",
    "charter" : "this is Christmas Eve, presents should be wrapped.",
    "session_duration" : 25
}"""

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def tc = SquashEntityBuilder.exploratoryTestCase {
            id = 240L
            name = "Christmas turkey test launch"
            project = proj
            charter = "On Christmas Eve, presents should be placed under the tree."
            sessionDuration = 42
        }

        and:
        restTestCaseService.patchTestCase(_, _) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test launch'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-cases/{id}", 240L)
            .accept("application/json")
            .contentType("application/json")
            .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "exploratory-test-case"
            "id".is 240
            "name".is "Christmas turkey test launch"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "charter".is "On Christmas Eve, presents should be placed under the tree."
            "session_duration".is 42
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */


    }

    def "patch-test-case-with-automation-attributes"() {

        given:
        def json = """{
    "_type" : "scripted-test-case",
    "name" : "Christmas turkey test launch",
    "script" : "this is Christmas Eve",
    "automated_test_technology" : "Cucumber 4",
    "scm_repository_id" : 6,
    "automated_test_reference" : "",
    "automation_status" : "AUTOMATED"
}"""

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "Christmas Eve"
        }
        def cucumberTechnology = SquashEntityBuilder.automatedTestTechnology {
            id = 4L
            name = "Cucumber 4"
            actionProviderKey = "cucumber/execute@v1"
        }
        def scmRepo = SquashEntityBuilder.scmRepository {
            id = 6l
            name = "repo01"
            workingBranch = "master"
            scmServer = SquashEntityBuilder.scmServer {
                id = 2l
                url = "https://github.com/test"
            }
        }
        def tc = SquashEntityBuilder.scriptedTestCase {
            id = 240L
            name = "Christmas turkey test launch"
            project = proj
            script = "this is Christmas Eve"
            automatedTestTechnology = cucumberTechnology
            scmRepository = scmRepo
            automatedTestReference = ""
            automationRequest = SquashEntityBuilder.automationRequest {
                requestStatus = AutomationRequestStatus.AUTOMATED
            }
        }

        and:
        restTestCaseService.patchTestCase(_, _) >> tc
        cufService.findAllCustomFieldValues(_) >> []
        pathService.buildTestCasePath(_) >> '/Christmas Eve/Christmas turkey test launch'
        hierService.findParentFor(_) >> SquashEntityBuilder.tcLibrary {
            project = proj
        }


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-cases/{id}", 240L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_type".is "scripted-test-case"
            "id".is 240
            "name".is "Christmas turkey test launch"
            "parent".test {
                "_type".is "project"
                "id".is 15
            }
            "script".is "this is Christmas Eve"
            "automated_test_technology".is "Cucumber 4"
            "scm_repository_id".is 6
            "scm_repository_url".is "https://github.com/test/repo01 (master)"
            "automated_test_reference".is ""
        }

        /*
         * No documentation, it is essentially the same as get-test-case
         */

    }


    //**************************************** DELETE ****************************************
    def "delete-test-case"() {
        given:
        def reportMsg = ["Les cas de test suivants ne seront pas supprimés : Test-Case3<br/>parce qu'ils sont appelés par les cas de test suivants :\",\n" +
                                 " Test-Case 1, Test-Case 4, Test-Case2<br/>\",\n" +
                                 "Le cas de test :Test-Case3<br/>est référencé dans au moins une itération. Après sa suppression, il ne pourra plus être exécuté.<br/>"
        ]

        and:
        restTestCaseService.deleteTestCase(_, _, _) >> reportMsg
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/test-cases/{ids}?dry-run=true", "2,3", true)
                .accept("application/json")
                .contentType("application/json")
                .header(HttpHeaders.ACCEPT_LANGUAGE, LocaleContextHolder.getLocale().toString())

        )


        then:

        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
        withResult(res) {
        }


        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    requestParams {
                        add "dry-run : indicates if you really want to delete the test case or you just want to do a simulation: if dryRun = true : to do just a delete simulation, if dryRun = false or null:  for delete test case"
                    }
                    pathParams {
                        add "ids : the list of ids of the test case"
                    }

                }
        ))
    }

    def "link-requirements"() {
        given:
        def verifiedRequirementVersions = [
            SquashEntityBuilder.requirementVersion {
                id = 12L
                name = "My first requirement"
                withDefaultRequirement()
            },
            SquashEntityBuilder.requirementVersion {
                id = 13L
                name = "My second requirement"
                withDefaultRequirement()
            },
            SquashEntityBuilder.requirementVersion {
                id = 14L
                name = "My third requirement"
                withDefaultRequirement()
            }
        ]

        def proj = SquashEntityBuilder.project {
            id = 15L
            name = "My project"
        }

        def folder = SquashEntityBuilder.testCaseFolder {
            id = 305L
            name= "My folder"
            project = proj
        }

        def testCase = SquashEntityBuilder.testCase {
            id = 240L
            name = "My test case"
            project = proj
            requirementVersions = verifiedRequirementVersions
        }

        def customFields = [cufValue {
            label = "test_is_automated"
            inputType "CHECKBOX"
            code = "AUTOMATED"
            value = false
        }]

        and:
        restTestCaseService.getOne(240) >> testCase
        1 * verifyingRequirementManagerService.linkRequirementsToTestCase([12, 13, 14], 240)
        nodeHierarchyHelpService.findParentFor(testCase) >> folder
        permService.canRead(_) >> true
        cufService.findAllCustomFieldValues(testCase) >> customFields

        when:
        def res = mockMvc.perform(
                post("/api/rest/latest/test-cases/{id}/coverages/{requirementIds}", 240L, "12,13,14")
                        .accept("application/json"))

        then:
        res.andExpect(status().isOk())
           .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "test-case"
            "id".is 240
            "name".is "My test case"
            "verified_requirements".hasSize 3
            "verified_requirements".test {
                "[0]".test {
                    "_type".is "requirement-version"
                    "id".is 12
                    "name".is "My first requirement"
                }
                "[1]".test {
                    "_type".is "requirement-version"
                    "id".is 13
                    "name".is "My second requirement"
                }
                "[2]".test {
                    "_type".is "requirement-version"
                    "id".is 14
                    "name".is "My third requirement"
                }
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test case"
                        add "requirementIds : the ids of the requirements to link"
                    }
                }
        ))
    }


    def "unlink-requirements"() {
        when:
        def res = mockMvc.perform(
                delete("/api/rest/latest/test-cases/{id}/coverages/{requirementIds}", 543L, "350,351")
                        .accept("application/json")
                        .contentType("application/json"))
        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * verifyingRequirementManagerService.unlinkRequirementsFromTestCase(_, _)

        /*
        * Documentation
        */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test case"
                        add "requirementIds : the ids of the requirements to unlink"
                    }
                }
        ))
    }

}
