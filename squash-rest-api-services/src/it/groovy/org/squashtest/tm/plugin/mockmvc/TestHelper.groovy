/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.mockmvc

import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.springframework.test.web.servlet.ResultActions

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath

//import static org.hamcrest.Matchers.*

public class TestHelper {

	private static ThreadLocal<Context> _localContext = new ThreadLocal<>()

		
	private static class Context{
		private ResultActions resultActions
		private String jsonPath = ""
		
		Context(ResultActions actions, String context){
			this.resultActions = actions
		}
	}
	
	
	
	public static void withResult(ResultActions actions, Closure cls){
		Context ctxt = new Context(actions, "")
		_localContext.set(ctxt)
		
		use(JsonPathCategory){
			cls()
		}
		
		_localContext.set(null)
	
	}
	
	public static void selfRelIs(String selfRel){
				
		JsonPathCategory.is("_links.self.href" , selfRel)
	}

	
	private static Context getContext(){
		return _localContext.get()
	}
	
	
	// **************** wrappers around Hamcrest matchers *****************

	
	public static class JsonPathCategory {
		
		static void test(String sub, Closure cls){
			
			Context ctxt = TestHelper.getContext()
			
			String oldPath = ctxt.jsonPath
			String newPath = append ctxt, sub
			
			ctxt.jsonPath = newPath
			
			cls()
			
			ctxt.jsonPath = oldPath
		}

		
		static void is(String path, Object value){
			testWith(path, Matchers.is(value))
		}
		
		static void hasSize(String path, int value){
			testWith(path, Matchers.hasSize(value))
		}
		
		static void contains(String path, Object... value){
			testWith(path, Matchers.contains(value))
		}
		
		
		static void linksTo(String linkName, String url){
			testWith("${linkName}.href", Matchers.is(url))
		} 
		
		private static String append(Context ctxt, String subpath){
			if (ctxt.jsonPath == ""){
				return subpath
			}
			if (subpath[0]=='['){
				return ctxt.jsonPath+subpath
			}
			return ctxt.jsonPath+"."+subpath
		}
		
		
		private static void testWith(String path, Matcher matcher){
			def ctxt = TestHelper.context
			def actions = ctxt.resultActions
			String finalPath = append ctxt, path
			
			actions.andExpect(jsonPath(finalPath, matcher))
		}
		
	}
	
	
}
