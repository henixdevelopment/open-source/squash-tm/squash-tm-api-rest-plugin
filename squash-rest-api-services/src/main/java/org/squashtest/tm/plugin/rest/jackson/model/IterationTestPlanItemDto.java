/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IterationTestPlanItemDto {

    private Long id;
    private boolean hasSetDataset = false;
    private boolean assignedTo = false;

    @JsonProperty("test_case")
    private TestCaseDto referencedTestCase;

    private Long projectId;

    @JsonProperty("assigned_to")
    private String user ;

    @JsonProperty("dataset")
    private DatasetDto referencedDataset;

    public IterationTestPlanItemDto() {
    }

    public DatasetDto getReferencedDataset() {
        return referencedDataset;
    }

    public void setReferencedDataset(DatasetDto referencedDataset) {

        this.referencedDataset = referencedDataset;
        hasSetDataset = true;
    }

    public boolean isHasSetDataset() {
        return hasSetDataset;
    }

    public boolean isAssignedTo() {
        return assignedTo;
    }

    public TestCaseDto getReferencedTestCase() {
        return referencedTestCase;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {

        this.user = user;
        assignedTo = true;
    }

    public void setReferencedTestCase(TestCaseDto referencedTestCase) {
        this.referencedTestCase = referencedTestCase;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TestCaseDto getTestCaseDto() {
        return referencedTestCase;
    }

    public void setTestCaseDto(TestCaseDto referencedTestCaseDto) {
        this.referencedTestCase = referencedTestCaseDto;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }



}
