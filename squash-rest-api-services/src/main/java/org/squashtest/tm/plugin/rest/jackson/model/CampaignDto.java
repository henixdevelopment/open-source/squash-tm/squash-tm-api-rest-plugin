/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.domain.campaign.CampaignStatus;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.library.Library;
import org.squashtest.tm.domain.milestone.Milestone;
import org.squashtest.tm.domain.project.Project;

import java.util.Date;
import java.util.List;
import java.util.Set;

public class CampaignDto implements RestNode {

    private Long id;

    private Project project;

    private String name;

    private String description;

    private ParentEntity parent;

    private String path;

    @JsonProperty("scheduled_start_date")
    private Date scheduledStartDate;

    @JsonProperty("scheduled_end_date")
    private Date scheduledEndDate;

    @JsonProperty("actual_start_date")
    private Date actualStartDate;

    @JsonProperty("actual_end_date")
    private Date actualEndDate;

    @JsonProperty("actual_start_auto")
    private boolean actualStartAuto;

    @JsonProperty("actual_end_auto")
    private boolean actualEndAuto;

    private List<Iteration> iterations;

    private List<CampaignTestPlanItem> testPlans;

    private Set<Milestone> milestones;

    private String reference;

    private CampaignStatus status;

    @JsonProperty("custom_fields")
    private List<CustomFieldValueDto> customFields;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setParent(ParentEntity parent) {
        this.parent = parent;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Date getScheduledStartDate() {
        return scheduledStartDate;
    }

    public void setScheduledStartDate(Date scheduledStartDate) {
        this.scheduledStartDate = scheduledStartDate;
    }

    public Date getScheduledEndDate() {
        return scheduledEndDate;
    }

    public void setScheduledEndDate(Date scheduledEndDate) {
        this.scheduledEndDate = scheduledEndDate;
    }

    public Date getActualStartDate() {
        return actualStartDate;
    }

    public void setActualStartDate(Date actualStartDate) {
        this.actualStartDate = actualStartDate;
    }

    public Date getActualEndDate() {
        return actualEndDate;
    }

    public void setActualEndDate(Date actualEndDate) {
        this.actualEndDate = actualEndDate;
    }

    public boolean isActualStartAuto() {
        return actualStartAuto;
    }

    public void setActualStartAuto(boolean actualStartAuto) {
        this.actualStartAuto = actualStartAuto;
    }

    public boolean isActualEndAuto() {
        return actualEndAuto;
    }

    public void setActualEndAuto(boolean actualEndAuto) {
        this.actualEndAuto = actualEndAuto;
    }

    public List<Iteration> getIterations() {
        return iterations;
    }

    public void setIterations(List<Iteration> iterations) {
        this.iterations = iterations;
    }

    public List<CampaignTestPlanItem> getTestPlans() {
        return testPlans;
    }

    public void setTestPlans(List<CampaignTestPlanItem> testPlans) {
        this.testPlans = testPlans;
    }

    public Set<Milestone> getMilestones() {
        return milestones;
    }

    public void setMilestones(Set<Milestone> milestones) {
        this.milestones = milestones;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public CampaignStatus getStatus() {
        return status;
    }

    public void setStatus(CampaignStatus status) {
        this.status = status;
    }

    public void setCustomFields(List<CustomFieldValueDto> customFields) {
        this.customFields = customFields;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public ParentEntity getParent() {
        return parent;
    }

    @Override
    public List<CustomFieldValueDto> getCustomFields() {
        return customFields;
    }

    @Override
    public RestType getRestType() {
        return RestType.CAMPAIGN;
    }

    @Override
    public void notifyAssociatedWithProject(Project project) {
        this.project = project;
    }

    @Override
    public Library<?> getLibrary() {
        return getProject().getRequirementLibrary();
    }

}
