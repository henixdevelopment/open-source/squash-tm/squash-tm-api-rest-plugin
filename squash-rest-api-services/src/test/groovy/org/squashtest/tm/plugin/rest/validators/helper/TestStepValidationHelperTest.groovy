/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper

import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.plugin.rest.jackson.model.ActionTestStepDto
import org.squashtest.tm.plugin.rest.jackson.model.KeywordTestStepDto
import org.squashtest.tm.plugin.rest.jackson.model.TestStepDto
import spock.lang.Specification

import javax.persistence.EntityManager

class TestStepValidationHelperTest extends Specification {
    TestStepValidationHelper helper = new TestStepValidationHelper()
    def entityManager = Mock(EntityManager)

    def setup(){
        helper.entityManager = entityManager
    }

    def "should check a null project id in a TestStep dto"(){
        given:
        TestStepDto testStepDto = new KeywordTestStepDto()

        when:
        helper.checkProject(testStepDto.getProjectId())

        then:
        final IllegalArgumentException exception = thrown()
        exception.message == 'Programmatic error, you must provide a not null project id to add an action step'
    }

    def "should check an invalid project id in a TestStep dto"(){
        given:
        TestStepDto testStepDto = new ActionTestStepDto()
        testStepDto.setProjectId(-68L)

        when:
        entityManager.find(Project.class, testStepDto) >> null
        helper.checkProject(testStepDto.getProjectId())

        then:
        final IllegalArgumentException exception = thrown()
        exception.message == 'Programmatic error, you must provide a valid project id to add an action step'
    }
}
