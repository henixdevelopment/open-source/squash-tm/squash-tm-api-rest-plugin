/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.helper;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.validators.helper.CustomFieldValueValidationHelper;
import org.squashtest.tm.service.customfield.CustomFieldBindingFinderService;
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService;
import org.squashtest.tm.service.internal.customfield.PrivateCustomFieldValueService;
import org.squashtest.tm.service.internal.repository.CustomFieldDao;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Component
public class CustomFieldValueHelper {

    @Inject
    private CustomFieldDao customFieldDao;

    @Inject
    private PrivateCustomFieldValueService privateCustomFieldValueService;

    @Inject
    private CustomFieldValueFinderService customFieldValueFinderService;

    @Inject
    private CustomFieldBindingFinderService bindingService;

    private CustomFieldValueHelper() {
    }

    public Map<Long, RawValue> convertCustomFieldDtoToMap(List<CustomFieldValueDto> customFieldValueDtos) {
        if (customFieldValueDtos == null) {
            return new HashMap<>();
        }
        Map<Long, RawValue> customFieldRawValues = new HashMap<>();

        for (CustomFieldValueDto customFieldValueDto : customFieldValueDtos) {
            CustomField customField = customFieldDao.findByCode(customFieldValueDto.getCode());
            customFieldRawValues.put(customField.getId(), customFieldValueDto.getValue());
        }
        return customFieldRawValues;
    }

    public List<CustomFieldValue> patchCustomFieldValue(BoundEntity boundEntity, List<CustomFieldValueDto> cufValuesDto){

        List<CustomFieldValue> cufValues = customFieldValueFinderService.findAllCustomFieldValues(boundEntity);

        Iterator it1 = cufValuesDto.iterator();
        while (it1.hasNext()) {
            CustomFieldValueDto dto = (CustomFieldValueDto) it1.next();
            for (CustomFieldValue value : cufValues) {
                CustomField cf = value.getBinding().getCustomField();
                if (dto.getCode().equals(cf.getCode())) {
                    privateCustomFieldValueService.changeValue(value.getId(), dto.getValue());
                }
            }
        }
        return cufValues;
    }

    public void checkCufs(final Errors errors, BindableEntity bindableEntity, Long projectId, List<CustomFieldValueDto> listCufsDto ){
        List<CustomField> customFields = bindingService.findBoundCustomFields(projectId, bindableEntity);
        CustomFieldValueValidationHelper helper = new CustomFieldValueValidationHelper(bindableEntity, projectId, customFields, listCufsDto, errors);
        helper.validate();
    }



  }
