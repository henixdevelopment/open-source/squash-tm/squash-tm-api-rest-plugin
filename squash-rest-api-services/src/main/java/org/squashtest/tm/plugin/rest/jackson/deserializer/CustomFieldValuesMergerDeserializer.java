/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.deserializer;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.CustomFieldBinding;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.plugin.rest.core.exception.ProgrammingError;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.validators.helper.CustomFieldValueValidationHelper;
import org.squashtest.tm.service.customfield.CustomFieldBindingFinderService;
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.squashtest.tm.plugin.rest.utils.DeserializationUtils.findBoundEntityOrDie;
import static org.squashtest.tm.plugin.rest.utils.DeserializationUtils.getHints;

/**
 * <p></p>That deserializer knows how to merge the custom fields defined in the incoming json with actual custom fields instance. Depending
 * on whether the entity is being created or patched, it will create the custom field</p>
 *
 * <p>
 *     Note : the enclosing object should be either an implementation of a {@link org.squashtest.tm.domain.customfield.BoundEntity},
 *     or a <em>subclass of</em> {@link org.squashtest.tm.plugin.rest.core.jackson.WrappedDTO} that wraps such an entity.
 * </p>
 *
 */
@Deprecated // just cannot work
@Component
public class CustomFieldValuesMergerDeserializer extends JsonDeserializer<List<CustomFieldValue>> {

    @Inject
    private CustomFieldValueFinderService cufService;

    @Inject
    private CustomFieldBindingFinderService bindingService;

    @Override
    public Class<?> handledType() {
        return List.class;
    }

    @Override
    public List<CustomFieldValue> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return deserialize(p, ctxt, new ArrayList<CustomFieldValue>());
    }


    @Override
    public Object deserializeWithType(JsonParser p, DeserializationContext ctxt, TypeDeserializer typeDeserializer) throws IOException {
        return deserialize(p, ctxt, new ArrayList<CustomFieldValue>());
    }


    @Override
    public List<CustomFieldValue> deserialize(JsonParser p, DeserializationContext ctxt, List<CustomFieldValue> intoValue) throws IOException, JsonProcessingException {


        if (!p.isExpectedStartArrayToken()) {
            throw new IOException("malformed json for field 'custom_fields', an array was expected");
        }

        // first, the deserialization job
        List<CustomFieldValueDto> dtos = deserializeDto(p);

        // now get the hints from the context and prepare for validation and updates
        DeserializationHints hints = getHints(ctxt);

        Project project = hints.getProject();
        DeserializationHints.Mode mode = hints.getMode();
        BoundEntity entity = findBoundEntityOrDie(p);

        List<CustomFieldValue> currentValues = null;

        switch (mode) {
            // in that case we need to fetch the custom field value for that instance and merge the
            // incoming value with them
            case DESERIALIZE_UPDATE:
                currentValues = cufService.findAllCustomFieldValues(entity);
                break;
            case DESERIALIZE_CREATE:
                currentValues = createCustomFieldsFor(project.getId(), entity.getBoundEntityType());
                break;

            default:
                throw new ProgrammingError("attempted to deserialize custom fields in a mode that is neither create or update !");
        }

        // validate then update
        validate(hints.getBindingResult(), project, entity, currentValues, dtos);
        updateValues(currentValues, dtos);

        intoValue.clear();
        intoValue.addAll(currentValues);

        return intoValue;
    }


    private List<CustomFieldValueDto> deserializeDto(JsonParser p) {

        try {
            List<CustomFieldValueDto> dtos = new ArrayList<>();

            // pop the START_ARRAY token
            p.nextToken();

            Iterator<CustomFieldValueDto> iter = p.readValuesAs(CustomFieldValueDto.class);
            while (iter.hasNext()) {
                dtos.add(iter.next());
            }
            return dtos;
        } catch (IOException e) {
            // no ProgrammingError thrown here : an error could mean that the stream is corrupt, or an user input error.
            throw new RuntimeException(e);
        }
    }


    private void validate(BindingResult binding, Project project, BoundEntity entity, List<CustomFieldValue> currentValues, List<CustomFieldValueDto> dtos) {
        List<CustomField> customFields = CustomFieldValueValidationHelper.collectCustomFields(currentValues);
        CustomFieldValueValidationHelper helper = new CustomFieldValueValidationHelper(entity.getBoundEntityType(), project.getId(), customFields, dtos, binding);
        helper.validate();
    }

    // update procedure. We must take care of avoiding NPE and such, but validation logic is supposed to be handled already
    private void updateValues(List<CustomFieldValue> currentValues, List<CustomFieldValueDto> dtos) {
        for (CustomFieldValueDto dto : dtos) {
            for (CustomFieldValue cfv : currentValues) {
                String code = dto.getCode();
                RawValue value = dto.getValue();
                CustomFieldValue targetValue = locateActualValue(currentValues, code);
                if (targetValue != null && value != null) {
                    value.setValueFor(targetValue);
                }
            }
        }
    }

    private CustomFieldValue locateActualValue(List<CustomFieldValue> currentValues, String code) {
        for (CustomFieldValue cfv : currentValues) {
            if (cfv.getCustomField().getCode().equals(code)) {
                return cfv;
            }
        }
        return null;
    }


    private List<CustomFieldValue> createCustomFieldsFor(Long projectId, BindableEntity type) {
        List<CustomFieldBinding> bindings = bindingService.findCustomFieldsForProjectAndEntity(projectId, type);
        List<CustomFieldValue> values = new ArrayList<>(bindings.size());

        for (CustomFieldBinding binding : bindings) {
            values.add(binding.createNewValue());
        }

        return values;

    }

}
