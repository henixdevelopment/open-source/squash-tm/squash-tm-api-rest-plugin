/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.BeanDeserializer;
import com.fasterxml.jackson.databind.deser.BeanDeserializerBase;
import com.fasterxml.jackson.databind.deser.BeanDeserializerBuilder;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.deser.SettableBeanProperty;
import com.fasterxml.jackson.databind.deser.impl.BeanPropertyMap;
import com.fasterxml.jackson.databind.deser.impl.ObjectIdReader;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.util.NameTransformer;
import org.springframework.core.annotation.AnnotationUtils;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by benoit on 16/07/17.
 */
public class SquashRestDeserializerModifier extends BeanDeserializerModifier {

    @Override
    public List<BeanPropertyDefinition> updateProperties(DeserializationConfig config, BeanDescription beanDesc, List<BeanPropertyDefinition> propDefs) {
        return super.updateProperties(config, beanDesc, propDefs);
    }

    @Override
    public BeanDeserializerBuilder updateBuilder(DeserializationConfig config, BeanDescription beanDesc, BeanDeserializerBuilder builder) {
        return super.updateBuilder(config, beanDesc, builder);
    }

    @Override
    public JsonDeserializer<?> modifyDeserializer(DeserializationConfig config, BeanDescription beanDesc, JsonDeserializer<?> deserializer) {
        if (canApply(config, beanDesc, deserializer)){
            return new FilterableDeserializer((BeanDeserializer)deserializer, false, null);
        }
        else{
            return deserializer;
        }

    }

    /*
     * The modified deserializer can apply if
     * - the deserializer is indeed a bean deserializer AND
     * - the bean is a dto that declares filtering or ,
     * - the bean uses a mixin that do so
     */
    private boolean canApply(DeserializationConfig config, BeanDescription beanDesc, JsonDeserializer<?> deserializer){

        Class<?> clazz = beanDesc.getBeanClass();
        Class<?> mixin = config.findMixInClassFor(clazz);

        return (deserializer instanceof BeanDeserializer &&
                (hasFilter(mixin) || hasFilter(clazz))
        );

    }

    private boolean hasFilter(Class<?> toInspect){
        if (toInspect == null) return false;
        JsonFilter filter = AnnotationUtils.findAnnotation(toInspect, JsonFilter.class);
        return (filter != null && SerializationDynamicFilter.FILTER_ID.equals(filter.value()));
    }



    /*
        The most interesting documentation will be found on method #deserializeFromObject
     */
    private static final class FilterableDeserializer extends BeanDeserializer {

        private boolean isUnwrapping = false;
        private String ctxtPropertyName = null;


        //********************* The battalion of constructors ***************

        public FilterableDeserializer(BeanDeserializerBase src, boolean isUnwrapping, String ctxtPropertyName) {
            super(src);
            this.isUnwrapping = isUnwrapping;
            this.ctxtPropertyName = ctxtPropertyName;
        }

        public FilterableDeserializer(BeanDeserializerBase src, NameTransformer unwrapper, boolean isUnwrapping, String ctxtPropertyName) {
            super(src, unwrapper);
            this.isUnwrapping = isUnwrapping;
            this.ctxtPropertyName = ctxtPropertyName;
        }

        public FilterableDeserializer(BeanDeserializerBase src, ObjectIdReader oir, boolean isUnwrapping, String ctxtPropertyName) {
            super(src, oir);
            this.isUnwrapping = isUnwrapping;
            this.ctxtPropertyName = ctxtPropertyName;
        }

        public FilterableDeserializer(BeanDeserializerBase src, Set<String> ignorableProps, boolean isUnwrapping, String ctxtPropertyName) {
            super(src, ignorableProps);
            this.isUnwrapping = isUnwrapping;
            this.ctxtPropertyName = ctxtPropertyName;
        }

        public FilterableDeserializer(BeanDeserializerBase src, BeanPropertyMap props, boolean isUnwrapping, String ctxtPropertyName) {
            super(src, props);
            this.isUnwrapping = isUnwrapping;
            this.ctxtPropertyName = ctxtPropertyName;
        }

        // ***************** the battalion of factory methods ***************************

        @Override
        public FilterableDeserializer withObjectIdReader(ObjectIdReader oir) {
            return new FilterableDeserializer(this,oir, isUnwrapping, ctxtPropertyName);
        }

        @Override
        public FilterableDeserializer withIgnorableProperties(Set<String> ignorableProps) {
            return new FilterableDeserializer(this,ignorableProps, isUnwrapping, ctxtPropertyName);
        }

        @Override
        public FilterableDeserializer withBeanProperties(BeanPropertyMap props) {
            return new FilterableDeserializer(this,props, isUnwrapping, ctxtPropertyName);
        }

        /* used in case the deserialized entity is deserialized as a member of a parent object.
         * sometimes it is invoked even when there is no parent context (no context is a context after all)
         * in which case we return this.
         *
         */
        @Override
        public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property) throws JsonMappingException {
            if (property == null){
                return this;
            }
            else {
                // see note on #isUnwrappingProperty
                boolean isUnwrapping = isUnwrappingProperty(property);
                return new FilterableDeserializer(this, isUnwrapping, property.getName());
            }
        }

        // used in case the deserialized entity is embedded as a wrapped object - in our case,
        // most commonly with a WrappedDTO or such
        // Also, see note on #isUnwrappingProperty
        @Override
        public JsonDeserializer<Object> unwrappingDeserializer(NameTransformer unwrapper) {
            return new FilterableDeserializer(this, unwrapper, true, ctxtPropertyName);
        }

        // ******************** internal logic *******************************



        /*
         * This method is called in 'createContextual'. I had to double-check for whether the property is unwrapping or not
         * here despite the existence of "unwrappingDeserilizer" : Jackson indeed invoke both methods when required
         * then forget about it anyway and recreate one without reinvoking "unwrappingDeserializer" on it, which lead to
         * incomplete initialization.
         *
         * This solution is not really satisfying but we don't have much options here
         */
        private boolean isUnwrappingProperty(BeanProperty property){
            JsonUnwrapped unwrapped = property.getAnnotation(JsonUnwrapped.class);
            return (unwrapped != null && unwrapped.enabled());
        }

        private boolean isContextual() {
            return (ctxtPropertyName != null);
        }



        /*
         * Because there is no mechanism similar to FilterProvider and PropertyFilter for deserialization, we have
         * to create a custom deserializer every time a bean is being deserialized, because we need to recreate it
         * minus the properties that aren't allowed by the filter.
         *
         * Here is how it works :
         * - we read the filter out of the hints we stashed in the DeserializationContext
         * - we create a new deserializer with a reduced property map, as specified by the filter.
         * - we invoke the newly created deserializer.
         *
         */
        @Override
        public Object deserializeFromObject(JsonParser p, DeserializationContext ctxt) throws IOException {

            DeserializationDynamicFilter filter = getFilter(ctxt);

            // should we move the filter onward ? we do so if this is not a root bean (ie not a contextual bean)
            // and we are not processing an unwrapped bean
            boolean mustAdvanceFilter = (isContextual() && (!isUnwrapping));

            if (mustAdvanceFilter){
                filter.setCurrentProperty(ctxtPropertyName);
                filter.advance();
            }

            // create the deserializer and invoke it
            BeanDeserializer configuredDeserializer = filteredDeserializer(ctxt);
            Object res = configuredDeserializer.deserializeFromObject(p, ctxt);

            // if the filter was advanced, put it back in position
            if (mustAdvanceFilter){
                filter.reset();
            }

            return res;

        }


        private BeanDeserializer filteredDeserializer(DeserializationContext ctxt){
            DeserializationDynamicFilter filter = getFilter(ctxt);
            Set<String> blacklist = new HashSet<>();

            Iterator<SettableBeanProperty> props = properties();

            while (props.hasNext()){
                SettableBeanProperty prop = props.next();
                String name = prop.getName();
                if (! filter.include(prop.getName())){
                    blacklist.add(name);
                }
            }

            BeanPropertyMap blacklisted = _beanProperties.withoutProperties(blacklist);

            BeanDeserializer deserializer = new BeanDeserializer(this, blacklist);

            return deserializer;
        }

        private DeserializationDynamicFilter getFilter(DeserializationContext ctxt){
            DeserializationHints hints = (DeserializationHints)ctxt.getAttribute(DeserializationHints.HINTS_KEY);
            return hints.getFilter();
        }
    }




}
