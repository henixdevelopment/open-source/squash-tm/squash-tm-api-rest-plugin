/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementVersionDto;
import org.squashtest.tm.plugin.rest.service.RestIssueService;
import org.squashtest.tm.plugin.rest.service.RestRequirementVersionService;
import org.squashtest.tm.plugin.rest.service.RestVerifyingTestCaseManagerService;
import org.squashtest.tm.plugin.rest.validators.RequirementVersionValidator;
import org.squashtest.tm.service.requirement.RequirementVersionManagerService;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestApiController(RequirementVersion.class)
@UseDefaultRestApiConfiguration
public class RestRequirementVersionController extends BaseRestController{

	@Inject
	private RestRequirementVersionService service;

	@Inject
	private RequirementVersionValidator requirementVersionValidator;

	@Inject
	private RestVerifyingTestCaseManagerService verifyingTestCaseManager;

    @Inject
    private RestIssueService restIssueService;
    @Inject
    private RequirementVersionManagerService requirementVersionManagerService;

	@ResponseBody
	@GetMapping("/requirement-versions/{id}")
	@DynamicFilterExpression("*, requirement[name], verifying_test_cases[name]")
	@EntityGetter
	public ResponseEntity<EntityModel<RequirementVersion>> findRequirementVersion(@PathVariable("id") long id){

		RequirementVersion reqVersion = service.findRequirementVersion(id);

		EntityModel<RequirementVersion> res = toEntityModel(reqVersion);

		res.add(linkService.createLinkTo(reqVersion.getProject()));
		res.add(linkService.createLinkTo(reqVersion.getRequirement()));
		res.add(linkService.createRelationTo(reqVersion, "attachments"));
        res.add(createRelationTo("issues"));

        return ResponseEntity.ok(res);
	}

	@ResponseBody
	@PostMapping("/requirement-versions/{reqId}")
	@DynamicFilterExpression("*, requirement[name], verifying_test_cases[name]")
	public ResponseEntity<EntityModel<RequirementVersion>> createRequirementVersion(@PathVariable("reqId") long id,
																					@RequestParam(value = "req_link", required=false, defaultValue = "false") boolean inheritReqLinks,
																					@RequestParam(value = "tc_req_link", required=false, defaultValue = "false") boolean inheritTestcasesReqLinks) throws BindException {

		requirementVersionValidator.validatePostRequirementVersion(id);
		RequirementVersion newlyCreatedRequirementVersion = service.createRequirementVersion(id,inheritReqLinks,inheritTestcasesReqLinks);
		return findRequirementVersion(newlyCreatedRequirementVersion.getId());

	}

	@ResponseBody
    @PatchMapping("/requirement-versions/{reqVersionId}")
	@DynamicFilterExpression("*, requirement[name], verifying_test_cases[name]")
	public ResponseEntity<EntityModel<RequirementVersion>> modifyRequirementVersion(@PathVariable("reqVersionId") long id,
																					@RequestBody RequirementVersionDto requirementVersionDto) throws BindException {
		RequirementVersion reqVersion = service.findRequirementVersion(id);
		requirementVersionValidator.validatePatchRequirementVersion(requirementVersionDto, reqVersion.getRequirement().getId());
		RequirementVersion newlyCreatedRequirementVersion = service.modifyRequirementVersion(requirementVersionDto,id);
		return findRequirementVersion(newlyCreatedRequirementVersion.getId());
	}

	@ResponseBody
	@PostMapping("/requirement-versions/{id}/coverages/{testCaseIds}")
	@DynamicFilterExpression("*, requirement[name], verifying_test_cases[name]")
	public ResponseEntity<EntityModel<RequirementVersion>> associateTestCases(
			@PathVariable("id") Long requirementVersionId, @PathVariable("testCaseIds") List<Long> testCaseIds)
			throws BindException {
		requirementVersionValidator.validateAssociateTestCases(requirementVersionId,testCaseIds);
		verifyingTestCaseManager.addVerifyingTestCasesToRequirementVersion(testCaseIds, requirementVersionId);
		return findRequirementVersion(requirementVersionId);
	}

	@DeleteMapping(value = "/requirement-versions/{id}/coverages/{testCaseIds}")
	public ResponseEntity<Void> removeVerifyingTestCasesFromRequirement(@PathVariable("id") Long requirementVersionId, @PathVariable("testCaseIds") List<Long> testCaseIds) throws BindException {

		requirementVersionValidator.validateDisassociateTestCases(requirementVersionId,testCaseIds);
		verifyingTestCaseManager.removeVerifyingTestCasesToRequirementVersion(testCaseIds, requirementVersionId);
		return ResponseEntity.noContent().build();
	}

    @GetMapping("/requirement-versions/{id}/issues")
    @ResponseBody
    @DynamicFilterExpression("executions[id], requirements[id]")
    public ResponseEntity<PagedModel<EntityModel<IssueDto>>> findRequirementVersionIssues(@PathVariable("id") long id, Pageable pageable) {


        List<Long> executionIds = service.getExecutionIdsByRequirementVersion(id);

        Page<IssueDto> pagedIssue = restIssueService.getIssuesFromExecutionIds(executionIds, pageable);

        for (IssueDto issue : pagedIssue.getContent()) {
            List<RequirementVersion> allValues;
            Set<Execution> executions = issue.getExecutions();
            if (executions != null) {
                allValues = issue.getExecutions().stream()
                    .map(execution -> service.getRequirementVersionFromIssue(issue.getRemoteIssueId(), execution.getId()))
                    .flatMap(List::stream)
                    .distinct()
                    .toList();

                Set<Long> encounteredIds = new HashSet<>();
                List<RequirementVersion> filteredList = allValues.stream()
                    .filter(reqVer -> encounteredIds.add(reqVer.getId()))
                    .toList();

                issue.setRequirements(new HashSet<>(filteredList));
            }
        }

        PagedModel<EntityModel<IssueDto>> res = pageAssembler.toModel(pagedIssue);

        return ResponseEntity.ok(res);
    }

}
