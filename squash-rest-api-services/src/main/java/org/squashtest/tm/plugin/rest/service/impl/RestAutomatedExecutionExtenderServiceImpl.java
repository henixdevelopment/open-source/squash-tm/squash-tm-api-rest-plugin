/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.plugin.rest.repository.RestAutomatedExecutionExtenderRepository;
import org.squashtest.tm.plugin.rest.service.RestAutomatedExecutionExtenderService;

import javax.inject.Inject;

@Service
@Transactional
public class RestAutomatedExecutionExtenderServiceImpl implements RestAutomatedExecutionExtenderService {

    @Inject
    private RestAutomatedExecutionExtenderRepository dao;

    @Override
    public AutomatedExecutionExtender getOne(long id) {
        return dao.getReferenceById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Page<AutomatedExecutionExtender> findAutomatedExecutionExtenderByAutomatedSuiteId(String suiteId, Pageable pageable) {
        Page<AutomatedExecutionExtender> extenders = dao.findAllByAutomatedSuite_Id(suiteId, pageable);
        return extenders;
    }
}
