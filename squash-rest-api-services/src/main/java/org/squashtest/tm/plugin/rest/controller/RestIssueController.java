/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.csp.core.bugtracker.core.BugTrackerNoCredentialsException;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchRequest;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchTerm;
import org.squashtest.tm.bugtracker.advanceddomain.exception.InvalidRemoteIssueSearchRequestException;
import org.squashtest.tm.domain.bugtracker.Issue;
import org.squashtest.tm.exception.IssueAlreadyBoundException;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.service.RestIssueService;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(Issue.class)
@Validated
@UseDefaultRestApiConfiguration
public class RestIssueController extends BaseRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestIssueController.class);

    @Inject
    private RestIssueService  restIssueService;

    @GetMapping(value = "/issues/{id}")
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<EntityModel<Issue>> findCampaign(@PathVariable("id") long id) {
        throw new UnsupportedOperationException("This service is not available yet.");
    }

    @PostMapping(value ="/executions/{executionId}/issues")
    @ResponseBody
    @DynamicFilterExpression("*")
    public  ResponseEntity<EntityModel<Issue>> attachIssueToExecution(@PathVariable("executionId") long executionId, @RequestBody IssueDto issueDto) throws UnsupportedOperationException  {
        Issue issue = restIssueService.attachIssue(issueDto,executionId);
        EntityModel<Issue> res = toEntityModel(issue);
        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    @PostMapping(value ="/executions/{executionId}/attach-issue")
    @ResponseBody
    @DynamicFilterExpression
    public ResponseEntity<Object> searchAndAttachIssueToExecution(
            @PathVariable("executionId") long executionId,
            @Valid @RequestBody FindIssueRequestBody request) {
        try {
            Optional<Issue> issue = restIssueService.searchAndAttachIssueToExecution(request.toSearchRequest(), executionId);

            if (issue.isEmpty()) {
                return buildDetailedErrorResponse(HttpStatus.NOT_FOUND, "The item could not be found");
            } else {
                final EntityModel<Issue> res = toEntityModel(issue.get());
                return ResponseEntity.status(HttpStatus.CREATED).body(res);
            }
        } catch(InvalidRemoteIssueSearchRequestException | IssueAlreadyBoundException e) {
            LOGGER.error("Cannot link issue", e);
            return buildDetailedErrorResponse(HttpStatus.BAD_REQUEST, e);
        } catch (BugTrackerNoCredentialsException e) {
            LOGGER.error("No credentials found to link issue", e);
            return buildDetailedErrorResponse(HttpStatus.UNAUTHORIZED, e);
        }
    }

    public record FindIssueRequestBody (Map<String, String> values) {
        public RemoteIssueSearchRequest toSearchRequest() {
            List<RemoteIssueSearchTerm> searchTerms =
                values.entrySet().stream()
                    .map(entry -> new RemoteIssueSearchTerm(entry.getKey(), entry.getValue()))
                    .toList();

            return new RemoteIssueSearchRequest(searchTerms);
        }
    }
}
























