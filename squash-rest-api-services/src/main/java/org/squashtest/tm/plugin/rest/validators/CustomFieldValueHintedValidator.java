/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;
import org.squashtest.tm.plugin.rest.core.validation.HintedValidator;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.WrappedDtoWithCustomFields;
import org.squashtest.tm.plugin.rest.validators.helper.CustomFieldValueValidationHelper;
import org.squashtest.tm.service.customfield.CustomFieldBindingFinderService;
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService;

import javax.inject.Inject;
import java.util.List;

/**
 * Validates the custom fields for instances of {@link WrappedDtoWithCustomFields}
 *
 *
 */
@Component
public class CustomFieldValueHintedValidator implements HintedValidator {

    @Inject
    private CustomFieldValueFinderService cufService;

    @Inject
    private CustomFieldBindingFinderService bindingService;

    @Override
    public boolean supports(Class<?> clazz) {
        return WrappedDtoWithCustomFields.class.isAssignableFrom(clazz);

    }


    @Override
    public void validate(Object target, Errors errors) {
        // NO-OP
    }

    @Override
    public void validateWithHints(Object target, Errors errors, DeserializationHints hints) {

        WrappedDtoWithCustomFields wrapped = (WrappedDtoWithCustomFields) target;

        Project project = hints.getProject();

        BoundEntity entity = wrapped.getWrapped();
        List<CustomFieldValueDto> dtos = wrapped.getCustomFields();

        List<CustomField> customFields = bindingService.findBoundCustomFields(project.getId(), entity.getBoundEntityType());

        CustomFieldValueValidationHelper helper = new CustomFieldValueValidationHelper(entity.getBoundEntityType(), project.getId(), customFields, dtos, errors);

        helper.validate();


    }


}
