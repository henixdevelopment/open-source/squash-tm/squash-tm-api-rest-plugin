/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.plugin.rest.jackson.model.RemoteReqInfoRecord;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementDto;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementVersionDto;

import java.util.List;

public interface RestRequirementVersionService {

    /**
     * Returns a requirement, given its id
     *
     * @param requirementId
     * @return
     */
    Requirement findRequirement(long requirementId);

    /**
     * Returns a paged list of all the requirements, as long as the requestor can read them
     *
     * @param paging
     * @return
     */
    Page<Requirement> findAllReadable(Pageable paging);

    /**
     * Returns the paged children of a Requirement, given its id
     *
     * @param requirementId
     * @param paging
     * @return
     */
    Page<Requirement> findRequirementChildren(long requirementId, Pageable paging);

    /**
     * Returns the paged children of a requirement, including every descendant Requirement
     *
     * @param requirementId
     * @param paging
     * @return
     */
    Page<Requirement> findRequirementAllChildren(long requirementId, Pageable paging);

    /**
     * Returns a RequirementVersion, given its id
     *
     * @param versionId
     * @return
     */
    RequirementVersion findRequirementVersion(long versionId);

    Requirement createRequirement(RequirementDto requirementDto);

    void deleteRequirements(List<Long> reqIds);

    List<Requirement> findSynchronizedRequirementsBy(String remoteKey, String serverName);

    List<Long> findReqIdsByVersionIds(List<Long> versionIds);

    Requirement  modifyRequirement(RequirementDto requirementDto, Long requirementId);

    Long findCurrentVersionIdByRequirementId(Long requirementId);

    RequirementVersion createRequirementVersion(long requirementId, boolean inheritReqLinks, boolean inheritTestcasesReqLinks);

    RequirementVersion  modifyRequirementVersion(RequirementVersionDto requirementVersionDto, Long requirementId);

    List<RequirementVersion> getRequirementVersionFromIssue(String remoteIssueId, Long executionId);

    List<Long> getExecutionIdsByRequirementVersion(Long versionId);

    RemoteReqInfoRecord getRemoteReqInfoByRequirementId(Long id);
}
