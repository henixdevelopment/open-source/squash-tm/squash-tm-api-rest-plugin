/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.springframework.restdocs.payload.PayloadDocumentation
import org.squashtest.tm.domain.customfield.InputType
import org.squashtest.tm.domain.execution.ExecutionStatus
import org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationDynamicFilter
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints
import org.squashtest.tm.plugin.rest.core.utils.DeserializationConfigHelper
import org.squashtest.tm.plugin.rest.service.RestExecutionService
import org.squashtest.tm.plugin.rest.validators.CustomFieldValueHintedValidator
import org.squashtest.tm.plugin.rest.validators.DenormalizedFieldValueHintedValidator
import org.squashtest.tm.plugin.rest.validators.ExecutionHintedValidator
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.denormalizedfield.DenormalizedFieldValueManager
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.campaign
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.iteration
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.itpi
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project

/**
 * Created by bsiri on 10/07/2017.
 */
@WebMvcTest(RestExecutionController)
class RestExecutionPostControllerIT extends BaseControllerSpec {

    @Inject
    private RestExecutionService restExecutionService

    @Inject
    private DeserializationConfigHelper confHelper

    @Inject
    private CustomFieldValueFinderService cufService

    @Inject
    private DenormalizedFieldValueManager denoService

    @Inject
    private CustomFieldValueHintedValidator validator

    @Inject
    private DenormalizedFieldValueHintedValidator denoValidator

    @Inject
    private ExecutionHintedValidator execValidator

    def setup() {
        validator.supports(_) >> true
        denoValidator.supports(_) >> true
        execValidator.supports(_) >> true
    }

    def "patch-execution"() {

        given:
        def json = """{
    "_type" : "execution",
    "custom_fields" : [
        {
            "code" : "TXT_STATUS",
            "value" : "allright"
        },
        {
            "code" : "TAGS_RELATED",
            "value" : ["see this","also that"]
        }
    ],
    "execution_status" : "RUNNING",
    "comment" : "<p>the comment was modified...</p>",
    "prerequisite" : "<p>impossible modification of the prerequisite</p>"
}"""


        and:
        def exec = SquashEntityBuilder.execution {
            id = 83L
            name = "execution"
            executionStatus "blocked"
            description = "<p>no comment</p>"
            prerequisite = "<p>... but the prerequisite was not</p>"

            testPlan = itpi {
                iteration = iteration {
                    campaign = campaign {
                        project = project {
                            id = 14L
                            name = "project"
                        }
                    }
                }
            }
        }

        confHelper.createHints(_, _) >> new DeserializationHints(
                targetEntity: exec,
                mode: DeserializationHints.Mode.DESERIALIZE_UPDATE,
                filter: new DeserializationDynamicFilter(RestExecutionController.PATCH_DYNAMIC_FILTER),
                project: SquashEntityBuilder.project { id = 14L }
        )

        cufService.findAllCustomFieldValues(exec) >> [
                SquashEntityBuilder.cufValue {
                    name = "text"
                    label = "text"
                    bound "EXECUTION:83"
                    inputType InputType.PLAIN_TEXT
                    code = "TXT_STATUS"
                    value = "allright"
                },
                SquashEntityBuilder.cufValue {
                    name = "see also"
                    label = "see also"
                    inputType InputType.TAG
                    code = "TAGS_RELATED"
                    bound "EXECUTION:83"
                    value = ["see this", "also that"]
                }
        ]

        denoService.findAllForEntity(exec) >> [
                SquashEntityBuilder.denoValue {
                    bound = "EXECUTION:83"
                    boundOriginal = "TEST_CASE:20"
                    code = "TC_TEXT"
                    label = "test case cuf"
                    inputType "PLAIN_TEXT"
                    value = "I'm from the test case"
                },
                SquashEntityBuilder.denoValue {
                    bound = "EXECUTION:83"
                    boundOriginal = "TEST_CASE:20"
                    code = "TC_LABELS"
                    label = "labels"
                    inputType "TAG"
                    value = ["was", "not", "updated"]
                }
        ]


        when:


        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/executions/{id}?fields=execution_status,comment,prerequisite, custom_fields, test_case_custom_fields", 83L)
                .content(json)
                .contentType("application/json")
                .accept("application/json"))

        then:

        // check the deserialized content that is given to the service
        1 * restExecutionService.updateExecution({ args ->
            def ex = args.wrapped
            def cufs = args.customFields
            def deno = args.denormalizedFields

            return (
                    ex.id == 83L &&
                            ex.name == "execution" &&
                            ex.executionStatus == ExecutionStatus.RUNNING &&
                            ex.description == "<p>the comment was modified...</p>" &&
                            ex.prerequisite == "<p>... but the prerequisite was not</p>" &&
                            cufs.size() == 2 &&
                            cufs[0].code == "TXT_STATUS" &&
                            cufs[0].value.value == "allright" &&
                            cufs[1].code == "TAGS_RELATED" &&
                            cufs[1].value.values == ["see this", "also that"] &&
                            deno.size() == 1 &&
                            deno[0].code == "TC_TEXT" &&
                            deno[0].value.value == "I'm from the test case"
            )
        })

        /*
         * Test the response
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "execution"
            "id".is 83
            "execution_status".is "RUNNING"
            "comment".is "<p>the comment was modified...</p>"
            "prerequisite".is "<p>... but the prerequisite was not</p>"
            "custom_fields".test {
                "[0]".test {
                    "code".is "TXT_STATUS"
                    "label".is "text"
                    "value".is "allright"
                }
                "[1]".test {
                    "code".is "TAGS_RELATED"
                    "label".is "see also"
                    "value".contains "see this", "also that"
                }
            }
            "test_case_custom_fields".test {
                "[0]".test {
                    "code".is "TC_TEXT"
                    "label".is "test case cuf"
                    "value".is "I'm from the test case"
                }
                "[1]".test {
                    "code".is "TC_LABELS"
                    "label".is "labels"
                    "value".contains "was", "not", "updated"
                }
            }
            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/executions/83"
                "project".linksTo "http://localhost:8080/api/rest/latest/projects/14"
                "execution-steps".linksTo "http://localhost:8080/api/rest/latest/executions/83/execution-steps"
            }
        }


        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the execution"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    requestFields {
                        // ignore the prerequisite : it's part of the example to demonstrate that this field will not be modified, but then should not be documented
                        add PayloadDocumentation.fieldWithPath("prerequisite").ignored()

                        // actual API now
                        add "_type (string) : the type of the entity (mandatory)"
                        add "execution_status (string) : the new status of that execution"
                        add "comment (string) : the new comment of the execution"

                        add DescriptorLists.updateCufFields
                    }
                    fields {
                        relaxed = true
                        add "_type (string) : the type of the entity, etc"
                    }
                    _links {
                        add "self : the link to this execution"
                        add "project : the link to the execution project"
                        add "test_plan_item : the link to the test plan item of this execution"
                        add "execution-steps : the link to the execution steps"
                        add "attachments : the link to the attachments"

                    }
                }
        ))


    }

    def "patch-scripted-execution"() {

        given:
        def json = """{
    "_type" : "execution",
    "custom_fields" : [
        {
            "code" : "TXT_STATUS",
            "value" : "allright"
        },
        {
            "code" : "TAGS_RELATED",
            "value" : ["see this","also that"]
        }
    ],
    "test_case_custom_fields" : [
        {
            "code" : "TC_TEXT",
            "value" : "I'm from the test case"
        }
    ],
    "execution_status" : "RUNNING",
    "comment" : "<p>the comment was modified...</p>",
    "prerequisite" : "<p>impossible modification of the prerequisite</p>"
}"""


        and:
        def exec = SquashEntityBuilder.scriptedExecution {
            id = 83L
            name = "scripted-execution"
            executionStatus "blocked"
            description = "<p>no comment</p>"
            prerequisite = "<p>... but the prerequisite was not</p>"

            testPlan = itpi {
                iteration = iteration {
                    campaign = campaign {
                        project = project {
                            id = 14L
                            name = "project"
                        }
                    }
                }
            }
        }

        confHelper.createHints(_, _) >> new DeserializationHints(
                targetEntity: exec,
                mode: DeserializationHints.Mode.DESERIALIZE_UPDATE,
                filter: new DeserializationDynamicFilter(RestExecutionController.PATCH_DYNAMIC_FILTER),
                project: SquashEntityBuilder.project { id = 14L }
        )

        cufService.findAllCustomFieldValues(exec) >> [
                SquashEntityBuilder.cufValue {
                    name = "text"
                    label = "text"
                    bound "EXECUTION:83"
                    inputType InputType.PLAIN_TEXT
                    code = "TXT_STATUS"
                    value = "allright"
                },
                SquashEntityBuilder.cufValue {
                    name = "see also"
                    label = "see also"
                    inputType InputType.TAG
                    code = "TAGS_RELATED"
                    bound "EXECUTION:83"
                    value = ["see this", "also that"]
                }
        ]

        denoService.findAllForEntity(exec) >> [
                SquashEntityBuilder.denoValue {
                    bound = "EXECUTION:83"
                    boundOriginal = "TEST_CASE:20"
                    code = "TC_TEXT"
                    label = "test case cuf"
                    inputType "PLAIN_TEXT"
                    value = "I'm from the test case"
                },
                SquashEntityBuilder.denoValue {
                    bound = "EXECUTION:83"
                    boundOriginal = "TEST_CASE:20"
                    code = "TC_LABELS"
                    label = "labels"
                    inputType "TAG"
                    value = ["was", "not", "updated"]
                }
        ]


        when:


        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/executions/{id}?fields=execution_status,comment,prerequisite, custom_fields, test_case_custom_fields", 83L)
                .content(json)
                .contentType("application/json")
                .accept("application/json"))

        then:

        // check the deserialized content that is given to the service
        1 * restExecutionService.updateExecution({ args ->
            def ex = args.wrapped
            def cufs = args.customFields
            def deno = args.denormalizedFields

            return (
                    ex.id == 83L &&
                            ex.name == "scripted-execution" &&
                            ex.executionStatus == ExecutionStatus.RUNNING &&
                            ex.description == "<p>the comment was modified...</p>" &&
                            ex.prerequisite == "<p>... but the prerequisite was not</p>" &&
                            cufs.size() == 2 &&
                            cufs[0].code == "TXT_STATUS" &&
                            cufs[0].value.value == "allright" &&
                            cufs[1].code == "TAGS_RELATED" &&
                            cufs[1].value.values == ["see this", "also that"] &&
                            deno.size() == 1 &&
                            deno[0].code == "TC_TEXT" &&
                            deno[0].value.value == "I'm from the test case"
            )
        })

        /*
         * Test the response
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "scripted-execution"
            "id".is 83
            "execution_status".is "RUNNING"
            "comment".is "<p>the comment was modified...</p>"
            "prerequisite".is "<p>... but the prerequisite was not</p>"
            "custom_fields".test {
                "[0]".test {
                    "code".is "TXT_STATUS"
                    "label".is "text"
                    "value".is "allright"
                }
                "[1]".test {
                    "code".is "TAGS_RELATED"
                    "label".is "see also"
                    "value".contains "see this", "also that"
                }
            }
            "test_case_custom_fields".test {
                "[0]".test {
                    "code".is "TC_TEXT"
                    "label".is "test case cuf"
                    "value".is "I'm from the test case"
                }
                "[1]".test {
                    "code".is "TC_LABELS"
                    "label".is "labels"
                    "value".contains "was", "not", "updated"
                }
            }
            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/executions/83"
                "project".linksTo "http://localhost:8080/api/rest/latest/projects/14"
                "execution-steps".linksTo "http://localhost:8080/api/rest/latest/executions/83/execution-steps"
            }
        }

    }

    def "patch-keyword-execution"() {

        given:
        def json = """{
    "_type" : "execution",
    "custom_fields" : [
        {
            "code" : "TXT_STATUS",
            "value" : "allright"
        },
        {
            "code" : "TAGS_RELATED",
            "value" : ["see this","also that"]
        }
    ],
    "test_case_custom_fields" : [
        {
            "code" : "TC_TEXT",
            "value" : "I'm from the test case"
        }
    ],
    "execution_status" : "RUNNING",
    "comment" : "<p>the comment was modified...</p>",
    "prerequisite" : "<p>impossible modification of the prerequisite</p>"
}"""


        and:
        def exec = SquashEntityBuilder.keywordExecution {
            id = 83L
            name = "keyword-execution"
            executionStatus "blocked"
            description = "<p>no comment</p>"
            prerequisite = "<p>... but the prerequisite was not</p>"

            testPlan = itpi {
                iteration = iteration {
                    campaign = campaign {
                        project = project {
                            id = 14L
                            name = "project"
                        }
                    }
                }
            }
        }

        confHelper.createHints(_, _) >> new DeserializationHints(
                targetEntity: exec,
                mode: DeserializationHints.Mode.DESERIALIZE_UPDATE,
                filter: new DeserializationDynamicFilter(RestExecutionController.PATCH_DYNAMIC_FILTER),
                project: SquashEntityBuilder.project { id = 14L }
        )

        cufService.findAllCustomFieldValues(exec) >> [
                SquashEntityBuilder.cufValue {
                    name = "text"
                    label = "text"
                    bound "EXECUTION:83"
                    inputType InputType.PLAIN_TEXT
                    code = "TXT_STATUS"
                    value = "allright"
                },
                SquashEntityBuilder.cufValue {
                    name = "see also"
                    label = "see also"
                    inputType InputType.TAG
                    code = "TAGS_RELATED"
                    bound "EXECUTION:83"
                    value = ["see this", "also that"]
                }
        ]

        denoService.findAllForEntity(exec) >> [
                SquashEntityBuilder.denoValue {
                    bound = "EXECUTION:83"
                    boundOriginal = "TEST_CASE:20"
                    code = "TC_TEXT"
                    label = "test case cuf"
                    inputType "PLAIN_TEXT"
                    value = "I'm from the test case"
                },
                SquashEntityBuilder.denoValue {
                    bound = "EXECUTION:83"
                    boundOriginal = "TEST_CASE:20"
                    code = "TC_LABELS"
                    label = "labels"
                    inputType "TAG"
                    value = ["was", "not", "updated"]
                }
        ]


        when:


        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/executions/{id}?fields=execution_status,comment,prerequisite, custom_fields, test_case_custom_fields", 83L)
                .content(json)
                .contentType("application/json")
                .accept("application/json"))

        then:

        // check the deserialized content that is given to the service
        1 * restExecutionService.updateExecution({ args ->
            def ex = args.wrapped
            def cufs = args.customFields
            def deno = args.denormalizedFields

            return (
                    ex.id == 83L &&
                            ex.name == "keyword-execution" &&
                            ex.executionStatus == ExecutionStatus.RUNNING &&
                            ex.description == "<p>the comment was modified...</p>" &&
                            ex.prerequisite == "<p>... but the prerequisite was not</p>" &&
                            cufs.size() == 2 &&
                            cufs[0].code == "TXT_STATUS" &&
                            cufs[0].value.value == "allright" &&
                            cufs[1].code == "TAGS_RELATED" &&
                            cufs[1].value.values == ["see this", "also that"] &&
                            deno.size() == 1 &&
                            deno[0].code == "TC_TEXT" &&
                            deno[0].value.value == "I'm from the test case"
            )
        })

        /*
         * Test the response
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "keyword-execution"
            "id".is 83
            "execution_status".is "RUNNING"
            "comment".is "<p>the comment was modified...</p>"
            "prerequisite".is "<p>... but the prerequisite was not</p>"
            "custom_fields".test {
                "[0]".test {
                    "code".is "TXT_STATUS"
                    "label".is "text"
                    "value".is "allright"
                }
                "[1]".test {
                    "code".is "TAGS_RELATED"
                    "label".is "see also"
                    "value".contains "see this", "also that"
                }
            }
            "test_case_custom_fields".test {
                "[0]".test {
                    "code".is "TC_TEXT"
                    "label".is "test case cuf"
                    "value".is "I'm from the test case"
                }
                "[1]".test {
                    "code".is "TC_LABELS"
                    "label".is "labels"
                    "value".contains "was", "not", "updated"
                }
            }
            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/executions/83"
                "project".linksTo "http://localhost:8080/api/rest/latest/projects/14"
                "execution-steps".linksTo "http://localhost:8080/api/rest/latest/executions/83/execution-steps"
            }
        }
    }

}
