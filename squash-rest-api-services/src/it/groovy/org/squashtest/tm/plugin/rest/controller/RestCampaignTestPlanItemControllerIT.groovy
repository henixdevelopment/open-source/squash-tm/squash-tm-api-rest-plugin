/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestCampaignTestPlanItemService

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.campaign
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.campaignTestPlanItem
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.dataset
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scriptedTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.user

/**
 * Created by jlor on 24/07/2017.
 */
@WebMvcTest(RestCampaignTestPlanItemController)
public class RestCampaignTestPlanItemControllerIT extends BaseControllerSpec {

    @Inject
    private RestCampaignTestPlanItemService service;


    def "get-campaign-test-plan-item"() {

        given:

        def campaignTestPlanItem = campaignTestPlanItem {
            id = 89l
            referencedTestCase = testCase {
                id = 12l
                name = "referenced test case 12"
            }
            referencedDataset = dataset {
                id = 9l
                name = "referenced dataset 9"
            }
            assignedUser "User-1"
            campaign = campaign {
                id = 8l
                name = "sample campaign 8"
                reference = "SAMP_CAMP_8"
                project = project {
                    id = 7l
                }
            }
        }

        and:

        service.getOne(89) >> campaignTestPlanItem

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/campaign-test-plan-items/{id}", 89)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "campaign-test-plan-item"
            "id".is 89
            "referenced_test_case".test {
                "_type".is "test-case"
                "id".is 12
                "name".is "referenced test case 12"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/12"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 9
                "name".is "referenced dataset 9"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/9"
            }
            "assigned_to".is "User-1"
            "campaign".test {
                "_type".is "campaign"
                "id".is 8
                "name".is "sample campaign 8"
                "reference".is "SAMP_CAMP_8"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/8"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaign-test-plan-items/89"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/7"
                "test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/12"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/9"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/8"
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the campaign test plan item"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        add "_type (string) : the type of the entity"
                        add "id (number) : the id of this campaign test plan item"
                        addAndStop "referenced_test_case (object) : the test case associated with this campaign test plan item"
                        addAndStop "referenced_dataset (object) : the dataset associated with this campaign test plan item"
                        add "assigned_to (string) : the user assigned to this campaign test plan item"
                        addAndStop "campaign (object) : the campaign this campaign test plan item belongs to"
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this campaign test plan item"
                        add "project : link to the project of this item"
                        add "test-case : link to the test case referenced by this item"
                        add "dataset : link to the dataset referenced by this item"
                        add "campaign : link to the campaign this item belongs to"
                    }
                }
        ))

    }

    def "get-campaign-test-plan-item for scripted test case"() {

        given:

        def campaignTestPlanItem = campaignTestPlanItem {
            id = 89l
            referencedTestCase = scriptedTestCase {
                id = 12l
                name = "referenced test case 12"
            }
            referencedDataset = dataset {
                id = 9l
                name = "referenced dataset 9"
            }
            assignedUser "User-1"
            campaign = campaign {
                id = 8l
                name = "sample campaign 8"
                reference = "SAMP_CAMP_8"
                project = project {
                    id = 7l
                }
            }
        }

        and:

        service.getOne(89) >> campaignTestPlanItem

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/campaign-test-plan-items/{id}", 89)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "campaign-test-plan-item"
            "id".is 89
            "referenced_test_case".test {
                "_type".is "scripted-test-case"
                "id".is 12
                "name".is "referenced test case 12"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/12"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 9
                "name".is "referenced dataset 9"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/9"
            }
            "assigned_to".is "User-1"
            "campaign".test {
                "_type".is "campaign"
                "id".is 8
                "name".is "sample campaign 8"
                "reference".is "SAMP_CAMP_8"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/8"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaign-test-plan-items/89"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/7"
                "scripted-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/12"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/9"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/8"
            }
        }
    }

    def "get-campaign-test-plan-item for keyword test case"() {

        given:

        def campaignTestPlanItem = campaignTestPlanItem {
            id = 89l
            referencedTestCase = keywordTestCase {
                id = 12l
                name = "referenced test case 12"
            }
            referencedDataset = dataset {
                id = 9l
                name = "referenced dataset 9"
            }
            assignedUser "User-1"
            campaign = campaign {
                id = 8l
                name = "sample campaign 8"
                reference = "SAMP_CAMP_8"
                project = project {
                    id = 7l
                }
            }
        }

        and:

        service.getOne(89) >> campaignTestPlanItem

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/campaign-test-plan-items/{id}", 89)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "campaign-test-plan-item"
            "id".is 89
            "referenced_test_case".test {
                "_type".is "keyword-test-case"
                "id".is 12
                "name".is "referenced test case 12"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/12"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 9
                "name".is "referenced dataset 9"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/9"
            }
            "assigned_to".is "User-1"
            "campaign".test {
                "_type".is "campaign"
                "id".is 8
                "name".is "sample campaign 8"
                "reference".is "SAMP_CAMP_8"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/8"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaign-test-plan-items/89"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/7"
                "keyword-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/12"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/9"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/8"
            }
        }
    }

    def "post-add-campaign-test-plan-item"() {
        given:
        def json = """{
                "_type": "campaign-test-plan-item",
                
                    "test_case": {
                        "_type": "test-case",
                        "id": 238
                    },
                    "dataset": {
                        "_type": "dataset",
                        "id": 6
                    },
                    "assigned_to":"User-1"                     
                }"""

        def ctpi = campaignTestPlanItem {
            id = 15L
            referencedTestCase = testCase {
                id = 238L
                name = "Test-Case 1"
                reference = "Ref Test-Case 1"
            }
            referencedDataset = dataset {
                id = 6L
                name = "JD-1"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            campaign = campaign {
                id = 45L
                name = "AKM - Campaign Test"
                reference = "ABCD"
                project = project {
                    id = 2L
                }
            }
        }

        and:
        service.addTestCaseToCampaign(_, _) >> ctpi
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/campaign/{id}/test-plan", 45L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "campaign-test-plan-item"
            "id".is 15
            "referenced_test_case".test {
                "_type".is "test-case"
                "id".is 238
                "name".is "Test-Case 1"
                "reference".is "Ref Test-Case 1"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/238"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 6
                "name".is "JD-1"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/6"
            }
            "assigned_to".is "User-1"
            "campaign".test {
                "_type".is "campaign"
                "id".is 45
                "name".is "AKM - Campaign Test"
                "reference".is "ABCD"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/45"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaign-test-plan-items/15"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/2"
                "test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/238"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/6"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/45"
            }

        }
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the campaign"
                    }
                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "test_case (object) : the test case to include in the test plan (as described below)"
                        add "test_case._type (string) : the type of the entity (always 'test-case')"
                        add "test_case.id (number) : the id of the test case"
                        add "dataset (object) : the dataset to be used when the test case will be executed (optional)"
                        add "dataset._type (string) : the type of the entity (always 'dataset')"
                        add "dataset.id (number) : the id of the dataset. Remember that the dataset must belong to the planned test case."
                        add "assigned_to (string) : the username of the user assigned to this test case (optional)"
                    }

                }

        ))
    }

    def "post-add-campaign-test-plan-item for scripted test case"() {
        given:
        def json = """{
                "_type": "campaign-test-plan-item",
                
                    "test_case": {
                        "_type": "scripted-test-case",
                        "id": 238
                    },
                    "dataset": {
                        "_type": "dataset",
                        "id": 6
                    },
                    "assigned_to":"User-1"                     
                }"""

        def ctpi = campaignTestPlanItem {
            id = 15L
            referencedTestCase = scriptedTestCase {
                id = 238L
                name = "Test-Case 1"
                reference = "Ref Test-Case 1"
            }
            referencedDataset = dataset {
                id = 6L
                name = "JD-1"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            campaign = campaign {
                id = 45L
                name = "AKM - Campaign Test"
                reference = "ABCD"
                project = project {
                    id = 2L
                }
            }
        }

        and:
        service.addTestCaseToCampaign(_, _) >> ctpi
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/campaign/{id}/test-plan", 45L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "campaign-test-plan-item"
            "id".is 15
            "referenced_test_case".test {
                "_type".is "scripted-test-case"
                "id".is 238
                "name".is "Test-Case 1"
                "reference".is "Ref Test-Case 1"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/238"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 6
                "name".is "JD-1"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/6"
            }
            "assigned_to".is "User-1"
            "campaign".test {
                "_type".is "campaign"
                "id".is 45
                "name".is "AKM - Campaign Test"
                "reference".is "ABCD"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/45"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaign-test-plan-items/15"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/2"
                "scripted-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/238"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/6"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/45"
            }

        }
    }

    def "post-add-campaign-test-plan-item for keyword test case"() {
        given:
        def json = """{
                "_type": "campaign-test-plan-item",
                
                    "test_case": {
                        "_type": "keyword-test-case",
                        "id": 238
                    },
                    "dataset": {
                        "_type": "dataset",
                        "id": 6
                    },
                    "assigned_to":"User-1"                     
                }"""

        def ctpi = campaignTestPlanItem {
            id = 15L
            referencedTestCase = keywordTestCase {
                id = 238L
                name = "Test-Case 1"
                reference = "Ref Test-Case 1"
            }
            referencedDataset = dataset {
                id = 6L
                name = "JD-1"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            campaign = campaign {
                id = 45L
                name = "AKM - Campaign Test"
                reference = "ABCD"
                project = project {
                    id = 2L
                }
            }
        }

        and:
        service.addTestCaseToCampaign(_, _) >> ctpi
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/campaign/{id}/test-plan", 45L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "campaign-test-plan-item"
            "id".is 15
            "referenced_test_case".test {
                "_type".is "keyword-test-case"
                "id".is 238
                "name".is "Test-Case 1"
                "reference".is "Ref Test-Case 1"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/238"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 6
                "name".is "JD-1"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/6"
            }
            "assigned_to".is "User-1"
            "campaign".test {
                "_type".is "campaign"
                "id".is 45
                "name".is "AKM - Campaign Test"
                "reference".is "ABCD"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/45"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaign-test-plan-items/15"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/2"
                "keyword-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/238"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/6"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/45"
            }

        }
    }

    def "patch-modify-campaign-test-plan-item"() {
        given:
        def json = """{
                        "_type": "campaign-test-plan-item",                              
                        "dataset": {
                            "_type": "dataset",
                            "id": 6
                        },
                        "assigned_to":"User-1"                     
                    }"""
        def ctpi = campaignTestPlanItem {
            id = 13L
            referencedTestCase = testCase {
                id = 238L
                name = "Test-Case 1"
                reference = "Ref Test-Case 1"
            }
            referencedDataset = dataset {
                id = 6L
                name = "JD-1"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            campaign = campaign {
                id = 45L
                name = "AKM - Campaign Test"
                reference = "ABCD"
                project = project {
                    id = 2L
                }
            }
        }
        and:
        service.modifyCampaignTestPlan(_, _) >> ctpi
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/campaign-test-plan-items/{id}", 13L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "campaign-test-plan-item"
            "id".is 13
            "referenced_test_case".test {
                "_type".is "test-case"
                "id".is 238
                "name".is "Test-Case 1"
                "reference".is "Ref Test-Case 1"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/238"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 6
                "name".is "JD-1"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/6"
            }
            "assigned_to".is "User-1"
            "campaign".test {
                "_type".is "campaign"
                "id".is 45
                "name".is "AKM - Campaign Test"
                "reference".is "ABCD"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/45"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaign-test-plan-items/13"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/2"
                "test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/238"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/6"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/45"
            }

        }
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the campaign test plan item"
                    }
                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "dataset (object) : the dataset to use when the test case is executed (optional). You can remove the dataset by setting this to null."
                        add "dataset._type (string) : the type of the entity ('dataset')"
                        add "dataset.id (number) : the id of this dataset"
                        add "assigned_to (string) : the username of the user assigned to this test (optional). You can assign this test to nobody by setting this to null."
                    }

                }

        ))
    }

    def "patch-modify-campaign-test-plan-item for scripted test case"() {
        given:
        def json = """{
                        "_type": "campaign-test-plan-item",                              
                        "dataset": {
                            "_type": "dataset",
                            "id": 6
                        },
                        "assigned_to":"User-1"                     
                    }"""
        def ctpi = campaignTestPlanItem {
            id = 13L
            referencedTestCase = scriptedTestCase {
                id = 238L
                name = "Test-Case 1"
                reference = "Ref Test-Case 1"
            }
            referencedDataset = dataset {
                id = 6L
                name = "JD-1"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            campaign = campaign {
                id = 45L
                name = "AKM - Campaign Test"
                reference = "ABCD"
                project = project {
                    id = 2L
                }
            }
        }
        and:
        service.modifyCampaignTestPlan(_, _) >> ctpi
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/campaign-test-plan-items/{id}", 13L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "campaign-test-plan-item"
            "id".is 13
            "referenced_test_case".test {
                "_type".is "scripted-test-case"
                "id".is 238
                "name".is "Test-Case 1"
                "reference".is "Ref Test-Case 1"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/238"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 6
                "name".is "JD-1"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/6"
            }
            "assigned_to".is "User-1"
            "campaign".test {
                "_type".is "campaign"
                "id".is 45
                "name".is "AKM - Campaign Test"
                "reference".is "ABCD"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/45"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaign-test-plan-items/13"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/2"
                "scripted-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/238"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/6"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/45"
            }

        }
    }

    def "patch-modify-campaign-test-plan-item for keyword test case"() {
        given:
        def json = """{
                        "_type": "campaign-test-plan-item",                              
                        "dataset": {
                            "_type": "dataset",
                            "id": 6
                        },
                        "assigned_to":"User-1"                     
                    }"""
        def ctpi = campaignTestPlanItem {
            id = 13L
            referencedTestCase = keywordTestCase {
                id = 238L
                name = "Test-Case 1"
                reference = "Ref Test-Case 1"
            }
            referencedDataset = dataset {
                id = 6L
                name = "JD-1"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            campaign = campaign {
                id = 45L
                name = "AKM - Campaign Test"
                reference = "ABCD"
                project = project {
                    id = 2L
                }
            }
        }
        and:
        service.modifyCampaignTestPlan(_, _) >> ctpi
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/campaign-test-plan-items/{id}", 13L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "campaign-test-plan-item"
            "id".is 13
            "referenced_test_case".test {
                "_type".is "keyword-test-case"
                "id".is 238
                "name".is "Test-Case 1"
                "reference".is "Ref Test-Case 1"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/238"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 6
                "name".is "JD-1"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/6"
            }
            "assigned_to".is "User-1"
            "campaign".test {
                "_type".is "campaign"
                "id".is 45
                "name".is "AKM - Campaign Test"
                "reference".is "ABCD"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/45"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/campaign-test-plan-items/13"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/2"
                "keyword-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/238"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/6"
                "campaign.href".is "http://localhost:8080/api/rest/latest/campaigns/45"
            }

        }
    }

    def "delete-campaign-test-plan-item"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/campaign-test-plan-items/{testPlanItemsIds}", "44,43")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * service.deleteCampaignTestPlan(_)

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "testPlanItemsIds : the list of ids of the campaign test plan items"
                    }
                }
        ))

    }


}
