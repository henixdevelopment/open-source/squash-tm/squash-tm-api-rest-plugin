/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.plugin.rest.service.RestXsquashLicenseCheckService;
import org.squashtest.tm.service.license.LicenseInfoExtender;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class RestXsquashLicenseCheckServiceImpl implements RestXsquashLicenseCheckService {

    private static final String API_REST_ADMIN_KEY = "api-rest-admin";

    @Autowired(required = false)
    private List<LicenseInfoExtender> licenseInfoExtenders = Collections.emptyList();

    @Override
    public Boolean isValidLicenseForXsquash() {
        LicenseInfoExtender license = licenseInfoExtenders.stream().findFirst().orElse(null);

        return license != null ? isValid(license) : null;
    }

    // For Xsquash, we want : license is valid + one of the premium plugins is enabled, like api rest admin
    private boolean isValid(@NotNull LicenseInfoExtender license) {
        return license.isValidLicense() && license.isPluginEnabled(API_REST_ADMIN_KEY);
    }

}
