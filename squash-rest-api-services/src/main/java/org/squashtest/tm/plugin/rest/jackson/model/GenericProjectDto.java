/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.project.ProjectTemplate;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "_type")
@JsonSubTypes({ //here are the _type -> java class for DESERIALIZATION only
        @JsonSubTypes.Type(value = ProjectDto.class, name = "project"),
        @JsonSubTypes.Type(value = ProjectTemplateDto.class, name = "project-template"),
})
public abstract class GenericProjectDto {

    private Long id;

    private String description;

    private String label;

    private String name;

    private GenericProjectCopyParameterDto params;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GenericProjectCopyParameterDto getParams() {
        return params;
    }

    public void setParams(GenericProjectCopyParameterDto params) {
        this.params = params;
    }

    public abstract void accept(GenericProjectDtoVisitor genericProjectDtoVisitor);


    public static GenericProject convertDto(GenericProjectDto dto) {
        final GenericProject[] genericProjects = new GenericProject[1];

        GenericProjectDtoVisitor visitor = new GenericProjectDtoVisitor() {

            @Override
            public void visit(ProjectDto projectDto) {
                Project project = new Project();
                project.setName(projectDto.getName());
                project.setDescription(projectDto.getDescription());
                project.setLabel(projectDto.getLabel());
                genericProjects[0] = project;
            }

            @Override
            public void visit(ProjectTemplateDto projectTemplateDto) {
                ProjectTemplate projectTemplate = new ProjectTemplate();
                projectTemplate.setName(projectTemplateDto.getName());
                projectTemplate.setDescription(projectTemplateDto.getDescription());
                projectTemplate.setLabel(projectTemplateDto.getLabel());
                genericProjects[0] = projectTemplate;
            }
        };
        dto.accept(visitor);
        return genericProjects[0];
    }
}
