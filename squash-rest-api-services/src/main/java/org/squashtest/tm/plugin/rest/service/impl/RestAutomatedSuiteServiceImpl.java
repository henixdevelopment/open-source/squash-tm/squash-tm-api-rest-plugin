/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.testautomation.AutomatedSuite;
import org.squashtest.tm.plugin.rest.repository.RestAutomatedSuiteRepository;
import org.squashtest.tm.plugin.rest.service.RestAutomatedSuiteService;
import org.squashtest.tm.service.campaign.IterationTestPlanManagerService;
import org.squashtest.tm.service.testautomation.AutomatedSuiteManagerService;
import org.squashtest.tm.service.testautomation.testplanretriever.RestTestPlanFinder;

import javax.inject.Inject;
import java.util.List;

@Service
@Transactional
public class RestAutomatedSuiteServiceImpl implements RestAutomatedSuiteService {

    @Inject
    private AutomatedSuiteManagerService automatedSuiteManagerService;

    @Inject
    private RestAutomatedSuiteRepository suiteRepository;

    @Inject
    private IterationTestPlanManagerService iterationTestPlanManagerService;

    @Inject
    private RestTestPlanFinder testPlanFinder;

    @Override
    public AutomatedSuite findById(String id) {
        return automatedSuiteManagerService.findById(id);
    }

    @Override
    @Transactional(readOnly=true)
    public Page<AutomatedSuite> findAllReadable(Pageable pageable) {
        return suiteRepository.findAll(pageable);
    }

    @Override
    public AutomatedSuite createAutomatedSuiteFromIteration(long iterationId) {
        List<Long> itemIds = testPlanFinder.getItemTestPlanIdsByIterationId(iterationId);
        return automatedSuiteManagerService.createFromIterationTestPlan(iterationId, itemIds);
    }

    @Override
    public AutomatedSuite createAutomatedSuiteFromTestSuite(long testSuiteId) {
        List<Long> itemIds = testPlanFinder.getItemTestPlanIdsByTestSuiteId(testSuiteId);
        return automatedSuiteManagerService.createFromTestSuiteTestPlan(testSuiteId, itemIds);
    }

    @Override
    public AutomatedSuite createAutomatedSuiteFromIterationTestPlanItems(List<Long> itemIds) {

        IterationTestPlanItem item = iterationTestPlanManagerService.findTestPlanItem(itemIds.get(0));
        return automatedSuiteManagerService.createFromItemsAndIteration(itemIds, item.getIteration().getId());
    }

    @Override
    public void start(AutomatedSuite suite) {
        automatedSuiteManagerService.start(suite);
    }
}
