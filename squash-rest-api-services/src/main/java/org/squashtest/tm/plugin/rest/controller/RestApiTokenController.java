/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.users.ApiToken;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.ApiTokenRestDto;
import org.squashtest.tm.plugin.rest.service.RestApiTokenService;
import org.squashtest.tm.plugin.rest.validators.ApiTokenPostValidator;
import org.squashtest.tm.service.internal.dto.ApiTokenDto;

import javax.inject.Inject;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestApiController(ApiToken.class)
@UseDefaultRestApiConfiguration
@Validated
public class RestApiTokenController extends BaseRestController {

    @Inject private RestApiTokenService restApiTokenService;

    @Inject private ApiTokenPostValidator apiTokenPostValidator;

    @DeleteMapping(value = "/tokens/self")
    @ResponseBody
    public ResponseEntity<String> selfDestroyApiToken(
        @RequestHeader(value = "Authorization", required = false) String authorizationHeader) {
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            return new ResponseEntity<>("Token not provided", HttpStatus.UNAUTHORIZED);
        }

        String token = authorizationHeader.substring(7);
        restApiTokenService.selfDestroyToken(token);

        return new ResponseEntity<>("Token invalidated", HttpStatus.OK);
    }

    @GetMapping(value = "/tokens")
    @ResponseBody
    @EntityGetter
    public ResponseEntity<PagedModel<EntityModel<ApiToken>>> getAllTokensByUser(Pageable pageable) {
        Page<ApiToken> pagedApiTokens = restApiTokenService.getAllTokensByUser(pageable);
        PagedModel<EntityModel<ApiToken>> res = toPagedModel(pagedApiTokens);

        return ResponseEntity.ok(res);
    }

    @DeleteMapping(value = "/tokens/{id}")
    public ResponseEntity<String> deletePersonalApiToken(@PathVariable("id") long tokenId) {
        restApiTokenService.deletePersonalApiToken(tokenId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value="/tokens")
    @ResponseBody
    @DynamicFilterExpression("*,name,api_token[name]")

    public ResponseEntity<ApiTokenRestDto> postApiToken(@RequestBody @Valid ApiTokenRestDto apiTokenRestDto) throws BindException {
        validatePostApiToken(apiTokenRestDto);
        ApiTokenDto apiTokenDto = restApiTokenService.addToken(apiTokenRestDto);

        apiTokenRestDto.setGeneratedToken(apiTokenDto.generatedJwtToken());
        apiTokenRestDto.setId(apiTokenDto.squashApiToken().getId());
        apiTokenRestDto.setUser(apiTokenDto.squashApiToken().getUser());
        apiTokenRestDto.setCreatedOn(apiTokenDto.squashApiToken().getCreatedOn());
        apiTokenRestDto.setCreatedBy(apiTokenDto.squashApiToken().getCreatedBy());
        apiTokenRestDto.setUuid(apiTokenDto.squashApiToken().getUuid());
        apiTokenRestDto.setLastUsage(apiTokenDto.squashApiToken().getLastUsage());

        return ResponseEntity.status(HttpStatus.CREATED).body(apiTokenRestDto);
    }

    private void validatePostApiToken(ApiTokenRestDto apiTokenRestDto) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(apiTokenRestDto, "post-api-token");
        apiTokenPostValidator.validate(apiTokenRestDto, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(apiTokenRestDto, errors, "post-api-token");
    }
}
