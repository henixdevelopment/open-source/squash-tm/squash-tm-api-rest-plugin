/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.PropertyFilter;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.Annotations;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.customfield.CustomField;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.plugin.rest.core.jackson.SerializationDynamicFilter;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestCustomFieldMembers;
import org.squashtest.tm.plugin.rest.service.ExternalDataHandler;
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@Component
public class CustomFieldValuesPropertyWriter extends VirtualBeanPropertyWriter{

    @Inject
    private CustomFieldValueFinderService cufServices;

    @Inject
    private ExternalDataHandler externalDataHandler;

    public CustomFieldValuesPropertyWriter() {
        super();
    }


	public CustomFieldValuesPropertyWriter(BeanPropertyDefinition propDef, Annotations contextAnnotations,
                                           JavaType declaredType, ExternalDataHandler externalDataHandler, CustomFieldValueFinderService cufServices){
		super(propDef, contextAnnotations, declaredType);
		this.externalDataHandler = externalDataHandler;
        this.cufServices = cufServices;
    }


	@Override
	protected Object value(Object bean, JsonGenerator gen, SerializerProvider prov) throws Exception {
        RestCustomFieldMembers customFieldValueDtos = new RestCustomFieldMembers();

        List<CustomFieldValue> customFieldValues = getCustomFieldValues(bean);
        List<CustomFieldValue> filtered = applyFilter(customFieldValues, prov);
        for (CustomFieldValue customFieldValue : filtered) {
            CustomField customField = customFieldValue.getCustomField();
            customFieldValueDtos.add(new CustomFieldValueDto(customField, customFieldValue));
        }

		return customFieldValueDtos;
	}

    @SuppressWarnings("unchecked")
    private List<CustomFieldValue> getCustomFieldValues(Object bean) {
        List<?> values = externalDataHandler.getExternalData(bean, "custom_fields");
        if (values == null) {
            return cufServices.findAllCustomFieldValues((BoundEntity)bean);
        }
        return (List<CustomFieldValue>) values;
    }

    @Override
	public VirtualBeanPropertyWriter withConfig(MapperConfig<?> config, AnnotatedClass declaringClass,
			BeanPropertyDefinition propDef, JavaType type) {
		return new CustomFieldValuesPropertyWriter(propDef, declaringClass.getAnnotations(), type, externalDataHandler, cufServices);
	}


	/*
	 * The rules here a different from the regular rules (see FilterExpression). Indeed, a custom field
	 * will always be included unless explicitly disabled. Sort of a blacklist mode.
	 */
	private List<CustomFieldValue> applyFilter(List<CustomFieldValue> orig, SerializerProvider provider){

		// first, locate the filter if any
		SerializationDynamicFilter filter = findFilter(orig, provider);

		// abort if the filter is null or if the filter is not our dynamic filter
		if (filter == null){
			return orig;
		}

		// position the filter to the current bean
		filter = filter.forCurrentBean();

		// now filter
		List<CustomFieldValue> newlist = new ArrayList<>();
		for (CustomFieldValue value : orig){
			if (filter.include(value.getCustomField().getCode())){
				newlist.add(value);
			}
		}

		return newlist;

	}

	private SerializationDynamicFilter findFilter(List<CustomFieldValue> cufs, SerializerProvider provider){

		FilterProvider filterProvider = provider.getConfig().getFilterProvider();

		if (filterProvider != null){
			try{
				PropertyFilter filter = filterProvider.findPropertyFilter(SerializationDynamicFilter.FILTER_ID, cufs);
				if (filter != null && SerializationDynamicFilter.class.isAssignableFrom(filter.getClass())){
					return (SerializationDynamicFilter) filter;
				}
			}
			catch(IllegalArgumentException ex){
				// no big deal, that query just doesn't filter that object. Strange though.;
			}
		}

		return null;

	}


}
