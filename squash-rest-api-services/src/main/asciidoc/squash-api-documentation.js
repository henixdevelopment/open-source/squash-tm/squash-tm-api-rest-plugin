/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
document.addEventListener('DOMContentLoaded', () => {
  createExpandEltAndCollapseAllChapters();

  addClassOnAnchorTitles();

  selectChapterToc();

  createEvents();

  resizeColumnTableWidths();

  document.body.style.display = 'block';
});

function createEvents() {
  addEventListener("scrollend", () => handleScrollEnd());
  addEventListener('hashchange',() => selectChapterToc());
  createClickOnTocTitle();
}

function addClassOnAnchorTitles() {
  const anchors = document.querySelectorAll('h3, h2');
  for (const anchor of anchors) {
    anchor.classList.add('anchorTitle');
  }
}

function createClickOnTocTitle() {
  const titleElt = document.getElementById('toctitle');
  titleElt.addEventListener('click', initSelected);
  titleElt.style.cursor = 'pointer';
}

function createExpandEltAndCollapseAllChapters() {
  const items = document.querySelector('.sectlevel1').children;

  for (const item of items) {
    const div = createDivAndMoveA(item);
    item.insertBefore(div, item.firstChild);
    const itemUl = item.querySelector('ul');
    if (itemUl) {
      itemUl.style.display = 'none';
    }
  }
}

function expandOrCollapse(event) {
  const imgElt = event.target;
  const parentElement = imgElt.parentElement.parentElement;
  const chapterUlElement = parentElement.children[1];
  const isCollapsed = chapterUlElement.style.display === 'none';

  chapterUlElement.style.display = isCollapsed ? 'block' : 'none';

  imgElt.src = isCollapsed ? 'images/triangle-down.svg' : 'images/triangle-right.svg';
  imgElt.alt = isCollapsed ? 'collapse' : 'expand';
}

function removeTocSelectedClasses() {
  const selectedElement = document.querySelector('.selected');
  const selectedChildElement = document.querySelector('.selectedChild');

  if (selectedElement) {
    selectedElement.classList.remove('selected');
  }

  if (selectedChildElement) {
    selectedChildElement.classList.remove('selectedChild');
  }
}

function selectChapterToc() {
  removeTocSelectedClasses();

  const hashValue = window.location.hash;
  if (hashValue === '') {
    initSelected();
    return;
  }

  const element = document.querySelector(`.sectlevel1 a[href="${hashValue}"]`);
  if (element) {
    initTocCollapseOnLoad(element);
    return;
  }

  const sect2Parent = getParentByClass(document.getElementById(hashValue.substring(1)), 'sect2');
  if (sect2Parent !== null) {
    const sect2ParentHashValue = sect2Parent.querySelector('h3 a').hash;
    const element2 = document.querySelector(`.sectlevel1 a[href="${sect2ParentHashValue}"]`);
    if (element2) {
      initTocCollapseOnLoad(element2);
      return;
    }
  }

  initSelected();
}

function initTocCollapseOnLoad(element) {
  let isDirectLiParent = element.parentElement.classList.contains('collapsable');
  const liParent = isDirectLiParent ? element.parentElement.parentElement : element.parentElement.parentElement.parentElement;

  liParent.classList.add('selected');

  if (liParent.querySelector('ul')) {
    const img = liParent.querySelector('img');
    if (img && img.alt === 'expand') {
      img.click();
    }

    if (isDirectLiParent) {
      liParent.querySelector('li').classList.add('selectedChild');
    } else {
      element.parentElement.classList.add("selectedChild");
    }
  }
}

function getParentByClass(elt, classParent) {
  if (!elt || !elt.parentElement) {
    return null;
  }

  let parent = elt.parentElement;
  while (parent && !parent.classList.contains(classParent)) {
    parent = parent.parentElement;
  }
  return parent;
}

function createDivAndMoveA(item) {
  const divElt = document.createElement('div');
  divElt.classList.add("collapsable");

  if (item.querySelector('ul')) {
    const imgElt = createExpandImgElement();
    divElt.append(imgElt);
  } else {
    divElt.classList.add("noChildChapter");
  }

  divElt.append(item.querySelector('a'));
  return divElt;
}

function initSelected() {
  removeTocSelectedClasses();

  const isCollapsed = document.querySelector('.sectlevel1 li ul').style.display === 'none';
  if (isCollapsed) {
    document.querySelector('.sectlevel1 li img').click();
  }

  document.querySelector('.sectlevel1 li').classList.add('selected');
  document.querySelector('.sectlevel1 li ul li').classList.add('selectedChild');
  window.scrollTo({ top: 0 });
}

function createExpandImgElement() {
  const expandImg = document.createElement('img');
  expandImg.addEventListener('click', expandOrCollapse, this);
  expandImg.src = 'images/triangle-right.svg';
  expandImg.alt = 'expand';

  return expandImg;
}

function resizeColumnTableWidths() {
  const colgroups = document.querySelectorAll('#content colgroup');
  colgroups.forEach(colgroup => {
    const cols = colgroup.children;
    if (cols.length === 2) {
      cols[0].style.width = '30%';
      cols[1].style.width = '70%';
    } else if (cols.length === 3) {
      cols[0].style.width = '30%';
      cols[1].style.width = '10%';
      cols[2].style.width = '60%';
    }
  });
}

function handleScrollEnd() {
  const autoScroll = window.location.hash !== '' ? isAutoScroll() : false;
  if (!autoScroll) {
    const h3Elmts = document.querySelectorAll("#content .anchorTitle");
    let i = 0;

    // added 1 for chromium browsers
    const isScrollAtBottom = (window.innerHeight + Math.round(window.scrollY) + 1) >= document.body.offsetHeight;
    if (!isScrollAtBottom) {
      while (i < h3Elmts.length - 1 && h3Elmts[i].getBoundingClientRect().top < 30) {
        i++;
      }
      if (i > 0) {
        i--;
      }
    } else {
      i = h3Elmts.length - 1;
    }

    const newHash = "#" + h3Elmts[i].id;

    if (window.location.hash !== newHash) {
      selectChapterOnScroll(newHash);
    }
  }
}

function selectChapterOnScroll(newHash) {
  updateUrlHashWithoutTriggeringEvent(newHash);

  selectChapterToc();

  adjustTocScrollPosition();
}

function updateUrlHashWithoutTriggeringEvent(newHash) {
  if (history.pushState) {
    history.pushState(null, null, newHash);
  } else {
    window.location.hash = newHash;
  }
}

function adjustTocScrollPosition() {
  const selected = document.querySelector('.selected');
  let scrollTocNew = selected.offsetTop - 60;
  const toc = document.getElementById('toc');
  const selectedChild = document.querySelector('.selectedChild');
  if (selectedChild) {
    if (selected.offsetHeight > window.innerHeight) {
      scrollTocNew = selectedChild.offsetTop - 60;
    }
    if ((selectedChild.offsetTop > window.innerHeight + toc.scrollTop) || (selectedChild.offsetTop < toc.scrollTop)) {
      toc.scrollTop = scrollTocNew;
    }
  } else {
    if ((selected.offsetTop > window.innerHeight + toc.scrollTop) || (selected.offsetTop < toc.scrollTop)) {
      toc.scrollTop = scrollTocNew;
    }
  }
}

function isAutoScroll() {
  const hashSelector = document.querySelector(window.location.hash);
  return hashSelector !== null
    ? hashSelector.getBoundingClientRect().top > -1 && hashSelector.getBoundingClientRect().top < 1
    : false;
}
