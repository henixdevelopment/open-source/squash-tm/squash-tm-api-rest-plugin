/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson


import org.squashtest.tm.plugin.rest.core.exception.UnbalancedFilterExpressionException
import spock.lang.Specification
import spock.lang.Unroll

public class BaseDynamicFilterTest extends Specification{

	
	// ******** test the constructor *******	
		
	def "should initialize with an expression"(){
		given :
			def expr = "name,games[tagname,clans[*]],age"
			
		when :
			BaseDynamicFilter filter = new BaseDynamicFilter(expr)
		
		then :
			filter.rootPointer == new FilterExpression([ 
				name : null, 
				games : [
					tagname : null,
					clans : [
						"*" : null	
					] 
				],
				age : null
			])
	}
	
	
	def "should rant because the expression is invalid"(){
		
		when :
			new BaseDynamicFilter("omg]fail")
		
		then :
			thrown UnbalancedFilterExpressionException
	}
	
	
	// ******** simple functions **********
	
	def "the filter pointer should point to the nested filter"(){
		
		given :
			def filter = new BaseDynamicFilter("bob,mike[name],robert")
			filter.currentProperty = "mike"
			
		when :
			filter.advancePointer()
		
		then :
			filter.pointer == FilterExpressionBuilder.from("name")
			filter.pointerStack == [filter.rootPointer] as Deque<FilterExpression>
		
	}
	
	
	def "should reset the filter pointer to the previous position"(){
		
		given :
			def expr = FilterExpressionBuilder.from("bob,mike[name],robert")
		
		and :
			def filter = new BaseDynamicFilter()
			filter.rootPointer = expr
			filter.pointer = expr["mike"]
			filter.pointerStack = [ expr ] as Deque
			
		when :
			filter.resetPointer()
			
		then :
			filter.pointer == filter.rootPointer
			filter.pointerStack == [] as Deque
			
	}
	
	
	// ******** simple predicates **********
	
	def "should detect the filter is empty"(){
		
		expect :
			new BaseDynamicFilter("").filterIsEmpty()
			new BaseDynamicFilter(pointer:null).filterIsEmpty()
			new BaseDynamicFilter(pointer:new FilterExpression()).filterIsEmpty()
		
	}
	
	
	def "should detect that the current context is the root object"(){
		
		when :
			def filter = new BaseDynamicFilter("bob,mike[name],robert")
		
		then :
			filter.isRootObject()
		
	}
	
	def "should detect that current context is not at the root object"(){
		
		given : 
			def filter = new BaseDynamicFilter("bob,mike[name],robert")
		
		when :
			filter.advancePointer()
			
		then :
			! filter.isRootObject()
		
	}
	
	
	def "should assert that the property is defined"(){
		given :
			def filter = new BaseDynamicFilter("bob,mike[name],robert")
			filter.currentProperty = "bob"
			
		expect :
			filter.isPropertyDefined()
	}
	
	def "should assert that the property is not defined"(){
		given :
			def filter = new BaseDynamicFilter("bob,mike[name],robert")
			filter.currentProperty = "larry"
			
		expect :
			! filter.isPropertyDefined()
	}

	@Unroll("should assert that the property '#ppt' is #humanbool mandatory")
	def "should find whether the property is mandatory or not"(){
		
		given :
			def filter = new BaseDynamicFilter("bob,mike[name],robert")
			filter.currentProperty = ppt
			
		expect :
			filter.isMandatory() == isMandatory
			
		where :
			ppt		|	isMandatory		|	humanbool
			"_type"	|	true			|	""
			"id"	|	true			|	""
			"booze"	|	false			| 	"not"
			
	}
	
	
	def "should validate a property because a wildcard was set"(){
		
		given :
			def filter = new BaseDynamicFilter("*")
			filter.currentProperty = "whatever"
			
		expect :
			filter.usesStarAndNotDisabled()
				
	}
	
	def "should invalidate a property because it was specifically disabled"(){
		
		given :
			def filter = new BaseDynamicFilter("*,-name")
			filter.currentProperty = "name"
		
		expect :
			! filter.usesStarAndNotDisabled()
	}
	
	
	@Unroll("should assert that a property should #humanbool be included because #humanreason")
	def "should assert that a property should be included"(){
		
		given :
			def filter = new BaseDynamicFilter(expression)
		
		expect :
			filter.include(ppt) == res 
		
		where :
			expression	|	ppt		|	res		|	humanbool	|	humanreason
			"bob,mike"	|	"bob"	|	true	|	""			|	"the property is explicitly stated"
			""			|	"bob"	|	true	|	""			|	"filter is empty yet this is the root object"
			"*"			|	"bob"	|	true	|	""			|	"the filter is a star"
			"*,-bob"	|	"bob"	| 	false	|	"not"		|	"the filter is a star yet the property was explicitly disabled"
			"nothing"	|	"id"	|	true	|	""			|	"the property is mandatory and is always included"
			"*,-id"		|	"id"	|	true	|	""			|	"the property is mandatory and cannot be disabled"
			
	}
	
	
	def "should assert that a property should not be included because filter is empty and this is not the root object"(){
		
		given :
			def filter = new BaseDynamicFilter("bob,mike")		// aka : bob,mike[]
			filter.currentProperty = "mike"
			filter.pointer = filter.rootPointer["mike"]
			filter.pointerStack = [filter.rootPointer] as Deque
			
		expect :
			filter.include("name") == false
		
	}
	
	
	def "should return a new filter, based on the current state"(){
		
		given :
			def filter = new BaseDynamicFilter("bob,mike[name,address[zipcode]]")
			
		and :
			filter.currentProperty = "mike";
			filter.advancePointer();
			filter.currentProperty = "address";
			filter.advancePointer();
			
			
		when :
			def newfilter = filter.forCurrentBean()
		
		then :
			newfilter.rootPointer == filter.rootPointer["mike"]["address"]
			newfilter.pointer == newfilter.rootPointer
			newfilter.pointer == FilterExpressionBuilder.from("zipcode")
			newfilter.pointerStack.isEmpty()
			newfilter.currentProperty == null
			
		
	}


	def "should override the mandator properties, and now allow to disable 'id'"(){
		given :
			def filter = new BaseDynamicFilter("bob,-id")
			filter.defineMandatoryProperties("_type")

		when :
			def res = filter.include("id")

		then :
			res == false

	}

}
