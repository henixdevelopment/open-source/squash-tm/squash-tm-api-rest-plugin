/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.LinkBuilder;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.plugin.rest.core.hateoas.SingleRelPagedResources;
import org.squashtest.tm.plugin.rest.core.web.exceptionhandler.response.DetailedJsonError;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

public abstract class BaseRestController implements IRestController {

	@Inject
	protected BasePathAwareLinkBuildingService linkService;

	@Inject
	protected PagedResourcesAssembler pageAssembler;

	@Inject
	private BasicResourceAssembler resAssembler;


	protected <ENTITY extends Identified> LinkBuilder fromBasePath(WebMvcLinkBuilder builder){
		return linkService.fromBasePath(builder);
	}

	protected <ENTITY extends Identified> Link createSelfLink(ENTITY entity){
		return linkService.createSelfLink(entity);
	}

	protected <ENTITY extends Identified> Link createLinkTo(ENTITY entity){
		return linkService.createLinkTo(entity);
	}

	protected <ENTITY extends Identified> Link createLinkTo(Class<ENTITY> clazz, Long id){
		return linkService.createLinkTo(clazz, id);
	}

	protected <ENTITY extends Identified> Link createRelationTo(String relname){
		return linkService.createRelationRelativeToCurrentUri(relname);
	}

	protected <ENTITY extends Identified> Link createRelationTo(ENTITY entity, String relname){
		return linkService.createRelationTo(entity, relname);
	}


	protected <ENTITY extends Identified> EntityModel<ENTITY> toEntityModel(ENTITY object){
		return (EntityModel<ENTITY>) resAssembler.toModel((Identified)object);
	}

	protected <T> PagedModel<EntityModel<T>> toPagedModel(Page<T> page){
		return pageAssembler.toModel(page,resAssembler);
	}

	protected <T> SingleRelPagedResources<EntityModel<T>> toPagedResourcesWithRel(Page<T> page, String rel){
		PagedModel<EntityModel<T>> res = pageAssembler.toModel(page,resAssembler);
		return new SingleRelPagedResources<>(res, rel);
	}

    protected List<String> getFields(String fields) {
        if (!StringUtils.hasLength(fields)) {
            return Collections.emptyList();
        }
        return List.of(fields.split(","));
    }

    protected ResponseEntity<Object> buildDetailedErrorResponse(HttpStatus status, Exception e) {
        return buildDetailedErrorResponse(status, e.getMessage());
    }

    protected ResponseEntity<Object> buildDetailedErrorResponse(HttpStatus status, String message) {
        return ResponseEntity.status(status)
            .body(new DetailedJsonError(
                status.value(),
                status.getReasonPhrase(),
                message));
    }


}
