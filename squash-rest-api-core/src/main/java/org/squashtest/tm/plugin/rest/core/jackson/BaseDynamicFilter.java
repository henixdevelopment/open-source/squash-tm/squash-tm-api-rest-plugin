/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import org.squashtest.tm.plugin.rest.core.exception.UnbalancedFilterExpressionException;

import javax.annotation.concurrent.NotThreadSafe;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * <p>
 * 	That filter works with a {@link FilterExpression}, that is a hierarchical white-list of properties. See {@link FilterExpressionBuilder}. It is used with
 * 	{@link SerializationDynamicFilter} for serialization, or {@link DeserializationDynamicFilter} for deserialization
 * </p>
 * 
 * <p>Note : The filtering of the custom field values is handled </p>
 * 
 * @author bsiri
 *
 */

/*
 * Just like Jackson, the implementation crawls down the FilterExpression recursively. It uses a pointer to the current FilterExpression and a stack to reset its state when 
 * Jackson crawls back.
 * 
 * Hmm, pointers and stacks. It'd been a long time.
 *   
 */
@NotThreadSafe
public class BaseDynamicFilter{


	private static final Set<String> ALWAYS_INCLUDE;

	private static final String STAR = "*";

	static {
		ALWAYS_INCLUDE = new HashSet<>();
		ALWAYS_INCLUDE.add("_type");
		ALWAYS_INCLUDE.add("id");
	}


	private FilterExpression rootPointer;

	private FilterExpression pointer;

	private Deque<FilterExpression> pointerStack = new LinkedList<>();

	private String currentProperty;

	private Set<String> alwaysInclude = ALWAYS_INCLUDE;



	BaseDynamicFilter(){
		this("");
	}


	public BaseDynamicFilter(String filterExpression){
		super();
		try{
			this.rootPointer = FilterExpressionBuilder.from(filterExpression);
		}catch(UnbalancedFilterExpressionException ex){
			throw new UnbalancedFilterExpressionException("unbalanced filter expression : "+filterExpression);
		}
		this.pointer = this.rootPointer;
	}


	/**
	 * Non-public constructor
	 *
	 * @param objExpression
	 */
	public BaseDynamicFilter(FilterExpression objExpression){
		super();
		this.rootPointer = objExpression;
		this.pointer = this.rootPointer;
	}


	
	// *** semi-public API (for savvy users) ***

	/**
	 *	Allows to override the list of mandatory properties, ie the properties that a filter will always include, even
	 * when they are explicitly disabled by the {@link FilterExpression}
	 *
	 *
	 * @param properties
	 */
	void defineMandatoryProperties(String... properties){
		alwaysInclude = new HashSet<>(properties.length);
		for (String ppt : properties){
			alwaysInclude.add(ppt);
		}
	}

	/**
	 * Returns the current position (which node in the filtering tree) this instance
	 * is currently pointing to
	 *
	 * @return
	 */
	FilterExpression getPointer(){
		return pointer;
	}

	/**
	 *	Define which property of the current pointer will be considered next time
	 * {@link #advancePointer()} is invoked
	 *
	 * @param current
	 */
	void setCurrentProperty(String current){
		this.currentProperty = current;
	}


	public boolean isRootObject(){
		return pointerStack.isEmpty();
	}


	/**
	 * States whether a given property should be included or not in the serialization. 
	 * The result depends on the sate of the filter, ie which object is being serialized.
	 * Note that embedded (ie jsonunwrapped) will not be looked up.
	 * 
	 * @param pptName
	 * @return
	 */
	boolean include(String pptName){
		
		// lazy technique : save the current property, test, then restore it
		String savePpt = currentProperty;
		currentProperty = pptName;
		
		boolean res = include();
		
		currentProperty = savePpt;
		return res;
		
	}
	
	/**
	 * Returns a new filter, with the current pointer as the root pointer. The new filter 
	 * will then treat the current object as the new root, which changes then the 
	 * behavior of the "root object and empty filter" rule (see {@link FilterExpressionBuilder} )
	 * 
	 * @return
	 */
	BaseDynamicFilter forCurrentBean(){
		return new BaseDynamicFilter(pointer);
	}
	

	private boolean filterIsEmpty(){
		return (pointer == null) || (pointer.isEmpty());
	}

	private boolean isMandatory(){
		return alwaysInclude.contains(currentProperty);
	}
	
	private boolean isPropertyDefined(){
		return pointer.containsKey(currentProperty);
	}
		
	private boolean usesStarAndNotDisabled(){
		return (pointer.containsKey(STAR)) && (! pointer.containsKey("-"+currentProperty));
	}

	// *** high level predicates ***
	
	boolean include(){
		
		boolean shouldInclude = false;
		
		// if the property is mandatory -> serialize no matter what
		if (isMandatory()){
			shouldInclude = true;
		}
		
		// if filter is undefined, yet the object is the root object
		else if (filterIsEmpty()){ 
			if (isRootObject()){
				shouldInclude = true;
			}
		}
		
		/*
		 * if the filter is defined, the property gets serialized if :
		 *  - it was explicitly set,
		 *  - the star operator is set and it was not disabled with the prefix '-'
		 */		
		else if (isPropertyDefined() || usesStarAndNotDisabled()){
			shouldInclude = true;
		}
		
		return shouldInclude;
		
	}

	
	
	// *** operations ***
	
	void advancePointer(){
		pointerStack.push(pointer);
		if (pointer != null){
			pointer = pointer.get(currentProperty);
		}
	}
	
	void resetPointer(){
		pointer = pointerStack.pop();
	}


}
