/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.Annotations;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.environmentvariable.DenormalizedEnvironmentVariable;
import org.squashtest.tm.plugin.rest.jackson.model.DenormalizedEnvironmentVariableDto;
import org.squashtest.tm.plugin.rest.service.RestEnvironmentVariablesService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


@Component
public class EnvironmentVariablePropertyWriter extends VirtualBeanPropertyWriter{

    @Inject
    private RestEnvironmentVariablesService variableService;


    public EnvironmentVariablePropertyWriter() {
        super();
    }

    public EnvironmentVariablePropertyWriter(BeanPropertyDefinition propDef, Annotations contextAnnotations,
                                             JavaType declaredType, RestEnvironmentVariablesService variableService){
        super(propDef, contextAnnotations, declaredType);
        this.variableService = variableService;

    }


    @Override
    protected Object value(Object bean, JsonGenerator gen, SerializerProvider prov) throws Exception {
        List<DenormalizedEnvironmentVariableDto> environmentVariableData = new ArrayList<>();

        if (variableService != null) {
            List<DenormalizedEnvironmentVariable> values = variableService.findAllDenormalizedEnvironmentVariableBoundToExecution((BoundEntity) bean);

            for (DenormalizedEnvironmentVariable denormalizedEnvironmentVariable : values) {
                environmentVariableData.add(new DenormalizedEnvironmentVariableDto(denormalizedEnvironmentVariable
                ));
            }
        }

        return environmentVariableData;
    }

    @Override
    public VirtualBeanPropertyWriter withConfig(MapperConfig<?> mapperConfig, AnnotatedClass annotatedClass, BeanPropertyDefinition beanPropertyDefinition, JavaType javaType) {
        return new EnvironmentVariablePropertyWriter(beanPropertyDefinition, annotatedClass.getAnnotations(), javaType, variableService);

    }
}
