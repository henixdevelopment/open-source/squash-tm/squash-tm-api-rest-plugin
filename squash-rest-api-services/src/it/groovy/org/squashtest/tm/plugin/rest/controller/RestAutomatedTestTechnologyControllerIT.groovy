/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestAutomatedTestTechnologyService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestAutomatedTestTechnologyController)
public class RestAutomatedTestTechnologyControllerIT extends BaseControllerSpec {

    @Inject
    private RestAutomatedTestTechnologyService restAutomatedTestTechnologyService;

    def "get-automated-test-technology"() {
        given:
            def automatedTestTechnology = SquashEntityBuilder.automatedTestTechnology {
                id = 3L
                name = "JUnit"
                actionProviderKey = "junit/execute@v1"
            }
        and:
            restAutomatedTestTechnologyService.getOne(3L) >> automatedTestTechnology
        when:
            def result = mockMvc.perform(
                    get(
                            "/api/rest/latest/automated-test-technologies/{id}",
                            3L)
                            .header("Accept", "application/json"))
        then:
            result.andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
            withResult(result) {
                "_type".is "automated-test-technology"
                "id".is 3
                "name".is "JUnit"
                "action_provider_key".is "junit/execute@v1"
                selfRelIs "http://localhost:8080/api/rest/latest/automated-test-technologies/3"
            }

            result.andDo(doc.document(
                    documentationBuilder {
                        pathParams {
                            add "id : the id of the technology"
                        }
                        requestParams {
                            add DescriptorLists.fieldsParams
                        }
                        fields {
                            add "_type (string) : the type of the entity"
                            add "id (number) : the id of the technology"
                            add "name (string) : the name of the technology"
                            add "action_provider_key (string) : the action provider key of the technology"
                            add DescriptorLists.linksFields
                        }
                        _links {
                            add "self : link to this technology"
                        }
                    }
            ))
    }

    def "get-all-automated-test-technologies"() {
        given:
            def technology1 = SquashEntityBuilder.automatedTestTechnology {
                id = 1L
                name = "Robot Framework"
                actionProviderKey = "robotframework/execute@v1"
            }
            def technology2 = SquashEntityBuilder.automatedTestTechnology {
                id = 2L
                name = "Cypress"
                actionProviderKey = "cypress/execute@v1"
            }
            def technology3 = SquashEntityBuilder.automatedTestTechnology {
                id = 3L
                name = "JUnit"
                actionProviderKey = "junit/execute@v1"
            }
        and:
            restAutomatedTestTechnologyService.findAll(_) >> {
                args -> new PageImpl<AutomatedTestTechnology>([technology1, technology2, technology3], args[0], 3)
            }
        when:
            def result = mockMvc.perform(
                    get("/api/rest/latest/automated-test-technologies")
                    .header("Accept", "application/json"))
            then:
                result.andExpect(status().isOk())
                        .andExpect(content().contentType("application/json"))

                withResult(result) {
                    "_embedded.automated-test-technologies".hasSize 3
                    "_embedded.automated-test-technologies".test {
                        "[0]".test {
                            "_type".is "automated-test-technology"
                            "id".is 1
                            "name".is "Robot Framework"
                            "action_provider_key".is "robotframework/execute@v1"
                            selfRelIs "http://localhost:8080/api/rest/latest/automated-test-technologies/1"
                        }
                        "[1]".test {
                            "_type".is "automated-test-technology"
                            "id".is 2
                            "name".is "Cypress"
                            "action_provider_key".is "cypress/execute@v1"
                            selfRelIs "http://localhost:8080/api/rest/latest/automated-test-technologies/2"
                        }
                        "[2]".test {
                            "_type".is "automated-test-technology"
                            "id".is 3
                            "name".is "JUnit"
                            "action_provider_key".is "junit/execute@v1"
                            selfRelIs "http://localhost:8080/api/rest/latest/automated-test-technologies/3"
                        }
                    }
                }

                result.andDo(doc.document(
                        DocumentationSnippets.AllInOne.createBrowseAllEntities("automated-test-technologies")
                ))
    }

    def "post-automated-test-technology"() {
        given:
            def json = """
{
    "_type": "automated-test-technology",
    "name": "NewTechnology",
    "action_provider_key": "newtechnology/execute@v1"
}
"""
            def newTechnology = SquashEntityBuilder.automatedTestTechnology {
                id = 4L
                name = "NewTechnology"
                actionProviderKey = "newtechnology/execute@v1"
            }
        and:
            restAutomatedTestTechnologyService.addAutomatedTestTechnology(_) >> newTechnology
        when:
            def result = mockMvc.perform(
                    post("/api/rest/latest/automated-test-technologies")
                    .accept("application/json")
                    .contentType("application/json")
                    .content(json))
        then:
            result.andExpect(status().isCreated())
                    .andExpect(content().contentType("application/json"))

            withResult(result) {
                "_type".is "automated-test-technology"
                "id".is 4
                "name".is "NewTechnology"
                "action_provider_key".is "newtechnology/execute@v1"
                selfRelIs "http://localhost:8080/api/rest/latest/automated-test-technologies/4"
            }

            result.andDo(doc.document(
                    documentationBuilder {
                        requestParams {
                            add DescriptorLists.fieldsParams
                        }
                        requestFields {
                            add "_type (string) : the type of the entity"
                            add "name (string) : the name of the technology"
                            add "action_provider_key (string) : the action provider key of the technology"
                        }
                        fields {
                            add DescriptorLists.idTypeFields
                            add "name (string) : the name of the technology"
                            add "action_provider_key (string) : the action provider key of the technology"
                            add DescriptorLists.linksFields
                        }
                        _links {
                            add "self : link to this technology"
                        }
                    }
            ))

    }
}
