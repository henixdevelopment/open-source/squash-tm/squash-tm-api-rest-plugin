/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.project.Project;

import java.util.List;

/*AMK*/

public class TestSuiteDto {

    /* Attributes sent in Json*/
    private Long id;
    private String name;
    private String description;
    private String status;
    private ParentEntity parent;

    /*Attribue manipulé par le service qui va utiliser le dto*/
    private Iteration iteration;
    private Project project;

    /* indique si un attribut a été envoyé et sa valeur est vide ou l'attribut n'est pas envoyé dans le json  */
    private boolean hasCufs = false;
    private boolean hasListItpi = false;
    private boolean hasDescription = false;
    private boolean hasStatus = false;

    @JsonProperty("custom_fields")
    private List<CustomFieldValueDto> customFields;

    @JsonProperty("test_plan")
    private List<IterationTestPlanItemDto> listItpi;

    public Long getId() { return id;}

    public void setId(Long id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) {this.name = name;}

    public String getDescription() {return description;}

    public void setDescription(String description) {
        this.description = description;
        this.hasDescription = true;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        this.hasStatus = true;
    }

    public boolean isHasDescription() {
        return hasDescription;
    }

    public void setHasDescription(boolean hasDescription) {
        this.hasDescription = hasDescription;
    }

    public boolean isHasStatus() {
        return hasStatus;
    }

    public void setHasStatus(boolean hasStatus) {
        this.hasStatus = hasStatus;
    }

    public ParentEntity getParent() {return parent;}

    public void setParent(ParentEntity parent) {this.parent = parent;}

    public Iteration getIteration() {return iteration; }

    public void setIteration(Iteration iteration) {this.iteration = iteration;}

    public List<CustomFieldValueDto> getCustomFields() {return customFields;}

    public void setCustomFields(List<CustomFieldValueDto> customFields) {
        this.customFields = customFields;
        this.hasCufs = true;
    }

    public List<IterationTestPlanItemDto> getListItpi() {
        return listItpi;
    }

    public void setListItpi(List<IterationTestPlanItemDto> listItpi) {
        this.listItpi = listItpi;
        this.hasListItpi = true;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public boolean isHasCufs() {
        return hasCufs;
    }

    public void setHasCufs(boolean hasCufs) {
        this.hasCufs = hasCufs;
    }

    public boolean isHasListItpi() {
        return hasListItpi;
    }

    public void setHasListItpi(boolean hasListItpi) {
        this.hasListItpi = hasListItpi;
    }
}
