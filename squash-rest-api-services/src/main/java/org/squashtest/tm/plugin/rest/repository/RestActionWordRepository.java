/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.domain.bdd.ActionWord;

import java.util.Collection;
import java.util.List;

public interface RestActionWordRepository extends JpaRepository<ActionWord, Long> {

    @Query("from ActionWord aw where aw.project.id in (:projectIds)")
    Page<ActionWord> findAllInProjects(@Param("projectIds") Collection<Long> projectIds, Pageable pageable);

    @Query("from ActionWord aw where aw.project.id = :projectId")
    Page<ActionWord> findAllInProject(@Param("projectId") long projectId, Pageable pageable);

    @Query("select awln.id from ActionWordLibraryNode awln where awln.entityId in (:actionWordIds) and awln.entityType = 'ACTION_WORD'")
    List<Long> getNodeIdsFromActionWordIds(@Param("actionWordIds") Collection<Long> actionWordIds);

}
