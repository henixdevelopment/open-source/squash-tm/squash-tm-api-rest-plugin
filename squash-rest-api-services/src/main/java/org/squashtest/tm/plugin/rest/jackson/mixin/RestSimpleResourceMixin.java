/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.squashtest.tm.domain.audit.AuditableSupport;
import org.squashtest.tm.plugin.rest.core.jackson.SerializationDynamicFilter;

/**
 * Specific resource mixin for folders. Because it is solely used as a JsonUnwrapped attribute in a RestRequirementFolderMixin we cannot afford clashes with properties such as 'id' or 'audit'. 
 * 
 * @author bsiri
 *
 */
@JsonAutoDetect(fieldVisibility=Visibility.NONE, getterVisibility=Visibility.NONE, isGetterVisibility=Visibility.NONE)
@JsonPropertyOrder({"name", "audit", "description"})
@JsonFilter(SerializationDynamicFilter.FILTER_ID)
// that's odd but we have to specify the type here
public abstract class RestSimpleResourceMixin {

	
	@JsonProperty
	String name;
	
	@JsonProperty
	String description;
	
	@JsonUnwrapped
	AuditableSupport audit;
	
}
