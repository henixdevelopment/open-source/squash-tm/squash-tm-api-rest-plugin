/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.Annotations;
import org.springframework.stereotype.Component;
import org.squashtest.tm.core.foundation.lang.Wrapped;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionVisitor;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.KeywordExecution;
import org.squashtest.tm.domain.execution.ScriptedExecution;

@Component
public class ScriptedExecutionScriptNamePropertyWriter extends VirtualBeanPropertyWriter{

	public ScriptedExecutionScriptNamePropertyWriter() {
		super();
	}

	private ScriptedExecutionScriptNamePropertyWriter(BeanPropertyDefinition propDef, Annotations contextAnnotations, JavaType declaredType) {
		super(propDef, contextAnnotations, declaredType);

	}

	@Override
	protected Object value(Object bean, JsonGenerator gen, SerializerProvider prov) {
		Execution holder = (Execution) bean;

		Wrapped<String> language = new Wrapped<>("");
		ExecutionVisitor executionVisitor = new ExecutionVisitor() {
			@Override
			public void visit(Execution execution) {
				// no script
			}

			@Override
			public void visit(ScriptedExecution scriptedExecution) {
				language.setValue(scriptedExecution.getScriptName());
			}

			@Override
			public void visit(KeywordExecution keywordExecution) {
				// no script
			}

			@Override
			public void visit(ExploratoryExecution exploratoryExecution) {
				// no script
			}
		};

		holder.accept(executionVisitor);
		return language.getValue();
	}

	@Override
	public VirtualBeanPropertyWriter withConfig(MapperConfig<?> config, AnnotatedClass declaringClass,
												BeanPropertyDefinition propDef, JavaType type) {
		return new ScriptedExecutionScriptNamePropertyWriter(propDef, declaringClass.getAnnotations(), type);
	}
	
}
