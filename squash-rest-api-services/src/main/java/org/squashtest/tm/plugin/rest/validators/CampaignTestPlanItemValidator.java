/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignDto;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignTestPlanItemDto;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestType;
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto;
import org.squashtest.tm.plugin.rest.validators.helper.CampaignDtoValidationHelper;
import org.squashtest.tm.service.campaign.CampaignTestPlanManagerService;
import org.squashtest.tm.service.internal.repository.CampaignTestPlanItemDao;
import org.squashtest.tm.service.internal.repository.DatasetDao;
import org.squashtest.tm.service.internal.repository.UserDao;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Component
public class CampaignTestPlanItemValidator implements Validator {


    private static final String POST_CAMPAIGN_TEST_PLAN = "post-campaign-test-plan";
    private static final String PATCH_CAMPAIGN_TEST_PLAN = "patch-campaign-test-plan";


    @Inject
    private UserDao userDao;
    @Inject
    private DatasetDao datasetDao;
    @Inject
    private CampaignTestPlanItemDao ctpiDao;


    @Inject
    private CampaignDtoValidationHelper campaignDtoValidationHelper;

    @Inject
    private CampaignTestPlanManagerService campaignTestPlanManagerService;

    public CampaignTestPlanItemValidator() {
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return CampaignDto.class.equals(clazz);
    }

    public void validatePostTestPlanItem(CampaignTestPlanItemDto ctpiDto, Long campaignId) throws BindException{

        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(ctpiDto, POST_CAMPAIGN_TEST_PLAN);

        // validate campaign exist
        campaignDtoValidationHelper.checkEntityExist(validation,RestType.CAMPAIGN,campaignId);

        //validate testCase and dataSet
        validate(ctpiDto,validation);

        //validate assigned to
        validateAssignedTo(campaignId, ctpiDto, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }
        ErrorHandlerHelper.throwIfError(ctpiDto, errors, POST_CAMPAIGN_TEST_PLAN);

    }

    public void validatePatchTestPlanItem(CampaignTestPlanItemDto ctpiDto, Long testPlanId) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(ctpiDto, PATCH_CAMPAIGN_TEST_PLAN);

        // validate ctpi exist
        campaignDtoValidationHelper.checkEntityExist(validation,RestType.CAMPAIGN_TEST_PLAN_ITEM,testPlanId);

        CampaignTestPlanItem ctpi = ctpiDao.findById(testPlanId);

        if (ctpi != null) {
            //validate  dataSet
            validatePatch(ctpi,ctpiDto, testPlanId, validation);

            //validate assigned to
            Long campaignId = ctpi.getCampaign().getId();
            validateAssignedTo(campaignId, ctpiDto, validation);
        }

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(ctpiDto, errors, PATCH_CAMPAIGN_TEST_PLAN);

    }

    @Override
    public void validate(Object target, Errors errors) {

        CampaignTestPlanItemDto ctpiDto = (CampaignTestPlanItemDto) target;

        TestCaseDto referencedTcDTo = ctpiDto.getReferencedTestCase();

        DatasetDto referencedDatasetDto = ctpiDto.getReferencedDataset();

        if(ctpiDto.getId() != null) {
            errors.rejectValue("id", "generated value",
                    "This attribute is generated by database and should not be provided. " +
                            "If you want to update an existing campaign test plan item , " +
                            "please do a patch request to the campaign test plan item  id. ");
        }
        //validate Test case

        if(!referencedTcDTo.getRestType().equals(RestType.TEST_CASE)) {
            errors.rejectValue("_type", "invalid type","Type test-case expected");
        }else if(referencedTcDTo.getId()== null) {
            errors.rejectValue("id", "generated value","The test case id must not be null ");
        }
        campaignDtoValidationHelper.checkEntityExist(errors,RestType.TEST_CASE,referencedTcDTo.getId());

        //validate DataSet
        if(referencedDatasetDto != null){
            campaignDtoValidationHelper.checkEntityExist(errors,RestType.DATASET,referencedDatasetDto.getId());
            Dataset ds= datasetDao.getReferenceById(referencedDatasetDto.getId());
            if (!ds.getTestCase().getId().equals(referencedTcDTo.getId())){
                errors.rejectValue("id", "invalid value","This dataset is not attached to this test case");

            }

        }
    }
    public void validateAssignedTo(Long campaignId, CampaignTestPlanItemDto ctpiDto,Errors errors){

        if(ctpiDto.getUser()!=null) {

            User assignedTo = ctpiDto.getUser()!= null ? userDao.findUserByLogin(ctpiDto.getUser()): null;
            if (assignedTo == null) {
                errors.rejectValue("user", "not found value", "this user does not exist");

            } else {
                List<User> listUser = campaignTestPlanManagerService.findAssignableUserForTestPlan(campaignId);
                if (listUser.size()==0) {
                    errors.rejectValue("user", "empty value", "no user are assignable for this campaign");
                } else if (!listUser.stream().anyMatch(c -> c.getLogin().equals(assignedTo.getLogin()))) {
                    errors.rejectValue("user", "not found value", "this user is not assignable for this test plan");
                }

            }
        }

    }



    public void validatePatch(CampaignTestPlanItem ctpi, CampaignTestPlanItemDto ctpiDto, Long testPlanId, Errors errors){

        TestCase referencedTc = ctpi.getReferencedTestCase();
        DatasetDto referencedDatasetDto = ctpiDto.getReferencedDataset();

        //validate DataSet
        if(referencedDatasetDto!=null){
            if(referencedDatasetDto.getId()!=null){
                campaignDtoValidationHelper.checkEntityExist(errors,RestType.DATASET,referencedDatasetDto.getId());
                Dataset ds= referencedDatasetDto.getId()!= null ? datasetDao.getOne(referencedDatasetDto.getId()): null;
                if (!ds.getTestCase().getId().equals(referencedTc.getId())){
                    errors.rejectValue("id", "invalid value","This dataset is not attached to this test case");
                }
            }
        }
    }

}
