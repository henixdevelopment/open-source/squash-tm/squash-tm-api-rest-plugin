/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import com.google.common.base.CaseFormat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.acl.AclGroup;
import org.squashtest.tm.domain.users.Team;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyClearance;
import org.squashtest.tm.plugin.rest.jackson.model.RestUserPermission;
import org.squashtest.tm.plugin.rest.jackson.model.UserDto;
import org.squashtest.tm.plugin.rest.service.RestPartyService;
import org.squashtest.tm.plugin.rest.service.RestProjectService;
import org.squashtest.tm.plugin.rest.validators.PartyPatchValidator;
import org.squashtest.tm.plugin.rest.validators.PartyPostValidator;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestApiController(User.class)
@UseDefaultRestApiConfiguration
public class RestUserController extends BaseRestController {

    @Inject
    private RestPartyService restPartyService;

    @Inject
    private PartyPostValidator partyPostValidator;

    @Inject
    private PartyPatchValidator partyPatchValidator;

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private RestProjectService restProjectService;


    /**
     * Get the user with the given id
     * @param userId the id of the user
     * @return a User
     */
    @GetMapping(value = "/users/{id}")
    @EntityGetter
    @DynamicFilterExpression("*, teams[*, -members], audit[*]")
    public ResponseEntity<EntityModel<User>> findUser(@PathVariable("id") long userId) {

        User user = restPartyService.findUserByPartyId(userId);

        EntityModel<User> res = toEntityModel(user);

        return ResponseEntity.ok(res);
    }

    /**
     * get all users
     * @return a list of users
     */
    @GetMapping(value = "/users")
    @ResponseBody
    @DynamicFilterExpression("login, active, group")
    public ResponseEntity<PagedModel<EntityModel<User>>> findAllUsers(Pageable pageable) {
        Page<User> pagedUsers = restPartyService.findAllUsers(pageable);

        PagedModel<EntityModel<User>> res = toPagedModel(pagedUsers);

        return ResponseEntity.ok(res);
    }

    /**
     * create  a user
     * @return a User
     */
    @PostMapping(value = "/users")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<EntityModel<User>> createUser(@RequestBody UserDto userDto) throws BindException {
        validatePostUser(userDto);
        User user = (User) restPartyService.createParty(userDto);
        EntityModel<User> res = toEntityModel(user);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }

    /**
     * update the given user
     * @param id the id of the user
     * @return a User
     */
    @PatchMapping(value = "/users/{id}")
    @ResponseBody
    @DynamicFilterExpression("*, teams[*, -members]")
    public ResponseEntity<EntityModel<User>> patchUser(@RequestBody UserDto patch, @PathVariable("id") long id) throws BindException {

        patch.setId(id);

        validatePatchUser(patch);

        User user = (User) restPartyService.patchParty(patch, id);

        EntityModel<User> res = toEntityModel(user);

        return ResponseEntity.ok(res);
    }


    /**
     * delete all users with the given ids
     * @param userIds ids of users
     */
    @DeleteMapping(value = "/users/{ids}")
    public ResponseEntity<Void> deleteUsers(@PathVariable("ids") List<Long> userIds) {
        restPartyService.deleteUsers(userIds);

        return ResponseEntity.noContent().build();
    }


    /**
     * get all teams of the given user
     * @param userId the id of the user
     * @return a list of teams
     */
    @GetMapping(value = "/users/{userId}/teams")
    @ResponseBody
    @DynamicFilterExpression("*, -members")
    public ResponseEntity<PagedModel<EntityModel<Team>>> findTeams(@PathVariable("userId") long userId, Pageable pageable) {

        Page<Team> teams = restPartyService.findTeamsByUser(userId, pageable);
        PagedModel<EntityModel<Team>> res = toPagedModel(teams);

        return ResponseEntity.ok(res);
    }

    /**
     * add user to given teams
     * @param userId the id of the user
     * @param teamIds ids of teams
     */
    @PostMapping(value = "/users/{userId}/teams")
    public ResponseEntity<Object> addTeamsToUser(@PathVariable("userId") long userId, @RequestParam(value = "teamIds") List<Long> teamIds) {

        Set<String> failedTeams = restPartyService.addTeamsToUser(userId, teamIds);

        if (failedTeams.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            String warningMessage = "The following teams could not be added: " + String.join(", ", failedTeams);
            return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT)
                    .body(warningMessage);
        }
    }

    /**
     * delete user to the given teams
     * @param userId the id of the user
     * @param teamIds ids of teams
     */
    @DeleteMapping(value = "/users/{userId}/teams")
    public ResponseEntity<Void> disassociateUserFromTeams(@PathVariable("userId") long userId, @RequestParam(value = "teamIds") List<Long> teamIds) {

        restPartyService.disassociateUserFromTeams(userId, teamIds);
        return ResponseEntity.noContent().build();

    }

    @GetMapping("/users/{userId}/clearances")
    public ResponseEntity<EntityModel<RestPartyClearance>> getClearancesForGivenUser(@PathVariable("userId") long userId) {
        RestPartyClearance clearances = restPartyService.getAllClearancesForGivenUser(userId);
        EntityModel<RestPartyClearance> res = EntityModel.of(clearances);
        linksHelper.addAllLinksForUserClearance(userId, res);
        return ResponseEntity.ok(res);
    }

    /**
     * Get all permission for the giver user
     * @param userId the id of the user
     * @return all permission of the user
     */
    @GetMapping("/users/{userId}/permissions")
    public ResponseEntity<EntityModel<RestUserPermission>> getPermissionForGivenUser(@PathVariable("userId") long userId) {
        RestUserPermission restUserPermission = restPartyService.getAllPermissionsForGivenUser(userId);
        EntityModel<RestUserPermission> res = EntityModel.of(restUserPermission);
        linksHelper.addAllLinksForUserPermission(userId, res);
        return ResponseEntity.ok(res);
    }

    @PostMapping("/users/{userId}/clearances/{profileId}/projects/{projectIds}")
    public ResponseEntity<EntityModel<RestPartyClearance>> addClearanceToGivenUser(@PathVariable("userId") long userId,
                                                                                   @PathVariable("profileId") long profileId,
                                                                                   @PathVariable("projectIds") List<Long> projectIds) {
        restPartyService.addClearanceToUser(userId, projectIds, profileId);
        return getClearancesForGivenUser(userId);
    }

    /**
     * Get all permission for the given user
     * @param userId the id of the user
     * @param permissionGroup the profile you want to attribute to the user
     * @param projectIds all the squash projects you want bin with the profile
     */
    @PostMapping("/users/{userId}/permissions/{permissionGroup}")
    public ResponseEntity<EntityModel<RestUserPermission>> addPermissionToGivenUser(@PathVariable("userId") long userId,
                                                    @PathVariable(value = "permissionGroup")String permissionGroup,
                                                                                    @RequestParam(value = "ids") List<Long> projectIds) throws BindException {

        RestUserController.PostUserPermissionModel bean = new PostUserPermissionModel(projectIds, permissionGroup);
        validatePostNewPermissionsToUser(bean);
        restPartyService.addPermissionToUser(userId, projectIds, bean.permissionGroup);
        return getPermissionForGivenUser(userId);
    }

    @DeleteMapping("/users/{userId}/clearances/{projectIds}")
    public ResponseEntity<Void> removeClearanceToUser(@PathVariable("userId") long userId,
                                                       @PathVariable("projectIds") List<Long> projectIds) {
        restPartyService.removePermissionToUser(userId,projectIds);
        return ResponseEntity.noContent().build();
    }


    /**
     * Delete all permissions for the given projects for the given user
     * @param userId the id of the user
     * @param projectIds all the squash projects you want bin with the profile
     */
    @DeleteMapping("/users/{userId}/permissions")
    public ResponseEntity<Void> removePermissionToUser(@PathVariable("userId") long userId,
                                                       @RequestParam(value = "ids")List<Long> projectIds) {

        restPartyService.removePermissionToUser(userId,projectIds);
        return ResponseEntity.noContent().build();
    }


    private void validatePatchUser(UserDto patch) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(patch, "patch-user");
        partyPatchValidator.validate(patch, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(patch, errors, "patch-user");
    }

    private void validatePostUser(UserDto userDto) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(userDto, "post-user");
        partyPostValidator.validate(userDto, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(userDto, errors, "post-user");
    }

    private void validatePostNewPermissionsToUser(RestUserController.PostUserPermissionModel bean) throws BindException {

        List<Errors> errors = new ArrayList<>();
        BindingResult validation = new BeanPropertyBindingResult(bean, "post-user-permission");
        int index = 0;
        for (Long projectId : bean.projectIds) {
            if (!restProjectService.isGenericProjectExist(projectId)) {
              String message = String.format("the project with the id %d doesn't exit", projectId);
                String fieldName = String.format("ids[%d]", index);
                validation.rejectValue(fieldName, "invalid ids for project", message);
                errors.add(validation);
                validation = new BeanPropertyBindingResult(bean, "post-user-permission");

            }
            index += 1;
        }

        String permissionGroupName = bean.permissionGroup;

        // manual translation for advanced tester |*_*|
        if ("advanced_tester".equals(bean.permissionGroup)){
            bean.permissionGroup = "advance_tester";
        }


        bean.permissionGroup = CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, bean.permissionGroup);

        if (!getAllPermissionGroups().contains(bean.permissionGroup)) {

            String permissionGroupNames = getAllPermissionGroupsInString();
            String message = String.format("No permission group known for %s. Options available : %s"  , permissionGroupName,  permissionGroupNames);
            validation.rejectValue("permissionGroup", "invalid permission group", message);
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(bean, errors, "post-user-permission");

    }

    private List<String> getAllPermissionGroups(){
        List<AclGroup> groups = restProjectService.findAllPossiblePermission();
        List<String> groupNames = new ArrayList<>();
        for (AclGroup group : groups) {
            groupNames.add(group.getSimpleName());
        }

        return groupNames;
    }

    private String getAllPermissionGroupsInString(){
        return getAllPermissionGroups().stream()
                .map(val -> "advanceTester".equalsIgnoreCase(val)?"advancedTester":val )
                .map(pg -> CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE,pg))
                .collect(Collectors.joining(","));
    }

    /**
     * a private bean for validation purpose
     */
    private class PostUserPermissionModel {
        private List<Long> projectIds;
        private String permissionGroup;

        public PostUserPermissionModel(List<Long> projectIds, String permissionGroup) {
            this.projectIds = projectIds;
            this.permissionGroup = permissionGroup;
        }

        public List<Long> getProjectIds() {
            return projectIds;
        }
        public String getPermissionGroup() {
            return permissionGroup;
        }
    }

}
