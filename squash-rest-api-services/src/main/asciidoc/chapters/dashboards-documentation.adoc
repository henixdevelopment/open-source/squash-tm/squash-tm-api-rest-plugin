== Dashboards

This chapter focuses on services for the dashboards.

=== image:get.png[] Get dashboard

A `GET` to `/dashboards/{id}` returns the dashboard with the given id.

operation::RestReportingControllerIT/get-dashboard[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== image:delete.png[] Delete dashboards

A `DELETE` to `/dashboards/{ids}` deletes one or several dashboard(s) with the given id(s).

operation::RestReportingControllerIT/delete-dashboards[snippets='path-parameters,http-request']
