/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.domain.users.User;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class ApiTokenRestDto {

    private Long id;
    private String uuid;
    private User user;
    private String name;
    private String permissions;

    @NotNull @JsonProperty(value = "expiry_date", required = true) private Date expiryDate;
    @JsonProperty("created_on") private Date createdOn;
    @JsonProperty("created_by") private String createdBy;
    @JsonProperty("last_usage") private Date lastUsage;

    @JsonProperty("generated_token") private String generatedToken;

    public String getName() {
        return name;
    }

    public String getPermissions() {
        return permissions;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setGeneratedToken(String generatedToken) {
        this.generatedToken = generatedToken;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getUuid() {
        return uuid;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public Date getLastUsage() {
        return lastUsage;
    }

    public String getGeneratedToken() {
        return generatedToken;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setLastUsage(Date lastUsage) {
        this.lastUsage = lastUsage;
    }
}
