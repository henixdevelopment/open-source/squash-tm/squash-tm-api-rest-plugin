/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.utils;

import org.springframework.core.MethodParameter;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.library.GenericLibraryNode;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;
import org.squashtest.tm.plugin.rest.core.exception.ProgrammingError;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationDynamicFilter;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationFilterExpression;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;
import org.squashtest.tm.plugin.rest.core.jackson.EntityManagerWrapper;
import org.squashtest.tm.plugin.rest.core.jackson.WrappedDTO;
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints.Mode;

/**
 *  <p>In the context of deserialization, that utility will retrieve several useful entities that may be consumed by
 *  deserializers and validators : which project, parent node, and entity are under consideration, whether we are creating or
 *  updating an entity, and the deserialization filter.</p>
 *
 *  <p>
 *     It relies on several assumptions :
 *      <ul>
 *          <li>For PATCH :
 *              <ul>
 *                  <li>the patched entity exists</li>
 *                  <li>its ID can be extracted from the request URI because it's the first numeric sequence in it</li>
 *                  <li>its type can be deduced from the parameter type in the controller handler signature</li>
 *                  <li>its project and parent can then be retrieved too</li>
 *              </ul>
 *          </li>
 *          <li> For POST :
 *              <ul>
 *                  <li>the type of the posted entity can be deduced from the parameter type in the controller handler signature</li>
 *                  <li>the entity is posted in a container entity, </li>
 *                  <li>if posted in a container :
 *                      <li>in which case the type of the parent can be deduced from annotation @RestApiController,</li>
 *                      <li>type of the parent extracted from the request URI with the same rules than above,</li>
 *                      <li>the project is deduced from the parent</li>
 *                  </li>
 *                  <li>
 *                      ... or posted standalone in the overal referential (for instance, new customfields). In which case
 *                      no parent nor project is relevant.
 *                  </li>
 *              </ul>
 *          </li>
 *
 *      </ul>
 *  </p>
 *
 * Created by bsiri on 11/07/2017.
 */
public class DeserializationConfigHelper {

    private static final Pattern PLACEHOLDER_MATCHER = Pattern.compile("\\{[^\\\\}]+\\}") ;
    private static final String MATCH_IDS = "(\\\\d+)";



    private EntityManagerWrapper em;
    private NodeHierarchyHelpService hierarchyService;

    public DeserializationConfigHelper(EntityManagerWrapper em, NodeHierarchyHelpService hierarchyService){
        this.em = em;
        this.hierarchyService = hierarchyService;
    }

    public DeserializationHints createHints(HttpServletRequest request, MethodParameter param, String modelName){
        DeserializationHints hints = createHints(request, param);
        hints.setBindingResult(new BeanPropertyBindingResult(null, modelName));
        return hints;
    }

    public DeserializationHints createHints(HttpServletRequest request, MethodParameter param){

        Mode mode = extractMode(request);

        DeserializationDynamicFilter filter = extractFilter(param);

        /*
         * from now on, finding the target, the parent and the project depend on the mode :
         *
         * A/ PATCH
         *
         * in case of a patch, the entity is already persistent and the project and parent can be retrieved
         * from the entity itself. The type of the entity is deduced from the type of the method parameter.
         *
         * B/ POST
         *
         * in case of a post, the entity is new. The parent and project must be found from the invokation context.
         * The target type is still extracted from the type of the method parameter.
         */

        Identified target = null;
        Project project = null;
        Identified parent = null;
        switch(mode){
            case DESERIALIZE_UPDATE:
                target = getTarget(request, param);
                project = getProjectFromEntity(target);
                parent = getParentFromEntity(target);
                break;

            //case DESERIALIZE_CREATE: TODO

            default :
                throw new ProgrammingError("Eh, that case was not implemented yet !");

        }

        DeserializationHints hints = new DeserializationHints();
        hints.setMode(mode);
        hints.setFilter(filter);
        hints.setTargetEntity(target);
        hints.setProject(project);
        hints.setParent(parent);

        return hints;
    }


    private Mode extractMode(HttpServletRequest request){
        DeserializationHints.Mode mode = null;
        switch(request.getMethod()){
            case "POST" : mode = DeserializationHints.Mode.DESERIALIZE_CREATE; break;
            case "PATCH" : mode = DeserializationHints.Mode.DESERIALIZE_UPDATE; break;
            default :
                throw new RuntimeException("attempted processing http method '"+request.getMethod()+"', yet only POST or PATCH are supported");
        }
        return mode;
    }

    private DeserializationDynamicFilter extractFilter(MethodParameter param){
        String strExpr = "*";
        DeserializationFilterExpression expr = param.getMethodAnnotation(DeserializationFilterExpression.class);
        if (expr != null){
            strExpr = expr.value();
        }

        return new DeserializationDynamicFilter(strExpr);
    }


    // ******************** POST : TARGET extraction methods *********************************

    //TODO OOOOOOOO


    // ******************** PATCH : TARGET extraction methods ********************************

    /*
      * returns the entity type that must be deserialized. In case it is actually wrapped in an instance of
      * WrappedDTO, the type of the wrapped entity is returned instead
      */

    private Identified getTarget(HttpServletRequest request, MethodParameter param){
        Class<?> targetClass = extractTargetType(param);
        Long targetId = extractIdFromUrl(request, param);

        return (Identified)em.find(targetClass, targetId);
    }

    private Class<?> extractTargetType(Class<?> rawtype){
        if (WrappedDTO.class.isAssignableFrom(rawtype)){
            Type type = rawtype;

            do {
                if (type instanceof ParameterizedType) {
                    type = ((Class<?>) ((ParameterizedType) type).getRawType()).getGenericSuperclass();
                } else {
                    type = ((Class<?>) type).getGenericSuperclass();
                }
            }while(!(type instanceof ParameterizedType) && ((ParameterizedType) type).getRawType() != WrappedDTO.class);

            return (Class<?>)((ParameterizedType) type).getActualTypeArguments()[0];
        }
        else{
            return rawtype;
        }
    }

    private Class<?> extractTargetType(MethodParameter param){
        Class<?> rawtype = param.getParameterType();
        return extractTargetType(rawtype);
    }

    private Long extractIdFromUrl(HttpServletRequest request, MethodParameter param){
        RequestMapping annot = param.getMethodAnnotation(RequestMapping.class);
        Matcher matcher = toPattern(annot).matcher(request.getRequestURI());
        if (matcher.find()){
            return Long.valueOf(matcher.group(1));
        }
        else{
            return null;
        }
    }

    // ************** PATCH : project fetch code *******************

    // woaw, ugly code here
    // may return null (not all entities have a project)
    private Project getProjectFromEntity(Identified entity){
        Class<?> targetClass = entity.getClass();
        if (BoundEntity.class.isAssignableFrom(targetClass)){
            return ((BoundEntity)entity).getProject();
        }
        else if (GenericLibraryNode.class.isAssignableFrom(targetClass)){
            return ((GenericLibraryNode)entity).getProject();
        }
        else{
            // TODO : once you encounter your NPE, feel free to add more code here
            return null;
        }

    }


    // ************* PATCH : parent fetch code ********************

    // woaw, this is even more disgusting
    // we need a new interface, something like HasContainer or something
    // may return null (not all entities have a parent, or not all require a parent for processing)
    private Identified getParentFromEntity(Identified entity){
        Class<?> targetClass = entity.getClass();

        Identified parent = null;

        if (TestCaseLibraryNode.class.isAssignableFrom(targetClass)){
            parent = hierarchyService.findParentFor((TestCaseLibraryNode)entity);
        }
        else if (RequirementLibraryNode.class.isAssignableFrom(targetClass)){
            parent = hierarchyService.findParentFor((RequirementLibraryNode) entity);
        }
        else if (RequirementVersion.class.isAssignableFrom(targetClass)){
            parent = ((RequirementVersion)entity).getRequirement();
        }
        else if (CampaignLibraryNode.class.isAssignableFrom(targetClass)){
            parent = hierarchyService.findParentFor((CampaignLibraryNode) entity);
        }
        // else null, maybe we won't need it. if you do, implement your use case here.

        return parent;
    }


    // ******************* pattern matching utilities *************

    private Pattern toPattern(RequestMapping annot){
        String orig = annot.value()[0];
        String replaced = PLACEHOLDER_MATCHER.matcher(orig).replaceAll(MATCH_IDS);
        return Pattern.compile(replaced+"$");
    }

}
