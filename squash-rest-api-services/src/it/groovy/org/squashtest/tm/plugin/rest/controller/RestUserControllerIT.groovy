/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.project.GenericProject
import org.squashtest.tm.domain.users.Team
import org.squashtest.tm.domain.users.User
import org.squashtest.tm.domain.users.UsersGroup
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyClearance
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyProfileDto
import org.squashtest.tm.plugin.rest.jackson.model.RestUserPermission
import org.squashtest.tm.plugin.rest.service.RestPartyService
import org.squashtest.tm.plugin.rest.service.RestProjectService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.hamcrest.Matchers.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestUserController)
class RestUserControllerIT extends BaseControllerSpec {

    @Inject
    private RestPartyService service

    @Inject
    private RestProjectService restProjectService

    def "browse-users"() {
        given:
        service.findAllUsers(any(Pageable.class)) >> { args ->

            def user = SquashEntityBuilder.user {
                id = 486L
                firstName = "Charles"
                lastName = "Dupond"
                login = "User-1"
                email = "charlesdupond@aaaa.aa"
                active = true
                group UsersGroup.USER
                lastConnectedOn "2018/02/11"
            }

            new PageImpl<User>([user], args[0], 5)
        }


        when:
        def res = mockMvc.perform(get("/api/rest/latest/users?page=2&size=1").header("Accept", "application/json"))


        then:

        /*
         * Test (style is plain Spring testmvc)
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        // embedded section
                .andExpect(jsonPath("_embedded.users", hasSize(1)))
                .andExpect(jsonPath("_embedded.users[0]._type", is("user")))
                .andExpect(jsonPath("_embedded.users[0].id", is(486)))
                .andExpect(jsonPath("_embedded.users[0].login", is("User-1")))
                .andExpect(jsonPath("_embedded.users[0].active", is(true)))
                .andExpect(jsonPath("_embedded.users[0].group", is("User")))
                .andExpect(jsonPath("_embedded.users[0]._links.self.href", is("http://localhost:8080/api/rest/latest/users/486")))

        // links section
                .andExpect(jsonPath("_links.first.href", is("http://localhost:8080/api/rest/latest/users?page=0&size=1")))
                .andExpect(jsonPath("_links.prev.href", is("http://localhost:8080/api/rest/latest/users?page=1&size=1")))
                .andExpect(jsonPath("_links.self.href", is("http://localhost:8080/api/rest/latest/users?page=2&size=1")))
                .andExpect(jsonPath("_links.next.href", is("http://localhost:8080/api/rest/latest/users?page=3&size=1")))
                .andExpect(jsonPath("_links.last.href", is("http://localhost:8080/api/rest/latest/users?page=4&size=1")))

        // page section
                .andExpect(jsonPath("page.size", is(1)))
                .andExpect(jsonPath("page.totalElements", is(5)))
                .andExpect(jsonPath("page.totalPages", is(5)))
                .andExpect(jsonPath("page.number", is(2)))

        /*
         * Documentation
         */
        res.andDo(doc.document(
                DocumentationSnippets.AllInOne.createBrowseAllEntities("users")
        ))

    }

    def "get-user"() {

        given:
        def team = SquashEntityBuilder.team {
            id = 567L
            name = "Team A"
            description = "<p>black panther</p>"
        }

        def user = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
            canDeleteFromFront = true
        }

        and:
        service.findUserByPartyId(486) >> user

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/users/{id}", 486).header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "user"
            "id".is 486
            "first_name".is "Charles"
            "last_name".is "Dupond"
            "login".is "User-1"
            "email".is "charlesdupond@aaaa.aa"
            "active".is true
            "group".is "User"
            "can_delete_from_front".is true
            selfRelIs "http://localhost:8080/api/rest/latest/users/486"
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the user"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        add "_type (string) : the type of the entity"
                        add "id (number) : the id of the user"
                        add "first_name (string) : the first name of the user"
                        add "last_name (string) : the last name of the user"
                        add "login (string) : the login of the user"
                        add "email (string) : the email address of the user"
                        add "active (boolean) : whether the user is activate or not"
                        add "group (string) : the group of the user belongs to (admin or user)"
                        add "can_delete_from_front (boolean) : whether the user can delete library entities from the front-end with the Squash TM Premium plugin"
                        add "teams (array) : the team of the user participate"
                        add "last_connected_on (string) : the date of this user was last connected"
                        add "created_by (string) : the user who created this user account"
                        add "created_on (string) : the date of this user account was created"
                        add "last_modified_by (string) : the user who last modified this user account"
                        add "last_modified_on (string) : the date of this user account was last modified"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this user"
                    }
                }
        ))
    }

    def "post-user"() {
        given:
        def json = """{
            "_type": "user",
            "first_name": "Charles",
            "last_name": "Dupond",
            "login": "User-1",
            "password": "123456",
            "email": "charlesdupond@aaaa.aa",
            "group": "User",
            "can_delete_from_front": true
        }"""

        def user = SquashEntityBuilder.user {
            id = 987L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/03/05"
            canDeleteFromFront = true
        }
        and:
        service.createParty(_) >> user

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/users")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "user"
            "id".is 987
            "first_name".is "Charles"
            "last_name".is "Dupond"
            "login".is "User-1"
            "email".is "charlesdupond@aaaa.aa"
            "active".is true
            "group".is "User"
            "can_delete_from_front".is true
            selfRelIs "http://localhost:8080/api/rest/latest/users/987"
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "first_name (string) : the first name of the user (optional)"
                        add "last_name (string) : the last name of the user"
                        add "login (string) : the login of the user"
                        add "password (string) : the password of the user"
                        add "email (string) : the email address of the user (optional)"
                        add "group (string) : the group of the user belongs to (admin or user)"
                        add "can_delete_from_front (boolean) : whether the user can delete library entities from the front-end with the Squash TM Premium plugin"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the user"
                        add "active (boolean) : whether the user is activate or not"
                        add "teams (array) : the team of the user participate"
                        add "last_connected_on (string) : the date of this user was last connected"
                        add "created_by (string) : the user who created this user account"
                        add "created_on (string) : the date of this user account was created"
                        add "last_modified_by (string) : the user who last modified this user account"
                        add "last_modified_on (string) : the date of this user account was last modified"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this user"
                    }
                }
        ))
    }

    def "delete-user"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/users/{ids}", "169,189")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * service.deleteUsers(_)

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the users"
                    }
                }
        ))
    }

    def "get-teams"() {
        given:
        def user = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        def user2 = SquashEntityBuilder.user {
            id = 487L
            firstName = "John"
            lastName = "Doe"
            login = "User-2"
            email = "johndoe@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        and:
        service.findTeamsByUser(486L, any(Pageable.class)) >> { args ->

            def team = SquashEntityBuilder.team {
                id = 567L
                name = "Team A"
                description = "<p>black panther</p>"
                members = [user, user2].toSet()
            }

            def team2 = SquashEntityBuilder.team {
                id = 568L
                name = "Team B"
                description = "<p>black widow</p>"
                members = [user].toSet()
            }

            new PageImpl<Team>([team, team2], args[1], 2)

        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/users/{id}/teams", 486).header("Accept", "application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.teams".hasSize 2
            "_embedded.teams".test {
                "[0]".test {
                    "_type".is "team"
                    "name".is "Team A"
                    "description".is "<p>black panther</p>"
                }
                "[1]".test {
                    "_type".is "team"
                    "name".is "Team B"
                    "description".is "<p>black widow</p>"
                }
            }
            "page".test {
                "size".is 20
                "totalElements".is 2
                "totalPages".is 1
                "number".is 0
            }
            selfRelIs "http://localhost:8080/api/rest/latest/users/486/teams?page=0&size=20"
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the user"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.paginationParams
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "teams (array) : the teams of this user"
                        add DocumentationSnippets.DescriptorLists.paginationFields
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add DocumentationSnippets.DescriptorLists.paginationLinks
                    }
                }
        ))
    }

    def "add-teams"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/users/{userId}/teams?teamIds=486,487", 987L)
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        1 * service.addTeamsToUser(987, [486, 487]) >> []
        res.andExpect(status().isNoContent())
        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "userId : the id of the user"
                    }
                    requestParams {
                        add "teamIds : the list of ids of the teams"
                    }
                }
        ))
    }

    def "disassociate-teams"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/users/{userId}/teams?teamIds=486,487", 987L)
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * service.disassociateUserFromTeams(987, [486, 487])

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "userId : the id of the user"
                    }
                    requestParams {
                        add "teamIds : the list of ids of the teams"
                    }
                }
        ))
    }

    def "patch-user"() {
        given:
        def json = """{
            "_type": "user",
            "first_name": "Charles",
            "last_name": "Dupond",
            "login": "User-42",
            "password": "123456",
            "email": "charlesdupond@bbbb.bb",
            "active": false,
            "group": "User",
            "can_delete_from_front": false
        }"""

        def user = SquashEntityBuilder.user {
            id = 987L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-42"
            email = "charlesdupond@bbbb.bb"
            active = false
            group UsersGroup.USER
            lastConnectedOn "2018/03/05"
            canDeleteFromFront = false
        }

        and:
        service.patchParty(_, 987) >> user


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/users/{id}", 987L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))


        then:
        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "id".is 987
            "first_name".is "Charles"
            "last_name".is "Dupond"
            "login".is "User-42"
            "email".is "charlesdupond@bbbb.bb"
            "active".is false
            "group".is "User"
            "can_delete_from_front".is false
            selfRelIs "http://localhost:8080/api/rest/latest/users/987"
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the user"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "first_name (string) : the first name of the user"
                        add "last_name (string) : the last name of the user"
                        add "login (string) : the login of the user"
                        add "password (string) : the password of the user"
                        add "email (string) : the email address of the user"
                        add "active (boolean) : whether the user is activate or not"
                        add "group (string) : the group of the user belongs to (admin or user)"
                        add "can_delete_from_front (boolean) : whether the user can delete library entities from the front-end with the Squash TM Premium plugin"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the user"
                        add "teams (array) : the team of which the user is a member"
                        add "last_connected_on (string) : the date of this user was last connected"
                        add "created_by (string) : the user who created this user account"
                        add "created_on (string) : the date of this user account was created"
                        add "last_modified_by (string) : the user who last modified this user account"
                        add "last_modified_on (string) : the date of this user account was last modified"
                        add "can_delete_from_front (boolean) : whether the user can delete library entities from the front-end with the Squash TM Premium plugin"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this user"
                    }
                }
        ))
    }

    def "get-user-clearances"() {

        given:
        def project1 = SquashEntityBuilder.project {
            id = 367L
            name = "proj1"
            description = "<p>This project is the main sample project</p>"
            label = "Main Sample Project"
        }
        def project2 = SquashEntityBuilder.project {
            id = 368L
            name = "proj2"
            description = "<p>This project is the main sample project</p>"
            label = "Main Sample Project"
        }
        def project3 = SquashEntityBuilder.project {
            id = 369L
            name = "proj3"
            description = "<p>This project is the main sample project</p>"
            label = "Main Sample Project"
        }

        def user1 = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        def clearances = new RestPartyClearance()
        def testDesigner = new RestPartyProfileDto([project1, project2] as List<GenericProject>, 7L, "TestDesigner", true)
        def validator = new RestPartyProfileDto([project3] as List<GenericProject>, 9L, "Validator", true)
        clearances.partyClearances.put("test_designer", testDesigner)
        clearances.partyClearances.put("validator", validator)

        and:
        service.getAllClearancesForGivenUser(486) >> clearances

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/users/{id}/clearances", 486).header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "content.test_designer".test {
                "_type".is "profile"
                "id".is 7
                "name".is "TestDesigner"
                "type".is "system"
                "projects".hasSize 2
                "projects".test {
                    "[0]".test {
                        "_type".is "project"
                        "id".is 367
                        "name".is "proj1"
                        selfRelIs "http://localhost:8080/api/rest/latest/projects/367"

                    }
                    "[1]".test {
                        "_type".is "project"
                        "id".is 368
                        "name".is "proj2"
                        selfRelIs "http://localhost:8080/api/rest/latest/projects/368"

                    }
                }
            }

            "content.validator".test {
                "_type".is "profile"
                "id".is 9
                "name".is "Validator"
                "type".is "system"
                "projects".hasSize 1
                "projects".test {
                    "[0]".test {
                        "_type".is "project"
                        "id".is 369
                        "name".is "proj3"
                        selfRelIs "http://localhost:8080/api/rest/latest/projects/369"

                    }
                }
            }

            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/users/486/clearances"
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the user"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        addAndStop "content (object): contains clearances grouped by profiles"
                        add "content.validator (object) : contains the profile attributes (example of the validator profile)"
                        add "content.validator._type (string) : the type of the entity"
                        add "content.validator.id (number) : the id of the profile"
                        add "content.validator.name (string) : the name of the profile"
                        add "content.validator.type (string) : system if the profile is a system profile, custom if the profile is a custom profile"
                        addAndStop "content.validator.projects (array) : the list of projects linked to the profile"
                        addAndStop "_links (object) : related links"
                    }
                    _links {
                        add "self : the link to this user clearances"
                    }
                }
        ))
    }

    def "get-user-permissions"() {

        given:
        def project1 = SquashEntityBuilder.project {
            id = 367L
            name = "proj1"
            description = "<p>This project is the main sample project</p>"
            label = "Main Sample Project"
        }
        def project2 = SquashEntityBuilder.project {
            id = 368L
            name = "proj2"
            description = "<p>This project is the main sample project</p>"
            label = "Main Sample Project"
        }
        def project3 = SquashEntityBuilder.project {
            id = 369L
            name = "proj3"
            description = "<p>This project is the main sample project</p>"
            label = "Main Sample Project"
        }

        def user1 = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }


        def userPermissions = new RestUserPermission()

        userPermissions.userPermissions.put("test_designer", [project1, project2])
        userPermissions.userPermissions.put("validator", [project3])

        and:

        service.getAllPermissionsForGivenUser(486) >> userPermissions

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/users/{id}/permissions", 486).header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "content.test_designer".hasSize 2
            "content.test_designer".test {
                "[0]".test {
                    "_type".is "project"
                    "id".is 367
                    "name".is "proj1"
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/367"

                }
                "[1]".test {
                    "_type".is "project"
                    "id".is 368
                    "name".is "proj2"
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/368"

                }
            }

            "content.validator".hasSize 1
            "content.validator".test {
                "[0]".test {
                    "_type".is "project"
                    "id".is 369
                    "name".is "proj3"
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/369"

                }
            }

            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/users/486/permissions"
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the user"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    _links {
                        add "self : the link to this user clearances"
                    }
                }
        ))
    }

    def "add-new-clearance-to-user"() {
        given:

        def user1 = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        def group1 = SquashEntityBuilder.permissionGroup {
            id = 215L
            qualifiedName = "toto.TestRunner"
        }

        def group2 = SquashEntityBuilder.permissionGroup {
            id = 216L
            qualifiedName = "toto.Validator"
        }

        def group3 = SquashEntityBuilder.permissionGroup {
            id = 217L
            qualifiedName = "toto.TestManager"
        }

        def project3 = SquashEntityBuilder.project {
            id = 369L
            name = "proj3"
            description = "<p>This project is the main sample project</p>"
            label = "Main Sample Project"
        }

        def partyClearances = new RestPartyClearance()
        def testDesigner = new RestPartyProfileDto([project3] as List<GenericProject>,4L, "TestDesigner", true)
        partyClearances.partyClearances.put("test_designer", testDesigner)

        and:
        1 * service.addClearanceToUser(_,_,_)
        restProjectService.isGenericProjectExist(_) >> true
        restProjectService.findAllPossiblePermission() >> [group1, group2, group3]
        service.getAllClearancesForGivenUser(486) >> partyClearances

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/users/{userId}/clearances/{profileId}/projects/{projectIds}", 486, 9, 369)
                .header("Accept", "application/json"))

        then:
        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "content.test_designer".test {
                "_type".is "profile"
                "id".is 4
                "name".is "TestDesigner"
                "type".is "system"
                "projects".hasSize 1
                "projects".test {
                    "[0]".test {
                        "_type".is "project"
                        "id".is 369
                        "name".is "proj3"
                        selfRelIs "http://localhost:8080/api/rest/latest/projects/369"

                    }
                }
            }

            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/users/486/clearances"
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "userId : the id of the user"
                        add "profileId : the id of the profile"
                        add "projectIds : the ids of the projects"
                    }
                    fields {
                        addAndStop "content (object): contains the added clearance"
                        add "content.test_designer (object) : contains the profile attributes (example of the test designer profile)"
                        add "content.test_designer._type (string) : the type of the entity"
                        add "content.test_designer.id (number) : the id of the profile"
                        add "content.test_designer.name (string) : the name of the profile"
                        add "content.test_designer.type (string) : system if the profile is a system profile, custom if the profile is a custom profile"
                        addAndStop "content.test_designer.projects (array) : the list of projects linked to the profile"
                        addAndStop "_links (object) : related links"
                    }
                    _links {
                        add "self : the link to this user clearances"
                    }
                }
        ))
    }

    def "add-new-permission-to-user"() {
        given:

        def user1 = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        def group1 = SquashEntityBuilder.permissionGroup {
            id = 215L
            qualifiedName = "toto.TestRunner"
        }

        def group2 = SquashEntityBuilder.permissionGroup {
            id = 216L
            qualifiedName = "toto.TestDesigner"
        }

        def group3 = SquashEntityBuilder.permissionGroup {
            id = 217L
            qualifiedName = "toto.TestManager"
        }

        def project3 = SquashEntityBuilder.project {
            id = 369L
            name = "proj3"
            description = "<p>This project is the main sample project</p>"
            label = "Main Sample Project"
        }

        def userPermission = new RestUserPermission()
        userPermission.userPermissions.put("test_designer", [project3])


        and:
        1*service.addPermissionToUser(_,_,_)
        restProjectService.isGenericProjectExist(_) >>true
        restProjectService.findAllPossiblePermission() >> [group1, group2, group3]
        service.getAllPermissionsForGivenUser(486) >> userPermission

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/users/{userId}/permissions/{permissionGroup}?ids=369", 486, "test_designer")
                .header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "content.test_designer".hasSize 1
            "content.test_designer".test {
                "[0]".test {
                    "_type".is "project"
                    "id".is 369
                    "name".is "proj3"
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/369"

                }
            }

            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/users/486/permissions"
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "userId : the id of the user"
                        add "permissionGroup : the profile"
                    }
                    requestParams {
                        add "ids : the ids of the projects"
                    }
                    _links {
                        add "self : the link to this user clearances"
                    }
                }
        ))
    }

    def "remove-clearance-to-user"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/users/{userId}/clearances/{projectIds}", 486, "369,370")
                .header("Accept", "application/json"))

        then:
        1 * service.removePermissionToUser(_,_)

        res.andExpect(status().isNoContent())
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "userId : the id of the user"
                        add "projectIds: the ids of the project to remove"
                    }
                }
        ))
    }

    def "remove-permission-to-user"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/users/{id}/permissions", 486)
                .header("Accept", "application/json")
                .param("ids", "369"))


        then:
        1*service.removePermissionToUser(_,_)

        res.andExpect(status().isNoContent())
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the user"
                    }
                    requestParams {
                        add "ids : the ids of the projects"
                    }
                }
        ))
    }


}
