/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import org.squashtest.tm.domain.users.UsersGroup

def member1 = user{
    id = 1L
    firstName = "Charles"
    lastName = "Dupond"
    login = "User-1"
    email = "charlesdupond@aaaa.aa"
    active = true
    group UsersGroup.USER
    lastConnectedOn "2018/02/11"

}

def member2 = user{
    id = 2L
    firstName = "Chris"
    lastName = "Dupond"
    login = "User-2"
    email = "chrisdupond@aaaa.aa"
    active = true
    group UsersGroup.USER
    lastConnectedOn "2018/03/11"

}

def member3 = user{
    id = 3L
    firstName = "Victor"
    lastName = "Dupond"
    login = "User-3"
    email = "victordupond@aaaa.aa"
    active = true
    group UsersGroup.USER
    lastConnectedOn "2018/03/11"

}

def team1 = team {
    id = 888L
    name = "Team A"
    description = "<p>black panther</p>"
}

return [
        member1 : member1,
        member2 : member2,
        member3 : member3,
        team1 : team1
]



