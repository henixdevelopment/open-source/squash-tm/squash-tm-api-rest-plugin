/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDtoForExecution;
import org.squashtest.tm.plugin.rest.service.RestExploratoryExecutionService;
import org.squashtest.tm.plugin.rest.service.RestIssueService;
import org.squashtest.tm.plugin.rest.service.RestSessionNoteService;

import javax.inject.Inject;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@RestApiController(ExploratoryExecution.class)
@UseDefaultRestApiConfiguration
public class RestExploratoryExecutionController extends BaseRestController {
    public static final String EXPLO_EXECUTION_DYNAMIC_FILTER = "name,reference,execution_mode,progress_status,last_executed_by,last_executed_on,charter,task_division,session_notes[kind,content],reviewed,comment,custom_fields,test_case_custom_fields,attachments";

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private RestExploratoryExecutionService service;

    @Inject
    private RestIssueService restIssueService;

    @Inject
    private RestSessionNoteService sessionNoteService;

    @GetMapping(value ="/exploratory-executions/{id}")
    @EntityGetter({ExploratoryExecution.class})
    @ResponseBody
    @DynamicFilterExpression(EXPLO_EXECUTION_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<ExploratoryExecution>> findExploratoryExecution(@PathVariable("id") long id) {
        ExploratoryExecution exploratoryExecution = service.getOne(id);

        EntityModel<ExploratoryExecution> res = toEntityModel(exploratoryExecution);

        linksHelper.addAllLinksForExploratoryExecution(res);
        res.add(createRelationTo( "issues"));

        return ResponseEntity.ok(res);
    }

    @GetMapping("/exploratory-executions/{id}/issues")
    @ResponseBody
    @DynamicFilterExpression("execution-step[id]")
    public ResponseEntity<PagedModel<EntityModel<IssueDtoForExecution>>> findExecutionsIssues(@PathVariable("id") long id, Pageable pageable) {

        ExploratoryExecution exploratoryExecution = service.getOne(id);

        Page<IssueDto> pagedIssue = restIssueService.getIssuesFromExecutionIds(Collections.singletonList(exploratoryExecution.getId()), pageable);

        for (IssueDto issue : pagedIssue.getContent()) {
            List<SessionNote> sessionNotes = sessionNoteService.getSessionNotesFromIssue(issue.getRemoteIssueId(), id);
            issue.setSessionNotes(new HashSet<>(sessionNotes));
            issue.getExecutions().clear();
        }

        PagedModel<EntityModel<IssueDtoForExecution>> res = pageAssembler.toModel(pagedIssue);

        return ResponseEntity.ok(res);
    }
}
