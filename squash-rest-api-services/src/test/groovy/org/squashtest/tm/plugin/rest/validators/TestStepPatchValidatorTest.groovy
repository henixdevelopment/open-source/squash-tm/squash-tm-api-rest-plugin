/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators

import org.springframework.validation.Errors
import org.squashtest.tm.plugin.rest.jackson.model.ActionTestStepDto
import org.squashtest.tm.plugin.rest.jackson.model.KeywordTestStepDto
import org.squashtest.tm.plugin.rest.jackson.model.TestCaseDto
import org.squashtest.tm.plugin.rest.jackson.model.TestStepDto
import spock.lang.Specification

class TestStepPatchValidatorTest extends Specification{
    TestStepPatchValidator validator = new TestStepPatchValidator()

    def errors = Mock(Errors)

    def "should reject error on value testCase when patching TestCase DTO for a keyword Test Step DTO"(){
        given:
        TestStepDto testStepDto = new KeywordTestStepDto()
        testStepDto.setTestCaseDto(new TestCaseDto())

        when:
        validator.checkForbiddenPatchAttributes(errors, testStepDto)

        then:
        1*errors.rejectValue("testCase", "non patchable attribute", "Only attributes belonging to the test step itself can be modified. The attribute test case cannot be patched. Use direct url to the test case entity instead");
    }

    def "should reject error on value testCase when patching TestCase DTO for an Action Test Step DTO"(){
        given:
        TestStepDto testStepDto = new ActionTestStepDto()
        testStepDto.setTestCaseDto(new TestCaseDto())

        when:
        validator.checkForbiddenPatchAttributes(errors, testStepDto)

        then:
        1*errors.rejectValue("testCase", "non patchable attribute", "Only attributes belonging to the test step itself can be modified. The attribute test case cannot be patched. Use direct url to the test case entity instead");
    }
}
