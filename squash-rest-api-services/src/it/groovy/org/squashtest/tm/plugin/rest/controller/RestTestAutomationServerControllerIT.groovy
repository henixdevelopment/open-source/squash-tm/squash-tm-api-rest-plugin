/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.testautomation.TestAutomationServer
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestTestAutomationServerService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestTestAutomationServerController)
class RestTestAutomationServerControllerIT extends BaseControllerSpec {

    @Inject
    RestTestAutomationServerService serverService

    def "browse-test-automation-servers"() {
        given:
        serverService.findAllReadable(_) >> { args ->
            def taServer1 = SquashEntityBuilder.testAutomationServer {
                id = 569L
                name = "TA server 1"
                url = "http://1234:4567/jenkins/"
                kind = TestAutomationServerKind.jenkins
                manualSlaveSelection = false
                description = "<p>nice try</p>"
            }
            def taServer2 = SquashEntityBuilder.testAutomationServer {
                id = 654L
                name = "TA server 2"
                url = "http://127.0.0.1:7774"
                kind = TestAutomationServerKind.squashOrchestrator
                observerUrl = "http://127.0.0.1:7775"
                eventBusUrl = "http://127.0.0.1:38368"
                killswitchUrl = "http://127.0.0.1:7776"
                manualSlaveSelection = false
                description = "<p>second shot</p>"
            }

            new PageImpl<TestAutomationServer>([taServer1, taServer2], args[0], 4)
        }

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-automation-servers?size=2&page=1")
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.test-automation-servers".hasSize 2
            "_embedded.test-automation-servers".test {
                "[0]".test {
                    "_type".is "test-automation-server"
                    "id".is 569
                    "name".is "TA server 1"
                    "url".is "http://1234:4567/jenkins/"
                    "kind".is "jenkins"
                    "manual_agent_selection".is false
                    "description".is "<p>nice try</p>"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-automation-servers/569"
                }
                "[1]".test {
                    "_type".is "test-automation-server"
                    "id".is 654
                    "name".is "TA server 2"
                    "url".is "http://127.0.0.1:7774"
                    "kind".is "squashOrchestrator"
                    "observer_url".is "http://127.0.0.1:7775"
                    "event_bus_url".is "http://127.0.0.1:38368"
                    "killswitch_url".is "http://127.0.0.1:7776"
                    "manual_agent_selection".is false
                    "description".is "<p>second shot</p>"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-automation-servers/654"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/test-automation-servers?page=1&size=2"
            "page".test {
                "size".is 2
                "totalElements".is 4
                "totalPages".is 2
                "number".is 1
            }
        }

        res.andDo(doc.document(
                DocumentationSnippets.AllInOne.createBrowseAllEntities("test-automation-servers")
        ))
    }

    def "get-test-automation-server"() {

        given:
        def taServer = SquashEntityBuilder.testAutomationServer {
            id = 569L
            name = "TA server"
            kind = TestAutomationServerKind.squashOrchestrator
            url = "http://127.0.0.1:7774"
            observerUrl = "http://127.0.0.1:7775"
            eventBusUrl = "http://127.0.0.1:38368"
            killswitchUrl = "http://127.0.0.1:7776"
            manualSlaveSelection = false
            description = "<p>Orchestrator option</p>"
        }
        and:
        serverService.getOne(569) >> taServer
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-automation-servers/{id}", 569).header("Accept", "application/json"))
        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "test-automation-server"
            "id".is 569
            "name".is "TA server"
            "url".is "http://127.0.0.1:7774"
            "kind".is "squashOrchestrator"
            "observer_url".is "http://127.0.0.1:7775"
            "event_bus_url".is "http://127.0.0.1:38368"
            "killswitch_url".is "http://127.0.0.1:7776"
            "manual_agent_selection".is false
            "description".is "<p>Orchestrator option</p>"
            selfRelIs "http://localhost:8080/api/rest/latest/test-automation-servers/569"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
            documentationBuilder {

                pathParams {
                    add "id : the id of the test automation server"
                }

                requestParams {
                    add DocumentationSnippets.DescriptorLists.fieldsParams
                }

                fields {
                    add "id (number) : the id of the test automation server"
                    add "_type (string) : the type of the entity"
                    add "name (string) : the name of the test automation server"
                    add "url (string) : the url where to reach the test automation server"
                    add "kind (string) : the kind of the test automation server, can be jenkins (default) or squashOrchestrator"
                    add "observer_url (string) : the url for the observer endpoint, defaults is the receptionist url (for a Squash Orchestrator server only)"
                    add "event_bus_url (string) : the url for the event bus, defaults is the receptionist url (for a Squash Orchestrator server only)"
                    add "killswitch_url (string) : the url for the killswitch endpoint, defaults is the receptionist url (for a Squash Orchestrator server only)"
                    add "description (string) : the description of the test automation server"
                    add "manual_agent_selection (boolean) : whether the test automation server is manual agent or not"
                    add "created_by (string) : the user who created the test automation server"
                    add "created_on (string) : the date the test automation server was created"
                    add "last_modified_by (string) : the user who last modified the test automation server"
                    add "last_modified_on (string) : the date the test automation server was last modified"
                    add DocumentationSnippets.DescriptorLists.linksFields
                }

                _links {
                    add "self : link to this test automation server"
                }
            }
        ))


    }
}
