/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.campaign.IterationTestPlanItem
import org.squashtest.tm.domain.campaign.TestPlanStatus
import org.squashtest.tm.domain.execution.ExecutionStatus
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto
import org.squashtest.tm.plugin.rest.service.RestIssueService
import org.squashtest.tm.plugin.rest.service.RestTestSuiteService
import org.squashtest.tm.service.campaign.TestSuiteTestPlanManagerService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.internal.repository.IterationDao
import org.squashtest.tm.service.internal.repository.TestSuiteDao
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.campaign
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufValue
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.dataset
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.iteration
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.iterationTestPlanItem
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scriptedTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testSuite

/**
 * Created by jlor on 11/07/2017.
 */
@WebMvcTest
class RestTestSuiteControllerIT extends BaseControllerSpec {

    @Inject
    private RestTestSuiteService restTestSuiteService

    @Inject
    private CustomFieldValueFinderService cufService
    @Inject
    private IterationDao iterationDao
    @Inject
    private TestSuiteDao testSuiteDao
    @Inject
    private TestSuiteTestPlanManagerService testSuiteTestPlanManagerService

    @Inject
    private RestIssueService restIssueService


    def "get-test-suite"() {

        given:

        def mocks = loadMock("mocks/get-test-suite.groovy")

        def testPlanStatistics = SquashEntityBuilder.testPlanStatisticValue{
            nbTestCases = 0
            progression = 0
            status = TestPlanStatus.READY
            nbDone = 0
            statisticValues = null
        }

        and:
        restTestSuiteService.getOne(9) >> mocks.testSuite
        cufService.findAllCustomFieldValues(mocks.testSuite) >> mocks.cufs
        restTestSuiteService.getTestSuiteStatisticsByTestSuiteIds(9) >> testPlanStatistics

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-suites/{id}", 9).header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            // Simple attributes
            "_type".is "test-suite"
            "id".is 9
            "name".is "sample test suite"
            "description".is "<p>this is a sample test suite</p>"
            "uuid".is "2f7198zd-eb2e-4379-f82d-ddc207c866bd"
            "status".is "READY"
            "progress_status".is "READY"
            "created_by".is "admin"
            "created_on".is "2017-07-12T10:00:00.000+00:00"
            "last_modified_by".is "admin"
            "last_modified_on".is "2017-07-12T10:00:00.000+00:00"

            // Custom Fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CF_TXT"
                    "label".is "cuf text"
                    "value".is "the_value"
                }
                "[1]".test {
                    "code".is "CF_TAG"
                    "label".is "cuf tag"
                    "value".contains "tag_1", "tag_2"
                }
            }

            // Test plan
            "test_plan".hasSize 3
            "test_plan".test {
                "[0]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 80
                    "execution_status".is "READY"
                    "referenced_test_case".test {
                        "_type".is "test-case"
                        "id".is 90
                        "name".is "first test case"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/90"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 5
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/5"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/80"
                }
                "[1]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 41
                    "execution_status".is "SUCCESS"
                    "referenced_test_case".test {
                        "_type".is "scripted-test-case"
                        "id".is 91
                        "name".is "scripted test case 2"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/91"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 9
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/9"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/41"
                }
                "[2]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 95
                    "execution_status".is "SUCCESS"
                    "referenced_test_case".test {
                        "_type".is "keyword-test-case"
                        "id".is 105
                        "name".is "keyword test case 3"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/105"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 18
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/18"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/95"
                }
            }

            /*
            // Node hierarchy infos
            "iteration".test {
                "_type".is "iteration"
                "id".is 101
                "name".is "second iteration"
            }*/

            "parent".test {
                "_type".is "iteration"
                "id".is 101
                "name".is "second iteration"
                selfRelIs "http://localhost:8080/api/rest/latest/iterations/101"
            }

            selfRelIs "http://localhost:8080/api/rest/latest/test-suites/9"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/15"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/101"
                "test-plan.href".is "http://localhost:8080/api/rest/latest/test-suites/9/test-plan"
                "issues.href".is "http://localhost:8080/api/rest/latest/test-suites/9/issues"
            }

        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {

                    pathParams {
                        add "id : the id of the test suite"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (number) : the id of the test suite"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the test suite"
                        add "description (string) : the description of the test suite"
                        add "uuid (string) : the uuid of the test suite"
                        add "status (string) : the status of the test suite"
                        add "progress_status (string) : the value of the progress status, automatically determined based on the progress of test execution within the scope"
                        addAndStop "parent (object) : the parent entity of the test suite"
                        add "created_by (string) : the user who created the test suite"
                        add "created_on (string) : the date the test suite was created"
                        add "last_modified_by (string) : the user who last modified the test suite"
                        add "last_modified_on (string) : the date the test suite was last modified"
                        addAndStop "custom_fields (array) : the custom fields of the test suite"
                        addAndStop "test_plan (array) : the iteration test plan items of the test suite"
                        add "attachments (array) : the attachments of the test suite"
                        addAndStop "_links (object) : related links"
                    }

                    _links {
                        add "self : link to this test suite"
                        add "project : link to the project the test suite belongs to"
                        add "iteration : link to the iteration the test suite belongs to"
                        add "test-plan : link the test plan of the test suite"
                        add "attachments : link the test plan of the test suite"
                        add "issues : link the issues of the test suite"
                    }
                }
        ))
    }

    def "get-test-suite-test-plan"() {

        given:

        def mocks = loadMock("mocks/get-test-suite-test-plan.groovy")

        and:

        restTestSuiteService.findTestPlan(44, _) >> { args ->
            new PageImpl<IterationTestPlanItem>([mocks.itpi1,
                                                 mocks.itpi2,
                                                 mocks.itpi3], args[1], 2)
        }

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/test-suites/{id}/test-plan", 44).header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.test-plan".hasSize 3
            "_embedded.test-plan".test {
                "[0]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 1
                    "execution_status".is "BLOCKED"
                    "referenced_test_case".test {
                        "_type".is "test-case"
                        "id".is 1
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/1"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 1
                        "name".is "sample dataset 1"
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/1"
                    }
                    "last_executed_by".is "User-1"
                    "last_executed_on".is "2017-07-13T10:00:00.000+00:00"
                    "assigned_to".is "User-1"
                    "executions".hasSize 2
                    "executions".test {
                        "[0]".test {
                            "_type".is "execution"
                            "id".is 11
                            "execution_status".is "FAILURE"
                            "last_executed_by".is "User-1"
                            "last_executed_on".is "2017-07-13T10:00:00.000+00:00"
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/11"
                        }
                        "[1]".test {
                            "_type".is "execution"
                            "id".is 21
                            "execution_status".is "BLOCKED"
                            "last_executed_by".is "User-1"
                            "last_executed_on".is "2017-07-13T10:00:00.000+00:00"
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/21"
                        }
                    }
                    "iteration".test {
                        "_type".is "iteration"
                        "id".is 1
                        "name".is "sample iteration 1"
                        "reference".is "IT1"
                        selfRelIs "http://localhost:8080/api/rest/latest/iterations/1"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/1"
                }
                "[1]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 2
                    "execution_status".is "SUCCESS"
                    "referenced_test_case".test {
                        "_type".is "scripted-test-case"
                        "id".is 2
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/2"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 2
                        "name".is "sample dataset 2"
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/2"
                    }
                    "last_executed_by".is "User-2"
                    "last_executed_on".is "2017-07-15T10:00:00.000+00:00"
                    "assigned_to".is "User-2"
                    "executions".hasSize 3
                    "executions".test {
                        "[0]".test {
                            "_type".is "scripted-execution"
                            "id".is 12
                            "execution_status".is "SUCCESS"
                            "last_executed_by".is "User-2"
                            "last_executed_on".is "2017-07-07T10:00:00.000+00:00"
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/12"
                        }
                        "[1]".test {
                            "_type".is "scripted-execution"
                            "id".is 22
                            "execution_status".is "BLOCKED"
                            "last_executed_by".is "User-2"
                            "last_executed_on".is "2017-07-15T10:00:00.000+00:00"
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/22"
                        }
                        "[2]".test {
                            "_type".is "scripted-execution"
                            "id".is 32
                            "execution_status".is "RUNNING"
                            "last_executed_by".is "User-2"
                            "last_executed_on".is "2017-07-20T10:00:00.000+00:00"
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/32"
                        }
                    }
                    "iteration".test {
                        "_type".is "iteration"
                        "id".is 2
                        "name".is "sample iteration 2"
                        "reference".is "IT2"
                        selfRelIs "http://localhost:8080/api/rest/latest/iterations/2"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/2"
                }
                "[2]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 3
                    "execution_status".is "SUCCESS"
                    "referenced_test_case".test {
                        "_type".is "keyword-test-case"
                        "id".is 3
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/3"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 3
                        "name".is "sample dataset 3"
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/3"
                    }
                    "last_executed_by".is "User-3"
                    "last_executed_on".is "2020-04-15T10:00:00.000+00:00"
                    "assigned_to".is "User-3"
                    "executions".hasSize 3
                    "executions".test {
                        "[0]".test {
                            "_type".is "keyword-execution"
                            "id".is 13
                            "execution_status".is "SUCCESS"
                            "last_executed_by".is "User-3"
                            "last_executed_on".is "2020-04-15T10:00:00.000+00:00"
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/13"
                        }
                        "[1]".test {
                            "_type".is "keyword-execution"
                            "id".is 23
                            "execution_status".is "BLOCKED"
                            "last_executed_by".is "User-3"
                            "last_executed_on".is "2020-04-15T10:00:00.000+00:00"
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/23"
                        }
                        "[2]".test {
                            "_type".is "keyword-execution"
                            "id".is 33
                            "execution_status".is "RUNNING"
                            "last_executed_by".is "User-3"
                            "last_executed_on".is "2020-04-15T10:00:00.000+00:00"
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/33"
                        }
                    }
                    "iteration".test {
                        "_type".is "iteration"
                        "id".is 2
                        "name".is "sample iteration 2"
                        "reference".is "IT2"
                        selfRelIs "http://localhost:8080/api/rest/latest/iterations/2"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/3"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/test-suites/44/test-plan?page=0&size=20"

            "page".test {
                "size".is 20
                "totalElements".is 3
                "totalPages".is 1
                "number".is 0
            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test suite"
                    }
                    requestParams {
                        add DescriptorLists.paginationParams
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "test-plan (array) : the iteration test plan items of this test suite"
                        add DescriptorLists.paginationFields
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add DescriptorLists.paginationLinks
                    }
                }
        ))


    }

    def "post-test-suite"() {
        given:
        def json = """{
                        "_type": "test-suite",
                        "name": "sample test suite",
                        "description":"<p>this is a sample test suite</p>",
                        "parent": {
                            "_type": "iteration",
                            "id": 101
                        },
                         "custom_fields": [
                            {
                                "code": "CF_TXT",
                                "label": "cuf text",
                                "value": "the_value"
                            }
                        ],
                        "test_plan": [
                            {
                            "_type": "iteration-test-plan-item",
                                     "id": 80
                            },
                            {
                            "_type": "iteration-test-plan-item",
                                      "id": 41
                            }
                           ]
                        }"""

        def cufs = [
                cufValue {
                    label = "cuf text"
                    inputType "PLAIN_TEXT"
                    code = "CF_TXT"
                    value = "the_value"
                },
                cufValue {
                    label = "cuf tag"
                    inputType "TAG"
                    code = "CF_TAG"
                    value = ["tag_1", "tag_2"]
                }
        ]
        def cpg = campaign {
            id = 87l
            name = "sample campaign"
            project = project {
                id = 15l
                name = "sample project"
            }
        }
        def itr = iteration {
            id = 101l
            name = "second iteration"
            campaign = cpg
        }

        def testSuite = testSuite {

            id = 9l
            name = "sample test suite"
            description = "<p>this is a sample test suite</p>"
            executionStatus "READY"
            createdBy "admin"
            createdOn "2017/07/12"
            lastModifiedBy "admin"
            lastModifiedOn "2017/07/12"
            customFields = cufs
            iteration = itr
            testPlan = [
                    iterationTestPlanItem {
                        id = 80l
                        executionStatus = "READY" as ExecutionStatus
                        referencedTestCase = testCase {
                            id = 90L
                            name = "first test case"
                        }
                        referencedDataset = dataset {
                            id = 5l
                        }
                    },
                    iterationTestPlanItem {
                        id = 41l
                        executionStatus = "SUCCESS" as ExecutionStatus
                        referencedTestCase = scriptedTestCase {
                            id = 91L
                            name = "scripted test case 2"
                        }
                        referencedDataset = dataset {
                            id = 9L
                        }
                    },
                    iterationTestPlanItem {
                        id = 81l
                        executionStatus = "SUCCESS" as ExecutionStatus
                        referencedTestCase = keywordTestCase {
                            id = 99L
                            name = "keyword test case 3"
                        }
                        referencedDataset = dataset {
                            id = 19L
                        }
                    }
            ]
        }

        def testPlanStatistics = SquashEntityBuilder.testPlanStatisticValue{
            nbTestCases = 0
            progression = 0
            status = TestPlanStatus.READY
            nbDone = 0
            statisticValues = null
        }

        and:
        iterationDao.findById(_) >> itr
        cufService.findAllCustomFieldValues(_) >> cufs
        restTestSuiteService.addTestSuite(_, _) >> testSuite
        restTestSuiteService.getTestSuiteStatisticsByTestSuiteIds(9) >> testPlanStatistics

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-suites")
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
        withResult(res) {

            // Simple attributes
            "_type".is "test-suite"
            "id".is 9
            "name".is "sample test suite"
            "description".is "<p>this is a sample test suite</p>"
            "status".is "READY"
            "progress_status".is "READY"
            "parent".test {
                "_type".is "iteration"
                "id".is 101
                "name".is "second iteration"
                "_links".test {
                    "self.href".is "http://localhost:8080/api/rest/latest/iterations/101"
                }
            }
            "created_by".is "admin"
            "created_on".is "2017-07-12T10:00:00.000+00:00"
            "last_modified_by".is "admin"
            "last_modified_on".is "2017-07-12T10:00:00.000+00:00"

            // Custom Fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CF_TXT"
                    "label".is "cuf text"
                    "value".is "the_value"
                }
                "[1]".test {
                    "code".is "CF_TAG"
                    "label".is "cuf tag"
                    "value".contains "tag_1", "tag_2"
                }
            }

            // Test plan
            "test_plan".hasSize 3
            "test_plan".test {
                "[0]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 80
                    "execution_status".is "READY"
                    "referenced_test_case".test {
                        "_type".is "test-case"
                        "id".is 90
                        "name".is "first test case"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/90"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 5
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/5"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/80"
                }
                "[1]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 41
                    "execution_status".is "SUCCESS"
                    "referenced_test_case".test {
                        "_type".is "scripted-test-case"
                        "id".is 91
                        "name".is "scripted test case 2"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/91"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 9
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/9"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/41"
                }
                "[2]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 81
                    "execution_status".is "SUCCESS"
                    "referenced_test_case".test {
                        "_type".is "keyword-test-case"
                        "id".is 99
                        "name".is "keyword test case 3"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/99"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 19
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/19"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/81"
                }
            }

            selfRelIs "http://localhost:8080/api/rest/latest/test-suites/9"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/15"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/101"
                "test-plan.href".is "http://localhost:8080/api/rest/latest/test-suites/9/test-plan"
            }

        }
        res.andDo(doc.document(
                documentationBuilder {

                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the iteration"
                        add "description (string) : the description of the test suite"
                        addAndStop "parent (object) : the parent iteration of this test suite"
                        addAndStop "test_plan (array) : the iteration test plan items of the test suite (optional)"
                        addAndStop "custom_fields (array) : the custom fields of this iteration"
                    }

                }
        ))
    }

    def "patch-test-suite"() {
        given:
        def json = """{
                        "_type": "test-suite",
                        "description": "<p>modified description sample test suite</p>",
                        "status": "SUCCESS",
                        "custom_fields": [
                            {
                                "code": "CF_TXT",
                                "label": "cuf text",
                                "value": "the_new_value"
                            }
                        ]
                    }"""

        def cufs = [
                cufValue {
                    label = "cuf text"
                    inputType "PLAIN_TEXT"
                    code = "CF_TXT"
                    value = "the_new_value"
                },
                cufValue {
                    label = "cuf tag"
                    inputType "TAG"
                    code = "CF_TAG"
                    value = ["tag_1", "tag_2"]
                }
        ]
        def cpg = campaign {
            id = 87l
            name = "sample campaign"
            project = project {
                id = 15l
                name = "sample project"
            }
        }
        def itr = iteration {
            id = 101l
            name = "second iteration"
            campaign = cpg
        }

        def testSuite = testSuite {

            id = 9l
            name = "sample test suite"
            description = "<p>modified description sample test suite</p>"
            executionStatus "SUCCESS"
            createdBy "admin"
            createdOn "2017/07/12"
            lastModifiedBy "admin"
            lastModifiedOn "2017/07/12"
            customFields = cufs
            iteration = itr
            testPlan = [
                    iterationTestPlanItem {
                        id = 80l
                        executionStatus = "READY" as ExecutionStatus
                        referencedTestCase = testCase {
                            id = 90L
                            name = "first test case"
                        }
                        referencedDataset = dataset {
                            id = 5l
                        }
                    },
                    iterationTestPlanItem {
                        id = 41l
                        executionStatus = "SUCCESS" as ExecutionStatus
                        referencedTestCase = scriptedTestCase {
                            id = 91L
                            name = "scripted test case 2"
                        }
                        referencedDataset = dataset {
                            id = 9L
                        }
                    },
                    iterationTestPlanItem {
                        id = 91l
                        executionStatus = "SUCCESS" as ExecutionStatus
                        referencedTestCase = keywordTestCase {
                            id = 191L
                            name = "keyword test case 3"
                        }
                        referencedDataset = dataset {
                            id = 29L
                        }
                    }
            ]
        }

        def testPlanStatistics = SquashEntityBuilder.testPlanStatisticValue{
            nbTestCases = 0
            progression = 0
            status = TestPlanStatus.READY
            nbDone = 0
            statisticValues = null
        }

        and:
        cufService.findAllCustomFieldValues(_) >> cufs
        restTestSuiteService.modifyTestSuite(_, _) >> testSuite
        restTestSuiteService.getTestSuiteStatisticsByTestSuiteIds(9) >> testPlanStatistics

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/test-suites/{id}", 9l)
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
        withResult(res) {
            // Simple attributes
            "_type".is "test-suite"
            "id".is 9
            "name".is "sample test suite"
            "description".is "<p>modified description sample test suite</p>"
            "status".is "SUCCESS"
            "progress_status".is "READY"
            "parent".test {
                "_type".is "iteration"
                "id".is 101
                "name".is "second iteration"
                "_links".test {
                    "self.href".is "http://localhost:8080/api/rest/latest/iterations/101"
                }
            }
            "created_by".is "admin"
            "created_on".is "2017-07-12T10:00:00.000+00:00"
            "last_modified_by".is "admin"
            "last_modified_on".is "2017-07-12T10:00:00.000+00:00"

            // Custom Fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CF_TXT"
                    "label".is "cuf text"
                    "value".is "the_new_value"
                }
                "[1]".test {
                    "code".is "CF_TAG"
                    "label".is "cuf tag"
                    "value".contains "tag_1", "tag_2"
                }
            }

            // Test plan
            "test_plan".hasSize 3
            "test_plan".test {
                "[0]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 80
                    "execution_status".is "READY"
                    "referenced_test_case".test {
                        "_type".is "test-case"
                        "id".is 90
                        "name".is "first test case"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/90"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 5
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/5"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/80"
                }
                "[1]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 41
                    "execution_status".is "SUCCESS"
                    "referenced_test_case".test {
                        "_type".is "scripted-test-case"
                        "id".is 91
                        "name".is "scripted test case 2"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/91"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 9
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/9"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/41"
                }
                "[2]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 91
                    "execution_status".is "SUCCESS"
                    "referenced_test_case".test {
                        "_type".is "keyword-test-case"
                        "id".is 191
                        "name".is "keyword test case 3"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/191"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 29
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/29"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/91"
                }
            }

            selfRelIs "http://localhost:8080/api/rest/latest/test-suites/9"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/15"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/101"
                "test-plan.href".is "http://localhost:8080/api/rest/latest/test-suites/9/test-plan"
            }

        }
        res.andDo(doc.document(
                documentationBuilder {

                    pathParams {
                        add "id : the id of the test suite"
                    }
                }
        ))

    }

    def "delete-test-suite"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest//test-suites/{ids}", "169,189")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * restTestSuiteService.deleteTestSuite(_)

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the test suites"
                    }
                }
        ))
    }

    def "detach-item-from-test-suite"() {
        given:
        def cufs = [
                cufValue {
                    label = "cuf text"
                    inputType "PLAIN_TEXT"
                    code = "CF_TXT"
                    value = "the_new_value"
                },
                cufValue {
                    label = "cuf tag"
                    inputType "TAG"
                    code = "CF_TAG"
                    value = ["tag_1", "tag_2"]
                }
        ]
        def cpg = campaign {
            id = 87l
            name = "sample campaign"
            project = project {
                id = 15l
                name = "sample project"
            }
        }
        def itr = iteration {
            id = 101l
            name = "second iteration"
            campaign = cpg
        }

        def testSuite = testSuite {

            id = 9l
            name = "sample test suite"
            description = "<p>modified description sample test suite</p>"
            executionStatus "SUCCESS"
            createdBy "admin"
            createdOn "2017/07/12"
            lastModifiedBy "admin"
            lastModifiedOn "2017/07/12"
            customFields = cufs
            iteration = itr
            testPlan = [
                    iterationTestPlanItem {
                        id = 80l
                        executionStatus = "READY" as ExecutionStatus
                        referencedTestCase = testCase {
                            id = 90L
                            name = "first test case"
                        }
                        referencedDataset = dataset {
                            id = 5l
                        }
                    },
                    iterationTestPlanItem {
                        id = 81l
                        executionStatus = "READY" as ExecutionStatus
                        referencedTestCase = scriptedTestCase {
                            id = 91L
                            name = "scripted test case"
                        }
                        referencedDataset = dataset {
                            id = 6l
                        }
                    },
                    iterationTestPlanItem {
                        id = 82l
                        executionStatus = "READY" as ExecutionStatus
                        referencedTestCase = keywordTestCase {
                            id = 92L
                            name = "keyword test case"
                        }
                        referencedDataset = dataset {
                            id = 7l
                        }
                    }
            ]
        }

        def testPlanStatistics = SquashEntityBuilder.testPlanStatisticValue{
            nbTestCases = 0
            progression = 0
            status = TestPlanStatus.READY
            nbDone = 0
            statisticValues = null
        }

        and:
        cufService.findAllCustomFieldValues(_) >> cufs
        restTestSuiteService.getTestSuiteStatisticsByTestSuiteIds(9) >> testPlanStatistics
        restTestSuiteService.detachTestPlanFromTestSuite(_, _) >> testSuite

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/test-suites/{testSuiteId}/test-plan/{ids}", 9L, "40,41")
                .accept("application/json")
                .contentType("application/json"))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            // Simple attributes
            "_type".is "test-suite"
            "id".is 9
            "name".is "sample test suite"
            "description".is "<p>modified description sample test suite</p>"
            "status".is "SUCCESS"
            "progress_status".is "READY"
            "parent".test {
                "_type".is "iteration"
                "id".is 101
                "name".is "second iteration"
                "_links".test {
                    "self.href".is "http://localhost:8080/api/rest/latest/iterations/101"
                }
            }
            "created_by".is "admin"
            "created_on".is "2017-07-12T10:00:00.000+00:00"
            "last_modified_by".is "admin"
            "last_modified_on".is "2017-07-12T10:00:00.000+00:00"

            // Custom Fields
            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CF_TXT"
                    "label".is "cuf text"
                    "value".is "the_new_value"
                }
                "[1]".test {
                    "code".is "CF_TAG"
                    "label".is "cuf tag"
                    "value".contains "tag_1", "tag_2"
                }
            }

            // Test plan
            "test_plan".hasSize 3
            "test_plan".test {
                "[0]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 80
                    "execution_status".is "READY"
                    "referenced_test_case".test {
                        "_type".is "test-case"
                        "id".is 90
                        "name".is "first test case"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/90"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 5
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/5"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/80"
                }
                "[1]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 81
                    "execution_status".is "READY"
                    "referenced_test_case".test {
                        "_type".is "scripted-test-case"
                        "id".is 91
                        "name".is "scripted test case"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/91"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 6
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/6"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/81"
                }
                "[2]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 82
                    "execution_status".is "READY"
                    "referenced_test_case".test {
                        "_type".is "keyword-test-case"
                        "id".is 92
                        "name".is "keyword test case"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/92"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 7
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/7"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/82"
                }
            }

            selfRelIs "http://localhost:8080/api/rest/latest/test-suites/9"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/15"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/101"
                "test-plan.href".is "http://localhost:8080/api/rest/latest/test-suites/9/test-plan"
            }

        }
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "testSuiteId : the id of the test suite"
                        add "ids : the id(s) of the test plan item"
                    }
                }
        ))
    }

    def "attach-item-from-test-suite"() {
        given:
        def cpg = campaign {
            id = 87l
            name = "sample campaign"
            project = project {
                id = 15l
                name = "sample project"
            }
        }
        def itr = iteration {
            id = 101l
            name = "first iteration"
            campaign = cpg
        }
        def testSuite = testSuite {

            id = 9L
            name = "sample test suite"
            description = "<p>this is a sample test suite</p>"
            executionStatus "SUCCESS"
            createdBy "admin"
            createdOn "2017/07/12"
            lastModifiedBy "admin"
            lastModifiedOn "2017/07/12"
            iteration = itr
            testPlan = [
                    iterationTestPlanItem {
                        id = 80L
                        executionStatus = "READY" as ExecutionStatus
                        referencedTestCase = testCase {
                            id = 90L
                            name = "first test case"
                        }
                        referencedDataset = dataset {
                            id = 5L
                        }
                    },
                    iterationTestPlanItem {
                        id = 81L
                        executionStatus = "SUCCESS" as ExecutionStatus
                        referencedTestCase = scriptedTestCase {
                            id = 91L
                            name = "scripted test case"
                        }
                        referencedDataset = dataset {
                            id = 6L
                        }
                    },
                    iterationTestPlanItem {
                        id = 82L
                        executionStatus = "SUCCESS" as ExecutionStatus
                        referencedTestCase = keywordTestCase {
                            id = 92L
                            name = "keyword test case"
                        }
                        referencedDataset = dataset {
                            id = 7L
                        }
                    }
            ]
        }

        def testPlanStatistics = SquashEntityBuilder.testPlanStatisticValue{
            nbTestCases = 0
            progression = 0
            status = TestPlanStatus.READY
            nbDone = 0
            statisticValues = null
        }


        and:
        cufService.findAllCustomFieldValues(_) >> []
        restTestSuiteService.attachTestPlanToTestSuite(_, _) >> testSuite
        restTestSuiteService.getTestSuiteStatisticsByTestSuiteIds(9) >> testPlanStatistics

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/test-suites/{testSuiteId}/test-plan/{ids}", 9L, "80,81")
                .header("Accept", "application/json"))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
        withResult(res) {

            // Simple attributes
            "_type".is "test-suite"
            "id".is 9
            "name".is "sample test suite"
            "description".is "<p>this is a sample test suite</p>"
            "status".is "SUCCESS"
            "progress_status".is "READY"
            "parent".test {
                "_type".is "iteration"
                "id".is 101
                "name".is "first iteration"
                "_links".test {
                    "self.href".is "http://localhost:8080/api/rest/latest/iterations/101"
                }
            }
            "created_by".is "admin"
            "created_on".is "2017-07-12T10:00:00.000+00:00"
            "last_modified_by".is "admin"
            "last_modified_on".is "2017-07-12T10:00:00.000+00:00"

            // Custom Fields
            "custom_fields".hasSize 0
            // Test plan
            "test_plan".hasSize 3
            "test_plan".test {
                "[0]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 80
                    "execution_status".is "READY"
                    "referenced_test_case".test {
                        "_type".is "test-case"
                        "id".is 90
                        "name".is "first test case"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/90"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 5
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/5"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/80"
                }
                "[1]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 81
                    "execution_status".is "SUCCESS"
                    "referenced_test_case".test {
                        "_type".is "scripted-test-case"
                        "id".is 91
                        "name".is "scripted test case"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/91"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 6
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/6"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/81"
                }
                "[2]".test {
                    "_type".is "iteration-test-plan-item"
                    "id".is 82
                    "execution_status".is "SUCCESS"
                    "referenced_test_case".test {
                        "_type".is "keyword-test-case"
                        "id".is 92
                        "name".is "keyword test case"
                        selfRelIs "http://localhost:8080/api/rest/latest/test-cases/92"
                    }
                    "referenced_dataset".test {
                        "_type".is "dataset"
                        "id".is 7
                        selfRelIs "http://localhost:8080/api/rest/latest/datasets/7"
                    }
                    selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/82"
                }
            }

            selfRelIs "http://localhost:8080/api/rest/latest/test-suites/9"
            "_links".test {
                "project.href".is "http://localhost:8080/api/rest/latest/projects/15"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/101"
                "test-plan.href".is "http://localhost:8080/api/rest/latest/test-suites/9/test-plan"
            }

        }
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "testSuiteId : the id of the test suite"
                        add "ids : the id(s) of the test plan item"
                    }
                }
        ))

    }

    def "get-test-suite-issues"() {

        given:

        def executions =  [
            SquashEntityBuilder.execution {
                id = 2L
                executionStatus "BLOCKED"
                lastExecutedBy = "User-1"
                lastExecutedOn "2017/06/24"
            }] as Set


        def issue1 = new IssueDto(
            "165",
            new URL ("http://192.175.1.51/bugzilla/show_bug.cgi?id=165"),
            executions
        )

        def issues = new PageImpl<IssueDto>([issue1], PageRequest.of(0, 20), 1)


        def executionIds = [2L]

        and:
        restTestSuiteService.getExecutionIdsByTestSuite(_) >> executionIds
        restIssueService.getIssuesFromExecutionIds(_,_) >> issues


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders
            .get("/api/rest/latest/test-suites/{id}/issues?page=0&size=20", 1)
            .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.issues".hasSize 1
            "_embedded.issues".test {
                "[0]".test {
                    "remoteIssueId".is "165"
                    "url".is "http://192.175.1.51/bugzilla/show_bug.cgi?id=165"
                    "executions".hasSize 1
                    "executions".test {
                        "[0]".test {
                            "_type".is "execution"
                            "id".is 2
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/2"
                        }
                    }
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/test-suites/1/issues?page=0&size=20"
            "page".test {
                "size".is 20
                "totalElements".is 1
                "totalPages".is 1
                "number".is 0
            }

        }


        /*
     * Documentation
     */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the test suite"
                }
                requestParams {
                    add DescriptorLists.paginationParams
                    add DescriptorLists.fieldsParams
                }
                fields {
                    embeddedAndStop "issues (array) : the issues of this test suite"
                    add DescriptorLists.issuesFields
                    add DescriptorLists.paginationFields
                    add DescriptorLists.linksFields
                }
                _links {
                    add DescriptorLists.paginationLinks
                }
            }
        ))
    }

}
