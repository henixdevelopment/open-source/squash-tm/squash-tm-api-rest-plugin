/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;

import java.util.ArrayList;
import java.util.List;



@SuppressWarnings("serial")

@JsonSerialize(contentConverter=HateoasWrapperConverter.class)
public class RestParameterList extends ArrayList<Parameter> {

	public RestParameterList(){
		super();
	}

	public RestParameterList(List<Parameter> parameters){
		super();
		for (Parameter parameter : parameters) {
			add(parameter);
		}
	}

} 
