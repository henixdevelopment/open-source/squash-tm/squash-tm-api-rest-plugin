/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.users.Party;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TypePropertyWriter;

import java.util.List;


@JsonTypeName("profile")
@JsonPropertyOrder({"_type", "id", "name", "type", "users", "projects"})
@JsonAppend(prepend = true, props = @JsonAppend.Prop(name = "_type", value = TypePropertyWriter.class))
public class RestPartyProfileDto {
    private long id;

    private String name;

    private String type;

    @JsonSerialize(contentConverter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Party> users;

    @JsonSerialize(contentConverter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<GenericProject> projects;

    public RestPartyProfileDto(long id, String name, boolean isSystem, List<Party> users) {
        this(id, name, isSystem);
        this.users = users;
    }

    public RestPartyProfileDto(List<GenericProject> projects, long id, String name, boolean isSystem) {
        this(id, name, isSystem);
        this.projects = projects;
    }

    private RestPartyProfileDto(long id, String name, boolean isSystem) {
        this.id = id;
        this.name = name;
        this.type = isSystem ? "system" : "custom";
    }

    public List<Party> getUsers() {
        return users;
    }

    public void setUsers(List<Party> users) {
        this.users = users;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<GenericProject> getProjects() {
        return projects;
    }

    public void setProjects(List<GenericProject> projects) {
        this.projects = projects;
    }
}
