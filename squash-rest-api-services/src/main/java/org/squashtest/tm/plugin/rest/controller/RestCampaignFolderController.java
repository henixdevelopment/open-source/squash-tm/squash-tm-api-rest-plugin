/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.ContentInclusion;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignFolderDto;
import org.squashtest.tm.plugin.rest.jackson.model.FolderTreeDto;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.jackson.model.ParentFolderTreeDto;
import org.squashtest.tm.plugin.rest.jackson.model.ProjectTreeDto;
import org.squashtest.tm.plugin.rest.service.RestCampaignFolderService;
import org.squashtest.tm.plugin.rest.service.RestIssueService;
import org.squashtest.tm.plugin.rest.service.RestProjectService;
import org.squashtest.tm.plugin.rest.validators.CampaignFolderPatchValidator;
import org.squashtest.tm.plugin.rest.validators.CampaignFolderPostValidator;

import javax.inject.Inject;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(CampaignFolder.class)
@UseDefaultRestApiConfiguration
public class RestCampaignFolderController extends BaseRestController {

    private static final String CAMPAIGN_FOLDER_TYPE = "campaign-folder";

    @Inject
    private RestCampaignFolderService service;
    @Inject
    private CampaignFolderPostValidator campaignFolderPostValidator;
    @Inject
    private CampaignFolderPatchValidator campaignFolderPatchValidator;
    @Inject
    private ResourceLinksHelper linksHelper;
    @Inject
    private RestProjectService projectService;

    @Inject
    private RestIssueService restIssueService;


    @GetMapping("/campaign-folders")
    @ResponseBody
    @DynamicFilterExpression("name")
    public ResponseEntity<PagedModel<EntityModel<CampaignFolder>>> findAllReadableTestCaseFolders(Pageable pageable) {

        Page<CampaignFolder> folders = service.findAllReadable(pageable);

        PagedModel<EntityModel<CampaignFolder>> res = toPagedModel(folders);

        return ResponseEntity.ok(res);

    }

    @GetMapping("/campaign-folders/tree/{projectIds}")
    @ResponseBody
    @DynamicFilterExpression("name")
    public ResponseEntity<List<ProjectTreeDto>> findCampaignFolderByProject(@PathVariable("projectIds") List<Long> projectIds) {
        List<Long> alreadyAddedIds = new ArrayList<>();

        List<Long> readableProjectIds = projectService.getReadableProjectIdsOnCampaignLibrary(projectIds);
        List<CampaignFolder> folders = service.findCampaignFolderByProject(readableProjectIds);

        List<ParentFolderTreeDto> tree = getCampaignFolderTree(folders, alreadyAddedIds);
        List<ProjectTreeDto> projTree = buildProjectTreeFromParentFolders(tree, readableProjectIds);

        return ResponseEntity.ok(projTree);

    }


    @GetMapping(value = "/campaign-folders/{id}")
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
    public ResponseEntity<EntityModel<CampaignFolder>> findCampaignFolder(@PathVariable("id") long id) {

        CampaignFolder campaignFolder = service.getOne(id);

        EntityModel<CampaignFolder> res = toEntityModel(campaignFolder);

        res.add(createLinkTo(campaignFolder.getProject()));
        res.add(createRelationTo("content"));
        res.add(createRelationTo("attachments"));
        res.add(createRelationTo("issues"));


        return ResponseEntity.ok(res);
    }


    @GetMapping("/campaign-folders/{id}/content")
    @ResponseBody
    @DynamicFilterExpression("name,reference")
    public ResponseEntity<PagedModel<EntityModel<CampaignLibraryNode>>> findCampaignFolderContent(@PathVariable("id") long folderId,
                                                                                                  Pageable pageable,
                                                                                                  ContentInclusion include) {

        Page<CampaignLibraryNode> content;
        switch (include) {
            case NESTED:
                content = service.findFolderAllContent(folderId, pageable);
                break;
            default:
                content = service.findFolderContent(folderId, pageable);
                break;
        }
        PagedModel<EntityModel<CampaignLibraryNode>> res = toPagedResourcesWithRel(content, "content");

        return ResponseEntity.ok(res);
    }



    @PostMapping(value = "/campaign-folders")
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
    public ResponseEntity<EntityModel<CampaignFolder>> createCampaignFolder(@RequestBody CampaignFolderDto folderDto) throws BindException, InvocationTargetException, IllegalAccessException {

        campaignFolderPostValidator.validatePostCampaignFolder(folderDto);

        CampaignFolder folder = service.addCampaignFolder(folderDto);

        EntityModel<CampaignFolder> res = toEntityModel(folder);

        linksHelper.addAllLinksForCampaignFolder(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);
    }


    @PatchMapping(value = "/campaign-folders/{id}")
    @ResponseBody
    @DynamicFilterExpression("*, parent[name], project[name]")
    public ResponseEntity<EntityModel<CampaignFolder>> patchCampaignFolder(@RequestBody CampaignFolderDto folderPatch, @PathVariable("id") long id) throws BindException {
        folderPatch.setId(id);

        campaignFolderPatchValidator.validatePatchCampaignFolder(folderPatch);

        CampaignFolder folder = service.patchCampaignFolder(folderPatch, id);

        EntityModel<CampaignFolder> res = toEntityModel(folder);

        linksHelper.addAllLinksForCampaignFolder(res);

        return ResponseEntity.ok(res);
    }

    @DeleteMapping(value = "/campaign-folders/{ids}")
    @ResponseBody
    public ResponseEntity<Void> deleteCampaignFolder(@PathVariable("ids") List<Long> folderIds) {

        service.deleteFolder(folderIds);

        return ResponseEntity.noContent().build();
    }

    private List<ParentFolderTreeDto> getCampaignFolderTree(List<CampaignFolder> folders, List<Long> alreadyAddedIds) {

        List<ParentFolderTreeDto> listDto = new ArrayList<>();

        for (CampaignFolder folder : folders) {

            if (!alreadyAddedIds.contains(folder.getId())) {

                ParentFolderTreeDto dto = new ParentFolderTreeDto();

                dto.setType(CAMPAIGN_FOLDER_TYPE);
                dto.setId(folder.getId());
                dto.setName(folder.getName());
                dto.setUrl(createSelfLink(folder).getHref());
                dto.setChildren(Collections.emptyList());
                dto.setProject(folder.getProject());

                if (folder.hasContent()) {
                    List<CampaignFolder> folderChildren = getAllCampaignFolderFromContent(folder);
                    List<ParentFolderTreeDto> cftd = getCampaignFolderTree(folderChildren, alreadyAddedIds);
                    dto.setChildren(cftd);
                }

                alreadyAddedIds.add(folder.getId());
                listDto.add(dto);
            }
        }


        return listDto;
    }

    private List<CampaignFolder> getAllCampaignFolderFromContent(CampaignFolder folder) {
        List<CampaignFolder> reqFolderList = new ArrayList<>();

        for (CampaignLibraryNode rln : folder.getContent()) {
            if (rln instanceof CampaignFolder) {
                reqFolderList.add((CampaignFolder) rln);
            }
        }

        return reqFolderList;
    }

    private List<ProjectTreeDto> buildProjectTreeFromParentFolders(List<ParentFolderTreeDto> parentFolders, List<Long> projectIds) {

        if (!parentFolders.isEmpty()) {
            return appendProjectTreesWithFolderList(parentFolders);
        } else if (!projectIds.isEmpty()){
            return appendProjectTreesWithoutFolderList(projectIds);
        }

        return Collections.emptyList();
    }

    private List<ProjectTreeDto> appendProjectTreesWithFolderList(List<ParentFolderTreeDto> tree) {
        List<ProjectTreeDto> projTree = new ArrayList<>();

        Map<Project, List<FolderTreeDto>> map = tree.stream().collect(Collectors.groupingBy(FolderTreeDto::getProject));

        for (Map.Entry<Project, List<FolderTreeDto>> folderTreesByProjectMap : map.entrySet()) {

            Project project =  folderTreesByProjectMap.getKey();
            List<FolderTreeDto> list =  folderTreesByProjectMap.getValue();
            Collections.sort(list);

            ProjectTreeDto dto = new ProjectTreeDto(project.getId(),project.getName(),list);
            projTree.add(dto);

        }

        Collections.sort(projTree);

        return projTree;
    }

    private List<ProjectTreeDto> appendProjectTreesWithoutFolderList(List<Long> projectIds) {
        List<ProjectTreeDto> projTree = new ArrayList<>();

        Map<Long, String> projectNameByIdMap = projectService.findNamesByProjectIds(projectIds);

        projectNameByIdMap.forEach((projectId, projectName) -> {
            ProjectTreeDto dto = new ProjectTreeDto(projectId,projectName, new ArrayList<>());
            projTree.add(dto);
        });

        Collections.sort(projTree);
        return projTree;
    }

    @GetMapping("/campaign-folders/{id}/issues")
    @ResponseBody
    @DynamicFilterExpression("executions[id]")
    public ResponseEntity<PagedModel<EntityModel<IssueDto>>> findCampaignFoldersIssues(@PathVariable("id") long id, Pageable pageable) {

        List<Long> executionIds =  service.getExecutionIdsByCampaignFolder(id);

        Page<IssueDto> pagedIssue = restIssueService.getIssuesFromExecutionIds(executionIds, pageable);

        PagedModel<EntityModel<IssueDto>> res = pageAssembler.toModel(pagedIssue);

        return ResponseEntity.ok(res);
    }

}
