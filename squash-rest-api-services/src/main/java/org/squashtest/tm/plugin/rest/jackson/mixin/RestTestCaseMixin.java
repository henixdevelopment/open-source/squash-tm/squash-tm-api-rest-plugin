/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonAppend.Prop;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.scm.ScmRepository;
import org.squashtest.tm.domain.testautomation.AutomatedTest;
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.RequirementVersionCoverage;
import org.squashtest.tm.domain.testcase.TestCaseAutomatable;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus;
import org.squashtest.tm.plugin.rest.core.jackson.SerializeChainConverter;
import org.squashtest.tm.plugin.rest.jackson.deserializer.RestNatureTypeCategoryDeserializer;
import org.squashtest.tm.plugin.rest.jackson.model.RestCustomFieldMembers;
import org.squashtest.tm.plugin.rest.jackson.serializer.AutomatedTestTechnologyConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.CustomFieldValuesPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.RestNatureTypeCategorySerializer;
import org.squashtest.tm.plugin.rest.jackson.serializer.ScmRepositoryConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.ScmRepositoryIdPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestCaseAutomatablePropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestCaseAutomationPriorityPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestCaseAutomationStatusPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestCaseScriptAutoPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.UnauthorizedResourcesConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.VerifiedRequirementConverter;

import java.util.List;
import java.util.Set;

@JsonAutoDetect(fieldVisibility=Visibility.NONE, getterVisibility=Visibility.NONE, isGetterVisibility=Visibility.NONE)
@JsonTypeName("test-case")
@JsonAppend(props = {
        @Prop(name = "custom_fields", type = RestCustomFieldMembers.class, value = CustomFieldValuesPropertyWriter.class),
		@Prop(name = "script_auto", value = TestCaseScriptAutoPropertyWriter.class),
		@Prop(name = "automatable", value = TestCaseAutomatablePropertyWriter.class),
		@Prop(name = "automation_status", value = TestCaseAutomationStatusPropertyWriter.class),
		@Prop(name = "automation_priority", value = TestCaseAutomationPriorityPropertyWriter.class),
		@Prop(name = "scm_repository_id", value = ScmRepositoryIdPropertyWriter.class)
})
@JsonPropertyOrder({"_type", "id", "name", "reference", "project", "path", "parent", "audit", "importance", "status", "nature", "type",
        "prerequisite", "description", "drafted_by_ai", "automated_test", "automated_test_technology", "scm_repository_url", "scm_repository_id", "automated_test_reference", "uuid",
		"automatable", "automation_status", "automation_priority", "custom_fields", "steps", "parameters", "datasets", "verified_requirements"})
public abstract class RestTestCaseMixin extends RestGenericLibraryNodeMixin{

	@JsonProperty
	String reference;

	@JsonProperty
	String prerequisite;

	@JsonProperty
	String uuid;

	@JsonProperty("automated_test_technology")
	@JsonSerialize(converter = AutomatedTestTechnologyConverter.class)
	AutomatedTestTechnology automatedTestTechnology;

	@JsonProperty("scm_repository_url")
	@JsonSerialize(converter = ScmRepositoryConverter.class)
	ScmRepository scmRepository;

	@JsonProperty("scm_repository_id")
	Long scmRepositoryId;

	@JsonProperty("automated_test_reference")
	String automatedTestReference;

	@JsonProperty
	TestCaseImportance importance;

	@JsonProperty
	TestCaseStatus status;

	@JsonProperty("automated_test")
	@JsonSerialize(converter = HateoasWrapperConverter.class)
	@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
	AutomatedTest automatedTest;

	@JsonProperty
	TestCaseAutomatable automatable;

	@JsonProperty("automation_status")
	AutomationRequestStatus automationStatus;

	@JsonProperty("automation_priority")
	Integer automationPriority;

    @JsonProperty("drafted_by_ai")
    Boolean draftedByAi;

	@JsonProperty
	@JsonSerialize(using=RestNatureTypeCategorySerializer.class)
    @JsonDeserialize(using=RestNatureTypeCategoryDeserializer.class)
	InfoListItem nature;

	@JsonProperty
	@JsonSerialize(using=RestNatureTypeCategorySerializer.class)
    @JsonDeserialize(using=RestNatureTypeCategoryDeserializer.class)
	InfoListItem type;

	@JsonProperty
	@JsonSerialize(contentConverter=HateoasWrapperConverter.class)
	@JsonTypeInfo(use=Id.NONE)
	List<TestStep> steps;

	@JsonProperty
	@JsonSerialize(contentConverter=HateoasWrapperConverter.class)
	@JsonTypeInfo(use=Id.NONE)
	Set<Parameter> parameters;

    @JsonProperty
    @JsonSerialize(contentConverter=HateoasWrapperConverter.class)
	@JsonTypeInfo(use=Id.NONE)
    Set<Dataset> datasets;

	@JsonProperty("verified_requirements")
	@SerializeChainConverter(contentConverters={
			VerifiedRequirementConverter.class,
			HateoasWrapperConverter.class,
			UnauthorizedResourcesConverter.class
	})
	@JsonTypeInfo(use=Id.NONE)
	Set<RequirementVersionCoverage> requirementVersionCoverages;

}
