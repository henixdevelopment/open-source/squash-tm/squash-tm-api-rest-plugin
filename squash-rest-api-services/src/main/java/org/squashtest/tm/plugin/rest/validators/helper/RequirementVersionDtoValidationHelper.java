/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.domain.infolist.InfoListItem;
import org.squashtest.tm.domain.infolist.SystemInfoListItemCode;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementCriticality;
import org.squashtest.tm.domain.requirement.RequirementStatus;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementVersionDto;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.infolist.InfoListItemFinderService;

import javax.inject.Inject;
import java.util.List;

@Component
public class RequirementVersionDtoValidationHelper extends CustomFieldValueValidationHelper {

    @Inject
    private InfoListItemFinderService infoListItemFinderService;
    @Inject
    private CustomFieldValueHelper customFieldValueHelper;


    public void checkAndAssignValues(final Errors errors, RequirementVersionDto iRequirementVersion){
        checkAndAssignStrings(iRequirementVersion);
        checkAndAssignCategory(errors, iRequirementVersion);
        checkAndAssignCriticality(iRequirementVersion);
    }

    public void checkAndAssignValuesPatch(final Errors errors, RequirementVersionDto iRequirementVersion, Requirement requirement){
        if (iRequirementVersion.isHasReference() && iRequirementVersion.getReference() == null) {
            iRequirementVersion.setReference("");
        }
        if (iRequirementVersion.isHasDescription() && iRequirementVersion.getDescription() == null){
            iRequirementVersion.setDescription("");
        }

        if (iRequirementVersion.isHasCategory()) {
            checkAndAssignCategory(errors, iRequirementVersion);
        }
        if(iRequirementVersion.isHasCriticality()) {
            checkAndAssignCriticality(iRequirementVersion);
        }
        if (iRequirementVersion.isHasStatus()){
            checkAndAssignStatus(errors,iRequirementVersion, requirement);
        }
    }

    private void checkAndAssignStrings(RequirementVersionDto iRequirementVersion) {
        if (iRequirementVersion.getDescription() == null){
            iRequirementVersion.setDescription("");
        }
        if (iRequirementVersion.getReference() == null){
            iRequirementVersion.setReference("");
        }
    }

    private void checkAndAssignCriticality(RequirementVersionDto iRequirementVersion) {
        RequirementCriticality criticality = iRequirementVersion.getCriticality();
        if (criticality == null) {
            iRequirementVersion.setCriticality(RequirementCriticality.UNDEFINED);
        }
    }

    private void checkAndAssignCategory(final Errors errors, RequirementVersionDto iRequirementVersion) {
        InfoListItem category = iRequirementVersion.getCategory();
        if (category != null) {
            String code = category.getCode();
            Long projectId = iRequirementVersion.getProjectId();
            if (projectId == null) {
                //throw new IllegalArgumentException("Programmatic error : You must give a project to a Rest Node to be able to check infolist.");
                errors.rejectValue("category", "invalid projetId", "You must give a project to a Rest Node to be able to check infolist.");

            }
            if (infoListItemFinderService.isCategoryConsistent(projectId, code)) {
                iRequirementVersion.setCategory(infoListItemFinderService.findByCode(code));
            } else {
                errors.rejectValue("category", "invalid category", "Invalid requirement category for this project");
            }
        } else {
            InfoListItem defaultCategory = infoListItemFinderService.findByCode(SystemInfoListItemCode.CAT_UNDEFINED.getCode());
            iRequirementVersion.setCategory(defaultCategory);
        }
    }

    public void checkAndAssignStatus(final Errors errors, RequirementVersionDto requirementVersionDto, Requirement requirement){

        RequirementStatus actualStatus = requirement.getStatus();
        if (requirementVersionDto.getStatus() == null){
            requirementVersionDto.setStatus(RequirementStatus.WORK_IN_PROGRESS);
        }

        if (!actualStatus.getAllowsStatusUpdate() || !actualStatus.isTransitionLegal(requirementVersionDto.getStatus())) {
            errors.rejectValue("status","invalid status", "Only requirements which have the status 'Under review' can have their status updated to 'Approved'.");
        }
    }

    public void checkCufs(final Errors errors, RequirementVersionDto requirementVersionDto){

        List<CustomFieldValueDto> listCufsDto = requirementVersionDto.getCustomFields();
        customFieldValueHelper.checkCufs(errors,BindableEntity.REQUIREMENT_VERSION, requirementVersionDto.getProjectId(),listCufsDto);

    }


}
