/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web.exceptionhandler;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.squashtest.tm.core.foundation.exception.ActionException;
import org.squashtest.tm.exception.DomainException;
import org.squashtest.tm.plugin.rest.core.web.IRestController;
import org.squashtest.tm.plugin.rest.core.web.exceptionhandler.response.DetailedJsonError;
import org.squashtest.tm.plugin.rest.core.web.exceptionhandler.response.JsonError;

import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;

@ControllerAdvice(assignableTypes = IRestController.class)
public class RestApiGenericExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestApiGenericExceptionHandler.class);

    private static final String UNEXPECTED_ERROR_OCCURRED = "An unexpected error occurred.";

    // This list must be kept in sync with the list of exceptions in #handleNotFound
    private static final Class<?>[] NOT_FOUND_EXCEPTIONS = {
        NoResultException.class,
        EntityNotFoundException.class
    };

    @ExceptionHandler({JsonParseException.class, JsonMappingException.class})
    ResponseEntity<JsonError> handleJsonParseExceptions(Exception ex) {
        LOGGER.error(ex.getMessage(), ex);
        return buildDetailedResponse(HttpStatus.BAD_REQUEST, "Invalid JSON.");
    }

    @ExceptionHandler({
        HttpMessageNotReadableException.class,
        MissingServletRequestParameterException.class
    })
    public ResponseEntity<JsonError> handleInvalidJsonBody(HttpMessageNotReadableException ex) {
        LOGGER.error(ex.getMessage(), ex);
        return buildDetailedResponse(HttpStatus.BAD_REQUEST, "Invalid JSON.");
    }

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<JsonError> handleBadRequest(Throwable ex) {
        LOGGER.error(ex.getMessage(), ex);
        return buildDetailedResponse(HttpStatus.BAD_REQUEST, ex);
    }

    /**
     * Handle entity not found exceptions. The annotation must be kept in sync with the list of exceptions in
     * {@link #NOT_FOUND_EXCEPTIONS}.
     */
    @ExceptionHandler({ NoResultException.class, EntityNotFoundException.class })
    ResponseEntity<JsonError> handleNotFound(Throwable ex) {
        LOGGER.error(ex.getMessage(), ex);
        return buildDetailedResponse(HttpStatus.NOT_FOUND, ex);
    }

    @ExceptionHandler
    ResponseEntity<JsonError> handleAccessDenied(AccessDeniedException ex) {
        LOGGER.error(ex.getMessage(), ex);
        return buildOpaqueResponse(HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler
    ResponseEntity<JsonError> handleActionException(ActionException ex) {
        LOGGER.error(ex.getMessage(), ex);
        return buildDetailedResponse(HttpStatus.PRECONDITION_FAILED, ex);
    }

    @ExceptionHandler
    ResponseEntity<JsonError> handleDomainException(DomainException ex) {
        LOGGER.error(ex.getMessage(), ex);
        return buildDetailedResponse(HttpStatus.PRECONDITION_FAILED, ex);
    }

    /**
     * Catch-all exceptions, if no other more specific handler could do the job
     */
    @ExceptionHandler({RuntimeException.class})
    ResponseEntity<JsonError> catchAll(Exception unqualifiedException) {
        LOGGER.debug("wrapped-exception handler invoked, attempting to resolve the root exception");

        final Throwable root = findRootException(unqualifiedException);

        if (isNotFoundException(root)) {
            return handleNotFound(root);
        } else {
            return handleAnyException((Exception) root);
        }
    }

    private static boolean isNotFoundException(Throwable root) {
        for (Class<?> exception : NOT_FOUND_EXCEPTIONS) {
            if (exception.isAssignableFrom(root.getClass())) {
                return true;
            }
        }

        return false;
    }

    private ResponseEntity<JsonError> handleAnyException(Exception ex) {
        if (LOGGER.isErrorEnabled()) {
            LOGGER.error("resolving exception '%s' with the catch-all handler".formatted(ex.getClass().getName()), ex);
        }

        LOGGER.error(ex.getMessage(), ex);
        return buildDetailedResponse(HttpStatus.INTERNAL_SERVER_ERROR, UNEXPECTED_ERROR_OCCURRED);
    }

    private Throwable findRootException(Exception caught) {
        Throwable root = caught;

        while (root.getCause() != null && root.getCause() != root) {
            root = root.getCause();
        }

        return root;
    }

    private ResponseEntity<JsonError> buildDetailedResponse(HttpStatus status, String message) {
        return ResponseEntity
            .status(status)
            .body(new DetailedJsonError(status.value(), status.getReasonPhrase(), message));
    }

    private ResponseEntity<JsonError> buildDetailedResponse(HttpStatus status, Throwable exception) {
        return ResponseEntity
            .status(status)
            .body(new DetailedJsonError(status.value(), status.getReasonPhrase(), exception.getMessage()));
    }

    private ResponseEntity<JsonError> buildOpaqueResponse(HttpStatus status) {
        return ResponseEntity
            .status(status)
            .body(new JsonError(status.value(), status.getReasonPhrase()));
    }
}
