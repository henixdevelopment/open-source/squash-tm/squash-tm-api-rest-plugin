/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.execution.KeywordExecution;
import org.squashtest.tm.domain.execution.ScriptedExecution;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationFilterExpression;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.PersistentEntity;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.ExecutionAndCustomFields;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDtoForExecution;
import org.squashtest.tm.plugin.rest.service.RestExecutionService;
import org.squashtest.tm.plugin.rest.service.RestExecutionStepService;
import org.squashtest.tm.plugin.rest.service.RestIssueService;
import org.squashtest.tm.plugin.rest.validators.CustomFieldValueHintedValidator;
import org.squashtest.tm.plugin.rest.validators.DenormalizedFieldValueHintedValidator;
import org.squashtest.tm.plugin.rest.validators.ExecutionHintedValidator;

import javax.inject.Inject;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(Execution.class)
@UseDefaultRestApiConfiguration
public class RestExecutionController extends BaseRestController{

    // should not handle exploratory executions in this controller but inheritance makes that difficult, so we filter out the specific attributes
    public static final String FILTER_OUT_EXPLORATORY_EXECUTION_FIELDS = "-progress_status,-charter,-task_division,-session_notes,-reviewed,-execution_events";
    public static final String EXECUTION_DYNAMIC_FILTER = "*,execution_steps[execution_status,action,expected_result], nature[code], type[code], automated_execution_extender[result_url, result_status]" + FILTER_OUT_EXPLORATORY_EXECUTION_FIELDS;
    public static final String PATCH_DYNAMIC_FILTER = "comment, execution_status, custom_fields";

    @Inject
    private ResourceLinksHelper linksHelper;

    @Inject
    private RestExecutionService restExecutionService;

    @Inject
    private CustomFieldValueHintedValidator cufValidator;

    @Inject
    private DenormalizedFieldValueHintedValidator denoValidator;

    @Inject
    private ExecutionHintedValidator execValidator;

    @Inject
    private RestIssueService restIssueService;

    @Inject
    private RestExecutionStepService restExecutionStepService;

    public void initBinder(WebDataBinder binder){
        binder.addValidators(execValidator);
        binder.addValidators(cufValidator);
        binder.addValidators(denoValidator);
    }


    @GetMapping(value = "/executions/{id}")
    @EntityGetter({Execution.class, ScriptedExecution.class, KeywordExecution.class})
    @ResponseBody
    @DynamicFilterExpression(EXECUTION_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<Execution>> findExecution(@PathVariable("id") long id){

        Execution execution = restExecutionService.getOne(id);

        if (execution.getExecutionMode() == TestCaseExecutionMode.EXPLORATORY) {
            throw new IllegalArgumentException("Execution " + id + " is an exploratory execution and cannot be retrieved. Please use the dedicated endpoint : /exploratory-executions/" + id);
        }

        EntityModel<Execution> res = toEntityModel(execution);

        linksHelper.addAllLinksForExecution(res);
        res.add(createRelationTo( "issues"));

        return ResponseEntity.ok(res);
    }

    @PatchMapping(value = "/executions/{id}")
    @ResponseBody
    @DynamicFilterExpression(EXECUTION_DYNAMIC_FILTER)
    @DeserializationFilterExpression(PATCH_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<Execution>> patchExecution(@Validated @PersistentEntity ExecutionAndCustomFields exec){
    	restExecutionService.updateExecution(exec);

    	EntityModel<Execution> res = toEntityModel(exec.getWrapped());

    	linksHelper.addAllLinksForExecution(res);


        return ResponseEntity.ok(res);
    }


    @GetMapping("/executions/{id}/execution-steps")
    @ResponseBody
    @DynamicFilterExpression("*, execution[execution_status]")
    public ResponseEntity<PagedModel<EntityModel<ExecutionStep>>> findExecutionSteps(@PathVariable("id") long executionId, Pageable pageable){

        Page<ExecutionStep> steps = restExecutionService.findExecutionSteps(executionId, pageable);

        PagedModel<EntityModel<ExecutionStep>> res = toPagedResourcesWithRel(steps, "execution-steps");

        return ResponseEntity.ok(res);

    }

    /*AMK Delete execution*/
    @DeleteMapping(value = "/executions/{id}")
    @ResponseBody
    @DynamicFilterExpression("*")
    public ResponseEntity<Void> deleteExecution(@PathVariable("id") Long executionId) {

        restExecutionService.deleteExecution(executionId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/executions/{id}/issues")
    @ResponseBody
    @DynamicFilterExpression("execution-step[id]")
    public ResponseEntity<PagedModel<EntityModel<IssueDtoForExecution>>> findExecutionsIssues(@PathVariable("id") long id, Pageable pageable) {

        Execution execution =  restExecutionService.getOne(id);

        Page<IssueDto> pagedIssue = restIssueService.getIssuesFromExecutionIds(Collections.singletonList(execution.getId()), pageable);

        Page<IssueDtoForExecution> pagedIssueForExecution = pagedIssue.map(this::convertToIssueDtoForExecution);

        for (IssueDtoForExecution issue : pagedIssueForExecution.getContent()) {
            List<ExecutionStep> executionSteps = restExecutionStepService.getExecutionStepFromIssue(issue.getRemoteIssueId(), id);
            issue.setExecutionSteps(new HashSet<>(executionSteps));
        }

        PagedModel<EntityModel<IssueDtoForExecution>> res = pageAssembler.toModel(pagedIssueForExecution);

        return ResponseEntity.ok(res);
    }

    private IssueDtoForExecution convertToIssueDtoForExecution(IssueDto issueDto) {
        return new IssueDtoForExecution(issueDto.getRemoteIssueId(), issueDto.getUrl());
    }

}
