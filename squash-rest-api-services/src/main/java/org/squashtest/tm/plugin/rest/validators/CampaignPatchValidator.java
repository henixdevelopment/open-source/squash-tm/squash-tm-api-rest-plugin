/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestType;
import org.squashtest.tm.plugin.rest.validators.helper.CampaignDtoValidationHelper;

import javax.inject.Inject;

import static org.squashtest.tm.plugin.rest.jackson.model.RestType.CAMPAIGN;

@Component
public class CampaignPatchValidator implements Validator {

    private static final String NON_PATCHABLE_ATTRIBUTE = "non patchable attribute";


    @Inject
    private CampaignDtoValidationHelper campaignDtoValidationHelper;

    @Override
    public boolean supports(Class<?> clazz) {
        return CampaignDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        CampaignDto patch = (CampaignDto) target;

        campaignDtoValidationHelper.checkEntityExist(errors, RestType.CAMPAIGN, patch.getId());
        campaignDtoValidationHelper.loadProject(patch);
        campaignDtoValidationHelper.checkParent(errors, patch, CAMPAIGN);
        campaignDtoValidationHelper.checkAndAssignStatus(errors, patch);
        campaignDtoValidationHelper.checkCufs(errors, patch, BindableEntity.CAMPAIGN);

        checkForbiddenPatchAttributes(errors, patch);
    }

    private void checkForbiddenPatchAttributes(final Errors errors, CampaignDto post) {
        if (post.getIterations() != null) {
            errors.rejectValue("iterations", NON_PATCHABLE_ATTRIBUTE, "Only attributes belonging to the campaign itself can be posted. The attribute iterations cannot be posted. Use direct url to the iterations entity instead");
        }
        if(post.getMilestones() !=null) {
            errors.rejectValue("milestones", NON_PATCHABLE_ATTRIBUTE, "Only attributes belonging to the campaign itself can be posted. The attribute milestones cannot be posted. Use direct url to the milestones entity instead");
        }
        if(post.getTestPlans() != null) {
            errors.rejectValue("test-plan", NON_PATCHABLE_ATTRIBUTE, "Only attributes belonging to the campaign itself can be posted. The attribute test plan cannot be posted. Use direct url to the test plan entity instead");
        }
    }
}
