/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.plugin.rest.core.jackson.RestDtoName;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;


import java.net.URL;
import java.util.Set;

@RestDtoName(value = "issue")
public class IssueDtoForExecution {

    @JsonProperty("remoteIssueId")
    private String remoteIssueId;

    @JsonProperty("url")
    private URL url;

    @JsonProperty("execution-steps")
    @JsonSerialize(contentConverter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Set<ExecutionStep> executionSteps;


    public IssueDtoForExecution() {
    }


    public IssueDtoForExecution(String remoteIssueId, URL url) {
        this.remoteIssueId = remoteIssueId;
        this.url = url;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getRemoteIssueId() {
        return remoteIssueId;
    }

    public void setRemoteIssueId(String remoteIssueId) {
        this.remoteIssueId = remoteIssueId;
    }

    public Set<ExecutionStep> getExecutionSteps() {
        return executionSteps;
    }

    public void setExecutionSteps(Set<ExecutionStep> executionSteps) {
        this.executionSteps = executionSteps;
    }
}
