/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.campaign.CampaignLibrary
import org.squashtest.tm.domain.project.Project
import org.squashtest.tm.domain.testcase.TestCaseLibrary

import static org.squashtest.tm.test.domainbuilder.PropertyHelper.assign

public class ProjectExpandos {

	private ProjectExpandos(){
		super();
	}

	static class ProjectExpando extends BaseExpando<Project> implements AuditableFeatures{
		
		ProjectExpando(){
			super()
			
			id = 1l
			name = "sample project"
			
			createdBy "admin"
			createdOn "2017/06/15"
			lastModifiedBy "admin"
			lastModifiedOn "2017/06/15"

		}

		@Override
		def create(){
			Project project = super.create()

			if (project.testCaseLibrary){
				assign project.testCaseLibrary, "project", project
			}

			if (project.campaignLibrary){
				assign project.campaignLibrary, "project", project
			}

			return project
		}

	}

	static class TestCaseLibraryExpando extends BaseExpando<TestCaseLibrary>{

		TestCaseLibraryExpando(){
			super()
			id = 1L
		}

		@Override
		def create(){
			TestCaseLibrary library = super.create()

			if (library.project){
				assign library.project, "testCaseLibrary", library
			}

			return library
		}
	}

	static class CampaignLibraryExpando extends BaseExpando<CampaignLibrary>{

		CampaignLibraryExpando(){
			super()
			id = 1L
		}

		@Override
		def create(){
			CampaignLibrary library = super.create()

			if (library.project){
				assign library.project, "campaignLibrary", library
			}

			return library
		}
	}
}
