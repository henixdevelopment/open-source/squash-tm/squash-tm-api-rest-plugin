/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.SprintGroup;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.service.RestSprintGroupService;

import javax.inject.Inject;

@RestApiController(SprintGroup.class)
@UseDefaultRestApiConfiguration
public class RestSprintGroupController extends BaseRestController {
    @Inject
    private RestSprintGroupService sprintGroupService;

    @GetMapping("/sprint-groups/{id}")
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression("id, name")
    public ResponseEntity<EntityModel<SprintGroup>> findSprintGroup(@PathVariable("id") long id) {
        SprintGroup sprintGroup = sprintGroupService.getOne(id);

        EntityModel<SprintGroup> entityModel = toEntityModel(sprintGroup);
        entityModel.add(linkService.createLinkTo(sprintGroup.getProject()));

        return ResponseEntity.ok(entityModel);
    }
}
