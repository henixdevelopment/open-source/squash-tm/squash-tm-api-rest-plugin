/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints;

/**
 * <p></p>Validators that implement this interface will be supplied the {@link org.squashtest.tm.plugin.rest.core.jackson.DeserializationHints} used
 * in the context of a PATCH/POST that uses annotation @PersistentEntity.</p>
 *
 * <p>Implementors may leave {@link #validate(Object, Errors)} empty, if they prefer to focus on {@link #validateWithHints(Object, Errors, DeserializationHints)}</p>
 *
 */
public interface HintedValidator extends Validator {

    void validateWithHints(Object target, Errors errors, DeserializationHints hints);

}
