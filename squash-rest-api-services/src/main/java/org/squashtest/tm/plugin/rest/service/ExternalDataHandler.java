/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service;


import org.springframework.stereotype.Service;
import org.squashtest.tm.plugin.rest.jackson.model.ExternalData;

import java.util.ArrayList;
import java.util.List;

@Service
public class ExternalDataHandler {

    ThreadLocal<List<ExternalData>> threadLocalValue = ThreadLocal.withInitial(ArrayList::new);

    public void addExternalData(Object entity, String key, List<?> data) {
        List<ExternalData> externalDataList = threadLocalValue.get();
        ExternalData externalData = externalDataList.stream()
                .filter(ed -> ed.getEntity().equals(entity))
                .findFirst()
                .orElseGet(() -> new ExternalData(entity));

        externalData.addData(key, data);
        externalDataList.add(externalData);
    }

    public List<?> getExternalData(Object entity, String key) {
        return threadLocalValue.get().stream()
                .filter(ed -> ed.getEntity().equals(entity))
                .findFirst()
                .map(ed -> ed.getDataMap().get(key))
                .orElse(null);
    }

}
