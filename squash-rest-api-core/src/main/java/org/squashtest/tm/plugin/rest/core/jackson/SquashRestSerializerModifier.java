/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.ser.BeanPropertyWriter;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * That serializer modifier will re-sort the properties, because it seems that Jackson doesn't honor 
 * annotation {@link JsonPropertyOrder} for the virtual properties. Indeed the standard {@link com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector}
 * just append or prepend the virtual properties to the list of concrete properties. This class will re-sort them according to 
 * specified order (if the bean description specifies it).
 * 
 * 
 * @author bsiri
 *
 */
public class SquashRestSerializerModifier extends BeanSerializerModifier{

	@Override
	public List<BeanPropertyWriter> orderProperties(SerializationConfig config, BeanDescription beanDesc,
			List<BeanPropertyWriter> beanProperties) {
		
		JsonPropertyOrder annot = beanDesc.getClassInfo().getAnnotation(JsonPropertyOrder.class);
		
		// if nothing was specified -> nothing to do
		if (annot == null ){
			return beanProperties;
		}
		
		String[] pptOrdered = annot.value();
		Map<String, BeanPropertyWriter> mappedWriters = mapByName(beanProperties);
		List<BeanPropertyWriter> newWriters = new ArrayList<>(beanProperties.size());
		
		// according to Jackson policy, ordered properties should go first.
		for (String ppt : pptOrdered){
			BeanPropertyWriter writer = mappedWriters.remove(ppt);
			if (writer != null){
				newWriters.add(writer);
			}
		}
		
		// then, the remaining properties
		newWriters.addAll(mappedWriters.values());
		
		return newWriters;
		
	}
	
	
	private Map<String, BeanPropertyWriter> mapByName(List<BeanPropertyWriter> beanProperties){
		Map<String, BeanPropertyWriter> mapped = new LinkedHashMap<>(beanProperties.size());
		for (BeanPropertyWriter writer : beanProperties){
			mapped.put(writer.getName(), writer);
		}
		return mapped;
	}
	
}
