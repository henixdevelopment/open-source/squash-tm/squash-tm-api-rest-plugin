/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.testautomation.TestAutomationServerKind
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestAutomatedExecutionExtenderService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestAutomatedExecutionExtenderController)
class RestAutomatedExecutionExtenderControllerIT extends BaseControllerSpec {

    @Inject
    RestAutomatedExecutionExtenderService executionExtenderService;

    def "get-automated-execution-extender"() {

        given:
        def autoTest = SquashEntityBuilder.automatedTest {
            id = 569L
            name = "auto test"
            project = SquashEntityBuilder.testAutomatioProject {
                id = 569L
                jobName = "job 1"
                label = "wan wan"
                server = SquashEntityBuilder.testAutomationServer {
                    id = 569L
                    name = "Orchestrator"
                    url = "http://127.0.0.1:7774"
                    observer_url = "http://127.0.0.1:7775"
                    event_bus_url = "http://127.0.0.1:38368"
                    killswitch_url = "http://127.0.0.1:7776"
                    kind = TestAutomationServerKind.squashOrchestrator
                    description = "<p>nice try</p>"
                }
                tmProject = SquashEntityBuilder.project {
                    id = 367L
                    description = "<p>This project is the main sample project</p>"
                    label = "Main Sample Project"
                }
                slaves = "no slavery"
            }
        }

        def exec = SquashEntityBuilder.execution {

            referencedTestCase = SquashEntityBuilder.testCase {
            }

            id = 56l
            name = "sample test case 56"
            executionOrder = 4
            executionStatus "BLOCKED"

            lastExecutedBy = "User-5"
            lastExecutedOn "2017/07/24"

            executionMode "AUTOMATED"
            reference = "SAMP_EXEC_56"
            datasetLabel = "sample dataset"

            steps = [
                    SquashEntityBuilder.executionStep {
                        id = 22l
                        executionStatus "SUCCESS"
                        action = "<p>First action</p>"
                        expectedResult = "<p>First result</p>"
                    },
                    SquashEntityBuilder.executionStep {
                        id = 23l
                        executionStatus "BLOCKED"
                        action = "<p>Second action</p>"
                        expectedResult = "<p>Second result</p>"
                    },
                    SquashEntityBuilder.executionStep {
                        id = 27l
                        executionStatus "SUCCESS"
                        action = "<p>The Action</p>"
                        expectedResult = "<p>The Result</p>"
                    }
            ]

            description = "<p>I have no comment</p>"
            prerequisite = "<p>Being alive.</p>"
            tcdescription = "<p>This is nice.</p>"
            importance "LOW"
            nature "NAT_SECURITY_TESTING"
            type "TYP_EVOLUTION_TESTING"
            status "APPROVED"
            testPlan = SquashEntityBuilder.iterationTestPlanItem {
                id = 15L
                iteration = SquashEntityBuilder.iteration {
                    campaign = SquashEntityBuilder.campaign {
                        project = SquashEntityBuilder.project {
                            id = 87L
                        }
                    }
                }
            }

        }

        def automatedExecutionExtender = SquashEntityBuilder.automatedExecutionExtender {
            id = 778L
            automatedTest = autoTest
            execution = exec
            resultSummary = "all right"
            nodeName = "no root"
            duration = 4096
        }


        and:
        executionExtenderService.getOne(778) >> automatedExecutionExtender
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/automated-execution-extenders/{id}", 778).header("Accept", "application/json"))
        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "automated-execution-extender"
            "id".is 778
            "execution.name".is "sample test case 56"
            "automated_test.name".is "auto test"
            "result_url".is null
            "result_summary".is "all right"
            "result_status".is "BLOCKED"
            "execution_node".is "no root"
            "duration".is 4096
            "execution".test {
                "_type".is "execution"
                "id".is 56
            }
            selfRelIs "http://localhost:8080/api/rest/latest/automated-execution-extenders/778"
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {

                    pathParams {
                        add "id : the id of the automated execution extender"
                    }

                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (number) : the id of the automated execution extender"
                        add "_type (string) : the type of the entity"
                        addAndStop "execution (object) : the execution associated to this automated execution extender"
                        addAndStop "automated_test (object) : the automated test associated to this automated execution extender"
                        add "result_url (string?) : the result url of the automated execution extender"
                        add "result_summary (string) : the result summary of the automated execution extender"
                        add "result_status (string) : the result status of the automated execution extender"
                        add "execution_node (string) : the node name of the automated execution extender"
                        add "duration (number): the duration of the execution, in ms, as recorded in the execution report"

                        add DocumentationSnippets.DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this automated test"
                    }
                }
        ))


    }

    def "get-automated-execution-extender for scripted execution"() {

        given:
        def autoTest = SquashEntityBuilder.automatedTest {
            id = 569L
            name = "auto test"
            project = SquashEntityBuilder.testAutomatioProject {
                id = 569L
                jobName = "job 1"
                label = "wan wan"
                server = SquashEntityBuilder.testAutomationServer {
                    id = 569L
                    name = "Orchestrator"
                    url = "http://127.0.0.1:7774"
                    observer_url = "http://127.0.0.1:7775"
                    event_bus_url = "http://127.0.0.1:38368"
                    killswitch_url = "http://127.0.0.1:7776"
                    kind = TestAutomationServerKind.squashOrchestrator
                    description = "<p>nice try</p>"
                }
                tmProject = SquashEntityBuilder.project {
                    id = 367L
                    description = "<p>This project is the main sample project</p>"
                    label = "Main Sample Project"
                }
                slaves = "no slavery"
            }
        }

        def exec = SquashEntityBuilder.scriptedExecution {

            referencedTestCase = SquashEntityBuilder.scriptedTestCase {
            }

            id = 57l
            name = "sample scripted test case 57"
            executionOrder = 4
            executionStatus "BLOCKED"

            lastExecutedBy = "User-5"
            lastExecutedOn "2017/07/24"

            executionMode "AUTOMATED"
            reference = "SAMP_EXEC_56"
            datasetLabel = "sample dataset"

            steps = [

            ]

            description = "<p>I have no comment</p>"
            prerequisite = "<p>Being alive.</p>"
            tcdescription = "<p>This is nice.</p>"
            importance "LOW"
            nature "NAT_SECURITY_TESTING"
            type "TYP_EVOLUTION_TESTING"
            status "APPROVED"
            testPlan = SquashEntityBuilder.iterationTestPlanItem {
                id = 15L
                iteration = SquashEntityBuilder.iteration {
                    campaign = SquashEntityBuilder.campaign {
                        project = SquashEntityBuilder.project {
                            id = 87L
                        }
                    }
                }
            }

        }

        def automatedExecutionExtender = SquashEntityBuilder.automatedExecutionExtender {
            id = 778L
            automatedTest = autoTest
            execution = exec
            resultSummary = "all right"
            nodeName = "no root"
            duration = 4906
        }


        and:
        executionExtenderService.getOne(778) >> automatedExecutionExtender
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/automated-execution-extenders/{id}", 778).header("Accept", "application/json"))
        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "automated-execution-extender"
            "id".is 778
            "execution.name".is "sample scripted test case 57"
            "automated_test.name".is "auto test"
            "result_url".is null
            "result_summary".is "all right"
            "result_status".is "BLOCKED"
            "execution_node".is "no root"
            "duration".is 4906
            "execution".test {
                "_type".is "scripted-execution"
                "id".is 57
            }
            selfRelIs "http://localhost:8080/api/rest/latest/automated-execution-extenders/778"
        }

    }

    def "get-automated-execution-extender for keyword execution"() {

        given:
        def autoTest = SquashEntityBuilder.automatedTest {
            id = 569L
            name = "auto test"
            project = SquashEntityBuilder.testAutomatioProject {
                id = 569L
                jobName = "job 1"
                label = "wan wan"
                server = SquashEntityBuilder.testAutomationServer {
                    id = 569L
                    name = "Orchestrator"
                    url = "http://127.0.0.1:7774"
                    observer_url = "http://127.0.0.1:7775"
                    event_bus_url = "http://127.0.0.1:38368"
                    killswitch_url = "http://127.0.0.1:7776"
                    kind = TestAutomationServerKind.squashOrchestrator
                    description = "<p>nice try</p>"
                }
                tmProject = SquashEntityBuilder.project {
                    id = 367L
                    description = "<p>This project is the main sample project</p>"
                    label = "Main Sample Project"
                }
                slaves = "no slavery"
            }
        }

        def exec = SquashEntityBuilder.keywordExecution {

            referencedTestCase = SquashEntityBuilder.keywordTestCase {
            }

            id = 58l
            name = "sample keyword test case 58"
            executionOrder = 4
            executionStatus "BLOCKED"

            lastExecutedBy = "User-5"
            lastExecutedOn "2017/07/24"

            executionMode "AUTOMATED"
            reference = "SAMP_EXEC_56"
            datasetLabel = "sample dataset"

            steps = [

            ]

            description = "<p>I have no comment</p>"
            prerequisite = "<p>Being alive.</p>"
            tcdescription = "<p>This is nice.</p>"
            importance "LOW"
            nature "NAT_SECURITY_TESTING"
            type "TYP_EVOLUTION_TESTING"
            status "APPROVED"
            testPlan = SquashEntityBuilder.iterationTestPlanItem {
                id = 15L
                iteration = SquashEntityBuilder.iteration {
                    campaign = SquashEntityBuilder.campaign {
                        project = SquashEntityBuilder.project {
                            id = 87L
                        }
                    }
                }
            }

        }

        def automatedExecutionExtender = SquashEntityBuilder.automatedExecutionExtender {
            id = 778L
            automatedTest = autoTest
            execution = exec
            resultSummary = "all right"
            nodeName = "no root"
            duration = 4096
        }


        and:
        executionExtenderService.getOne(778) >> automatedExecutionExtender
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/automated-execution-extenders/{id}", 778).header("Accept", "application/json"))
        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "automated-execution-extender"
            "id".is 778
            "execution.name".is "sample keyword test case 58"
            "automated_test.name".is "auto test"
            "result_url".is null
            "result_summary".is "all right"
            "result_status".is "BLOCKED"
            "execution_node".is "no root"
            "duration".is 4096
            "execution".test {
                "_type".is "keyword-execution"
                "id".is 58
            }
            selfRelIs "http://localhost:8080/api/rest/latest/automated-execution-extenders/778"
        }

    }
}
