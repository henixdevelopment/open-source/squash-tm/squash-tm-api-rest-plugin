/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.squashtest.tm.plugin.rest.jackson.deserializer.DatasetParamValueDtoDeserializer;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by jthebault on 09/06/2017.
 */


public class DatasetDto  {

    private Long id;

    private String name;
    boolean hasName=false;
    boolean hasParamValue=false;

    public boolean isHasName() {
        return hasName;
    }

    public boolean isHasParamValue() {
        return hasParamValue;
    }

    @JsonProperty("test_case")
    private TestCaseDto referencedTestCase;

    @JsonProperty("parameter_values")
    @JsonDeserialize(contentUsing = DatasetParamValueDtoDeserializer.class)
    private Set<DatasetParamValueDto> valueDtos = new HashSet<>();

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<DatasetParamValueDto> getValueDtos() {
        return valueDtos;
    }

    public void setValueDtos(Set<DatasetParamValueDto> valueDtos) {

        this.valueDtos = valueDtos;
        this.hasParamValue = true;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
        this.hasName = true;
    }

    public TestCaseDto getReferencedTestCase() {
        return referencedTestCase;
    }

    public void setReferencedTestCase(TestCaseDto referencedTestCase) {
        this.referencedTestCase = referencedTestCase;
    }
}

/*public class DatasetDto  extends Dataset{

    @JsonProperty("parameter_values")
    @JsonDeserialize(contentUsing = DatasetParamValueDtoDeserializer.class)
    private Set<DatasetParamValueDto> valueDtos = new HashSet<>();

    public Set<DatasetParamValueDto> getValueDtos() {
        return valueDtos;
    }

    public void setValueDtos(Set<DatasetParamValueDto> valueDtos) {
        this.valueDtos = valueDtos;
    }
}*/
