/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import org.springframework.validation.AbstractBindingResult;
import org.springframework.validation.BeanPropertyBindingResult;
import org.squashtest.tm.domain.Identified;
import org.squashtest.tm.domain.project.Project;

/**
 * Various information that deserialization utilities might want to know. Those information, if available,
 * should be retrieved from the {@link com.fasterxml.jackson.databind.cfg.ContextAttributes} embedded in
 * the {@link com.fasterxml.jackson.databind.DeserializationContext}
 *
 * Created by bsiri on 11/07/2017.
 */
public class DeserializationHints {

    public static final String HINTS_KEY = "deserialization_hints";

    private Mode mode;

    private Identified targetEntity;

    private Project project;

    private Identified parent;

    private DeserializationDynamicFilter filter;

    /*
     * this BindingResult allows deserializers to store errors they encounter if they need to.
     * But remember that it is a stub : the target of validation is under creation so it is most likely
     * to be null. It still should do the job.
     */
    private AbstractBindingResult bindingResult = new BeanPropertyBindingResult(null, "payload");

    public DeserializationHints(){
        super();
    }



    public Mode getMode() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public Identified getTargetEntity() {
        return targetEntity;
    }

    public void setTargetEntity(Identified targetEntity) {
        this.targetEntity = targetEntity;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Identified getParent() {
        return parent;
    }

    public void setParent(Identified parent) {
        this.parent = parent;
    }

    public DeserializationDynamicFilter getFilter() {
        return filter;
    }

    public void setFilter(DeserializationDynamicFilter filter) {
        this.filter = filter;
    }

    public AbstractBindingResult getBindingResult() {
        return bindingResult;
    }

    public void setBindingResult(AbstractBindingResult bindingResult) {
        this.bindingResult = bindingResult;
    }

    // ****************** inner classes *********************

    public static enum Mode{
        DESERIALIZE_CREATE,
        DESERIALIZE_UPDATE;
    }

}
