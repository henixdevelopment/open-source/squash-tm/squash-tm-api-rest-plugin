/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class ContentInclusionArgumentResolver implements HandlerMethodArgumentResolver{

	private ContentInclusion defaultInclusion = ContentInclusion.ROOT;
	private String includeParameterName = UriComponents.CONTENT_INCLUDE;

	
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		return ContentInclusion.class.equals(parameter.getParameterType());
	}

	
	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
			NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
		
		String value = webRequest.getParameter(includeParameterName);
		
		if (value == null){
			return defaultInclusion;
		}
		
		try{
			return ContentInclusion.valueOf(value.toUpperCase());
		}
		catch(IllegalArgumentException ex){
			throw new IllegalArgumentException("Invalid value '"+value+"' for request parameter '"+includeParameterName+
											"' : must be one of '"+ContentInclusion.ROOT.name().toLowerCase()+"', '"+ContentInclusion.NESTED.name().toLowerCase()+"'");
		}
		
	}
	
	
	
	public ContentInclusion getDefaultInclusion() {
		return defaultInclusion;
	}

	public void setDefaultInclusion(ContentInclusion defaultInclusion) {
		this.defaultInclusion = defaultInclusion;
	}

	public String getIncludeParameterName() {
		return includeParameterName;
	}

	public void setIncludeParameterName(String includeParameterName) {
		this.includeParameterName = includeParameterName;
	}
	

}
