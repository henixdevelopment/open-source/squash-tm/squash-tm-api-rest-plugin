/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;


import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.hateoas.server.mvc.RepresentationModelProcessorInvoker;
import org.springframework.web.method.HandlerMethod;


/**
 * That handler adapter will ONLY trigger for handler methods taken from controllers annotated with {@link RestApiController}
 * 
 * @author bsiri
 *
 */
public class RestApiHandlerAdapter extends RepresentationModelProcessorInvokingHandlerAdapter {
	
	/**
	 * Order is overriden here because otherwise another catch-all handleradapter would pick up. It is harmless to put that handler first
	 * in the chain because it will react only to the controllers of the api (see {@link #supportsInternal(HandlerMethod)} )
	 *
	 */
	@Override
	public int getOrder() {
		return 0;
	}
	
	
	public RestApiHandlerAdapter(RepresentationModelProcessorInvoker invoker) {
		super(invoker);
	}

	@Override
	protected boolean supportsInternal(HandlerMethod handlerMethod) {

		Class<?> controllerType = handlerMethod.getBeanType();

		return AnnotationUtils.findAnnotation(controllerType, RestApiController.class) != null;
	}
}
