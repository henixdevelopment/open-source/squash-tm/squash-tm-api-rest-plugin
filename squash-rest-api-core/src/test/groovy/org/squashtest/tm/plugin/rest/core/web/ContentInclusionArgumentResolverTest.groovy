/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web

import org.springframework.web.context.request.NativeWebRequest
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Created by bsiri on 23/06/2017.
 */
class ContentInclusionArgumentResolverTest extends Specification{

    def "should resolve an undefined value to the default value"(){

        given :
            ContentInclusionArgumentResolver resolver = new ContentInclusionArgumentResolver()

        and :
            NativeWebRequest request = mockRequest null

        when :
            def res = resolver.resolveArgument(null, null, request, null)

        then :
            res == resolver.defaultInclusion

    }

    @Unroll("should resolve the inclusion for value #input as #result")
    def "should resolve inclusion value"(){

        expect :
            new ContentInclusionArgumentResolver()
                    .resolveArgument(null, null, request, null) == result

        where :
            input << ['root', 'nested']
            result << [ContentInclusion.ROOT, ContentInclusion.NESTED]
            request << [mockRequest(ContentInclusion.ROOT), mockRequest(ContentInclusion.NESTED)]

    }


    def "should rant if an unsupported value is given"(){

        given :
            def request = mockRequest "blablabla"
            def resolver = new ContentInclusionArgumentResolver()

        when :
            resolver.resolveArgument(null, null, request, null)

        then :
            thrown IllegalArgumentException;

    }

    def mockRequest(expected){
        def request = Mock(NativeWebRequest)
        request.getParameter(_) >> expected
        request
    }

}
