== Projects

This chapter focuses on services for the projects.

=== image:get.png[] Get all projects

A `GET` to `/projects` returns all the projects (standard and template) that the user is allowed to read. A parameter can be specified to retrieve only standard projects or template ones.


==== Get all projects (included project template)

operation::RestProjectControllerIT/browse-project[snippets='http-request,http-response,response-fields,links']


==== Get only standard projects (without project template)

operation::RestProjectControllerIT/browse-standard-project[snippets='http-request,request-parameters,http-response,response-fields,links']


==== Get only project templates

operation::RestProjectControllerIT/browse-project-template[snippets='http-request,request-parameters,http-response,response-fields,links']

=== image:post.png[] Create project

A `POST` to `/projects` creates a new project.


==== Create new project

operation::RestProjectControllerIT/post-project[snippets='http-request,request-fields,request-parameters,http-response,response-fields,links']


==== Create new project using template

operation::RestProjectControllerIT/post-project-from-template[snippets='http-request,request-fields,request-parameters,http-response,response-fields,links']

=== image:post.png[] Create project template

A `POST` to `/projects` creates a new project template.


==== Create new template

operation::RestProjectControllerIT/post-project-template[snippets='http-request,request-fields,request-parameters,http-response,response-fields,links']


==== Create new template from existing project

operation::RestProjectControllerIT/post-project-template-from-project[snippets='http-request,request-fields,request-parameters,http-response,response-fields,links']

=== image:get.png[] Get project

A `GET` to `/projects/{id}` returns the project with the given id. This retrieves a project administration data and is only authorized to administrators.

operation::RestProjectControllerIT/get-project[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== image:get.png[] Get project by name

A `GET` to `/projects` with a request parameter `projectName` returns the project with the given name.
This retrieves a project administration data and is only authorized to administrators.
Be careful, both the name of the parameter `projectName` and the value of the project name are case-sensitive.

operation::RestProjectControllerIT/get-project-by-name[snippets='http-request,request-parameters,http-response,response-fields,links']


=== image:get.png[] Get project clearances

A `GET` to `/projects/{id}/clearances` returns the clearances grouped by profiles for the project with the given id.

operation::RestProjectControllerIT/get-project-clearances[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

==== Get project clearances (deprecated)

A `GET` to `/projects/{id}/permissions` returns the clearances grouped by profiles for the project with the given id.

This endpoint is still functional but is deprecated. Please use the more comprehensive `/projects/{id}/clearances` endpoint instead.

operation::RestProjectControllerIT/get-project-permissions[snippets='path-parameters,http-request,request-parameters,http-response,links']


=== image:post.png[] Add clearances to project

A `POST` to `/projects/{projectId}/clearances/{profileId}/users/{partyIds}` adds users or teams to the profile for the project with the given id.

operation::RestProjectControllerIT/add-new-clearance-to-project[snippets='path-parameters,http-request,http-response,response-fields,links']

==== Add clearances to project (deprecated - only for system profiles)

A `POST` to `/projects/{id}/permissions/{permissionGroup}` adds users or teams to the system profile for the project with the given id.

The possible {permissionGroup} are test_editor, project_viewer, project_manager, test_runner, test_designer, advanced_tester and validator.

This endpoint is still functional but is deprecated. Please use the more comprehensive `/projects/{projectId}/clearances/{profileId}/users/{partyIds}` endpoint instead.

operation::RestProjectControllerIT/add-new-permission-to-project[snippets='path-parameters,http-request,request-parameters,http-response,links']


=== image:delete.png[] Delete clearances in project

A `DELETE` to `/projects/{projectId}/users/{partyIds}` deletes users/teams with the given ids (separated with comma).

operation::RestProjectControllerIT/delete-project-party[snippets='path-parameters,http-request']


=== image:get.png[] Get campaigns of project

A `GET` to `/projects/{id}/campaigns` returns the campaigns in the project with the given id.

operation::RestProjectControllerIT/get-project-campaigns[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== image:get.png[] Get requirements of project

A `GET` to `/projects/{id}/requirements` returns the requirements in the project with the given id.

operation::RestProjectControllerIT/get-project-requirements[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== image:get.png[] Get test cases of project

A `GET` to `/projects/{id}/test-cases` returns the test cases in the project with the given id.

operation::RestProjectControllerIT/get-project-test-cases[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== image:get.png[] Get custom report library contents

A `GET` to `/projects/{id}/reporting-library/content` returns the contents of the custom report library in the project with the given id.

operation::RestProjectControllerIT/browse-reporting-library-content[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== image:get.png[] Get execution library contents

A `GET` to `/projects/{id}/campaigns-library/content` returns the contents of the execution library in the project with the given id.

operation::RestProjectControllerIT/browse-execution-library-content[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== image:get.png[] Get requirement library contents

A `GET` to `/projects/{id}/requirements-library/content` returns the contents of the requirement library in the project with the given id.

operation::RestProjectControllerIT/browse-requirement-library-content[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== image:get.png[] Get test case library contents

A `GET` to `/projects/{id}/test-cases-library/content` returns the contents of the test case library in the project with the given id.

operation::RestProjectControllerIT/browse-test-case-library-content[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']
