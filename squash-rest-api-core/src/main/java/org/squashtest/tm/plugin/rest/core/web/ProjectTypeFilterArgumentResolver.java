/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.web;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * @author gfaucher - created on 28/04/2022
 */
public class ProjectTypeFilterArgumentResolver implements HandlerMethodArgumentResolver {

    private ProjectTypeFilter defaultFilter = ProjectTypeFilter.ALL;
    private String projectTypeFilter = UriComponents.PROJECT_TYPE_FILTER;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return ProjectTypeFilter.class.equals(parameter.getParameterType());
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String value = webRequest.getParameter(projectTypeFilter);

        if (value == null) {
            return defaultFilter;
        }

        try {

        return ProjectTypeFilter.valueOf(value.toUpperCase());

        } catch (IllegalArgumentException ex) {

            throw new IllegalArgumentException("Invalid value '"+value+"' for request parameter '" + projectTypeFilter+
                    "'. Must be one of these values: '"+ UriComponents.PROJECT_TYPE_FILTER_ALL +"', '"+ UriComponents.PROJECT_TYPE_FILTER_STANDARD +
                    "', '"+ UriComponents.PROJECT_TYPE_FILTER_TEMPLATE +"'");
        }
    }

    public ProjectTypeFilter getDefaultFilter() {
        return defaultFilter;
    }

    public void setDefaultFilter(ProjectTypeFilter defaultFilter) {
        this.defaultFilter = defaultFilter;
    }

    public String getProjectTypeFilter() {
        return projectTypeFilter;
    }

    public void setProjectTypeFilter(String projectTypeFilter) {
        this.projectTypeFilter = projectTypeFilter;
    }

}
