/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest

import org.apache.commons.collections.CollectionUtils
import org.squashtest.tm.core.foundation.lang.Couple
import spock.lang.Specification

class TestEqualsHashcode extends Specification {


    def "simple test of element belonging to collection"(){

        when:

        // la DB
        def listOfCouples = [new Couple<>(1L, 2L), new Couple(2L, 2L), new Couple(3L, 2L)]

        // le JSON
        def jsonParam1 = new Couple(2L, 2L)
        def jsonParam2 = new Couple(6L, 3L)

        def allParams = [jsonParam1, jsonParam2]

        then:

        listOfCouples.contains(jsonParam1)
        ! listOfCouples.contains(jsonParam2)

        ! listOfCouples.containsAll(allParams)

        def difference = CollectionUtils.subtract(allParams, listOfCouples)

        println difference

    }

}
