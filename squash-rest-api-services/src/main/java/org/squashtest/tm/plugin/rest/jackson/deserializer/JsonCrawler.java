/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.deserializer;

import com.fasterxml.jackson.core.JsonPointer;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.Identified;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Arrays;

@Component
public class JsonCrawler {

    @PersistenceContext
    private EntityManager entityManager;

    public String asMandatoryString(JsonNode jsonNode, String... path){
        JsonNode targetNode = getMandatoryTargetNode(jsonNode, path);
        String value = targetNode.asText();

        if(StringUtils.isBlank(value)){
            missingMandatory(jsonNode, path);
        }

        return value;
    }
    public Long asMandatoryLong(JsonNode jsonNode, String... path){
        JsonNode targetNode = getMandatoryTargetNode(jsonNode, path);
        Long value = targetNode.asLong();
        return value;
    }

    private JsonNode getMandatoryTargetNode(JsonNode jsonNode, String[] path) {
        JsonPointer jsonPointer = computePointer(path);
        JsonNode targetNode = jsonNode.at(jsonPointer);

        if(targetNode == null){
            missingMandatory(jsonNode, path);
        }
        return targetNode;
    }

    private void missingMandatory(JsonNode jsonNode, String[] path) {
        String message = String.format("Mandatory attribute %s in object %s is missing.", Arrays.toString(path), jsonNode.asText());
        throw new IllegalArgumentException(message);
    }

    public boolean hasValidId(JsonNode jsonNode){
        return hasValidId(jsonNode, new String[0]);
    }

    public boolean hasValidId(JsonNode jsonNode, String... path){
        JsonPointer jsonPointer = computeIdPointer(path);
        JsonNode node = jsonNode.at(jsonPointer);
        if(node == null){
            return false;
        }

        if(!node.canConvertToLong()){
            String message = "Invalid Id format. All ids must be provided as numbers";
            throw new IllegalArgumentException(message);
        }
        return true;
    }

    public <T extends Identified> T findMandatoryEntity(JsonNode jsonNode, Class<T> clazz) {
        return findMandatoryEntity(jsonNode,clazz, new String[0]);
    }

    public <T extends Identified> T findMandatoryEntity(JsonNode jsonNode, Class<T> clazz, String... path) {

        JsonPointer jsonPointer = computeIdPointer(path);
        long primaryKey = getPrimaryKey(jsonNode, clazz, jsonPointer);
        T entity = entityManager.find(clazz, primaryKey);

        if (entity == null) {
            String message = String.format("The json object with path : %s is not valid. No object %s found with id %d", jsonPointer, clazz.getName(), primaryKey);
            throw new IllegalArgumentException(message);
        }

        return entity;
    }

    private <T extends Identified> long getPrimaryKey(JsonNode jsonNode, Class<T> clazz, JsonPointer jsonPointer) {
        JsonNode targetNode = jsonNode.at(jsonPointer);

        if (targetNode == null) {
            String message = String.format("The json object with path : %s is null. Must provide a %s object with an id attribute inside.", jsonPointer, clazz.getName());
            throw new IllegalArgumentException(message);
        }

        if (!targetNode.canConvertToLong()) {
            String message = String.format("The json object with path : %s is not a number. Must provide a %s object with a valid id attribute inside.", jsonPointer, clazz.getName());
            throw new IllegalArgumentException(message);
        }

        return targetNode.asLong();
    }

    public JsonPointer computeIdPointer(String[] path) {

        if (path == null ||path.length == 0) {
            return JsonPointer.compile("/id");
        }

        String pointer = StringUtils.join(path, "/");

        if (!"id".equals(path[path.length - 1])) {//we add id only if user have forgot it
            pointer = StringUtils.appendIfMissing(pointer, "/id");
        }

        pointer = StringUtils.prependIfMissing(pointer, "/");

        return JsonPointer.compile(pointer);
    }

    public JsonPointer computePointer(String[] path) {
        if(path.length == 0){
            throw new IllegalArgumentException("Programmatic error, you should provide a not empty path");
        }

        JsonPointer jsonPointer = JsonPointer.compile(StringUtils.prependIfMissing(path[0],"/"));

        for (int i = 1; i < path.length; i++) {
            JsonPointer tail = JsonPointer.compile(StringUtils.prependIfMissing(path[0], "/"));
            jsonPointer = jsonPointer.append(tail);
        }
        return jsonPointer;
    }
}
