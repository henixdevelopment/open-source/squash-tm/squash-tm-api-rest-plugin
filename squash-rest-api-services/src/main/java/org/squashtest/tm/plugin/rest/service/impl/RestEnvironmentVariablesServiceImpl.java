/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.customfield.BoundEntity;
import org.squashtest.tm.domain.environmenttag.DenormalizedEnvironmentHolderType;
import org.squashtest.tm.domain.environmentvariable.DenormalizedEnvironmentVariable;
import org.squashtest.tm.plugin.rest.service.RestEnvironmentVariablesService;
import org.squashtest.tm.service.internal.repository.DenormalizedEnvironmentVariableDao;

import javax.inject.Inject;
import java.util.List;

@Service
@Transactional
public class RestEnvironmentVariablesServiceImpl implements RestEnvironmentVariablesService {

    @Inject
    private DenormalizedEnvironmentVariableDao denormalizedEnvironmentVariableDao;
    @Override
    @Transactional(readOnly = true)
    public List<DenormalizedEnvironmentVariable> findAllDenormalizedEnvironmentVariableBoundToExecution(BoundEntity boundEntity) {
        return denormalizedEnvironmentVariableDao.findAllByHolderIdAndHolderType(boundEntity.getId(), DenormalizedEnvironmentHolderType.AUTOMATED_EXECUTION_EXTENDER);
    }


}
