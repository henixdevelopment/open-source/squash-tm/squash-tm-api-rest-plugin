/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.plugin.rest.jackson.serializer.AttachmentHolderPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonPropertyOrder({"_type","id","name","reference","due_date","session_duration","session_status","comments","charter","iteration_test_plan_item"})
@JsonTypeName("exploratory-session-overview")
@JsonAppend(props = {
    @JsonAppend.Prop(name = "attachments", value = AttachmentHolderPropertyWriter.class)
})
public abstract class RestExploratorySessionOverviewMixin extends RestIdentifiedMixin {

    @JsonProperty("iteration_test_plan_item")
    @JsonSerialize(converter= HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private IterationTestPlanItem iterationTestPlanItem;

    @JsonProperty("charter")
    private String charter;

    @JsonProperty("session_duration")
    private Integer sessionDuration;

    @JsonProperty("name")
    private String name;

    @JsonProperty("reference")
    private String reference;

    @JsonProperty("due_date")
    private Date dueDate;

    @JsonProperty("session_status")
    private String sessionStatus;

    @JsonProperty("comments")
    private String comments;
}
