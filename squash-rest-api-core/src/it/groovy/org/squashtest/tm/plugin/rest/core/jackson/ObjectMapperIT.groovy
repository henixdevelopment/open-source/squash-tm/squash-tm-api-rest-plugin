/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson

import com.fasterxml.classmate.TypeResolver
import com.fasterxml.jackson.annotation.JsonAutoDetect
import com.fasterxml.jackson.annotation.JsonFilter
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.transform.AutoClone
import groovy.transform.EqualsAndHashCode
import org.springframework.beans.factory.annotation.Qualifier
import org.squashtest.tm.domain.Identified
import org.squashtest.tm.plugin.rest.core.testsetup.BaseITSpec

import javax.inject.Inject
import javax.persistence.Entity


class ObjectMapperIT extends BaseITSpec{

    @Inject
    @Qualifier("basicRestObjectMapper")
    private ObjectMapper mapper

    def "should deserialize-update only according to deserialization filter"(){

        /*
         * Note : that test execute two consecutive runs to verify that Jackson
         * does not cache de modified deserializers
         */

        given :
            mapper.addMixIn(Guy, RestGuyMixin)

        and :

        def json = """{
    "name" : "Bob",
    "age" : 21,
    "hobby" : "photobombing random bystanding tourists"
}"""

        and :
        def target = new Guy(id:83, name :"Mike", age : 15, hobby : "astrophysics")

        and :

        // run 1
        def hints1 = new DeserializationHints(
                targetEntity: target.clone(),
                mode: DeserializationHints.Mode.DESERIALIZE_UPDATE,
                filter: new DeserializationDynamicFilter("age, hobby")
        )

        // run 2
        def hints2 = new DeserializationHints(
                targetEntity: target.clone(),
                mode: DeserializationHints.Mode.DESERIALIZE_UPDATE,
                filter: new DeserializationDynamicFilter("name")
        )

        when :

        def res1 = mapper.reader()
                .withAttribute(DeserializationHints.HINTS_KEY, hints1)
                .forType(mapper.typeFactory.constructType(TypeResolver.forName(Guy.class.name)))
                .readValue(json)

        def res2 = mapper.reader()
                .withAttribute(DeserializationHints.HINTS_KEY, hints2)
                .forType(mapper.typeFactory.constructType(TypeResolver.forName(Guy.class.name)))
                .readValue(json);


        then :
        // result 1 : according to the filter, only age and hobby are modified
        res1 == target.clone().with { age = 21; hobby = "photobombing random bystanding tourists"; return it }

        // result 2 : according to the filter, only name is modified
        res2 == target.clone().with { name = "Bob"; return it }

    }

    def "wrapped persistent beans should still be loaded by PersistentValueInstantiator"(){


        given :
        mapper.addMixIn(Guy, RestGuyMixin)

        and :

        def json = """{
    "name" : "Bob",
    "age" : 21,
    "hobby" : "photobombing random bystanding tourists"
}"""

        and :
        def target = new Guy(id:83, name :"Mike", age : 15, hobby : "astrophysics")

        and :

        def hints = new DeserializationHints(
                targetEntity: target.clone(),
                mode: DeserializationHints.Mode.DESERIALIZE_UPDATE,
                filter: new DeserializationDynamicFilter("age, hobby")
        )

        when :

        def res = mapper.reader()
                .withAttribute(DeserializationHints.HINTS_KEY, hints)
                .forType(mapper.typeFactory.constructType(TypeResolver.forName(WrappedGuy.class.name)))
                .readValue(json)

        then :
        res.wrapped == target.clone().with { age = 21; hobby = "photobombing random bystanding tourists"; return it }

    }



    @EqualsAndHashCode(includeFields=true)
    @AutoClone
    @Entity // @Entity is needed by PersistentValueInstantiator
    private static final class Guy implements Identified{
        Long id
        String name
        int age
        String hobby
    }

    @JsonFilter(SerializationDynamicFilter.FILTER_ID)
    @JsonAutoDetect
    private static final class RestGuyMixin{

    }

    @EqualsAndHashCode(includeFields=true)
    @AutoClone
    private static class WrappedGuy extends WrappedDTO<Guy>{
        String wrapperProperty = "hey !"
    }
}
