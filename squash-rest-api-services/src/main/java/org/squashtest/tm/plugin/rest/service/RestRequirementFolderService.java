/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.resource.Resource;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementFolderDto;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public interface RestRequirementFolderService {
	
	/**
	 * Returns a RequirementFolder, given its id
	 * @param id
	 * @return
	 */
	RequirementFolder getOne(Long id);
	/**
	 * Returns a paged list of all the RequirementFolders, as long as the requestor can read them
	 *
	 * @param pageable
	 * @return
	 */
	Page<RequirementFolder> findAllReadable(Pageable pageable);

	/**
	 * Returns a paged list of all the RequirementFolders, as long as the requestor can read them
	 *
	 * @param projectIds
	 * @return
	 */
	List<RequirementFolder> findAllRequirementFoldersByPojectIds(List<Long> projectIds);
	
	/**
	 * Returns the paged content of a RequirementFolder, given its id
	 * 
	 * @param folderId
	 * @param pageable
	 * @return
	 */
	Page<RequirementLibraryNode<Resource>> findFolderContent(long folderId, Pageable pageable);
	
	
	/**
	 * Returns the paged content of a folder, including every descendant nodes (this also includes
	 * sub requirements of composite requirements)
	 * 
	 * @param folderId
	 * @param pageable
	 * @return
	 */
	Page<RequirementLibraryNode<Resource>> findFolderAllContent(long folderId, Pageable pageable);

	RequirementFolder addRequirementFolder(RequirementFolderDto folderDto )throws InvocationTargetException, IllegalAccessException;

	RequirementFolder patchRequirementFolder(RequirementFolderDto patch, long id);

	void deleteFolder( List<Long> folderIds);
}
