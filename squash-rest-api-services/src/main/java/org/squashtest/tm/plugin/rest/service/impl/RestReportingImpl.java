/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.plugin.rest.service.RestReportingService;
import org.squashtest.tm.service.internal.customreport.CRLNDeletionHandler;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class RestReportingImpl implements RestReportingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestReportingImpl.class);

    @Inject private DSLContext dslContext;

    @Inject private CRLNDeletionHandler deletionHandler;

    @Inject protected PermissionEvaluationService permissionService;


    @Override
    public List<Long> fetchAndDeleteReportingDeletableIds(List<Long> ids, String entityType) throws AccessDeniedException {
        List<Long> deletableIds = dslContext.select(Tables.CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID)
            .from(Tables.CUSTOM_REPORT_LIBRARY_NODE)
            .where(Tables.CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID.in(ids))
            .and(Tables.CUSTOM_REPORT_LIBRARY_NODE.ENTITY_TYPE.eq(entityType))
            .fetchInto(Long.class);

        List<Long> authorizedIds = new ArrayList<>();
        for (Long id : deletableIds) {
            // Permissions are checked per ID instead of on deletableIds to handle permission error alongside other
            // errors in fetchAndDeleteReportingDeletableIds, allowing partial processing of valid IDs.
            try {
                PermissionsUtils.checkPermission(
                    permissionService,
                    Collections.singletonList(id),
                    Permissions.DELETE.name(),
                    CustomReportLibraryNode.class.getName()
                );
                authorizedIds.add(id);
            } catch (AccessDeniedException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        deletionHandler.deleteNodes(authorizedIds);

        return authorizedIds;
    }

    @Override
    public CustomReportLibraryNode getReportingEntity(Long id, String entityType) {

        PermissionsUtils.checkPermission(
            permissionService,
            Collections.singletonList(id),
            Permissions.READ.name(),
            CustomReportLibraryNode.class.getName());

        CustomReportLibraryNode customReportLibraryNode = getCustomReportLibraryNode(id, entityType);
        if (customReportLibraryNode == null) {
            throw new EntityNotFoundException("Unable to find a custom library node of type " + entityType.toLowerCase() + " with id " + id );
        }
        return customReportLibraryNode;
    }

    private CustomReportLibraryNode getCustomReportLibraryNode(Long id, String entityType) {

        return dslContext.select(
                Tables.CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID.as(RequestAliasesConstants.ID),
                Tables.CUSTOM_REPORT_LIBRARY_NODE.ENTITY_TYPE.as("ENTITY_TYPE"),
                Tables.CUSTOM_REPORT_LIBRARY_NODE.NAME.as(RequestAliasesConstants.NAME)
            )
            .from(Tables.CUSTOM_REPORT_LIBRARY_NODE)
            .where(Tables.CUSTOM_REPORT_LIBRARY_NODE.CRLN_ID.eq(id))
            .and(Tables.CUSTOM_REPORT_LIBRARY_NODE.ENTITY_TYPE.eq(entityType))
            .fetchOneInto(CustomReportLibraryNode.class);
    }
}
