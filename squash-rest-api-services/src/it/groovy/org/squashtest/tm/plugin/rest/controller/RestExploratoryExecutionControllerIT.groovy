/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.execution.ExploratoryExecutionEventType
import org.squashtest.tm.domain.execution.ExploratoryExecutionRunningState
import org.squashtest.tm.domain.execution.SessionNote
import org.squashtest.tm.domain.execution.SessionNoteKind
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto
import org.squashtest.tm.plugin.rest.service.RestExploratoryExecutionService
import org.squashtest.tm.plugin.rest.service.RestIssueService
import org.squashtest.tm.plugin.rest.service.RestSessionNoteService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.denormalizedfield.DenormalizedFieldValueManager
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder

@WebMvcTest(RestExploratoryExecutionController)
class RestExploratoryExecutionControllerIT extends BaseControllerSpec {

    @Inject
    private RestExploratoryExecutionService service

    @Inject
    private CustomFieldValueFinderService cufService

    @Inject
    private DenormalizedFieldValueManager denormCufService

    @Inject
    private RestIssueService restIssueService

    @Inject
    private RestSessionNoteService sessionNoteService

    @Inject
    private RestExploratoryExecutionController controller


    def "get-exploratory-execution"() {
        given:
        def execution = SquashEntityBuilder.exploratoryExecution {
            id = 1L
            reference = "REF 1"
            name = "execution 1"
            progressStatus = ExploratoryExecutionRunningState.RUNNING
            lastExecutedBy = "User 1"
            lastExecutedOn "2023/11/03"
            taskDivision = "User 1 : Test the buttons, User 2 : test the login form"
            reviewed = true
            description = "no comment"
            sessionNotes = [SquashEntityBuilder.sessionNote {
                id = 1L
                kind = SessionNoteKind.SUGGESTION
                content = "Suggestion 1"
            }]
            testPlan = SquashEntityBuilder.iterationTestPlanItem {
                id = 1L
                iteration = SquashEntityBuilder.iteration {
                    campaign = SquashEntityBuilder.campaign {
                        project = SquashEntityBuilder.project {
                            id = 1L
                        }
                    }
                }
                exploratorySessionOverview = SquashEntityBuilder.exploratorySession {
                    id = 1L
                }
            }
            events = [
                SquashEntityBuilder.exploratoryExecutionEvent {
                    date "2023/11/03"
                    eventType = ExploratoryExecutionEventType.START
                },
                SquashEntityBuilder.exploratoryExecutionEvent {
                    date "2023/11/04"
                    eventType = ExploratoryExecutionEventType.PAUSE
                },
                SquashEntityBuilder.exploratoryExecutionEvent {
                    date "2023/11/05"
                    eventType = ExploratoryExecutionEventType.RESUME
                }
            ]
        }

        def charter = "this is an Exploratory Test Case Charter"

        def execCufs = [
            SquashEntityBuilder.cufValue {
                label = "cuf text"
                code = "CUF_TXT"
                value = "cuf text value"
            },
            SquashEntityBuilder.cufValue {
                label = "cuf text 2"
                code = "CUF_TXT_2"
                value = "cuf text value 2"
            }
        ] as List

        def tcCufs = [
            SquashEntityBuilder.denormCufValue {
                label = "tc cuf text"
                code = "TC_CUF_TXT"
                value = "tc cuf text value"
            },
            SquashEntityBuilder.denormCufValue {
                label = "tc cuf text 2"
                code = "TC_CUF_TXT_2"
                value = "tc cuf text value 2"
            }
        ] as List

        and:
        service.getOne(execution.id) >> execution
        service.getCharterByExploratoryExecutionId(execution.id) >> charter
        cufService.findAllCustomFieldValues(execution) >> execCufs
        denormCufService.findAllForEntity(execution) >> tcCufs

        when:
        def res = mockMvc.perform (RestDocumentationRequestBuilders.get("/api/rest/latest/exploratory-executions/{id}", 1L)
            .header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk()).andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "exploratory-execution"
            "id".is 1
            "reference".is "REF 1"
            "name".is "execution 1"
            "execution_mode".is "EXPLORATORY"
            "progress_status".is "RUNNING"
            "last_executed_by".is "User 1"
            "last_executed_on".is "2023-11-03T11:00:00.000+00:00"
            "charter".is "this is an Exploratory Test Case Charter"
            "task_division".is "User 1 : Test the buttons, User 2 : test the login form"
            "session_notes".hasSize(1)
            "session_notes[0]".test {
                "id".is 1
                "kind".is "SUGGESTION"
                "content".is "Suggestion 1"
                selfRelIs("http://localhost:8080/api/rest/latest/session-notes/1")
            }
            "reviewed".is true
            "comment".is "no comment"
            selfRelIs("http://localhost:8080/api/rest/latest/exploratory-executions/1")
            "_links".test {
                "issues.href".is "http://localhost:8080/api/rest/latest/exploratory-executions/1/issues"
            }
        }

        /*
         * Documentation
         */

        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the exploratory execution"
                }
                fields {
                    add "_type (string) : the type of the entity"
                    add "id (number) : the id of the exploratory execution"
                    add "reference (string) : the reference of the exploratory execution"
                    add "name (string) : the name of the exploratory execution"
                    add "execution_mode (string) : the execution mode of the exploratory execution"
                    add "progress_status (string) : the progress status of the exploratory execution"
                    add "last_executed_by (string) : the user who last executed this execution"
                    add "last_executed_on (string) : the date this execution was last executed"
                    add "charter (string) : the charter for the exploratory execution"
                    add "task_division (string) : the task division regarding the exploratory execution"
                    addAndStop "session_notes (array) : the session notes of the exploratory execution. Please refer to the session note documentation for more details."
                    add "reviewed (boolean) : whether the exploratory execution has been reviewed"
                    add "comment (string) : any comments added to the exploratory execution"
                    addAndStop"custom_fields (array) : the custom fields for the exploratory session. Please refer to the custom field documentation for more details."
                    addAndStop"test_case_custom_fields (array) : the custom fields for the referenced test case. Please refer to the custom field documentation for more details."
                    addAndStop"attachments (array) : the attachments of the exploratory session. Please refer to the attachment documentation for more details."
                    add DescriptorLists.linksFields
                }
                _links {
                    add "self : the link to this execution"
                    add "test_plan_item : the link to its test plan item"
                    add "project : the link to its project"
                    add "exploratory_session : the link to its exploratory session"
                    add "attachments : the link to an execution's attachments"
                    add "issues : link to the issues of this exploratory execution"
                }
            }
        ))
    }

    def "get-exploratory-execution-issues"() {
        given:

        def sessionNote = [SquashEntityBuilder.sessionNote {
            id = 1L
            kind = SessionNoteKind.SUGGESTION
            content = "Suggestion 1"
        }]


        def execution = SquashEntityBuilder.exploratoryExecution {
                id = 1L
                reference = "REF 1"
                name = "execution 1"
                sessionNotes = sessionNote
        }


        def issue = new IssueDto(
            remoteIssueId: "165",
            url:new URL("http://192.175.1.51/bugzilla/show_bug.cgi?id=165"),
            executions:[],
            requirements: [],
            sessionNotes: sessionNote as Set<SessionNote>
        )

        def issues = new PageImpl<IssueDto>([issue], PageRequest.of(0, 20), 1)


        and:
        service.getOne(_) >> execution
        restIssueService.getIssuesFromExecutionIds(_, _) >> issues
        sessionNoteService.getSessionNotesFromIssue(_,_) >> sessionNote
        controller.findExecutionsIssues(_,_) >> issues


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders
            .get("/api/rest/latest/exploratory-executions/{id}/issues", 1)
            .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.issues".hasSize 1
            "_embedded.issues".test {
                "[0]".test {
                    "remoteIssueId".is "165"
                    "url".is "http://192.175.1.51/bugzilla/show_bug.cgi?id=165"
                    "session-notes".hasSize 1
                    "session-notes".test {
                        "[0]".test {
                            "_type".is "session-note"
                            "id".is 1
                            selfRelIs("http://localhost:8080/api/rest/latest/session-notes/1")
                        }
                    }
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/exploratory-executions/1/issues?page=0&size=20"
            "page".test {
                "size".is 20
                "totalElements".is 1
                "totalPages".is 1
                "number".is 0
            }

        }


        /*
     * Documentation
     */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the exploratory execution"
                }
                requestParams {
                    add DescriptorLists.paginationParams
                    add DescriptorLists.fieldsParams
                }
                fields {
                    embeddedAndStop "issues (array) : the issues of this exploratory execution"
                    add "_embedded.issues[].remoteIssueId (string) : the remote issue id of the issue linked to the exploratory execution."
                    add "_embedded.issues[].url (string) : the url of the issue linked to the exploratory execution."
                    add "_embedded.issues[].session-notes (array) : the session notes linked to the issue. If the issue is not linked to any session notes, this field will not appear."
                    add DescriptorLists.paginationFields
                    add DescriptorLists.linksFields
                }
                _links {
                    add DescriptorLists.paginationLinks
                }
            }
        ))
    }
}

