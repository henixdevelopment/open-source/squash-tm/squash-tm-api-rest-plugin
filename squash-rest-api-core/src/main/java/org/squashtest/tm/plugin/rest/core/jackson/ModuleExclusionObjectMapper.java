/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashSet;
import java.util.Set;

/**
 * Object mapper that won't register the standard SquashModule, because we want to avoid any conflict with the configuration we want 
 * to set for the REST API.
 * 
 * @author bsiri
 *
 */
@SuppressWarnings("serial")
public class ModuleExclusionObjectMapper extends ObjectMapper {
	private static final Set<String> UNWANTED_MODULES;
	
	static {
		UNWANTED_MODULES = new HashSet<>(1);
		UNWANTED_MODULES.add("SquashModule");
	}
	
			
	public ModuleExclusionObjectMapper() {
		super();
	}

	public ModuleExclusionObjectMapper(ObjectMapper src) {
		super(src);
	}

	@Override
	public ObjectMapper registerModule(Module module){
		String modulename = module.getModuleName();
		if (UNWANTED_MODULES.contains(modulename)){
			return this;
		}
		else{
			return super.registerModule(module);
		}
	}
	
	// "copy" has to be overriden, see superclass to know why
	@Override
	public ObjectMapper copy(){
		return new ModuleExclusionObjectMapper(this);
	}


}
