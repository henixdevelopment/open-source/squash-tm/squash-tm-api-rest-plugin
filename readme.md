# Squash TM REST API plugin

## Validation

The validation of request bodies can be done using the `@Valid` annotation on the request body parameter of the controller method and
by adding the `@Validated` annotation on the controller class. This will trigger the validation of the request body using the constraints
defined in the model classes (JSR-303 annotations such as `@NotNull`, `@Size`, etc.).

For more complex validation, the Spring validation framework can be used. To do so, create a class that implements the `Validator` interface.
This class can be a Spring bean (to allow injection).

Both approaches can be combined.

## useful maven goals

* **from service module** : running the integration tests and generate the spring-restdoc : `mvn install -Pintegration-tests`
* **from root module** : create a distribution : `mvn install -Pdistro,integration-tests`. The profile `distro` explicitly
enforces the usage of `integration-tests` to make sure the documentation will be generated
