/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.squashtest.tm.plugin.rest.jackson.serializer.TypePropertyWriter;

import java.util.List;

@JsonAutoDetect(fieldVisibility= JsonAutoDetect.Visibility.NONE, getterVisibility= JsonAutoDetect.Visibility.NONE, isGetterVisibility= JsonAutoDetect.Visibility.NONE)
@JsonTypeName("project")
@JsonAppend(prepend = true, props = @JsonAppend.Prop(name = "_type", value = TypePropertyWriter.class))
@JsonPropertyOrder({"_type","id","name"})
public class ProjectTreeDto implements Comparable<ProjectTreeDto> {

    @JsonProperty
    private Long id;
    @JsonProperty
    private String name;
    @JsonProperty
    private List<FolderTreeDto> folders;

    public ProjectTreeDto() {
    }

    public ProjectTreeDto(Long id, String name, List<FolderTreeDto> folders) {
        this.id = id;
        this.name = name;
        this.folders = folders;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<FolderTreeDto> getFolders() {
        return folders;
    }

    public void setFolders(List<FolderTreeDto> folders) {
        this.folders = folders;
    }

    @Override
    public int compareTo(ProjectTreeDto p) {
        return this.getId().compareTo(p.getId());
    }
}
