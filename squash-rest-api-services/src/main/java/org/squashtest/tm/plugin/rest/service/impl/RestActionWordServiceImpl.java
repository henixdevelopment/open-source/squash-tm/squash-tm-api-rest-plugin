/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.core.foundation.collection.ColumnFiltering;
import org.squashtest.tm.domain.actionword.ActionWordLibraryNode;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.plugin.rest.jackson.model.RestActionWordDto;
import org.squashtest.tm.plugin.rest.repository.RestActionWordRepository;
import org.squashtest.tm.plugin.rest.service.RestActionWordService;
import org.squashtest.tm.service.actionword.ActionWordLibraryNodeService;
import org.squashtest.tm.service.annotation.IsUltimateLicenseAvailable;
import org.squashtest.tm.service.internal.repository.KeywordTestStepDao;
import org.squashtest.tm.service.internal.repository.UsingTestCaseDao;
import org.squashtest.tm.service.project.ProjectFinder;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.squashtest.tm.plugin.rest.utils.PaginationUtils.emptyPage;
import static org.squashtest.tm.service.security.Authorizations.OR_HAS_ROLE_ADMIN;

@Service
@Transactional
public class RestActionWordServiceImpl implements RestActionWordService {

    @Inject
    private RestActionWordRepository actionWordDao;

    @Inject
    private ProjectFinder projectFinder;

    @Inject
    private ActionWordLibraryNodeService actionWordLibraryNodeService;

    @Inject
    private KeywordTestStepDao keywordTestStepDao;

    @Autowired(required = false)
    private UsingTestCaseDao usingTestCaseDao;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("hasPermission(#actionWordId, 'org.squashtest.tm.domain.bdd.ActionWord', 'READ')" + OR_HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public ActionWord getOne(Long actionWordId) {
        return actionWordDao.getReferenceById(actionWordId);
    }

    @Override
    @Transactional(readOnly = true)
    @IsUltimateLicenseAvailable
    public Page<ActionWord> findAllReadableActionWords(Pageable pageable) {
        Collection<Long> projectIds = projectFinder.findReadableProjectIdsOnActionWordLibrary();
        return projectIds.isEmpty() ? emptyPage(pageable) : actionWordDao.findAllInProjects(projectIds, pageable);
    }

    @Override
    @PreAuthorize("hasPermission(#id, 'org.squashtest.tm.domain.bdd.ActionWord', 'WRITE')" + OR_HAS_ROLE_ADMIN)
    @IsUltimateLicenseAvailable
    public ActionWord patchActionWord(long id, RestActionWordDto patch) {
        final ActionWord actionWord = actionWordDao.getReferenceById(id);
        actionWord.setDescription(patch.getDescription());
        return actionWord;
    }

    @Override
    @IsUltimateLicenseAvailable
    public ActionWord createActionWord(RestActionWordDto post) {
        final ActionWord actionWord = post.convertToDto();
        Project project = entityManager.find(Project.class, post.getProject().getId());
        ActionWordLibraryNode actionWordLibraryNode =
                actionWordLibraryNodeService.findNodeFromEntity(project.getActionWordLibrary());
        actionWordLibraryNodeService.createNewNode(actionWordLibraryNode.getId(), actionWord);
        return actionWord;
    }

    @Override
    @IsUltimateLicenseAvailable
    public List<Long> deleteActionWordsByIds(List<Long> actionWordIds) {
        List<Long> actionWordLibraryNodeIds = actionWordDao.getNodeIdsFromActionWordIds(actionWordIds);
        actionWordLibraryNodeService.delete(actionWordLibraryNodeIds);
        return findActionWordsAssociatedToKeywordTestSteps(actionWordIds);
    }

    private List<Long> findActionWordsAssociatedToKeywordTestSteps(List<Long> actionWordIds) {
        return actionWordIds
                .stream()
                .filter(actionWordId -> !keywordTestStepDao.findByActionWord(actionWordId).isEmpty())
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    @IsUltimateLicenseAvailable
    public Page<ActionWord> findAllActionWordsByProject(long projectId, Pageable pageable) {
        List<Long> readableProjectIds = projectFinder.findReadableProjectIdsOnActionWordLibrary();

        if (!readableProjectIds.contains(projectId)) {
            throw new AccessDeniedException("Access denied");
        }

        return actionWordDao.findAllInProject(projectId, pageable);
    }

    @Override
    @IsUltimateLicenseAvailable
    public List<TestCase> findAllByActionWord(long actionWordId) {
        return getUsingTestCaseDao().findAllByActionWord(actionWordId);
    }

    @Override
    @IsUltimateLicenseAvailable
    public Page<TestCase> findAllByActionWord(long id, Pageable pageable, ColumnFiltering filtering) {
        return getUsingTestCaseDao().findPageByActionWord(id, pageable, filtering);
    }

    private UsingTestCaseDao getUsingTestCaseDao() {
        if (Objects.isNull(usingTestCaseDao)) {
            throw new AccessDeniedException("A dedicated plugin is required to find test cases using action words");
        }
        return usingTestCaseDao;
    }
}
