/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.execution.SessionNote
import org.squashtest.tm.domain.execution.SessionNoteKind
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto
import org.squashtest.tm.plugin.rest.service.RestIssueService
import org.squashtest.tm.plugin.rest.service.RestSessionNoteService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder

@WebMvcTest(RestSessionNoteController)
class RestSessionNoteControllerIT extends BaseControllerSpec {

    @Inject
    private RestSessionNoteService service

    @Inject
    private RestIssueService restIssueService

    @Inject
    private RestSessionNoteController controller

    def "get-session-note"() {
        given:
        def sessionNote = SquashEntityBuilder.sessionNote {
            id = 1L
            kind = SessionNoteKind.SUGGESTION
            content = "Suggestion 1"
            noteOrder = 0
            execution = SquashEntityBuilder.exploratoryExecution {
                id = 1L
                testPlan = SquashEntityBuilder.iterationTestPlanItem {
                    id = 1L
                    iteration = SquashEntityBuilder.iteration {
                        campaign = SquashEntityBuilder.campaign {
                            project = SquashEntityBuilder.project {
                                id = 1L
                            }
                        }
                    }
                }
            }
        }

        and:
        service.getOne(1L) >> sessionNote

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/session-notes/{id}", 1L)
                .header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk()).andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "session-note"
            "id".is 1
            "kind".is "SUGGESTION"
            "content".is "Suggestion 1"
            "note_order".is 0
            "created_by".is "admin"
            "created_on".is "2017-07-24T10:00:00.000+00:00"
            "last_modified_by".is "User 1"
            "last_modified_on".is "2023-07-25T10:00:00.000+00:00"
            selfRelIs("http://localhost:8080/api/rest/latest/session-notes/1")
            "_links".test {
                "issues.href".is "http://localhost:8080/api/rest/latest/session-notes/1/issues"
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the session note"
                }
                fields {
                    add "_type (string) : the type of the entity"
                    add "id (number) : the id of the session note"
                    add "kind (string) : the kind of the session note"
                    add "content (string) : the content of the session note"
                    add "note_order (number) : the order of the session note"
                    add DescriptorLists.auditableFields
                    addAndStop"attachments (array) : the attachments of the session note. Please refer to the attachments documentation for more details."
                    add DescriptorLists.linksFields
                }
                _links {
                    add "self : link to this session note"
                    add "project : link to the project of this session note"
                    add "exploratory_execution : link to the exploratory execution of this session note"
                    add "attachments : link to the attachments of this session note"
                    add "issues : link to the issues of this session note"
                }
            }
        ))
    }

    def "get-session-note-issues"() {
        given:

        def sessionNote = SquashEntityBuilder.sessionNote {
            id = 1L
            kind = SessionNoteKind.SUGGESTION
            content = "Suggestion 1"
        }


        def execution = SquashEntityBuilder.exploratoryExecution {
            id = 1L
            reference = "REF 1"
            name = "execution 1"
            sessionNotes = [sessionNote] as List
        }

        def issue = new IssueDto(
            remoteIssueId: "165",
            url:new URL("http://192.175.1.51/bugzilla/show_bug.cgi?id=165"),
            executions:[],
            requirements: [],
            sessionNotes: sessionNote as Set<SessionNote>
        )

        def issues = new PageImpl<IssueDto>([issue], PageRequest.of(0, 20), 1)


        and:
        service.getOne(_) >> sessionNote
        service.getExecutionFromSessionNote(_) >> execution
        restIssueService.getIssuesFromExecutionIds(_, _) >> issues
        service.getSessionNotesFromIssue(_,_) >> execution.sessionNotes
        controller.finSessionNotesIssues(_,_) >> issues


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders
            .get("/api/rest/latest/session-notes/{id}/issues", 1)
            .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.issues".hasSize 1
            "_embedded.issues".test {
                "[0]".test {
                    "remoteIssueId".is "165"
                    "url".is "http://192.175.1.51/bugzilla/show_bug.cgi?id=165"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/session-notes/1/issues?page=0&size=20"
            "page".test {
                "size".is 20
                "totalElements".is 1
                "totalPages".is 1
                "number".is 0
            }

        }

        /*
     * Documentation
     */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the session note"
                }
                requestParams {
                    add DescriptorLists.paginationParams
                    add DescriptorLists.fieldsParams
                }
                fields {
                    embeddedAndStop "issues (array) : the issues of this session note"
                    add "_embedded.issues[].remoteIssueId (string) : the remote issue id of the issue linked to the session note."
                    add "_embedded.issues[].url (string) : the url of the issue linked to the session note."
                    add DescriptorLists.paginationFields
                    add DescriptorLists.linksFields
                }
                _links {
                    add DescriptorLists.paginationLinks
                }
            }
        ))
    }
}
