/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.squashtest.tm.core.foundation.collection.ColumnFiltering;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.plugin.rest.jackson.model.RestActionWordDto;

import java.util.List;

public interface RestActionWordService {

    ActionWord getOne(Long id);

    Page<ActionWord> findAllReadableActionWords(Pageable pageable);

    ActionWord patchActionWord(long id, RestActionWordDto patch);

    ActionWord createActionWord(RestActionWordDto post);

    /**
     * Try to delete all given action words. Action words used in one or more test steps won't be deleted, their ids are
     * returned in a List.
     * @param actionWordIds the ids of the action words to delete
     * @return a List of all action word ids which could not be deleted because used in a test step
     */
    List<Long> deleteActionWordsByIds(List<Long> actionWordIds);

    Page<ActionWord> findAllActionWordsByProject(long projectId, Pageable pageable);

    /**
     * Find all KeywordTestCase which use, in one or more of their steps, the ActionWord with the
     * given id.
     *
     * @param actionWordId the id of the ActionWord
     * @return The List of all TestCases using the ActionWord with the given id
     */
    List<TestCase> findAllByActionWord(long actionWordId);

    /**
     * Find a page of KeywordTestCase which use, in one or more of their steps, the ActionWord with
     * the given id. The returned Page follows the given paging and sorting parameters.
     *
     * @param actionWordId the id of the ActionWord
     * @param pageable the page object
     * @param filtering the filter object
     * @return The paged and sorted List of all TestCases using the ActionWord with the given id
     */
    Page<TestCase> findAllByActionWord(
            long actionWordId, Pageable pageable, ColumnFiltering filtering);
}
