/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
def step1 = actionStep {
    id = 167L
    action = "<p>Quick step forward</p>\n"
    expectedResult = "<p>So does your opponent</p>\n"
}

def step2 = actionStep {
    id = 168L
    action = "<p>Another quick step forward, albeit smaller</p>\n"
    expectedResult = "<p>Opponent&nbsp;doubles his steps too then lunges forward for an attack</p>\n"
}

def step3 = actionStep {
    id = 169L
    action = "<p>Strong Quarte parry, possibly with a slight retreat.</p>\n"
    expectedResult = "<p>Opponent&#39;s attack gets blocked by your blade.</p>\n"
}

def step4 = callStep {
    id = 170L
    delegateParameterValues = true
    calledTestCase = testCase {
        id = 240L
        name = "Compound riposte"
    }
}

def tc = testCase{
    id = 1L
    name = "who cares"
    steps = [step1, step2, step3, step4]
}

def cufsFalse = cufValue {
    inputType  "CHECKBOX"
    code = "CHK_BODY_FEINT"
    label = "requires body feint"
    value = "false"
}

def cufsTrue = cufValue {
    inputType  "CHECKBOX"
    code = "CHK_BODY_FEINT"
    label = "requires body feint"
    value = "true"
}

return [
    step1    : step1,
    step2    : step2,
    step3    : step3,
    step4    : step4,
    cufsFalse: cufsFalse,
    cufsTrue : cufsTrue
]
