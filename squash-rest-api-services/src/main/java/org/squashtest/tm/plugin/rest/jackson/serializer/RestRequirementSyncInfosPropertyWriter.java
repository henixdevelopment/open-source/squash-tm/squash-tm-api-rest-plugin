/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.Annotations;
import org.springframework.beans.factory.annotation.Configurable;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.plugin.rest.jackson.model.RemoteReqInfoRecord;
import org.squashtest.tm.plugin.rest.service.RestRequirementVersionService;

import javax.inject.Inject;

@Configurable
public class RestRequirementSyncInfosPropertyWriter extends VirtualBeanPropertyWriter {

    @Inject
    RestRequirementVersionService restRequirementVersionService;

    public RestRequirementSyncInfosPropertyWriter() {
        super();
    }

    public RestRequirementSyncInfosPropertyWriter(BeanPropertyDefinition propDef,
                                                  Annotations contextAnnotations,
                                                  JavaType declaredType,
                                                  RestRequirementVersionService restRequirementVersionService) {
        super(propDef, contextAnnotations, declaredType);
        this.restRequirementVersionService = restRequirementVersionService;
    }

@Override
protected Object value(Object bean, JsonGenerator gen, SerializerProvider prov) throws Exception {

    Requirement holder = (Requirement) bean;

    if (holder.isSynchronized()) {
        RemoteReqInfoRecord remoteReqInfo= restRequirementVersionService.getRemoteReqInfoByRequirementId(holder.getId());

        gen.writeFieldName("remote_req_id");
        gen.writeString(remoteReqInfo.remoteReqId());
        gen.writeFieldName("remote_req_url");
        gen.writeString(remoteReqInfo.remoteReqUrl());
        gen.writeFieldName("remote_req_perimeter_status");
        gen.writeString(remoteReqInfo.remoteReqPerimeterStatus());

    } else {
        gen.writeFieldName("remote_req_id");
        gen.writeString("null");
        gen.writeFieldName("remote_req_url");
        gen.writeString("null");
        gen.writeFieldName("remote_req_perimeter_status");
        gen.writeString("null");
    }
    return null;
}

    @Override
    public VirtualBeanPropertyWriter withConfig(MapperConfig<?> config, AnnotatedClass declaringClass, BeanPropertyDefinition propDef, JavaType type) {
        return new RestRequirementSyncInfosPropertyWriter(propDef,declaringClass.getAnnotations(), type, restRequirementVersionService);
    }

}
