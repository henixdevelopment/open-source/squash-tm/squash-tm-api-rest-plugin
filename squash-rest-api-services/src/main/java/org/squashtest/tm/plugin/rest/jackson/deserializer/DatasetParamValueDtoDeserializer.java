/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import org.springframework.beans.factory.annotation.Configurable;
import org.squashtest.tm.plugin.rest.jackson.model.DatasetParamValueDto;

import javax.inject.Inject;
import java.io.IOException;

/**
 * Created by jthebault on 09/06/2017.
 */
@Configurable
public class DatasetParamValueDtoDeserializer extends JsonDeserializer<DatasetParamValueDto> {

    @Inject
    private JsonCrawler jsonCrawler;

    @Override
    public DatasetParamValueDto deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return doDeserialize(p);
    }

    @Override
    public Object deserializeWithType(JsonParser p, DeserializationContext ctxt, TypeDeserializer typeDeserializer) throws IOException {
        return super.deserializeWithType(p, ctxt, typeDeserializer);
    }

    private DatasetParamValueDto doDeserialize(JsonParser p) throws IOException {
        JsonNode rootNode = p.getCodec().readTree(p);

        DatasetParamValueDto datasetParamValueDto = new DatasetParamValueDto();
        datasetParamValueDto.setParameterId(jsonCrawler.asMandatoryLong(rootNode, "parameter_id"));
        datasetParamValueDto.setParameterName(jsonCrawler.asMandatoryString(rootNode, "parameter_name"));
        datasetParamValueDto.setValue(jsonCrawler.asMandatoryString(rootNode, "parameter_value"));
        datasetParamValueDto.setTestCaseId(jsonCrawler.asMandatoryLong(rootNode, "parameter_test_case_id"));


        return datasetParamValueDto;
    }
}
