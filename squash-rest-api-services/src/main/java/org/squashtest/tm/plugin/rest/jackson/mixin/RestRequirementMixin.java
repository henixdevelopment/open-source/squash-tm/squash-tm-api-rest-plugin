/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.requirement.ManagementMode;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;
import org.squashtest.tm.plugin.rest.jackson.serializer.RestRequirementSyncInfosPropertyWriter;

import java.util.List;


/**
 * Mixin for {@link Requirement}s. Note that the class hierarchy here differs a bit : it directly extends from {@link RestIdentifiedMixin} (not from {@link RestGenericLibraryNodeMixin}. It
 * is so because the domain object is not a regular node either.
 *
 * @author bsiri
 *
 */
@JsonAutoDetect(fieldVisibility=Visibility.NONE, getterVisibility=Visibility.NONE, isGetterVisibility=Visibility.NONE)
@JsonTypeName("requirement")
@JsonAppend(props = {
    @JsonAppend.Prop(name = "remote_req_infos", value = RestRequirementSyncInfosPropertyWriter.class)
})
@JsonPropertyOrder({"_type","id", "name", "project", "path", "parent", "mode", "remote_req_infos", "current_version", "versions"})
public abstract class RestRequirementMixin extends RestRequirementLibraryNodeMixin{

	@JsonGetter
	abstract String getName();

	@JsonProperty("current_version")
	@JsonSerialize(converter=HateoasWrapperConverter.class)
	@JsonTypeInfo(use=Id.NONE)
	RequirementVersion resource;

	@JsonProperty
	@JsonSerialize(contentConverter=HateoasWrapperConverter.class)
	@JsonTypeInfo(use=Id.NONE)
	List<RequirementVersion> versions;

	@JsonProperty
	ManagementMode mode;

}
