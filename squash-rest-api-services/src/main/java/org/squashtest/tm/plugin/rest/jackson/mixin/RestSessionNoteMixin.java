/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.audit.AuditableSupport;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.plugin.rest.jackson.serializer.AttachmentHolderPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;


@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.NONE, getterVisibility = JsonAutoDetect.Visibility.NONE, isGetterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonPropertyOrder({"_type","id","execution","kind","content","note_order"})
@JsonAppend(props={
        @JsonAppend.Prop(name = "attachments", value = AttachmentHolderPropertyWriter.class)
}
)@JsonTypeName("session-note")
public abstract class RestSessionNoteMixin extends RestIdentifiedMixin {

    @JsonProperty
    private String kind;

    @JsonProperty
    private String content;

    @JsonProperty("execution")
    @JsonSerialize(converter=HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private Execution execution;

    @JsonProperty("note_order")
    private int noteOrder;

    @JsonUnwrapped
    AuditableSupport audit;

}

