/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.mockmvc

import org.springframework.beans.factory.ObjectFactory
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import org.springframework.core.convert.ConversionService
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.ui.ModelMap
import org.springframework.web.context.request.WebRequest
import org.springframework.web.context.request.WebRequestInterceptor
import org.squashtest.tm.plugin.rest.core.configuration.ApiWebConfig
import org.squashtest.tm.plugin.rest.core.jackson.EntityManagerWrapper
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.core.utils.DeserializationConfigHelper
import spock.mock.DetachedMockFactory

import javax.persistence.EntityManagerFactory

@Configuration
@ComponentScan(basePackages = [
	"org.squashtest.tm.plugin.rest.core.jackson",
	"org.squashtest.tm.plugin.rest.core.web",	
	"org.squashtest.tm.plugin.rest.controller",
	"org.squashtest.tm.plugin.rest.jackson"
])
@PropertySource("classpath:/config/rest-default.properties")
class MockMvcConfiguration extends ApiWebConfig{

	MockMvcConfiguration(ApplicationContext context, ObjectFactory<ConversionService> conversionService) {
		super(context, conversionService)
	}

	@Override
	protected WebRequestInterceptor squashRestEntityManagerInViewInterceptor(EntityManagerFactory emf){
		//returns a noop
		return new WebRequestInterceptor(){
			void preHandle(WebRequest request) throws Exception{
				
			}
			void postHandle(WebRequest request, ModelMap model) throws Exception{
				
			}
			void afterCompletion(WebRequest request, Exception ex) throws Exception{
				
			}
		}
	}

	@Override
	EntityManagerWrapper entityManagerWrapper() {
		return new DetachedMockFactory().Mock(EntityManagerWrapper)
	}

	@Override
	public NodeHierarchyHelpService nodeHierarchyHelpService(){
		return new DetachedMockFactory().Mock(NodeHierarchyHelpService);
	}

	@Override
	@Bean
	protected DeserializationConfigHelper deserializationConfigHelper(){
		return new DetachedMockFactory().Mock(DeserializationConfigHelper)
	}

	@Override
	@Bean
	// exposed here for tests purposes
	protected MappingJackson2HttpMessageConverter restHttpMessageConverter() {
		return super.restHttpMessageConverter()
	}
}
