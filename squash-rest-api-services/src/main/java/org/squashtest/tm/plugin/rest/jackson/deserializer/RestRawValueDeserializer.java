/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import org.squashtest.tm.domain.customfield.RawValue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jthebault on 12/06/2017.
 */
public class RestRawValueDeserializer extends JsonDeserializer<RawValue> {
    @Override
    public RawValue deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return doDeserialize(p);
    }

    @Override
    public RawValue deserializeWithType(JsonParser p, DeserializationContext ctxt, TypeDeserializer typeDeserializer) throws IOException {
        return doDeserialize(p);
    }

    private RawValue doDeserialize(JsonParser p) throws IOException {
        JsonNode root = p.getCodec().readTree(p);
        if(root == null){
            return new RawValue();
        }
        if(root.isArray()){
            List<String> values = new ArrayList<>();
            Iterator<JsonNode> elements = root.elements();
            while (elements.hasNext()){
                JsonNode value = elements.next();
                values.add(value.asText());
            }
            return new RawValue(values);
        } else {
            return new RawValue( root.asText());
        }
    }
}
