/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.jooq.DSLContext;
import org.jooq.Record3;
import org.jooq.Result;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.bugtracker.advanceddomain.RemoteIssueSearchRequest;
import org.squashtest.tm.domain.bugtracker.BugTracker;
import org.squashtest.tm.domain.bugtracker.Issue;
import org.squashtest.tm.domain.bugtracker.IssueList;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.exception.IssueAlreadyBoundException;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.plugin.rest.core.web.BasePathAwareLinkBuildingService;
import org.squashtest.tm.plugin.rest.jackson.model.IssueBugtrackerConnector;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.repository.RestIssueRepository;
import org.squashtest.tm.plugin.rest.service.RestIssueService;
import org.squashtest.tm.service.bugtracker.BugTrackerFinderService;
import org.squashtest.tm.service.bugtracker.BugTrackersLocalService;
import org.squashtest.tm.service.execution.ExecutionProcessingService;
import org.squashtest.tm.service.internal.bugtracker.BugTrackerConnectorFactory;
import org.squashtest.tm.service.internal.bugtracker.adapter.InternalBugtrackerConnector;
import org.squashtest.tm.service.internal.repository.ExecutionDao;

import javax.inject.Inject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.function.Function;

import static org.jooq.impl.DSL.groupConcatDistinct;

/**
 * Created by jthebault on 23/06/2017.
 */
@Service
@Transactional
public class RestIssueServiceImpl implements RestIssueService {

    // Quick and dirty way to prevent attaching issues to unsupported bugtrackers
    // TODO: replace with a way to properly support these bugtrackers (see https://gitlab.com/henixdevelopment/squash/squash-tm/tm-plugin/api/squash-tm-api-rest/-/issues/53)
    private static final List<String> UNSUPPORTED_BUGTRACKER_KINDS = Arrays.asList("gitlab.bugtracker", "azuredevops.bugtracker");

    @Inject
    private RestIssueRepository issueRepository;

    @Inject
    private BugTrackersLocalService bugTrackersLocalService;

    @Inject
    private ExecutionProcessingService executionProcessingService;

    @Inject
    private DSLContext dslContext;

    @Inject
    private BugTrackerConnectorFactory bugTrackerConnectorFactory;

    @Inject
    private BugTrackerFinderService bugTrackerFinderService;

    @Inject
    protected BasePathAwareLinkBuildingService linkService;

    @Inject
    private ExecutionDao executionDao;

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#executionId,'org.squashtest.tm.domain.execution.Execution' , 'WRITE')")
    public Issue attachIssue(IssueDto issueDto, Long executionId) {

        Execution exc = executionProcessingService.findExecution(executionId);

        IssueList issueList = exc.getIssueList();

        BugTracker bt = exc.getBugTracker();

        if (UNSUPPORTED_BUGTRACKER_KINDS.contains(bt.getKind())) {
            throw new UnsupportedOperationException("This bugtracker is not supported for this operation");
        }

        if (issueList.hasRemoteIssue(issueDto.getRemoteIssueId())) {
            throw new IssueAlreadyBoundException();
        } else {
            Issue issue = new Issue();
            issue.setBugtracker(bt);
            issue.setRemoteIssueId(issueDto.getRemoteIssueId());
            issueList.addIssue(issue);
            issueRepository.save(issue);
            return issue;
        }
    }

    @Override
    public Page<IssueDto> getIssuesFromExecutionIds(List<Long> executionIds, Pageable pageable) {
        Map<Long, IssueBugtrackerConnector> bugtrackerConnectors = new HashMap<>();
        List<IssueDto> issues = new ArrayList<>();

        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();

        Result<Record3<String, Long, String>> result =
            dslContext.select(Tables.ISSUE.REMOTE_ISSUE_ID, Tables.ISSUE.BUGTRACKER_ID,
                    groupConcatDistinct(Tables.EXECUTION_ISSUES_CLOSURE.EXECUTION_ID).as("EXECUTION_IDS") )
                .from(Tables.ISSUE)
                .join(Tables.EXECUTION_ISSUES_CLOSURE)
                .on(Tables.EXECUTION_ISSUES_CLOSURE.ISSUE_ID.eq(Tables.ISSUE.ISSUE_ID))
                .where(Tables.EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.in(executionIds))
                .groupBy(Tables.ISSUE.REMOTE_ISSUE_ID, Tables.ISSUE.BUGTRACKER_ID)
                .fetch();


        Map<Long, Execution> executionMap = getLongExecutionMap(executionIds);

        int startIndex = pageNumber * pageSize;
        int endIndex = (pageNumber + 1) * pageSize;
        int count = 0;

        for (Record3<String, Long, String> issueRecord : result) {
            if (count >= startIndex && count < endIndex) {
                addIssue(issueRecord, bugtrackerConnectors, issues, executionMap);
            }
            count++;
        }

        return new PageImpl<>(issues, pageable, result.size());
    }

    private Map<Long, Execution> getLongExecutionMap(List<Long> executionIds) {
        return executionDao.findAllById(executionIds)
            .stream()
            .collect(Collectors.toMap(Execution::getId, Function.identity()));

    }


    private void addIssue(Record3<String, Long, String> issueRecord, Map<Long, IssueBugtrackerConnector> bugtrackerConnectors, List<IssueDto> issues, Map<Long, Execution> executionMap) {
        final Long bugtrackerId = issueRecord.get(Tables.ISSUE.BUGTRACKER_ID);
        final String remoteIssueId = issueRecord.get(Tables.ISSUE.REMOTE_ISSUE_ID);
        BugTracker bugtracker;

        bugtracker = bugtrackerConnectors.computeIfAbsent(bugtrackerId, id -> {
            BugTracker existingBugtracker = bugTrackerFinderService.findById(id);
            InternalBugtrackerConnector connector = bugTrackerConnectorFactory.createConnector(existingBugtracker);
            return new IssueBugtrackerConnector(existingBugtracker, connector);
        }).bugTracker();

        final URL issueUrl = bugtrackerConnectors.get(bugtrackerId).connector().makeViewIssueUrl(bugtracker, remoteIssueId);

        Set<Execution> executions = new HashSet<>();
        String[] executionIdsArray = issueRecord.get("EXECUTION_IDS", String.class).split(",");

        Arrays.stream(executionIdsArray)
            .map(Long::parseLong)
            .map(executionMap::get)
            .filter(Objects::nonNull)
            .forEach(executions::add);

        boolean remoteIssueAlreadyExists = false;
        boolean urlAlreadyExists = false;

        for (IssueDto issue : issues) {
            if (issue.getRemoteIssueId().equals(remoteIssueId)) {
                remoteIssueAlreadyExists = true;
            }
            if (issue.getUrl().equals(issueUrl)) {
                urlAlreadyExists = true;
            }
        }

        if (!remoteIssueAlreadyExists || !urlAlreadyExists) {
            issues.add(new IssueDto(remoteIssueId, issueUrl, executions));
        }
    }

    @Override
    public Optional<Issue> searchAndAttachIssueToExecution(RemoteIssueSearchRequest searchRequest, Long executionId) {
        return bugTrackersLocalService.searchAndAttachIssueToExecution(searchRequest, executionId);
    }
}
