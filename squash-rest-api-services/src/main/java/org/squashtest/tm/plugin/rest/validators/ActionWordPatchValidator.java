/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.plugin.rest.jackson.model.RestActionWordDto;

@Component
public class ActionWordPatchValidator extends EntityValidator {

    private static final String CODE_NON_PATCHABLE_ATTRIBUTE = "non patchable attribute";

    private static final String MESSAGE_ATTRIBUTE_CANNOT_BE_MODIFIED = "The attribute cannot be modified.";
    private static final String MESSAGE_ATTRIBUTE_CANNOT_BE_PATCHED =
            "Only attributes belonging to the action word itself can be patched. " +
                    "The attribute %1$s cannot be patched. " +
                    "Use direct url to the %1$s instead.";

    private static final String TEST_CASES = "testCases";
    private static final String PARAMETERS = "parameters";
    private static final String PROJECT = "project";
    private static final String WORD = "word";

    @Override
    public boolean supports(Class<?> clazz) {
        return RestActionWordDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final RestActionWordDto patch = (RestActionWordDto) target;
        checkEntityExist(errors, ActionWord.class, patch.getId());
        checkForbiddenPatchAttributes(errors, patch);
    }

    private void checkForbiddenPatchAttributes(Errors errors, RestActionWordDto patch) {
        if(patch.getWord() != null) {
            errors.rejectValue(
                    WORD,
                    CODE_NON_PATCHABLE_ATTRIBUTE,
                    MESSAGE_ATTRIBUTE_CANNOT_BE_MODIFIED);
        }
        if(patch.getParameters() != null) {
            errors.rejectValue(
                    PARAMETERS,
                    CODE_NON_PATCHABLE_ATTRIBUTE,
                    String.format(MESSAGE_ATTRIBUTE_CANNOT_BE_PATCHED, PARAMETERS));
        }
        if(patch.getTestCases() != null) {
            errors.rejectValue(
                    TEST_CASES,
                    CODE_NON_PATCHABLE_ATTRIBUTE,
                    String.format(MESSAGE_ATTRIBUTE_CANNOT_BE_PATCHED, TEST_CASES));
        }
        if(patch.getProject() != null) {
            errors.rejectValue(
                    PROJECT,
                    CODE_NON_PATCHABLE_ATTRIBUTE,
                    String.format(MESSAGE_ATTRIBUTE_CANNOT_BE_PATCHED, PROJECT));
        }
    }
}
