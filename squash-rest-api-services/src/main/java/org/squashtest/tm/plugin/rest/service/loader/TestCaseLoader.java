/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.loader;


import org.squashtest.tm.domain.testcase.TestCase;


import javax.persistence.EntityManager;
import java.util.Map;


public class TestCaseLoader extends AbstractEntityLoader<TestCase> {

    private static final String ATTACHMENTS = "attachments";

    public TestCaseLoader(String query,
                          Map<String, Object> parameters,
                          String countQuery,
                          EntityManager entityManager) {
        super(query, parameters, countQuery, entityManager);
    }

    @Override
    protected Map<String, String> getAttributes() {
        return Map.of(
            "steps", "steps",
            "parameters", "parameters",
            "nature", "nature",
            "type", "type",
            "datasets", "datasets",
            "verified_requirements", "requirementVersionCoverages",
            "script_auto", "automatedTest",
            "automation_status", "automationRequest",
            "automation_priority", "automationRequest",
            ATTACHMENTS, "attachmentList"
        );
    }

    @Override
    protected Map<String, String> getSubAttributes() {
        return Map.of(
            "verified_requirements", "verifiedRequirementVersion",
            ATTACHMENTS, ATTACHMENTS);
    }

    @Override
    protected Class<TestCase> getEntityClass() {
        return TestCase.class;
    }

}
