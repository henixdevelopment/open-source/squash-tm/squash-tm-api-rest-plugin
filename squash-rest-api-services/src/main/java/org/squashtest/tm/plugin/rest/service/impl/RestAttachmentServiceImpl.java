/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.squashtest.tm.api.security.acls.Roles;
import org.squashtest.tm.domain.EntityType;
import org.squashtest.tm.domain.attachment.Attachment;
import org.squashtest.tm.domain.attachment.AttachmentHolder;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.domain.testcase.ActionTestStep;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.plugin.rest.core.exception.ProgrammingError;
import org.squashtest.tm.plugin.rest.jackson.model.RestUploadedData;
import org.squashtest.tm.plugin.rest.service.RestAttachmentService;
import org.squashtest.tm.plugin.rest.service.helper.AttachmentContentFilterHelper;
import org.squashtest.tm.service.attachment.AttachmentManagerService;
import org.squashtest.tm.service.attachment.RawAttachment;
import org.squashtest.tm.service.internal.display.dto.AttachmentDto;
import org.squashtest.tm.service.internal.repository.MilestoneDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.squashtest.tm.domain.EntityType.ACTION_TEST_STEP;
import static org.squashtest.tm.domain.EntityType.CAMPAIGN;
import static org.squashtest.tm.domain.EntityType.CAMPAIGN_FOLDER;
import static org.squashtest.tm.domain.EntityType.EXECUTION;
import static org.squashtest.tm.domain.EntityType.EXECUTION_STEP;
import static org.squashtest.tm.domain.EntityType.ITERATION;
import static org.squashtest.tm.domain.EntityType.PROJECT;
import static org.squashtest.tm.domain.EntityType.REQUIREMENT_FOLDER;
import static org.squashtest.tm.domain.EntityType.REQUIREMENT_VERSION;
import static org.squashtest.tm.domain.EntityType.TEST_CASE;
import static org.squashtest.tm.domain.EntityType.TEST_CASE_FOLDER;
import static org.squashtest.tm.domain.EntityType.TEST_SUITE;

@Service
@Transactional
public class RestAttachmentServiceImpl implements RestAttachmentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestAttachmentServiceImpl.class);

    @Inject
    private AttachmentManagerService attachmentService;

    @Inject
    private AttachmentContentFilterHelper filterHelper;

    @Inject
    private PermissionEvaluationService permissionService;

    @Inject
    private MilestoneDao milestoneDao;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Attachment findById(long id) {
        try {
            return attachmentService.findAttachment(id);
        } catch (AccessDeniedException | IllegalArgumentException e) {
            LOGGER.error("Error while retrieving attachment with id: " + id, e);
            throw new EntityNotFoundException("The attachment with id: " + id + " does not exist.");
        }
    }

    @Override
    public Page<Attachment> findPagedAttachments(AttachmentHolder attached, Pageable pageable) {
        return attachmentService.findPagedAttachments(attached, pageable);
    }

    @Override
    public Attachment addAttachment(long attachmentListId, RawAttachment upload, EntityType holderType) throws IOException {
        if (!filterHelper.isTypeAllowed(upload)) {
            throw new IllegalArgumentException(upload.getName() + " : file type not supported");
        }
        milestoneDao.checkBlockingMilestonesOnAttachmentList(holderType, attachmentListId);
        AttachmentDto dto = attachmentService.addAttachment(attachmentListId, upload, holderType);
        return findById(dto.getId());
    }

    @Override
    public Attachment renameAttachment(long attachmentId, String newName) {

        if (newName != null && !newName.isEmpty()) {
            attachmentService.renameAttachment(attachmentId, newName);
            return findById(attachmentId);
        } else {
            throw new IllegalArgumentException("name can not be empty or null");
        }

    }

    @Override
    public List<Attachment> addAttachments(AttachmentHolder holder, List<MultipartFile> files, EntityType holderType) {
        long listId = holder.getAttachmentList().getId();
        return files.stream().map(file -> {
            try {
                RestUploadedData data = new RestUploadedData(file.getInputStream(), file.getOriginalFilename(), file.getSize());
                return addAttachment(listId, data, holderType);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
    }

    @Override
    public void deleteAttachments(AttachmentHolder holder, List<Long> attachmentIds, EntityType holderType) throws IOException {
        attachmentService.removeListOfAttachments(holder.getAttachmentList().getId(), attachmentIds, holder.toEntityReference(), holderType);
    }

    @Override
    public AttachmentHolder findHolder(String owner, long id) {
        return findHolderWithPermission(owner, id, "READ").entrySet().iterator().next().getValue();
    }

    @Override
    public Map<EntityType, AttachmentHolder> findHolderWithPermission(String owner, long id, String permission) {
        switch (owner) {
            case "test-steps":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, TestStep.class.getName());
                ActionTestStep testStep = entityManager.find(ActionTestStep.class, id);
                return Collections.singletonMap(ACTION_TEST_STEP, testStep) ;
            case "campaigns":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, Campaign.class.getName());
                Campaign campaign = entityManager.find(Campaign.class, id);
                return Collections.singletonMap(CAMPAIGN, campaign) ;
            case "campaign-folders":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, CampaignFolder.class.getName());
                CampaignFolder folder = entityManager.find(CampaignFolder.class, id);
                return Collections.singletonMap(CAMPAIGN_FOLDER, folder);
            case "executions":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, Execution.class.getName());
                Execution execution = entityManager.find(Execution.class, id);
                return Collections.singletonMap(EXECUTION, execution);
            case "execution-steps":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, ExecutionStep.class.getName());
                ExecutionStep executionStep = entityManager.find(ExecutionStep.class, id);
                return Collections.singletonMap(EXECUTION_STEP, executionStep);
            case "projects":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, Project.class.getName());
                Project project = entityManager.find(Project.class, id);
                return Collections.singletonMap(PROJECT, project);
            case "iterations":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, Iteration.class.getName());
                Iteration iteration = entityManager.find(Iteration.class, id);
                return Collections.singletonMap(ITERATION, iteration);
            case "requirement-folders":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, RequirementFolder.class.getName());
                RequirementFolder requirementFolder = entityManager.find(RequirementFolder.class, id);
                return Collections.singletonMap(REQUIREMENT_FOLDER, requirementFolder);
            case "requirement-versions":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, RequirementVersion.class.getName());
                RequirementVersion requirementVersion = entityManager.find(RequirementVersion.class, id);
                return Collections.singletonMap(REQUIREMENT_VERSION, requirementVersion);
            case "test-cases":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, TestCase.class.getName());
                TestCase testCase = entityManager.find(TestCase.class, id);
                return Collections.singletonMap(TEST_CASE, testCase);
            case "test-case-folders":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, TestCaseFolder.class.getName());
                TestCaseFolder testCaseFolder = entityManager.find(TestCaseFolder.class, id);
                return Collections.singletonMap(TEST_CASE_FOLDER, testCaseFolder);
            case "test-suites":
                PermissionsUtils.checkPermission(permissionService, Collections.singletonList(id), permission, TestSuite.class.getName());
                TestSuite testSuite = entityManager.find(TestSuite.class, id);
                return Collections.singletonMap(TEST_SUITE, testSuite);
            default:
                throw new ProgrammingError("the url : " + owner + "/{id}/attachments does not exist, please double check it");
        }
    }
}
