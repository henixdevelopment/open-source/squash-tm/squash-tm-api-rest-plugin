/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import org.squashtest.tm.core.foundation.lang.Wrapped;
import org.squashtest.tm.domain.testcase.TestCaseKind;

public class GetKindTestCaseDtoVisitor implements TestCaseDtoVisitor {
    private final Wrapped<TestCaseKind> kind = new Wrapped<>();

    public void visit(TestCaseDto testCase) {
        this.kind.setValue(TestCaseKind.STANDARD);
    }

    public void visit(KeywordTestCaseDto keywordTestCase) {
        this.kind.setValue(TestCaseKind.KEYWORD);
    }

    public void visit(ScriptedTestCaseDto scriptedTestCase) {
        this.kind.setValue(TestCaseKind.GHERKIN);
    }

    public void visit(ExploratoryTestCaseDto exploratoryTestCase) {
        this.kind.setValue(TestCaseKind.EXPLORATORY);
    }

    public TestCaseKind getKind() {
        return this.kind.getValue();
    }
}
