/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.loader;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.squashtest.tm.service.internal.repository.hibernate.loaders.EntityGraphQueryBuilder;


import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractEntityLoader<T> {

    private final String query;

    private final Map<String, Object> parameters;

    private final String countQuery;

    private final EntityManager entityManager;

    protected AbstractEntityLoader(String query,
                                   Map<String, Object> parameters,
                                   String countQuery,
                                   EntityManager entityManager) {
        this.query = query;
        this.parameters = parameters;
        this.countQuery = countQuery;
        this.entityManager = entityManager;
    }

    public Page<T> loadEntityPage(List<String> fields, Pageable pageable) {
        String[] attributes = fields.stream()
            .filter(getAttributes()::containsKey)
            .map(getAttributes()::get)
            .collect(Collectors.toCollection(HashSet::new))
            .toArray(String[]::new);

        EntityGraphQueryBuilder<T> builder = new EntityGraphQueryBuilder<>(entityManager, getEntityClass(), query)
            .addAttributeNodes(attributes);

        for (Map.Entry<String, String> entry : getSubAttributes().entrySet()) {
            if (fields.contains(entry.getKey())) {
                builder.addSubGraph(getAttributes().get(entry.getKey()), entry.getValue());
            }
        }

        return builder.executePageable(parameters, pageable, countQuery);
    }

    protected abstract Map<String, String> getAttributes();

    protected abstract Map<String, String> getSubAttributes();

    protected abstract Class<T> getEntityClass();

}
