/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import org.squashtest.tm.domain.execution.ExecutionStatus

import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.campaign
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.dataset
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.iteration
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.iterationTestPlanItem
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scriptedTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testSuite

def testSuite = testSuite {

    id = 9l
    name = "sample test suite"
    description = "<p>this is a sample test suite</p>"

    createdBy "admin"
    createdOn "2017/07/12"
    lastModifiedBy "admin"
    lastModifiedOn "2017/07/12"

    iteration = iteration {
        id = 101l
        name = "second iteration"
        campaign = campaign {
            id = 87l
            name = "sample campaign"
            project = project {
                id = 15l
                name = "sample project"
            }
        }
    }

    testPlan = [
            iterationTestPlanItem {
                id = 80l
                executionStatus = "READY" as ExecutionStatus
                referencedTestCase = testCase {
                    id = 90L
                    name = "first test case"
                }
                referencedDataset = dataset {
                    id = 5l
                }
            },
            iterationTestPlanItem {
                id = 41l
                executionStatus = "SUCCESS" as ExecutionStatus
                referencedTestCase = scriptedTestCase {
                    id = 91L
                    name = "scripted test case 2"
                }
                referencedDataset = dataset {
                    id = 9L
                }
            },
            iterationTestPlanItem {
                id = 95l
                executionStatus = "SUCCESS" as ExecutionStatus
                referencedTestCase = keywordTestCase {
                    id = 105L
                    name = "keyword test case 3"
                }
                referencedDataset = dataset {
                    id = 18L
                }
            }
    ]
}

def cufs = [
        cufValue {
            label = "cuf text"
            inputType "PLAIN_TEXT"
            code = "CF_TXT"
            value = "the_value"
        },
        cufValue {
            label = "cuf tag"
            inputType "TAG"
            code = "CF_TAG"
            value = ["tag_1", "tag_2"]
        }
]
/*
 * Now pack the mocks and return
 *
 */
[
        testSuite: testSuite,
        cufs     : cufs
]
