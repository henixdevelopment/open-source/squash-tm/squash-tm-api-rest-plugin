/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.bugtracker.Issue

import static org.squashtest.tm.test.domainbuilder.PropertyHelper.assign

/**
 * Created by jlor on 13/07/2017.
 */
class IssueExpandos {

    private IssueExpandos(){
        super();
    }

    static class IssueExpando extends BaseExpando<Issue> {

        IssueExpando() {
            super()
            id = 1L


        }

        @Override
        def create (){

            Issue issue= super.create()
            if(this.remoteissueId){
                assign issue ,"remoteIssueId", remoteissueId
            }
            return issue
        }
    }


}

/*
@Override
def create(){
    Project project = super.create()

    if (project.testCaseLibrary){
        assign project.testCaseLibrary, "project", project
    }

    if (project.campaignLibrary){
        assign project.campaignLibrary, "project", project
    }

    return project
}

}
*/
