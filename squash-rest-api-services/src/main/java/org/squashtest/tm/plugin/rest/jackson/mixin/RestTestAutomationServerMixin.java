/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.squashtest.tm.domain.audit.AuditableSupport;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestAutomationServerEventBusUrlPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestAutomationServerKillSwitchUrlPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.TestAutomationServerObserverUrlPropertyWriter;

@JsonAutoDetect(fieldVisibility=Visibility.NONE, getterVisibility=Visibility.NONE, isGetterVisibility=Visibility.NONE)
@JsonPropertyOrder({"_type", "id", "name","url", "observer_url", "event_bus_url", "killswitch_url", "login", "kind", "manual_agent_selection", "description", "audit"})
@JsonTypeName("test-automation-server")
@JsonAppend(props= {
        @JsonAppend.Prop(name = "observer_url", value = TestAutomationServerObserverUrlPropertyWriter.class),
        @JsonAppend.Prop(name = "event_bus_url", value = TestAutomationServerEventBusUrlPropertyWriter.class),
        @JsonAppend.Prop(name = "killswitch_url", value = TestAutomationServerKillSwitchUrlPropertyWriter.class)
})
public abstract class RestTestAutomationServerMixin extends RestIdentifiedMixin {

    @JsonProperty
    String name;

    @JsonProperty("url")
    String url;

    @JsonProperty("observer_url")
    String observerUrl;

    @JsonProperty("event_bus_url")
    String eventBusUrl;

    @JsonProperty("killswitch_url")
    private String killswitchUrl;

    @JsonProperty
    String login;

    @JsonProperty
    String kind;

    @JsonProperty("manual_agent_selection")
    boolean manualSlaveSelection;

    @JsonProperty
    String description;

    @JsonUnwrapped
    AuditableSupport audit;

}
