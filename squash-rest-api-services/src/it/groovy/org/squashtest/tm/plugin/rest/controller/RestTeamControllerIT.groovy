/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.users.Team
import org.squashtest.tm.domain.users.User
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestPartyService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.hamcrest.Matchers.any
import static org.hamcrest.Matchers.hasSize
import static org.hamcrest.Matchers.is
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestTeamController)
public class RestTeamControllerIT extends BaseControllerSpec {

    @Inject
    private RestPartyService service

    def "browse-teams"() {
        given:
        service.findAllTeams(any(Pageable.class)) >> { args ->

            def team = SquashEntityBuilder.team {
                id = 567L
                name = "Team A"
                description = "<p>black panther</p>"

            }

            new PageImpl<Team>([team], args[0], 5)
        }


        when:
        def res = mockMvc.perform(get("/api/rest/latest/teams?page=2&size=1").header("Accept", "application/json"))


        then:

        /*
         * Test (style is plain Spring testmvc)
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        // embedded section
                .andExpect(jsonPath("_embedded.teams", hasSize(1)))
                .andExpect(jsonPath("_embedded.teams[0]._type", is("team")))
                .andExpect(jsonPath("_embedded.teams[0].id", is(567)))
                .andExpect(jsonPath("_embedded.teams[0].name", is("Team A")))
                .andExpect(jsonPath("_embedded.teams[0]._links.self.href", is("http://localhost:8080/api/rest/latest/teams/567")))

        // links section
                .andExpect(jsonPath("_links.first.href", is("http://localhost:8080/api/rest/latest/teams?page=0&size=1")))
                .andExpect(jsonPath("_links.prev.href", is("http://localhost:8080/api/rest/latest/teams?page=1&size=1")))
                .andExpect(jsonPath("_links.self.href", is("http://localhost:8080/api/rest/latest/teams?page=2&size=1")))
                .andExpect(jsonPath("_links.next.href", is("http://localhost:8080/api/rest/latest/teams?page=3&size=1")))
                .andExpect(jsonPath("_links.last.href", is("http://localhost:8080/api/rest/latest/teams?page=4&size=1")))

        // page section
                .andExpect(jsonPath("page.size", is(1)))
                .andExpect(jsonPath("page.totalElements", is(5)))
                .andExpect(jsonPath("page.totalPages", is(5)))
                .andExpect(jsonPath("page.number", is(2)))

        /*
         * Documentation
         */
        res.andDo(doc.document(
                DocumentationSnippets.AllInOne.createBrowseAllEntities("teams")
        ))

    }

    def "get-team"() {

        given:
        def team = SquashEntityBuilder.team {
            id = 567L
            name = "Team A"
            description = "<p>black panther</p>"
        }

        and:
        service.findTeamByPartyId(567) >> team

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/teams/{id}", 567).header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "team"
            "id".is 567
            "name".is "Team A"
            "description".is "<p>black panther</p>"
            selfRelIs "http://localhost:8080/api/rest/latest/teams/567"
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the team"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        add "_type (string) : the type of the entity"
                        add "id (number) : the id of the team"
                        add "name (string) : the name of the team"
                        add "description (string) : the description of the team"
                        add "members (array) : the members of this team"
                        add "created_by (string) : the user who created this team"
                        add "created_on (string) : the date of this team account was created"
                        add "last_modified_by (string) : the user who last modified this team"
                        add "last_modified_on (string) : the date of this team was last modified"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this user"
                    }
                }
        ))
    }

    def "post-team"() {
        given:
        def json = """{
            "_type": "team",
            "name": "Team A",
            "description": "<p>black panther</p>"
        }"""

        def team = SquashEntityBuilder.team {
            id = 332L
            name = "Team A"
            description = "<p>black panther</p>"
        }

        and:
        service.createParty(_) >> team

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/teams")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "team"
            "id".is 332
            "name".is "Team A"
            "description".is "<p>black panther</p>"
            selfRelIs "http://localhost:8080/api/rest/latest/teams/332"

        }
        res.andDo(doc.document(
                documentationBuilder {
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the team"
                        add "description (string) : the description of the team"
                    }
                    fields {
                        relaxed = true
                        add "id (number) : the id of the team"
                        add "members (array) : the members of this team"
                        add "created_by (string) : the user who created this team"
                        add "created_on (string) : the date of this team account was created"
                        add "last_modified_by (string) : the user who last modified this team"
                        add "last_modified_on (string) : the date of this team was last modified"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this user"
                    }
                }
        ))
    }

    def "patch-team"() {
        given:
        def json = """{
            "_type": "team",
            "name": "Team A",
            "description": "<p>black panther</p>"
        }"""

        def team = SquashEntityBuilder.team {
            id = 332L
            name = "Team A"
            description = "<p>black panther</p>"
        }

        and:
        service.patchParty(_, _) >> team

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/teams/{id}", 567)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "team"
            "id".is 332
            "name".is "Team A"
            "description".is "<p>black panther</p>"
            selfRelIs "http://localhost:8080/api/rest/latest/teams/332"

        }
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the team"
                    }
                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the team"
                        add "description (string) : the description of the team"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        relaxed = true
                        add "id (number) : the id of the team"
                        add "members (array) : the members of this team"
                        add "created_by (string) : the user who created this team"
                        add "created_on (string) : the date of this team account was created"
                        add "last_modified_by (string) : the user who last modified this team"
                        add "last_modified_on (string) : the date of this team was last modified"
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this user"
                    }
                }
        ))
    }

    def "delete-team"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/teams/{ids}", "169,189")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * service.deleteTeams(_)

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "ids : the list of ids of the teams"
                    }
                }
        ))
    }

    def "get-team-members"() {
        given:
        def mocks = loadMock("mocks/get-team-members.groovy")

        and:
        service.findAllTeamMembers(_, _) >> { args ->
            new PageImpl<User>([mocks.member1,
                                mocks.member2,
                                mocks.member3], args[1], 3);
        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/teams/{id}/members", 888).header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.members".hasSize 3
            "_embedded.members".test {
                "[0]".test {
                    "_type".is "user"
                    "id".is 1
                    "first_name".is "Charles"
                    "last_name".is "Dupond"
                    "login".is "User-1"
                    "email".is "charlesdupond@aaaa.aa"
                    "active".is true
                    "group".is "User"

                    selfRelIs "http://localhost:8080/api/rest/latest/users/1"
                }
            }


            selfRelIs "http://localhost:8080/api/rest/latest/teams/888/members?page=0&size=20"

            "page".test {
                "size".is 20
                "totalElements".is 3
                "totalPages".is 1
                "number".is 0
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the team"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.paginationParams
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "members (array) : the members of this team"
                        add DocumentationSnippets.DescriptorLists.paginationFields
                        add DocumentationSnippets.DescriptorLists.linksFields
                    }
                    _links {
                        add DocumentationSnippets.DescriptorLists.paginationLinks
                    }
                }
        ))

    }

    def "add-new-members-to-team"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/teams/{teamId}/members?userIds=486,521", "888")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * service.addMembersToTeam(_, _)

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "teamId : the id of the team"
                    }
                    requestParams {
                        add "userIds : the ids of the members to add"
                    }
                }
        ))
    }

    def "remove-team-members"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/teams/{teamId}/members?userIds=486,521", "888")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * service.removeMembersFromTeam(_, _)

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "teamId : the id of the team"
                    }
                    requestParams {
                        add "userIds : the ids of the members to remove"
                    }
                }
        ))
    }
}
