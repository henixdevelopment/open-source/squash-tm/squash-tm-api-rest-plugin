/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper;

import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.project.Project;

// Even test step is not actually a rest node, we still want to benefit the cuf services in RestNodeValidationHelper
@Component
public class TestStepValidationHelper extends RestNodeValidationHelper{

    public void checkProject(Long testStepDtoProjectId) {
        if (testStepDtoProjectId == null) {
            throw new IllegalArgumentException("Programmatic error, you must provide a not null project id to add an action step");
        } else {
            Project project = entityManager.find(Project.class, testStepDtoProjectId);
            if (project == null) {
                throw new IllegalArgumentException("Programmatic error, you must provide a valid project id to add an action step");
            }
        }
    }
}
