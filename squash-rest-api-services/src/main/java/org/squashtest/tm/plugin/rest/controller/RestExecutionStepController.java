/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.plugin.rest.core.jackson.DeserializationFilterExpression;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.PersistentEntity;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.ExecutionStepAndCustomFields;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDtoForExecution;
import org.squashtest.tm.plugin.rest.service.RestExecutionStepService;
import org.squashtest.tm.plugin.rest.service.RestIssueService;
import org.squashtest.tm.plugin.rest.validators.CustomFieldValueHintedValidator;
import org.squashtest.tm.plugin.rest.validators.DenormalizedFieldValueHintedValidator;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(ExecutionStep.class)
@UseDefaultRestApiConfiguration
public class RestExecutionStepController extends BaseRestController{
    public static final String PATCH_DYNAMIC_FILTER = "*, execution[execution_status]";

    @Inject
    private RestExecutionStepService restExecutionStepService;

    @Inject
    private CustomFieldValueHintedValidator cufValidator;

    @Inject
    private DenormalizedFieldValueHintedValidator denoValidator;

    @Inject
    private RestIssueService restIssueService;

    @InitBinder
    public void initBinder(WebDataBinder binder){
        binder.addValidators(cufValidator);
        binder.addValidators(denoValidator);
    }

    @GetMapping(value = "/execution-steps/{id}")
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression("*, execution[execution_status]")
    public ResponseEntity<EntityModel<ExecutionStep>> findExecutionStep(@PathVariable("id") long id){

        ExecutionStep executionStep = restExecutionStepService.getOne(id);

        EntityModel<ExecutionStep> res = toEntityModel(executionStep);

        res.add(linkService.createLinkTo(executionStep.getProject()));
        res.add(linkService.createLinkTo(executionStep.getExecution()));
        res.add(linkService.createRelationTo(executionStep, "attachments"));
        res.add(linkService.createRelationTo(executionStep, "issues"));


        return ResponseEntity.ok(res);
    }

    @PatchMapping(value = "/execution-steps/{id}/execution-status/{status}")
    @ResponseBody
    @DynamicFilterExpression("*, execution[execution_status]")
    @PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.execution.ExecutionStep' , 'WRITE')")
    public ResponseEntity<EntityModel<ExecutionStep>> modifyExecutionStatus(@PathVariable("id") long id, @PathVariable("status") String status){

        try {
            ExecutionStatus validatedStatus = ExecutionStatus.valueOf(status.toUpperCase());
            restExecutionStepService.modifyExecutionStatus(id, validatedStatus);
        } catch (IllegalArgumentException e){
            throw new EnumConstantNotPresentException(ExecutionStatus.class, "Execution status "+status+" does not exist");
        }

        ExecutionStep executionStep = restExecutionStepService.getOne(id);

        EntityModel<ExecutionStep> res = toEntityModel(executionStep);

        res.add(linkService.createLinkTo(executionStep.getProject()));
        res.add(linkService.createLinkTo(executionStep.getExecution()));

        return ResponseEntity.ok(res);
    }

    @PatchMapping(value = "/execution-steps/{id}")
    @ResponseBody
    @DynamicFilterExpression("*, execution[execution_status]")
    @DeserializationFilterExpression("comment, custom_fields")
    public ResponseEntity<EntityModel<ExecutionStep>> patchExecutionStep(@Validated @PersistentEntity ExecutionStepAndCustomFields step) {
        restExecutionStepService.updateStep(step);

        ExecutionStep unwrapped = step.getWrapped();
        EntityModel<ExecutionStep> res = toEntityModel(unwrapped);

        res.add(linkService.createLinkTo(unwrapped.getProject()));
        res.add(linkService.createLinkTo(unwrapped.getExecution()));

        return ResponseEntity.ok(res);
    }

    @GetMapping("/execution-steps/{id}/issues")
    @ResponseBody
    @DynamicFilterExpression("*, execution")
    public ResponseEntity<PagedModel<EntityModel<IssueDtoForExecution>>> findExecutionStepsIssues(@PathVariable("id") long id, Pageable pageable) {

        ExecutionStep executionStep = restExecutionStepService.getOne(id);
        if (executionStep == null) {
            throw new IllegalArgumentException("Execution step with id " + id + " does not exist");
        }

        List<Execution> executions = restExecutionStepService.getExecutionFromExecutionStep(id);

            Optional<Long> firstExecutionId = executions.stream().findFirst().map(Execution::getId);
            Long idOfFirstExecution = firstExecutionId.get();

            Page<IssueDto> pagedIssue = restIssueService.getIssuesFromExecutionIds(Collections.singletonList(idOfFirstExecution), pageable);

            Page<IssueDtoForExecution> pagedIssueForExecution = pagedIssue.map(this::convertToIssueDtoForExecution);

            List<IssueDtoForExecution> filteredIssues = new ArrayList<>();

            for (IssueDtoForExecution issue : pagedIssueForExecution.getContent()) {
                List<ExecutionStep> executionSteps = restExecutionStepService.getExecutionStepFromIssue(issue.getRemoteIssueId(), idOfFirstExecution);
                issue.setExecutionSteps(new HashSet<>(executionSteps));

                if(executionSteps.stream().anyMatch(step -> step.getId() == id)) {
                    filteredIssues.add(issue);
                    issue.setExecutionSteps(new HashSet<>());
                }
            }
            Page<IssueDtoForExecution> pagedIssueToReturn = new PageImpl<> (filteredIssues, pageable, filteredIssues.size());

            PagedModel<EntityModel<IssueDtoForExecution>> res = pageAssembler.toModel(pagedIssueToReturn);

            return ResponseEntity.ok(res);
    }

    private IssueDtoForExecution convertToIssueDtoForExecution(IssueDto issueDto) {
        return new IssueDtoForExecution(issueDto.getRemoteIssueId(), issueDto.getUrl());
    }

}
