/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.execution.ExecutionStatus;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.domain.infolist.DenormalizedNature;
import org.squashtest.tm.domain.infolist.DenormalizedType;
import org.squashtest.tm.domain.testautomation.AutomatedExecutionExtender;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.domain.testcase.TestCaseImportance;
import org.squashtest.tm.domain.testcase.TestCaseStatus;
import org.squashtest.tm.plugin.rest.jackson.model.DenormalizedEnvironmentVariableDto;
import org.squashtest.tm.plugin.rest.jackson.model.RestCustomFieldMembers;
import org.squashtest.tm.plugin.rest.jackson.serializer.AttachmentHolderPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.CustomFieldValuesPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.DenormalizedCustomFieldValuesPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.EnvironmentVariablePropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;

import java.util.Date;
import java.util.List;

import static org.squashtest.tm.domain.testcase.TestCaseImportance.LOW;

/**
 * Created by jthebault on 20/06/2017.
 */
@JsonAutoDetect(fieldVisibility= JsonAutoDetect.Visibility.NONE, getterVisibility= JsonAutoDetect.Visibility.NONE, isGetterVisibility= JsonAutoDetect.Visibility.NONE)
@JsonPropertyOrder({"_type", "id","name","execution_order","execution_status","last_executed_by","last_executed_on","execution_mode",
        "reference","dataset_label","execution_steps","comment","prerequisite","description",
        "importance","nature","type","test_case_status","test_plan_item", "denormalized_environment_variables"})
@JsonAppend(props = {
        @JsonAppend.Prop(name = "custom_fields", type = RestCustomFieldMembers.class, value = CustomFieldValuesPropertyWriter.class),
        @JsonAppend.Prop(name = "environment_variables", type = DenormalizedEnvironmentVariableDto.class, value = EnvironmentVariablePropertyWriter.class, include = JsonInclude.Include.NON_EMPTY),
        @JsonAppend.Prop(name = "test_case_custom_fields", type = RestCustomFieldMembers.class, value = DenormalizedCustomFieldValuesPropertyWriter.class),
        @JsonAppend.Prop(name = "attachments", value = AttachmentHolderPropertyWriter.class)
})
@JsonTypeName("execution")
public abstract class RestExecutionMixin extends RestIdentifiedMixin{

    @JsonProperty
    private String name;

    @JsonProperty(value="execution_order", access=Access.READ_ONLY)
    private int executionOrder;

    @JsonProperty("dataset_label")
    private String datasetLabel;

    @JsonProperty("execution_status")
    private ExecutionStatus executionStatus;

    @JsonProperty("last_executed_by")
    private String lastExecutedBy;

    @JsonProperty("last_executed_on")
    private Date lastExecutedOn;

    @JsonProperty("execution_mode")
    protected TestCaseExecutionMode executionMode;

    @JsonProperty("comment")
    private String description;

    @JsonProperty
    private String prerequisite = "";

    @JsonProperty
    private String reference = "";

    @JsonProperty("description")
    private String tcdescription;

    @JsonProperty("importance")
    private TestCaseImportance importance = LOW;

    @JsonProperty("nature")
    private DenormalizedNature nature;

    @JsonProperty("type")
    private DenormalizedType type;

    @JsonProperty("test_case_status")
    private TestCaseStatus status = TestCaseStatus.WORK_IN_PROGRESS;

    @JsonProperty("execution_steps")
    @JsonSerialize(contentConverter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private List<ExecutionStep> steps;

    @JsonProperty("test_plan_item")
    @JsonSerialize(converter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private IterationTestPlanItem testPlan;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("automated_execution_extender")
    @JsonSerialize(converter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private AutomatedExecutionExtender automatedExecutionExtender;

}

