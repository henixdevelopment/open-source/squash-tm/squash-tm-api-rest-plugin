/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.domain.campaign.Campaign;

import java.util.Collection;

/**
 * Created by jthebault on 19/06/2017.
 */
public interface RestCampaignRepository  extends JpaRepository<Campaign, Long>{

    @Query("from Campaign camp where camp.project.id in (:projectIds)")
    Page<Campaign> findAllInProjects(@Param("projectIds") Collection<Long> projectIds, Pageable pageable);

    @Query("from Campaign camp where camp.name = :campaignName")
    Page<Campaign> findAllByName(@Param("campaignName") String campaignName, Pageable pageable);
}
