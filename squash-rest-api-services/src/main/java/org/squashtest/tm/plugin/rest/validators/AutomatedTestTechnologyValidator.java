/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.testautomation.AutomatedTestTechnology;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.jackson.model.AutomatedTestTechnologyDto;
import org.squashtest.tm.plugin.rest.repository.RestAutomatedTestTechnologyRepository;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class AutomatedTestTechnologyValidator implements Validator {

    private static final String POST_AUTOMATED_TEST_TECHNOLOGY = "post-automated-test-technology";

    @Inject
    private RestAutomatedTestTechnologyRepository restAutomatedTestTechnologyRepository;

    @Override
    public boolean supports(Class<?> clazz) {
        return AutomatedTestTechnology.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        AutomatedTestTechnologyDto technologyDto = (AutomatedTestTechnologyDto) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required", "This attribute can't be empty.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "actionProviderKey", "required", "This attribute can't be empty.");
        String technologyDtoName = technologyDto.getName();
        if (technologyDtoName != null
                && Objects.nonNull(restAutomatedTestTechnologyRepository.findByName(technologyDtoName))) {
            errors.rejectValue("name", "invalid value", "A technology with the same name already exists");
        }
    }

    public void validatePostAutomatedTestTechnology(AutomatedTestTechnologyDto automatedTestTechnologyDto) throws BindException {
        List<Errors> errors = new ArrayList<>();
        BindingResult validation = new BeanPropertyBindingResult(automatedTestTechnologyDto, POST_AUTOMATED_TEST_TECHNOLOGY);
        validate(automatedTestTechnologyDto, validation);
        if (validation.hasErrors()) {
            errors.add(validation);
        }
        ErrorHandlerHelper.throwIfError(automatedTestTechnologyDto, errors, POST_AUTOMATED_TEST_TECHNOLOGY);
    }
}
