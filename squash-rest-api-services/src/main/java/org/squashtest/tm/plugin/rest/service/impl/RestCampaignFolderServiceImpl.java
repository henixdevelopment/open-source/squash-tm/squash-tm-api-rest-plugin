/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.jooq.DSLContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.plugin.rest.jackson.model.CampaignFolderDto;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.ParentEntity;
import org.squashtest.tm.plugin.rest.repository.RestCampaignFolderRepository;
import org.squashtest.tm.plugin.rest.service.RestCampaignFolderService;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.campaign.CampaignLibraryNavigationService;
import org.squashtest.tm.service.project.ProjectFinder;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.List;

import static org.squashtest.tm.plugin.rest.utils.PaginationUtils.emptyPage;

/**
 * Created by jthebault on 19/06/2017.
 */
@Service
@Transactional
public class RestCampaignFolderServiceImpl implements RestCampaignFolderService {

    @Inject
    private RestCampaignFolderRepository dao;

    @Inject
	private ProjectFinder projectFinder;

    @Inject
    private CustomFieldValueHelper customFieldValueHelper;

    @Inject
    private CampaignLibraryNavigationService campaignLibraryNavigationService;

    @Inject
    private DSLContext dslContext;


    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly=true)
    @PostAuthorize("hasPermission(returnObject , 'READ') or hasRole('ROLE_ADMIN')")
    public CampaignFolder getOne(Long campaignFolderId) {
        return dao.getReferenceById(campaignFolderId);
    }

	@Override
    @Transactional(readOnly=true)
	public Page<CampaignFolder> findAllReadable(Pageable pageable) {
        Collection<Long> projectIds = projectFinder.findReadableProjectIdsOnCampaignLibrary();
		return projectIds.isEmpty() ? emptyPage(pageable) : dao.findAllInProjects(projectIds, pageable);
	}

    @Override
    public List<CampaignFolder> findCampaignFolderByProject(List<Long> projectIds) {
        return dao.findAllCampaignFoldersByProjects(projectIds);
    }

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#folderId,'org.squashtest.tm.domain.campaign.CampaignFolder' , 'READ')")
    public Page<CampaignLibraryNode> findFolderContent(long folderId, Pageable pageable) {
        return dao.findFolderContent(folderId, pageable);
    }

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#folderId,'org.squashtest.tm.domain.campaign.CampaignFolder' , 'READ')")
    public Page<CampaignLibraryNode> findFolderAllContent(long folderId, Pageable pageable) {
    	return dao.findFolderAllContent(folderId, pageable);
    }

    @Override
    public CampaignFolder addCampaignFolder(CampaignFolderDto folderDto) throws InvocationTargetException, IllegalAccessException{
        final CampaignFolder folder = new CampaignFolder();

        folder.setName(folderDto.getName());
        folder.setDescription(folderDto.getDescription());

        addToParent(folderDto, folder);
        if(folderDto.getCustomFields()!=null) {
            List<CustomFieldValueDto> listCufsDto = folderDto.getCustomFields();
            customFieldValueHelper.patchCustomFieldValue(folder,listCufsDto);
        }
        entityManager.persist(folder);
        entityManager.flush();
        return folder;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.campaign.CampaignFolder' , 'WRITE')")
    public CampaignFolder patchCampaignFolder(CampaignFolderDto folderPatch, long id) {
        CampaignFolder folder = this.getOne(id);

        folder.setName(folderPatch.getName());
        folder.setDescription(folderPatch.getDescription());

        if(folderPatch.getCustomFields()!=null) {
            List<CustomFieldValueDto> listCufsDto = folderPatch.getCustomFields();
            customFieldValueHelper.patchCustomFieldValue(folder,listCufsDto);
        }

        return folder;
    }

    @Override
    //@PreAuthorize est fait du coté du squash TM
    public void deleteFolder(List<Long> folderIds) {
        campaignLibraryNavigationService.deleteNodes(folderIds);
    }

    private void addToParent(CampaignFolderDto folderDto, CampaignFolder folder) {

        ParentEntity parent = folderDto.getParent();
        switch (parent.getRestType()) {
            case PROJECT:
                addCampaignFolderToLibrary(folder, folderDto, parent);
                break;
            case CAMPAIGN_FOLDER:
                addCampaignFolderToFolder(folder, folderDto, parent);
                break;
            default:
                throw new IllegalArgumentException("Programmatic error : Rest type " + parent.getRestType() + "is not a valid parent. You should validate this before.");
        }

    }

    private void addCampaignFolderToFolder(CampaignFolder folder, CampaignFolderDto folderDto, ParentEntity parent) {
        CampaignFolder campaignFolder = entityManager.find(CampaignFolder.class, parent.getId());
        if (campaignFolder != null) {
            campaignLibraryNavigationService.addFolderToFolder(campaignFolder.getId(), folder);
        } else {
            throw new IllegalArgumentException("Programmatic error : test case folder with id " + parent.getId() + "is unknown. You should validate this before.");

        }
    }

    private void addCampaignFolderToLibrary(CampaignFolder folder, CampaignFolderDto folderDto, ParentEntity parent) {
        Project project = entityManager.find(Project.class, parent.getId());
        if (project != null) {
            campaignLibraryNavigationService.addFolderToLibrary(project.getCampaignLibrary().getId(), folder);
        } else {
            throw new IllegalArgumentException("Programmatic error : project with id " + parent.getId() + "is unknown. You should validate this before.");
        }
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#folderId,'org.squashtest.tm.domain.campaign.CampaignFolder' , 'READ')")
    public List<Long> getExecutionIdsByCampaignFolder(Long folderId) {

        return dslContext.select(Tables.ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID)
            .from(Tables.ITEM_TEST_PLAN_EXECUTION)
            .join(Tables.ITEM_TEST_PLAN_LIST).on(Tables.ITEM_TEST_PLAN_LIST.ITEM_TEST_PLAN_ID.eq(Tables.ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID))
            .join(Tables.CAMPAIGN_ITERATION).on(Tables.CAMPAIGN_ITERATION.ITERATION_ID.eq(Tables.ITEM_TEST_PLAN_LIST.ITERATION_ID))
            .join(Tables.CLN_RELATIONSHIP_CLOSURE).on(Tables.CLN_RELATIONSHIP_CLOSURE.DESCENDANT_ID.eq(Tables.CAMPAIGN_ITERATION.CAMPAIGN_ID))
            .where(Tables.CLN_RELATIONSHIP_CLOSURE.ANCESTOR_ID.eq(folderId))
            .fetchInto(Long.class);
    }

}
