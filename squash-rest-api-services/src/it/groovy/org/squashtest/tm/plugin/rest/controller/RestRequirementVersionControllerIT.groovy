/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.squashtest.tm.domain.requirement.RequirementVersion
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.core.service.NodeHierarchyHelpService
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto
import org.squashtest.tm.plugin.rest.service.RestIssueService
import org.squashtest.tm.plugin.rest.service.RestRequirementVersionService
import org.squashtest.tm.plugin.rest.service.RestVerifyingTestCaseManagerService
import org.squashtest.tm.plugin.rest.service.impl.RequirementVersionPatcher
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.requirement.RequirementVersionManagerService
import org.squashtest.tm.service.security.PermissionEvaluationService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder
import javax.inject.Inject
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.delete
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufValue
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirement
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirementVersion

@WebMvcTest(RestRequirementVersionController)
public class RestRequirementVersionControllerIT extends BaseControllerSpec {

    @Inject
    private RestRequirementVersionService service;

    @Inject
    private CustomFieldValueFinderService cufService

    @Inject
    private PermissionEvaluationService permService

    @Inject
    private RestVerifyingTestCaseManagerService verifyingTestCaseService

    @Inject
    private RequirementVersionPatcher requirementVersionPatcher

    @Inject
    private NodeHierarchyHelpService nodeHierarchyHelpService

    @Inject
    private RequirementVersionManagerService requirementVersionManagerService

    @Inject
    private RestIssueService restIssueService

    @Inject
    private RestRequirementVersionController controller



    def "get-requirement-version"() {

        given:

        def reqVersion = SquashEntityBuilder.requirementVersion {
            id = 3l
            name = "sample requirement"
            reference = "SAMP_REQ_VER"
            versionNumber = 2

            createdBy "User-1"
            createdOn "2017/07/19"
            lastModifiedBy "User-2"
            lastModifiedOn "2017/07/20"

            criticality "CRITICAL"
            category "CAT_PERFORMANCE"
            status "APPROVED"
            description = "<p>Approved performance requirement-version</p>"

            testCases = [
                    SquashEntityBuilder.testCase {
                        id = 4l
                        name = "verifying test case 1"
                    },
                    SquashEntityBuilder.scriptedTestCase {
                        id = 9l
                        name = "verifying scripted test case 2"
                    },
                    SquashEntityBuilder.keywordTestCase {
                        id = 14l
                        name = "verifying keyword test case 3"
                    }
            ]
        }

        def requirement = SquashEntityBuilder.requirement {
            id = 64l
            project = SquashEntityBuilder.project {
                id = 85l
                name = "myProject"
            }
            versions = [reqVersion]
        }

        def cufs = [
                SquashEntityBuilder.cufValue {
                    label = "Cuf One"
                    inputType "PLAIN_TEXT"
                    code = "CUF1"
                    value = "value_1"
                },
                SquashEntityBuilder.cufValue {
                    label = "Cuf Two"
                    inputType "PLAIN_TEXT"
                    code = "CUF2"
                    value = "value_2"
                }
        ]

        and:

        service.findRequirementVersion(3) >> reqVersion
        cufService.findAllCustomFieldValues(reqVersion) >> cufs
        permService.canRead(_) >> true

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/requirement-versions/{id}", 3)
                .header("Accept", "application/json"))

        then:

        /*
       * Test (with TestHelper)
       */
        res.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))

        withResult(res) {
            "_type".is "requirement-version"
            "id".is 3
            "name".is "sample requirement"
            "reference".is "SAMP_REQ_VER"
            "version_number".is 2

            "requirement".test {
                "_type".is "requirement"
                "id".is 64
                "name".is "sample requirement"
                selfRelIs "http://localhost:8080/api/rest/latest/requirements/64"
            }

            "created_by".is "User-1"
            "created_on".is "2017-07-19T10:00:00.000+00:00"
            "last_modified_by".is "User-2"
            "last_modified_on".is "2017-07-20T10:00:00.000+00:00"
            "criticality".is "CRITICAL"
            "category".test { "code".is "CAT_PERFORMANCE" }
            "status".is "APPROVED"
            "description".is "<p>Approved performance requirement-version</p>"

            "verifying_test_cases".hasSize 3
            "verifying_test_cases".test {
                "[0]".test {
                    "_type".is "test-case"
                    "id".is 4
                    "name".is "verifying test case 1"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/4"
                }
                "[1]".test {
                    "_type".is "scripted-test-case"
                    "id".is 9
                    "name".is "verifying scripted test case 2"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/9"
                }
                "[2]".test {
                    "_type".is "keyword-test-case"
                    "id".is 14
                    "name".is "verifying keyword test case 3"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/14"
                }
            }

            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CUF1"
                    "label".is "Cuf One"
                    "value".is "value_1"
                }
                "[1]".test {
                    "code".is "CUF2"
                    "label".is "Cuf Two"
                    "value".is "value_2"
                }
            }

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/requirement-versions/3"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/85"
                "requirement.href".is "http://localhost:8080/api/rest/latest/requirements/64"
                "issues.href".is "http://localhost:8080/api/rest/latest/requirement-versions/3/issues"
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the requirement version"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (number) : the id of the requirement version"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the requirement version"
                        add "reference (string) : the reference of the requirement version"
                        add "version_number (number) : the version number"
                        addAndStop "requirement (object) : the requirement of this requirement version"
                        add DescriptorLists.auditableFields
                        add "criticality (string) : the criticality of this requirement version"
                        addAndStop "category (object) : the category of this requirement version"
                        add "status (string) : the status of this requirement version"
                        add "description (string) : the description of this requirement version"
                        addAndStop "verifying_test_cases (array) : the test cases which cover this requirement version"
                        addAndStop "custom_fields (array) : the custom fields of this requirement version"
                        add "attachments (array) : the attachments of this requirement version"

                        add DescriptorLists.linksFields
                    }

                    _links {
                        add "self  : link to this requirement version"
                        add "project : link to the project this requirement version belongs to"
                        add "requirement : link to the requirement this requirement version belongs to"
                        add "attachments : link to the attachments this requirement version owns"
                        add "issues : link to the issues of this requirement version"
                    }

                }
        ))

    }

    def "post-requirement-version"() {

        given:

        def reqVersion = SquashEntityBuilder.requirementVersion {
            id = 3l
            name = "sample requirement"
            reference = "SAMP_REQ_VER"
            versionNumber = 2
            requirement = requirement

            createdBy "User-1"
            createdOn "2017/07/19"
            lastModifiedBy "User-2"
            lastModifiedOn "2017/07/20"

            criticality "CRITICAL"
            category "CAT_PERFORMANCE"
            status "APPROVED"
            description = "<p>Approved performance requirement-version</p>"

            testCases = [
                    SquashEntityBuilder.testCase {
                        id = 4l
                        name = "verifying test case 1"
                    },
                    SquashEntityBuilder.scriptedTestCase {
                        id = 9l
                        name = "verifying scripted test case 2"
                    },
                    SquashEntityBuilder.keywordTestCase {
                        id = 14l
                        name = "verifying keyword test case 3"
                    }
            ]
        }
        def requirement = SquashEntityBuilder.requirement {
            id = 64l
            project = SquashEntityBuilder.project {
                id = 85l
                name = "myProject"
            }
            versions = [reqVersion]
        }

        def cufs = [
                SquashEntityBuilder.cufValue {
                    label = "Cuf One"
                    inputType "PLAIN_TEXT"
                    code = "CUF1"
                    value = "value_1"
                },
                SquashEntityBuilder.cufValue {
                    label = "Cuf Two"
                    inputType "PLAIN_TEXT"
                    code = "CUF2"
                    value = "value_2"
                }
        ]

        and:

        service.createRequirementVersion(_,false,false) >> reqVersion
        service.findRequirementVersion(3) >> reqVersion
        cufService.findAllCustomFieldValues(reqVersion) >> cufs
        permService.canRead(_) >> true


        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/requirement-versions/{id}?req_link=false&tc_req_link=false", 3)
                .header("Accept", "application/json"))

        then:

        /*
       * Test (with TestHelper)
       */
        res.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType("application/json"))

        withResult(res) {
            "_type".is "requirement-version"
            "id".is 3
            "name".is "sample requirement"
            "reference".is "SAMP_REQ_VER"
            "version_number".is 2

            "requirement".test {
                "_type".is "requirement"
                "id".is 64
                "name".is "sample requirement"
                selfRelIs "http://localhost:8080/api/rest/latest/requirements/64"
            }

            "created_by".is "User-1"
            "created_on".is "2017-07-19T10:00:00.000+00:00"
            "last_modified_by".is "User-2"
            "last_modified_on".is "2017-07-20T10:00:00.000+00:00"
            "criticality".is "CRITICAL"
            "category".test { "code".is "CAT_PERFORMANCE" }
            "status".is "APPROVED"
            "description".is "<p>Approved performance requirement-version</p>"

            "verifying_test_cases".hasSize 3
            "verifying_test_cases".test {
                "[0]".test {
                    "_type".is "test-case"
                    "id".is 4
                    "name".is "verifying test case 1"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/4"
                }
                "[1]".test {
                    "_type".is "scripted-test-case"
                    "id".is 9
                    "name".is "verifying scripted test case 2"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/9"
                }
                "[2]".test {
                    "_type".is "keyword-test-case"
                    "id".is 14
                    "name".is "verifying keyword test case 3"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/14"
                }
            }

            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "CUF1"
                    "label".is "Cuf One"
                    "value".is "value_1"
                }
                "[1]".test {
                    "code".is "CUF2"
                    "label".is "Cuf Two"
                    "value".is "value_2"
                }
            }

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/requirement-versions/3"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/85"
                "requirement.href".is "http://localhost:8080/api/rest/latest/requirements/64"
                "issues.href".is "http://localhost:8080/api/rest/latest/requirement-versions/3/issues"
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the requirement for which you create a new version"
                    }

                    requestParams {
                        add parameterWithName("req_link").optional().description("true if you want to copy the links between the non-obsolete requirement versions (optional)")
                        add parameterWithName("tc_req_link").optional().description("true if you want to copy the associated test cases in the new requirement version (optional)")
                        add DescriptorLists.fieldsParams
                    }

                    fields {
                        add "id (number) : the id of the requirement for which you create a new version"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the requirement version"
                        add "reference (string) : the reference of the requirement version"
                        add "version_number (number) : the version number"
                        addAndStop "requirement (object) : the requirement of this requirement version"
                        add DescriptorLists.auditableFields
                        add "criticality (string) : the criticality of this requirement version"
                        addAndStop "category (object) : the category of this requirement version"
                        add "status (string) : the status of this requirement version"
                        add "description (string) : the description of this requirement version"
                        addAndStop "custom_fields (array) : the custom fields of this requirement version"
                        addAndStop "verifying_test_cases (array) : the test cases which cover this requirement version"
                        add "attachments (array) : the attachments of this requirement version"
                        add DescriptorLists.linksFields
                    }

                    _links {
                        add "self  : link to this requirement version"
                        add "project : link to the project this requirement version belongs to"
                        add "requirement : link to the requirement this requirement version belongs to"
                        add "attachments : link to the attachments this requirement version owns"
                        add "issues : link to the issues of this requirement version"
                    }

                }
        ))

    }

    def "patch-requirement-version"() {

        given:
        def reqVersion = SquashEntityBuilder.requirementVersion {
            id = 3l
            name = "new requirement version after modify"
            reference = "SAMP_REQ_VER"
            versionNumber = 2
            requirement = requirement
            createdBy "User-1"
            createdOn "2017/07/19"
            lastModifiedBy "User-2"
            lastModifiedOn "2017/07/20"
            criticality "MAJOR"
            category "CAT_USER_STORY"
            status "APPROVED"
            description = "<p>Comment after modify</p>"

            testCases = [
                    SquashEntityBuilder.testCase {
                        id = 4l
                        name = "verifying test case 1"
                    },
                    SquashEntityBuilder.scriptedTestCase {
                        id = 9l
                        name = "verifying scripted test case 2"
                    },
                    SquashEntityBuilder.keywordTestCase {
                        id = 14l
                        name = "verifying keyword test case 3"
                    }
            ]
        }
        def requirement = SquashEntityBuilder.requirement {
            id = 64l
            project = SquashEntityBuilder.project {
                id = 85l
                name = "myProject"
            }
            versions = [reqVersion]
        }
        def cufs = [
                SquashEntityBuilder.cufValue {
                    label = "Cuf One"
                    inputType "PLAIN_TEXT"
                    code = "cuf_txt_note1"
                    value = "Star Trek style welcomed but not mandatory"
                },
                SquashEntityBuilder.cufValue {
                    label = "Cuf Two"
                    inputType "PLAIN_TEXT"
                    code = "cuf_txt_note2"
                    value = "may the force be with you"
                }
        ]
        def reqVersionjson = """{
                    "_type" : "requirement-version",
                    "name" : "new requirement version after modify",
                    "reference" : "SAMP_REQ_VER",
                    "criticality": "MAJOR",
                    "category":{
                        "code": "CAT_USER_STORY"
                    },
                    "status": "APPROVED",
                    "description": "<p>Comment after modify</p>",
                     "custom_fields": [
                    {
                        "code": "cuf_txt_note1",
                        "value": "Star Trek style welcomed but not mandatory"
                    },
                    {
                        "code": "cuf_txt_note2",
                        "value": "may the force be with you"
                    }
                ]
        }"""



        and:
        service.modifyRequirementVersion(_,_) >> reqVersion
        service.findRequirementVersion(3) >> reqVersion
        cufService.findAllCustomFieldValues(reqVersion) >> cufs
        permService.canRead(_) >> true


        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/requirement-versions/{id}", 3)
                .accept("application/json")
                .contentType("application/json")
                .content(reqVersionjson))

        then:
        res.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType("application/json"))

        withResult(res) {
            "_type".is "requirement-version"
            "id".is 3
            "name".is "new requirement version after modify"
            "reference".is "SAMP_REQ_VER"
            "version_number".is 2

            "requirement".test {
                "_type".is "requirement"
                "id".is 64
                "name".is "new requirement version after modify"
                selfRelIs "http://localhost:8080/api/rest/latest/requirements/64"
            }

            "created_by".is "User-1"
            "created_on".is "2017-07-19T10:00:00.000+00:00"
            "last_modified_by".is "User-2"
            "last_modified_on".is "2017-07-20T10:00:00.000+00:00"
            "criticality".is "MAJOR"
            "category".test { "code".is "CAT_USER_STORY" }
            "status".is "APPROVED"
            "description".is "<p>Comment after modify</p>"

            "verifying_test_cases".hasSize 3
            "verifying_test_cases".test {
                "[0]".test {
                    "_type".is "test-case"
                    "id".is 4
                    "name".is "verifying test case 1"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/4"
                }
                "[1]".test {
                    "_type".is "scripted-test-case"
                    "id".is 9
                    "name".is "verifying scripted test case 2"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/9"
                }
                "[2]".test {
                    "_type".is "keyword-test-case"
                    "id".is 14
                    "name".is "verifying keyword test case 3"
                    selfRelIs "http://localhost:8080/api/rest/latest/test-cases/14"
                }
            }

            "custom_fields".hasSize 2
            "custom_fields".test {
                "[0]".test {
                    "code".is "cuf_txt_note1"
                    "label".is "Cuf One"
                    "value".is "Star Trek style welcomed but not mandatory"
                }
                "[1]".test {
                    "code".is "cuf_txt_note2"
                    "label".is "Cuf Two"
                    "value".is "may the force be with you"
                }
            }

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/requirement-versions/3"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/85"
                "requirement.href".is "http://localhost:8080/api/rest/latest/requirements/64"
                "issues.href".is "http://localhost:8080/api/rest/latest/requirement-versions/3/issues"
            }
        }

        /*
        * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the requirement version"
                    }

                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the requirement version"
                        add "reference (string) : the reference of the requirement version"
                        add "criticality (string) : the criticality of this requirement version"
                        addAndStop "category (object) : the category of this requirement version"
                        add "status (string) : the status of this requirement version"
                        add "description (string) : the description of this requirement version"
                        addAndStop "custom_fields (array) : the custom fields of this requirement version"
                    }

                    fields {
                        add "id (number) : the id of the requirement version"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the requirement version"
                        add "reference (string) : the reference of the requirement version"
                        add "version_number (number) : the version number"
                        addAndStop "requirement (object) : the requirement of this requirement version"
                        add DescriptorLists.auditableFields
                        add "criticality (string) : the criticality of this requirement version"
                        addAndStop "category (object) : the category of this requirement version"
                        add "status (string) : the status of this requirement version"
                        add "description (string) : the description of this requirement version"
                        addAndStop "custom_fields (array) : the custom fields of this requirement version"
                        addAndStop "verifying_test_cases (array) : the test cases which cover this requirement version"
                        add "attachments (array) : the attachments of this requirement version"

                        add DescriptorLists.linksFields
                    }

                    _links {
                        add "self  : link to this requirement version"
                        add "project : link to the project this requirement version belongs to"
                        add "requirement : link to the requirement this requirement version belongs to"
                        add "attachments : link to the attachments this requirement version owns"
                        add "issues : link to the issues of this requirement version"
                    }

                }
        ))

    }

    def "associate-test-cases-to-requirement-version"() {
        given:
        def verifyingTestCases = [
                SquashEntityBuilder.testCase {
                    id = 350L
                    name = "Verify click number"
                },
                SquashEntityBuilder.testCase {
                    id = 351L
                    name = "Verify element number"
                },
                SquashEntityBuilder.testCase {
                    id = 352L
                    name = "Verify page space"
                }
        ]
        def reqVersion = SquashEntityBuilder.requirementVersion {
            id = 658L
            name = "User friendly interface"
            reference = "UX-5"
            criticality "MAJOR"
            category "CAT_ERGONOMIC"
            status "WORK_IN_PROGRESS"
            description = "<p>User interface is minimalist and easy to use.</p>"
            requirement = requirement
            testCases = verifyingTestCases
        }


        def projectMock = SquashEntityBuilder.project {
            id = 5L
            name = "Application-5"
        }
        def folder = SquashEntityBuilder.requirementFolder {
            id = 305L
            resource = SquashEntityBuilder.resource {
                name = "User Interface Folder"
            }
            project = projectMock
        }
        def requirement = SquashEntityBuilder.requirement {
            id = 64l
            project = SquashEntityBuilder.project {
                id = 85l
                name = "myProject"
            }
            versions = [reqVersion]
        }
        def customFields = [cufValue {
            label = "test_is_automated"
            inputType "CHECKBOX"
            code = "AUTOMATED"
            value = false
        }]

        and:
        1 * verifyingTestCaseService.addVerifyingTestCasesToRequirementVersion([350, 351, 352], 658)
        nodeHierarchyHelpService.findParentFor(reqVersion) >> folder

        service.findRequirementVersion(658) >> reqVersion
        cufService.findAllCustomFieldValues(reqVersion) >> customFields
        permService.canRead(_) >> true
        when:
        def res = mockMvc.perform(
                post("/api/rest/latest/requirement-versions/{id}/coverages/{testCaseIds}", 658L, "350,351,352")
                        .accept("application/json"))
        then:
        res.andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content()
                        .contentType("application/json"))
        withResult(res) {
                "_type".is "requirement-version"
                "id".is 658
                "name".is "User friendly interface"
                "reference".is "UX-5"
                "version_number".is 1
                "criticality".is "MAJOR"
                "category.code".is "CAT_ERGONOMIC"
                "status".is "WORK_IN_PROGRESS"
                "description".is "<p>User interface is minimalist and easy to use.</p>"
                "custom_fields[0]".test {
                    "code".is "AUTOMATED"
                    "label".is "test_is_automated"
                    "value".is "false"
                }
                "verifying_test_cases".hasSize 3
                "verifying_test_cases".test {
                    "[0]".test {
                        "_type".is "test-case"
                        "id".is 350
                        "name".is "Verify click number"
                    }
                    "[1]".test {
                        "_type".is "test-case"
                        "id".is 351
                        "name".is "Verify element number"
                    }
                    "[2]".test {
                        "_type".is "test-case"
                        "id".is 352
                        "name".is "Verify page space"
                    }
                }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/requirement-versions/658"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/85"
                "requirement.href".is "http://localhost:8080/api/rest/latest/requirements/64"
                "issues.href".is "http://localhost:8080/api/rest/latest/requirement-versions/658/coverages/350,351,352/issues"
            }
        }
        res.andDo(
                doc.document(documentationBuilder {
                    pathParams {
                        add "id : the id of the requirement version"
                        add "testCaseIds : the ids of the test cases to associate"
                    }
                    requestParams {
                        add DocumentationSnippets.DescriptorLists.fieldsParams
                    }
                    fields {
                        add "id (number) : the id of the requirement version"
                        add "_type (string) : the type of the entity"
                        add "name (string) : the name of the requirement version"
                        add "reference (string) : the reference of the requirement version"
                        add "version_number (number) : the version number"
                        addAndStop "requirement (object) : the requirement of this requirement version"
                        add DescriptorLists.auditableFields
                        add "criticality (string) : the criticality of this requirement version"
                        addAndStop "category (object) : the category of this requirement version"
                        add "status (string) : the status of this requirement version"
                        add "description (string) : the description of this requirement version"
                        addAndStop "custom_fields (array) : the custom fields of this requirement version"
                        addAndStop "verifying_test_cases (array) : the test cases which cover this requirement version"
                        add "attachments (array) : the attachments of this requirement version"

                        add DescriptorLists.linksFields
                    }
                    _links {
                        add "self  : link to this requirement version"
                        add "project : link to the project this requirement version belongs to"
                        add "requirement : link to the requirement this requirement version belongs to"
                        add "attachments : link to the attachments this requirement version owns"
                        add "issues : link to the issues of this requirement version"
                    }

                }))
    }

    def "disassociate-test-cases-to-requirement-version"() {
        when:
        def res = mockMvc.perform(
                delete("/api/rest/latest/requirement-versions/{id}/coverages/{testCaseIds}", 543L, "350,351")
                        .accept("application/json")
                        .contentType("application/json"))
        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * verifyingTestCaseService.removeVerifyingTestCasesToRequirementVersion(_,_)

        /*
        * Documentation
        */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the requirement version"
                        add "testCaseIds : the ids of the test cases to disassociate"
                    }
                }
        ))
    }

    def "get-requirement-version-issues"() {
        given:

        def reqVer1 = requirementVersion {
            id = 78l
            name = "sample requirement 98-1"
            versionNumber = 1
        }

        def reqVer2 = requirementVersion {
            id = 88l
            name = "sample requirement 98-2"
            versionNumber = 2
        }

        def req = requirement {

            id = 624l
            name = "sample requirement"

            resource = reqVer2

            versions = [
                reqVer1,
                reqVer2
            ]

        }

        def executions =  [
            SquashEntityBuilder.execution {
                id = 2L
                executionStatus "BLOCKED"
                lastExecutedBy = "User-1"
                lastExecutedOn "2017/06/24"
            }] as Set

        def requirementsVersion = [reqVer2] as List<RequirementVersion>
        def requirementVersion = [reqVer2] as Set<RequirementVersion>

        def issue = new IssueDto(
            "165",
            new URL("http://192.175.1.51/bugzilla/show_bug.cgi?id=165"),
            executions,
            requirementVersion
        )

        def issues = new PageImpl<IssueDto>([issue], PageRequest.of(0, 20), 1)


        and:
//        requirementVersionManagerService.findById() >> reqVer2
        service.getExecutionIdsByRequirementVersion(_) >>  executions.id
        restIssueService.getIssuesFromExecutionIds(_, _) >> issues
        service.getRequirementVersionFromIssue(_,_) >> requirementsVersion
        controller.findRequirementVersionIssues(_,_) >> issues

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders
            .get("/api/rest/latest/requirement-versions/{id}/issues", 88)
            .header("Accept", "application/json"))

        then:
        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.issues".hasSize 1
            "_embedded.issues".test {
                "[0]".test {
                    "remoteIssueId".is "165"
                    "url".is "http://192.175.1.51/bugzilla/show_bug.cgi?id=165"
                    "executions".hasSize 1
                    "executions".test {
                        "[0]".test {
                            "_type".is "execution"
                            "id".is 2
                            selfRelIs "http://localhost:8080/api/rest/latest/executions/2"
                        }
                    }
                    "requirement-versions".hasSize 1
                    "requirement-versions".test {
                        "[0]".test {
                            "_type".is "requirement-version"
                            "id".is 88
                            selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/88"
                        }
                    }
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/requirement-versions/88/issues?page=0&size=20"
            "page".test {
                "size".is 20
                "totalElements".is 1
                "totalPages".is 1
                "number".is 0
            }

        }

        /*
   * Documentation
   */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the requirement version"
                }
                requestParams {
                    add DescriptorLists.paginationParams
                    add DescriptorLists.fieldsParams
                }
                fields {
                    embeddedAndStop "issues (array) : the issues of this requirement version"
                    add DocumentationSnippets.DescriptorLists.issuesFields
                    add "_embedded.issues[].requirement-versions (array) : the requirement versions linked to the issue."
                    add DescriptorLists.paginationFields
                    add DescriptorLists.linksFields
                }
                _links {
                    add DescriptorLists.paginationLinks
                }
            }
        ))
    }


}
