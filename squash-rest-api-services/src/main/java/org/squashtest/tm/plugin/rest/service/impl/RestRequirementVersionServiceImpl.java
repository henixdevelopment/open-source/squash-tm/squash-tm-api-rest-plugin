/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.api.security.acls.Permissions;
import org.squashtest.tm.domain.customfield.RawValue;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.requirement.NewRequirementVersionDto;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.requirement.RequirementLibrary;
import org.squashtest.tm.domain.requirement.RequirementSyncExtender;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.jooq.domain.Tables;
import org.squashtest.tm.plugin.rest.jackson.model.ParentEntity;
import org.squashtest.tm.plugin.rest.jackson.model.RemoteReqInfoRecord;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementDto;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementVersionDto;
import org.squashtest.tm.plugin.rest.repository.RestRequirementRepository;
import org.squashtest.tm.plugin.rest.repository.RestRequirementSyncExtenderRepository;
import org.squashtest.tm.plugin.rest.repository.RestRequirementVersionRepository;
import org.squashtest.tm.plugin.rest.service.RestRequirementVersionService;
import org.squashtest.tm.plugin.rest.service.helper.CustomFieldValueHelper;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;
import org.squashtest.tm.service.internal.repository.hibernate.ProjectDaoImpl;
import org.squashtest.tm.service.project.ProjectFinder;
import org.squashtest.tm.service.requirement.CustomRequirementVersionManagerService;
import org.squashtest.tm.service.requirement.RequirementLibraryNavigationService;
import org.squashtest.tm.service.requirement.RequirementVersionManagerService;
import org.squashtest.tm.service.user.UserAccountService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.squashtest.tm.jooq.domain.Tables.*;
import static org.squashtest.tm.jooq.domain.Tables.ISSUE;
import static org.squashtest.tm.jooq.domain.tables.RequirementVersion.REQUIREMENT_VERSION;
import static org.squashtest.tm.plugin.rest.utils.PaginationUtils.emptyPage;

@Service
@Transactional
public class RestRequirementVersionServiceImpl implements RestRequirementVersionService {

    private static final String WRONG_PARENT_REST_TYPE_ERROR_MESSAGE = "Rest type %s is not a valid parent. You should validate this before.";
    public static final String MAX_VERSIONS = "MAX_VERSIONS";
    public static final String REQUIREMENT_ID = "REQUIREMENT_ID";
    public static final String MAX_VERSION_NUMBER = "MAX_VERSION_NUMBER";

    @Inject
    private ProjectFinder projectFinder;

    @Inject
    private RequirementVersionManagerService service;

    @Inject
    private RestRequirementRepository reqDao;

    @Inject
    private RestRequirementVersionRepository reqVerRepository;

    @Inject
    private RestRequirementSyncExtenderRepository syncExtenderDao;

    @Inject
    private RequirementLibraryNavigationService requirementLibraryNavigationService;

    @Inject
    private CustomFieldValueHelper customFieldValueConverter;

    @Inject
    private RestRequirementRepository restRequirementRepository;

    @Inject
    private RequirementVersionPatcher requirementVersionPatcher;

    @Inject
    private CustomRequirementVersionManagerService customRequirementVersionManagerService;

    @Inject
    private UserAccountService userAccountService;

    @Inject
    private ProjectDaoImpl projectDaoImpl;

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private DSLContext dslContext;


    @Override
    @Transactional(readOnly = true)
    public Page<Requirement> findAllReadable(Pageable paging) {
        Collection<Long> projectIds = projectFinder.findReadableProjectIdsOnRequirementLibrary();
        return projectIds.isEmpty() ? emptyPage(paging) : reqDao.findAllInProjects(projectIds, paging);
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("@apiSecurity.hasPermission(#requirementId,'org.squashtest.tm.domain.requirement.Requirement' , 'READ')")
    public Requirement findRequirement(long requirementId) {
        return service.findRequirementById(requirementId);
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("@apiSecurity.hasPermission(#requirementId,'org.squashtest.tm.domain.requirement.Requirement' , 'READ')")
    public Page<Requirement> findRequirementChildren(long requirementId, Pageable paging) {
        return reqDao.findRequirementChildren(requirementId, paging);
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("@apiSecurity.hasPermission(#requirementId,'org.squashtest.tm.domain.requirement.Requirement' , 'READ')")
    public Page<Requirement> findRequirementAllChildren(long requirementId, Pageable paging) {
        return reqDao.findRequirementAllChildren(requirementId, paging);
    }


    @Override
    @Transactional(readOnly = true)
    public RequirementVersion findRequirementVersion(long versionId) {
        return service.findById(versionId);
    }

    @Override
    public Requirement createRequirement(RequirementDto requirementDto) {

        NewRequirementVersionDto newRequirementVersionDto = convertRequirementDtoToNewRequirementVersionDto(requirementDto);

        return addToParent(requirementDto, newRequirementVersionDto);
    }

    @Override
    public void deleteRequirements(List<Long> reqIds) {
        requirementLibraryNavigationService.deleteNodes(reqIds);
    }

    @Override
    @Transactional(readOnly=true)
    public List<Requirement> findSynchronizedRequirementsBy(String remoteKey, String serverName) {
        Collection<RequirementSyncExtender> extenders = syncExtenderDao.findAllByRemoteReqIdAndServerName(remoteKey, serverName);
        if (!userAccountService.findCurrentUserDto().isAdmin()) {
            List<Long> accessibleRequirementLibraryIds = findAllRequirementLibraryIdsByPermission(userAccountService.findCurrentUserDto().getPartyIds(), Permissions.READ.getMask());
            extenders = extenders.stream()
                .filter(ex -> accessibleRequirementLibraryIds.contains(ex.getRequirement().getLibrary().getId()))
                .toList();
        }
        return extenders.stream().map(RequirementSyncExtender::getRequirement).toList();
    }

    @Override
    public List<Long> findReqIdsByVersionIds(List<Long> versionIds) {
        return reqVerRepository.findRequirementIdsByRequirementVersionIds(versionIds);
    }

    @Override
    public RequirementVersion createRequirementVersion(long requirementId, boolean inheritReqLinks, boolean inheritTestcasesReqLinks) {
        customRequirementVersionManagerService.createNewVersion(requirementId,inheritReqLinks,inheritTestcasesReqLinks);
        Requirement currentRequirement = reqDao.getReferenceById(requirementId);

        return currentRequirement.getCurrentVersion();
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#versionId,'org.squashtest.tm.domain.requirement.RequirementVersion' , 'WRITE')")
    public RequirementVersion modifyRequirementVersion(RequirementVersionDto requirementVersionDto, Long versionId) {
        RequirementVersion reqVersion = service.findById(versionId);
        requirementVersionPatcher.patch(reqVersion, requirementVersionDto);

        if(requirementVersionDto.isHasCufs()){
            requirementVersionPatcher.patchCustomFieldValue(requirementVersionDto,reqVersion);
        }

        return reqVersion;
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#requirementId,'org.squashtest.tm.domain.requirement.Requirement' , 'WRITE')")
    public Requirement modifyRequirement(RequirementDto requirementDto, Long requirementId) {
        Requirement requirement = restRequirementRepository.getReferenceById(requirementId);
        RequirementVersion reqVersion = requirement.getCurrentVersion();
        //Status traitement particulier
        requirementVersionPatcher.patch(reqVersion, requirementDto.getCurrentVersion());
        if(requirementDto.getCurrentVersion().isHasCufs()){
            requirementVersionPatcher.patchCustomFieldValue(requirementDto.getCurrentVersion(),reqVersion);
        }


        return requirement;
    }

    @Override
    public Long findCurrentVersionIdByRequirementId(Long requirementId) {
        Long currentVersionId =  dslContext.select(REQUIREMENT.CURRENT_VERSION_ID).from(Tables.REQUIREMENT)
            .where(REQUIREMENT.RLN_ID.eq(requirementId)).fetchOneInto(Long.class);

        if (currentVersionId == null) {
            throw new EntityNotFoundException("Try to retrieve Requirement with given id: %d, but could not find it".formatted(requirementId));
        }

        return currentVersionId;
    }

    @Override
    public List<RequirementVersion> getRequirementVersionFromIssue(String remoteIssueId, Long executionId) {

        return dslContext.select(
                REQUIREMENT.CURRENT_VERSION_ID.as(RequestAliasesConstants.ID), RESOURCE.NAME.as(RequestAliasesConstants.NAME))
            .from(REQUIREMENT_VERSION)
            .join(REQUIREMENT)
            .on(REQUIREMENT.RLN_ID.eq(REQUIREMENT_VERSION.REQUIREMENT_ID))
            .join(RLN_RESOURCE).on(RLN_RESOURCE.RLN_ID.eq(REQUIREMENT.RLN_ID))
            .join(RESOURCE).on(RESOURCE.RES_ID.eq(RLN_RESOURCE.RES_ID))
            .join(REQUIREMENT_VERSION_COVERAGE)
            .on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
            .join(EXECUTION)
            .on(EXECUTION.TCLN_ID.eq(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID))
            .join(EXECUTION_ISSUES_CLOSURE)
            .on(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
            .join(ISSUE)
            .on(ISSUE.ISSUE_ID.eq(EXECUTION_ISSUES_CLOSURE.ISSUE_ID))
            .where(
                EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(executionId)
                    .and(ISSUE.REMOTE_ISSUE_ID.eq(remoteIssueId))
            )
            .fetchInto(RequirementVersion.class);
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#versionId,'org.squashtest.tm.domain.requirement.RequirementVersion', 'READ')")
    public List<Long> getExecutionIdsByRequirementVersion(Long versionId) {

        List<Requirement> allRequirement = new ArrayList<>();

        Page<Requirement> requirementPage;
        int page = 0;

        List<Long> requirementId = reqVerRepository.findRequirementIdsByRequirementVersionIds(Collections.singletonList(versionId));
        Optional<Long> premierIdOptional = requirementId.stream().findFirst();
        do {
            requirementPage = restRequirementRepository.findRequirementAllChildren(premierIdOptional.get(), PageRequest.of(page, Integer.MAX_VALUE));
            allRequirement.addAll(requirementPage.getContent());
            page++;
        } while (requirementPage.hasNext());

        List<Long> finalRequirementIds = new ArrayList<>(allRequirement.stream()
            .map(Requirement::getId)
            .toList());

        List<Long> linkedRequirementIds = getRequirementIdsLinkedToHighLevelRequirement(versionId);

        finalRequirementIds.addAll(linkedRequirementIds);

        return dslContext.select(EXECUTION.EXECUTION_ID)
            .from(EXECUTION)
            .join(REQUIREMENT_VERSION_COVERAGE).on(REQUIREMENT_VERSION_COVERAGE.VERIFYING_TEST_CASE_ID.eq(EXECUTION.TCLN_ID))
            .join(REQUIREMENT_VERSION).on(REQUIREMENT_VERSION_COVERAGE.VERIFIED_REQ_VERSION_ID.eq(REQUIREMENT_VERSION.RES_ID))
            .leftJoin(DSL.select(REQUIREMENT_VERSION.REQUIREMENT_ID, DSL.max(REQUIREMENT_VERSION.VERSION_NUMBER).as(MAX_VERSION_NUMBER))
                .from(REQUIREMENT_VERSION)
                .where(REQUIREMENT_VERSION.REQUIREMENT_ID.in(finalRequirementIds))
                .groupBy(REQUIREMENT_VERSION.REQUIREMENT_ID)
                .asTable(MAX_VERSIONS))
            .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(DSL.field(DSL.name(MAX_VERSIONS, REQUIREMENT_ID), Long.class))
                .and(REQUIREMENT_VERSION.VERSION_NUMBER.eq(DSL.field(DSL.name(MAX_VERSIONS, MAX_VERSION_NUMBER), Long.class).coerce(REQUIREMENT_VERSION.VERSION_NUMBER.getDataType()))))
            .where(REQUIREMENT_VERSION.RES_ID.eq(versionId)
                .or(REQUIREMENT_VERSION.REQUIREMENT_ID.in(finalRequirementIds)
                    .and(DSL.field(DSL.name(MAX_VERSIONS, REQUIREMENT_ID), Long.class).isNotNull())))
            .fetchInto(Long.class);

    }

    @Override
    public RemoteReqInfoRecord getRemoteReqInfoByRequirementId(Long id) {
        return dslContext.select(
                REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_ID,
                REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_URL,
                REQUIREMENT_SYNC_EXTENDER.REMOTE_REQ_PERIMETER_STATUS
            )
            .from(REQUIREMENT_SYNC_EXTENDER)
            .where(REQUIREMENT_SYNC_EXTENDER.REQUIREMENT_ID.eq(id))
            .fetchOneInto(RemoteReqInfoRecord.class);
    }

    private List<Long> getRequirementIdsLinkedToHighLevelRequirement(Long versionId) {
        return dslContext.selectDistinct(REQUIREMENT.RLN_ID)
            .from(REQUIREMENT)
            .join(REQUIREMENT_VERSION)
            .on(REQUIREMENT_VERSION.REQUIREMENT_ID.eq(REQUIREMENT.RLN_ID))
            .where(REQUIREMENT.HIGH_LEVEL_REQUIREMENT_ID.eq(
                dslContext.select(REQUIREMENT_VERSION.REQUIREMENT_ID)
                    .from(REQUIREMENT_VERSION)
                    .where(REQUIREMENT_VERSION.RES_ID.eq(versionId))))
            .fetchInto(Long.class);
    }


    private Requirement addToParent(RequirementDto requirementDto, NewRequirementVersionDto newRequirementVersionDto) {
        ParentEntity parent = requirementDto.getParent();
        Requirement requirement = switch (parent.getRestType()) {
            case PROJECT -> addRequirementToLibrary(newRequirementVersionDto, parent);
            case REQUIREMENT_FOLDER -> addRequirementToFolder(newRequirementVersionDto, parent);
            case REQUIREMENT -> addRequirementToRequirement(newRequirementVersionDto, parent);
            default -> throw new IllegalArgumentException(String.format(WRONG_PARENT_REST_TYPE_ERROR_MESSAGE, parent.getRestType()));
        };
        requirement.getCurrentVersion().updateStatusWithoutCheck(requirementDto.getCurrentVersion().getStatus());
        return requirement;
    }

    private Requirement addRequirementToLibrary(NewRequirementVersionDto newRequirementVersionDto, ParentEntity parent) {
        Project project = entityManager.find(Project.class, parent.getId());
        if (project != null) {
            return requirementLibraryNavigationService.addRequirementToRequirementLibrary(project.getRequirementLibrary().getId(), newRequirementVersionDto, new ArrayList<Long>());
        } else {
            throw new IllegalArgumentException("Programmatic error : project with id " + parent.getId() + "is unknown. You should validate this before.");
        }
    }

    private Requirement addRequirementToFolder(NewRequirementVersionDto newRequirementVersionDto, ParentEntity parent) {
        RequirementFolder requirementFolder = entityManager.find(RequirementFolder.class, parent.getId());
        if (requirementFolder != null) {
            return requirementLibraryNavigationService.addRequirementToRequirementFolder(requirementFolder.getId(), newRequirementVersionDto, new ArrayList<Long>());
        } else {
            throw new IllegalArgumentException("Programmatic error : requirement folder with id " + parent.getId() + "is unknown. You should validate this before.");
        }
    }

    private Requirement addRequirementToRequirement(NewRequirementVersionDto newRequirementVersionDto, ParentEntity parent) {
        Requirement req = entityManager.find(Requirement.class, parent.getId());
        if (req != null) {
            return requirementLibraryNavigationService.addRequirementToRequirement(req.getId(), newRequirementVersionDto, new ArrayList<Long>());
        } else {
            throw new IllegalArgumentException("Programmatic error : requirement folder with id " + parent.getId() + "is unknown. You should validate this before.");
        }
    }

    private NewRequirementVersionDto convertRequirementDtoToNewRequirementVersionDto(RequirementDto requirementDto) {
        NewRequirementVersionDto newRequirementVersionDto = new NewRequirementVersionDto();

        Map<Long, RawValue> customFieldMap = customFieldValueConverter.convertCustomFieldDtoToMap(requirementDto.getCustomFields());

        newRequirementVersionDto.setName(requirementDto.getName());
        newRequirementVersionDto.setReference(requirementDto.getCurrentVersion().getReference());
        newRequirementVersionDto.setCategory(requirementDto.getCurrentVersion().getCategory().getCode());
        newRequirementVersionDto.setCriticality(requirementDto.getCurrentVersion().getCriticality());
        newRequirementVersionDto.setDescription(requirementDto.getDescription());
        newRequirementVersionDto.setCustomFields(customFieldMap);

        return newRequirementVersionDto;

    }

    private List<Long> findAllRequirementLibraryIdsByPermission(List<Long> partyIds, int permission) {
        return projectDaoImpl.findAllProjectIdsByPermissionMaskAndClassName(partyIds, permission, RequirementLibrary.class.getName(), PROJECT.RL_ID);
    }
}
