/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.EnumSet;

/**
 * Created by jthebault on 02/06/2017.
 */
public abstract class AbstractPatcher {

    void patch(Object entity, Object dto) {

        Class entityClass = entity.getClass();
        Class dtoClass = dto.getClass();

        for (PatchableAttributes patchableProperty : getPatchableAttributes()) {
            try {
                Method getter = dtoClass.getMethod(patchableProperty.getGetterName());
                Method setter = entityClass.getMethod(patchableProperty.getSetterName(), patchableProperty.getPropertyClass());

                Object value = getter.invoke(dto);

                if (value != null) {
                    setter.invoke(entity, value);
                }

            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
    }

    abstract EnumSet<? extends PatchableAttributes> getPatchableAttributes();
}
