/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.controller.helper.ResourceLinksHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto;
import org.squashtest.tm.plugin.rest.jackson.model.IterationDto;
import org.squashtest.tm.plugin.rest.service.RestCampaignService;
import org.squashtest.tm.plugin.rest.service.RestIssueService;
import org.squashtest.tm.plugin.rest.service.RestIterationService;
import org.squashtest.tm.plugin.rest.validators.IterationPatchValidator;
import org.squashtest.tm.plugin.rest.validators.IterationPostValidator;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.squashtest.tm.plugin.rest.controller.RestIterationTestPlanItemController.ITPI_DYNAMIC_FILTER;
import static org.squashtest.tm.plugin.rest.controller.RestTestSuiteController.TEST_SUITE_DYNAMIC_FILTER;

/**
 * Created by jthebault on 16/06/2017.
 */
@RestApiController(Iteration.class)
@UseDefaultRestApiConfiguration
public class RestIterationController extends BaseRestController{

    public static final String ITERATION_DYNAMIC_FILTER = "*, parent[name], -test_plan, test_suites[name],-path";
    public static final String ITPI_EMBEDDED_FILTER = ITPI_DYNAMIC_FILTER + ",-iteration";
    public static final String TEST_PLAN = "test-plan";
    public static final String TEST_SUITES = "test-suites";
    public static final String TEST_SUITE_EMBEDDED_FILTER = TEST_SUITE_DYNAMIC_FILTER + ",-iteration";

    @Inject private RestIterationService restIterationService;

    @Inject private RestCampaignService restCampaignService;

    @Inject private IterationPostValidator iterationPostValidator;

    @Inject private IterationPatchValidator iterationPatchValidator;

    @Inject private ResourceLinksHelper linksHelper;

    @Inject private RestIssueService restIssueService;

    @GetMapping(value = "/iterations/{id}")
    @EntityGetter
    @ResponseBody
    @DynamicFilterExpression(ITERATION_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<Iteration>> findIteration(@PathVariable("id") long id){

        Iteration iteration = restIterationService.getOne(id);

        EntityModel<Iteration> res = toEntityModel(iteration);

        res.add(linkService.createLinkTo(iteration.getProject()));
        res.add(linkService.createLinkTo(iteration.getCampaign()));
        res.add(createRelationTo(TEST_SUITES));
        res.add(createRelationTo(TEST_PLAN));
        res.add(createRelationTo("attachments"));
        res.add(createRelationTo("issues"));
        return ResponseEntity.ok(res);
    }

    @GetMapping(value = "/iterations", params = "iterationName")
    @ResponseBody
    @DynamicFilterExpression(ITERATION_DYNAMIC_FILTER)
    public ResponseEntity<PagedModel<EntityModel<Iteration>>> findIterationsByName(@RequestParam("iterationName") String iterationName, Pageable pageable) {
        Page<Iteration> iterations = restIterationService.findAllIterationsByName(iterationName, pageable);
        PagedModel<EntityModel<Iteration>> res = toPagedModel(iterations);
        return ResponseEntity.ok(res);
    }

    @PostMapping("/campaigns/{id}/iterations")
    @ResponseBody
    @DynamicFilterExpression(ITERATION_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<Iteration>> createIteration(@RequestBody IterationDto iterationDto,
                                                                  @PathVariable("id") long campaignId) throws BindException {
        Campaign campaign = restCampaignService.getOne(campaignId);
        iterationDto.setProjectId(campaign.getProject().getId());
        validatePostIteration(iterationDto);

        Iteration iteration = restIterationService.createIteration(iterationDto, campaignId);
        EntityModel<Iteration> res = toEntityModel(iteration);
        linksHelper.addAllLinksForIteration(res);

        return ResponseEntity.status(HttpStatus.CREATED).body(res);

    }

    @PatchMapping(value = "/iterations/{id}")
    @ResponseBody
    @DynamicFilterExpression(ITERATION_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<Iteration>> patchIteration(@RequestBody IterationDto patch,
                                                                 @PathVariable("id") long id) throws BindException {
        Iteration iteration = restIterationService.getOne(id);
        patch.setId(id);
        patch.setProjectId(iteration.getProject().getId());

        validatePatchIteration(patch);
        iteration = restIterationService.patchIteration(patch, id);

        EntityModel<Iteration> res = toEntityModel(iteration);
        linksHelper.addAllLinksForIteration(res);

        return ResponseEntity.ok(res);
    }

    @DeleteMapping(value = "/iterations/{ids}")
    @ResponseBody
    @DynamicFilterExpression(ITERATION_DYNAMIC_FILTER)
    public ResponseEntity<EntityModel<Iteration>> deleteIterations(@PathVariable("ids")  List<Long> ids) {

        restIterationService.deleteIterationsByIds(ids);

        return ResponseEntity.noContent().build();
    }

    @GetMapping("/iterations/{id}/test-plan")
    @ResponseBody
    @DynamicFilterExpression(ITPI_EMBEDDED_FILTER)
    public ResponseEntity<PagedModel<EntityModel<IterationTestPlanItem>>> findIterationTestPlan(@PathVariable("id") long iterationId,Pageable pageable){
        Page<IterationTestPlanItem> steps = restIterationService.findIterationTestPlan(iterationId, pageable);
        PagedModel<EntityModel<IterationTestPlanItem>> res = toPagedResourcesWithRel(steps, TEST_PLAN);

        return ResponseEntity.ok(res);

    }

    @GetMapping("/iterations/{id}/test-suites")
    @ResponseBody
    @DynamicFilterExpression(TEST_SUITE_EMBEDDED_FILTER)
    public ResponseEntity<PagedModel<EntityModel<TestSuite>>> findIterationTestSuite(@PathVariable("id") long iterationId,Pageable pageable){
        Page<TestSuite> steps = restIterationService.findIterationTestSuite(iterationId, pageable);
        PagedModel<EntityModel<TestSuite>> res = toPagedResourcesWithRel(steps, TEST_SUITES);

        return ResponseEntity.ok(res);

    }

     private void validatePostIteration(IterationDto iterationDto) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(iterationDto, "post-iteration");
        iterationPostValidator.validate(iterationDto, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(iterationDto, errors, "post-iteration");

    }

    private void validatePatchIteration(IterationDto patch) throws BindException {
        List<Errors> errors = new ArrayList<>();

        BindingResult validation = new BeanPropertyBindingResult(patch, "patch-iteration");
        iterationPatchValidator.validate(patch, validation);

        if (validation.hasErrors()) {
            errors.add(validation);
        }

        ErrorHandlerHelper.throwIfError(patch, errors, "patch-iteration");
    }

    @GetMapping("/iterations/{id}/issues")
    @ResponseBody
    @DynamicFilterExpression("executions[id]")
    public ResponseEntity<PagedModel<EntityModel<IssueDto>>> findIterationIssues(@PathVariable("id") long id, Pageable pageable) {

        List<Long> executionIds =  restIterationService.getExecutionIdsByIteration(id);

        Page<IssueDto> pagedIssue = restIssueService.getIssuesFromExecutionIds(executionIds, pageable);

        PagedModel<EntityModel<IssueDto>> res = pageAssembler.toModel(pagedIssue);

        return ResponseEntity.ok(res);
    }
}
