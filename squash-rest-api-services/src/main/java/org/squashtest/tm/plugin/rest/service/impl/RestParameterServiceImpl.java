/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.testcase.Parameter;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.plugin.rest.jackson.model.ParameterDto;
import org.squashtest.tm.plugin.rest.repository.RestParameterRepository;
import org.squashtest.tm.plugin.rest.service.RestParameterService;
import org.squashtest.tm.service.internal.repository.ParameterDao;
import org.squashtest.tm.service.security.PermissionEvaluationService;
import org.squashtest.tm.service.security.PermissionsUtils;
import org.squashtest.tm.service.testcase.ParameterModificationService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.Collections;

/**
 * Created by jthebault on 30/05/2017.
 */
@Service
@Transactional
public class RestParameterServiceImpl implements RestParameterService {

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private ParameterDao parameterDao;
    @Inject
    private RestParameterRepository restParameterRepository;
    @Inject
    private PermissionEvaluationService permissionEvaluationService;
    @Inject
    private ParameterModificationService parameterModificationService;
    @Inject
    private PermissionEvaluationService permissionService;

    @Override
    @Transactional(readOnly=true)
    public Parameter getOne(long id) throws AccessDeniedException {
        Parameter parameter = restParameterRepository.retrieveById(id);
        if(parameter == null){
            throw new EntityNotFoundException("The parameter with id : " + id + " do not exist.");
        }
        //checking permission on the test case not the param directly
        TestCase testCase = parameter.getTestCase();
        if (permissionEvaluationService.canRead(testCase)){
            return parameter;
        } else {
            throw new AccessDeniedException("Access denied");
        }
    }

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#testCaseId,'org.squashtest.tm.domain.testcase.TestCase' , 'READ')")
    public Page<Parameter> findAllByTestCaseId(long testCaseId, Pageable pageable) {
        return restParameterRepository.findByTestCase_Id(testCaseId,pageable);
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#parameterDto.getReferencedTestCase().getId(),'org.squashtest.tm.domain.testcase.TestCase' , 'WRITE')")
    public Parameter addParameter(ParameterDto parameterDto) {
        Parameter parameter = new Parameter(parameterDto.getName());
        parameter.setDescription(parameterDto.getDescription());
        parameterModificationService.addNewParameterToTestCase(parameter, parameterDto.getReferencedTestCase().getId());
        return parameter;
    }

    @Override
    public Parameter modifyParameter(ParameterDto parameterDto) {
        Parameter parameter = parameterDao.getReferenceById(parameterDto.getId());
        PermissionsUtils.checkPermission(permissionService, Collections.singletonList(parameter.getTestCase().getId()), "WRITE", TestCase.class.getName());
        if(parameterDto.isHasName()){
            parameter.setName(parameterDto.getName());
        }
        if(parameterDto.isHasDescription()){
            parameter.setDescription(parameterDto.getDescription());
        }
        return parameter;
    }

    @Override
    public void deleteParameter(Long parameterId) {
        Parameter parameter = parameterDao.getReferenceById(parameterId);
        PermissionsUtils.checkPermission(permissionService, Collections.singletonList(parameter.getTestCase().getId()), "WRITE", TestCase.class.getName());
        parameterModificationService.removeById(parameterId);
    }
}
