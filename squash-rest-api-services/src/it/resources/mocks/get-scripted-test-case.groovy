/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.automatedTest
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.automatedTestTechnology
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufValue
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.dataset
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.parameter
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirementVersion
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scmRepository
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scmServer
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scriptedTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testAutomatioProject

/**
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 - 2019 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */

def autoTest = automatedTest {
    id = 2l
    name = "script_custom_field_params_all.ta"
    project = testAutomatioProject {
        id = 2l
        jobName = "ta-tests"
    }
}

def automatedTestTechno = automatedTestTechnology {
    id = 2l
    name = "Cucumber"
}

def scmRepo = scmRepository {
    id = 2l
    name = "repo01"
    workingBranch = "master"
    scmServer = scmServer {
        id = 2l
        url = "https://github.com/test"
    }
}

def stc = scriptedTestCase {
    project = project {
        id = 14l
        name = "sample project"
    }

    id = 1l
    name = "coffee machine test"

    script = "This is a default Gherkin script"

    createdBy "admin"
    createdOn "2020/04/02"
    lastModifiedBy "admin"
    lastModifiedOn "2020/04/03"

    reference = "stc1"
    importance "MEDIUM"
    nature "NAT_PERFORMANCE_TESTING"
    type "TYP_EVOLUTION_TESTING"

    prerequisite = "<p>You must have a coffee machine and a coffee bag</p>\n"
    description = "<p>check that you can walk through the API (literally)</p>\n"

    steps = []

    parameters = [
            parameter {
                id = 1l
                name = "trademark"
            },
            parameter {
                id = 2l
                name = "price"
            }
    ] as Set

    datasets = [
            dataset {
                id = 1l
                name = "philips"
            },
            dataset {
                id = 2l
                name = "100"
            }

    ] as Set

    requirementVersions = [
            requirementVersion {
                id = 255l
                name = "Must have electricity"
                withDefaultRequirement()
            }
    ]

    automatedTest = autoTest
    automatedTestTechnology = automatedTestTechno
    scmRepository = scmRepo
    automatedTestReference = "repo01/src/resources/script_custom_field_params_all.ta#Test_case_1"
    uuid = "4a7cdf59-d6ed-4380-89d6-cc393952e413"
}

/*
 * The custom fields
 */
def tcCufs = [
        cufValue {
            code = "CF_TXT"
            inputType "PLAIN_TEXT"
            label = "test level"
            value = "mandatory"
        },
        cufValue {
            code = "CF_TAGS"
            inputType "TAG"
            label = "see also"
            value = ["expresso", "starbuck"]
        }
]

/*
 * Now pack the mocks and return
 * 
 */
[
        tc    : stc,
        tcCufs: tcCufs,
]
