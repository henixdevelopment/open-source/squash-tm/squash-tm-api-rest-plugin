/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.plugin.rest.controller.helper.ErrorHandlerHelper;
import org.squashtest.tm.plugin.rest.core.jackson.DynamicFilterExpression;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.jackson.model.RestActionWordDto;
import org.squashtest.tm.plugin.rest.service.RestActionWordService;
import org.squashtest.tm.plugin.rest.validators.ActionWordPatchValidator;
import org.squashtest.tm.plugin.rest.validators.ActionWordPostValidator;
import org.squashtest.tm.plugin.rest.validators.EntityValidator;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;

@UseDefaultRestApiConfiguration
@RestApiController(ActionWord.class)
public class RestActionWordController extends BaseRestController {

    private static final String PATCH_ACTION_WORD = "patch-action-word";
    private static final String POST_ACTION_WORD = "post-action-word";

    @Inject
    private RestActionWordService restActionWordService;
    @Inject
    private ActionWordPatchValidator actionWordPatchValidator;

    @Inject
    private ActionWordPostValidator actionWordPostValidator;


    @EntityGetter
    @DynamicFilterExpression("*, project[name], parameters[name, default_value], test_cases[reference, name, importance, status]")
    @GetMapping("/action-words/{id}")
    public ResponseEntity<EntityModel<ActionWord>> getActionWord(@PathVariable("id") long id) {
        ActionWord actionWord = restActionWordService.getOne(id);
        EntityModel<ActionWord> resource = toEntityModel(actionWord);
        addAllLinksForActionWord(resource);
        return ResponseEntity.ok(resource);
    }

    @DynamicFilterExpression("word")
    @GetMapping("/action-words")
    public ResponseEntity<PagedModel<EntityModel<ActionWord>>> getAllReadableActionWords(Pageable pageable) {
        Page<ActionWord> actionWords = restActionWordService.findAllReadableActionWords(pageable);
        PagedModel<EntityModel<ActionWord>> res = toPagedModel(actionWords);
        return ResponseEntity.ok(res);
    }

    @DynamicFilterExpression("*, project[name], parameters[name, default_value], test_cases[reference, name, importance, status]")
    @PatchMapping("/action-words/{id}")
    public ResponseEntity<EntityModel<ActionWord>> patchActionWord(
            @PathVariable("id") long id, @RequestBody RestActionWordDto patch)
            throws BindException {
        patch.setId(id);
        validateSourceDto(patch, actionWordPatchValidator, PATCH_ACTION_WORD);
        ActionWord actionWord = restActionWordService.patchActionWord(id, patch);

        EntityModel<ActionWord> res = toEntityModel(actionWord);
        addAllLinksForActionWord(res);
        return ResponseEntity.ok(res);
    }

    @DynamicFilterExpression("*, project[name], parameters[name, default_value], " +
            "-last_modified_by, -last_modified_on, -last_implementation_technology, -last_implementation_date," +
            "-test_cases")
    @PostMapping("/action-words")
    public ResponseEntity<EntityModel<ActionWord>> postActionWord(@RequestBody RestActionWordDto post)
            throws BindException {
        validateSourceDto(post, actionWordPostValidator, POST_ACTION_WORD);
        ActionWord actionWord = restActionWordService.createActionWord(post);

        EntityModel<ActionWord> res = toEntityModel(actionWord);
        addAllLinksForActionWord(res);
        return ResponseEntity.status(CREATED).body(res);
    }

    private void validateSourceDto(
            RestActionWordDto actionWordDto, EntityValidator validatorService, String validationName)
            throws BindException {
        List<Errors> errors = new ArrayList<>();
        BindingResult validationLog = new BeanPropertyBindingResult(actionWordDto, validationName);
        validatorService.validate(actionWordDto, validationLog);
        if(validationLog.hasErrors()) {
            errors.add(validationLog);
        }
        ErrorHandlerHelper.throwIfError(actionWordDto, errors, validationName);
    }

    @DeleteMapping("/action-words/{ids}")
    public ResponseEntity<String> deleteActionWord(@PathVariable("ids") List<Long> ids) {
        List<Long> notDeleteActionWordIds = restActionWordService.deleteActionWordsByIds(ids);
        if (notDeleteActionWordIds.isEmpty()) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(
                    generateNotDeletedActionWordsMessage(notDeleteActionWordIds));
        }
    }

    private String generateNotDeletedActionWordsMessage(List<Long> actionWordIdsUsedInKeywordTestSteps) {
        StringBuilder builder = new StringBuilder(
                "The action words with the following identifiers were not deleted because they are used in at least one test step: ");
        for (Long actionWordId : actionWordIdsUsedInKeywordTestSteps) {
            builder.append(actionWordId).append(", ");
        }
        return builder.substring(0, builder.length() - 2);
    }

    @DynamicFilterExpression("word")
    @GetMapping("/projects/{id}/action-words")
    public ResponseEntity<PagedModel<EntityModel<ActionWord>>> getAllActionWordsByProject(
            @PathVariable("id") long projectId, Pageable pageable) {
        Page<ActionWord> actionWords = restActionWordService.findAllActionWordsByProject(projectId, pageable);
        PagedModel<EntityModel<ActionWord>> res = toPagedModel(actionWords);
        return ResponseEntity.ok(res);
    }

    private void addAllLinksForActionWord(EntityModel<ActionWord> resource) {
        ActionWord actionWord = resource.getContent();
        if (actionWord != null) {
            resource.add(linkService.createLinkTo(actionWord.getProject()));
        }
    }
}
