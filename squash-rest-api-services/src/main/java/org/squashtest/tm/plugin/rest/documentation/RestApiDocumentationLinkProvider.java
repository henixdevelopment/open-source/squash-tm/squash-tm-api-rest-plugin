/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.documentation;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import org.squashtest.tm.service.documentation.DocumentationLinkProvider;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;

@Component
public class RestApiDocumentationLinkProvider implements DocumentationLinkProvider {

    private static final String DOC_LABEL_KEY = "label.rest-api";
    private static final String DOC_URL = "/api/rest/latest/docs/api-documentation.html";

    @Inject
    private MessageSource messageSource;

    @Override
    public List<DocumentationLinkProvider.Link> getDocumentationLinks() {
        return Collections.singletonList(new DocumentationLinkProvider.Link(
                messageSource.getMessage(DOC_LABEL_KEY, null, LocaleContextHolder.getLocale()),
                DOC_URL));
    }
}
