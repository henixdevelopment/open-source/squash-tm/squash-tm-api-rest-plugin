/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.domain.requirement.RequirementVersion;
import org.squashtest.tm.plugin.rest.core.jackson.RestDtoName;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;

import java.net.URL;
import java.util.Set;

@JsonAutoDetect(fieldVisibility= JsonAutoDetect.Visibility.NONE, getterVisibility= JsonAutoDetect.Visibility.NONE, isGetterVisibility= JsonAutoDetect.Visibility.NONE)
@RestDtoName(value = "issue")
public class IssueDto {

    @JsonProperty("remoteIssueId")
    private String remoteIssueId;
    @JsonProperty("url")
    private URL url;

    @JsonProperty("executions")
    @JsonSerialize(contentConverter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Set<Execution> executions;

    @JsonProperty("requirement-versions")
    @JsonSerialize(contentConverter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Set<RequirementVersion> requirements;

    @JsonProperty("session-notes")
    @JsonSerialize(contentConverter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Set<SessionNote> sessionNotes;

    @JsonProperty("exploratory-executions")
    @JsonSerialize(contentConverter = HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Set<ExploratoryExecution> exploratoryExecutions;

    public IssueDto() {
    }

    public IssueDto(String remoteIssueId, URL url, Set<Execution> executions) {
        this.remoteIssueId = remoteIssueId;
        this.url = url;
        this.executions = executions;
    }

    public IssueDto(String remoteIssueId, URL url, Set<Execution> executions, Set<RequirementVersion> requirements) {
        this.remoteIssueId = remoteIssueId;
        this.url = url;
        this.executions = executions;
        this.requirements = requirements;
    }

    public IssueDto(String remoteIssueId, URL url, Set<Execution> executions, Set<RequirementVersion> requirements, Set<SessionNote> sessionNotes) {
        this.remoteIssueId = remoteIssueId;
        this.url = url;
        this.executions = executions;
        this.requirements = requirements;
        this.sessionNotes = sessionNotes;
    }


    public IssueDto(String remoteIssueId, URL url, Set<Execution> executions, Set<RequirementVersion> requirements, Set<SessionNote> sessionNotes, Set<ExploratoryExecution> exploratoryExecutions) {
        this.remoteIssueId = remoteIssueId;
        this.url = url;
        this.executions = executions;
        this.requirements = requirements;
        this.sessionNotes = sessionNotes;
        this.exploratoryExecutions = exploratoryExecutions;
    }

    public Set<Execution> getExecutions() {
        return executions;
    }

    public void setExecutions(Set<Execution> executions) {
        this.executions = executions;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getRemoteIssueId() {
        return remoteIssueId;
    }

    public void setRemoteIssueId(String remoteIssueId) {
        this.remoteIssueId = remoteIssueId;
    }

    public Set<RequirementVersion> getRequirements() {
        return requirements;
    }

    public void setRequirements(Set<RequirementVersion> requirements) {
        this.requirements = requirements;
    }

    public Set<SessionNote> getSessionNotes() {
        return sessionNotes;
    }

    public void setSessionNotes(Set<SessionNote> sessionNotes) {
        this.sessionNotes = sessionNotes;
    }

    public Set<ExploratoryExecution> getExploratoryExecutions() {
        return exploratoryExecutions;
    }

    public void setExploratoryExecutions(Set<ExploratoryExecution> exploratoryExecutions) {
        this.exploratoryExecutions = exploratoryExecutions;
    }
}
