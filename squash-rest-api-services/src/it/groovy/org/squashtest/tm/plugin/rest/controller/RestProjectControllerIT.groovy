/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.campaign.Campaign
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode
import org.squashtest.tm.domain.customreport.CustomReportTreeDefinition
import org.squashtest.tm.domain.project.GenericProject
import org.squashtest.tm.domain.requirement.Requirement
import org.squashtest.tm.domain.requirement.RequirementLibraryNode
import org.squashtest.tm.domain.testcase.TestCase
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode
import org.squashtest.tm.domain.users.Party
import org.squashtest.tm.domain.users.UsersGroup
import org.squashtest.tm.plugin.docutils.DocumentationSnippets
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyClearance
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyPermission
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyProfileDto
import org.squashtest.tm.plugin.rest.service.RestPartyService
import org.squashtest.tm.plugin.rest.service.RestProjectService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.*
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

@WebMvcTest(RestProjectController)
class RestProjectControllerIT extends BaseControllerSpec {

    @Inject
    private RestProjectService service

    @Inject
    private RestPartyService partyService;

    // ************** browsing *******************

    def "get-project"() {
        given:
            def project = SquashEntityBuilder.project {
                id = 367L
                description = "<p>This project is the main sample project</p>"
                label = "Main Sample Project"
            }
        and:
            service.getOne(367) >> project
        when:
            def res = mockMvc.perform(
                    RestDocumentationRequestBuilders.get("/api/rest/latest/projects/{id}", 367)
                            .header("Accept", "application/json"))
        then:
            /* Test (using TestHelper) */
            res.andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
            withResult(res) {
                "_type".is "project"
                "id".is 367
                "description".is "<p>This project is the main sample project</p>"
                "label".is "Main Sample Project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/367"
                "_links".test {
                    "requirements.href".is "http://localhost:8080/api/rest/latest/projects/367/requirements-library/content"
                    "test-cases.href".is "http://localhost:8080/api/rest/latest/projects/367/test-cases-library/content"
                    "campaigns.href".is "http://localhost:8080/api/rest/latest/projects/367/campaigns-library/content"
                    "clearances.href".is "http://localhost:8080/api/rest/latest/projects/367/clearances"
                }
            }
            /* Documentation */
            res.andDo(doc.document(
                    documentationBuilder {
                        pathParams {
                            add "id : the id of the project"
                        }
                        requestParams {
                            add DescriptorLists.fieldsParams
                        }
                        fields {
                            add "id (number) : the id of the project"
                            add "_type (string) : the type of the entity"
                            add "name (string) : the name of the project"
                            add "label (string) : the label of the project"
                            add "description (string) : the description of the project"
                            add "attachments (array) : the attachments of the project"
                            add DescriptorLists.linksFields
                        }
                        _links {
                            add "self : link to this project"
                            add "requirements : link to the content of the requirement library of this project"
                            add "test-cases : link to the content of the test case library of this project"
                            add "campaigns : link to the content of the execution library of this project"
                            add "clearances : link to the clearance list of this project"
                            add "attachments : link to the attachments of this project"
                        }
                    }
            ))
    }

    def "get-project-by-name"() {
        given:
            def project = SquashEntityBuilder.project {
                id = 367L
                description = "<p>This project is the main sample project</p>"
                label = "Main Sample Project"
            }
        and:
            service.getOneByName("sample project") >> project
        when:
            def res = mockMvc.perform(
                    RestDocumentationRequestBuilders.get("/api/rest/latest/projects")
                    .param("projectName", "sample project")
                    .header("Accept", "application/json"))
        then:
            /* Test (using TestHelper) */
            res.andExpect(status().isOk())
                    .andExpect(content().contentType("application/json"))
            withResult(res) {
                "_type".is "project"
                "id".is 367
                "description".is "<p>This project is the main sample project</p>"
                "label".is "Main Sample Project"
                selfRelIs "http://localhost:8080/api/rest/latest/projects/367"
                "_links".test {
                    "requirements.href".is "http://localhost:8080/api/rest/latest/projects/367/requirements-library/content"
                    "test-cases.href".is "http://localhost:8080/api/rest/latest/projects/367/test-cases-library/content"
                    "campaigns.href".is "http://localhost:8080/api/rest/latest/projects/367/campaigns-library/content"
                    "clearances.href".is "http://localhost:8080/api/rest/latest/projects/367/clearances"
                }
            }
            /* Documentation */
            res.andDo(doc.document(
                    documentationBuilder {
                        requestParams {
                            add "projectName : the name of the project"
                            add DescriptorLists.fieldsParams
                        }
                        fields {
                            add "id (number) : the id of the project"
                            add "_type (string) : the type of the entity"
                            add "name (string) : the name of the project"
                            add "label (string) : the label of the project"
                            add "description (string) : the description of the project"
                            add "attachments (array) : the attachments of the project"
                            add DescriptorLists.linksFields
                        }
                        _links {
                            add "self : link to this project"
                            add "requirements : link to the content of the requirement library of this project"
                            add "test-cases : link to the content of the test case library of this project"
                            add "campaigns : link to the content of the execution library of this project"
                            add "clearances : link to the clearance list of this project"
                            add "attachments : link to the attachments of this project"
                        }
                    }
            ))
    }

    def "browse-project"() {
        given:
        def project1 = SquashEntityBuilder.project {
            id = 367L
            name = "sample project 1"
            description = "<p>This project is the main sample project 1</p>"
            label = "Main Sample Project 1"
        }

        def project2 = SquashEntityBuilder.project {
            id = 456L
            name = "sample project 2"
            description = "<p>This project is the main sample project 2</p>"
            label = "Main Sample Project 2"
        }

        def project3 = SquashEntityBuilder.project {
            id = 789L
            name = "sample project 3"
            description = "<p>This project is the main sample project 3</p>"
            label = "Main Sample Project 3"
        }

        def project4 = SquashEntityBuilder.projectTemplate {
            id = 971L
            name = "project template 4"
            description = "<p>This project the project template 4</p>"
            label = "Project Template 4"
        }


        and:
        service.findAllReadable(_) >> { args -> new PageImpl<GenericProject>([project1, project2, project3, project4])}

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects?page=0&size=4")
                .header("Accept", "application/json"))
        then:

        /*
        * Test (using TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.projects".hasSize 3
            "_embedded.projects".test {
                "[0]".test {
                    "_type".is "project"
                    "id".is 367
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/367"
                }
                "[1]".test {
                    "_type".is "project"
                    "id".is 456
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/456"
                }
                "[2]".test {
                    "_type".is "project"
                    "id".is 789
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/789"
                }
            }

            "_embedded.project-templates".hasSize 1
            "_embedded.project-templates".test {
                "[0]".test {
                    "_type".is "project-template"
                    "id".is 971
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/971"
                }
            }

            selfRelIs "http://localhost:8080/api/rest/latest/projects?page=0&size=4"
            "page".test {
                "size".is 4
                "totalElements".is 4
                "totalPages".is 1
                "number".is 0
            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                /*DocumentationSnippets.AllInOne.createBrowseAllEntities("projects")*/
                documentationBuilder {
                    fields {
                        embeddedAndStop "projects (array) : all the standard projects"
                        embeddedAndStop "project-templates (array) : all the project templates"
                        add DescriptorLists.paginationFields
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add DescriptorLists.paginationLinks
                    }
                }
        ))
    }

    def "browse-standard-project"() {
        given:
        def project1 = SquashEntityBuilder.project {
            id = 367L
            name = "standard project 1"
            description = "<p>This the standard project: project 3</p>"
            label = "Main Sample Project 1"
        }

        def project2 = SquashEntityBuilder.project {
            id = 456L
            name = "standard project 2"
            description = "<p>This the standard project: project 2</p>"
            label = "Main Sample Project 2"
        }

        def project3 = SquashEntityBuilder.project {
            id = 789L
            name = "standard project 3"
            description = "<p>This the standard project: project 3</p>"
            label = "Main Sample Project 3"
        }

        and:
        service.findAllReadableStandardProject(_) >> { args -> new PageImpl<GenericProject>([project1, project2, project3], args[0], 3) }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects?type=STANDARD")
                .param("type","STANDARD").header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.projects".hasSize 3
            "_embedded.projects".test {
                "[0]".test {
                    "_type".is "project"
                    "id".is 367
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/367"
                }
                "[1]".test {
                    "_type".is "project"
                    "id".is 456
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/456"
                }
                "[2]".test {
                    "_type".is "project"
                    "id".is 789
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/789"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/projects?type=STANDARD&page=0&size=20"
            "page".test {
                "size".is 20
                "totalElements".is 3
                "totalPages".is 1
                "number".is 0
            }
        }
        res.andDo(doc.document(
                AllInOne.createBrowseAllEntities("projects")
        ))
    }

    def "browse-project-template"() {
        given:
        def project1 = SquashEntityBuilder.projectTemplate {
            id = 367L
            name = "sample project 1"
            description = "<p>This project is the main sample project 1</p>"
            label = "Main Sample Project 1"
        }

        def project2 = SquashEntityBuilder.projectTemplate {
            id = 456L
            name = "sample project 2"
            description = "<p>This project is the main sample project 2</p>"
            label = "Main Sample Project 2"
        }

        def project3 = SquashEntityBuilder.projectTemplate {
            id = 789L
            name = "sample project 3"
            description = "<p>This project is the main sample project 3</p>"
            label = "Main Sample Project 3"
        }

        and:
        service.findAllReadableProjectTemplate(_) >> { args -> new PageImpl<GenericProject>([project1, project2, project3], args[0], 3) }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects?type=TEMPLATE")
        .param("type","TEMPLATE").header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.project-templates".hasSize 3
            "_embedded.project-templates".test {
                "[0]".test {
                    "_type".is "project-template"
                    "id".is 367
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/367"
                }
                "[1]".test {
                    "_type".is "project-template"
                    "id".is 456
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/456"
                }
                "[2]".test {
                    "_type".is "project-template"
                    "id".is 789
                    selfRelIs "http://localhost:8080/api/rest/latest/projects/789"
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/projects?type=TEMPLATE&page=0&size=20"
            "page".test {
                "size".is 20
                "totalElements".is 3
                "totalPages".is 1
                "number".is 0
            }
        }
        res.andDo(doc.document(
                DocumentationSnippets.AllInOne.createBrowseAllEntities("project-templates")

        ))
    }

    def "browse-test-case-library-content"() {


        given:
        service.findTestCaseLibraryAllContent(_, _) >> { args ->

            def folder = SquashEntityBuilder.testCaseFolder {
                id = 255L
                name = "root-level folder"
            }

            def tcroot = SquashEntityBuilder.testCase {
                id = 122L
                name = "root-level test case"
                reference = "TC-R"
            }

            def tcnested1 = SquashEntityBuilder.scriptedTestCase {
                id = 147L
                name = "content of root-level folder"
                reference = "TC-N"
            }

            def tcnested2 = SquashEntityBuilder.keywordTestCase {
                id = 148L
                name = "content of root-level folder"
                reference = "TC-N"
            }
            new PageImpl<TestCaseLibraryNode>([tcroot, folder, tcnested1, tcnested2], args[1], 10)

        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects/{id}/test-cases-library/content?page=2&size=3&sort=name,desc&fields=name,reference&include=nested", 14)
                .header("Accept", "application/json"))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.test-case-library-content".hasSize 4
            "_embedded.test-case-library-content[0]".test {
                "_type".is "test-case"
                "id".is 122
                "name".is "root-level test case"
                "reference".is "TC-R"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/122"
            }
            "_embedded.test-case-library-content[1]".test {
                "_type".is "test-case-folder"
                "id".is 255
                "name".is "root-level folder"
                selfRelIs "http://localhost:8080/api/rest/latest/test-case-folders/255"
            }
            "_embedded.test-case-library-content[2]".test {
                "_type".is "scripted-test-case"
                "id".is 147
                "name".is "content of root-level folder"
                "reference".is "TC-N"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/147"
            }
            "_embedded.test-case-library-content[3]".test {
                "_type".is "keyword-test-case"
                "id".is 148
                "name".is "content of root-level folder"
                "reference".is "TC-N"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/148"
            }

            "_links".test {
                "first".linksTo "http://localhost:8080/api/rest/latest/projects/14/test-cases-library/content?fields=name,reference&include=nested&page=0&size=3&sort=name,desc"
                "prev".linksTo "http://localhost:8080/api/rest/latest/projects/14/test-cases-library/content?fields=name,reference&include=nested&page=1&size=3&sort=name,desc"
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/14/test-cases-library/content?fields=name,reference&include=nested&page=2&size=3&sort=name,desc"
                "next".linksTo "http://localhost:8080/api/rest/latest/projects/14/test-cases-library/content?fields=name,reference&include=nested&page=3&size=3&sort=name,desc"
                "last".linksTo "http://localhost:8080/api/rest/latest/projects/14/test-cases-library/content?fields=name,reference&include=nested&page=3&size=3&sort=name,desc"
            }

            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 2
            }

        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                AllInOne.createListEntityContent("test-case-library-content", "project", true)
        ))
    }

    def "browse-reporting-library-content"() {


        given:
        service.findCustomReportLibraryAllContent(_, _) >> { args ->

            def library = SquashEntityBuilder.customReportLibrary {
                id = 3l
                name = "Folder"
                entityType = CustomReportTreeDefinition.FOLDER

            }

            def report = SquashEntityBuilder.customReportLibrary {
                id = 44l
                name = "Report"
                entityType = CustomReportTreeDefinition.REPORT
            }

            def chart = SquashEntityBuilder.customReportLibrary {
                id = 42l
                name = "Chart"
                entityType = CustomReportTreeDefinition.CHART
            }

            new PageImpl<CustomReportLibraryNode>([library, report, chart], args[1], 10)

        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects/{id}/reporting-library/content?page=2&size=3&include=nested", 14)
            .header("Accept", "application/json"))

        then:

        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.reporting-library-content".hasSize 3

            "_embedded.reporting-library-content[0]".test {
                "entity_type".is "FOLDER"
                "id".is 3
                "name".is "Folder"
            }
            "_embedded.reporting-library-content[1]".test {
                "entity_type".is "REPORT"
                "id".is 44
                "name".is "Report"
            }
            "_embedded.reporting-library-content[2]".test {
                "entity_type".is "CHART"
                "id".is 42
                "name".is "Chart"
            }

            "_links".test {
                "first".linksTo "http://localhost:8080/api/rest/latest/projects/14/reporting-library/content?include=nested&page=0&size=3"
                "prev".linksTo "http://localhost:8080/api/rest/latest/projects/14/reporting-library/content?include=nested&page=1&size=3"
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/14/reporting-library/content?include=nested&page=2&size=3"
                "next".linksTo "http://localhost:8080/api/rest/latest/projects/14/reporting-library/content?include=nested&page=3&size=3"
                "last".linksTo "http://localhost:8080/api/rest/latest/projects/14/reporting-library/content?include=nested&page=3&size=3"
            }

            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 2
            }

        }

        res.andDo(doc.document(
            AllInOne.createListEntityContent("reporting-library-content", "project", true)
        ))
    }

    def "browse-execution-library-content"() {


        given:
        service.findCampaignLibraryAllContent(_, _) >> { args ->

            def folder = SquashEntityBuilder.campaignFolder {
                id = 255L
                name = "root-level folder"
            }

            def root = SquashEntityBuilder.campaign {
                id = 122L
                name = "root-level campaign"
                reference = "C-R"
            }

            def nested = SquashEntityBuilder.campaign {
                id = 147L
                name = "content of root-level folder"
                reference = "C-N"
            }

            new PageImpl<TestCaseLibraryNode>([root, folder, nested], args[1], 10)

        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects/{id}/campaigns-library/content?page=2&size=3&sort=name,desc&fields=name,reference&include=nested", 14)
                .header("Accept", "application/json"))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.campaign-library-content".hasSize 3
            "_embedded.campaign-library-content[0]".test {
                "_type".is "campaign"
                "id".is 122
                "name".is "root-level campaign"
                "reference".is "C-R"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/122"
            }
            "_embedded.campaign-library-content[1]".test {
                "_type".is "campaign-folder"
                "id".is 255
                "name".is "root-level folder"
                selfRelIs "http://localhost:8080/api/rest/latest/campaign-folders/255"
            }
            "_embedded.campaign-library-content[2]".test {
                "_type".is "campaign"
                "id".is 147
                "name".is "content of root-level folder"
                "reference".is "C-N"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/147"
            }

            "_links".test {
                "first".linksTo "http://localhost:8080/api/rest/latest/projects/14/campaigns-library/content?fields=name,reference&include=nested&page=0&size=3&sort=name,desc"
                "prev".linksTo "http://localhost:8080/api/rest/latest/projects/14/campaigns-library/content?fields=name,reference&include=nested&page=1&size=3&sort=name,desc"
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/14/campaigns-library/content?fields=name,reference&include=nested&page=2&size=3&sort=name,desc"
                "next".linksTo "http://localhost:8080/api/rest/latest/projects/14/campaigns-library/content?fields=name,reference&include=nested&page=3&size=3&sort=name,desc"
                "last".linksTo "http://localhost:8080/api/rest/latest/projects/14/campaigns-library/content?fields=name,reference&include=nested&page=3&size=3&sort=name,desc"
            }

            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 2
            }

        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                AllInOne.createListEntityContent("campaign-library-content", "project", true)
        ))
    }

    def "browse-requirement-library-content"() {

        given:

        service.findRequirementLibraryAllContent(_, _) >> { args ->

            def reqroot = SquashEntityBuilder.requirement {
                id = 122L
                resource = SquashEntityBuilder.requirementVersion {
                    name = "root-level requirement"
                }
            }

            def folder = SquashEntityBuilder.requirementFolder {
                id = 255L
                resource = SquashEntityBuilder.resource {
                    name = "root-level folder"
                }
            }

            def reqnested = SquashEntityBuilder.requirement {
                id = 147L
                resource = SquashEntityBuilder.requirementVersion {
                    name = "content of root-level folder"
                }
            }

            new PageImpl<RequirementLibraryNode>([folder, reqnested, reqroot], args[1], 10)

        }

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects/{id}/requirements-library/content?page=2&size=3&include=nested&sort=id,desc", 14)
                .header("Accept", "application/json"))

        then:

        /*
		* Test
	    */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.requirement-library-content".hasSize 3
            "_embedded.requirement-library-content[0]".test {
                "_type".is "requirement-folder"
                "id".is 255
                "name".is "root-level folder"
                selfRelIs "http://localhost:8080/api/rest/latest/requirement-folders/255"
            }
            "_embedded.requirement-library-content[1]".test {
                "_type".is "requirement"
                "id".is 147
                "name".is "content of root-level folder"
                selfRelIs "http://localhost:8080/api/rest/latest/requirements/147"
            }
            "_embedded.requirement-library-content[2]".test {
                "_type".is "requirement"
                "id".is 122
                "name".is "root-level requirement"
                selfRelIs "http://localhost:8080/api/rest/latest/requirements/122"
            }

            "_links".test {
                "first".linksTo "http://localhost:8080/api/rest/latest/projects/14/requirements-library/content?include=nested&page=0&size=3&sort=id,desc"
                "prev".linksTo "http://localhost:8080/api/rest/latest/projects/14/requirements-library/content?include=nested&page=1&size=3&sort=id,desc"
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/14/requirements-library/content?include=nested&page=2&size=3&sort=id,desc"
                "next".linksTo "http://localhost:8080/api/rest/latest/projects/14/requirements-library/content?include=nested&page=3&size=3&sort=id,desc"
                "last".linksTo "http://localhost:8080/api/rest/latest/projects/14/requirements-library/content?include=nested&page=3&size=3&sort=id,desc"
            }

            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 2
            }

        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                AllInOne.createListEntityContent("requirement-library-content", "project", true)
        ))
    }

    def "post-project"() {
        given:
        def json = """{
            "_type": "project",
            "name": "sample project",
            "label": "no price tag",
            "description": "<p>do something meaningful</p>"
        }"""

        def proj = SquashEntityBuilder.project {
            id = 333L
            name = "sample project"
            label = "no price tag"
            description = "<p>do something meaningful</p>"
        }

        and:
        service.createGenericProject(_) >> proj

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/projects")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "project"
            "id".is 333
            "name".is "sample project"
            "label".is "no price tag"
            "description".is "<p>do something meaningful</p>"
            selfRelIs "http://localhost:8080/api/rest/latest/projects/333"
            "_links".test {
                "requirements.href".is "http://localhost:8080/api/rest/latest/projects/333/requirements-library/content"
                "test-cases.href".is "http://localhost:8080/api/rest/latest/projects/333/test-cases-library/content"
                "campaigns.href".is "http://localhost:8080/api/rest/latest/projects/333/campaigns-library/content"
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    requestFields {
                        add "_type (string) : the type of the entity (mandatory)"
                        add "name (string) : the name of the project"
                        add "label (string) : the label of the project"
                        add "description (string) : the description of the project"
                    }
                    fields {
                        relaxed = true
                        add "id (number) : the id of the project"
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this project"
                        add "requirements : link to the content of the requirement library of this project"
                        add "test-cases : link to the content of the test case library of this project"
                        add "campaigns : link to the content of the execution library of this project"
                    }
                }
        ))
    }

    def "post-project-from-template"() {
        given:
        def json = """{
            "_type": "project",
            "name": "sample project",
            "label": "no price tag",
            "description": "<p>do something meaningful</p>",
            "template_id": 23,
            "params" : {
                "copy_permissions": true,
                "copy_cuf": true,
                "copy_bugtracker_binding": true,
                "copy_ai_server_binding": true,
                "copy_automated_projects": true,
                "copy_infolists": true,
                "copy_milestone": true,
                "copy_allow_tc_modif_from_exec": true,
                "copy_optional_exec_statuses": true,
                "copy_plugins_activation": true,
                "keep_template_binding": true,
                "copy_plugins_configuration": true,
                "keep_plugins_binding": true
            }
        }"""

        def proj = SquashEntityBuilder.project {
            id = 333L
            name = "sample project"
            label = "no price tag"
            description = "<p>do something meaningful</p>"
        }

        and:
        service.createGenericProject(_) >> proj

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/projects")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        /*
         * Test the response
         */

        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "project"
            "id".is 333
            "name".is "sample project"
            "label".is "no price tag"
            "description".is "<p>do something meaningful</p>"
            selfRelIs "http://localhost:8080/api/rest/latest/projects/333"
            "_links".test {
                "requirements.href".is "http://localhost:8080/api/rest/latest/projects/333/requirements-library/content"
                "test-cases.href".is "http://localhost:8080/api/rest/latest/projects/333/test-cases-library/content"
                "campaigns.href".is "http://localhost:8080/api/rest/latest/projects/333/campaigns-library/content"
            }

        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    requestFields {
                        add "_type (string) : the type of the entity (mandatory)"
                        add "name (string) : the name of the project"
                        add "label (string) : the label of the project"
                        add "description (string) : the description of the project"
                        add "template_id (number) : the id of project template"
                        add "params (Object) : the parameters to create a new project from template"
                        add "params.copy_permissions (boolean) : whether the template's permissions will be copied or not"
                        add "params.copy_cuf (boolean) : whether the template's custom fields will be copied or not"
                        add "params.copy_bugtracker_binding (boolean) : whether the template's bugtracker will be copied or not"
                        add "params.copy_ai_server_binding (boolean) : whether the project's ai server will be linked to the template's"
                        add "params.copy_automated_projects (boolean) : whether the template's test automation management will be copied or not"
                        add "params.copy_infolists (boolean) : whether the template's information lists will be copied or not"
                        add "params.copy_milestone (boolean) : whether the template's milestones will be copied or not"
                        add "params.copy_allow_tc_modif_from_exec (boolean) : whether the template's execution option will be copied or not"
                        add "params.copy_optional_exec_statuses (boolean) : whether the template's optional execution statuses will be copied or not"
                        add "params.copy_plugins_activation (boolean) : whether the template's plugins activation will be copied or not"
                        add "params.keep_template_binding (boolean) : whether the template binding will be kept or not (true by default)"
                        add "params.copy_plugins_configuration (boolean) : whether the template's plugins configuration will be copied or not"
                        add "params.keep_plugins_binding (boolean) : whether the project's plugins configuration will be tied to the template's plugin configuration"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the project"
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this project"
                        add "requirements : link to the content of the requirement library of this project"
                        add "test-cases : link to the content of the test case library of this project"
                        add "campaigns : link to the content of the execution library of this project"
                    }
                }
        ))
    }

    def "post-project-template"() {
        given:
        def json = """{
            "_type": "project-template",
            "name": "sample project template",
            "label": "no price tag",
            "description": "<p>do something meaningful</p>"
        }"""

        def projTemplate = SquashEntityBuilder.projectTemplate {
            id = 333L
            name = "sample project template"
            label = "no price tag"
            description = "<p>do something meaningful</p>"
        }

        and:
        service.createGenericProject(_) >> projTemplate

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/projects")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "project-template"
            "id".is 333
            "name".is "sample project template"
            "label".is "no price tag"
            "description".is "<p>do something meaningful</p>"
            selfRelIs "http://localhost:8080/api/rest/latest/projects/333"
            "_links".test {
                "requirements.href".is "http://localhost:8080/api/rest/latest/projects/333/requirements-library/content"
                "test-cases.href".is "http://localhost:8080/api/rest/latest/projects/333/test-cases-library/content"
                "campaigns.href".is "http://localhost:8080/api/rest/latest/projects/333/campaigns-library/content"
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    requestFields {
                        add "_type (string) : the type of the entity (mandatory)"
                        add "name (string) : the name of the project template"
                        add "label (string) : the label of the project template"
                        add "description (string) : the description of the project template"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the project template"
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this project"
                        add "requirements : link to the content of the requirement library of this project template"
                        add "test-cases : link to the content of the test case library of this project template"
                        add "campaigns : link to the content of the execution library of this project template"
                    }
                }
        ))
    }

    def "post-project-template-from-project"() {
        given:
        def json = """{
            "_type": "project-template",
            "name": "sample project template",
            "label": "no price tag",
            "description": "<p>do something meaningful</p>",
            "project_id": 55,
            "params" : {
                "copy_permissions": true,
                "copy_cuf": true,
                "copy_bugtracker_binding": true,
                "copy_automated_projects": true,
                "copy_ai_server_binding" : true,
                "copy_optional_exec_statuses" : true,
                "copy_plugins_activation" : true,
                "copy_infolists": true,
                "copy_milestone": true,
                "copy_allow_tc_modif_from_exec": true,
                "copy_plugins_configuration": true
            }
        }"""

        def projectTemplate = SquashEntityBuilder.projectTemplate {
            id = 333L
            name = "sample project template"
            label = "no price tag"
            description = "<p>do something meaningful</p>"
        }

        and:
        service.createGenericProject(_) >> projectTemplate

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/projects")
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:

        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "project-template"
            "id".is 333
            "name".is "sample project template"
            "label".is "no price tag"
            "description".is "<p>do something meaningful</p>"
            selfRelIs "http://localhost:8080/api/rest/latest/projects/333"
            "_links".test {
                "requirements.href".is "http://localhost:8080/api/rest/latest/projects/333/requirements-library/content"
                "test-cases.href".is "http://localhost:8080/api/rest/latest/projects/333/test-cases-library/content"
                "campaigns.href".is "http://localhost:8080/api/rest/latest/projects/333/campaigns-library/content"
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    requestFields {
                        add "_type (string) : the type of the entity (mandatory)"
                        add "name (string) : the name of the project"
                        add "label (string) : the label of the project"
                        add "description (string) : the description of the project"
                        add "project_id (number) : the id of project template"
                        add "params (Object) : the parameters to create a new project from template"
                        add "params.copy_permissions (boolean) : whether the project's permissions will be copied or not"
                        add "params.copy_cuf (boolean) : whether the project's custom fields will be copied or not"
                        add "params.copy_bugtracker_binding (boolean) :  whether the project's bugtracker will be copied or not"
                        add "params.copy_automated_projects (boolean) : whether the project's test automation management will be copied or not"
                        add "params.copy_ai_server_binding (boolean) : whether the project's ai server will be linked to the template's"
                        add "params.copy_optional_exec_statuses (boolean) : whether the template's optional execution statuses will be copied or not"
                        add "params.copy_plugins_activation (boolean) : whether the template's plugins activation will be copied or not"
                        add "params.copy_infolists (boolean) : whether the project's information lists will be copied or not"
                        add "params.copy_milestone (boolean) : whether the project's milestones will be copied or not"
                        add "params.copy_allow_tc_modif_from_exec (boolean) : whether the project's execution option will be copied or not"
                        add "params.copy_plugins_configuration (boolean) : whether the project's plugins' configuration will be copied or not"
                    }

                    fields {
                        relaxed = true
                        add "id (number) : the id of the project template"
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add "self : link to this project"
                        add "requirements : link to the content of the requirement library of this project template"
                        add "test-cases : link to the content of the test case library of this project template"
                        add "campaigns : link to the content of the execution library of this project template"
                    }
                }
        ))
    }

    def "get-project-clearances"() {

        given:

        def team1 = SquashEntityBuilder.team {
            id = 567L
            name = "Team A"
            description = "<p>Black panther</p>"
        }

        def team2 = SquashEntityBuilder.team {
            id = 852L
            name = "Team B"
            description = "<p>Iron man</p>"
        }

        def team3 = SquashEntityBuilder.team {
            id = 853L
            name = "Team C"
            description = "<p>Legend</p>"
        }

        def user1 = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        def user2 = SquashEntityBuilder.user {
            id = 521L
            firstName = "David"
            lastName = "Bowie"
            login = "User-2"
            email = "davidbowie@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/27"
        }

        def partyClearances = new RestPartyClearance()
        def testDesigner = new RestPartyProfileDto(7L, "TestDesigner", true, [user1, user2] as List<Party>)
        def customTester = new RestPartyProfileDto(11L, "Custom Tester", false, [team3] as List<Party>)
        def testRunner = new RestPartyProfileDto(6L, "TestRunner", true, [team1] as List<Party>)
        def validator = new RestPartyProfileDto(9L, "Validator", true, [team2] as List<Party>)
        partyClearances.partyClearances.put("test_designer", testDesigner)
        partyClearances.partyClearances.put("custom_tester", customTester)
        partyClearances.partyClearances.put("test_runner", testRunner)
        partyClearances.partyClearances.put("validator", validator)

        and:

        service.findAllClearancesByProjectId(367) >> partyClearances

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects/{id}/clearances", 367).header("Accept", "application/json"))

        then:
        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "content.validator".test {
                "_type".is "profile"
                "id".is 9
                "name".is "Validator"
                "type".is "system"
                "users".hasSize 1
                "users".test {
                    "[0]".test {
                        "_type".is "team"
                        "id".is 852
                        "name".is "Team B"
                        selfRelIs "http://localhost:8080/api/rest/latest/teams/852"
                    }
                }
            }

            "content.custom_tester".test {
                "_type".is "profile"
                "id".is 11
                "name".is "Custom Tester"
                "type".is "custom"
                "users".hasSize 1
                "users".test {
                    "[0]".test {
                        "_type".is "team"
                        "id".is 853
                        "name".is "Team C"
                        selfRelIs "http://localhost:8080/api/rest/latest/teams/853"
                    }
                }
            }

            "content.test_designer".test {
                "_type".is "profile"
                "id".is 7
                "name".is "TestDesigner"
                "type".is "system"
                "users".hasSize 2
                "users".test {
                    "[0]".test {
                        "_type".is "user"
                        "id".is 486
                        "login".is "User-1"
                        selfRelIs "http://localhost:8080/api/rest/latest/users/486"
                    }
                    "[1]".test {
                        "_type".is "user"
                        "id".is 521
                        "login".is "User-2"
                        selfRelIs "http://localhost:8080/api/rest/latest/users/521"
                    }
                }
            }

            "content.test_runner".test {
                "_type".is "profile"
                "id".is 6
                "name".is "TestRunner"
                "type".is "system"
                "users".hasSize 1
                "users".test {
                    "[0]".test {
                        "_type".is "team"
                        "id".is 567
                        "name".is "Team A"
                        selfRelIs "http://localhost:8080/api/rest/latest/teams/567"
                    }
                }
            }

            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/367/clearances"
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the project"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        addAndStop "content (object): contains clearances grouped by profiles"
                        add "content.validator (object) : contains the profile attributes (example of the validator profile)"
                        add "content.validator._type (string) : the type of the entity"
                        add "content.validator.id (number) : the id of the profile"
                        add "content.validator.name (string) : the name of the profile"
                        add "content.validator.type (string) : system if the profile is a system profile, custom if the profile is a custom profile"
                        addAndStop "content.validator.users (array) : the list of users/teams linked to the profile"
                        addAndStop "_links (object) : related links"
                    }
                    _links {
                        add "self : the link to this project clearances"
                    }
                }
        ))
    }

    def "get-project-permissions"() {

        given:

        def team1 = SquashEntityBuilder.team {
            id = 567L
            name = "Team A"
            description = "<p>Black panther</p>"
        }

        def team2 = SquashEntityBuilder.team {
            id = 852L
            name = "Team B"
            description = "<p>Iron man</p>"
        }

        def user1 = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        def user2 = SquashEntityBuilder.user {
            id = 521L
            firstName = "David"
            lastName = "Bowie"
            login = "User-2"
            email = "davidbowie@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/27"
        }

        def partyPermissions = new RestPartyPermission()
        partyPermissions.partyPermissions.put("test_designer", [user1, user2])
        partyPermissions.partyPermissions.put("test_runner", [team1])
        partyPermissions.partyPermissions.put("validator", [team2])


        and:

        service.findAllPermissionsByProjectId(367) >> partyPermissions

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects/{id}/permissions", 367).header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "content.test_runner".hasSize 1
            "content.test_runner".test {
                "[0]".test {
                    "_type".is "team"
                    "id".is 567
                    "name".is "Team A"
                    selfRelIs "http://localhost:8080/api/rest/latest/teams/567"

                }
            }

            "content.validator".hasSize 1
            "content.validator".test {
                "[0]".test {
                    "_type".is "team"
                    "id".is 852
                    "name".is "Team B"
                    selfRelIs "http://localhost:8080/api/rest/latest/teams/852"

                }
            }

            "content.test_designer".hasSize 2
            "content.test_designer".test {
                "[0]".test {
                    "_type".is "user"
                    "id".is 486
                    "login".is "User-1"
                    selfRelIs "http://localhost:8080/api/rest/latest/users/486"

                }
                "[1]".test {
                    "_type".is "user"
                    "id".is 521
                    "login".is "User-2"
                    selfRelIs "http://localhost:8080/api/rest/latest/users/521"

                }
            }

            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/367/permissions"
            }
        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the project"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    _links {
                        add "self : the link to this project clearances"
                    }
                }
        ))
    }

    def "add-new-clearance-to-project"() {
        given:

        def user1 = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        def user2 = SquashEntityBuilder.user {
            id = 521L
            firstName = "David"
            lastName = "Bowie"
            login = "User-2"
            email = "davidbowie@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/27"
        }

        def group1 = SquashEntityBuilder.permissionGroup {
            id = 215L
            qualifiedName = "permissionGroup.TestRunner"
        }

        def group2 = SquashEntityBuilder.permissionGroup {
            id = 216L
            qualifiedName = "permissionGroup.TestDesigner"
        }

        def group3 = SquashEntityBuilder.permissionGroup {
            id = 217L
            qualifiedName = "permissionGroup.TestManager"
        }

        def partyClearances = new RestPartyClearance()
        def testDesigner = new RestPartyProfileDto(7L, "TestDesigner", true, [user1, user2] as List<Party>)
        partyClearances.partyClearances.put("test_designer", testDesigner)

        and:
        partyService.getOne(_) >>> [user1, user2]
        service.findAllPossiblePermission() >> [group1, group2, group3]
        service.buildPartyClearanceDataModel(_, _) >> partyClearances

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/projects/{projectId}/clearances/{profileId}/users/{partyIds}", 367, 4, "486,521")
                .header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "content.test_designer".test {
                "_type".is "profile"
                "id".is 7
                "name".is "TestDesigner"
                "type".is "system"
                "users".hasSize 2
                "users".test {
                    "[0]".test {
                        "_type".is "user"
                        "id".is 486
                        "login".is "User-1"
                        selfRelIs "http://localhost:8080/api/rest/latest/users/486"
                    }
                    "[1]".test {
                        "_type".is "user"
                        "id".is 521
                        "login".is "User-2"
                        selfRelIs "http://localhost:8080/api/rest/latest/users/521"
                    }
                }
            }

            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/367/clearances"
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "projectId : the id of the project"
                        add "profileId : the id of the profile"
                        add "partyIds : the ids of the users/teams"
                    }
                    fields {
                        addAndStop "content (object): contains the added clearance"
                        add "content.test_designer (object) : contains the profile attributes (example of the test designer profile)"
                        add "content.test_designer._type (string) : the type of the entity"
                        add "content.test_designer.id (number) : the id of the profile"
                        add "content.test_designer.name (string) : the name of the profile"
                        add "content.test_designer.type (string) : system if the profile is a system profile, custom if the profile is a custom profile"
                        addAndStop "content.test_designer.users (array) : the list of users/teams linked to the profile"
                        addAndStop "_links (object) : related links"
                    }
                    _links {
                        add "self : the link to this project clearances"
                    }
                }
        ))
    }

    def "add-new-permission-to-project"() {
        given:

        def user1 = SquashEntityBuilder.user {
            id = 486L
            firstName = "Charles"
            lastName = "Dupond"
            login = "User-1"
            email = "charlesdupond@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/11"
        }

        def user2 = SquashEntityBuilder.user {
            id = 521L
            firstName = "David"
            lastName = "Bowie"
            login = "User-2"
            email = "davidbowie@aaaa.aa"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2018/02/27"
        }

        def group1 = SquashEntityBuilder.permissionGroup {
            id = 215L
            qualifiedName = "permissionGroup.TestRunner"
        }

        def group2 = SquashEntityBuilder.permissionGroup {
            id = 216L
            qualifiedName = "permissionGroup.TestDesigner"
        }

        def group3 = SquashEntityBuilder.permissionGroup {
            id = 217L
            qualifiedName = "permissionGroup.TestManager"
        }

        def partyPermissions = new RestPartyPermission()

        partyPermissions.partyPermissions.put("test_designer", [user1, user2] as List<Party>)


        and:
        partyService.getOne(_) >>> [user1, user2]
        service.findAllPossiblePermission() >> [group1, group2, group3]
        service.buildPartyPermissionDataModel(_, _) >> partyPermissions

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/projects/{projectId}/permissions/{permissionGroup}?ids=486,521", 367, "test_designer")
                .header("Accept", "application/json"))

        then:

        /*
        * Test
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "content.test_designer".hasSize 2
            "content.test_designer".test {
                "[0]".test {
                    "_type".is "user"
                    "id".is 486
                    "login".is "User-1"
                    selfRelIs "http://localhost:8080/api/rest/latest/users/486"

                }
                "[1]".test {
                    "_type".is "user"
                    "id".is 521
                    "login".is "User-2"
                    selfRelIs "http://localhost:8080/api/rest/latest/users/521"

                }
            }

            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/367/permissions"
            }
        }

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "projectId : the id of the project"
                    }
                    pathParams {
                        add "permissionGroup : the profile in which the users/teams will be added"
                    }
                    requestParams {
                        add "ids : the ids of the users/teams"
                    }
                    _links {
                        add "self : the link to this project clearances"
                    }
                }
        ))
    }

    def "get-project-requirements"() {


        given:
        service.findRequirementsByProject(_, _) >> { args ->


            def req1 = SquashEntityBuilder.requirement {
                id = 122L
                resource = SquashEntityBuilder.requirementVersion {
                    name = "requirement 1"
                }
            }

            def req2 = SquashEntityBuilder.requirement {
                id = 147L
                resource = SquashEntityBuilder.requirementVersion {
                    name = "requirement 2"
                }
            }

            def req3 = SquashEntityBuilder.requirement {
                id = 255L
                resource = SquashEntityBuilder.requirementVersion {
                    name = "requirement 3"
                }
            }

            new PageImpl<Requirement>([req1, req2, req3], args[1], 10)

        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects/{id}/requirements?page=2&size=3&sort=id,desc", 14)
                .header("Accept", "application/json"))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.requirements".hasSize 3
            "_embedded.requirements[0]".test {
                "_type".is "requirement"
                "id".is 122
                "name".is "requirement 1"
                selfRelIs "http://localhost:8080/api/rest/latest/requirements/122"
            }
            "_embedded.requirements[1]".test {
                "_type".is "requirement"
                "id".is 147
                "name".is "requirement 2"
                selfRelIs "http://localhost:8080/api/rest/latest/requirements/147"
            }
            "_embedded.requirements[2]".test {
                "_type".is "requirement"
                "id".is 255
                "name".is "requirement 3"
                selfRelIs "http://localhost:8080/api/rest/latest/requirements/255"
            }

            "_links".test {
                "first".linksTo "http://localhost:8080/api/rest/latest/projects/14/requirements?page=0&size=3&sort=id,desc"
                "prev".linksTo "http://localhost:8080/api/rest/latest/projects/14/requirements?page=1&size=3&sort=id,desc"
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/14/requirements?page=2&size=3&sort=id,desc"
                "next".linksTo "http://localhost:8080/api/rest/latest/projects/14/requirements?page=3&size=3&sort=id,desc"
                "last".linksTo "http://localhost:8080/api/rest/latest/projects/14/requirements?page=3&size=3&sort=id,desc"
            }

            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 2
            }

        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the project"
                    }
                    requestParams {
                        add DescriptorLists.paginationAndSortParams
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "requirements (array) : the requirements of this project"
                        add DescriptorLists.paginationFields
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add DescriptorLists.paginationLinks
                    }
                }
        ))
    }

    def "get-project-test-cases"() {

        given:
        service.findTestCasesByProject(_, _, _) >> { args ->


            def tc1 = SquashEntityBuilder.testCase {
                id = 122L
                name = "test case 1"
                reference = "TC-1"
            }


            def stc1 = SquashEntityBuilder.scriptedTestCase {
                id = 222L
                name = "scripted test case 1"
                reference = "STC-1"
            }



            def ktc1 = SquashEntityBuilder.keywordTestCase {
                id = 322L
                name = "keyword test case 1"
                reference = "KTC-1"
            }



            new PageImpl<TestCase>([tc1, stc1, ktc1], args[1], 10)

        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects/{id}/test-cases?page=2&size=3&sort=name,desc", 14)
                .header("Accept", "application/json"))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.test-cases".hasSize 3
            "_embedded.test-cases[0]".test {
                "_type".is "test-case"
                "id".is 122
                "name".is "test case 1"
                "reference".is "TC-1"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/122"
            }
            "_embedded.test-cases[1]".test {
                "_type".is "scripted-test-case"
                "id".is 222
                "name".is "scripted test case 1"
                "reference".is "STC-1"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/222"
            }

            "_embedded.test-cases[2]".test {
                "_type".is "keyword-test-case"
                "id".is 322
                "name".is "keyword test case 1"
                "reference".is "KTC-1"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/322"
            }
            "_links".test {
                "first".linksTo "http://localhost:8080/api/rest/latest/projects/14/test-cases?page=0&size=3&sort=name,desc"
                "prev".linksTo "http://localhost:8080/api/rest/latest/projects/14/test-cases?page=1&size=3&sort=name,desc"
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/14/test-cases?page=2&size=3&sort=name,desc"
                "next".linksTo "http://localhost:8080/api/rest/latest/projects/14/test-cases?page=3&size=3&sort=name,desc"
                "last".linksTo "http://localhost:8080/api/rest/latest/projects/14/test-cases?page=3&size=3&sort=name,desc"
            }

            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 2
            }

        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the project"
                    }
                    requestParams {
                        add DescriptorLists.paginationAndSortParams
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "test-cases (array) : the test cases of this project"
                        add DescriptorLists.paginationFields
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add DescriptorLists.paginationLinks
                    }
                }
        ))
    }

    def "get-project-campaigns"() {


        given:
        service.findCampaignsByProject(_, _) >> { args ->


            def c1 = SquashEntityBuilder.campaign {
                id = 255L
                name = "campaign 1"
                reference = "C-1"
            }

            def c2 = SquashEntityBuilder.campaign {
                id = 122L
                name = "campaign 2"
                reference = "C-2"
            }

            def c3 = SquashEntityBuilder.campaign {
                id = 147L
                name = "campaign 3"
                reference = "C-3"
            }

            new PageImpl<Campaign>([c1, c2, c3], args[1], 10)

        }

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/projects/{id}/campaigns?page=2&size=3&sort=name,desc", 14)
                .header("Accept", "application/json"))

        then:

        /*
         * Test
         */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.campaigns".hasSize 3
            "_embedded.campaigns[0]".test {
                "_type".is "campaign"
                "id".is 255
                "name".is "campaign 1"
                "reference".is "C-1"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/255"
            }

            "_embedded.campaigns[1]".test {
                "_type".is "campaign"
                "id".is 122
                "name".is "campaign 2"
                "reference".is "C-2"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/122"
            }

            "_embedded.campaigns[2]".test {
                "_type".is "campaign"
                "id".is 147
                "name".is "campaign 3"
                "reference".is "C-3"
                selfRelIs "http://localhost:8080/api/rest/latest/campaigns/147"
            }

            "_links".test {
                "first".linksTo "http://localhost:8080/api/rest/latest/projects/14/campaigns?page=0&size=3&sort=name,desc"
                "prev".linksTo "http://localhost:8080/api/rest/latest/projects/14/campaigns?page=1&size=3&sort=name,desc"
                "self".linksTo "http://localhost:8080/api/rest/latest/projects/14/campaigns?page=2&size=3&sort=name,desc"
                "next".linksTo "http://localhost:8080/api/rest/latest/projects/14/campaigns?page=3&size=3&sort=name,desc"
                "last".linksTo "http://localhost:8080/api/rest/latest/projects/14/campaigns?page=3&size=3&sort=name,desc"
            }

            "page".test {
                "size".is 3
                "totalElements".is 10
                "totalPages".is 4
                "number".is 2
            }

        }

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the project"
                    }
                    requestParams {
                        add DescriptorLists.paginationAndSortParams
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        embeddedAndStop "campaigns (array) : the test cases of this project"
                        add DescriptorLists.paginationFields
                        add DescriptorLists.linksFields
                    }
                    _links {
                        add DescriptorLists.paginationLinks
                    }
                }
        ))
    }

    def "delete-project-party"() {
        given:

        def party1 = SquashEntityBuilder.user {
            id = 77L
            firstName = "Harry"
            lastName = "Potter"
            login = "harry"
            email = "harrypotter@hogwart.wiz"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2019/11/07"
        }

        def party2 = SquashEntityBuilder.user {
            id = 99L
            firstName = "Tom"
            lastName = "Riddle"
            login = "voldemort"
            email = "tomriddle@hogwart.wiz"
            active = true
            group UsersGroup.USER
            lastConnectedOn "2010/02/10"
        }

        partyService.findById(77L) >> party1
        partyService.findById(99L) >> party2

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/projects/{projectId}/users/{partyIds}", 44L, "77,99")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         *Test
         */
        res.andExpect(status().isNoContent())
        2 * service.deletePartyFromProject(_, _)

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "projectId : the id of the project"
                        add "partyIds : the list of user/team ids to be deleted"
                    }
                }
        ))
    }
}
