/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import org.squashtest.tm.domain.customfield.CustomField
import org.squashtest.tm.domain.customfield.CustomFieldOption
import org.squashtest.tm.domain.customfield.InputType
import org.squashtest.tm.domain.customfield.MultiSelectField
import org.squashtest.tm.domain.customfield.NumericField
import org.squashtest.tm.domain.customfield.RichTextField
import org.squashtest.tm.domain.customfield.SingleSelectField

import static org.squashtest.tm.domain.customfield.InputType.DROPDOWN_LIST
import static org.squashtest.tm.domain.customfield.InputType.NUMERIC
import static org.squashtest.tm.domain.customfield.InputType.RICH_TEXT
import static org.squashtest.tm.domain.customfield.InputType.TAG

/**
 * That class is a duplicate of org.squashtest.tm.web.internal.controller.customfield.NewCustomField, 
 * because it contains interesting logic for creation of custom fields, but we don't want a hard 
 * dependency on module tm.web just because of that.
 * 
 * @author bsiri
 *
 */
class NewCustomField {
	private String label = ""
	private String code = ""
	private String name = ""
	private boolean optional = true
	private String defaultValue = ""
	private InputType inputType = InputType.PLAIN_TEXT;
	private String[][] options;

	public NewCustomField() {
		inputType = InputType.PLAIN_TEXT;
	}

	public CustomField createTransientEntity() {
		CustomField res
		switch (inputType) {
		case DROPDOWN_LIST:
			res = createSingleSelectField()
			break;
		case RICH_TEXT :
			res = createRichTextField()
			break;
		case TAG :
			res = createTag()
			break;
		case NUMERIC:
			res = createNumeric()
			break;
		default:
			res = new CustomField(inputType)
		}
		
		res.code = code
		res.label = label
		res.name = name
		res.optional = optional
		res.defaultValue = defaultValue

		return res
	}

	private CustomField createNumeric() {
		new NumericField()
	}

	private CustomField createSingleSelectField() {
		CustomField res
		SingleSelectField ssf = new SingleSelectField()

		for(String[] option : options) {
			String label = option[0]
			String code = option[1]
			ssf.addOption(new CustomFieldOption(label, code) )
		}

		return ssf
	}

	private CustomField createRichTextField(){
		new RichTextField()
	}

	private CustomField createTag(){
		new MultiSelectField()
	}


}
