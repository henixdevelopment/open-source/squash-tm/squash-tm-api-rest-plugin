/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.users.ApiTokenPermission;
import org.squashtest.tm.plugin.rest.jackson.model.ApiTokenRestDto;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;


@Service
public class ApiTokenPostValidator implements Validator {

    private static final String EXPIRY_DATE = "expiryDate";
    private static final String INVALID_VALUE = "Invalid value";
    private static final String NAME = "name";
    private static final String NO_EMPTY_ATTRIBUTE = "This attribute can't be empty";
    private static final String PERMISSIONS = "permissions";
    private static final String REQUIRED = "Required";

    @Override
    public boolean supports(Class<?> clazz) {
        return ApiTokenRestDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ApiTokenRestDto apiTokenRestDto = (ApiTokenRestDto) target;
        boolean permissionsExist = Arrays.asList(ApiTokenPermission.READ.toString(), ApiTokenPermission.READ_WRITE.toString())
            .contains(apiTokenRestDto.getPermissions());

        if (!permissionsExist && apiTokenRestDto.getPermissions() != null) {
            errors.rejectValue(PERMISSIONS, INVALID_VALUE, "Permissions must be either 'READ' or 'READ_WRITE'.");
        }

        if (isDateBeforeTomorrow(apiTokenRestDto.getExpiryDate())) {
            errors.rejectValue(EXPIRY_DATE, INVALID_VALUE, "Expiry date cannot be set before tomorrow.");
        }

        if (isDateAfterOneYearFromToday(apiTokenRestDto.getExpiryDate())) {
            errors.rejectValue(EXPIRY_DATE, INVALID_VALUE, "Expiry date cannot be set later one year from today.");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, NAME, REQUIRED, NO_EMPTY_ATTRIBUTE);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, PERMISSIONS, REQUIRED, NO_EMPTY_ATTRIBUTE);
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, EXPIRY_DATE, REQUIRED, NO_EMPTY_ATTRIBUTE);
    }

    private static boolean isDateAfterOneYearFromToday(Date givenDate) {
        LocalDate localGivenDate = givenDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate today = LocalDate.now();
        LocalDate oneYearFromToday = today.plusYears(1);
        return localGivenDate.isAfter(oneYearFromToday);
    }

    private static boolean isDateBeforeTomorrow(Date givenDate) {
        LocalDate localGivenDate = givenDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate tomorrow = LocalDate.now().plusDays(1);
        return localGivenDate.isBefore(tomorrow);
    }

}
