/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExecutionStep;
import org.squashtest.tm.plugin.rest.jackson.model.ExecutionAndCustomFields;
import org.squashtest.tm.plugin.rest.repository.RestExecutionRepository;
import org.squashtest.tm.plugin.rest.repository.RestExecutionStepRepository;
import org.squashtest.tm.plugin.rest.service.RestExecutionService;
import org.squashtest.tm.plugin.rest.service.RestInternalCustomFieldValueUpdaterService;
import org.squashtest.tm.service.campaign.IterationModificationService;
import org.squashtest.tm.service.execution.ExecutionModificationService;

import javax.inject.Inject;

/**
 * Created by jthebault on 20/06/2017.
 */
@Service
@Transactional
public class RestExecutionServiceImpl implements RestExecutionService {

    @Inject
    private RestExecutionRepository executionRepository;

    @Inject
    private RestExecutionStepRepository executionStepRepository;

    @Inject
    private RestInternalCustomFieldValueUpdaterService internalCufService;

    @Inject
    private IterationModificationService iterationModService;
    @Inject
    private ExecutionModificationService executionModService;


    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.execution.Execution' , 'READ')")
    public Execution getOne(long id) {
        Execution execution = executionRepository.getReferenceById(id);
        return execution;
    }

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("@apiSecurity.hasPermission(#executionId,'org.squashtest.tm.domain.execution.Execution' , 'READ')")
    public Page<ExecutionStep> findExecutionSteps(long executionId, Pageable pageable) {
        Execution execution = getOne(executionId);//will check null and rights
        return executionStepRepository.findAllByExecution(execution,pageable);
    }

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#exec.wrapped , 'EXECUTE')")
    public void updateExecution(ExecutionAndCustomFields exec) {
        // NOOP is to be done on the execution itself,
        // as we just entered a writable transaction.
        // The custom fields still need to be merged though.
        internalCufService.mergeCustomFields(exec);
    }

    @Override
    public void deleteExecution(Long execId) {
        Execution execution = executionModService.findById(execId);
        executionModService.deleteExecution(execution);
    }

}
