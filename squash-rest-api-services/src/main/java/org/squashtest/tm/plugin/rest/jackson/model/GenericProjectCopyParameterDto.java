/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.squashtest.tm.service.project.GenericProjectCopyParameter;

public class GenericProjectCopyParameterDto {

    @JsonProperty("keep_template_binding")
    private boolean keepTemplateBinding = true; // SQUASH-4548
    @JsonProperty("copy_permissions")
    private boolean copyPermissions;
    @JsonProperty("copy_cuf")
    private boolean copyCUF;
    @JsonProperty("copy_bugtracker_binding")
    private boolean copyBugtrackerBinding;
    @JsonProperty("copy_ai_server_binding")
    private boolean copyAiServerBinding;
    @JsonProperty("copy_automated_projects")
    private boolean copyAutomatedProjects;
    @JsonProperty("copy_infolists")
    private boolean copyInfolists;
    @JsonProperty("copy_milestone")
    private boolean copyMilestone;
    @JsonProperty("copy_allow_tc_modif_from_exec")
    private boolean copyAllowTcModifFromExec;
    @JsonProperty("copy_optional_exec_statuses")
    private boolean copyOptionalExecStatuses;
    @JsonProperty("copy_plugins_activation")
    private boolean copyPluginsActivation;
    @JsonProperty("keep_plugins_binding")
    private boolean keepPluginsBinding;
    @JsonProperty("copy_plugins_configuration")
    private boolean copyPluginsConfiguration;

    public boolean isKeepTemplateBinding() {
        return keepTemplateBinding;
    }

    public void setKeepTemplateBinding(boolean keepTemplateBinding) {
        this.keepTemplateBinding = keepTemplateBinding;
    }

    public boolean isCopyPermissions() {
        return copyPermissions;
    }

    public void setCopyPermissions(boolean copyPermissions) {
        this.copyPermissions = copyPermissions;
    }

    public boolean isCopyCUF() {
        return copyCUF;
    }

    public void setCopyCUF(boolean copyCUF) {
        this.copyCUF = copyCUF;
    }

    public boolean isCopyBugtrackerBinding() {
        return copyBugtrackerBinding;
    }

    public void setCopyBugtrackerBinding(boolean copyBugtrackerBinding) {
        this.copyBugtrackerBinding = copyBugtrackerBinding;
    }

    public boolean isCopyAiServerBinding() {
        return copyAiServerBinding;
    }

    public void setCopyAiServerBinding(boolean copyAiServerBinding) {
        this.copyAiServerBinding = copyAiServerBinding;
    }

    public boolean isCopyAutomatedProjects() {
        return copyAutomatedProjects;
    }

    public void setCopyAutomatedProjects(boolean copyAutomatedProjects) {
        this.copyAutomatedProjects = copyAutomatedProjects;
    }

    public boolean isCopyInfolists() {
        return copyInfolists;
    }

    public void setCopyInfolists(boolean copyInfolists) {
        this.copyInfolists = copyInfolists;
    }

    public boolean isCopyMilestone() {
        return copyMilestone;
    }

    public void setCopyMilestone(boolean copyMilestone) {
        this.copyMilestone = copyMilestone;
    }

    public boolean isCopyAllowTcModifFromExec() {
        return copyAllowTcModifFromExec;
    }

    public void setCopyAllowTcModifFromExec(boolean copyAllowTcModifFromExec) {
        this.copyAllowTcModifFromExec = copyAllowTcModifFromExec;
    }

    public boolean isCopyOptionalExecStatuses() {
        return copyOptionalExecStatuses;
    }

    public void setCopyOptionalExecStatuses(boolean copyOptionalExecStatuses) {
        this.copyOptionalExecStatuses = copyOptionalExecStatuses;
    }

    public boolean isCopyPluginsActivation() {
        return copyPluginsActivation;
    }

    public void setCopyPluginsActivation(boolean copyPluginsActivation) {
        this.copyPluginsActivation = copyPluginsActivation;
    }

    public boolean isKeepPluginsBinding() {
        return keepPluginsBinding;
    }

    public void setKeepPluginsBinding(boolean keepPluginsBinding) {
        this.keepPluginsBinding = keepPluginsBinding;
    }

    public boolean isCopyPluginsConfiguration() {
        return copyPluginsConfiguration;
    }

    public void setCopyPluginsConfiguration(boolean copyPluginsConfiguration) {
        this.copyPluginsConfiguration = copyPluginsConfiguration;
    }

    public static GenericProjectCopyParameter convertDto(GenericProjectCopyParameterDto dto) {
        GenericProjectCopyParameter genericProjectCopyParameter = new GenericProjectCopyParameter();
        genericProjectCopyParameter.setCopyAllowTcModifFromExec(dto.isCopyAllowTcModifFromExec());
        genericProjectCopyParameter.setCopyAutomatedProjects(dto.isCopyAutomatedProjects());
        genericProjectCopyParameter.setCopyBugtrackerBinding(dto.isCopyBugtrackerBinding());
        genericProjectCopyParameter.setCopyAiServerBinding(dto.isCopyAiServerBinding());
        genericProjectCopyParameter.setCopyCUF(dto.isCopyCUF());
        genericProjectCopyParameter.setCopyInfolists(dto.isCopyInfolists());
        genericProjectCopyParameter.setCopyMilestone(dto.isCopyMilestone());
        genericProjectCopyParameter.setCopyOptionalExecStatuses(dto.isCopyOptionalExecStatuses());
        genericProjectCopyParameter.setCopyPermissions(dto.isCopyPermissions());
        genericProjectCopyParameter.setCopyPluginsActivation(dto.isCopyPluginsActivation());
        genericProjectCopyParameter.setKeepTemplateBinding(dto.isKeepTemplateBinding());
        genericProjectCopyParameter.setKeepPluginsBinding(dto.isKeepPluginsBinding());
        genericProjectCopyParameter.setCopyPluginsConfiguration(dto.isCopyPluginsConfiguration());

        return genericProjectCopyParameter;
    }
}
