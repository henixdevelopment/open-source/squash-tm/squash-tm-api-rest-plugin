/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller.helper;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.campaign.CampaignFolder;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.campaign.ExploratorySessionOverview;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.IterationTestPlanItem;
import org.squashtest.tm.domain.campaign.TestSuite;
import org.squashtest.tm.domain.execution.Execution;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.domain.execution.SessionNote;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementFolder;
import org.squashtest.tm.domain.testcase.Dataset;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseExecutionMode;
import org.squashtest.tm.domain.testcase.TestCaseFolder;
import org.squashtest.tm.domain.testcase.TestStep;
import org.squashtest.tm.domain.users.User;
import org.squashtest.tm.plugin.rest.core.web.BasePathAwareLinkBuildingService;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyClearance;
import org.squashtest.tm.plugin.rest.jackson.model.RestPartyPermission;
import org.squashtest.tm.plugin.rest.jackson.model.RestUserPermission;

import javax.inject.Inject;


/**
 * This class defines the set of links for certain entities, in order to share among different controllers
 * these definitions. It is helpful when one handler must return an entity which is normally handled in a different
 * controller.
 *
 * Created by bsiri on 04/07/2017.
 */
@Component
public class ResourceLinksHelper {

    private static final String ATTACHMENTS = "attachments";
    private static final String CONTENT = "content";

    @Inject
    private BasePathAwareLinkBuildingService linkService;

    public void addAllLinksForPartyClearance(Long projectId,EntityModel<RestPartyClearance> res) {
        res.add(linkService.createRelationTo(GenericProject.class, projectId, "self","/clearances"));
    }
    public void addAllLinksForPartyPermission(Long projectId,EntityModel<RestPartyPermission> res) {
        res.add(linkService.createRelationTo(GenericProject.class, projectId, "self","/permissions"));
    }
    public void addAllLinksForUserClearance(Long userID,EntityModel<RestPartyClearance> res) {
        res.add(linkService.createRelationTo(User.class, userID, "self","/clearances"));
    }
    public void addAllLinksForUserPermission(Long userID,EntityModel<RestUserPermission> res) {
        res.add(linkService.createRelationTo(User.class, userID, "self","/permissions"));
    }
    public void addAllLinksForDataset(EntityModel<Dataset> res ){
        Dataset ds = res.getContent();
        if (ds != null) {
            res.add(linkService.createLinkTo(ds.getTestCase()));
        }
    }
    public void addAllLinksForTestCaseFolder(EntityModel<TestCaseFolder> res) {
        TestCaseFolder cf = res.getContent();
        if (cf != null) {
            res.add(linkService.createLinkTo(cf.getProject()));
            res.add(linkService.createRelationTo(cf, CONTENT));
            res.add(linkService.createRelationTo(cf, ATTACHMENTS));
        }
    }
    public void addAllLinksForCampaignFolder(EntityModel<CampaignFolder> res) {
        CampaignFolder cf = res.getContent();
        if (cf != null) {
            res.add(linkService.createLinkTo(cf.getProject()));
            res.add(linkService.createRelationTo(cf, CONTENT));
            res.add(linkService.createRelationTo(cf, ATTACHMENTS));
        }
    }
    public void addAllLinksForRequirementFolder(EntityModel<RequirementFolder> res) {
        RequirementFolder cf = res.getContent();

        if (cf != null) {
            res.add(linkService.createLinkTo(cf.getProject()));
            res.add(linkService.createRelationTo(cf, CONTENT));
            res.add(linkService.createRelationTo(cf, ATTACHMENTS));
        }
    }
    public void addAllLinksForExecution(EntityModel<Execution> res) {
        Execution exec = res.getContent();

        if (exec != null) {
            if (!hasSelfLink(res)) {
                res.add(linkService.createSelfLink(exec));
            }

            res.add(linkService.createLinkTo(exec.getProject()));
            res.add(linkService.createLinkTo(exec.getTestPlan(), "test_plan_item"));
            if (exec.getExecutionMode() != TestCaseExecutionMode.EXPLORATORY) {
                res.add(linkService.createRelationTo(exec, "execution-steps"));
            }
            res.add(linkService.createRelationTo(exec, ATTACHMENTS));
        }
    }

    public void addAllLinksForTestCase(EntityModel<TestCase> res) {
        TestCase tc = res.getContent();
        if (tc != null) {
            res.add(linkService.createLinkTo(tc.getProject()));
            res.add(linkService.createRelationTo(tc, "steps"));
            res.add(linkService.createRelationTo(tc, "parameters"));
            res.add(linkService.createRelationTo(tc, "datasets"));
            res.add(linkService.createRelationTo(tc, ATTACHMENTS));
        }
    }
    public void addAllLinksForTestSuite(EntityModel<TestSuite> res){
        TestSuite ts= res.getContent();

        if (ts != null) {
            res.add(linkService.createLinkTo(ts.getProject()));
            res.add(linkService.createLinkTo(ts.getIteration()));
            res.add(linkService.createRelationTo(ts, "test-plan"));
            res.add(linkService.createRelationTo(ts, ATTACHMENTS));
        }
    }
    public void addAllLinksForRequirement(EntityModel<Requirement> res){
        Requirement requirement = res.getContent();

        if (requirement != null) {
            res.add(linkService.createLinkTo(requirement.getCurrentVersion().getProject()));
            res.add(linkService.createRelationTo(requirement.getCurrentVersion(), "current_version"));
        }
    }
    public void populateLinks(EntityModel<TestStep> res) {
        TestStep step = res.getContent();
        if (step != null) {
            res.add(linkService.createLinkTo(step.getTestCase()));
        }
    }
    public void addAllLinksForIteration(EntityModel<Iteration> res) {
        Iteration iteration = res.getContent();

        if (iteration != null) {
            res.add(linkService.createLinkTo(iteration.getProject()));
            res.add(linkService.createLinkTo(iteration.getCampaign()));
            res.add(linkService.createRelationTo(iteration, "test-suites"));
            res.add(linkService.createRelationTo(iteration, "test-plan"));
            res.add(linkService.createRelationTo(iteration, ATTACHMENTS));
        }
    }
    public void addAllLinksForIterationTestPlanItem(EntityModel<IterationTestPlanItem> res) {
        IterationTestPlanItem  itp = res.getContent();

        if (itp != null) {
            res.add(linkService.createLinkTo(itp.getProject()));

            if (itp.getReferencedTestCase() != null) {
                res.add(linkService.createLinkTo(itp.getReferencedTestCase()));
            }

            if (itp.getReferencedDataset() != null) {
                res.add(linkService.createLinkTo(itp.getReferencedDataset()));
            }

            if (itp.getExploratorySessionOverview() != null) {
                res.add(linkService.createLinkTo(itp.getExploratorySessionOverview()));
            }

            res.add(linkService.createLinkTo(itp.getIteration()));
            res.add(linkService.createRelationTo(itp, "executions"));
        }
    }
    public void addAllLinksForCampaignTestPlanItem(EntityModel<CampaignTestPlanItem> res) {
        CampaignTestPlanItem ctpi = res.getContent();

        if (ctpi != null) {
            res.add(linkService.createLinkTo(ctpi.getProject()));

            if (ctpi.getReferencedTestCase() != null) {
                res.add(linkService.createLinkTo(ctpi.getReferencedTestCase()));
            }

            if (ctpi.getReferencedDataset() != null) {
                res.add(linkService.createLinkTo(ctpi.getReferencedDataset()));
            }
            res.add(linkService.createLinkTo(ctpi.getCampaign()));
        }
    }


    private boolean hasSelfLink(EntityModel<?> res) {
        return res.getLink(IanaLinkRelations.SELF).isPresent();
    }

    public void addAllLinksForExploratorySessionOverview(EntityModel<ExploratorySessionOverview> res) {
        ExploratorySessionOverview exploSession = res.getContent();

        if (exploSession != null) {
            IterationTestPlanItem itp = exploSession.getIterationTestPlanItem();
            if (itp !=null) {
                addExploratorySessionLinks(res, itp);
            }
        }
    }

    public void addAllLinksForExploratoryExecution(EntityModel<ExploratoryExecution> res) {
        ExploratoryExecution exec = res.getContent();

        if (exec != null) {
            if (!hasSelfLink(res)) {
                res.add(linkService.createSelfLink(ExploratoryExecution.class, exec.getId()));
            }

            res.add(linkService.createLinkTo(exec.getTestPlan(), "test_plan_item"));
            res.add(linkService.createLinkTo(exec.getProject()));
            res.add(linkService.createLinkTo(exec.getTestPlan().getExploratorySessionOverview(), "exploratory_session"));
            res.add(linkService.createRelationTo(exec, ATTACHMENTS));
        }
    }

    private void addExploratorySessionLinks(EntityModel<ExploratorySessionOverview> res, IterationTestPlanItem itp) {
        res.add(linkService.createLinkTo(itp.getProject()));

        if (itp.getReferencedTestCase() != null) {
            res.add(linkService.createLinkTo(itp.getReferencedTestCase()));
        }
        res.add(linkService.createLinkTo(itp.getIteration()));
    }

    public void addAllLinksForSessionNote(EntityModel<SessionNote> res) {
        SessionNote note = res.getContent();
        if (note != null) {
            if (!hasSelfLink(res)) {
                res.add(linkService.createSelfLink(SessionNote.class, note.getId()));
            }
            res.add(linkService.createLinkTo(note.getProject()));
            res.add(linkService.createLinkTo(note.getExecution(), "exploratory_execution"));
            res.add(linkService.createRelationTo(note, ATTACHMENTS));
        }
    }
}
