== Reports

This chapter focuses on services for the reports.

=== image:get.png[] Get report

A `GET` to `/reports/{id}` returns the report with the given id.

operation::RestReportingControllerIT/get-report[snippets='path-parameters,http-request,request-parameters,http-response,response-fields,links']

=== image:delete.png[] Delete reports

A `DELETE` to `/reports/{ids}` deletes one or several report(s) with the given id(s).

operation::RestReportingControllerIT/delete-reports[snippets='path-parameters,http-request']
