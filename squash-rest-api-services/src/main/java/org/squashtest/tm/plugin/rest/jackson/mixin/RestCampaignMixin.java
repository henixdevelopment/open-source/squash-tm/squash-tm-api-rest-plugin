/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.mixin;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.squashtest.tm.domain.campaign.ActualTimePeriod;
import org.squashtest.tm.domain.campaign.CampaignStatus;
import org.squashtest.tm.domain.campaign.CampaignTestPlanItem;
import org.squashtest.tm.domain.campaign.Iteration;
import org.squashtest.tm.domain.campaign.ScheduledTimePeriod;
import org.squashtest.tm.domain.campaign.TestPlanStatus;
import org.squashtest.tm.plugin.rest.jackson.model.RestCustomFieldMembers;
import org.squashtest.tm.plugin.rest.jackson.serializer.CampaignStatusWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.CustomFieldValuesPropertyWriter;
import org.squashtest.tm.plugin.rest.jackson.serializer.HateoasWrapperConverter;

import java.util.List;

/**
 * Created by jthebault on 19/06/2017.
 */
@JsonAutoDetect(fieldVisibility= JsonAutoDetect.Visibility.NONE, getterVisibility= JsonAutoDetect.Visibility.NONE, isGetterVisibility= JsonAutoDetect.Visibility.NONE)
@JsonPropertyOrder({"_type", "id", "name","reference", "description", "status", "progress_status", "project", "path", "parent", "audit", "scheduledPeriod", "actualPeriod", "custom_fields", "iterations", "test_plan"
})
@JsonAppend(props= {
    @JsonAppend.Prop(name="progress_status",type = TestPlanStatus.class, value= CampaignStatusWriter.class),
    @JsonAppend.Prop(name="custom_fields",type = RestCustomFieldMembers.class, value=CustomFieldValuesPropertyWriter.class),
})
@JsonTypeName("campaign")
public abstract class RestCampaignMixin extends RestGenericLibraryNodeMixin {

    @JsonProperty
    private String reference;

    @JsonUnwrapped
    private ScheduledTimePeriod scheduledPeriod;

    @JsonUnwrapped
    private ActualTimePeriod actualPeriod;

    @JsonProperty
    private CampaignStatus status;

    @JsonProperty
    @JsonSerialize(contentConverter=HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private List<Iteration> iterations;

    @JsonProperty("test_plan")
    @JsonSerialize(contentConverter=HateoasWrapperConverter.class)
    @JsonTypeInfo(use= JsonTypeInfo.Id.NONE)
    private List<CampaignTestPlanItem> testPlan;

    @JsonProperty("progress_status")
    private TestPlanStatus progressStatus;


}
