/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.squashtest.tm.domain.campaign.Campaign;
import org.squashtest.tm.domain.campaign.CampaignLibraryNode;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.domain.project.GenericProject;
import org.squashtest.tm.domain.requirement.Requirement;
import org.squashtest.tm.domain.requirement.RequirementLibraryNode;
import org.squashtest.tm.domain.resource.Resource;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.testcase.TestCaseLibraryNode;

import java.util.List;

public interface RestGenericProjectRepository extends JpaRepository<GenericProject, Long>{

	@Override
	GenericProject getOne(Long id);

	GenericProject getOneByName(String projectName);

	@Override
	Page<GenericProject> findAll(Pageable pageable);

    @Query("from ProjectTemplate pt")
    Page<GenericProject> findAllTemplates(Pageable pageable);

	@Query("select nodes from RequirementLibraryNode nodes where nodes.project.id = :projectId")
    Page<RequirementLibraryNode<Resource>> findRequirementLibraryAllContent(@Param("projectId") long id, Pageable pageable);

	@Query("select nodes from Project p join p.requirementLibrary rl join rl.rootContent nodes where p.id = :projectId")
	Page<RequirementLibraryNode<Resource>> findRequirementLibraryRootContent(@Param("projectId") long id, Pageable pageable);

	@Query("select nodes from TestCaseLibraryNode nodes where nodes.project.id = :projectId")
	Page<TestCaseLibraryNode> findTestCaseLibraryAllContent(@Param("projectId") long id, Pageable pageable);

	@Query("select nodes from Project p join p.testCaseLibrary tcl join tcl.rootContent nodes where p.id = :projectId")
	Page<TestCaseLibraryNode> findTestCaseLibraryRootContent(@Param("projectId") long id, Pageable pageable);

	@Query("select nodes from CampaignLibraryNode nodes where nodes.project.id = :projectId")
	Page<CampaignLibraryNode> findCampaignLibraryAllContent(@Param("projectId") long id, Pageable pageable);

	@Query("select nodes from Project p join p.campaignLibrary cl join cl.rootContent nodes where p.id = :projectId")
	Page<CampaignLibraryNode> findCampaignLibraryRootContent(@Param("projectId") long id, Pageable pageable);

	@Query("select reqs from Requirement reqs where reqs.project.id = :projectId")
	Page<Requirement> findAllRequirementByProjectId(@Param("projectId") long id, Pageable pageable);

	@Query("select tcs from TestCase tcs where tcs.project.id = :projectId")
	Page<TestCase> findAllTestCaseByProjectId(@Param("projectId") long id, Pageable pageable);

	@Query("select cams from Campaign cams where cams.project.id = :projectId")
	Page<Campaign> findAllCampaignByProjectId(@Param("projectId") long id, Pageable pageable);

	@Query("select project.id, project.name from Project project where project.id in :projectIds group by project.id")
	List<Object[]> findNamesByProjectIds(@Param("projectIds") List<Long> projectIds);

    @Query("select crln from CustomReportLibraryNode crln where crln.library.id = :projectId and crln.entityType <> 'LIBRARY'")
    Page<CustomReportLibraryNode> findAllCustomReportLibraryByProjectId(@Param("projectId") long projectId, Pageable paging);

    @Query("select crln from CustomReportLibraryNode crln where crln.id in :crlnIds and crln.entityType <> 'LIBRARY'")
    Page<CustomReportLibraryNode> findAllCustomReportLibraryByCustomReportLibraryNodeIds(@Param("crlnIds") List<Long> crlnIds, Pageable paging);
}
