/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.cfg.HandlerInstantiator;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;
import com.fasterxml.jackson.databind.jsontype.TypeResolverBuilder;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.ClassUtil;
import com.fasterxml.jackson.databind.util.Converter;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.hateoas.mediatype.MessageResolver;
import org.springframework.hateoas.mediatype.hal.CurieProvider;
import org.springframework.hateoas.mediatype.hal.HalConfiguration;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule.HalHandlerInstantiator;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule.HalLinkListSerializer;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule.HalResourcesSerializer;
import org.springframework.hateoas.server.LinkRelationProvider;
import org.springframework.http.converter.json.SpringHandlerInstantiator;
import org.squashtest.tm.plugin.rest.core.hateoas.SingleRelHalResourcesSerializer;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *     This class is a replace for {@link HalHandlerInstantiator} (from the {@link org.springframework.hateoas.mediatype.hal.Jackson2HalModule}) does the following :
 * </p>
 *
 *
 * <ol>
 *    <li>It handles the special annotations {@link SerializeChainConverter} and {@link org.squashtest.tm.plugin.rest.core.jackson.ChainConverter.DeserialChainConverter}</li>
 *    <li>
 *        It replace some custom serializers of HalHandlerInstantiator, especially the {@link org.springframework.hateoas.mediatype.hal.Jackson2HalModule.HalLinkListSerializer}
 *        by a {@link org.squashtest.tm.plugin.rest.core.hateoas.SingleRelHalResourcesSerializer}
 *    </li>
 *    <li>
 *        Delegates to a {@link SpringHandlerInstantiator} when the object to create is a SpringBean
 *    </li>
 *
 * </ol>
 *
 *
 */
/*
 * Note that the default behavior of {@link HalHandlerInstantiator} is to delegate to the bean factory too and would have
 * been a good candidate for extension, but eventually I decided against it. However the code here is largely inspired by it.
 */
public class SquashRestHalHandlerInstantiator extends HandlerInstantiator {

	private Map<Class<?>, Object> halHandlerInstantiatorSerializers = new HashMap<Class<?>, Object>();

	private final SpringHandlerInstantiator springHandlerInstantiator;

	public SquashRestHalHandlerInstantiator(LinkRelationProvider provider,
																					CurieProvider curieProvider,
																					MessageResolver messageSourceAccessor,
																					AutowireCapableBeanFactory beanFactory){

		// initialize the SpringHandlerInstantiator
		this.springHandlerInstantiator = new SpringHandlerInstantiator(beanFactory);

		// initialize the custom serializers, by initializing a HalHandlerInstantiator and then
		// scrapping the custom serializers from it.

		HalConfiguration configuration = new HalConfiguration().withRenderSingleLinks(HalConfiguration.RenderSingleLinks.AS_SINGLE);
		HalHandlerInstantiator halHI = new HalHandlerInstantiator(provider, curieProvider, messageSourceAccessor, configuration, beanFactory);

		HalLinkListSerializer listSer = (HalLinkListSerializer)halHI.serializerInstance(null, null, HalLinkListSerializer.class);
		HalResourcesSerializer resSer = (HalResourcesSerializer) halHI.serializerInstance(null, null, HalResourcesSerializer.class);

		halHandlerInstantiatorSerializers.put(HalLinkListSerializer.class, listSer);
		// wrap the HalResourcesSerializer with our SingleRelHalResourcesSerializer
		halHandlerInstantiatorSerializers.put(HalResourcesSerializer.class, new SingleRelHalResourcesSerializer(resSer));
	}



	@Override
	public JsonDeserializer<?> deserializerInstance(DeserializationConfig config, Annotated annotated,
			Class<?> deserClass) {
		if (isCustom(deserClass)){
			return getCustomSerializer(deserClass);
		}
		else{
			return springHandlerInstantiator.deserializerInstance(config, annotated, deserClass);
		}
	}

	@Override
	public KeyDeserializer keyDeserializerInstance(DeserializationConfig config, Annotated annotated,
			Class<?> keyDeserClass) {
		if (isCustom(keyDeserClass)){
			return getCustomSerializer(keyDeserClass);
		}
		else{
			return springHandlerInstantiator.keyDeserializerInstance(config, annotated, keyDeserClass);
		}
	}

	@Override
	public JsonSerializer<?> serializerInstance(SerializationConfig config, Annotated annotated, Class<?> serClass) {
		if (isCustom(serClass)){
			return getCustomSerializer(serClass);
		}
		else{
			return springHandlerInstantiator.serializerInstance(config, annotated, serClass);
		}
	}

	@Override
	public TypeResolverBuilder<?> typeResolverBuilderInstance(MapperConfig<?> config, Annotated annotated,
			Class<?> builderClass) {
		if (isCustom(builderClass)){
			return getCustomSerializer(builderClass);
		}
		else{
			return springHandlerInstantiator.typeResolverBuilderInstance(config, annotated, builderClass);
		}
	}

	@Override
	public TypeIdResolver typeIdResolverInstance(MapperConfig<?> config, Annotated annotated, Class<?> resolverClass) {
		if (isCustom(resolverClass)){
			return getCustomSerializer(resolverClass);
		}
		else{
			return springHandlerInstantiator.typeIdResolverInstance(config, annotated, resolverClass);
		}
	}


	
	@Override
	public VirtualBeanPropertyWriter virtualPropertyWriterInstance(MapperConfig<?> config, Class<?> implClass) {
		if (isCustom(implClass)){
			return getCustomSerializer(implClass);
		}
		else{
			return springHandlerInstantiator.virtualPropertyWriterInstance(config, implClass); 
		}
	}
	

	
	@Override
	public Converter<?, ?> converterInstance(MapperConfig<?> config, Annotated annotated, Class<?> implClass) {
		if (isCustom(implClass)){
			return getCustomSerializer(implClass);
		}
		else if (isChainConverter(implClass)){
			return instantiateChainConverter(config, annotated, implClass);
		}
		else{
			return springHandlerInstantiator.converterInstance(config, annotated, implClass); 
		}
	}
	
	
	
	
	private boolean isChainConverter(Class<?> clazz){
		return ChainConverter.class.isAssignableFrom(clazz);
	}
	
	
	private ChainConverter<?,?> instantiateChainConverter(MapperConfig<?> config, Annotated annotated, Class<?> implClass){
		
		ChainConverter<?,?> converter = (ChainConverter<?,?>)ClassUtil.createInstance(implClass, config.canOverrideAccessModifiers());
		
		SerializeChainConverter serAnnot =   annotated.getAnnotation(SerializeChainConverter.class);
		DeserializeChainConverter deserAnnot =   annotated.getAnnotation(DeserializeChainConverter.class);
		
		Class<? extends Converter>[] classes = converter.convertersFrom(serAnnot, deserAnnot);		

		for (Class<? extends Converter> clz : classes){
			Converter<?,?> conv = converterInstance(config, annotated, clz);
			converter.addConverter(conv);
		}
	
		return converter;
	}
	
	
	private boolean isCustom(Class<?> clazz){
		return halHandlerInstantiatorSerializers.containsKey(clazz);
	}
	
	@SuppressWarnings("unchecked")
	private <T> T getCustomSerializer(Class<?> clazz){
		return (T) halHandlerInstantiatorSerializers.get(clazz);
	}
}
