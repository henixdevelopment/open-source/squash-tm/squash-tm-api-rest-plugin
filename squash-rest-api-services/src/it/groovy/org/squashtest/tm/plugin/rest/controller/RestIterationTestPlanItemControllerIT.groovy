/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.execution.Execution
import org.squashtest.tm.domain.testcase.TestCaseStatus
import org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.service.RestEnvironmentVariablesService
import org.squashtest.tm.plugin.rest.service.RestIterationTestPlanItemService
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService
import org.squashtest.tm.service.denormalizedfield.DenormalizedFieldValueManager
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import javax.inject.Inject

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.AllInOne
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.campaign
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.dataset
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.executionStep
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.iteration
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.iterationTestPlanItem
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.itpi
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.keywordTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scriptedTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.user

@WebMvcTest(RestIterationTestPlanItemController)
class RestIterationTestPlanItemControllerIT extends BaseControllerSpec {

    @Inject
    private RestIterationTestPlanItemService service;

    @Inject
    private CustomFieldValueFinderService cufService

    @Inject
    private RestEnvironmentVariablesService variableService;

    @Inject
    private DenormalizedFieldValueManager denoService

    def "create-execution"() {

        given:
        def execution = SquashEntityBuilder.execution {
            testPlan = itpi {
                iteration = iteration {
                    campaign = campaign {
                        project = project {
                            id = 15L
                        }
                    }
                }
            }
            id = 25L
            name = "Christmas turkey test flight"
            executionOrder = 0
            executionStatus "READY"
            reference = "CHR-T024"
            datasetLabel = ""
            steps = [
                    executionStep {
                        id = 50L
                        action = "<p>arm the slingshot</p>"
                        expectedResult = "<p>slingshot is armed</p>"
                    },
                    executionStep {
                        id = 51L
                        action = "<p>install the turkey</p>"
                        expectedResult = "<p>the turkey groans and is in place</p>"
                    },
                    executionStep {
                        id = 52L
                        action = "<p>release the slingshot</p>"
                        expectedResult = "<p>the turkey groans, at a distance though</p>"
                    }
            ]
            description = null
            tcdescription = "<p>Will test the aerodynamic profile of a sample turkey</p>"
            importance "LOW"
            nature "NAT_PERFORMANCE_TESTING"
            type "TYP_COMPLIANCE_TESTING"
            testCaseStatus = TestCaseStatus.WORK_IN_PROGRESS
        }

        and:
        service.createExecution(_) >> execution
        variableService.findAllDenormalizedEnvironmentVariableBoundToExecution(execution) >>[]
        cufService.findAllCustomFieldValues(execution) >> []
        denoService.findAllForEntity(execution) >> []

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/iteration-test-plan-items/{id}/executions", 265)
                .accept("application/json")
                .contentType("application/json"))


        then:

        def steptester = { id, action, result ->
            "_type".is "execution-step"
            "id".is id
            "execution_status".is "READY"
            "action".is action
            "expected_result".is result
            "_links.self.href".is "http://localhost:8080/api/rest/latest/execution-steps/" + id
        }

        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "execution"
            "id".is 25
            "name".is "Christmas turkey test flight"
            "execution_order".is 0
            "execution_status".is "READY"
            "last_executed_by".is null
            "last_executed_on".is null
            "execution_mode".is "MANUAL"
            "reference".is "CHR-T024"
            "dataset_label".is ""
            "execution_steps".test {
                "[0]".test steptester.curry(50, "<p>arm the slingshot</p>", "<p>slingshot is armed</p>")
                "[1]".test steptester.curry(51, "<p>install the turkey</p>", "<p>the turkey groans and is in place</p>")
                "[2]".test steptester.curry(52, "<p>release the slingshot</p>", "<p>the turkey groans, at a distance though</p>")
            }
            "comment".is null
            "prerequisite".is ""
            "description".is "<p>Will test the aerodynamic profile of a sample turkey</p>"
            "importance".is "LOW"
            "nature.code".is "NAT_PERFORMANCE_TESTING"
            "type.code".is "TYP_COMPLIANCE_TESTING"
            "test_case_status".is "WORK_IN_PROGRESS"
            "custom_fields".hasSize 0
            "test_case_custom_fields".hasSize 0
            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/executions/25"
                "project".linksTo "http://localhost:8080/api/rest/latest/projects/15"
                "execution-steps".linksTo "http://localhost:8080/api/rest/latest/executions/25/execution-steps"
            }

        }

        /*
         * Documentation
         */

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the test plan element"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }
                    fields {
                        relaxed = true
                        add "_type (string) : the type of the entity, etc"
                    }
                    _links {
                        add "self : the link to this execution"
                        add "project : the link to the execution project"
                        add "test_plan_item : the test plan item of this execution"
                        add "execution-steps : the link to the execution steps"
                        add "attachments : the attachments to the test plan element"
                    }
                }
        ))


    }

    def "create-scripted-execution"() {

        given:
        def execution = SquashEntityBuilder.scriptedExecution {
            testPlan = itpi {
                iteration = iteration {
                    campaign = campaign {
                        project = project {
                            id = 15L
                        }
                    }
                }
            }
            id = 25L
            name = "Christmas turkey test flight"
            executionOrder = 0
            executionStatus "READY"
            reference = "CHR-T024"
            datasetLabel = ""
            steps = [

            ]
            scriptName = "This is script name"
            description = null
            tcdescription = "<p>Will test the aerodynamic profile of a sample turkey</p>"
            importance "LOW"
            nature "NAT_PERFORMANCE_TESTING"
            type "TYP_COMPLIANCE_TESTING"
            testCaseStatus = TestCaseStatus.WORK_IN_PROGRESS
        }

        and:
        service.createExecution(_) >> execution
        variableService.findAllDenormalizedEnvironmentVariableBoundToExecution(execution) >>[]
        cufService.findAllCustomFieldValues(execution) >> []
        denoService.findAllForEntity(execution) >> []

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/iteration-test-plan-items/{id}/executions", 265)
                .accept("application/json")
                .contentType("application/json"))


        then:

        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "scripted-execution"
            "id".is 25
            "name".is "Christmas turkey test flight"
            "execution_order".is 0
            "execution_status".is "READY"
            "last_executed_by".is null
            "last_executed_on".is null
            "execution_mode".is "MANUAL"
            "reference".is "CHR-T024"
            "dataset_label".is ""
            "execution_steps".hasSize 0
            "comment".is null
            "prerequisite".is ""
            "description".is "<p>Will test the aerodynamic profile of a sample turkey</p>"
            "importance".is "LOW"
            "nature.code".is "NAT_PERFORMANCE_TESTING"
            "type.code".is "TYP_COMPLIANCE_TESTING"
            "test_case_status".is "WORK_IN_PROGRESS"
            "custom_fields".hasSize 0
            "script_name".is "This is script name"
            "test_case_custom_fields".hasSize 0
            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/executions/25"
                "project".linksTo "http://localhost:8080/api/rest/latest/projects/15"
                "execution-steps".linksTo "http://localhost:8080/api/rest/latest/executions/25/execution-steps"
            }

        }

    }

    def "create-keyword-execution"() {

        given:
        def execution = SquashEntityBuilder.keywordExecution {
            testPlan = itpi {
                iteration = iteration {
                    campaign = campaign {
                        project = project {
                            id = 15L
                        }
                    }
                }
            }
            id = 25L
            name = "Monday test scenario"
            executionOrder = 0
            executionStatus "READY"
            reference = "CHR-T024"
            datasetLabel = ""
            steps = [
                    executionStep {
                        id = 50L
                        action = "GIVEN today is monday"
                        expectedResult = null
                    },
                    executionStep {
                        id = 51L
                        action = "WHEN i go to work"
                        expectedResult = null
                    },
                    executionStep {
                        id = 52L
                        action = "THEN i am at the office"
                        expectedResult = null
                    }
            ]
            description = null
            tcdescription = "<p>Monday testing</p>"
            importance "LOW"
            nature "NAT_PERFORMANCE_TESTING"
            type "TYP_COMPLIANCE_TESTING"
            testCaseStatus = TestCaseStatus.WORK_IN_PROGRESS
        }

        and:
        service.createExecution(_) >> execution
        variableService.findAllDenormalizedEnvironmentVariableBoundToExecution(execution) >>[]
        cufService.findAllCustomFieldValues(execution) >> []
        denoService.findAllForEntity(execution) >> []

        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/iteration-test-plan-items/{id}/executions", 265)
                .accept("application/json")
                .contentType("application/json"))


        then:

        def steptester = { id, action, result ->
            "_type".is "execution-step"
            "id".is id
            "execution_status".is "READY"
            "action".is action
            "expected_result".is result
            "_links.self.href".is "http://localhost:8080/api/rest/latest/execution-steps/" + id
        }

        /*
         * Test
         */
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "keyword-execution"
            "id".is 25
            "name".is "Monday test scenario"
            "execution_order".is 0
            "execution_status".is "READY"
            "last_executed_by".is null
            "last_executed_on".is null
            "execution_mode".is "MANUAL"
            "reference".is "CHR-T024"
            "dataset_label".is ""
            "execution_steps".test {
                "[0]".test steptester.curry(50, "GIVEN today is monday", null)
                "[1]".test steptester.curry(51, "WHEN i go to work", null)
                "[2]".test steptester.curry(52, "THEN i am at the office", null)
            }
            "comment".is null
            "prerequisite".is ""
            "description".is "<p>Monday testing</p>"
            "importance".is "LOW"
            "nature.code".is "NAT_PERFORMANCE_TESTING"
            "type.code".is "TYP_COMPLIANCE_TESTING"
            "test_case_status".is "WORK_IN_PROGRESS"
            "custom_fields".hasSize 0
            "test_case_custom_fields".hasSize 0
            "_links".test {
                "self".linksTo "http://localhost:8080/api/rest/latest/executions/25"
                "project".linksTo "http://localhost:8080/api/rest/latest/projects/15"
                "execution-steps".linksTo "http://localhost:8080/api/rest/latest/executions/25/execution-steps"
            }

        }

    }

    def "get-iteration-test-plan-item"() {

        given:

        def iterationTestPlanItem = SquashEntityBuilder.iterationTestPlanItem {
            id = 6L
            executionStatus "SUCCESS"
            referencedTestCase = SquashEntityBuilder.testCase {
                id = 3L
                name = "Test Case 3"
                reference = "TC3"
            }
            referencedDataset = SquashEntityBuilder.dataset {
                id = 2L
                name = "Dataset 2"
            }
            lastExecutedBy = "User 6"
            lastExecutedOn "2017/02/04"
            assignedUser "User 6"
            executions = [
                    SquashEntityBuilder.execution {
                        id = 10L
                        executionStatus "SUCCESS"
                        lastExecutedBy = "User 6"
                        lastExecutedOn "2017/02/04"
                    }
            ]
            iteration = SquashEntityBuilder.iteration {
                id = 1L
                name = "Iteration 1"
                reference = "IT1"
                campaign = SquashEntityBuilder.campaign {
                    project = SquashEntityBuilder.project {
                        id = 1L
                    }
                }
            }
        }

        and:

        service.getOne(4) >> iterationTestPlanItem

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/iteration-test-plan-items/{id}", 4)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "iteration-test-plan-item"
            "id".is 6
            "execution_status".is "SUCCESS"

            "referenced_test_case".test {
                "_type".is "test-case"
                "id".is 3
                "name".is "Test Case 3"
                "reference".is "TC3"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/3"
            }

            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 2
                "name".is "Dataset 2"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/2"
            }

            "last_executed_on".is "2017-02-04T11:00:00.000+00:00"
            "assigned_to".is "User 6"

            "executions".hasSize 1
            "executions[0]".test {
                "_type".is "execution"
                "id".is 10
                "execution_status".is "SUCCESS"
                "last_executed_by".is "User 6"
                "last_executed_on".is "2017-02-04T11:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/10"
            }

            "iteration".test {
                "_type".is "iteration"
                "id".is 1
                "name".is "Iteration 1"
                "reference".is "IT1"
                selfRelIs "http://localhost:8080/api/rest/latest/iterations/1"
            }

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/6"
                "test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/3"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/1"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/2"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/1"
                "executions.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/6/executions"
            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the iteration test plan item"
                    }
                    requestParams {
                        add DescriptorLists.fieldsParams
                    }

                    fields {
                        add "_type (string) : the type of this entity"
                        add "id (number) : the id of this iteration test plan item"
                        add "execution_status (string) : the execution status of this item"
                        addAndStop "referenced_test_case (object) : the corresponding test case of this item"
                        addAndStop "referenced_dataset (object) : the referenced dataset of this item"
                        add "last_executed_by (string) : the login of the user who last executed this item"
                        add "last_executed_on (string) : the date this item was last executed"
                        add "assigned_to (string) : the login of the user this item is assigned to"
                        addAndStop "executions (array) : all the executions of this item"
                        addAndStop "iteration (object) : the iteration this item belongs to"
                        add DescriptorLists.linksFields
                    }

                    _links {
                        add "self : link to this iteration test plan item"
                        add "project : link to the project this item belongs to"
                        add "test-case : link to the test case corresponding to this item"
                        add "dataset : link to the dataset used in this item"
                        add "iteration : link to the iteration this item belongs to"
                        add "executions : link to the executions of this item"
                    }
                }
        ))
    }

    def "get-iteration-test-plan-item for scripted test case"() {

        given:

        def iterationTestPlanItem = SquashEntityBuilder.iterationTestPlanItem {
            id = 6L
            executionStatus "SUCCESS"
            referencedTestCase = SquashEntityBuilder.scriptedTestCase {
                id = 3L
                name = "Test Case 3"
                reference = "TC3"
            }
            referencedDataset = SquashEntityBuilder.dataset {
                id = 2L
                name = "Dataset 2"
            }
            lastExecutedBy = "User 6"
            lastExecutedOn "2017/02/04"
            assignedUser "User 6"
            executions = [
                    SquashEntityBuilder.scriptedExecution {
                        id = 10L
                        executionStatus "SUCCESS"
                        lastExecutedBy = "User 6"
                        lastExecutedOn "2017/02/04"
                    }
            ]
            iteration = SquashEntityBuilder.iteration {
                id = 1L
                name = "Iteration 1"
                reference = "IT1"
                campaign = SquashEntityBuilder.campaign {
                    project = SquashEntityBuilder.project {
                        id = 1L
                    }
                }
            }
        }

        and:

        service.getOne(4) >> iterationTestPlanItem

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/iteration-test-plan-items/{id}", 4)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "iteration-test-plan-item"
            "id".is 6
            "execution_status".is "SUCCESS"

            "referenced_test_case".test {
                "_type".is "scripted-test-case"
                "id".is 3
                "name".is "Test Case 3"
                "reference".is "TC3"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/3"
            }

            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 2
                "name".is "Dataset 2"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/2"
            }

            "last_executed_on".is "2017-02-04T11:00:00.000+00:00"
            "assigned_to".is "User 6"

            "executions".hasSize 1
            "executions[0]".test {
                "_type".is "scripted-execution"
                "id".is 10
                "execution_status".is "SUCCESS"
                "last_executed_by".is "User 6"
                "last_executed_on".is "2017-02-04T11:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/10"
            }

            "iteration".test {
                "_type".is "iteration"
                "id".is 1
                "name".is "Iteration 1"
                "reference".is "IT1"
                selfRelIs "http://localhost:8080/api/rest/latest/iterations/1"
            }

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/6"
                "scripted-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/3"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/1"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/2"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/1"
                "executions.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/6/executions"
            }
        }

    }

    def "get-iteration-test-plan-item for keyword test case"() {

        given:

        def iterationTestPlanItem = SquashEntityBuilder.iterationTestPlanItem {
            id = 6L
            executionStatus "SUCCESS"
            referencedTestCase = SquashEntityBuilder.keywordTestCase {
                id = 3L
                name = "Test Case 3"
                reference = "TC3"
            }
            referencedDataset = SquashEntityBuilder.dataset {
                id = 2L
                name = "Dataset 2"
            }
            lastExecutedBy = "User 6"
            lastExecutedOn "2017/02/04"
            assignedUser "User 6"
            executions = [
                    SquashEntityBuilder.keywordExecution {
                        id = 10L
                        executionStatus "SUCCESS"
                        lastExecutedBy = "User 6"
                        lastExecutedOn "2017/02/04"
                    }
            ]
            iteration = SquashEntityBuilder.iteration {
                id = 1L
                name = "Iteration 1"
                reference = "IT1"
                campaign = SquashEntityBuilder.campaign {
                    project = SquashEntityBuilder.project {
                        id = 1L
                    }
                }
            }
        }

        and:

        service.getOne(4) >> iterationTestPlanItem

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/iteration-test-plan-items/{id}", 4)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "iteration-test-plan-item"
            "id".is 6
            "execution_status".is "SUCCESS"

            "referenced_test_case".test {
                "_type".is "keyword-test-case"
                "id".is 3
                "name".is "Test Case 3"
                "reference".is "TC3"
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/3"
            }

            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 2
                "name".is "Dataset 2"
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/2"
            }

            "last_executed_on".is "2017-02-04T11:00:00.000+00:00"
            "assigned_to".is "User 6"

            "executions".hasSize 1
            "executions[0]".test {
                "_type".is "keyword-execution"
                "id".is 10
                "execution_status".is "SUCCESS"
                "last_executed_by".is "User 6"
                "last_executed_on".is "2017-02-04T11:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/10"
            }

            "iteration".test {
                "_type".is "iteration"
                "id".is 1
                "name".is "Iteration 1"
                "reference".is "IT1"
                selfRelIs "http://localhost:8080/api/rest/latest/iterations/1"
            }

            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/6"
                "keyword-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/3"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/1"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/2"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/1"
                "executions.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/6/executions"
            }
        }

    }

    def "get-iteration-test-plan-item-executions"() {

        given:

        def executions = [
                SquashEntityBuilder.execution {
                    id = 10L
                    name = "TC1 - Test Case 1"
                    executionOrder = 0
                    executionStatus "FAILURE"
                    lastExecutedBy = "User 8"
                    lastExecutedOn "2017/06/12"
                },
                SquashEntityBuilder.execution {
                    id = 11L
                    name = "TC1 - Test Case 1"
                    executionOrder = 1
                    executionStatus "BLOCKED"
                    lastExecutedBy = "User 8"
                    lastExecutedOn "2017/06/13"
                },
                SquashEntityBuilder.execution {
                    id = 12L
                    name = "TC1 - Test Case 1"
                    executionOrder = 2
                    executionStatus "SUCCESS"
                    lastExecutedBy = "User 8"
                    lastExecutedOn "2017/06/14"
                }
        ]

        and:

        service.findExecutions(1, _) >> { args ->
            new PageImpl<Execution>(executions, args[1], 6)
        }

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/iteration-test-plan-items/{id}/executions?size=3&page=1", 1)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.executions".hasSize 3
            "_embedded.executions[0]".test {
                "_type".is "execution"
                "id".is 10
                "name".is "TC1 - Test Case 1"
                "execution_order".is 0
                "execution_status".is "FAILURE"
                "last_executed_by".is "User 8"
                "last_executed_on".is "2017-06-12T10:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/10"
            }
            "_embedded.executions[1]".test {
                "_type".is "execution"
                "id".is 11
                "name".is "TC1 - Test Case 1"
                "execution_order".is 1
                "execution_status".is "BLOCKED"
                "last_executed_by".is "User 8"
                "last_executed_on".is "2017-06-13T10:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/11"
            }
            "_embedded.executions[2]".test {
                "_type".is "execution"
                "id".is 12
                "name".is "TC1 - Test Case 1"
                "execution_order".is 2
                "execution_status".is "SUCCESS"
                "last_executed_by".is "User 8"
                "last_executed_on".is "2017-06-14T10:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/12"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/1/executions?page=1&size=3"
            "page".test {
                "size".is 3
                "totalElements".is 6
                "totalPages".is 2
                "number".is 1
            }
        }

        /*
        * Documentation
        * */
        res.andDo(doc.document(
                AllInOne.createListEntityContent("executions", "iteration-test-plan-item")
        ))
    }

    def "get-iteration-test-plan-item-executions for scripted test case"() {

        given:

        def executions = [
                SquashEntityBuilder.scriptedExecution {
                    id = 10L
                    name = "TC1 - Test Case 1"
                    executionOrder = 0
                    executionStatus "FAILURE"
                    lastExecutedBy = "User 8"
                    lastExecutedOn "2017/06/12"
                },
                SquashEntityBuilder.scriptedExecution {
                    id = 11L
                    name = "TC1 - Test Case 1"
                    executionOrder = 1
                    executionStatus "BLOCKED"
                    lastExecutedBy = "User 8"
                    lastExecutedOn "2017/06/13"
                },
                SquashEntityBuilder.scriptedExecution {
                    id = 12L
                    name = "TC1 - Test Case 1"
                    executionOrder = 2
                    executionStatus "SUCCESS"
                    lastExecutedBy = "User 8"
                    lastExecutedOn "2017/06/14"
                }
        ]

        and:

        service.findExecutions(1, _) >> { args ->
            new PageImpl<Execution>(executions, args[1], 6)
        }

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/iteration-test-plan-items/{id}/executions?size=3&page=1", 1)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.executions".hasSize 3
            "_embedded.executions[0]".test {
                "_type".is "scripted-execution"
                "id".is 10
                "name".is "TC1 - Test Case 1"
                "execution_order".is 0
                "execution_status".is "FAILURE"
                "last_executed_by".is "User 8"
                "last_executed_on".is "2017-06-12T10:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/10"
            }
            "_embedded.executions[1]".test {
                "_type".is "scripted-execution"
                "id".is 11
                "name".is "TC1 - Test Case 1"
                "execution_order".is 1
                "execution_status".is "BLOCKED"
                "last_executed_by".is "User 8"
                "last_executed_on".is "2017-06-13T10:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/11"
            }
            "_embedded.executions[2]".test {
                "_type".is "scripted-execution"
                "id".is 12
                "name".is "TC1 - Test Case 1"
                "execution_order".is 2
                "execution_status".is "SUCCESS"
                "last_executed_by".is "User 8"
                "last_executed_on".is "2017-06-14T10:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/12"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/1/executions?page=1&size=3"
            "page".test {
                "size".is 3
                "totalElements".is 6
                "totalPages".is 2
                "number".is 1
            }
        }

    }

    def "get-iteration-test-plan-item-executions for keyword test case"() {

        given:

        def executions = [
                SquashEntityBuilder.keywordExecution {
                    id = 10L
                    name = "TC1 - Test Case 1"
                    executionOrder = 0
                    executionStatus "FAILURE"
                    lastExecutedBy = "User 8"
                    lastExecutedOn "2017/06/12"
                },
                SquashEntityBuilder.keywordExecution {
                    id = 11L
                    name = "TC1 - Test Case 1"
                    executionOrder = 1
                    executionStatus "BLOCKED"
                    lastExecutedBy = "User 8"
                    lastExecutedOn "2017/06/13"
                },
                SquashEntityBuilder.keywordExecution {
                    id = 12L
                    name = "TC1 - Test Case 1"
                    executionOrder = 2
                    executionStatus "SUCCESS"
                    lastExecutedBy = "User 8"
                    lastExecutedOn "2017/06/14"
                }
        ]

        and:

        service.findExecutions(1, _) >> { args ->
            new PageImpl<Execution>(executions, args[1], 6)
        }

        when:

        def res = mockMvc.perform(RestDocumentationRequestBuilders.get("/api/rest/latest/iteration-test-plan-items/{id}/executions?size=3&page=1", 1)
                .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        * */
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_embedded.executions".hasSize 3
            "_embedded.executions[0]".test {
                "_type".is "keyword-execution"
                "id".is 10
                "name".is "TC1 - Test Case 1"
                "execution_order".is 0
                "execution_status".is "FAILURE"
                "last_executed_by".is "User 8"
                "last_executed_on".is "2017-06-12T10:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/10"
            }
            "_embedded.executions[1]".test {
                "_type".is "keyword-execution"
                "id".is 11
                "name".is "TC1 - Test Case 1"
                "execution_order".is 1
                "execution_status".is "BLOCKED"
                "last_executed_by".is "User 8"
                "last_executed_on".is "2017-06-13T10:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/11"
            }
            "_embedded.executions[2]".test {
                "_type".is "keyword-execution"
                "id".is 12
                "name".is "TC1 - Test Case 1"
                "execution_order".is 2
                "execution_status".is "SUCCESS"
                "last_executed_by".is "User 8"
                "last_executed_on".is "2017-06-14T10:00:00.000+00:00"
                selfRelIs "http://localhost:8080/api/rest/latest/executions/12"
            }
            selfRelIs "http://localhost:8080/api/rest/latest/iteration-test-plan-items/1/executions?page=1&size=3"
            "page".test {
                "size".is 3
                "totalElements".is 6
                "totalPages".is 2
                "number".is 1
            }
        }
    }

    def "post-iteration-test-plan-item"() {
        given:
        def json = """{
                "_type": "iteration-test-plan-item",

                "test_case": {
                    "_type": "test-case",
                    "id": 25
                },
                "dataset": {
                    "_type": "dataset",
                    "id": 3
                },
                "assigned_to":"User-1"
            }"""

        def itp = iterationTestPlanItem {
            id = 38L
            execution_status = "READY"
            referencedTestCase = testCase {
                id = 25L
                name = "AKM-Test case 1"
            }
            referencedDataset = dataset {
                id = 3L
                name = "Jeu2"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            executions = []
            iteration = iteration {
                id = 4L
                campaign = campaign {
                    project = project {
                        id = 14L
                    }
                }

            }
        }

        and:
        service.addIterationTestPlanItem(_, _) >> itp
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/iterations/{iterationId}/test-plan", 4L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "iteration-test-plan-item"
            "id".is 38
            "execution_status".is "READY"
            "referenced_test_case".test {
                "_type".is "test-case"
                "id".is 25
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/25"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 3
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/3"
            }
            "last_executed_by".is null
            "last_executed_on".is null
            "assigned_to".is "User-1"
            "executions".hasSize 0
            "iteration".test {
                "_type".is "iteration"
                "id".is 4
                selfRelIs "http://localhost:8080/api/rest/latest/iterations/4"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/38"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/14"
                "test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/25"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/3"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/4"
                "executions.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/38/executions"
            }

        }
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "iterationId : the id of the iteration"
                    }
                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "test_case (object) : the test case to include in the test plan (as described below)"
                        add "test_case._type (string) : the type of the entity (always 'test-case')"
                        add "test_case.id (number) : the id of the test case"
                        add "dataset (object) : the dataset to be used when the test case will be executed (optional)"
                        add "dataset._type (string) : the type of the entity (always 'dataset')"
                        add "dataset.id (number) : the id of the dataset. Remember that the dataset must belong to the planned test case."
                        add "assigned_to (string) : the username of the user assigned to this test case (optional)"
                    }
                }

        ))
    }

    def "post-iteration-test-plan-item for scripted test case"() {
        given:
        def json = """{
                "_type": "iteration-test-plan-item",

                "test_case": {
                    "_type": "scripted-test-case",
                    "id": 25
                },
                "dataset": {
                    "_type": "dataset",
                    "id": 3
                },
                "assigned_to":"User-1"
            }"""

        def itp = iterationTestPlanItem {
            id = 38L
            execution_status = "READY"
            referencedTestCase = scriptedTestCase {
                id = 25L
                name = "AKM-Test case 1"
            }
            referencedDataset = dataset {
                id = 3L
                name = "Jeu2"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            executions = []
            iteration = iteration {
                id = 4L
                campaign = campaign {
                    project = project {
                        id = 14L
                    }
                }

            }
        }

        and:
        service.addIterationTestPlanItem(_, _) >> itp
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/iterations/{iterationId}/test-plan", 4L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "iteration-test-plan-item"
            "id".is 38
            "execution_status".is "READY"
            "referenced_test_case".test {
                "_type".is "scripted-test-case"
                "id".is 25
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/25"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 3
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/3"
            }
            "last_executed_by".is null
            "last_executed_on".is null
            "assigned_to".is "User-1"
            "executions".hasSize 0
            "iteration".test {
                "_type".is "iteration"
                "id".is 4
                selfRelIs "http://localhost:8080/api/rest/latest/iterations/4"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/38"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/14"
                "scripted-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/25"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/3"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/4"
                "executions.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/38/executions"
            }

        }

    }

    def "post-iteration-test-plan-item for keyword test case"() {
        given:
        def json = """{
                "_type": "iteration-test-plan-item",

                "test_case": {
                    "_type": "keyword-test-case",
                    "id": 25
                },
                "dataset": {
                    "_type": "dataset",
                    "id": 3
                },
                "assigned_to":"User-1"
            }"""

        def itp = iterationTestPlanItem {
            id = 38L
            execution_status = "READY"
            referencedTestCase = keywordTestCase {
                id = 25L
                name = "AKM-Test case 1"
            }
            referencedDataset = dataset {
                id = 3L
                name = "Jeu2"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            executions = []
            iteration = iteration {
                id = 4L
                campaign = campaign {
                    project = project {
                        id = 14L
                    }
                }

            }
        }

        and:
        service.addIterationTestPlanItem(_, _) >> itp
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.post("/api/rest/latest/iterations/{iterationId}/test-plan", 4L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))

        then:
        res.andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "iteration-test-plan-item"
            "id".is 38
            "execution_status".is "READY"
            "referenced_test_case".test {
                "_type".is "keyword-test-case"
                "id".is 25
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/25"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 3
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/3"
            }
            "last_executed_by".is null
            "last_executed_on".is null
            "assigned_to".is "User-1"
            "executions".hasSize 0
            "iteration".test {
                "_type".is "iteration"
                "id".is 4
                selfRelIs "http://localhost:8080/api/rest/latest/iterations/4"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/38"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/14"
                "keyword-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/25"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/3"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/4"
                "executions.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/38/executions"
            }

        }

    }

    def "patch-modify-iteration-test-plan-item"() {
        given:
        def json = """{
                        "_type": "iteration-test-plan-item",
                         "dataset": {
                            "_type": "dataset",
                            "id": 3
                        },
                        "assigned_to":"User-1"
                    }"""
        def itpi = iterationTestPlanItem {
            id = 50L
            execution_status = "READY"
            referencedTestCase = testCase {
                id = 25L
                name = "AKM-Test case 1"
            }
            referencedDataset = dataset {
                id = 3L
                name = "Jeu2"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            executions = []
            iteration = iteration {
                id = 4L
                campaign = campaign {
                    project = project {
                        id = 14L
                    }
                }

            }
        }

        and:
        service.modifyIterationTestPlan(_, _) >> itpi
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/iteration-test-plan-items/{id}", 50L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "iteration-test-plan-item"
            "id".is 50
            "execution_status".is "READY"
            "referenced_test_case".test {
                "_type".is "test-case"
                "id".is 25
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/25"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 3
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/3"
            }
            "last_executed_by".is null
            "last_executed_on".is null
            "assigned_to".is "User-1"
            "executions".hasSize 0
            "iteration".test {
                "_type".is "iteration"
                "id".is 4
                selfRelIs "http://localhost:8080/api/rest/latest/iterations/4"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/50"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/14"
                "test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/25"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/3"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/4"
                "executions.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/50/executions"
            }

        }
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the iteration test plan item"
                    }
                    requestFields {
                        add "_type (string) : the type of the entity"
                        add "dataset (object) : the dataset to use when the test case is executed (optional). You can remove the dataset by setting this to null."
                        add "dataset._type (string) : the type of the entity ('dataset')"
                        add "dataset.id (number) : the id of this dataset"
                        add "assigned_to (string) : the username of the user assigned to this test (optional). You can assign this test to nobody by setting this to null."
                    }

                }

        ))


    }

    def "patch-modify-iteration-test-plan-item for scripted test case"() {
        given:
        def json = """{
                        "_type": "iteration-test-plan-item",
                         "dataset": {
                            "_type": "dataset",
                            "id": 3
                        },
                        "assigned_to":"User-1"
                    }"""
        def itpi = iterationTestPlanItem {
            id = 50L
            execution_status = "READY"
            referencedTestCase = scriptedTestCase {
                id = 25L
                name = "AKM-Test case 1"
            }
            referencedDataset = dataset {
                id = 3L
                name = "Jeu2"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            executions = []
            iteration = iteration {
                id = 4L
                campaign = campaign {
                    project = project {
                        id = 14L
                    }
                }

            }
        }

        and:
        service.modifyIterationTestPlan(_, _) >> itpi
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/iteration-test-plan-items/{id}", 50L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "iteration-test-plan-item"
            "id".is 50
            "execution_status".is "READY"
            "referenced_test_case".test {
                "_type".is "scripted-test-case"
                "id".is 25
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/25"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 3
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/3"
            }
            "last_executed_by".is null
            "last_executed_on".is null
            "assigned_to".is "User-1"
            "executions".hasSize 0
            "iteration".test {
                "_type".is "iteration"
                "id".is 4
                selfRelIs "http://localhost:8080/api/rest/latest/iterations/4"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/50"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/14"
                "scripted-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/25"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/3"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/4"
                "executions.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/50/executions"
            }

        }

    }

    def "patch-modify-iteration-test-plan-item for keyword test case"() {
        given:
        def json = """{
                        "_type": "iteration-test-plan-item",
                         "dataset": {
                            "_type": "dataset",
                            "id": 3
                        },
                        "assigned_to":"User-1"
                    }"""
        def itpi = iterationTestPlanItem {
            id = 50L
            execution_status = "READY"
            referencedTestCase = keywordTestCase {
                id = 25L
                name = "AKM-Test case 1"
            }
            referencedDataset = dataset {
                id = 3L
                name = "Jeu2"
            }
            user = user {
                id = 486L
                login = "User-1"
            }
            executions = []
            iteration = iteration {
                id = 4L
                campaign = campaign {
                    project = project {
                        id = 14L
                    }
                }

            }
        }

        and:
        service.modifyIterationTestPlan(_, _) >> itpi
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.patch("/api/rest/latest/iteration-test-plan-items/{id}", 50L)
                .accept("application/json")
                .contentType("application/json")
                .content(json))
        then:
        res.andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "iteration-test-plan-item"
            "id".is 50
            "execution_status".is "READY"
            "referenced_test_case".test {
                "_type".is "keyword-test-case"
                "id".is 25
                selfRelIs "http://localhost:8080/api/rest/latest/test-cases/25"
            }
            "referenced_dataset".test {
                "_type".is "dataset"
                "id".is 3
                selfRelIs "http://localhost:8080/api/rest/latest/datasets/3"
            }
            "last_executed_by".is null
            "last_executed_on".is null
            "assigned_to".is "User-1"
            "executions".hasSize 0
            "iteration".test {
                "_type".is "iteration"
                "id".is 4
                selfRelIs "http://localhost:8080/api/rest/latest/iterations/4"
            }
            "_links".test {
                "self.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/50"
                "project.href".is "http://localhost:8080/api/rest/latest/projects/14"
                "keyword-test-case.href".is "http://localhost:8080/api/rest/latest/test-cases/25"
                "dataset.href".is "http://localhost:8080/api/rest/latest/datasets/3"
                "iteration.href".is "http://localhost:8080/api/rest/latest/iterations/4"
                "executions.href".is "http://localhost:8080/api/rest/latest/iteration-test-plan-items/50/executions"
            }

        }

    }

    def "delete-iteration-test-plan-item"() {
        when:
        def res = mockMvc.perform(RestDocumentationRequestBuilders.delete("/api/rest/latest/iteration-test-plan-items/{testPlanItemsIds}", "45,46")
                .accept("application/json")
                .contentType("application/json"))

        then:
        /*
         * Test
         */
        res.andExpect(status().isNoContent())
        1 * service.deleteIterationTestPlan(_)

        /*
         * Documentation
         */
        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "testPlanItemsIds : the list of ids of the iteration test plan items"
                    }
                }
        ))

    }


}
