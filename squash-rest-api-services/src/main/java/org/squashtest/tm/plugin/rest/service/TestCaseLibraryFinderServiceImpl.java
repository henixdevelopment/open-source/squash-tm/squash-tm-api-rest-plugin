/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.squashtest.tm.domain.customfield.CustomFieldValue;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.plugin.rest.service.loader.TestCaseLoader;
import org.squashtest.tm.service.customfield.CustomFieldValueFinderService;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
public class TestCaseLibraryFinderServiceImpl implements TestCaseLibraryFinderService{

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private ExternalDataHandler externalDataHandler;

    @Inject
    private CustomFieldValueFinderService customFieldValueFinderService;

    @Override
    public Page<TestCase> findAllTestCaseByProjectId(long projectId, Pageable pageable, List<String> fields) {
        TestCaseLoader loader = new TestCaseLoader(TestCaseLibraryQuery.FIND_ALL_TEST_CASE_BY_PROJECT_ID.query, Map.of("projectId", projectId), TestCaseLibraryQuery.FIND_ALL_TEST_CASE_BY_PROJECT_ID.countQuery, entityManager);
        Page<TestCase> page = loader.loadEntityPage(fields, pageable);
        loadExternalAttributes(fields, page.getContent());
        return page;
    }

    private void loadExternalAttributes(List<String> fields, List<TestCase> testCases) {
        if (fields.contains("custom_fields")) {
            Map<Long, List<CustomFieldValue>> customFieldValues = customFieldValueFinderService.getCufValuesMapByBoundEntity(testCases);
            testCases.forEach(testCase ->
                Optional.ofNullable(customFieldValues.get(testCase.getId()))
                    .ifPresent(cufValues -> externalDataHandler.addExternalData(testCase, "custom_fields", cufValues))
            );
        }
    }


    private enum TestCaseLibraryQuery {
        FIND_ALL_TEST_CASE_BY_PROJECT_ID("SELECT tc FROM TestCase tc WHERE tc.project.id = :projectId",
            "SELECT COUNT(tc) FROM TestCase tc WHERE tc.project.id = :projectId");

        private final String query;

        private final String countQuery;

        TestCaseLibraryQuery(String query, String countQuery) {
            this.query = query;
            this.countQuery = countQuery;
        }

        public String getQuery() {
            return query;
        }

        public String getCountQuery() {
            return countQuery;
        }
    }
}
