/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.squashtest.tm.domain.execution.ExploratoryExecution
import org.squashtest.tm.plugin.mockmvc.BaseControllerSpec
import org.squashtest.tm.plugin.rest.jackson.model.IssueDto
import org.squashtest.tm.plugin.rest.service.RestExploratoryExecutionService
import org.squashtest.tm.plugin.rest.service.RestExploratorySessionService
import org.squashtest.tm.plugin.rest.service.RestIssueService
import org.squashtest.tm.test.domainbuilder.SquashEntityBuilder

import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.DescriptorLists
import static org.squashtest.tm.plugin.docutils.DocumentationSnippets.documentationBuilder

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.squashtest.tm.plugin.mockmvc.TestHelper.selfRelIs
import static org.squashtest.tm.plugin.mockmvc.TestHelper.withResult

import javax.inject.Inject

@WebMvcTest(RestExploratorySessionController)
class RestExploratorySessionControllerIT extends BaseControllerSpec {

    @Inject
    private RestExploratorySessionService service

    @Inject
    private RestIssueService restIssueService

    @Inject
    private RestExploratoryExecutionService exploratoryExecutionService

    @Inject
    private RestExploratorySessionController controller



    def "get-exploratory-session"() {
        given:
        def session = SquashEntityBuilder.exploratorySession {
            id = 1L
            name = "session 1"
            reference = "REF 1"
            dueDate "2023/01/01"
            sessionDuration = 30
            sessionStatus = "FINISHED"
            comments = ""
            charter = "this is an Exploratory Test Case Charter"
            iterationTestPlanItem = SquashEntityBuilder.iterationTestPlanItem {
                id = 1L
                executionStatus "SUCCESS"
                executions = [SquashEntityBuilder.exploratoryExecution {
                    id = 1L
                    lastExecutedOn "2023/11/03"
                    assigneeUser  "User 1"
                    taskDivision = "User 1 : Test the buttons, User 2 : test the login form"
                    reviewed = false
                }]
                iteration = SquashEntityBuilder.iteration {
                    campaign = SquashEntityBuilder.campaign {
                        project = SquashEntityBuilder.project {
                            id = 1L
                        }
                    }
                }
            }
        }

        and:
        service.getOne(1L) >> session

        when:
        def res = mockMvc.perform (RestDocumentationRequestBuilders.get("/api/rest/latest/exploratory-sessions/{id}", 1L)
                .header("Accept", "application/json"))

        then:
        res.andExpect(status().isOk()).andExpect(content().contentType("application/json"))

        withResult(res) {
            "_type".is "exploratory-session-overview"
            "id".is 1
            "reference".is "REF 1"
            "name".is "session 1"
            "due_date".is "2023-01-01T11:00:00.000+00:00"
            "session_duration".is 30
            "session_status".is "FINISHED"
            "comments".is ""
            "charter".is "this is an Exploratory Test Case Charter"
            "iteration_test_plan_item".test {
                "_type".is "iteration-test-plan-item"
                "id".is 1
                "execution_status".is "SUCCESS"
                "executions".hasSize 1
                "executions[0]".test {
                    "_type".is "exploratory-execution"
                    "id".is 1
                    "last_executed_on".is "2023-11-03T11:00:00.000+00:00"
                    "assignee_user".is "User 1"
                    "task_division".is "User 1 : Test the buttons, User 2 : test the login form"
                    "reviewed".is false
                    selfRelIs("http://localhost:8080/api/rest/latest/exploratory-executions/1")
                }
                selfRelIs("http://localhost:8080/api/rest/latest/iteration-test-plan-items/1")
            }
        }

        /*
         * Documentation
         */

        res.andDo(doc.document(
                documentationBuilder {
                    pathParams {
                        add "id : the id of the exploratory session"
                    }
                    fields {
                        add "_type (string) : the type of the entity"
                        add "id (number) : the id of the exploratory session"
                        add "reference (string) : the reference of the exploratory session"
                        add "name (string) : the name of the exploratory session"
                        add "due_date (string) : the due date of the exploratory session"
                        add "session_duration (number) : the session duration of the exploratory session"
                        add "session_status (string) : the session status of the exploratory session"
                        add "comments (string) : the comments of the exploratory session"
                        add "charter (string) : the charter of the exploratory session"
                        addAndStop "iteration_test_plan_item (object) : the iteration test plan item of the exploratory session. Please refer to the iteration test plan item documentation for more details."
                        addAndStop "iteration_test_plan_item.executions (array) : the exploratory executions of the iteration test plan item. Please refer to the exploratory executions documentation for more details."
                        addAndStop"attachments (array) : the attachments of the exploratory session. Please refer to the attachments documentation for more details."
                        add DescriptorLists.linksFields

                    }
                    _links {
                        add "self : link to this exploratory session"
                        add "project : link to the project of this exploratory session"
                        add "iteration : link to the iteration of this exploratory session"
                        add "issues : link to the issues of this exploratory session"
                    }
                }
        ))
    }

    def "get-exploratory-session-issues"() {
        given:

        def exploratoryExecution = SquashEntityBuilder.exploratoryExecution {
            id = 1L
        }

        def session = SquashEntityBuilder.exploratorySession {
            id = 1L
            name = "session 1"
            iterationTestPlanItem = SquashEntityBuilder.iterationTestPlanItem {
                id = 1L
                executions = [exploratoryExecution]
            }
        }

        def exploratoryExecutionsPage = new PageImpl<ExploratoryExecution>([exploratoryExecution], PageRequest.of(0, 20), 1)

        def issue = new IssueDto(
            remoteIssueId: "165",
            url:new URL("http://192.175.1.51/bugzilla/show_bug.cgi?id=165"),
            executions:[],
            requirements: [],
            sessionNotes: [],
            exploratoryExecutions: exploratoryExecution as Set<ExploratoryExecution>
        )

        def issues = new PageImpl<IssueDto>([issue], PageRequest.of(0, 20), 1)

        and:
        service.getOne(_) >> session
        service.findExploratoryExecutionsByItpiId(_,_) >> exploratoryExecutionsPage
        restIssueService.getIssuesFromExecutionIds(_, _) >> issues
        exploratoryExecutionService.getExploratoryExecutionFromIssues(_,_) >> session.iterationTestPlanItem.executions
        controller.findExploratorySessionIssues(_,_) >> issues

        when:
        def res = mockMvc.perform (RestDocumentationRequestBuilders
            .get("/api/rest/latest/exploratory-sessions/{id}/issues", 1)
            .header("Accept", "application/json"))

        then:

        /*
        * Test (using the TestHelper)
        */
        res.andExpect(status().isOk())
            .andExpect(content().contentType("application/json"))

        withResult(res) {

            "_embedded.issues".hasSize 1
            "_embedded.issues".test {
                "[0]".test {
                    "remoteIssueId".is "165"
                    "url".is "http://192.175.1.51/bugzilla/show_bug.cgi?id=165"
                    "exploratory-executions".hasSize 1
                    "exploratory-executions".test {
                        "[0]".test {
                            "_type".is "exploratory-execution"
                            "id".is 1
                            selfRelIs("http://localhost:8080/api/rest/latest/exploratory-executions/1")
                        }
                    }
                }
            }
            selfRelIs "http://localhost:8080/api/rest/latest/exploratory-sessions/1/issues?page=0&size=20"
            "page".test {
                "size".is 20
                "totalElements".is 1
                "totalPages".is 1
                "number".is 0
            }
        }

        /*
  * Documentation
  */
        res.andDo(doc.document(
            documentationBuilder {
                pathParams {
                    add "id : the id of the exploratory session"
                }
                requestParams {
                    add DescriptorLists.paginationParams
                    add DescriptorLists.fieldsParams
                }
                fields {
                    embeddedAndStop "issues (array) : the issues of this exploratory session."
                    add "_embedded.issues[].remoteIssueId (string) : the remote issue id of the issue linked to the exploratory session."
                    add "_embedded.issues[].url (string) : the url of the issue linked to the exploratory session."
                    add "_embedded.issues[].exploratory-executions (array) : the exploratory executions linked to the issue."
                    add DescriptorLists.paginationFields
                    add DescriptorLists.linksFields
                }
                _links {
                    add DescriptorLists.paginationLinks
                }
            }
        ))
    }
}
