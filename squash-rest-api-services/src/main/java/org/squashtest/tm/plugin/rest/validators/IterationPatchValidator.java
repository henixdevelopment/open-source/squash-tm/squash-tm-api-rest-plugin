/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.squashtest.tm.domain.customfield.BindableEntity;
import org.squashtest.tm.plugin.rest.jackson.model.CustomFieldValueDto;
import org.squashtest.tm.plugin.rest.jackson.model.IterationDto;
import org.squashtest.tm.plugin.rest.validators.helper.IterationValidationHelper;

import javax.inject.Inject;
import java.util.List;

@Component
public class IterationPatchValidator implements Validator {

    private static final String NON_PATCHABLE_ATTRIBUTE = "non patchable attribute";

    @Inject
    private IterationValidationHelper iterationValidationHelper;

    @Override
    public boolean supports(Class<?> clazz) {
        return IterationDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        IterationDto patch = (IterationDto) target;


        iterationValidationHelper.checkProject(patch);
        iterationValidationHelper.checkAndAssignStatus(errors, patch);
        List<CustomFieldValueDto> customFieldValueDtos = patch.getCustomFields();

        iterationValidationHelper.checkCufs(errors, customFieldValueDtos, patch.getProjectId(), BindableEntity.ITERATION);

        checkForbiddenPatchAttributes(errors, patch);
    }

    private void checkForbiddenPatchAttributes(final Errors errors, IterationDto post) {
        if (post.getTestSuites() != null) {
            errors.rejectValue("test-suites", NON_PATCHABLE_ATTRIBUTE, "Only attributes belonging to the iteration itself can be patched. The attribute test suite cannot be patched. Use direct url to the test suites entity instead");
        }
        if(post.getTestPlan() != null) {
            errors.rejectValue("test-plan", NON_PATCHABLE_ATTRIBUTE, "Only attributes belonging to the iteration itself can be patched. The attribute test plan cannot be patched. Use direct url to the test plan entity instead");
        }
    }
}
