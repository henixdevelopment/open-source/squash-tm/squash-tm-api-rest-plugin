/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.Annotations;
import org.springframework.stereotype.Component;
import org.squashtest.tm.domain.bdd.ActionWord;
import org.squashtest.tm.plugin.rest.core.web.BasicResourceAssembler;

import javax.inject.Inject;

@Component
public class ActionWordParametersPropertyWriter extends VirtualBeanPropertyWriter {

    @Inject
    private BasicResourceAssembler assembler;

    public ActionWordParametersPropertyWriter() {
        super();
    }

    public ActionWordParametersPropertyWriter(
            BeanPropertyDefinition propDef,
            Annotations contextAnnotations,
            JavaType declaredType,
            BasicResourceAssembler assembler) {
        super(propDef, contextAnnotations, declaredType);
        this.assembler = assembler;
    }

    @Override
    protected Object value(Object bean, JsonGenerator gen, SerializerProvider prov) {
        ActionWord holder = (ActionWord) bean;
        return holder.getActionWordParams();
    }

    @Override
    public VirtualBeanPropertyWriter withConfig(
            MapperConfig<?> config, AnnotatedClass declaringClass, BeanPropertyDefinition propDef, JavaType type) {
        return new ActionWordParametersPropertyWriter(propDef, declaringClass.getAnnotations(), type, assembler);
    }

}
