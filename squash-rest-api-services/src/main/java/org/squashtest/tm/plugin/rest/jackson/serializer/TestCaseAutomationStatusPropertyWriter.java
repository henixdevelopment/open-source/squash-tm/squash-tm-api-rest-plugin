/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedClass;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.fasterxml.jackson.databind.ser.VirtualBeanPropertyWriter;
import com.fasterxml.jackson.databind.util.Annotations;
import org.squashtest.tm.domain.project.Project;
import org.squashtest.tm.domain.testcase.TestCase;
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequest;
import org.squashtest.tm.domain.tf.automationrequest.RemoteAutomationRequestExtender;

import java.io.IOException;

import static org.squashtest.tm.domain.project.AutomationWorkflowType.NATIVE;
import static org.squashtest.tm.domain.project.AutomationWorkflowType.NATIVE_SIMPLIFIED;
import static org.squashtest.tm.domain.testcase.TestCaseAutomatable.Y;

public class TestCaseAutomationStatusPropertyWriter extends VirtualBeanPropertyWriter {

    private static final String AUTOMATION_STATUS_PROPERTY_LABEL = "automation_status";

    public TestCaseAutomationStatusPropertyWriter() {
        super();
    }

    private TestCaseAutomationStatusPropertyWriter(BeanPropertyDefinition propDef, Annotations contextAnnotations, JavaType declaredType) {
        super(propDef, contextAnnotations, declaredType);
    }

    @Override
    protected Object value(Object bean, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        TestCase testCaseHolder = (TestCase) bean;
        Project project = testCaseHolder.getProject();
        if (Y.equals(testCaseHolder.getAutomatable()) && project.isAllowAutomationWorkflow()) {
            AutomationRequest automRequest = testCaseHolder.getAutomationRequest();
            if (NATIVE.equals(project.getAutomationWorkflowType()) ||
                NATIVE_SIMPLIFIED.equals(project.getAutomationWorkflowType())
            ) {
                return automRequest.getRequestStatus();
            } else {
                RemoteAutomationRequestExtender rare = automRequest.getRemoteAutomationRequestExtender();
                return rare != null ? rare.getRemoteRequestStatus() : writePropertyIfNull(jsonGenerator);
            }
        } else {
            return null;
        }
    }

    private Object writePropertyIfNull(JsonGenerator jsonGenerator) throws IOException {
        jsonGenerator.writeNullField(AUTOMATION_STATUS_PROPERTY_LABEL);
        return null;
    }

    @Override
    public VirtualBeanPropertyWriter withConfig(MapperConfig<?> config, AnnotatedClass declaringClass, BeanPropertyDefinition propDef, JavaType type) {
        return new TestCaseAutomationStatusPropertyWriter(propDef, declaringClass.getAnnotations(), type);
    }
}
