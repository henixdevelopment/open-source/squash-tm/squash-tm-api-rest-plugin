/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.service.impl;

import org.jooq.DSLContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.squashtest.tm.domain.execution.ExploratoryExecution;
import org.squashtest.tm.plugin.rest.repository.RestExploratoryExecutionRepository;
import org.squashtest.tm.plugin.rest.service.RestExploratoryExecutionService;
import org.squashtest.tm.service.internal.repository.display.utils.RequestAliasesConstants;

import javax.inject.Inject;

import java.util.List;

import static org.squashtest.tm.jooq.domain.Tables.*;

@Service
@Transactional
public class RestExploratoryExecutionServiceImpl implements RestExploratoryExecutionService {

    @Inject
    private RestExploratoryExecutionRepository exploratoryExecutionRepository;

    @Inject
    private DSLContext dsl;

    @Override
    @PreAuthorize("@apiSecurity.hasPermission(#id,'org.squashtest.tm.domain.execution.Execution' , 'READ')")
    @Transactional(readOnly=true)
    public ExploratoryExecution getOne(long id) {
        return exploratoryExecutionRepository.getReferenceById(id);
    }

    @Override
    public String getCharterByExploratoryExecutionId(long exploratoryExecutionId) {
        return dsl.select(EXPLORATORY_SESSION_OVERVIEW.CHARTER)
            .from(EXPLORATORY_EXECUTION)
            .leftJoin(ITEM_TEST_PLAN_EXECUTION).on(EXPLORATORY_EXECUTION.EXECUTION_ID.eq(ITEM_TEST_PLAN_EXECUTION.EXECUTION_ID))
            .leftJoin(EXPLORATORY_SESSION_OVERVIEW).on(ITEM_TEST_PLAN_EXECUTION.ITEM_TEST_PLAN_ID.eq(EXPLORATORY_SESSION_OVERVIEW.ITEM_TEST_PLAN_ID))
            .where(EXPLORATORY_EXECUTION.EXECUTION_ID.eq(exploratoryExecutionId))
            .fetchOneInto(String.class);
    }

    @Override
    public List<ExploratoryExecution> getExploratoryExecutionFromIssues(String remoteIssueId, List<Long> executionIds) {
        return dsl.selectDistinct(EXPLORATORY_EXECUTION.EXECUTION_ID.as(RequestAliasesConstants.ID))
            .from(EXPLORATORY_EXECUTION)
            .join(EXECUTION).on(EXPLORATORY_EXECUTION.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
            .join(EXECUTION_ISSUES_CLOSURE)
            .on(EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.eq(EXECUTION.EXECUTION_ID))
            .join(ISSUE)
            .on(ISSUE.ISSUE_ID.eq(EXECUTION_ISSUES_CLOSURE.ISSUE_ID))
            .where(
                EXECUTION_ISSUES_CLOSURE.EXECUTION_ID.in(executionIds)
                    .and(ISSUE.REMOTE_ISSUE_ID.eq(remoteIssueId))
            )
            .fetchInto(ExploratoryExecution.class);
    }
}
