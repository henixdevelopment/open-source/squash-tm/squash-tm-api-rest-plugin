/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.validators.helper;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.squashtest.tm.plugin.rest.jackson.model.RequirementDto;


@Component
public class RequirementDtoValidationHelper extends RestNodeValidationHelper {

    public void checkCurrentVersion(Errors errors, RequirementDto iRequirement) {
        if (iRequirement.getCurrentVersion() == null) {
            errors.rejectValue("current_version", "Undefined current version", "you must give a current version to create a new requirement");
        } else {
            iRequirement.getCurrentVersion().setProjectId(iRequirement.getProject().getId());
        }
    }
}
