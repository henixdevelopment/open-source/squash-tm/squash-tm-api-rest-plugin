/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.core.jackson;

import java.util.HashMap;
import java.util.Map;



/**
 * A FilterExpression is basically a map. Each entry maps a property name to 
 * <ul>
 * 	<li>
 * 		null : it means that the property is either of a basic type, or maybe a relation but no filter was specified for it.
 * 	</li>
 * 	<li>
 * 		FilterExpression : a sub FilterExpression defined for that property, which mean that the property is a relation and should be also filtered.
 * 	</li>
 *  </ul>
 *  
 *  Do not use the constructors directly, use FilterExpressionBuilder to create instances instead. 
 *  
 *  @see FilterExpressionBuilder 
 * 
 * @author bsiri
 *
 */

@SuppressWarnings({"serial","rawtypes"})
public class FilterExpression extends HashMap<String, FilterExpression>{
	
	FilterExpression(){
		super();
	}

	// warning : recursive constructor. Be sure there is no cycle in this.
	FilterExpression(Map<String,Map> fromMap){
		super();
		init(fromMap);
	}
	
	
	private final void init(Map<String,Map> fromMap){
		if (fromMap == null){
			return;
		}
		for (Map.Entry<String,Map> entry : fromMap.entrySet()){
			String ppt = entry.getKey();
			Map val = entry.getValue();
			FilterExpression asFilter = null;
			if (val != null){
				asFilter = new FilterExpression(val);
			}
			put(ppt, asFilter);
		}
	}
}