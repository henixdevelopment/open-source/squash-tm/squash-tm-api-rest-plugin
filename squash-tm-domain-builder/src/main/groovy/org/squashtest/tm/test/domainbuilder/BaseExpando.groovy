/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.test.domainbuilder

import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.text.DateFormat
import java.text.SimpleDateFormat

import static org.squashtest.tm.test.domainbuilder.PropertyHelper.assign

/**
 * Base class for the Expando-based peusdo classes used in our builders
 * 
 * 
 * @author bsiri
 *
 */
abstract class BaseExpando<T> extends Expando{

	private final Class<?> _gentype
	
	
	private static final DateFormat _dateFormat 

	static {
		_dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
		_dateFormat.timeZone = TimeZone.getTimeZone("Europe/Paris")
	}
	/*
	 * That constructor owes much to https://stackoverflow.com/questions/18707582/get-actual-type-of-generic-type-argument-on-abstract-superclass
	 */
	protected BaseExpando(){
		
		Type t = this.class.genericSuperclass
		
		while (!(t instanceof ParameterizedType) || t.rawType != BaseExpando ) {
			if (t instanceof ParameterizedType){
				t = t.rawType.genericSuperclass
			}
			else{
				t = t.genericSuperclass
			}
		}
		
		_gentype = t.actualTypeArguments[0]
		
	}
	
	
	/*
	 *  creates an instance of the class to which the expando is dedicated. Note that fields are directly injected, 
	 *  thus bypassing the setters : this gives you control, however some business rules may not apply as a result.
	 */
	def create(){
		
		def instance = _gentype.newInstance()		
		def fields = _gentype.declaredFields
		Map ppts = this.properties
		
		ppts.each { k,v ->			
			assign instance, k, v			
		}
		
		instance
	}
	
	
	// add time to ensure that it wont default to midnight
	static Date parseDate(String orig){
		def date = (orig.size() == 10) ? "$orig 12:00:00" : orig
		_dateFormat.parse(date)
	}
	
	
	
}
