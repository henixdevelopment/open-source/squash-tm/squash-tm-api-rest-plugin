/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.plugin.rest.controller;

import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.squashtest.tm.domain.customreport.CustomReportLibraryNode;
import org.squashtest.tm.domain.customreport.CustomReportNodeType;
import org.squashtest.tm.plugin.rest.core.web.BaseRestController;
import org.squashtest.tm.plugin.rest.core.web.EntityGetter;
import org.squashtest.tm.plugin.rest.core.web.RestApiController;
import org.squashtest.tm.plugin.rest.core.web.UseDefaultRestApiConfiguration;
import org.squashtest.tm.plugin.rest.service.RestProjectService;
import org.squashtest.tm.plugin.rest.service.RestReportingService;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


@RestApiController(CustomReportLibraryNode.class)
@UseDefaultRestApiConfiguration
public class RestReportingController extends BaseRestController {

    private static final String CANNOT_DELETE_ENTITIES_MESSAGE = "reporting entities cannot be deleted. They don't exist, you don't have permissions on them or they are not ";
    @Inject
    private RestReportingService resChartsService;

    @Inject
    private RestProjectService projectService;

    /*********************** get endpoints ***********************/

    @GetMapping("/charts/{id}")
    @EntityGetter({CustomReportLibraryNode.class})
    public ResponseEntity<EntityModel<CustomReportLibraryNode>> getChart(@PathVariable("id") Long id) {

        CustomReportLibraryNode customReportLibraryNode = resChartsService.getReportingEntity(id, CustomReportNodeType.CHART_NAME);

        EntityModel<CustomReportLibraryNode> res = toEntityModel(customReportLibraryNode);
        String entityType = projectService.toKebabCase((res.getContent()).getEntityType().getTypeName()) + "s";
        projectService.replaceLinksByCustomSelfLinks(res, entityType);

        return ResponseEntity.ok(res);
    }

    @GetMapping("/custom-exports/{id}")
    @EntityGetter({CustomReportLibraryNode.class})
    public ResponseEntity<EntityModel<CustomReportLibraryNode>> getCustomExport(@PathVariable("id") Long id) {

        CustomReportLibraryNode customReportLibraryNode = resChartsService.getReportingEntity(id, CustomReportNodeType.CUSTOM_EXPORT_NAME);

        EntityModel<CustomReportLibraryNode> res = toEntityModel(customReportLibraryNode);
        String entityType = projectService.toKebabCase((res.getContent()).getEntityType().getTypeName()) + "s";
        projectService.replaceLinksByCustomSelfLinks(res, entityType);

        return ResponseEntity.ok(res);
    }

    @GetMapping("/dashboards/{id}")
    @EntityGetter({CustomReportLibraryNode.class})
    public ResponseEntity<EntityModel<CustomReportLibraryNode>> getDashoard(@PathVariable("id") Long id) {

        CustomReportLibraryNode customReportLibraryNode = resChartsService.getReportingEntity(id, CustomReportNodeType.DASHBOARD_NAME);

        EntityModel<CustomReportLibraryNode> res = toEntityModel(customReportLibraryNode);
        String entityType = projectService.toKebabCase((res.getContent()).getEntityType().getTypeName()) + "s";
        projectService.replaceLinksByCustomSelfLinks(res, entityType);

        return ResponseEntity.ok(res);
    }

    @GetMapping("/reporting-folders/{id}")
    @EntityGetter({CustomReportLibraryNode.class})
    public ResponseEntity<EntityModel<CustomReportLibraryNode>> getFolder(@PathVariable("id") Long id) {

        CustomReportLibraryNode customReportLibraryNode = resChartsService.getReportingEntity(id, CustomReportNodeType.FOLDER_NAME);

        /**
         * The entities handled in this class are subtypes of the reporting library,
         * so the URLs need to be manually adjusted to accurately reflect the
         * corresponding entity types and paths.
         */
        EntityModel<CustomReportLibraryNode> res = toEntityModel(customReportLibraryNode);
        String entityType = projectService.toKebabCase("reporting-" + (res.getContent()).getEntityType().getTypeName()) + "s";
        projectService.replaceLinksByCustomSelfLinks(res, entityType);

        return ResponseEntity.ok(res);
    }

    @GetMapping("/reports/{id}")
    @EntityGetter({CustomReportLibraryNode.class})
    public ResponseEntity<EntityModel<CustomReportLibraryNode>> getReport(@PathVariable("id") Long id) {

        CustomReportLibraryNode customReportLibraryNode = resChartsService.getReportingEntity(id, CustomReportNodeType.REPORT_NAME);

        EntityModel<CustomReportLibraryNode> res = toEntityModel(customReportLibraryNode);
        String entityType = projectService.toKebabCase((res.getContent()).getEntityType().getTypeName()) + "s";
        projectService.replaceLinksByCustomSelfLinks(res, entityType);

        return ResponseEntity.ok(res);
    }

    /*********************** delete endpoints ***********************/

    @DeleteMapping("/charts/{ids}")
    public ResponseEntity<String> deleteCharts(@PathVariable("ids") List<Long> ids) {
        return deleteReportingEntities("charts", ids, CustomReportNodeType.CHART_NAME);
    }

    @DeleteMapping("/custom-exports/{ids}")
    public ResponseEntity<String> deleteCustomExports(@PathVariable("ids") List<Long> ids) {
        return deleteReportingEntities("custom exports", ids, CustomReportNodeType.CUSTOM_EXPORT_NAME);
    }

    @DeleteMapping("/dashboards/{ids}")
    public ResponseEntity<String> deleteDashboards(@PathVariable("ids") List<Long> ids) {
        return deleteReportingEntities("dashboards", ids, CustomReportNodeType.DASHBOARD_NAME);
    }

    @DeleteMapping("/reporting-folders/{ids}")
    public ResponseEntity<String> deleteFolders(@PathVariable("ids") List<Long> ids) {
        return deleteReportingEntities("folders", ids, CustomReportNodeType.FOLDER_NAME);
    }

    @DeleteMapping("/reports/{ids}")
    public ResponseEntity<String> deleteReports(@PathVariable("ids") List<Long> ids) {
        return deleteReportingEntities("reports", ids, CustomReportNodeType.REPORT_NAME);
    }

    private ResponseEntity<String> deleteReportingEntities(String entityType, List<Long> ids, String internalEntityType) {
        List<Long> deletableIds = resChartsService.fetchAndDeleteReportingDeletableIds(ids, internalEntityType);

        if (deletableIds.containsAll(ids)) {
            return ResponseEntity.noContent().build();
        } else {
            List<Long> invalidIds = new ArrayList<>(ids);
            invalidIds.removeAll(deletableIds);

            if (invalidIds.size() == ids.size()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body("The " + CANNOT_DELETE_ENTITIES_MESSAGE + entityType + ": " + invalidIds);
            } else {
                return ResponseEntity.status(HttpStatus.MULTI_STATUS)
                    .body("Some " + CANNOT_DELETE_ENTITIES_MESSAGE + entityType + ": " + invalidIds);
            }
        }
    }
}
