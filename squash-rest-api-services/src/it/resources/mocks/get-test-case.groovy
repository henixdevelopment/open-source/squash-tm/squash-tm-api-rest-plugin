/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import org.squashtest.tm.domain.project.AutomationWorkflowType
import org.squashtest.tm.domain.testcase.TestCaseAutomatable
import org.squashtest.tm.domain.tf.automationrequest.AutomationRequestStatus

import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.actionStep
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.automatedTest
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.automatedTestTechnology
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.automationRequest
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.callStep
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufValue
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.dataset
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.parameter
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirementVersion
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scmRepository
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.scmServer
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testAutomatioProject
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.testCase

/*
 * Main test case
 */
def autoTest = automatedTest {
	id = 2l
	name = "script_custom_field_params_all.ta"
	project = testAutomatioProject {
		id = 2l
		jobName = "ta-tests"
	}
}

def automatedTestTechno = automatedTestTechnology {
	id = 2l
	name = "Cucumber"
}

def scmRepo = scmRepository {
	id = 2l
	name = "repo01"
	workingBranch = "master"
	scmServer = scmServer {
		id = 2l
		url = "https://github.com/test"
	}
}

def tc = testCase{
	project = project {
		id = 14l
		name = "sample project"
		allowAutomationWorkflow = true
		automationWorkflowType = AutomationWorkflowType.NATIVE
	}
	
	id = 238l
	name = "walking test"
	
	createdBy "User-1"
	createdOn "2017/06/15"
	lastModifiedBy "User-1"
	lastModifiedOn "2017/06/15"
	
	reference = "TC1"
	importance "LOW"
	nature "NAT_USER_TESTING"
	type "TYP_EVOLUTION_TESTING"
	
	prerequisite = "<p>You must have legs with feet attached to them (one per leg)</p>\n"
	description = "<p>check that you can walk through the API (literally)</p>\n"
	
	steps = [
		actionStep {
			id = 165l
			action = "<p>move \${first_foot} forward</p>\n"
			expectedResult = "<p>I just advanced by one step</p>\n"
		},
		actionStep {
			id = 166l
			action = "<p>move \${second_foot}&nbsp;forward</p>\n"
			expectedResult = "<p>and another step !</p>\n"
		},
		callStep {
			id = 167l
			calledTestCase = testCase{
				id = 239l
				name = "victory dance"
			}
		},
		callStep {
			id = 168l
			calledTestCase = testCase{
				id = 240l
				name = "unauthorized"
			}
		}
	]
	
	parameters = [
		parameter {
			id = 1l
			name = "first_foot"
		},
		parameter {
			id = 2l
			name = "second_foot"
		}
	] as Set
	
	datasets = [
		dataset {
			id = 1l
			name = "right handed people"
		},
		dataset {
			id = 2l
			name = "left handed people"
		}
		
	] as Set
	
	
	requirementVersions = [
		requirementVersion {
			id = 255l
			name = "Must have legs"
			withDefaultRequirement()
		},
		requirementVersion {
			id = 256l
			name = "unauthorized"
			withDefaultRequirement()
		}
		
	]

	automatedTest = autoTest
	automatedTestTechnology = automatedTestTechno
	scmRepository = scmRepo
	automatedTestReference = "repo01/src/resources/script_custom_field_params_all.ta#Test_case_238"
	uuid = "2f7194ca-eb2e-4378-a82d-ddc207c866bd"

	automatable = TestCaseAutomatable.Y
	automationRequest = automationRequest {
		automationPriority = 42
		automationStatus = AutomationRequestStatus.WORK_IN_PROGRESS
	}
}


/*
 * The custom fields
 */
def tcCufs = [
	cufValue {
		code = "CF_TXT"
		inputType "PLAIN_TEXT"
		label = "test level"
		value = "mandatory"
	},
	cufValue {
		code = "CF_TAGS"
		inputType "TAG"
		label = "see also"
		value = ["walking", "bipedal"]
	}
]


def step1Cufs = [
	cufValue {
		code = "CF_TXT"
		inputType "PLAIN_TEXT"
		label = "test level"
		value = "mandatory"
	},
	cufValue {
		code = "CF_TAGS"
		inputType "TAG"
		label = "see also"
		value = ["basic", "walking"]
	}
]



def step2Cufs = [
	cufValue {
		code = "CF_TXT"
		inputType "PLAIN_TEXT"
		label = "test level"
		value = "mandatory"
	},
	cufValue {
		code = "CF_TAGS"
		inputType "TAG"
		label = "see also"
		value = ["basic", "walking"]
	}
]
/*
 * Now pack the mocks and return
 * 
 */
[
	tc : tc,
	tcCufs : tcCufs,
	step1Cufs : step1Cufs,
	step2Cufs : step2Cufs
]
