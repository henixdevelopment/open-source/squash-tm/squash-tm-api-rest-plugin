/*
 *
 *     This file is part of the Squashtest platform.
 *     Copyright (C) 2017 Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.cufValue
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.exploratoryTestCase
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.project
import static org.squashtest.tm.test.domainbuilder.SquashEntityBuilder.requirementVersion

def extc = exploratoryTestCase {
    project = project {
        id = 14l
        name = "sample project"
    }

    id = 1l
    name = "sample exploratory test case"

    charter = "sample charter"
    sessionDuration = 45

    createdBy "admin"
    createdOn "2023/12/15"
    lastModifiedBy "admin"
    lastModifiedOn "2023/12/23"

    reference = "extc1"
    importance "MEDIUM"
    nature "NAT_PERFORMANCE_TESTING"
    type "TYP_EVOLUTION_TESTING"

    prerequisite = ""
    description = "description of the test case"

    steps = []

    parameters = [] as Set

    datasets = [] as Set

    requirementVersions = [
        requirementVersion {
            id = 255l
            name = "Must have electricity"
            withDefaultRequirement()
        }
    ]

//    automatedTest = null
//    automatedTestTechnology = null
//    scmRepository = null
//    automatedTestReference = null
    uuid = "4a7cdf59-d6ed-4380-89d6-cc393952e425"
}

/*
 * The custom fields
 */
def tcCufs = [
    cufValue {
        code = "CF_TXT"
        inputType "PLAIN_TEXT"
        label = "test level"
        value = "mandatory"
    },
    cufValue {
        code = "CF_TAGS"
        inputType "TAG"
        label = "see also"
        value = ["expresso", "starbuck"]
    }
]

/*
 * Now pack the mocks and return
 *
 */
[
    tc    : extc,
    tcCufs: tcCufs,
]
